SocialExperimentData;
clf;

colors = [];
for i=1:length(Groups)
    colors(i, :) = Groups(i).color;
end
%%
shapes = {'o', 's', 'd', 'p', 'h', '^', 'v', '>', '<', 'o', 's', 'd', 'p', 'h', '^', 'v', '>', '<', 'o', 's', 'd', 'p', 'h', '^', 'v', '>', '<', 'o', 's', 'd', 'p', 'h', '^', 'v', '>', '<', 'o'};
chaseEscapeProb = {};
nLevels = {};
hStrength = {};
Contacts = {};
ChaseEscape = {};
for day = 1:nDays
    chaseEscapeProb{day} = [];
    nLevels{day} = [];
    hStrength{day} = [];
    Contacts{day} = [];
    ChaseEscape{day} = [];
end
pGroupId = inf;
CumEvents = [];
for id=GroupsData.index
    groupId = GroupsData.group(id);
    localId = find(GroupsData.index(GroupsData.group == groupId) == id, 1);
    if pGroupId ~= groupId
        %CumEvents = [];
        CumEvents = [CumEvents; nan(1, size(CumEvents, 2))]
        pGroupId = groupId;
    end

    group = Groups(groupId);
    
    chaseEscape = zeros(obj.nSubjects);
    contacts = zeros(obj.nSubjects);
    rank = zeros(1, obj.nSubjects);
    
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        try
            obj = TrackLoad([obj.OutputPath prefix '.obj.mat'], {'Interactions', 'Hierarchy'});
        catch me
            MyWarning(me)
            corrupt = {corrupt{:}, prefix};
            continue;
        end
        if ~isfield(obj, 'Hierarchy')
            obj = TrackLoad([obj.OutputPath prefix '.obj.mat'], {'common'});
            obj = SocialHierarchy(obj);
        end
        if ~isfield(obj, 'Interactions')
            obj = SocialAnalysePredPreyModel(obj);
        end
        if ~isfield(obj.Interactions.PredPrey, 'PostPredPrey');
            obj = SocialAnalysePredPreyModel(obj);
        end
        chaseEscape = chaseEscape + obj.Interactions.PredPrey.PostPredPrey;
        rank = rank + obj.Hierarchy.ChaseEscape.rank;
%        chaseEscapeProb{day} = [chaseEscapeProb{day}, sum(obj.Interactions.PredPrey.PostPredPrey(:)) ./ (sum(obj.Interactions.PredPrey.Contacts(:)) / 2)];
        nLevels{day} = [nLevels{day}, obj.Hierarchy.ChaseEscape.nLevels];
        hStrength{day} = [hStrength{day}, obj.Hierarchy.ChaseEscape.strength];
        nContacts = 0;
        currContacts = zeros(obj.nSubjects);
        for i=1:obj.nSubjects
            for j=1:obj.nSubjects
                nContacts = nContacts + length(obj.Interactions.PredPrey.events{i, j});
                currContacts(i, j) = currContacts(i, j) + length(obj.Interactions.PredPrey.events{i, j});
            end
        end
        contacts = contacts + currContacts;
        chaseEscapeProb{day} = [chaseEscapeProb{day}, mean(sum(obj.Interactions.PredPrey.PostPredPrey, 2) ./ sum(currContacts, 2))];
        Contacts{day} = [Contacts{day}, nContacts / 2];
        ChaseEscape{day} = [ChaseEscape{day}, sum(obj.Interactions.PredPrey.PostPredPrey(:))];
    end
    %%
    figure(1);
    pChase = sum(chaseEscape, 2) ./ sum(contacts, 2) * 100;
    pEscape = sum(chaseEscape, 1) ./ sum(contacts, 1) * 100;
    [q, order] = sort(rank);
    %c = rgb2hsv(reshape(group.color, [1 1 3])); c(2) = .5 + .5 * localId / sum(group.map); c = hsv2rgb(c); c = c(:)';
    c = group.color;
    color = c;
    plot(pChase(order), pEscape(order), [shapes{localId} ':'], 'Color',  color, 'MarkerFaceColor', color, 'MarkerEdgeColor', 'none', 'MarkerSize', 20, 'LineWidth', 2)
    for i=1:obj.nSubjects
        text(pChase(order(i)), pEscape(order(i)), num2str(obj.nSubjects-i+1), 'VerticalAlignment', 'middle' ,'HorizontalAlignment', 'center', 'Color', 'w', 'FontSize', 10, 'FontWeight', 'bold');
    end
    drawnow;
    hold on;
    figure(2);
    cumEvents = [sum(chaseEscape, 2) sum(chaseEscape, 1)' ];
    cumEvents = [cumEvents sum(contacts, 2) - sum(cumEvents, 2)];
    cumEvents = [cumEvents; inf(1,size(cumEvents, 2))];
    CumEvents = [CumEvents; cumEvents];
    [Tilde, cobj] = MyCategoricalColormap;
    PlotPersonas(CumEvents, true, [cobj.Red; cobj.Blue; cobj.Green]);
    title(GroupsData.Titles{groupId});
    hold on;
end
hold off;
xlabel('contacts that end in chase [%]');
ylabel('contacts that end in escape [%]');

%%


%% measure chase escape levels over days
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(chaseEscapeProb{day}(Groups(g).idx)*100);
        s(day) = stderr(chaseEscapeProb{day}(Groups(g).idx)*100);
        all(day, :) = chaseEscapeProb{day}(Groups(g).idx)*100;
    end
    subplot(1,3,1:2);
    errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    %plot(1:nDays, all)
    hold on;
    %plot(1:nDays, all,'Color', Groups(g).color)
    
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
legend(GroupsData.Titles);
legend boxoff
hold off
set(gca, 'XTick', [1:4]);
xlabel('times [days]');
ylabel('% contacts that end in a chase');
subplot(1,3,3); 
MyBar(1:length(Groups), tm, ts, colors);
set(gca, 'XTickLabel', GroupsData.Initials);
ylabel('% contacts that end in a chase');

%% measure chase escape numbers over days
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(chaseEscapeProb{day}(Groups(g).idx)*100);
        s(day) = stderr(chaseEscapeProb{day}(Groups(g).idx)*100);
        all(day, :) = chaseEscapeProb{day}(Groups(g).idx)*100;
    end
    subplot(1,3,1:2);
    errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    %plot(1:nDays, all)
    hold on;
    %plot(1:nDays, all,'Color', Groups(g).color)
    
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
legend(GroupsData.Titles);
hold off
set(gca, 'XTick', [1:4]);
xlabel('times [days]');
ylabel('% contacts that end in a chase');
subplot(1,3,3); 
MyBar(1:length(Groups), tm, ts, colors);
set(gca, 'XTickLabel', GroupsData.Titles);
ylabel('% contacts that end in a chase');


%% measure number of contacts over days
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(Contacts{day}(Groups(g).idx)/2);
        s(day) = stderr(Contacts{day}(Groups(g).idx)/2);
        all(day, :) = Contacts{day}(Groups(g).idx)/2;
    end
subplot(1,3,1:2)    
    errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    %plot(1:nDays, all)
    hold on;
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
hold off;
legend(GroupsData.Titles);
legend boxoff
set(gca, 'XTick', [1:4]);
xlabel('times [days]');
ylabel('number of contacts');
subplot(1,3,3); 
MyBar(1:length(Groups), tm, ts, colors);
set(gca, 'XTickLabel', GroupsData.Initials);
ylabel('number of contacts');
%% measure number of hierarchy levels over days
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(nLevels{day}(Groups(g).idx));
        s(day) = stderr(nLevels{day}(Groups(g).idx));
        all(day, :) = nLevels{day}(Groups(g).idx);
    end
    subplot(1,3,1:2)
    errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    %plot(1:nDays, all)
    hold on;
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
hold off;
legend(GroupsData.Titles);
subplot(1,3,3); 
MyBar(1:length(Groups), tm, ts, colors);
set(gca, 'XTickLabel', GroupsData.Initials);
%% measure number of hierarchy strength over days
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(hStrength{day}(Groups(g).idx));
        s(day) = stderr(hStrength{day}(Groups(g).idx));
        all(day, :) = hStrength{day}(Groups(g).idx);
    end
    subplot(1,3,1:2)
    errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    %plot(1:nDays, all)
    hold on;
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
hold off;
legend(GroupsData.Titles);
subplot(1,3,3); 
barweb(tm, ts, [], [], [], [], [], colors);
%% measure number of chaseEscape  over days
tm = [];
ts = [];
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(ChaseEscape{day}(Groups(g).idx));
        s(day) = stderr(ChaseEscape{day}(Groups(g).idx));
        all(day, :) = ChaseEscape{day}(Groups(g).idx);
    end
    subplot(1,3,1:2); errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    hold on;
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
hold off;
legend(GroupsData.Titles);
legend boxoff;
subplot(1,3,3); 
MyBar(1:length(Groups), tm, ts, colors);
set(gca, 'XTickLabel', GroupsData.Initials);
