function obj = SocialDiluteZones(obj, fs)
obj = TrackLoad(obj);
obj.zones = blkproc(obj.zones(:, obj.valid), [1, fs], @Majority);
obj.valid = true(1, size(obj.zones, 2));
obj.nFrames = size(obj.zones, 2);
%TrackSave(obj);