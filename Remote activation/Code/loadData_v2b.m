%% Loading the Excel file for all mice (if needed)
fprintf('# loading data: \n');

options.filename = {...
    'Data\Enriched1\cage 1 day 1 analysis (Trial     1)-Arena 1 Subject 1(1).txt'
    'Data\Enriched1\cage 1 day 1 analysis (Trial     1)-Arena 1 Subject 2(1).txt'
    'Data\Enriched1\cage 1 day 1 analysis (Trial     1)-Arena 1 Subject 3(1).txt'
    'Data\Enriched1\cage 1 day 1 analysis (Trial     1)-Arena 1 Subject 4(1).txt'};
options.save = 'Data\Enriched1\cage 1 day 1 analysis (Trial     1)-Arena 1';
options.title = 'cage 1 day 1 analysis (Trial     1)-Arena 1';
options.output_path = 'res/';

global socialZones;
global socialData;
global social;

if (~exist('socialData') || isempty(socialData))
    res = 0;
    if isfield(options, 'save') && ~isempty(options.save)
        res = fileattrib([options.save '.mat']);
        loadfrom = [options.save '.mat'];
        if res == 0
            res = fileattrib([options.save]);
            loadfrom = options.save;
        end
    end
    if res ~= 0
        load(loadfrom);
    else
        orig.x = [];
        orig.y = [];
        orig.distance = [];
        orig.velocity = [];
        orig.area = [];
        orig.elongation = [];
        
        zones.feeder1 = [];
        zones.feeder2 = [];
        zones.water = [];
        zones.labyrinth = [];
        zones.bigNest = [];
        zones.smallNest = [];
        zones.saftyStrip = [];
        zones.upperSmallNest = [];
        zones.entrySmallNest = [];
        zones.saftyStrip = [];
        zones.all = [];
        
        for i=1:options.nSubjects
            fprintf('# - subject %d/%d\n', i, options.nSubjects);
            
            fid = fopen(options.filename{i});
            data = textscan(fid, '', 'Delimiter', ';', 'HeaderLines', 27, 'TreatAsEmpty', '"-"');
            fclose(fid);
            %
            if i == 1
                orig.time       = data{1};
                orig.dt         = orig.time(2) - orig.time(1);
            end
            orig.x          = [orig.x; data{3}(:)'];
            orig.y          = [orig.y; data{4}(:)'];
            %orig.distance   = [orig.distance; data{8}(:)'];
            %orig.velocity   = [orig.velocity; data{9}(:)'];
            %orig.area       = [orig.area;     data{5}(:)'];
            %orig.elongation = [orig.elongation; data{7}(:)'];
            
            %         zones.feeder1        = [zones.feeder1; data{10}];
            %         zones.feeder2        = [zones.feeder2; data{11}];
            %         zones.water          = [zones.water; data{12}];
            %         %zones.upperSmallNest = [zones.smallNest; data{13}];
            %         zones.smallNest      = [zones.smallNest; data{14}];
            %         %zones.entrySmallNest = [zones.smallNest; data{15}];
            %         zones.bigNest        = [zones.bigNest; data{16}];
            %         %zones.entryBigNest   = [zones.smallNest; data{17}];
            %         zones.labyrinth      = [zones.labyrinth; data{18}];
            %         %zones.saftyStrip     = [zones.saftyStrip; data{19}];
            
            all = data{10}(:)' * 0;
            all(data{10}        > 0) = 1; % feeder1
            all(data{11}        > 0) = 2; % feeder2
            all(data{12}        > 0) = 3; % water
            all(data{14}        > 0) = 4; % smallNest
            all(data{16}        > 0) = 5; % bigNest
            all(data{18}        > 0) = 6; % labyrinth
            zones.all = [zones.all; all];
        end
        orig.nData = length(orig.time);
        zones.labels = {'Open Space', 'Feeder #1', 'Feeder #2', 'Water', 'Small-Nest', 'Labyrinth', 'Big-Nest'};
        zones.count = 7;
        fprintf('# - done\n');
        
        orig.colors = {[172 190 206]/255, [208 179 189]/255, [181 73 157]/255, [144 90 209]/255};
        
        socialData = orig;
        socialZones = zones;
 
        if isfield(options, 'save') && ~isempty(options.save)
            fprintf('# - saving data...\n');
            save(options.save, 'socialData', 'socialZones');
        end
    end
else
    fprintf('# - data already loaded\n');
end
