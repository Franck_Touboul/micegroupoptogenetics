function KLine(P0, P1, varargin);
plot3([P0(1) P1(1)], [P0(2) P1(2)], [P0(3) P1(3)], varargin{:});