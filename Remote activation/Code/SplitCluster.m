function cents = SplitCluster(depth, n)
if nargout == 0
    imagesc(depth);
end
im = depth;
im(isnan(im)) = max(im(:));
im = imfill(im);
mx = imregionalmax(-im);
if nargout == 0
    [x,y] = find(mx);
    hon
    plot(y, x, 'k.');
    hoff
end
rp = regionprops(mx, 'Centroid');
%%
[x, y] = find(~isnan(depth));
coord = round(reshape([rp.Centroid], 2, length(rp)));
dist = pdist2(coord', [x,y]);
%%
p = nchoosek(1:length(rp), n);
meand = zeros(1, size(p, 1));
for i=1:size(p, 1)
    curr = p(i, :);
    ldist = dist(curr, :);
    meand(i) = mean(min(ldist, [], 1));
end

[~, idx] = min(meand);
cents = coord(:, p(idx, :))';

if nargout == 0
    hon
    plot(cents(:, 1), cents(:, 2), 'ok');
    hoff
end
