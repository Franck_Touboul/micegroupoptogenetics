function TrackClassifyFunc(data,width,height,frameNr,time)

persistent blobs;
persistent sedisk;
persistent bkgFrame;
persistent options;

if nargin == 2
    options = width;
    blobs = gmm(2, options.nSubjects, 'diag');
    sedisk = strel('disk',options.minRadius);
    bkgFrame = data;
    
    return;
end

size(data)

m = imsubtract(data, bkgFrame);
m = imresize(m, options.scale);
m = im2double(m);
m = (m > options.noiseThresh * std(m(:))) .* m;
if r==1
    [posx, posy] = meshgrid(1:size(m,1), 1:size(m,2));
end
%%
if options.output
    subplot(2,2,1);
    imagesc(m);
end
%%
hsv_m = rgb2hsv(m);
hm = hsv_m(:,:,1);
sm = hsv_m(:,:,2);
vm = hsv_m(:,:,3);
%%
subplot(2,2,3);
bw = sum(m > 0, 3);
cc = bwconncomp(bw);
all = [];
for i=1:cc.NumObjects
    if length(cc.PixelIdxList{i}) < options.noiseSize
        cc.PixelIdxList{i} = [];
    else
        all = [all; cc.PixelIdxList{i}];
    end
end

%bw = bwareaopen(bw, options.noiseSize);
%bw = imerode(bw, strel('disk',5));
%labels = bwlabel(bw);
if options.output
    subplot(2,2,3);
    rgblbls = label2rgb(labels);
    imshow(rgblbls);
end

%%
[b, idx_h] = histc(hm(all), subjectColorBins);
[b, idx_s] = histc(sm(all), subjectColorBins);
[b, idx_v] = histc(vm(all), subjectColorBins);
prob_h = zeros(options.nSubjects, length(idx_h));
prob_s = zeros(options.nSubjects, length(idx_s));
prob_v = zeros(options.nSubjects, length(idx_v));
for i=1:options.nSubjects
    prob_h(i, :) = subjectColors_h(i, idx_h);
    prob_s(i, :) = subjectColors_s(i, idx_s);
    prob_v(i, :) = subjectColors_v(i, idx_v);
end
prob_h = prob_h ./ repmat(sum(prob_h, 1), options.nSubjects, 1);
prob_s = prob_s ./ repmat(sum(prob_s, 1), options.nSubjects, 1);
prob_v = prob_v ./ repmat(sum(prob_v, 1), options.nSubjects, 1);
joint_prob = prob_h .* prob_s .* prob_v;

data = [posx(all), posy(all)];
blob_prob = gmmpost(blobs, data);
joint_prob = joint_prob.*blob_prob';
[m, idx] = max(joint_prob, [], 1);
nlabels = zeros(size(bw));
nlabels(all) = idx;

for i=1:options.nSubjects
    cc = bwconncomp(nlabels == i);
    if cc.NumObjects == 0
        blobs.covars(i, :) = [1 1] * options.searchZones;
        hidden(i, r) = 1;
        continue;
    end
    max_blob = 0;
    max_blob_size = 0;
    for j=1:cc.NumObjects
        if length(cc.PixelIdxList{j}) > max_blob_size
            max_blob_size = length(cc.PixelIdxList{j});
            max_blob = j;
        end
    end
    nlabels(nlabels == i) = 0;
    
    %         map = false(size(nlabels));
    %         map(cc.PixelIdxList{j}) = 1;
    %         %
    %         nlabels(map) = i;
    map = nlabels * 0;
    map(cc.PixelIdxList{max_blob}) = 1;
    map = imopen(map, sedisk);
    
    %nlabels(cc.PixelIdxList{max_blob}) = i;
    nlabels(map > 0) = i;
    currdata = [posx(map > 0), posy(map > 0)];
    
    blobs.covars(i, :) = var(currdata);
    %blobs.covars(i, :) = var(data(idx == i, :));
    if any(isnan(blobs.covars(i, :)))
        blobs.covars(i, :) = [1 1] * options.searchZones;
    else
        %blobs.centres(i, :) = mean(data(idx == i, :));
        blobs.centres(i, :) = mean(currdata);
    end
end

%idx(m < 0.1) = 0;
%[m, idx] = max(prob_v, [], 1);
%%
if options.output
    subplot(2,2,4);
    rgblbls = label2rgb(nlabels);
    imshow(rgblbls);
    drawnow
end
%%
