function obj = TrackCreateGroup(obj)
obj = TrackLoad(obj);
%%
SocialExperimentData;
% data = TrackParse(obj);
% for day=1:GroupsData.nDays
%     data.Day = day;
%     TrackName(obj, data);
% end

id = find(strcmp(regexprep(obj.FilePrefix, 'day[^\.]*', 'day%02d'), experiments));
if ~isempty(id)
    objs = TrackLoad({id});
    TrackSave(objs);
end


