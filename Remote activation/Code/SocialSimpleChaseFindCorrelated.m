function chases = SocialSimpleChaseFindCorrelated(local, s1, s2)

spd1 = local.speed{s1, s2};
spd2 = local.speed{s2, s1};

knnargs = {...
    'NumNeighbors',local.NumNeighbors,...
    'DistanceWeight', 'equal'};

mdl = ClassificationKNN.fit(...
    [[spd1(:), spd2(:)]; [-spd1(:), spd2(:)]], ...
    [ones(length(spd1), 1); 2 * ones(length(spd1), 1)],...
    knnargs{:});
[label1, score] = mdl.predict([spd1(:), spd2(:)]);
bt1 = binotest(score(:, 1)*local.NumNeighbors, local.NumNeighbors);

mdl = ClassificationKNN.fit(...
    [[spd1(:), spd2(:)]; [spd1(:), -spd2(:)]], ...
    [ones(length(spd1), 1); 2 * ones(length(spd1), 1)],...
    knnargs{:});
[label2, score] = mdl.predict([spd1(:), spd2(:)]);
bt2 = binotest(score(:, 1)*local.NumNeighbors, local.NumNeighbors);

chases = false(length(spd1), 1);
% chases(~bt1 & ~bt2 & label1 == 1 & label2 == 1) = true;
chases((~bt1 & label1 == 1) | (~bt2 & label2 == 1)) = true;

if nargout == 0
    h1 = plot(spd1(~chases), spd2(~chases), '.');
    customDataCursor(h1, -local.indices{s1, s2}(~chases));
    hon
    h2 = plot(spd1(chases), spd2(chases), 'r.');
    customDataCursor(h2, local.indices{s1, s2}(chases));
    hoff
    xaxis(-100, 100);
    yaxis(-100, 100);
end