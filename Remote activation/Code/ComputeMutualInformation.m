function [I, H] = ComputeMutualInformation(X, Y)
% [I, H] = ComputeMutualInformation(X, Y)
%   computes mutual information and Entropy between the two matrices

%%
valid = all(~isnan(X), 1) & all(~isnan(Y), 1);
ux = unique(X(:, valid))';
uy = unique(Y(:, valid))';

if isint(ux) && isint(uy)
    bx = X(:, valid) - min(X(:)) + 1;
    by = Y(:, valid) - min(Y(:)) + 1;
else
    [h ,bx] = histc(X(:, valid), ux);
    [h ,by] = histc(Y(:, valid), uy);
end

%%
x = bx - 1; mx = max(x(:)) + 1;
y = by - 1; my = max(y(:)) + 1;
%
vecx = mx.^(0:size(x, 1)-1);
vecy = my.^(0:size(y, 1)-1);
%
ix = vecx * x + 1;
iy = vecy * y + 1;
%%
% p = zeros(mx.^size(x, 1), my.^size(y, 1));
% for i=1:length(ix)
%     p(ix(i), iy(i)) = p(ix(i), iy(i)) + 1;
% end
% p = p / sum(p(:));
edges = cell(1,2); 
edges{1} = 1:mx^size(x, 1); 
edges{2} = 1:my^size(y, 1); 
p = hist3([ix; iy]', 'Edges', edges); 
p=p/sum(p(:));
%%
px = sum(p, 2); px = px / sum(px);
py = sum(p, 1); py = py / sum(py);

H(1) = -px(px > 0)' * log2(px(px > 0));
H(2) = -py(py > 0) * log2(py(py > 0))';

v = p.*log2(p ./ repmat(px, 1, size(p, 2)) ./ repmat(py, size(p, 1), 1));
v(p == 0) = 0;
I = sum(v(:));

%logp = p; 
%logp(p > 0) = log(p(p > 0));
function a = isint(x)
a = all(x - floor(x) == 0);