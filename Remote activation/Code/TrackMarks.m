data = dlmread('aux/contacts.SC.exp0001.day02.cam01.marks', '\t', [1 0 500 13]);
marks = struct();
marks.id       = data(:, 1);
marks.begin    = data(:, 2);
marks.end      = data(:, 3);
marks.subject = data(:, 4:5);
marks.approach = data(:, 6:7);
marks.leave    = data(:, 8:9);
marks.pred     = data(:, 10:11);
marks.prey     = data(:, 12:13);
marks.artifact = data(:, 14);

%%
for s1=1:obj.nSubjects
    for s2=1:obj.nSubjects
        if s1==s2; continue; end
        e = obj.Hierarchy.ChaseEscape.interactions.events{s1, s2};
        m = (marks.subject(:, 1) == s1 & marks.subject(:, 2) == s2) | (marks.subject(:, 2) == s1 & marks.subject(:, 1) == s2);
        
        midx = find(m);
        begf = marks.begin(m);
        endf = marks.end(m);
        curr = struct();
        for i=1:length(e)
            midx2 = midx(find(~(e{i}.BegFrame > endf | e{i}.EndFrame < begf)));
            if isempty(midx2); continue; end
            i1 = find(marks.subject(midx2, :) == s1);
            i2 = 2-i1 + 1;
            curr.approach = any(e{i}.data == 2);
        end
    end
end