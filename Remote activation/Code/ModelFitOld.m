function [z, pos] = ModelFitOld(Model, pos2d)
fields = {...
    'Angle',0,...
    'MajorAxis',1,...
    'DX',0,...
    'DY',0,...
    'Height', 0.05...
    };

Param = struct(fields{:});

if isvector(Model)
    p = Model;
    for i=1:length(p)
        Param.(fields{2*i-1}) = p(i);
    end
else
end
pos = ModelInSpace(Param);
%%
warning('off', 'MATLAB:DelaunayTri:DupPtsWarnId')
dt = DelaunayTri(pos(:, 1), pos(:, 2));
id = dt.nearestNeighbor(pos2d(:, 1), pos2d(:, 2));

z = pos(id, 3);
if nargout == 0
    plot3(pos(:, 1), pos(:, 2), pos(:, 3), '.')
end