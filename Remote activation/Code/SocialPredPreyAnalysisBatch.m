experiments = {...
    'Enriched.exp0006.day%02d.cam01',...
    'Enriched.exp0005.day%02d.cam04',...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam01',...
    'SC.exp0007.day%02d.cam04',...
    };

E.map = logical([1 1 1 0 1 0 1 0 1 0 0 0 0]);
E.idx = find(E.map);

SC.map = ~E.map;
SC.idx = find(SC.map);


%%
% for id=1:length(experiments)
%     DJS = [];
%     for day = 1:nDays
%         prefix = sprintf(experiments{id}, day);
%         fprintf('# -> %s\n', prefix);
%         obj = trackload([obj.OutputPath prefix '.obj.mat'], {'zones', 'common', 'Interactions'});
%         obj = SocialPredPreyModel2(obj);
%         
%     end
% end

%%
nDays = 4;

all.PostPred = [];
all.PostPrey = [];
all.PrePred = [];
all.PrePrey = [];

all.PostPredPrey = [];

all.Contacts = [];
all.id = [];
all.day = [];

%%
index = 1;
for id=1:length(experiments)
    DJS = [];
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        obj = trackload([obj.OutputPath prefix '.obj.mat'], {'zones', 'common', 'Interactions'});
        obj = SocialAnalysePredPreyModel(obj);
        curr = obj.Interactions.PredPrey;
        
        all.PostPred(:, :, index) = curr.PostPred;
        all.PostPrey(:, :, index) = curr.PostPrey;
        all.PrePred(:, :, index) = curr.PrePred;
        all.PrePrey(:, :, index) = curr.PrePrey;
        all.PostPredPrey(:, :, index) = curr.PostPredPrey;
        all.Contacts(:, :, index) = curr.Contacts;
        all.id(index)  = id;
        all.day(index)  = day;
        index = index + 1;
    end
end
%%
titles = {'PostPred', 'PostPrey', 'PrePred', 'PrePrey'};
for n = 1:length(titles);
    figure(n);
    clf
    curr = E;
    map = all.id * 0;
    for i=1:length(curr.idx)
        map = map |all.id ==  curr.idx(i);
    end
    E.(titles{n}) = all.(titles{n})(:, :, map) ./ all.Contacts(:, :, map);
    % [curr.hPostPred, curr.xPostPred] = hist(curr.pPostPred(:),30);
    % plot(curr.xPostPred, curr.hPostPred);
    % hold on;
    %%
    curr = SC;
    map = all.id * 0;
    for i=1:length(curr.idx)
        map = map |all.id ==  curr.idx(i);
    end
    SC.(titles{n}) = all.(titles{n})(:, :, map) ./ all.Contacts(:, :, map);
    % [curr.hPostPred, curr.xPostPred] = hist(curr.pPostPred(:),30);
    % plot(curr.xPostPred, curr.hPostPred);
    
    %%
    allperms = myPerms(length(experiments),2) - 1;
    c = nan(1, size(allperms, 1));
    for i=1:size(allperms, 1)
        v = ~isnan(upmat);
        v(logical(allperms(i, :)), :, :) = false;
        map = all.id * 0;
        for m=find(v)'
            map = map | all.id ==  m;
        end
        if sum(map) == 0;
            continue;
        end
        q = all.(titles{n})(:, :, map) ./ all.Contacts(:, :, map);
        c(i) = mean(q(~isnan(q)));
    end
    hist(c*100,20)
    vert_line(validmean(E.(titles{n})(:))*100, 'Color', 'b', 'LineStyle', '--');
    vert_line(validmean(SC.(titles{n})(:))*100, 'Color', 'r', 'LineStyle', '--');
    legend('all permutations', 'Enriched', 'Standard Conditions')
    
    xlabel('percentage of contacts');
    ylabel('count');
    
    title(titles{n});
    saveFigure(['Res/SocialPredPreyAnalysisBatch.' titles{n}]);
end

%%
clf
curr = E;
map = all.id * 0;
for i=1:length(curr.idx)
    map = map |all.id ==  curr.idx(i);
end
E.cont = all.Contacts(:, :, map);
for i=1:size(E.cont, 3)
    for j=1:obj.nSubjects
        E.cont(j,j,i) = nan;
    end
end
% [curr.hPostPred, curr.xPostPred] = hist(curr.pPostPred(:),30);
% plot(curr.xPostPred, curr.hPostPred);
% hold on;
%
curr = SC;
map = all.id * 0;
for i=1:length(curr.idx)
    map = map |all.id ==  curr.idx(i);
end
SC.cont = all.Contacts(:, :, map);
for i=1:size(SC.cont, 3)
    for j=1:obj.nSubjects
        SC.cont(j,j,i) = nan;
    end
end

%
allperms = myPerms(length(experiments),2) - 1;
c = nan(1, size(allperms, 1));
for i=1:size(allperms, 1)
    v = ~isnan(upmat);
    v(logical(allperms(i, :)), :, :) = false;
    map = all.id * 0;
    for m=find(v)'
        map = map | all.id ==  m;
    end
    if sum(map) == 0;
        continue;
    end
    q = all.Contacts(:, :, map);
    for a=1:size(q, 3)
        for j=1:obj.nSubjects
            q(j,j,a) = nan;
        end
    end
    c(i) = mean(q(~isnan(q)));
end
hist(c,20)
vert_line(validmean(E.cont(:)), 'Color', 'b', 'LineStyle', '--');
vert_line(validmean(SC.cont(:)), 'Color', 'r', 'LineStyle', '--');
legend('all permutations', 'Enriched', 'Standard Conditions')

xlabel('number of contacts');
ylabel('count');

%%
clf
colors = [0 0 1; 1 0 0];
for n=1:2
    if n==1
        curr = E;
    else
        curr = SC;
    end
    map = all.id * 0;
    sym = {'x', 's', 'o', '^', '*', 'd', 'h'};
    stat.pred = [];
    stat.prey = [];
    for i=1:length(curr.idx)
        c = all.Contacts(:, :, all.id ==  curr.idx(i));
        c(c == 0) = 1;
        pred = all.PostPred(:, :, all.id ==  curr.idx(i)) ./ c;
        prey = all.PostPrey(:, :, all.id ==  curr.idx(i)) ./ c;
        cpred = mean(sum(pred, 2) / (obj.nSubjects - 1), 3);
        cprey = mean(sum(prey, 2) / (obj.nSubjects - 1), 3);
%        plot(cpred, cprey, [sym{i}, ':'], 'Color', colors(n, :));
        [~, order] = sort(cpred);
        
        subplot(2,1,1);
        plot(cpred(order), cprey(order), [sym{i}, '-'], 'Color', colors(n, :));
        hold on;
        stat.pred(i) = var(cpred);
        stat.prey(i) = var(cprey);
    end
    
end
xlabel('Pred');
ylabel('Prey');
a = axis;
a(1) = 0;
a(2) = .4;
axis(a);

%%
colors = [0 0 1; 1 0 0];
for n=1:2
    if n==1
        curr = E;
    else
        curr = SC;
    end
    map = all.id * 0;
    sym = {'x', 's', 'o', '^', '*', 'd', 'h'};
    stat.pred = [];
    stat.prey = [];
    for i=1:length(curr.idx)
        c = all.Contacts(:, :, all.id ==  curr.idx(i));
        c(c == 0) = 1;
        aggr = all.PostPredPrey(:, :, all.id ==  curr.idx(i)) ./ c;
        prey = all.PrePrey(:, :, all.id ==  curr.idx(i)) ./ c;
        caggr = mean(sum(aggr, 2) / (obj.nSubjects - 1), 3);
        cprey = mean(sum(prey, 2) / (obj.nSubjects - 1), 3);
%        plot(cpred, cprey, [sym{i}, ':'], 'Color', colors(n, :));
        [~, order] = sort(caggr);
        
        subplot(2,1,2);
        plot(caggr(order), cprey(order), [sym{i}, '-'], 'Color', colors(n, :));
        hold on;
        stat.pred(i) = var(caggr);
        stat.prey(i) = var(cprey);
    end
    
end
xlabel('aggr');
ylabel('approach');
a = axis;
a(1) = 0;
a(2) = .4;
axis(a);
%% percentage chase escape
clf
colors = [0 0 1; 1 0 0];





for n=1:2
    if n==1
        curr = E;
    else
        curr = SC;
    end
    map = all.id * 0;
    sym = {'x', 's', 'o', '^', '*', 'd', 'h'};
    stat.pred = [];
    stat.prey = [];
    %stat.pChase = 
    for i=1:length(curr.idx)
        c = all.Contacts(:, :, all.id ==  curr.idx(i));
        %c(c == 0) = 1;
        c = max(sum(c, 3), sum(c, 3)');
        c(1:obj.nSubjects+1:end) = 1;
        aggr = sum(all.PostPredPrey(:, :, all.id ==  curr.idx(i)), 3);
%         cchase = mean(sum(aggr ./ c, 2), 3);
%         cescape = mean(sum(aggr ./ c, 1), 3);
        cchase = sum(aggr, 2) ./ (sum(c, 2) - obj.nSubjects);
        cescape = sum(aggr, 1) ./ (sum(c, 1)  - obj.nSubjects);
%        [i n cchase' + cescape]
         %sum(sum(aggr, 3) + sum(aggr, 3)', 2) ./sum(sum(c, 3), 2)
%        plot(cpred, cprey, [sym{i}, ':'], 'Color', colors(n, :));
        [~, order] = sort(cchase);

        ncchase = sum(sum(aggr, 3), 2);
        ncescape = sum(sum(aggr, 3), 1);
        
        subplot(2,1,1);
        plot(ncchase(order), ncescape(order), [sym{i}, '-'], 'Color', colors(n, :));
        hold on;
        
        subplot(2,1,2);
        plot(cchase(order) * 100, cescape(order) * 100, [sym{i}, '-'], 'Color', colors(n, :));
        hold on;
        stat.pred(i) = var(caggr);
        stat.prey(i) = var(cprey);
    end
    
end
xlabel('% chase');
ylabel('% escape');

r = 30;
axis([0 30 0 30])
for r = 5:5:r*2;;
    line([0 r], [r 0], 'LineStyle', ':', 'Color', 'k');
end
%%
clf
colors = [0 0 1; 1 0 0];
for n=1:2
    if n==1
        curr = E;
    else
        curr = SC;
    end
    map = all.id * 0;
    sym = {'x', 's', 'o', '^', '*', 'd'};
    for i=1:length(curr.idx)
        c = all.Contacts(:, :, all.id ==  curr.idx(i));
        c(c == 0) = 1;
        pre = all.PrePred(:, :, all.id ==  curr.idx(i)) ./ c;
        pst = all.PostPred(:, :, all.id ==  curr.idx(i)) ./ c;
        cpre = mean(sum(pre, 2) / (obj.nSubjects - 1), 3);
        cpst = mean(sum(pst, 2) / (obj.nSubjects - 1), 3);
        if i==1
                plot(cpre, cpst, sym{i}, 'Color', colors(n, :), 'Annotation', 'on');
            else
                plot(cpre, cpst, sym{i}, 'Color', colors(n, :), 'Annotation', 'off');
        end
        hold on;
    end
end
xlabel('Pre-Prey');
ylabel('Post-Prey');
