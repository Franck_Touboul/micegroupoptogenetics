function SocialSaveFigure(varargin)
if nargin > 0
    if nargin == 1
        saveFigure(['Graphs/' varargin{1}]);
    elseif nargin == 2
        obj = varargin{1};
        saveFigure(['Graphs/' varargin{2} '.' obj.FilePrefix]);
    end
else
    
    if ~isempty(get(gcf, 'Name'))
        saveFigure(['Graphs/' get(gcf, 'Name')]);
    end
end