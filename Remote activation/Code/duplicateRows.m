function t = duplicateRows(d, N, trans)
if nargin == 2
    trans = 0;
end
if N==1
    t = d;
    return
end
t = [];
[x, y] = size(d);
for i=1:N
    t = [t; d'];
end
if trans > 0
    step = 1/(trans + 1);
    f = step;
    diff = [d(2:end, :) - d(1:end-1, :); zeros(1, y)];
    for i=1:trans
        t = [t; d' + f * diff'];
        f = f + step;
    end
end
t = reshape(t, y, x * (N + trans))';
