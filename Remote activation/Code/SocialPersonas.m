function SocialPersonas(obj)
%%
order = [1 7 4 2 3 5 8 10 6 9];

for s=1:obj.nSubjects
    subplot(obj.nSubjects, 1, s);
    h = histc(obj.zones(s, :), 1:obj.ROI.nZones);
    h = h(order);
    cmap = MyZonesColormap;
    cmap = cmap(order, :);
    PlotPersonas(h, false, cmap);
end

%%
return;

% cmap = [    189 190 200;
%     148 216 239;
%     0 177 229;
%     234 165 193;
%     179 213 157;
%     53 178 87;
%     220 73 89;
%     240 191 148;
%     240 145 55;
%     98 111 179;
%     246 237 170;
%     248 231 83;
%     ]/256;
cmap = MyZonesColormap;
height = 10;
gap = 1;
Gap = 10;
GAP = 20;
width = 250;

SocialExperimentData;
Groups = Groups(1:2);
for g=1:length(Groups)
    I = [];
    subplot(length(Groups), 1, g);
    for id=Groups(g).idx
        prefix = sprintf(experiments{id}, 2);
        fprintf('# -> %s\n', prefix);
        obj = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid'});
        for s=1:obj.nSubjects
            zHist = histc(obj.zones(s, obj.valid), 1:obj.ROI.nZones);
            zHist = zHist / sum(zHist);
            %
            %subplot(obj.nSubjects, 1, s);
            cumProb = cumsum(zHist);
            img = zeros(height, width, 3);
            coord = zeros(1, obj.ROI.nZones);
            pw = width;
            for i=1:length(cumProb)
                w = round(cumProb(end-i+1) * width);
                img(:, 1:w, :) = repmat(reshape(cmap(i, :), [1 1 3]), [height, w, 1]);
                if i>1
                    coord(end-i+2) = .5 * (pw + w);
                end
                pw = w;
            end
            coord(1) = .5 * pw;
            %imagesc(img);
            if s == obj.nSubjects
                %set(gca, 'XTick', coord, 'XTickLabel', obj.ROI.ZoneNames);
                set(gca,'YColor', [1 1 1])
                set(gca,'TickLength', [-0.005 0.1]);
                
                box off;
            else
                axis off;
            end
            if s > 1
                I = [I; ones(gap, width,3); img];
            else
                I = [I; img];
            end
        end
        set(gcf, 'Color', 'w');
        I = [I; ones(Gap, width,3)];
        %clf
        imagesc(I);
        axis off;
        drawnow;
        %return
        %saveFigure([obj.OutputPath obj.FilePrefix '.SocialPersonas']);
    end
    title(GroupsData.Titles{g});
end