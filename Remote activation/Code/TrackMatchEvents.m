function obj = TrackMatchEvents(obj)
newe = [];
curr = struct();
idx = 1;
for s1=1:obj.nSubjects    
    for s2=s1+1:obj.nSubjects
        if s1==s2; continue; end
        e1 = obj.Hierarchy.ChaseEscape.interactions.events{s1, s2};
        e2 = obj.Hierarchy.ChaseEscape.interactions.events{s2, s1};
        e = [e2{:}];
        endf = [e.EndFrame];
        begf = [e.BegFrame];
        %subject = [ones(1, length(e1)) * s1 ones(1, length(e2)) * s2];
        for i=1:length(e1)
            m = ~(e1{i}.BegFrame > endf | e1{i}.EndFrame < begf);
            for j=find(m)
                curr.beg = [e1{i}.BegFrame; begf(j)];
                curr.end = [e1{i}.EndFrame; endf(j)];
                curr.data{1} = e1{i}.data;
                curr.data{2} = e(j).data;
                curr.subjects = [s1 s2]';
                if isempty(newe)
                    newe = curr;
                else
                    newe(idx) = curr;
                end
                idx = idx + 1;
            end
        end
    end
end
[v, o] = sort(min([newe.beg]));
obj.Events = newe(o);