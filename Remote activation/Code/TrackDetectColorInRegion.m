Color.Names = {'Orange', 'Yellow-Orange', 'Yellow', 'Yellow-Green', 'Green', 'Blue-Green', 'Blue', 'Blue-Violet', 'Violet', 'Red-Violet', 'Red', 'Red-Orange'};
Color.Hues = [35, 45, 60, 68, 95, 198, 223, 262, 285, 338, 5, 18];
Color.ValueThresh = .2;
%%
frame = 495519;
img = myMMReader(obj.VideoFile, frame);
%%
subplot(3,3,[1 2 4 5 7 8]);
imagesc(img);
r = imrect;
%%


pos = round(r.getPosition);
local = zeros(pos(4), pos(3), 'uint8');
for i=1:size(img, 3)
    local(1:pos(4), 1:pos(3), i) = img(pos(2):pos(2)+pos(4)-1,pos(1):pos(1)+pos(3)-1,i);
end
hsv = rgb2hsv(local);

subplot(3,3,3);
imagesc(local);

subplot(3,3,6);
imagesc(sqrt(hsv(:, :, 3) .* hsv(:, :, 2)));
colorbar

valid = hsv(:, :, 3) >= Color.ValueThresh;

hue = hsv(:, :, 1); hue = hue(:)';
sat = hsv(:, :, 2); sat = sat(:)';
val = hsv(:, :, 3); val = val(:)';
weight = sqrt(sat);

%meanhue = atan2(mean(sin(hue * 2 * pi)), mean(cos(hue * 2 * pi))) / (2 * pi) * 360;
meanhue = atan2((sin(hue * 2 * pi) * weight')/sum(weight), (cos(hue * 2 * pi) * weight')/sum(weight)) / (2 * pi) * 360;
if meanhue < 0; meanhue = 360 + meanhue; end;
[q, coloridx] = min(abs(Color.Hues - meanhue));
Color.Names{coloridx};

[C, S] = NameMoreColors(local, weight(:) .* valid(:));
C.Hist'
[C.Names{C.Order(1)} '-' C.Names{C.Order(2)} ' (' num2str(C.Count(1)) '-' num2str(C.Count(2)) ')']

%%
h=0:0.01:1;
v=0:0.01:1;
s=0.5;
H = repmat(h, length(v), 1);
S = s * ones(length(v), length(h));
V = repmat(v(:), 1, length(h));

hsv = cat(3, cat(3, H, S), V);
imagesc(hsv2rgb(hsv))
xlabel('H');
ylabel('V');

%%
subplot(3,3,6);
cmap = MyCategoricalColormap;
for i=1 %:size(img, 3)
    c = hsv(:, :, i);
    x = sequence(0, 1, 256);
    h = histc(c(:), x);
    plot(x, h, 'color', cmap(i, :));
    hon
end
legend({'hue' 'sat' 'val'});
legend('boxoff');
hoff

subplot(3,3,9);
cmap = [];
for i=1:360
    cmap(i, :) = [i/360 1 1];
end
cmaphsv = hsv2rgb(reshape(cmap, [1, 360, 3]));
imagesc(cmaphsv);
axis off;

