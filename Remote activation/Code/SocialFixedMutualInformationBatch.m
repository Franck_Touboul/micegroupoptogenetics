SocialExperimentData;
clear Groups;
Groups(1) = GroupsData.Standard;
experiments = {experiments{Groups(1).idx}};
Groups(1).idx = 1:length(Groups(1).idx);
Groups(1).map = true(1, length(Groups(1).idx));
%%
clf;
index = 1;
for id=1:length(experiments)
    for day = 2
        prefix = sprintf(experiments{id}, day);
        obj = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid'});
        SocialFixedMutualInformation(obj);
        fprintf('# -> %s\n', prefix);
    end
    index = index + 1;
    if index > 3
        break;
    end
end