cEnt = zeros(obj.nSubjects);
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1 == m2; continue; end;
        cEnt(m1, m2) = ConditionalEntropy(obj.zones([m1 m2], :));
    end
end
%%
mat = cEnt - cEnt';
mat = mat .* (mat > 0);
[rank, removed] = TopoFeedbackArcSetOrder(mat);
[~, bg] = SocialGraph(obj, mat, removed, rank);
g = biograph.bggui(bg);
