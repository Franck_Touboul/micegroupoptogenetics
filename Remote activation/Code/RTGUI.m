function varargout = RTGUI(varargin)
% RTGUI MATLAB code for RTGUI.fig
%      RTGUI, by itself, creates a new RTGUI or raises the existing
%      singleton*.
%
%      H = RTGUI returns the handle to a new RTGUI or the handle to
%      the existing singleton*.
%
%      RTGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RTGUI.M with the given input arguments.
%
%      RTGUI('Property','Value',...) creates a new RTGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before RTGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to RTGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help RTGUI

% Last Modified by GUIDE v2.5 15-Jun-2014 11:09:16

% Begin initializagtion code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RTGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @RTGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function Message(varargin)
global RTGuiData;
msg = sprintf(varargin{:});
t = get(RTGuiData.handles.MainBox, 'String');
t{end+1}=['# ' msg];
set(RTGuiData.handles.MainBox, 'String', t)
ScrollDown(RTGuiData.handles.MainBox);

function Alert(varargin)
global RTGuiData;
msg = sprintf(varargin{:});
set(RTGuiData.handles.MessagesBox, 'String', msg)
fprintf(RTGuiData.LogFile, '%s %s\n', datestr(now), msg);

% --- Executes just before RTGUI is made visible.
function RTGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RTGUI (see VARARGIN)

% Choose default command line output for RTGUI
handles.output = hObject;

global RTGuiData;
RTGuiData.handles = handles;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes RTGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%%
axes(handles.PreviewAxes);
axis off;
axes(handles.ColorAxes);
axis off;

%%

set(handles.MinAreaSlider, 'Max', 20);
set(handles.MinAreaSlider, 'Value', RTGuiData.Opt.MinArea);
set(handles.MinAreaSlider, 'SliderStep', ones(1,2) * 1/20);

MinAreaSlider_Callback(handles.MinAreaSlider, eventdata, handles);

ThresholdSlider_Callback(handles.ThresholdSlider, eventdata, handles);

%%
s = [];
Message('Starting ni device... ');
try
    predevices = cell(1,2); % analog digital
    predevices{2} = 'Dev5';
    if false
        %%
        Message('- using legacy interface (32bit)');
        %%
        out = daqhwinfo('nidaq');
        d = out.InstalledBoardIds;
        names = out.BoardNames;
        if isempty(d)
            Message('- no devices found');
        else
            %%
            s = [];
            Message('- devices:');
            dev = '';
            for i=1:length(d)
                Message(['  . ' names{i} '(' d{i} ')']);
                if i==1
                    dev = d{i};
                end
            end
            Message('- creating session')
            s.dio = digitalio('nidaq', dev);
            Message('- adding digital channel');
            s.lines = addline(s.dio,0:3,'out');
            set(handles.MessagesBox, 'String', 'NI device OK')
            Message('- [DONE]');
        end
        RTGuiData.DAQ32bit = true;
    else
        Message('- using session based interface (64bit)');
        d = daq.getDevices;
        if length(d) == 0
            Message('- no devices found');
        else
            Message('- devices:');
            dev = '';
            doneAnalog = false;
            doneDigital = false;
            for i=1:length(d)
                Message(['  . ' d(i).Description '(' d(i).ID ')']);
                s = d(i).Subsystems;
                isdigital = false;
                isanalog = false;
                for j=1:length(s)
                    if ~isempty(regexpi( s(1).SubsystemType, 'digital'))
                        isdigital = true;
                    end
                    if ~isempty(regexpi( s(1).SubsystemType, 'analog'))
                        isanalog = true;
                    end
                end
                dev = d(i).ID;
                if (isanalog && ~doneAnalog && ~any(strcmpi(dev, predevices))) || (~isempty(predevices{1}) && strcmpi(predevices{1}, dev))
                    Message('- creating session')
                    s = daq.createSession('ni');
                    Message('- adding analog channel');
                    try
                        s.addAnalogInputChannel(dev, [1], 'Voltage');
                        addlistener(s,'DataAvailable', @LightDetect);
                        s.IsContinuous = true;
                        Message('- [DONE]');
                        startBackground(s);
                        doneAnalog = true;
                        RTGuiData.aDAQ = s;
                    catch
                        Message('- [FAILED]');
                    end
                elseif (isdigital && ~doneDigital && ~any(strcmpi(dev, predevices))) || (~isempty(predevices{2}) && strcmpi(predevices{2}, dev))
                    Message('- creating session')
                    s = daq.createSession('ni');
                    Message('- adding digital channel');
                    try
                        s.addDigitalChannel(dev, 'port0/line0:3', 'OutputOnly');
                        set(handles.MessagesBox, 'String', 'NI device OK')
                        Message('- [DONE]');
                        doneDigital = true;
                        RTGuiData.DAQ = s;
                    catch
                        Message('- [FAILED]');
                    end
                end
            end
        end
        RTGuiData.DAQ32bit = false;
    end
    
catch err
    Message(['- [FAILED]: ' err.message]);
    set(handles.MessagesBox, 'String', 'no NI device')
end
RTGuiData.DAQLastEvent = -inf;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function LightDetect(src, event)
global RTGuiData;
RTGuiData.LightDetecterLevel = sum(event.Data(1, :));
%RTGuiData.LightDetecterLevel
if event.TimeStamps(1) - RTGuiData.DAQLastEvent > .2
    RTGuiData.DAQLastEvent = event.TimeStamps(1);
    %event.TimeStamps(1)
    set(RTGuiData.handles.LightEdit, 'String', sprintf('%.1f', RTGuiData.LightDetecterLevel));
end
nowv = datevec(now);
%         RTGuiData.LightDetecterHigh < RTGuiData.LightDetecterLow && ...
%         RTGuiData.LightDetecterLevel < RTGuiData.LightDetecterHigh + abs(RTGuiData.LightDetecterHigh - RTGuiData.LightDetecterLow) * .3
if nowv(4) >= RTGuiData.DarkStart && nowv(4) <= RTGuiData.DarkEnd && ...
        RTGuiData.LightDetecterLow < RTGuiData.LightDetecterHigh && ...
        RTGuiData.LightDetecterLevel > RTGuiData.LightDetecterLow + abs(RTGuiData.LightDetecterHigh - RTGuiData.LightDetecterLow) * .7
    if ~RTGuiData.IsLight
        set(RTGuiData.handles.LightRadio, 'Value', 1);
        RTGuiData.IsLight = true;
    end
else
    if RTGuiData.IsLight
        set(RTGuiData.handles.LightRadio, 'Value', 0);
        RTGuiData.IsLight = false;
    end
end
t = mod(event.TimeStamps, 1);

% axes(RTGuiData.handles.LightAxes);
% if t(1) < 0.05;
%     clf
% end
% plot(t, event.Data)
% axis([0, 1, -3, 3]);
% hon

function CloseDevice()
global RTGuiData;
delete(RTGuiData.Timer);
if isfield(RTGuiData.CurrDevice, 'obj') && isvalid(RTGuiData.CurrDevice.obj)
    delete(RTGuiData.CurrDevice.obj);
end

function [img, id] = AnalyzeFrame(img)
global RTGuiData;
id = 0;
if ~isempty(RTGuiData.ROI)
    %%
    thresh = get(RTGuiData.handles.ThresholdSlider, 'Value');
    x = round(RTGuiData.ROI(2)); wx = round(RTGuiData.ROI(4));
    y = round(RTGuiData.ROI(1)); wy = round(RTGuiData.ROI(3));
    patch = img(x:x+wx, y:y+wy, :);
    hsv = rgb2hsv(patch);
    h = hsv(:, :, 1);
    s = hsv(:, :, 2);
    v = hsv(:, :, 3);
    valid = v >= thresh;
    valid = imerode(valid, strel('disk', RTGuiData.Opt.MinArea));
    if ~isempty(RTGuiData.Colors)
        %%
        ih = round(h(valid) / (1 / (length(RTGuiData.Colors.Bins)-1))) + 1;
        is = round(s(valid) / (1 / (length(RTGuiData.Colors.Bins)-1))) + 1;
        iv = round(v(valid) / (1 / (length(RTGuiData.Colors.Bins)-1))) + 1;
        if length(ih) < RTGuiData.Opt.MinNumOfPixels
            id = 0;
        else
            %     s = zeros(1, size(Colors.Histrogram.H, 1));
            %     for i=1:size(Colors.Histrogram.H, 1)
            %         s(i) = mean(Colors.Histrogram.H(i, ih));
            %     end
            
            nSubj = size(RTGuiData.Colors.Histrogram.H, 1);
            s = zeros(size(RTGuiData.Colors.Histrogram.H, 1), length(ih));
            for i=1:nSubj
                s(i, :) = RTGuiData.Colors.Histrogram.H(i, ih) .* RTGuiData.Colors.Histrogram.S(i, is);
            end
            [~, imxs] = max(s);
            h = histc(imxs, 1:nSubj);
            [mxh, imxh] = max(h);
            h(imxh) = 0;
            mxh2 = max(h);
            if mxh2 > mxh * RTGuiData.Opt.Coherence
                id = 0;
            else
                id = imxh;
            end
            
        end
    end
    g = img(:, :, 2);
    g(x:x+wx, y:y+wy, :) = 1 * valid;
    img(:, :, 2) = g;

    img(x:x+wx, y, :) = 1;
    img(x:x+wx, y+1, :) = 1;
    img(x:x+wx, y+wy, :) = 1;
    img(x:x+wx, y+wy-1, :) = 1;
    img(x, y:y+wy, :) = 1;
    img(x+1, y:y+wy, :) = 1;
    img(x+wx, y:y+wy, :) = 1;
    img(x+wx-1, y:y+wy, :) = 1;
end



function ProcessVideoFrame(timerobj, event)
global RTGuiData;
try
    img = RTGuiData.CurrDevice.obj.step();
    [img, id] = AnalyzeFrame(img);
    HandleMouse(id);
    set(RTGuiData.Image, 'CData', img);
catch
end

function StartCapture()
global RTGuiData;
Message('Starting recording device...');
CloseDevice();
RTGuiData.CurrDevice = RTGuiData.Devices(get(RTGuiData.handles.DeviceListBox, 'Value'));
if RTGuiData.CurrDevice.ID < 0
    [FileName, PathName] = uigetfile('*.avi');
    RTGuiData.Filename = [PathName FileName];
    Message('Loading from file ''%s''...', RTGuiData.Filename);
    obj = vision.VideoFileReader(RTGuiData.Filename);
    Message('Done loading from file ''%s''', RTGuiData.Filename);
    RTGuiData.CurrDevice.obj = obj;
    axes(RTGuiData.handles.PreviewAxes);
    RTGuiData.Image = image( obj.step );
    axis off;
    RTGuiData.Timer = timer('TimerFcn', @ProcessVideoFrame, 'Period', 1/obj.info.VideoFrameRate);
    RTGuiData.Timer.ExecutionMode = 'fixedRate';
    start(RTGuiData.Timer);
else
    obj = imaq.VideoDevice(RTGuiData.CurrDevice.Adaptor, RTGuiData.CurrDevice.ID);
    RTGuiData.CurrDevice.obj = obj;
    axes(RTGuiData.handles.PreviewAxes);
    RTGuiData.Image = image( obj.step );
    axis off;
    RTGuiData.Timer = timer('TimerFcn', @ProcessVideoFrame, 'Period', 0.1);
    RTGuiData.Timer.ExecutionMode = 'fixedRate';
    start(RTGuiData.Timer);
end

% --- Outputs from this function are returned to the command line.
function varargout = RTGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in DeviceListBox.
function DeviceListBox_Callback(hObject, eventdata, handles)
% hObject    handle to DeviceListBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
StartCapture();


% --- Executes during object creation, after setting all properties.
function DeviceListBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DeviceListBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
global RTGuiData;
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'String', {});
DeviceNames = {};
idx = 1;
adaptors = imaqhwinfo;
for i=1:length(adaptors.InstalledAdaptors)
    devices = imaqhwinfo(adaptors.InstalledAdaptors{i});
    for j=1:length(devices.DeviceInfo)
        RTGuiData.Devices(idx).Name = devices.DeviceInfo(j).DeviceName;
        RTGuiData.Devices(idx).Adaptor = adaptors.InstalledAdaptors{i};
        RTGuiData.Devices(idx).ID = devices.DeviceInfo(j).DeviceID;
        DeviceNames{idx} = devices.DeviceInfo(j).DeviceName;
        idx = idx + 1;
    end
end
RTGuiData.Devices(idx).Name = 'From file...';
RTGuiData.Devices(idx).Adaptor = [];
RTGuiData.Devices(idx).ID = -1;
DeviceNames{idx} = RTGuiData.Devices(idx).Name;

set(hObject, 'String', DeviceNames);
guidata(hObject, handles);


% --- Executes on button press in ROIButton.
function ROIButton_Callback(hObject, eventdata, handles)
% hObject    handle to ROIButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global RTGuiData;

roi = imrect;
handles.ROI.Mask = roi.createMask;
p = regionprops(handles.ROI.Mask, 'BoundingBox');
RTGuiData.ROI = p.BoundingBox;
delete(roi)

% --- Executes on button press in SetColorsButton.
function SetColorsButton_Callback(hObject, eventdata, handles)
% hObject    handle to SetColorsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global RTGuiData;

[FileName, PathName] = uigetfile('*.obj.mat');
f = regexprep([PathName, FileName], '\\', '\/');
obj = TrackLoad(f, {'Colors'});
RTGuiData.Colors = obj.Colors;
axes(handles.ColorAxes);
imagesc(reshape(obj.Colors.Centers, [1, size(obj.Colors.Centers, 1), 3]));
axis off;
axes(handles.PreviewAxes);

% --- Executes on button press in SaveVideoFileButton.
function SaveVideoFileButton_Callback(hObject, eventdata, handles)
% hObject    handle to SaveVideoFileButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function ThresholdSlider_Callback(hObject, eventdata, handles)
% hObject    handle to ThresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global RTGuiData;
set(RTGuiData.handles.ThresholdShow, 'String', num2str(get(RTGuiData.handles.ThresholdSlider, 'Value')));


% --- Executes during object creation, after setting all properties.
function ThresholdSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ThresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function MinAreaSlider_Callback(hObject, eventdata, handles)
% hObject    handle to MinAreaSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global RTGuiData;
RTGuiData.Opt.MinArea = get(handles.MinAreaSlider, 'Value');
set(RTGuiData.handles.MinAreaShow, 'String', num2str(RTGuiData.Opt.MinArea));

% --- Executes during object creation, after setting all properties.
function MinAreaSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MinAreaSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function MainBox_Callback(hObject, eventdata, handles)
% hObject    handle to MainBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MainBox as text
%        str2double(get(hObject,'String')) returns contents of MainBox as a double


% --- Executes during object creation, after setting all properties.
function MainBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MainBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function MessagesBox_Callback(hObject, eventdata, handles)
% hObject    handle to MessagesBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MessagesBox as text
%        str2double(get(hObject,'String')) returns contents of MessagesBox as a double


% --- Executes during object creation, after setting all properties.
function MessagesBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MessagesBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global RTGuiData;
try
    stop(RTGuiData.Timer);
    delete(RTGuiData.Timer);
catch
end

try
    delete(RTGuiData.CurrDevice.obj);
catch
end
try
    if RTGuiData.DAQ32bit
        delete(RTGuiData.DAQ.dio);
    else
        try
            RTGuiData.DAQ.stop;
        catch
        end
        delete(RTGuiData.DAQ);
        try
            RTGuiData.aDAQ.stop;
        catch
        end
        delete(RTGuiData.aDAQ);
    end
catch
end
delete(hObject);

function WindowTime_Callback(hObject, eventdata, handles)
% hObject    handle to WindowTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of WindowTime as text
%        str2double(get(hObject,'String')) returns contents of WindowTime as a double


% --- Executes during object creation, after setting all properties.
function WindowTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WindowTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in ResetTimerButton.
function ResetTimerButton_Callback(hObject, eventdata, handles)
% hObject    handle to ResetTimerButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global RTGuiData;
RTGuiData.Activation = zeros(1, RTGuiData.nMice);
RTGuiData.WindowStartTime = clock;
data = get(RTGuiData.handles.MouseTable, 'Data');
for i=1:RTGuiData.nMice
    data{i, 1} = 0;
end
set(RTGuiData.handles.MouseTable, 'Data', data);



function ThresholdShow_Callback(hObject, eventdata, handles)
% hObject    handle to ThresholdShow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ThresholdShow as text
%        str2double(get(hObject,'String')) returns contents of ThresholdShow as a double


% --- Executes during object creation, after setting all properties.
function ThresholdShow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ThresholdShow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function MinAreaShow_Callback(hObject, eventdata, handles)
% hObject    handle to MinAreaShow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MinAreaShow as text
%        str2double(get(hObject,'String')) returns contents of MinAreaShow as a double


% --- Executes during object creation, after setting all properties.
function MinAreaShow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MinAreaShow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in LightButton.
function LightButton_Callback(hObject, eventdata, handles)
% hObject    handle to LightButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global RTGuiData;
RTGuiData.LightDetecterHigh = RTGuiData.LightDetecterLevel;


% --- Executes on button press in DarkButton.
function DarkButton_Callback(hObject, eventdata, handles)
% hObject    handle to DarkButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global RTGuiData;
RTGuiData.LightDetecterLow = RTGuiData.LightDetecterLevel;


% --- Executes on button press in LightRadio.
function LightRadio_Callback(hObject, eventdata, handles)
% hObject    handle to LightRadio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of LightRadio



function LightEdit_Callback(hObject, eventdata, handles)
% hObject    handle to LightEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LightEdit as text
%        str2double(get(hObject,'String')) returns contents of LightEdit as a double


% --- Executes during object creation, after setting all properties.
function LightEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LightEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function HandleMouse(id)
global RTGuiData;

z = false(1, 4);
if etime(clock, RTGuiData.WindowStartTime) > RTGuiData.TimeBetweenActivations
    RTGuiData.Activation = zeros(1, RTGuiData.nMice);
    RTGuiData.WindowStartTime = clock;
    data = get(RTGuiData.handles.MouseTable, 'Data');
    for i=1:RTGuiData.nMice
        data{i, 1} = 0;
    end
    set(RTGuiData.handles.MouseTable, 'Data', data);
end
set(RTGuiData.handles.WindowTime, 'string', sprintf('%.1f/%.0f', etime(clock, RTGuiData.WindowStartTime), RTGuiData.TimeBetweenActivations));

if ~isempty(RTGuiData.Event) && ~isempty(RTGuiData.Event.Activation) && (RTGuiData.Event.ID ~= id  || ~RTGuiData.IsLight)
    Alert('Done #%d for %f sec', RTGuiData.Event.ID, toc(RTGuiData.Event.Activation));
    RTGuiData.Activation(RTGuiData.Event.ID) = RTGuiData.Activation(RTGuiData.Event.ID) + toc(RTGuiData.Event.Activation);
    RTGuiData.Event.Activation = [];
    UpdateTable
end

RTGuiData.Opt.MaxTimeGap = .2;
ingap = false;
if ~isempty(RTGuiData.Event) && RTGuiData.Event.ID == 0 && toc(RTGuiData.Event.Stamp) < RTGuiData.Opt.MaxTimeGap
    disp 'gap'
    ingap = true;
end

if (isempty(RTGuiData.Event) || RTGuiData.Event.ID ~= id) && ~ingap
    if id > 0
        RTGuiData.Event.ID = id;
        RTGuiData.Event.Start = tic;
        RTGuiData.Event.Activation = [];
        RTGuiData.Event.Stamp = tic;
    else
        RTGuiData.Event = [];
    end
else
    if ~ingap
        RTGuiData.Event.Stamp = tic;
    end
    if ...
            (isempty(RTGuiData.Event.Activation) && RTGuiData.Activation(id) < RTGuiData.ActivationDuration) || ...
            (~isempty(RTGuiData.Event.Activation) && RTGuiData.Activation(id) + toc(RTGuiData.Event.Activation) < RTGuiData.ActivationDuration)
        dt = toc(RTGuiData.Event.Start);
        if dt < RTGuiData.TimeToStart
            Alert('Prepare #%d (%.1f/%.0f)', id, dt, RTGuiData.TimeToStart);
        else
            if isempty(RTGuiData.Event.Activation)
                if RTGuiData.IsLight
                    Alert('Active #%d', id);
                    RTGuiData.Event.Activation = tic;
                else
                    Alert('Open #%d', id);
                end
            else
            end
            z(id) = true;
        end
    else
        if ~isempty(RTGuiData.Event.Activation)
            RTGuiData.Activation(id) = RTGuiData.Activation(id) + toc(RTGuiData.Event.Activation)
            Alert('Done #%d after %f sec', id, toc(RTGuiData.Event.Activation));
            RTGuiData.Event.Activation = [];
            UpdateTable;
        else
            Alert('Done #%d', id);
        end
    end
end
try
    if ~RTGuiData.Warnings.DAQ
        if RTGuiData.IsOn
            DAQPut(z);
        end
    end
catch me
    if ~RTGuiData.Warnings.DAQ
        fprintf('failed setting daq output: %s\n', me.message);
        Message('failed setting daq output: %s\n', me.message);
        RTGuiData.Warnings.DAQ = true;
    end
end

function UpdateTable
global RTGuiData;
data = get(RTGuiData.handles.MouseTable, 'Data');
for i=1:RTGuiData.nMice
    data{i, 1} = round(RTGuiData.Activation(i) * 10) / 10;
end
set(RTGuiData.handles.MouseTable, 'Data', data);

function DAQPut(z)
global RTGuiData;
if RTGuiData.DAQ32bit
    putvalue(RTGuiData.DAQ.dio, z);
else
    RTGuiData.DAQ.outputSingleScan(z);
end

% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
global RTGuiData;

RTGuiData = struct();

RTGuiData.Devices = struct();
RTGuiData.CurrDevice = [];
RTGuiData.IDDuration = 0;
RTGuiData.ROI = [];
RTGuiData.Colors = [];
RTGuiData.Filename = '';
RTGuiData.OutputFile = [];
RTGuiData.Output = [];
RTGuiData.Timer = [];
RTGuiData.Handles = handles;
RTGuiData.Event = [];
RTGuiData.Activation = [];
RTGuiData.Warnings.DAQ = false;
RTGuiData.WindowStartTime = clock;

%%
RTGuiData.Opt.Coherence = 0.4;
RTGuiData.Opt.MinNumOfPixels = 250;
RTGuiData.Opt.MinArea = 10;
RTGuiData.Opt.MaxTimeGap = .1;

%%
%RTGuiData.Opt.DetectorGradientWindow = 10;
%RTGuiData.Opt.DetectorThresh = -.1;
RTGuiData.LogFile = fopen('D:/opto/log', 'a+');
RTGuiData.IsOn = true;

RTGuiData.LightDetecterLevel = inf;
RTGuiData.LightDetecterHigh = -.8;
RTGuiData.LightDetecterLow = 1.8;
RTGuiData.IsLight = false;
RTGuiData.DarkStart = 10;
RTGuiData.DarkEnd = 22;
%%
RTGuiData.TimeBetweenActivations = 60*60;
RTGuiData.TimeToStart = .1;
RTGuiData.ActivationDuration = 2;
RTGuiData.nMice = 4;
%%
RTGuiData.LastEventTime = cell(1, RTGuiData.nMice); for i=1:RTGuiData.nMice; RTGuiData.LastEventTime{i} = -1; end
RTGuiData.Activation = zeros(1, RTGuiData.nMice);
RTGuiData.IsOpen = false(1, RTGuiData.nMice);


% --- Executes on button press in ManualButton.
function ManualButton_Callback(hObject, eventdata, handles)
% hObject    handle to ManualButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global RTGuiData;
z = ones(1, 4);
DAQPut(z);
t = timer('TimerFcn', @StopActivation, 'StartDelay', 10);
start(t);
set(RTGuiData.handles.ManualButton, 'String', 'Wait...')

function StopActivation(a, b)
global RTGuiData;
z = zeros(1, 4);
DAQPut(z);
set(RTGuiData.handles.ManualButton, 'String', 'Activate!')


% --- Executes on button press in OffButton.
function OffButton_Callback(hObject, eventdata, handles)
% hObject    handle to OffButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global RTGuiData;
z = zeros(1, 4);
DAQPut(z);
if RTGuiData.IsOn
set(RTGuiData.handles.OffButton, 'string', 'On');
DAQPut(false(1,4));
RTGuiData.IsOn = false;
else
set(RTGuiData.handles.OffButton, 'string', 'Off');
RTGuiData.IsOn = true;
end

% --- Executes on button press in StartBtn.
function StartBtn_Callback(hObject, eventdata, handles)
% hObject    handle to StartBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global RTGuiData;
if strcmp(get(RTGuiData.Handles.StartBtn, 'String'), 'Start!')
    set(RTGuiData.Handles.StartBtn, 'String', 'Finish');
elseif strcmp(get(RTGuiData.Handles.StartBtn, 'String'), 'Finish')
    set(RTGuiData.Handles.StartBtn, 'String', 'Start');
end
