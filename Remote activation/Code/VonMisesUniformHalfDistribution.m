function p = VonMisesUniformHalfDistribution(x, m, v, base)
if nargin < 4
    base = 1/pi;
end
k = 1 ./ v;
b = besseli(0, k);
p1 = exp(k .* cos(x - m)) ./ ( 2 * pi * b);
p2 = exp(k .* cos(-x - m)) ./ ( 2 * pi * b);
p = p1 + p2;
p = (base + p) / (1 + pi * base);
