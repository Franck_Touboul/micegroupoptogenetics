function TrackPreprocessVideoFile(obj)
obj = TrackLoad(obj);
obj = TrackFindDarkPeriod(obj, .2);
try
    obj = TrackComputeNoiseStatistics(obj);
catch
end

%% Output to file
if obj.OutputToFile
    TrackSave(obj);
    if obj.OutputInOldFormat
        meta = struct();
        try
            meta.bkgFrame = obj.BkgImage;
        end
        try
            meta.subject.centerColors = obj.Colors.Centers;
            meta.subject.colorBins = obj.Colors.Bins;
            
            meta.subject.r = obj.Colors.Histrogram.R;
            meta.subject.g = obj.Colors.Histrogram.G;
            meta.subject.b = obj.Colors.Histrogram.B;
            
            meta.subject.h = obj.Colors.Histrogram.H;
            meta.subject.s = obj.Colors.Histrogram.S;
            meta.subject.v = obj.Colors.Histrogram.V;
            
            meta.subject.filler.r = obj.Colors.FillerHistogram.R;
            meta.subject.filler.g = obj.Colors.FillerHistogram.G;
            meta.subject.filler.b = obj.Colors.FillerHistogram.B;
            
            meta.subject.filler.h = obj.Colors.FillerHistogram.H;
            meta.subject.filler.s = obj.Colors.FillerHistogram.S;
            meta.subject.filler.v = obj.Colors.FillerHistogram.V;
        end
        meta.options = obj;
        filename = [obj.OutputPath obj.FilePrefix '.meta.mat'];
        try
            fprintf('# - saving in old meta file format to %s\n', filename);
            save(filename, 'meta');
        catch me
            fprintf('#  . save failed: %s\n', me.message);
        end
    end
end
