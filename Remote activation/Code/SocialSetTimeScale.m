function obj = SocialSetTimeScale(obj, fs)
fprintf('# - setting time-scale to %d\n', fs);
obj = TrackLoad(obj);
%%
zones = obj.zones(:, obj.valid);
u = 1:obj.ROI.nZones;
zones = [zones, ones(obj.nSubjects, fs - mod(size(zones, 2), fs)) * (min(u) - 1)];
newzones = [];
for s=1:obj.nSubjects
    %%
    z = zones(s, :);
    z = reshape(z, fs, length(z)/fs);
    count = zeros(length(u), size(z, 2));
    idx = 1;
    for i=u(:)'
        count(idx, :) = sum(z == i);
        idx = idx + 1;
    end
    [q, i] = max(count);
    if s==1
        newzones = zeros(obj.nSubjects, length(i));
    end
    newzones(s, :) = u(i);
end
obj = SocialSetZones(obj, newzones);
%%
return;
if fs > 2
    obj.zones = blkproc(obj.zones(:, obj.valid), [1, fs], @Majority);
    obj.valid = true(1, size(obj.zones, 2));
    obj.nFrames = size(obj.zones, 2);
elseif fs == 2
    obj.zones = obj.zones(:, 1:2:obj.nFrames);
    obj.valid = obj.valid(1:2:obj.nFrames);
    obj.nFrames = size(obj.zones, 2);    
end
%%