function me = METrain(me, data, varargin)
options.nIters = 5000;
options.Algorithm = 'GIS';
%options.Algorithm = 'NestrovGD';
options.MinNumberOfIters = 0;
options.KLConvergenceRatio = 1e-7;
options.Confidence = .05;
% - NestrovGD
%   - Line search
options.MaxLineSearchSteps = 50;
options.UseLineSearch = true;
options.Tk = 1;
options.Beta = .5;
options.Alpha = .5;
% - GIS
options.MinExpectation = .1 / size(data, 1);
options.MaxLog = 10;
%
options.Output = true;
%%
options = setDefaultParameters(varargin, options);
output = options.Output;
%%
base = me.nVals;
baseVector = base.^(me.Dim-1:-1:0);
pEmp = histc(baseVector * (data'-1)+1, 1:base ^ size(data, 2));
pEmp = pEmp / sum(pEmp);
%%
me.Dkl = inf;
me.nConverged = 0;
me.KLconverged = false;
me.LSconverged = false;
me.Converged = false;
me.Options = options;
if isfield(options, 'InitWeights')
    me.Weights = options.InitWeights;
else
    me.Weights = randn(1, me.nPatterns) / me.nPatterns;
end
me.Momentum = me.Weights;
me.C = max(sum(me.Features, 2));
[me.Constraints, me.PCI] = binofit(me.PatternHist, me.nData, options.Confidence);
%%LSconverged
tic;
ticTime = toc;
%% initial halfKL
p = exp(me.Features * me.Weights');
Z = sum(p);
p = p / Z;
kl2 = -pEmp * log2(p);
kl1 = pEmp * log2(pEmp' + (pEmp' == 0));
%%
for iter=1:options.nIters
    %%
    currTime = toc;
    if output
        fprintf('# - %s, iter (%4d/%4d) [x%d ps] : ', options.Algorithm, iter, options.nIters, round(1 / (currTime - ticTime)));
    end
    ticTime = currTime;
    %% compute the pdf for each sample
    if isempty(p)
        p = exp(me.Features * me.Weights');
        Z = sum(p);
        p = p / Z;
    end
    prev_Dkl = kl1 + kl2;
    kl2 = -pEmp * log2(p + (pEmp' == 0));
    me.Dkl = kl1 + kl2;
    E = p' * me.Features;
    p = [];
    %%
    if options.Confidence ~= 0
        nconverged = sum(E'  > me.PCI(:, 1) & E' < me.PCI(:, 2));
        if output
            fprintf('KL = %6.4f, convergence = %5.2f%%, KL-convergence = %6.4e', me.Dkl, (nconverged / length(me.Momentum) * 100), abs(me.Dkl - prev_Dkl));
        end
        me.nConverged = nconverged;
        if nconverged == length(me.Momentum) && iter >= options.MinNumberOfIters
            %%
            if output && ~me.Converged;
                fprintf('\n # converged to confidence interval (alpha = %3.2f)\n', options.Confidence);
            end
            me.Converged = true;
            %%
            if options.KLConvergenceRatio > 0
                if abs(me.Dkl - prev_Dkl) <= options.KLConvergenceRatio
                    if me.KLconverged
                        if output;
                            fprintf('\n# converged to KL convergence ratio (ratio = %3.2f)\n', options.KLConvergenceRatio);
                        end
                        break;
                    else
                        me.KLconverged = true;
                    end
                elseif me.KLconverged
                    me.KLconverged = false;
                end
            else
                break;
            end
        end
    else
        if output; fprintf('KL = %6.4f (%.3f)', me.Dkl, abs((me.Dkl - prev_Dkl)/prev_Dkl)); end
    end
    %%
    if strcmp(options.Algorithm, 'NestrovGD')
        %% Line search
        t = options.Tk;
        if options.UseLineSearch
            df = (me.Constraints - E);
            found = false;
            nsteps = 1;
            while ~found
                LSMomentum = me.Weights + t * df;
                LSWeights = LSMomentum + (iter - 1)/(iter+2) * (LSMomentum - me.Momentum);
                p = exp(me.Features * LSWeights');
                p = p / sum(p);
                found = -pEmp * log2(p) < kl2 - options.Alpha * t * sum(df.^2);
                if ~found
                    t = options.Beta * t;
                end
                if nsteps >= options.MaxLineSearchSteps
                    break;
                end
                nsteps = nsteps + 1;
            end
        end
        if 1==2 && nsteps >= options.MaxLineSearchSteps
            if output;
                fprintf('\n# converged after reaching maximal number of line searches (limit = %d)\n', options.MaxLineSearchSteps);
            end
            me.LSconverged = true;
            break;
        end
        if output; fprintf(', Tk = %.2f', t); end;
        prevMomentum = me.Momentum;
        me.Momentum = me.Weights + t * (me.Constraints - E);
        me.Weights = me.Momentum + (iter - 1)/(iter+2) * (me.Momentum - prevMomentum);
    elseif strcmp(options.Algorithm, 'GIS')
        dw = log(me.Constraints ./ E);
        dw(me.Constraints == 0 & E < options.MinExpectation) = 0;
        dw = MyTrim(dw, [-options.MaxLog options.MaxLog]);
        me.Weights = me.Weights + 1/me.C * dw;
    end
    %%
    if output; fprintf('\n'); end
end
%% finalize model
if isempty(p)
    p = exp(me.Features * me.Weights');
    Z = sum(p);
    p = p / Z;
end
me.Probs = p(:);
me.Dkl = kl1 + -pEmp * log2(p + (pEmp' == 0));
me.nIters = iter;
