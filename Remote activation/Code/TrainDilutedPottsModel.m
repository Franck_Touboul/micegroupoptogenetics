function me = TrainDilutedPottsModel(obj, orders, zones, minOcc)

Options.nIters = 1;

if exist('zones', 'var')
    data = zones - 1;
else
    data = zeros(obj.nSubjects, sum(obj.valid));
    for s=1:obj.nSubjects
        currZones = obj.zones(s, :);
        data(s, :) = currZones(obj.valid == 1) - 1;
    end
end

me = cell(1, max(orders));
fprintf('# - training Potts model:\n');
for level=orders
    fprintf('#      . level %d\n', level);
    local = obj;
    local.initialPottsWeights = [];
    local.n = level;
    %local.output = true;
    local.count = obj.ROI.nZones;
    for iter = 1:Options.nIters
        local.nIters = obj.Analysis.Potts.nIters(1);
        local.MinOccurance = minOcc;
        if local.nIters > 0
            local.MinNumberOfIters = obj.Analysis.Potts.MinNumberOfIters(1);
            me{level} = trainPottsModelUsingGIS(local, data, obj.Analysis.Potts.Confidence);
            local.initialPottsWeights = me{level}.weights;
        end
        orig = me{level};
        %    if local.nIters == 0 || ~me{level}.converged || obj.Analysis.Potts.MinNumberOfIters(2) > 0
        local.nIters = obj.Analysis.Potts.nIters(2);
        local.MinOccurance = minOcc;
        local.MinNumberOfIters = obj.Analysis.Potts.MinNumberOfIters(2);
        me{level} = trainPottsModelUsingNesterovGD(local, data, obj.Analysis.Potts.Confidence);
        if any(isnan(me{level}.weights))
            me{level} = orig;
        end
        if me{level}.converged
            break;
        else
            local.initialPottsWeights = me{level}.weights;
        end
    end
end

