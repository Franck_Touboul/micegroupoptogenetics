function obj = TrackClear(obj)
obj = TrackLoad(obj);
nobj = TrackGenerateObject(obj.VideoFile);
nobj = rmfield(nobj, 'Source'); 
nobj = CopyField(obj, nobj, 'ROI');
nobj = CopyField(obj, nobj, 'Colors');
nobj = CopyField(obj, nobj, 'BkgImage');
nobj = CopyField(obj, nobj, 'StartTime');
nobj = CopyField(obj, nobj, 'EndTime');
obj = nobj;
TrackSave(obj);

function to = CopyField(from, to, name)
if isfield(from, name)
    to.(name) = from.(name);
end