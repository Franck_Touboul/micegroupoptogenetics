function TrackDetectMovement(obj)
obj = TrackLoad(obj);
%%
AllAngles = [];
for s=1:obj.nSubjects
    %%
    dx = [0 diff(obj.x(s, :), 1, 2)];
    dy = [0 diff(obj.y(s, :), 1, 2)];
    v = [dx; dy] ./ repmat(sqrt(dx.^2 + dy.^2), 2, 1);
    angle = real(acos(sum(v(:, 2:end) .* v(:, 1:end-1))));
    AllAngles = [AllAngles, angle(~isnan(angle))];
    %Angle = [Angle, angle];
end
Angle = AllAngles(~isnan(AllAngles));
%atan2()
%%
model.nBins = 10;
model.trans = [0.95,0.05; 0.10,0.90];
model.emis = [...
    ones(1, model.nBins) / model.nBins;
    1/2 ones(1, model.nBins - 1) / (model.nBins - 1)];
[bins, AngleBins] = histc(Angle, sequence(0, pi, model.nBins+1));
AngleBins = AngleBins(AngleBins > 0);
%%
[new.trans, new.emis] = hmmtrain(AngleBins, model.trans, model.emis);
%%
%hmmviterbi(new.trans, new.emis);
for s=1:obj.nSubjects
    %%
    dx = [0 diff(obj.x(s, :), 1, 2)];
    dy = [0 diff(obj.y(s, :), 1, 2)];
    v = [dx; dy] ./ repmat(sqrt(dx.^2 + dy.^2), 2, 1);
    angle = real(acos(sum(v(:, 2:end) .* v(:, 1:end-1))));
    [bins, angleBins] = histc(angle, sequence(0, pi, model.nBins+1));
    valid = angleBins > 0;
    [b, e] = FindEvents(valid);
    states = angleBins * 0;
    for i=1:length(b)
        states(b(i):e(i)) = hmmviterbi(angleBins(b(i):e(i)), new.trans, new.emis);
    end
    %%
    x = obj.x(s, r);
    y = obj.y(s, r);
    rs = states(r);
    r=1:10000; plot3(r(rs == 2), x(rs == 2), y(rs == 2), 'r.')
    hon
    r=1:10000; plot3(r(rs == 1), x(rs == 1), y(rs == 1), 'g.')
    hoff
    view(90, 0)
end