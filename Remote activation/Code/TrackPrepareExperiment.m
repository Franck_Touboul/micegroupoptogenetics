function obj = TrackPrepareExperiment(obj, days, prototype, varargin)
options = ParseArguments(struct(...
    'AutomaticColors', false, ...
    'AutomaticRegions', false ...
    ), varargin{:});

%%
if ischar(obj)
    fprintf(['# preparing experiment for file: ''' obj '''\n']);
    obj = TrackGenerateObject(obj);
    obj.Output = true;
else
    fprintf(['# preparing experiment for file: ''' obj.VideoFile '''\n']);
end

%%
meta = regexp(obj.VideoFile, '(?<path>.*[\\\/])?(?<group>.*)\.exp(?<number>[0-9]*)\.day(?<day>[0-9]*)\.cam(?<camera>[0-9]*)(?<ext>.*)', 'names');
fprintf(['#    group: ' meta.group '\n']);
TrackDefaults = TrackGetDefaults(meta.group, str2double(meta.camera));
f = fields(TrackDefaults);
for i=1:length(f)
    obj.(f{i}) = TrackDefaults.(f{i});
end

if ~exist('days', 'var')
    days=1:TrackDefaults.nDays;
end
if isempty(meta.day)
    days = [];
end
fprintf(['#    days: ' mat2str(days) '\n']);

%%
if ~exist('prototype', 'var')
    prototype = TrackDefaults.Prototype;
end
if ischar(prototype)
    fprintf(['# - loading prototype from: ' prototype '\n']);
    prototype = TrackLoad(prototype, {'Colors', 'ROI', 'BkgImage'});
else
    fprintf(['# - using prototype from: ' prototype.Source '\n']);
end
obj.ROI.RegionNames = prototype.ROI.RegionNames;
obj.ROI.IsSheltered = prototype.ROI.IsSheltered;

%%
if options.AutomaticRegions
    fprintf(['# - matching background for regions\n']);
    obj = TrackFindBackground(obj);
    %if ~isfield(obj, 'ROI') || ~isfield(obj.ROI, 'Regions')
        %obj = TrackRegister(obj, prototype);
    %end
    obj.ROI = prototype.ROI;
    %TrackShowAllRegions(obj);
    %drawnow;
else
    obj = TrackFindBackground(obj);
    
    obj.ROI = prototype.ROI;
    frame = 1;
    TrackShowAllRegions(obj, frame);
    while true
        title('enter number of region to change (q to quit)');
        k = 0;
        while k ~= 1
            k = waitforbuttonpress;
        end
        key = get(gcf, 'CurrentCharacter');
        if char(key) == 'q' || char(key) == 'Q'
            break
        end
        if char(key) == 'b' || char(key) == 'B'
            if isempty(frame)
                frame = 1;
            else
                frame = [];
            end
            TrackShowAllRegions(obj, frame);
            continue;
        end
        label = key - 48;
        if label > 0 && label <= prototype.ROI.nRegions
            obj.ROI.Regions{label} = obj.ROI.Regions{label} * 0;
            TrackShowAllRegions(obj, frame);
            title(['mark bounderies for "' obj.ROI.RegionNames{label} '"']);
            obj.ROI.Regions{label} = roipoly;
            %                 [obj.ROI.Regions{label}, x0, y0] = roipoly;
            %                 [i, j] = find(obj.ROI.Regions{label});
            %                 cx = mean(j); cy = mean(i);
            %                 x = cx + (x0 - cx) * (1 + obj.RegionPaddingPercentage);
            %                 y = cy + (y0 - cy) * (1 + obj.RegionPaddingPercentage);
            %                 obj.ROI.Regions{label} = roipoly(zeros(obj.VideoHeight, obj.VideoWidth) , x, y);
        end
        TrackShowAllRegions(obj, frame);
    end
    close(gcf);
end
%%
if options.AutomaticColors
    obj.Colors = prototype.Colors;
    obj = TrackFindDarkPeriod(obj, .2);
else
    fprintf(['# - computing background frame\n']);
    obj = TrackFindDarkPeriod(obj, .2);
    obj = TrackComputeColorsGUI(obj);
    obj = TrackComputeColors(obj);
    %obj = TrackComputeColorsAutomatically(obj);
end
%
fprintf(['# - saving object\n']);
obj = TrackSave(obj);
%%
if ~isempty(days)
    for day = days
        if day == str2double(meta.day)
            continue
        end
        videofile = sprintf('%s%s.exp%04d.day%02d.cam%02d%s', meta.path, meta.group, str2double(meta.number), day, str2double(meta.camera), meta.ext);
        nobj = TrackResetObject(obj, videofile);
        try
            TrackPrepareExperiment(nobj, day, obj, 'AutomaticColors', true, 'AutomaticRegions', true);
        catch me
            disp me.message
        end
    end
end