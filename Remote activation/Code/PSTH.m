zones = obj.zones(:, obj.valid);
[si, st] = find(diff([zeros(obj.nSubjects, 1) zones], 1, 2));
ST = cell(1, obj.nSubjects);
for s=1:obj.nSubjects
    ST{s} = st(si == s);
end
%%
s1 = 1;
m = 100;
other = 1:obj.nSubjects; other = other(other ~= s1);
st = ST{s1};
psth = cell(obj.ROI.nZones);
for i=1:obj.ROI.nZones
    for j=1:obj.ROI.nZones
        psth{i,j} = [];
    end
end
for i=1:length(st)
    if st(i) < m
        continue;
    end
    fr = zones(s1, st(i)-1);
    to = zones(s1, st(i));
    psth{fr, to} = cat(3, psth{fr, to}, zones(other, st(i)-m+1:st(i)));
end
%
i1 = 2;
i2 = 1;
cmap = MyMouseColormap;
c = psth{i1,i2};
j = 1;
for s=other
    subplot(obj.nSubjects-1, 1, j);
    z = flipdim(sortrows(squeeze(c(j, :, :))'), 2);
    imagesc(z);
    set(gca, 'xticklabel', -get(gca, 'xtick'))
    j = j + 1;
end
hoff
colormap(MyZonesColormap);
%%
m = 100;
nbins = 25;

s1 = 1;
st = ST{s1};
%st = randperm(size(zones, 2)); st = st(1:length(ST{s1}));
other = 1:obj.nSubjects; other = other(other ~= s1);
stat = [];
stat.last.times = [];
stat.last.subj = [];
stat.prelast.times = [];
stat.prelast.subj = [];
for i=1:length(st)
    for s2=other
        idxF = find(ST{s2} <= st(i), 1, 'last');
        if ~isempty(idxF)
            stat.last.times = [stat.last.times, st(i) - ST{s2}(idxF)];
            stat.last.subj = [stat.last.subj, s2];
            if idxF > 1
                stat.prelast.times = [stat.prelast.times, st(i) - ST{s2}(idxF-1)];
                stat.prelast.subj = [stat.prelast.subj, s2];
            end
        end
    end
end
%
x = [sequence(0, m, nbins+1) inf];
h = histc(stat.last.times, x);
bar(x(1:nbins), h(1:nbins)/sum(h(1:nbins)))

%%
WindowSize = 200;
FrameSize = 10;
MinWindowSize = 50;

s1 = 4;
other = 1:obj.nSubjects; other = other(other ~= s1);

change = [false(obj.nSubjects, 1) diff(zones, 1, 2) ~= 0];
changeF = convn(change, ones(1, FrameSize)); 
changeF = changeF(:, FrameSize:FrameSize:length(changeF)-mod(length(change), FrameSize)) > 0;

idxF = find(changeF(s1, :));
idxF = idxF([idxF(1) diff(idxF)] >= ceil(MinWindowSize / FrameSize));

nrmlzWindowSize = ceil(WindowSize / FrameSize);
z = zeros(obj.nSubjects-1, 2*nrmlzWindowSize + 1);
zonecount = zeros(obj.ROI.nZones, 2*nrmlzWindowSize + 1);
for i=1:length(idxF)
    if idxF(i) <= nrmlzWindowSize || idxF(i) + nrmlzWindowSize >= size(changeF, 2)
        continue;
    end
    z = z + changeF(other, idxF(i)-nrmlzWindowSize:idxF(i)+nrmlzWindowSize);
end
x = (-nrmlzWindowSize:nrmlzWindowSize) * FrameSize / obj.FrameRate;
plot(x, sum(z) - median(sum(z)))
a = axis;
fill([-MinWindowSize / obj.FrameRate, MinWindowSize / obj.FrameRate, MinWindowSize / obj.FrameRate, -MinWindowSize / obj.FrameRate], [a(3) a(3) a(4) a(4)], ones(1,3)*.8, 'edgecolor', 'none');
hon;
plot(x, sum(z) - median(sum(z)))
vert_line(0, 'color', 'k');
axis([min(x) max(x) a(3) a(4)]);
xlabel('time lag [secs]');
hoff;