function obj = SocialControls(obj)
obj.TemporalResolutionSecs = 1; % [sec]

nFramesInBlock = round(obj.TemporalResolutionSecs / obj.dt);
func = @(x) mode(x.data);
validZones = double(obj.zones);
validZones(:, ~obj.valid) = nan;
coarse = blockproc(validZones, [1 nFramesInBlock], func);

%%
countZones  = zeros(obj.nSubjects, max(obj.zones(:)));
countCoarse = zeros(obj.nSubjects, max(obj.zones(:)));
for i=1:obj.nSubjects
    for j=1:length(countZones)
        countZones(i, j) = sum(obj.zones(i, :) == j);
        countCoarse(i, j) = sum(coarse(i, :) == j);
    end
    countZones(i, :)  = countZones(i, :) / sum(countZones(i, :)) * 100;
    countCoarse(i, :) = countCoarse(i, :) / sum(countCoarse(i, :)) * 100;
end

%%
clf
map = jet(length(countZones));
inZoneDuration = {};
inZoneDurationAll = [];
nEventsLessThenHalfWindow = zeros(1, length(countZones));
timeInEventsLessThenHalfWindow = zeros(1, length(countZones));
for j=1:length(countZones)
    subplot(4,2,1:2);
    seq = sequence(j-1/4, j+1/4, obj.nSubjects);
    VertLine(j-1/2, 'Color', 'k', 'LineStyle', ':','HandleVisibility','off');
    hold on;
    inZoneDuration{j} = [];
    for i=1:obj.nSubjects
        plot(seq(i), countZones(i, j), 's', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', obj.Colors.Centers(i, :));
        plot(seq(i), countCoarse(i, j), 'x', 'MarkerEdgeColor', 'k', 'LineWidth', 2);
        %
        d = diff([0 obj.zones(i, :) == j 0], 1, 2);
        start  = find(d > 0);
        finish = find(d < 0) - 1;
        inZoneDuration{j} = [inZoneDuration{j}, finish - start + 1];
        inZoneDurationAll = [inZoneDurationAll, finish - start + 1];
    end
    nEventsLessThenHalfWindow(j) = mean(inZoneDuration{j} < nFramesInBlock / 2);
    timeInEventsLessThenHalfWindow(j) = sum(inZoneDuration{j}(inZoneDuration{j} < nFramesInBlock / 2)) / sum(inZoneDuration{j});
    %
    subplot(4,2,3:4);
    if j == 1;
        x = sequence(min(inZoneDurationAll), max(inZoneDurationAll), 100); 
        dx = x(2) - x(1);
    end
    h = histc(inZoneDuration{j}, x);
    
    loglog((x(1:end-1) + x(2:end))/2*obj.dt/60, h(1:end-1)/sum(h(1:end-1)), '.:', 'Color', map(j, :)); hold on;
end
%
subplot(4,2,1:2);
VertLine(length(countZones)+1/2, 'Color', 'k', 'LineStyle', ':');
set(gca, 'XTick', 1:length(countZones), 'XTickLabel', obj.ROI.ZoneNames)
prettyPlot('Location Histogram for Coarser Temporal Resolution', 'zone name', '% of time in zone');
legend([num2str(obj.dt) ' sec window'], [num2str(obj.TemporalResolutionSecs) ' sec window'], 'location', 'EastOutside');
legend boxoff
hold off;
%
subplot(4,2,3:4);
prettyPlot('Time Spent in Zone Histogram', 'time [mins]', 'prob');
legend(obj.ROI.ZoneNames, 'location', 'EastOutside');
legend boxoff
hold off;
%%
subplot(4,2,5);
for j=1:length(countZones)
    bar(j, nEventsLessThenHalfWindow(j) * 100, 'FaceColor', map(j, :));
    hold on;
end
set(gca, 'XTick', 1:length(countZones), 'XTickLabel', obj.ROI.ZoneNames)
prettyPlot('Relative No. of Events that Last Less then Half The Window Duration', 'zone name', '%');
hold off;
%
subplot(4,2,6);
for j=1:length(countZones)
    bar(j, timeInEventsLessThenHalfWindow(j) * 100, 'FaceColor', map(j, :));
    hold on;
end
set(gca, 'XTick', 1:length(countZones), 'XTickLabel', obj.ROI.ZoneNames)
prettyPlot('Relative Duration of Events that Last Less then Half The Window Duration', 'zone name', '%');
hold off;
%%
subplot(4,2,7);
%%
coarseObj = obj;
coarseObj.zones = coarse;
coarseObj.valid = ~isnan(coarseObj.zones(1, :));

[obj, indepProbs, jointProbs, pci, patterns] = SocialComputePatternProbs(obj, false);
[coarseObj, coarseIndepProbs, coarseJointProbs] = SocialComputePatternProbs(coarseObj, false);
Dkl = jointProbs * (log2(jointProbs) - log2(coarseJointProbs))';

PlotProbProb(jointProbs, coarseJointProbs, sum(coarseObj.valid));
prettyPlot(sprintf('prob-prob plot ($D_{KL}$=%f)', Dkl), 'observed pattern prob [log10]', 'coarse pattern prob [log10]');



