function TrackSegmentF(nruns, id)
%%
fprintf('# segmenting movie frames:\n');
if ~license('checkout', 'image_toolbox')
    fprintf('# - waiting for image processing toolbox license\n');
    while ~license('checkout', 'image_toolbox'); pause(5); end
end
TrackDefaults;
options.output = true;
%%
fprintf('# segmenting frames\n');
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nFrames = xyloObj.NumberOfFrames;
dt = 1/xyloObj.FrameRate;
%%
fprintf('# - loading meta data\n');
filename = [options.output_path options.test.name '.meta.mat'];
waitforfile(filename);
load(filename);

%%
ROIloaded = false;
% roi = struct();
% if isfield(options, 'loadROI') && options.loadROI
%     filename = [options.output_path options.test.name '.roi.mat'];
%     load(filename);
%     ROIloaded = true;
% elseif isfield(options, 'ROIFile') && ~isempty(options.ROIFile)
%     options.loadROI = true;
%     load(options.ROIFile);
%     ROIloaded = true;
% end
% ignoreRegion = [];
% if ROIloaded &&  isfield(roi, 'Ignore')
%     ignoreRegion = roi.Ignore;
% end

%%
if nargin == 0
    startframe = 426408;
    endframe = 426408+1000;
    
        startframe = 557355;
    endframe = startframe+1000;

else
    step = floor(nFrames / nruns);
    curr = 0;
    for i=1:id
        prev = curr + 1;
        if i == nruns
            curr = nFrames;
        else
            curr = prev + step - 1;
        end
    end
    startframe = prev;
    endframe = curr;
end
nFrames = endframe - startframe + 1;
%%
prevProps = [];
sx = []; sy = [];

cents.x = zeros(nFrames, options.maxNumCents);
cents.y = zeros(nFrames, options.maxNumCents);
cents.label = zeros(nFrames, options.maxNumCents, 'uint8');
cents.area = zeros(nFrames, options.maxNumCents, 'uint16');
cents.solidity = zeros(nFrames, options.maxNumCents, 'single');
cents.logprob = zeros(nFrames, options.maxNumCents, options.nSubjects);
%%
doubleBkgFrame = im2double(meta.bkgFrame);
bkgNoise = std(doubleBkgFrame(:));
cmap = [meta.subject.centerColors; 0 0 0];
%%
nchars = RePrintf('# - frame %6d [%d-%d] (%6.2fxiRT)', startframe, startframe, endframe, 0); 
tic;
bkgFrame = im2double(imresize(meta.bkgFrame, options.scale));
for r=1:nFrames
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d [%d-%d] (%6.2fxiRT)', r+startframe-1, startframe, endframe, RT);
    currTime = (r+startframe-1) * dt;
    if isfield(options, 'movieStartTime') && currTime < options.movieStartTime;
        continue;
    end
    if isfield(options, 'movieEndTime') && options.movieEndTime > 0 && currTime > options.movieEndTime
        continue;
    end
    
    orig = myMMReader(options.MovieFile, r+startframe-1, meta.bkgFrame);
    orig = im2double(imresize(orig, options.scale));
    m = imsubtract(orig, bkgFrame);
    
    if isempty(sx)
        [sx, sy, nc] = size(m);
    end
    %%
    rm = orig(:, :, 1);
    gm = orig(:, :, 2);
    bm = orig(:, :, 3);
    
%    hsv_m = rgb2hsv(m);
%     hm = hsv_m(:,:,1);
%     sm = hsv_m(:,:,2);
%     sm = hsv_m(:,:,2);
    vm = max(m,[],3);
    %%
    meanBKG = mean(vm(:)); 
    stdBKG = std(vm(:));
    if ~isfield(options, 'useAdaptiveThresh')
        options.useAdaptiveThresh = true;
    end
    if options.useAdaptiveThresh
        upper = options.noiseThresh;
        lower = 1;
        prev_thresh = round((upper + lower)/2);
        while true
            thresh = round((upper + lower)/2);
            bw = vm > meanBKG + thresh * stdBKG;
            %bw(ignoreRegion) = false;
            cc = bwconncomp(bw);
            if cc.NumObjects < options.maxNumObjects
                upper = thresh - 1;
                prev_thresh = thresh;
            else
                lower = thresh + 1;
            end
            if lower > upper
                break
            end
        end
        if thresh ~= prev_thresh
            thresh = prev_thresh;
            bw = vm > meanBKG + thresh * stdBKG;
        end
    else
        thresh = options.noiseThresh;
        bw = vm > meanBKG + thresh * stdBKG;
        cc = bwconncomp(bw);
        if cc.NumObjects > options.maxNumObjects
            continue;
        end
    end
    bw = bwareaopen(bw, options.minNumPixels);

    %%
    %labels = bwlabel(bw);
    %nobjects = max(labels(:));
    %%
    [b, idx_r] = histc(rm(bw), meta.subject.colorBins);
    [b, idx_g] = histc(gm(bw), meta.subject.colorBins);
    [b, idx_b] = histc(bm(bw), meta.subject.colorBins);
    prob_r = zeros(options.nSubjects, length(idx_r));
    prob_g = zeros(options.nSubjects, length(idx_g));
    prob_b = zeros(options.nSubjects, length(idx_b));
    for i=1:options.nSubjects
        prob_r(i, :) = meta.subject.r(i, idx_r);
        prob_g(i, :) = meta.subject.g(i, idx_g);
        prob_b(i, :) = meta.subject.b(i, idx_b);
    end
    %     prob_h = prob_h ./ repmat(sum(prob_h, 1), options.nSubjects, 1);
    %     prob_s = prob_s ./ repmat(sum(prob_s, 1), options.nSubjects, 1);
    %     prob_v = prob_v ./ repmat(sum(prob_v, 1), options.nSubjects, 1);
    %     joint_prob = prob_h .* prob_s .* prob_v;
    joint_prob = prob_r .* prob_g .* prob_b;
    joint_prob = joint_prob ./ repmat(sum(joint_prob, 1), options.nSubjects, 1);
    
    [m, idx] = max(joint_prob, [], 1);
    nlabels = zeros(sx,sy,'uint8');
    nlabels(bw) = idx;
    
    logprobmap = cell(1, options.nSubjects);
    for k=1:options.nSubjects
        logprobmap{k} = zeros(sx,sy);
        logprobmap{k}(bw) = flog(joint_prob(k, :));
    end
    if options.output
        flabels = zeros(sx,sy,'uint8');
    end
    %% filter small regions
    rejected = false(size(bw));
    conn = cell(1, options.nSubjects);
    validmap = cell(1, options.nSubjects);
    for i=1:options.nSubjects
        reg = bwconncomp(nlabels == i);
        conn{i} = regionprops(reg, 'Solidity', 'Centroid', 'PixelIdxList');
        validmap{i} = false(size(bw));
        for j=1:length(conn{i})
            if conn{i}(j).Solidity < options.solidity || ...
                    length(reg.PixelIdxList{j}) < options.minNumPixels
                rejected(reg.PixelIdxList{j}) = true;
                conn{i}(j).PixelIdxList = [];
            else
                validmap{i}(conn{i}(j).PixelIdxList) = true;
            end
        end
    end
    %% reassign rejected regions to clusters
    lrejected = bwlabel(rejected);
    dlrejected = imdilate(lrejected, ones(3,3));
    for i=1:options.nSubjects
        u_ = unique(dlrejected(validmap{i}))';
        if ~isempty(u_)
            for u=u_
                if u > 0
                    validmap{i}(lrejected == u) = true;
                    rejected(lrejected == u) = false;
                end
            end
        end
    end
    
    %% compute the new region props
    for i=1:options.nSubjects
        validmap{i} = imerode(validmap{i}, strel('disk', options.erodeRadios));
        reg = bwconncomp(validmap{i});
        conn{i} = regionprops(reg, 'Solidity', 'Centroid', 'PixelIdxList');
    end    
    rejected = imerode(rejected, strel('disk', options.erodeRadios));
    reg = bwconncomp(rejected);
    conn{options.nSubjects+1} = regionprops(reg, 'Solidity', 'Centroid', 'PixelIdxList');
    %% save centers
    centIndex = 1;
    for i=1:options.nSubjects+1
        %%
        if i <= options.nSubjects
            currmap = validmap{i};
        else
            currmap = rejected;
        end
        for j=1:length(conn{i})
            if length(conn{i}(j).PixelIdxList) > options.minNumPixelsAfterErode
                cents.x(r, centIndex) = conn{i}(j).Centroid(1);
                cents.y(r, centIndex) = conn{i}(j).Centroid(2);
                cents.label(r, centIndex) = i;
                cents.area(r, centIndex) = length(conn{i}(j).PixelIdxList);
                cents.solidity(r, centIndex) = conn{i}(j).Solidity;
                for k=1:options.nSubjects
                    cents.logprob(r, centIndex, k) = sum(logprobmap{k}(conn{i}(j).PixelIdxList));
                end
                cents.logprob(r, centIndex, :) = cents.logprob(r, centIndex, :) - flog(sum(exp(cents.logprob(r, centIndex, :))));
                centIndex = centIndex + 1;
                if centIndex > options.maxNumCents
                    break;
                end
            else
                currmap(conn{i}(j).PixelIdxList) = 0;
           end
        end
        if centIndex > options.maxNumCents
             cents.label(1, :) = 0;
        end

        if options.output
            reg = bwconncomp(currmap);
            img = labelmatrix(reg);
            if 1 == 2
                subplot(1,2,i);
                imagesc(img > 0);
            end
            flabels(img > 0) = i;
        end
    end
    %%
     if options.output
        subplot(1,2,2);
        rgblbls = label2rgb(flabels, cmap);
        imagesc(rgblbls);
        subplot(1,2,1);
        boundries = (imdilate(flabels ~= 0, ones(3,3)) - (flabels ~= 0)) ~= 0;
        for i=1:3
            slice = orig(:, :, i);
            slice(boundries) = 1;
            orig(:, :, i) = slice;
        end
        imagesc(orig);
        title(num2str(r));
        drawnow
    end
    %%
end
