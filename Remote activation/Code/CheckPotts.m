o={}; 
for i=1:4; 
    o{i} = SocialSimulatePottsModel(obj, i); 
end
%%
for i=2:4;
    %%
    o{i}.OutputToFile = false;
    o{i}.OutputInOldFormat = false;
    o{i} = SocialPotts(o{i});
    %%
    orig = obj.Analysis.Potts.Model{i};
    me = orig;
    neutral = false(1, length(orig.labels));
    for l=1:length(me.labels)
        neutral(l) = any(orig.labels{l}(2, :) == 1);
    end
    me.constraints = orig.constraints(~neutral);
    me.perms = orig.perms(:, ~neutral);
    me.labels = orig.labels(~neutral);
    me.weights = orig.weights(~neutral);

    %%
    cmap = MyCategoricalColormap;
    subplot(4,5,(i-1)*5+5);
    myBoxPlot(cumsum(o{i}.Analysis.Potts.Ik), {'Pairwise (I_{(2)})', 'Triplet (I_{(3)})', 'Quadruplet (I_{(4)})'}, cmap(1:end, :));
    subplot(4,5,(i-1)*5+1:(i-1)*5+4);
    plot(o{i}.Analysis.Potts.Model{i}.weights, me.weights, '.');
end