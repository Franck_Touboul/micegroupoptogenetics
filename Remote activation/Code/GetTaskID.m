function id = GetTaskID
id = str2num(getenv('SGE_TASK_ID'));
fprintf('# TASK ID: %s\n' , id);