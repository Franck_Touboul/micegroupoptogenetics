function OverSampleMovie(obj)
%%
startframe = obj.StartTime * obj.FrameRate;
endframe = obj.EndTime * obj.FrameRate;
valid = find(all(obj.sheltered, 1));
valid = valid(valid > startframe & valid < endframe);
valid = valid(randperm(length(valid)));
%%
nframes = 5000;
s = zeros(obj.VideoHeight, obj.VideoWidth, 3);
for i=1:nframes
    i
    m = im2double(myMMReader(obj.VideoFile, valid(i)));
    s = s + m;
end