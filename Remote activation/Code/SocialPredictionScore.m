% fid = fopen('SC.list');
% C = textscan(fid, '%s');
% fclose(fid);

rankBy = 'highest';
type = 'Enriched';
path = 'log/';
C = dir([path 'SocialPredictionApprox.o*']);
flist = {C.name};
%%
stat = struct();
stat.Rank.I = cell(1, obj.nSubjects);
stat.Rank.H1 = cell(1, obj.nSubjects);
stat.Rank.H2 = cell(1, obj.nSubjects);
Experiments = containers.Map();

%%
for i=1:length(flist)
    data = ReadStrudels([path flist{i}]);
    if length(fieldnames(data)) > 0
        if strcmp(rankBy, 'highest')
            data.rank = max(data.rank) + 1 - data.rank;
        else
            data.rank = 5 - data.rank;
        end
        
        experiment.Type = regexprep(data.name, '^([^\.]*)\..*', '$1');
        if ~isKey(Experiments, experiment.Type)
            stat = struct();
            stat.Rank.I = cell(1, obj.nSubjects);
            stat.Rank.H1 = cell(1, obj.nSubjects);
            stat.Rank.H2 = cell(1, obj.nSubjects);
            
            stat.nRank.I = cell(1, obj.nSubjects);
            stat.nRank.rI = cell(1, obj.nSubjects);
            
            stat.times = data.times;
            stat.Group = struct();
            stat.count = 0;
            Experiments(experiment.Type) = stat;
        else
            stat = Experiments(experiment.Type);
        end
        experiment.GroupID = str2double(regexprep(data.name, '.*exp([0-9]*).*', '$1'));
        experiment.Day = str2double(regexprep(data.name, '.*day([0-9]*).*', '$1'));
        experiment.CameraID = str2double(regexprep(data.name, '.*cam([0-9]*).*', '$1'));
        %%
        stat.Group(stat.count + 1).I = data.I;
        stat.Group(end).H1 = data.H1;
        stat.Group(end).H2 = data.H2;
        stat.Group(end).rank = data.rank;
        stat.count = stat.count + 1;
        
        minI = min(data.I(:, stat.times == 0));
        maxI = max(data.I(:, stat.times == 0));
        
        minIH = min(data.I(:, stat.times == 0) ./ data.H1(:, stat.times == 0));
        maxIH = max(data.I(:, stat.times == 0) ./ data.H1(:, stat.times == 0));
        
        
        IH = data.I ./ data.H1;
        for s=1:length(data.rank)
            %             stat.Rank.I{data.rank(s)} = [stat.Rank.I{data.rank(s)}; data.I(s, :)];
            %             stat.Rank.H1{data.rank(s)} = [stat.Rank.H1{data.rank(s)}; data.H1(s, :)];
            %             stat.Rank.H2{data.rank(s)} = [stat.Rank.H2{data.rank(s)}; data.H2(s, :)];
            stat.Rank.I{data.rank(s)} = [stat.Rank.I{data.rank(s)}; data.I(s, :)];
            stat.Rank.H1{data.rank(s)} = [stat.Rank.H1{data.rank(s)}; data.H1(s, :)];
            stat.Rank.H2{data.rank(s)} = [stat.Rank.H2{data.rank(s)}; data.H2(s, :)];
            
%             stat.nRank.I{data.rank(s)} = [stat.nRank.I{data.rank(s)}; (data.I(s, :) - minI) ./ (maxI - minI)];
%             stat.nRank.rI{data.rank(s)} = [stat.nRank.rI{data.rank(s)}; ((data.I(s, :) ./ data.H1(s, :)) - minIH) / (maxIH - minIH)];
            
%             stat.nRank.I{data.rank(s)} = [stat.nRank.I{data.rank(s)}; (data.I(s, :) - mean(data.I, 1))];
%             stat.nRank.rI{data.rank(s)} = [stat.nRank.rI{data.rank(s)}; IH(s, :) - mean(IH)];

            stat.nRank.I{data.rank(s)} = [stat.nRank.I{data.rank(s)}; (data.I(s, :) - mean(data.I(:, stat.times == 0), 1))];
            stat.nRank.rI{data.rank(s)} = [stat.nRank.rI{data.rank(s)}; IH(s, :) - mean(IH(:, stat.times == 0), 1)];
        
        end
        %%
        Experiments(experiment.Type) = stat;
    end
end
%%
figure(1);
cmap = MyCategoricalColormap;

stat = Experiments(type);
%entries = {};
entries = {'alpha', 'beta', 'gamma', 'delta'};
for s=1:obj.nSubjects
    I = mean(stat.Rank.I{s});
    sI = stderr(stat.Rank.I{s});
    H1 = stat.Rank.H1{s};
    H2 = stat.Rank.H2{s};
    rI = mean(stat.Rank.I{s} ./ stat.Rank.H1{s} * 100, 1);
    srI = stderr(stat.Rank.I{s} ./ stat.Rank.H1{s} * 100, 1);
    dt_ = stat.times;
    
    subplot(3,1,1);
    errorbar(stat.times, rI, srI, '.-', 'color', cmap(s, :), 'LineWidth', 2)
    hon;
    plot(-stat.times(stat.times > 0), rI(stat.times > 0), ':', 'color', cmap(s, :), 'LineWidth', 2,'HandleVisibility','off')
    ylabel('explained information [%]');
    
    subplot(3,1,2);
    errorbar(stat.times, I, sI, '.-', 'color', cmap(s, :), 'LineWidth', 2)
    hon;
    plot(-stat.times(stat.times > 0), I(stat.times > 0), ':', 'color', cmap(s, :), 'LineWidth', 2,'HandleVisibility','off')
    ylabel('mutual information [bits]');
    
    nrI = mean(stat.nRank.rI{s});
    snrI = stderr(stat.nRank.rI{s});
    subplot(3,1,3);
    errorbar(stat.times, nrI, snrI, '.-', 'color', cmap(s, :), 'LineWidth', 2)
    hon;
    plot(-stat.times(stat.times > 0), nrI(stat.times > 0), ':', 'color', cmap(s, :), 'LineWidth', 2,'HandleVisibility','off')
    ylabel('mutual information [normalized units]');
    
end
for i=1:3
    subplot(3,1,i);
    
    legend(entries);
    legend boxoff;
    xaxis(dt_(1), dt_(end));
    set(gca, 'XTickLabel', get(gca, 'XTick') / obj.FrameRate);
    xlabel('time lag [secs]');
    
    hoff
end
subplot(3,1,1);
if strcmp(rankBy, 'highest')
    title([ type ': mice with highest status in group are alphas' ]);
else
    title([ type ': mice with lowest status in group are deltas' ]);    
end
saveFigure(['Graphs/' type '.SocialPrediction.rankedBy' rankBy '.mean']);

%%
figure(2);
clf
stat = Experiments(type);

for i=1:length(stat.Group)
    subplot(2, length(stat.Group), i);
    
    I = stat.Group(i).I;
    rank = stat.Group(i).rank;
    H1 = stat.Group(i).H1;
    rI = I ./ H1;
    dt_ = stat.times;
    if i==1;
        for s=1:obj.nSubjects
            plot(stat.times(1)-1, rI(s, 1)*100, '.-', 'color', cmap(s, :), 'LineWidth', 2); hon;
        end
    end
    
    for s=1:obj.nSubjects
        plot(stat.times, rI(s, :)*100, '.-', 'color', cmap(rank(s), :), 'LineWidth', 2)
        hon;
    end
    xaxis(dt_(1), dt_(end));
    if i==1;
        ylabel('explained information [%]');
        legend(entries);
        legend boxoff;
        
    end
    
    subplot(2, length(stat.Group), length(stat.Group) + i);
    for s=1:obj.nSubjects
        plot(stat.times, I(s, :), '.-', 'color', cmap(rank(s), :), 'LineWidth', 2)
        hon;
    end
    ylabel('mutual information [bits]');
end
TieAxis(2, length(stat.Group), 1:length(stat.Group))
TieAxis(2, length(stat.Group), length(stat.Group) + (1:length(stat.Group)))


    subplot(2, length(stat.Group), floor(length(stat.Group) / 2));
if strcmp(rankBy, 'highest')
    title([ type ': mice with highest status in group are alphas' ]);
else
    title([ type ': mice with lowest status in group are deltas' ]);    
end
saveFigure(['Graphs/' type '.SocialPrediction.rankedBy' rankBy '.all']);
