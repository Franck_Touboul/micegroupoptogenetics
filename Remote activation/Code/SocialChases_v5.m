%function [events, state] = SocialChases_v4(obj, m1, m2)
%%
fprintf('# finding ''Chases''\n');
nIters = 2;
obj.Interactions.MinEventDuration = 12;
obj.Interactions.MinSubEventDuration = 6;
obj.MaxShelteredContanctDuration = 25;
%%
obj = TrackLoad(obj);
obj = SocialFindInteractions(obj);
%%
speed = obj.speed(:);
minspeed = FindProbabilityCutoffPoint(speed(speed>0), .95);
%%
obj.Interactions.States = cell(obj.nSubjects);
isfirst = true;
for iter=0:nIters
    fprintf('# - iter %d/%d\n', iter, nIters);
    allstates = [];
    allinputs = [];
    fprintf('#  . expectation ');
    count = 0;
    for m1=1:obj.nSubjects
        count = Reprintf(count, '(%3d perc.)', round(m1/obj.nSubjects * 100));
        for m2=1:obj.nSubjects
            if m1 == m2; continue; end;
            %%
            [features, valid, segmented, begF, endF] = PPModelComputeFeatures_v2(obj, m1, m2, minspeed);
            %%
            if isfirst
                model = GeneratePerdPreyModel(features(:, valid));
                isfirst = false;
            end
            %%
            backtrack = ModelViterbiSequence_v2(model, segmented, false);
            
            state = zeros(1, size(features, 2));
            for i=1:length(begF)
                currBacktrack = backtrack{i};
                %currBacktrack(currBacktrack == 1 & currContacts(begF(i):endF(i)) == 1) = 4;
                state(begF(i):endF(i)) = currBacktrack;
            end
            allstates = [allstates, state];
            allinputs = [allinputs, features];
            obj.Interactions.States{m1, m2} = state;
        end
    end
    
    %%
    fprintf('\n#  . maximization\n');
    if iter < nIters
        for s=1:length(model.states)
            [beg, fin, len] = FindEvents(allstates == s);
            model = model.states(s).estimate(allinputs(:, allstates == s), model, s);
            prob = (sum(allstates == s) - length(len)) / sum(allstates == s);
            model.trans(s, :) = (model.trans(s, :) > 0) * (1-prob) / (sum(model.trans(s, :) > 0) - 1);
            model.trans(s, s) = prob;
        end
        drawnow;
    end
end
fprintf('\n');
obj.Interactions.Model = model;
%TrackSave(obj);

%% remove short events
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1==m2; continue; end;
        [beg, fin, len] = FindEvents(obj.Interactions.States{m1, m2} > 1);
        for i=find(len < obj.Interactions.MinEventDuration)
            obj.Interactions.States{m1, m2}(beg(i):fin(i)) = 1;
        end
    end
end
%% remove short sub events
s_ = [2 3 5 6];

for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1==m2; continue; end;
        for s=s_
            [start, finish, len] = FindEvents(obj.Interactions.States{m1, m2} == s);
            for i=find(len < obj.Interactions.MinSubEventDuration)
                range = start(i):finish(i);
                contact = obj.contact{min(m1, m2), max(m1, m2)}(range);
                states = obj.Interactions.States{m1, m2}(range);
                states( contact) = 4;
                states(~contact) = 1;
                obj.Interactions.States{m1, m2}(range) = states;
            end
        end
    end
end

%% plot angle histograms
for s=1:length(model.states)
    if isempty(model.states(s).histogram)
        model.states(s).estimate(allinputs(:, allstates == s), model, s);
    else
        bins = sequence(model.Histogram.minval, model.Histogram.maxval, model.Histogram.nBins + 1);
        centers = (bins(1:end-1) + bins(2:end)) / 2;
        [~, ~, m, n] = SquareSubplpot(length(model.states), s);
        cplot(centers, model.states(s).histogram, 'k:');
    end
    title(obj.Interactions.Model.states(s).title);
end
TieAxis(m, n, 1:length(model.states));

%% plot speed histograms
maxspeed = 250;
minspeed = 0;
maxspeedVal = 250;

speed.hist = [];
speed.x = [];
nBins = 50;
for s=1:length(model.states)
    speeds = [];
    for m1=1:obj.nSubjects
        for m2=1:obj.nSubjects
            if m1==m2; continue; end;
            speeds = [speeds, obj.speed(m1, obj.Interactions.States{m1, m2} == s)];
        end
    end
    %maxspeed = max(speeds);
    %minspeed = min(speeds);
    if s==1
        edges = sequence(minspeed, maxspeed, nBins+1);
        centres = (edges(1:end-1)+edges(2:end))/2;
        speed.maxval = maxspeed;
        speed.minval = minspeed;
    end
    
    [pr, pc, m, n] = SquareSubplpot(length(model.states), s);
    h = histc(speeds(speeds < maxspeed & speeds > minspeed), edges);
    h = h(1:end-1);
    speed.hist(s, :) = h / sum(h) / (centres(2) - centres(1));
    plot(centres(centres < maxspeedVal), speed.hist(s, centres < maxspeedVal));
    title(obj.Interactions.Model.states(s).title);
    if pr == 1
        xlabel('speed [pixel/sec]');
    end
    if pc == 0
        ylabel('density');
    end
    a = axis;
    axis([0 maxspeedVal, a(3) a(4)]);
    set(gca, 'XTick', [0 maxspeedVal/2 maxspeedVal]);
end
TieAxis(m, n, 1:length(model.states));
%% compute events
m1=3;
m2=1;
[start, finish, len] = FindEvents(obj.Interactions.States{m1, m2} > 1);
events = cell(1, length(start));
for i=1:length(start)
    events{i}.start  = start(i);
    events{i}.finish = finish(i);
    events{i}.states = obj.Interactions.States{m1, m2}(start - 1: finish + 1);
    prev = 1;
    desc = '';
    for f=start(i):finish(i)+1
        if f>finish(i) || obj.Interactions.States{m1, m2}(f) ~= prev
            if prev ~= 1
                desc = sprintf([desc '%-5s (%8d - %8d)\n'], model.states(prev).title , pstart, f-1);
            end
            if f<=finish(i)
                prev = obj.Interactions.States{m1, m2}(f);
            end
            pstart = f;
        end
    end
    events{i}.desc = desc;
end

return;
%% aggressive
nbins = 50;
thresh = s_ * 0;
index = 1;
for s=s_
    s
    likelihoodRatio_ = [];
    for m1=1:obj.nSubjects
        for m2=1:obj.nSubjects
            if m1==m2; continue; end;
            [beg, fin] = FindEvents(obj.Interactions.States{m1, m2} == s);
            likelihoodRatio = beg * 0;
            for i=1:length(beg)
                curr = obj.speed(m1, beg(i):fin(i));
                p1 = DiscreteProbability(curr, speed.hist(1, :), speed.minval, speed.maxval);
                p2 = DiscreteProbability(curr, speed.hist(2, :), speed.minval, speed.maxval);
                p1 = mean(log(p1(~isnan(p2))));
                p2 = mean(log(p2(~isnan(p2))));
                likelihoodRatio(i) = p1 - p2;
                likelihoodRatio_ = [likelihoodRatio_, likelihoodRatio];
            end
        end
    end
    %%
    path(path,'../netlab');
    mix = gmm(1, 2, 'diag');
    options = [];
    options(1) = 1;
    options(14) = 100;
    valid = likelihoodRatio_ ~= 0 & ~isnan(likelihoodRatio_);
    mix = gmmem(mix, likelihoodRatio_(valid)', options);
    %%
    SquareSubplpot(length(s_), index);
    [h, x] = hist(likelihoodRatio_(valid), nbins);
    plot(x, h / sum(h) / (x(2) - x(1)), 'kx--');
    hold on;
    plot(x, gmmprob(mix, x'), 'LineWidth', 2);
    
    [~, i] = max(mix.centres);
    g1 = gauss(mix.centres(i), mix.covars(i), x') * mix.priors(i);
    g2 = gauss(mix.centres(3-i), mix.covars(3-i), x') * mix.priors(3-i);
    plot(x, g1, '--');
    plot(x, g2, '--');
    hold off;
    
    try
        threshidx = find(g1 > g2 == false, 1, 'last');
        thresh(index) = mean(x(threshidx:threshidx + 1));
        vert_line(thresh(index), 'Color', 'r', 'LineStyle', '--');
    catch
    end
    a = axis;
    axis([a(1) a(2) a(3) max(g1+g2) * 2]);
    xlabel('log-likelihood ratio');
    ylabel('pdf');
    title(obj.Interactions.Model.states(s).title);
    drawnow;
    index = index + 1;
end
%% find aggressive events
obj.Interactions.Aggressive = cell(obj.nSubjects);
index = 1;
for s=s_
    s
    for m1=1:obj.nSubjects
        for m2=1:obj.nSubjects
            if m1==m2; continue; end;
            [beg, fin] = FindEvents(obj.Interactions.States{m1, m2} == s);
            obj.Interactions.Aggressive{m1, m2} = false(1, length(obj.Interactions.States{m1, m2}));
            likelihoodRatio = beg * 0;
            for i=1:length(beg)
                curr = obj.speed(m1, beg(i):fin(i));
                p1 = DiscreteProbability(curr, speed.hist(1, :), speed.minval, speed.maxval);
                p2 = DiscreteProbability(curr, speed.hist(2, :), speed.minval, speed.maxval);
                p1 = mean(log(p1(~isnan(p2))));
                p2 = mean(log(p2(~isnan(p2))));
                likelihoodRatio(i) = p1 - p2;
                obj.Interactions.Aggressive{m1, m2}(beg(i):fin(i)) = likelihoodRatio(i) > thresh(index);
            end
        end
    end
    index = index + 1;
end
return
%%
u = unique(likelihoodRatio_(valid));
CDF = normcdf(u, mix.centres(i), sqrt(mix.covars(i))) * mix.priors(i) + normcdf(u, mix.centres(3-i), sqrt(mix.covars(3-i))) * mix.priors(3-i);
[h,p,k,c] = kstest(likelihoodRatio_(valid), [u', CDF'])
%%
return
%%
nbins = 100;
a = randn(1, 1000);
[h, x] = hist(a, nbins);
p = h/sum(h);
[~, idx] = DiscreteProbability(a, p, min(a), max(a));
cdf = cumsum(p);
[~, s] = sort(a);
CDF = [a(s)', cdf(idx(s))'];
[h,p,k,c] = kstest(a, CDF)
%% find chases
m1=1;
eventsidx = {};
eventsidx{1} = zeros(obj.nSubjects, obj.nFrames);
eventsidx{2} = zeros(obj.nSubjects, obj.nFrames);
for m2=1:obj.nSubjects
    if m1==m2; continue; end;
    [start, finish, len] = FindEvents(obj.Interactions.States{m1, m2} > 1);
    for i=1:length(start)
        eventsidx{1}(m2, start(i):finish(i)) = i;
    end
    
    [start, finish, len] = FindEvents(obj.Interactions.States{m2, m1} > 1);
    for i=1:length(start)
        eventsidx{2}(m2, start(i):finish(i)) = i;
    end
end

chases = cell(obj.nSubjects);
index = 1;
fprintf('# - computing chases ');
for m2=1:obj.nSubjects
    nEvents = max(eventsidx{1}(m2, :));
    for i=1:nEvents
        ProgressReport(i, nEvents);
        u_ = unique(eventsidx{2}(m2, eventsidx{1}(m2, :) == i));
        u_ = u_(u_ > 0);
        for u=u_
            s1 = obj.Interactions.States{m1, m2}(eventsidx{1}(m2, :) == i);
            s2 = obj.Interactions.States{m2, m1}(eventsidx{2}(m2, :) == u);
            if ismember(5, s1) & ismember(6, s2)
                chases{m1, m2}{index} = sprintf([...
                    PPModelEventDescription(model, s1, find(eventsidx{1}(m2, :) == i, 1)) '\n-\n'...
                    PPModelEventDescription(model, s2, find(eventsidx{2}(m2, :) == u, 1))]);
                index = index + 1;
%             elseif ismember(6, s1) & ismember(5, s2)
%                 chases{m1, m2}{index} = ...
%                     PPModelEventDescription(model, s2, find(eventsidx{2}(m2, :) == i, 1));
%                 index = index + 1;
            end
        end
    end
end
fprintf('\n');
%% match events
m1=1;
eventsidx = {};
eventsidx{1} = zeros(obj.nSubjects, obj.nFrames);
eventsidx{2} = zeros(obj.nSubjects, obj.nFrames);
for m2=1:obj.nSubjects
    if m1==m2; continue; end;
    [start, finish, len] = FindEvents(obj.Interactions.States{m1, m2} > 1);
    for i=1:length(start)
        eventsidx{1}(m2, start(i):finish(i)) = i;
    end
    
    [start, finish, len] = FindEvents(obj.Interactions.States{m2, m1} > 1);
    for i=1:length(start)
        eventsidx{2}(m2, start(i):finish(i)) = i;
    end
end

eventsmap = false(1, obj.nFrames);
for m2=1:obj.nSubjects
    if m1==m2; continue; end;
    eventsmap = eventsmap | obj.Interactions.States{m1, m2} > 1;
end
[start, finish, len] = FindEvents(eventsmap);
events = cell(1, length(start));
ProgressReport();
for i=1:length(start)
    ProgressReport(i, length(start));

    events{i}.start = start(i);
    events{i}.finish = finish(i);
    events{i}.desc = '';
    
    for j=1:2
        for m2=1:obj.nSubjects
            if j==1
                a1=m1; a2=m2;
            else
                a1=m2; a2=m1;
            end
            if m1==m2; continue; end;
            u_ = unique(eventsidx{j}(m2, start(i):finish(i)));
            u_ = u_(u_ ~= 0);
            if ~isempty(u_)
                for u=u_
                    [lstart, lfinish] = FindEvents(eventsidx{j}(m2, :) == u);
                    prev = 1;
                    desc = '';
                    for f=lstart:lfinish+1
                        if f>lfinish || obj.Interactions.States{a1, a2}(f) ~= prev
                            if prev ~= 1
                                desc = sprintf([desc '%-5s (%8d - %8d)\n'], model.states(prev).title , pstart, f-1);
                            end
                            if f<=finish(i)
                                prev = obj.Interactions.States{a1, a2}(f);
                            end
                            pstart = f;
                        end
                    end
                    events{i}.desc = sprintf([events{i}.desc '-(%2d - %2d)-------------------\n' desc], a1, a2);
                end
            end
        end
    end
end
fprintf('\n');
