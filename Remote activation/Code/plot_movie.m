function plot_movie(varargin)

nargin = length(varargin);
data_index = [];
data_len   = [];
for v=1:nargin
    if (isnumeric(varargin{v}))
        data_index = [data_index, v];
        data_len   = [data_len, length(varargin{v})];
    end
end

max_len = max(data_len, 2);
dt = 1 / max_len;
s = varargin;
for t=1:max_len
    for j=1:nargin
        s{j} = varargin{data_index(j)}(1:ceil(dt * t * end));
    end
    plot(s);
    pause(0.5);
end