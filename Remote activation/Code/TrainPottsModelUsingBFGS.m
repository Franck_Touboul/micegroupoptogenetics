function TrainPottsModelUsingBFGS(obj)
%%
me = obj.Analysis.Potts.Model;
level = 2;
fun = @(w) -ComputePottsEntropy(me{level}, w);
con = @(w) PottsConstraints(me{level}, w);
fmincon(fun, randn(1, length(me{level}.weights)), [], [], [], [], [], [], con, optimset('Display', 'iter-detailed'))