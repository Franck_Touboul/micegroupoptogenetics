function SocialSetFigureProperties(obj)
[st] = dbstack('-completenames')
if length(st)>1
    set(gcf, 'Name', [st(2).name '.' obj.FilePrefix]);
else
    set(gcf, 'Name', '');
end