function [obj, s] = TrackPrintf(obj, varargin)

s = TrackSPrintf(obj, varargin);

if obj.Output
    fprintf([s '\n']);
end