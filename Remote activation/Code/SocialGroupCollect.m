function res = SocialGroupCollect(objs, fields, dim)

if ~exist('dim', 'var')
    dim = 1;
end

tests = fieldnames(objs);
res = struct();
if ischar(fields)
    field = subs(fields);
    res = setfield(res, field{end}, []);
else
    for j=1:length(fields)
        field = subs(fields{j});
        res = setfield(res, field{end}, []);
    end
end
for i=1:length(tests)
    if ischar(fields)
        field = subs(fields);
        obj = cat(dim, res.(field{end}), getfield(objs.(tests{i}), field{:}));
        res = setfield(res, field{end}, obj);
    else
        for j=1:length(fields)
            field = subs(fields{j});
            obj = cat(dim, res.(field{end}), getfield(objs.(tests{i}), field{:}));
            res = setfield(res, field{end}, obj);
        end
    end
end

function r = subs(field)
r = regexp(field,'\.','split');