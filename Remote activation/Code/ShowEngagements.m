function varargout = ShowEngagements(varargin)
% SHOWENGAGEMENTS M-file for ShowEngagements.fig
%      SHOWENGAGEMENTS, by itself, creates a new SHOWENGAGEMENTS or raises the existing
%      singleton*.
%
%      H = SHOWENGAGEMENTS returns the handle to a new SHOWENGAGEMENTS or the handle to
%      the existing singleton*.
%
%      SHOWENGAGEMENTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOWENGAGEMENTS.M with the given input arguments.
%
%      SHOWENGAGEMENTS('Property','Value',...) creates a new SHOWENGAGEMENTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ShowEngagements_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ShowEngagements_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ShowEngagements

% Last Modified by GUIDE v2.5 23-Jun-2010 18:57:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ShowEngagements_OpeningFcn, ...
                   'gui_OutputFcn',  @ShowEngagements_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

global options;
options.VLCCmd = 'C:\Program Files\VideoLAN\VLC\VLC.exe';
options.MovieFile= 'C:\Documents and Settings\USER\Desktop\Hezi\Trial     1.mpg';

% --- Executes just before ShowEngagements is made visible.
function ShowEngagements_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ShowEngagements (see VARARGIN)

% Choose default command line output for ShowEngagements
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using ShowEngagements.
% if strcmp(get(hObject,'Visible'),'off')
%     plot(rand(5));
% end

global currMice;
currMice = [];
pushMouse(1);
pushMouse(2);

global myHandels;
myHandels.main = findall(gcf, 'Tag', 'mainAxes');

% UIWAIT makes ShowEngagements wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ShowEngagements_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
global yairMatOrig1;
global yairMatOrig2;
global yairMatOrig3;
global yairMatOrig4;

Message('loading data...');
if isempty(yairMatOrig1)
    Message(sprintf('subject %d/%d', 1, 4));
    [yairMatOrig1,yairText1]=xlsread('Subject 1');
end
if isempty(yairMatOrig2)
    Message(sprintf('subject %d/%d', 2, 4));
    [yairMatOrig2,yairText2]=xlsread('Subject 2');
end
if isempty(yairMatOrig3)
    Message(sprintf('subject %d/%d', 3, 4));
    [yairMatOrig3,yairText3]=xlsread('Subject 3');
end
if isempty(yairMatOrig4)
    Message(sprintf('subject %d/%d', 4, 4));
    [yairMatOrig4,yairText4]=xlsread('Subject 4');
end
Message('');

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Message('Finding engagements...');
global currMice;
global yairMatOrig1;
global yairMatOrig2;
global yairMatOrig3;
global yairMatOrig4;
global ResultsTable;
global currEventId;

eval(['[ResultsTable, totalEvents] = findEngagements(yairMatOrig' num2str(currMice(end-1)) ', yairMatOrig' num2str(currMice(end)) ')']);

h = findall(gcf, 'Tag', 'EventList');
%get(h, 'String')

global Events;
eventNames = {};
for i=1:length(totalEvents)
    eventNames{i} = sprintf('%-20s (%.1f-%.1f)', ResultsTable{i+1,2}, ResultsTable{i+1,3}, ...
        ResultsTable{i+1,3} + size(totalEvents{i}{2}, 2) * 0.04);
    Events{i}.type = ResultsTable{i+1,2};
    Events{i}.startTime =  ResultsTable{i+1,3};
    Events{i}.endTime = ResultsTable{i+1,3} + size(totalEvents{i}{2}, 2) * 0.04;
    Events{i}.total = totalEvents{i};
end
set(h, 'String', eventNames);
currEventId = -1;

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});


% --- Executes on selection change in EventList.
function EventList_Callback(hObject, eventdata, handles)
% hObject    handle to EventList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns EventList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from EventList
global options;
global Events;
global currEventId;
global video;
global myHandels;

options.VLCCmd = '"C:\Program Files\VideoLAN\VLC\VLC.exe"';
%options.MovieFile= '"C:\Documents and Settings\USER\Desktop\Hezi\Trial     1.mpg"';
options.MovieFile= 'C:\Documents and Settings\USER\Desktop\Hezi\Trial     1.mpg';

%system([options.VLCCmd  ' ' options.MovieFile ' --start-time ' num2str(Events{get(hObject, 'Value')}.startTime)]);
id = get(hObject, 'Value');
currEvent = Events{id};
if id ~= currEventId
    hmsg = waitbar(0,'Please wait...');
    video = mmread(options.MovieFile,[],[currEvent.startTime-1 currEvent.endTime+1]);
    close(hmsg);
    currEventId = id;
end
axes(myHandels.main)
for i=1:length(video.frames)
    imshow(video.frames(i).cdata);
    a = axis;
    if i == 1
        w = a(2)-a(1);
        h = a(4)-a(3);
        text(a(1) + w / 14, a(3) + h / 14, 'Before', 'Color', 'w');
    end
    if currEvent.endTime < video.times(i)
        text(a(1) + w / 14, a(3) + h / 14, 'After', 'Color', 'w');
    elseif currEvent.startTime < video.times(i)
        text(a(1) + w / 14, a(3) + h / 14, 'Engagement', 'Color', 'w');
    else
        text(a(1) + w / 14, a(3) + h / 14, 'Before', 'Color', 'w');
    end
    drawnow;
    pause(0.05);
end
%disp 'a';

% --- Executes during object creation, after setting all properties.
function EventList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EventList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
if get(hObject,'Value') 
    pushMouse(1);
end

% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2
if get(hObject,'Value') 
    pushMouse(2);
end

% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3
if get(hObject,'Value') 
    pushMouse(3);
end

% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4
if get(hObject,'Value') 
    pushMouse(4);
end

function pushMouse(num)
global currMice;
currMice = [currMice, num];
h = findall(gcf, 'Tag', ['checkbox' num2str(num)]);
set(h, 'Value', 1);
if length(currMice) > 2
    remove = currMice(1:end-2);
    for r=remove
        if r~=num
            h = findall(gcf, 'Tag', ['checkbox' num2str(r)]);
            set(h, 'Value', 0);
        end
    end
    currMice = currMice(end-1:end);
end



function MessageBox_Callback(hObject, eventdata, handles)
% hObject    handle to MessageBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MessageBox as text
%        str2double(get(hObject,'String')) returns contents of MessageBox as a double


% --- Executes during object creation, after setting all properties.
function MessageBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MessageBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Message(str)
h = findall(gcf, 'Tag', 'MessageBox');
full = get(h, 'String');
set(h, 'String', str);
if ~isempty(str)
    fprintf(['# ' str '\n']);
end
