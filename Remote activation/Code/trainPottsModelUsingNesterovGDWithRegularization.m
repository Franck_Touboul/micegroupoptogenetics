function me = trainPottsModelUsingNesterovGDWithRegularization(options, data, confidence)
options.param_report = false;
options = setDefaultParameters(options, ...
    'Statistics', false, ...
    'beta', 0.01, ...
    'tk', 1, 'nIters', 20000);

me.tk = options.tk;
me.alpha = .5;
me.beta = .5;
useLineSearch = true;
me.LineSearchMaxSteps = 30;
me.KLConvergenceRatio = 1e-4;
me.KLConvergenceRatio = -1;

options.NeutralZone = 1; % Open
%%
use_sparse = true;
if nargin < 3
    confidence = -.05;
end
me.confidence = confidence;
me.neutralZone = options.NeutralZone;

if isfield(options, 'Output')
    output = options.Output;
else
    output = true;
end
%
if output; fprintf('# Training Potts Model\n'); end
%
if output; fprintf('# - finding all permutation of max dimension %d...\n', options.n); end;
%%
allPerms = nchoose(1:options.nSubjects);
nPerms = {};
j = 1;
for i=1:length(allPerms)
    if length(allPerms{i}) <= options.n 
        nPerms{j} = allPerms{i};
        j = j + 1;
    end
end

%%
if output; fprintf('# - computing constraints...\n'); end
[me.constraints, pci] = PottsComputeConstraints(data' + 1, options.count, nPerms, abs(confidence));
C = length(nPerms);
%
me.inputPerms = myPerms(options.nSubjects, options.count);
[me.perms, me.labels] = assignFeature(me.inputPerms, options.count, nPerms, use_sparse);

valid = false(1, length(me.labels));
if options.count == 2
    for i=1:length(me.labels)
        if all(me.labels{i}(2, :) ~= 2)
            valid(i) = true;
        end
    end
    me.labels = me.labels(valid);
    me.perms = me.perms(:, valid);
    me.constraints = me.constraints(valid);
    pci = pci(valid);
end
%
me.nSubjects = options.nSubjects;
me.range = options.count;
me.weights = randn(1, size(me.labels, 2)) / size(me.labels, 2);
orig = me;
%% filter neutral zone
neutral = false(1, length(orig.labels));
for l=1:length(me.labels)
    neutral(l) = any(orig.labels{l}(2, :) == options.NeutralZone);
end
me.constraints = orig.constraints(~neutral);
me.perms = orig.perms(:, ~neutral);
me.labels = orig.labels(~neutral);
% initialize the weights
if isfield(options, 'initialPottsWeights') && ~isempty(options.initialPottsWeights)
    me.weights = options.initialPottsWeights;
else
    me.weights = orig.weights(~neutral);
end
%
me.momentum = me.weights;
pci = pci(~neutral, :);

%% compute empirical probabilities
base = me.range;
baseVector = base.^(size(data, 1)-1:-1:0);
pEmp = histc(baseVector * data+1, 1:base ^ size(data, 1));
pEmp = pEmp / sum(pEmp);
%% initial halfKL
p = exp(me.perms * me.momentum');
Z = sum(p);
p = p / Z;
kl2 = -pEmp * log2(p);
kl1 = pEmp * log2(pEmp' + (pEmp' == 0));
%%
if options.Statistics; me.Statistics = struct(); end
tic;
me.converged = false;
me.nconverged = inf;
me.KLconverged = false;
best = me;

for iter=1:options.nIters
    if output; fprintf('# - iter (%4d/%4d) : ', iter, options.nIters); end
    % compute the (un-normalized) pdf for each sample
    p = exp(me.perms * me.momentum');
    perms_p = p;
    % normalize the pdf (the Z)
    Z = sum(p);
    p = p / Z;
    prev_kl = kl1 + kl2 + options.beta * sum(abs(me.weights));
    kl2 = -pEmp * log2(p + (pEmp' == 0));
    me.Dkl = kl1 + kl2 + options.beta * sum(abs(me.weights));
    E = full(sum(me.perms .* repmat(p, 1, size(me.perms, 2))));
    if confidence ~= 0
        %[sum(E'  > pci(:, 1) & E' < pci(:, 2)) (1 - confidence) * length(me.weights)]
        nconverged = sum(E'  > pci(:, 1) & E' < pci(:, 2));
        if output; fprintf('KL = %6.4f, convergence = %5.2f%%\n', kl1 + kl2 + options.beta * sum(abs(me.weights)), (nconverged / length(me.weights) * 100)); end
        if options.Statistics;
            me.Statistics(iter).convergence = nconverged / length(me.weights);
        end
        me.nconverged = nconverged;
        if nconverged == length(me.weights) && iter >= options.MinNumberOfIters 
            if output; 
                fprintf('# converged to confidence interval (alpha = %3.2f)\n', confidence); 
            end
            me.converged = true;
            best = me;
            break;
        end
        if me.nconverged > best.nconverged
            best = me;
            best.nIters = iter;
            best.Dkl = kl1 + kl2 + options.beta * sum(abs(me.weights));
        end
    else
        if output; fprintf('KL = %6.4f (%.3f)\n', kl1 + kl2 + options.beta * sum(abs(me.weights)), abs((kl1 + kl2 + options.beta * sum(abs(me.weights)) - prev_kl)/prev_kl)); end
    end
    %%
    if me.KLConvergenceRatio > 0
        if abs((kl1 + kl2 + options.beta * sum(abs(me.weights))) - prev_kl) / prev_kl <= me.KLConvergenceRatio
            if me.KLconverged
                if output;
                    fprintf('# converged to KL convergence ratio (ratio = %3.2f)\n', me.KLConvergenceRatio);
                end
                break;
            else
                me.KLconverged = true;
            end
        elseif me.KLconverged
            me.KLconverged = false;
        end
    end
    %%
    t = me.tk;
    pWeights = me.weights;
    if useLineSearch
        df = (me.constraints - E);
        found = false;
        nsteps = 1;
        while ~found
            lsWeights = me.momentum + t * df;
            lsMomentum = lsWeights + (iter - 1)/(iter+2) * (lsWeights - pWeights);
            pt = exp(me.perms * (lsMomentum + t * df)');
            pt = pt / sum(pt);
            found = -pEmp * log2(pt) + options.beta * sum(abs(lsWeights)) < kl2 + options.beta * sum(abs(me.weights)) - me.alpha * t * sum(df.^2);
            if ~found
                t = me.beta * t;
            end
            if nsteps >= me.LineSearchMaxSteps
                break;
            end
            nsteps = nsteps + 1;
        end
    end
    
    me.weights = me.momentum + t * (me.constraints - E + options.beta * sum(sign(me.weights)));
    me.momentum = me.weights + (iter - 1)/(iter+2) * (me.weights - pWeights);

    if confidence == 0
        best = me;
    end
    
    %%
    if options.Statistics;
        me.Statistics(iter).times = toc; 
        me.Statistics(iter).KL = kl1 + kl2 + options.beta * sum(abs(me.weights));
    end
end
me = best;
me.nIters = iter;
me.Dkl = kl1 + kl2 + options.beta * sum(abs(me.weights));
% if me.nconverged < best.nconverged || (me.nconverged == best.nconverged && me.Dkl > best.Dkl)
%     me = best;
% end

%me = rmfield(me, 'features');
