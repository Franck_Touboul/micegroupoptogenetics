options.order = 3;
me = obj.Analysis.Potts.Model{options.order};
res.subjectmap = false(obj.nSubjects, length(me.weights));
res.zonemap = false(obj.nSubjects * obj.ROI.nZones, length(me.weights));
for i=1:length(me.weights)
    idx = 1;
    for j=me.labels{i}(1, :)
        res.subjectmap(j, i) = true;
        res.zonemap((j - 1) * obj.ROI.nZones + me.labels{i}(2, idx), i) = true;
        idx = idx + 1;
    end
end
vec = 2.^(0:obj.nSubjects * obj.ROI.nZones-1);
res.zonecode = res.zonemap' * vec';
%% histogram
clf
cmap = MyCategoricalColormap;
for o=1:3
    [q1, q1, hw, hh] = SquareSubplpot(obj.nSubjects, o);
    w = [];
    for s=1:obj.nSubjects
        map = res.subjectmap(s, :) & me.order == o;
        w = [w, me.weights(map)];
    end
    [h, x] = hist(w, .5*sqrt(length(w)));
    for s=1:obj.nSubjects
        map = res.subjectmap(s, :) & me.order == o;
        h = histc(me.weights(map), x);
        plot(x, h/sum(h), 'color', cmap(obj.Hierarchy.Group.AggressiveChase.rank(s), :)); hon;
    end
end
%TieAxis(hw, hh, 1:obj.nSubjects);
%% weights
clf
cmap = MyCategoricalColormap;
clf
for s=1:obj.nSubjects
    [q1, q1, hw, hh] = SquareSubplpot(obj.nSubjects, s);
    PVAL = [];
    for o=1:3
        w = [];
        map = res.subjectmap(s, :) & me.order == o;
        idx = find(map);
        for i=idx
            others = find(~res.subjectmap(:, i));
            pattern = res.zonemap(:, i);
            off1 = (s - 1) * obj.ROI.nZones;
            for j=others'
                off2 = (j - 1) * obj.ROI.nZones;
                curr = pattern;
                curr(off2+1:off2+10) = pattern(off1+1:off1+10);
                curr(off1+1:off1+10) = 0;
                code = curr' * vec';
                w = [w; [me.weights(i), me.weights(res.zonecode == code)]];
            end
        end
        [q1, q1, hw, hh] = SquareSubplpot(obj.nSubjects, s);
        p = polyfit(w(:,1),w(:,2), 1);
        [crr, pval] = corr(w(:,1),w(:,2));
        PVAL = [PVAL, sprintf('(%.2f, %.2f)', crr, pval)];
        plot(w(:, 1), w(:, 2), '.', 'color', cmap(o, :));
        x = -15:15;
        plot(x, polyval(p, x), '-', 'color', cmap(o, :));
        title(['rank: ' num2str(obj.Hierarchy.Group.AggressiveChase.rank(s)) ', pval=' PVAL]);
        hon;
    end
end
TieAxis(hw, hh, 1:obj.nSubjects);
%% properties
clf
d = zeros(obj.nSubjects);
[q, S] = sort(obj.Hierarchy.Group.AggressiveChase.rank);
weights = abs(me.weights);
for o=1:options.order
    entries = {};
    stat = [];
    for i=1:obj.nSubjects
        s = S(i);
        map = me.order == o  & res.subjectmap(s, :);
        entries{i} = sprintf('%d (%d)', s, q(i));
        stat.mean(i) = mean(weights(map));
        stat.stderr(i) = stderr(weights(map));
    end
    SquareSubplpot(obj.nSubjects, o);
    MyBarWeb(1:obj.nSubjects, stat.mean, stat.stderr, cmap);
    set(gca, 'XTickLabel', q);
end
for i=1:obj.nSubjects
    s = S(i);
    map = res.subjectmap(s, :);
    entries{i} = sprintf('%d (%d)', s, q(i));
    stat.mean(i) = mean(weights(map));
    stat.stderr(i) = stderr(weights(map));
end
SquareSubplpot(obj.nSubjects, 4);
MyBarWeb(1:obj.nSubjects, stat.mean, stat.stderr, cmap);


%% pairwise correlations by order
clf
d = zeros(obj.nSubjects);
[q, S] = sort(obj.Hierarchy.Group.AggressiveChase.rank);

entries = {};
for i1=1:obj.nSubjects
    s1 = S(i1);
    entries{i1} = sprintf('%d (%d)', s1, q(i1));
    for i2=1:obj.nSubjects
        s2 = S(i2);
        map = me.order == 2 & res.subjectmap(s1, :) & res.subjectmap(s2, :);
        w = me.weights(map);
        d(i1, i2) = mean(w);
    end
end
MyImagesc(d, true)

set(gca, 'XTick', 1:obj.nSubjects, 'XTickLabel', entries);
%% pairwise correlations by zone
clf
d = zeros(obj.nSubjects, obj.ROI.nZones);
[q, S] = sort(obj.Hierarchy.Group.AggressiveChase.rank);
for i1=1:obj.nSubjects
    s1 = S(i1);
    for z=1:obj.ROI.nZones
        map = me.order == 2 & res.zonemap((s1 - 1) * obj.ROI.nZones + z);
    end
end
