function PottsTargetFunction(model, weights)

p = exp(model.perms * weights');
perms_p = exp(model.perms * weights');
Z = sum(perms_p);
p = p / Z;
Entropy = -p' * log2(p);