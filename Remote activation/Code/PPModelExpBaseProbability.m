function p = PPModelExpBaseProbability(x, model, state)

alpha = abs(model.states(state).ExpBase.alpha);
m = abs(model.states(state).ExpBase.m);
input = abs(model.states(state).ExpBase.theta - x(1, :));

p = halfCircularUniformExp(input, alpha, m);
