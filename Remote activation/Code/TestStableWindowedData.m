function TestStableWindowedData(obj)
rand('twister',sum(100*clock));
randn('seed',sum(100*clock));
%%
r = randperm(8);
r = r(1:4);
s = randi(4, [1 4]);
data = [];
data.obj = [];
data.zones = [];
data.id = r;
data.subjs = s;
for i=1:4
    obj = TrackLoad({10 + r(i) 2:4}, {'common'});
    %%
    z = obj.zones(s(i), :);
    if i == 1
        %%
        data.obj = obj;
        data.zones = z;
    else
        %%
        if obj.nFrames > size(data.zones, 2)
            data.zones(:, size(data.zones, 2)+1:obj.nFrames) = nan;
        else
            z(:, obj.nFrames + 1:size(data.zones, 2)) = nan;
        end
        data.zones = [data.zones; z];
    end    
end
obj = [];
%%
data.obj.valid = all(~isnan(data.zones));
data.obj.zones = data.zones;
data.obj.nFrames = length(data.obj.valid);
data.obj.OutputToFile = false;
data.obj.OutputInOldFormat = false;
%%
data.obj = SocialSetTimeScale(data.obj, 6);
data.obj = SocialPotts(data.obj);

%res(iter).Ik = curr.Analysis.Potts.Ik';
fprintf('@IkSrc=');
fprintf(' %.2f', data.obj.Analysis.Potts.Ik);
fprintf('\n');

fprintf('@IDS=');
fprintf(' %d', data.id);
fprintf('\n');
fprintf('@subjs=');
fprintf(' %d', data.subjs);
fprintf('\n');

% %%
% stat.obj = {};
% %
% for idx=1:length(stat.bkgobj)
%     %%
%     stat.obj{idx} = TrackLoad(stat.bkgobj(idx));
% end
