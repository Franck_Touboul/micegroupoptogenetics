function TrackFinalize(nruns, varargin)
fprintf('# finalizing...\n');
TrackDefaults;
%%
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nFrames = xyloObj.NumberOfFrames;
dt = 1/xyloObj.FrameRate;
%%
fprintf('# - reading paths ');
if nargin > 0
    res = cell(1, options.nSubjects);
    for j=1:options.nSubjects
        res{j}.x    = zeros(1, nFrames, 'single');
        res{j}.y    = zeros(1, nFrames, 'single');
        res{j}.prob = zeros(1, nFrames, 'single');
        res{j}.id   = zeros(1, nFrames, 'uint8');
    end
    nchars = 0;
    from = 1;
    for i=1:nruns
        nchars = reprintf(nchars, '(%3d/%3d)', i, nruns);
        filename = sprintf([options.output_path options.test.name '.track.%03d.mat'], i);
        currTrack = load(filename);
        to = from + length(currTrack.res{j}.x) - 1;
        for j=1:options.nSubjects
            %res{j} = structcat(res{j}, currTrack.res{j}, 2);
            res{j}.x(from:to) = currTrack.res{j}.x;
            res{j}.y(from:to) = currTrack.res{j}.y;
            res{j}.prob(from:to) = currTrack.res{j}.prob;
            res{j}.id(from:to) = currTrack.res{j}.id;
        end
        from = to + 1;
    end
else
    filename = [options.output_path options.test.name '.track.mat'];
    load(filename);
end
fprintf('\n');

%%
fprintf('# - reading metadata\n');
filename = [options.output_path options.test.name '.meta.mat'];
load(filename);
%%
social = struct('nSubjects', 0, 'nData', 0, 'x', [], 'y', [], 'time', [], 'dt', 0, ...
    'options', struct(), 'meta', struct(), 'colors', [], 'roi', struct(), 'zones', struct());
%
fprintf('# - interpolate position\n');
for curr = 1:options.nSubjects;
    hidden = isnan(res{curr}.x);
    d = diff([0 hidden 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    loc = find(start > 1 & finish < length(hidden));
    for l=loc
        range = start(l):finish(l);
%         res{curr}.x(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.x([start(l)-1, finish(l)+1]), start(l):finish(l));
%         res{curr}.y(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.y([start(l)-1, finish(l)+1]), start(l):finish(l));
        [res{curr}.x(range), res{curr}.y(range)] = MotionInterp(res{curr}.x, res{curr}.y, range, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
    end
    if length(finish) > 0 && finish(end) == length(hidden) && start(end) > 1
        res{curr}.x(start(end):finish(end)) = res{curr}.x(start(end)-1);
        res{curr}.y(start(end):finish(end)) = res{curr}.y(start(end)-1);
    end
    if length(finish) > 0 && finish(1) < length(hidden) && start(1) == 1
        res{curr}.x(start(1):finish(1)) = res{curr}.x(finish(1)+1);
        res{curr}.y(start(1):finish(1)) = res{curr}.y(finish(1)+1);
    end
end
%
fprintf('# - finding paths\n');
social.options = options;
social.dt = dt;
social.x = zeros(length(res), length(res{1}.x), 'single');
social.y = zeros(length(res), length(res{1}.x), 'single');
for i=1:length(res)
    social.x(i, :) = res{i}.x / options.scale;
    social.y(i, :) = res{i}.y / options.scale;
    % to save memory:
    res{i}.x = [];
    res{i}.y = [];    
end
%
% fprintf('# - finding dark zones\n');
% grayBkgFrame = rgb2gray(meta.bkgFrame);
% grayBkgFrameEq = im2double(histeq(grayBkgFrame)) > .4;
% se = strel('disk',4);
% se2 = strel('disk',15);
% hiddenMask = imclose(imopen(grayBkgFrameEq, se), se2);
%
fprintf('# - organizing results\n');
%clear('res');
social.nData = size(social.x, 2);
social.time = (1:social.nData) *  dt;
% zones.labyrinth = false(size(social.x));
% zones.smallNest = false(size(social.x));
% zones.bigNest = false(size(social.x));
%orig = social;
%
for i=1:options.nSubjects
    x = round(social.x(i, :)); x(x == 0) = 1;
    y = round(social.y(i, :)); y(y == 0) = 1;
    %
    startnan = find(isnan(x), 1); 
    if startnan > 1
        x(startnan:end) = x(startnan - 1);
        y(startnan:end) = y(startnan - 1);
    else
        x(startnan:end) = 1;
        y(startnan:end) = 1;
    end
    %
    %ind = sub2ind(size(hiddenMask), y, x);
    %zones.dark(i, :) = hiddenMask(ind);
    
    zones.hidden(i, :) = res{i}.id == 0;
end
%
% scaledCents = cents;
% scaledCents.x = cents.x / options.scale;
% scaledCents.y = cents.y / options.scale;
% scaledCents.area = cents.area / options.scale^2;

%social.cents = scaledCents;
social.zones = zones;
social.meta = meta;
social.colors = social.meta.subject.centerColors;
%social.meta.hiddenMask = hiddenMask;
social.nSubjects = options.nSubjects;

%%
fprintf('# - computing kalman filter\n');
if isfield(options, 'kalman')
	social = kalmanFilterTrack(social, options.kalman.q, options.kalman.r);
end

%%
filename = [options.output_path options.test.name '.social.mat'];
fprintf('# - saving data\n');
save(filename, 'social', '-v7.3');

if (isfield(options, 'loadROI') && options.loadROI) || (isfield(options, 'ROIFile') && ~isempty(options.ROIFile))
    TrackRegions;
end

filename = [regexprep(options.MovieFile, '\.[^\.]*$', '') '.mat'];
saveSocialForTracking(filename, social);

%%
return;
fprintf('# - saving data to text file\n');
socialText = double(social.time');
for i=1:social.nSubjects
    socialText = [socialText, double(social.x(i, :)'), double(social.y(i, :)')];
end
filename = [options.output_path options.test.name '.social.txt'];
save(filename, 'socialText', '-ascii');