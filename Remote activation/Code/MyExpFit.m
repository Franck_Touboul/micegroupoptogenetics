function Estimates = MyExpFit(x, y)


Starting = rand(1,3);
options = optimset('Display','off');
Estimates = fminsearch(@myfit, Starting, options, x, y);


function sse = myfit(params, Input, Actual_Output)
a = params(1);
b = params(2);
lamda = params(3);
Fitted_Curve = a .* exp(-lamda * Input) + b;
Error_Vector = Fitted_Curve - Actual_Output;
% When curvefitting, a typical quantity to
% minimize is the sum of squares error
sse = sum(Error_Vector.^2);
% You could also write sse as
% sse=Error_Vector(:)'*Error_Vector(:);