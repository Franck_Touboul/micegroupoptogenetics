%% parameters
niters = 20;
cmd = 'NeuralNetwork.exe simulateToMatlab=false corrTime=50 nIters=30'

%%
p = {};

p{1}.title = 'Noise Standard Deviation';
p{1}.name = 'noiseSTD';
p{1}.value = {0.01 0.02 0.04 0.08 0.16 0.32};
p{1}.inf = 0;

%p{2}.title = 'Adaptive Noise Ratio';
%p{2}.name = 'adaptiveNoiseFactor';
%p{2}.value = {0.01 0.02 0.04 0.08 0.16 0.32};
%p{2}.inf = 0;

% p{2}.title = 'Average Connectivity';
% p{2}.name = 'connectivity';
% p{2}.value = {3 6 12 24 48 100000};
% p{2}.inf = 1;
% 
% p{3}.title = 'Runing Average Timescale';
% p{3}.name = 'timeScale';
% p{3}.value = {32 64 128 256 512 1024 100000};
% p{3}.inf = 1;

%% program
s = {};
s = parametrize(s, p, 1);

totalProb = {};

for i=1:length(s)
    curr_cmd = cmd;
    for j=1:length(p)
        curr_cmd = [curr_cmd ' ' p{j}.name '=' num2str(s{i}(j))];
    end
    for iter=1:niters
        fprintf('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n');
        fprintf('param set no. %d/%d,  iter %d:\n', i, length(s), iter);
        fprintf('%s\n', curr_cmd);
        fprintf('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n');
        system(curr_cmd);
        totalProb{i}{iter} = Prob_;
    end
end

return;



param_ = [0 1];
%param_ = 0;
nIters = 100;
%wstruct = {};
i = 1;
for p = param_
    j = 1;
    for iter=1:nIters
        system(['NeuralNetwork.exe noiseSTD=0.06 simulateToMatlab=false corrTime=50 nIters=20 trainFromLayer=', num2str(p)]);
        %         wstruct{iter} = [];
        %         for m=1:200
        %             wstruct{iter}=[wstruct{iter}, finalStructure_.layer{2}.neuron{m}.weights];
        %         end
        totalProb{j, i} = Prob_;
        j = j + 1;
    end
    i = i + 1;
end