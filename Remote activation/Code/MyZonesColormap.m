function cmap = MyZonesColormap(nZones)
if nargin == 0
    nZones = 10;
end
if 2==3;
    cmap = [
        53 178 87;
        51,102,51;
        255 153 51;
        234 165 193;
        255 153 0;
        0 177 229;
        179 213 157;
        159,0,32;
        148 216 239;
        245,91,121;
        ] / 255;
end

cmap = [
        220 73 89; % open
        53 178 87; % feeder 1
        51,102,51; % feeder 2
        179 213 157; % water
        204,102,51; % small-nest
        200 200 200; % (small-nest)
        168,39,76; % Z
        255 153 51; % (big nest)
        230 230 230;
        255 204 153;
] / 255;

if 1==2
    cmap = [
        98 111 179;
        255 153 51;
        255 204 153;
        220 73 89;
        53 178 87;
        179 213 157;
        234 165 193;
        0 177 229;
        148 216 239;
        189 190 200;
        ]/256;
end
% cmap = [186,80,110; % Open
%     121,181,172;
%     159,214,210;
%     208,236,234;
%     139,122,94;
%     194,192,177; % (Small Nest)
%     182,136,141; % Labyrinth
%     175,151,114;
%     227,224,211; % (Big Nest)
%     100,74,49;
%     255 255 255;
%     ]/255;

cmap = cmap(1:nZones, :);

return
cmap = [    189 190 200;
    124,216,255;
    0,180,255;
    0,116,192;
    179 213 157;
    53 178 87;
    220 73 89;
    240 191 148;
    %240 145 55; %
    178 211 155;
    98 111 179;
    246 237 170;
    248 231 83;
    ]/256;
% cmap = [51 51 51;  % Open
%     96 120 72; % Feeder 1
%     120 144 72; % Feeder 2
%     192 216 97; % Water
%     239 65 65; % Small-Nest
% cmap = [51 51 51;  % Open
%     96 120 72; % Feeder 1
%     120 144 72; % Feeder 2
%     192 216 97; % Water
%     239 65 65; % Small-Nest
%     27 127 195; % (Small-Nest)
%     86 99 113;  % Labyrinth
%     131 35 58; % Big-Nest
%     250 250 250; % (Big-Nest)
%     244 118 66; % Block
%     ] / 255;
%    17 66 99; % (Big-Nest)
