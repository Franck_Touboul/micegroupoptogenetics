function m = medianFrame(data,width,height,frameNr,time)
persistent all;
if nargout > 0
    m = all;
    return;
end
if nargin == 0
    all = uint8([]);
    return;
end
all(:, :, :, frameNr) = data;
