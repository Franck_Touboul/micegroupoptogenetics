function SocialPlay(obj)
dt = 125;
DT = obj.FrameRate * 60 * 15;
s=1;
t0 = find(~isnan(sum(obj.x)), 1);
cmap = MyMouseColormap;
clf;
try
    first = true;
    for t=t0:dt:obj.nFrames
        if mod(t, DT) < dt
            clf;
            first = true;
        end
        for s=1:obj.nSubjects
            plot(obj.x(s, t:t+dt-1), obj.VideoHeight - obj.y(s, t:t+dt-1), 'Color', cmap(s, :));
        end
        if first
            hold on;
            axis([0 obj.VideoWidth 0 obj.VideoHeight]);
            first = false;
        end
        
        drawnow;
        title(sec2time(t * obj.dt));
    end
    hold off;
catch
    hold off;
end
