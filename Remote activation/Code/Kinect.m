classdef Kinect
    properties
        DepthFilename = '';
        ColorFilename = '';
        Frame = [];
        FrameNumber = 0;
        LeftMargin = 0;
        RightMargin = 0;
        TopMargin = 0;
        BottomMargin = 0;
        VideoWidth = 0;
        VideoHeight = 0;
        NumberOfFrames = 0;
        ChannelsOffset = 0;
        X = [];
        Y = [];
        Calibration = [];
        Meta = [];
        %
        Bkg = [];
    end
    
    properties (GetAccess = 'private', SetAccess = 'private')
        FrameNumber_ = 0;
        ColorFile_ = 0;
        DepthFile_ = 0;
        
        CM2BYTES = 8000;
    end
    
    methods
        function value = get.NumberOfFrames(obj)
            value = obj.ColorFile_.NumberOfFrames()-abs(obj.ChannelsOffset);
        end
        function obj = set.FrameNumber(obj, num)
            obj = obj.SetFrame(num);
        end
        function value = get.FrameNumber(obj)
            value = obj.FrameNumber_;
        end
        
    end
    
    methods
        
        %% constructor for the Kinect object
        %% usage: Kinect(colorFilename)
        %%              For example, if colorFilename is 'd:\temp\file.1.avi', assumes
        %%              that depthFilename = 'd:\temp\file.1.avi' and 
        %%              metaFilename = 'd:\temp\file.xml' and 
        %%        Kinect(colorFilename, depthFilename, metaFilename)
        %%              explicitly specify all associated files
        function obj = Kinect(varargin)
            if nargin > 0
                obj = obj.SetVideoFiles(varargin{:});
            end
        end

        %% load Kinect object
        %% usage: Load(filename)
        function obj = Load(obj, filename)
            newobj = Kinect();
            f = properties(obj);
            f = f(~strcmp(f, 'FrameNumber') & ~strcmp(f, 'NumberOfFrames'));
            for i=1:length(f)
                if isprop(newobj, f{i})
                    obj.(f{i}) = newobj.(f{i});
                end
            end
            temp = load(filename, '-mat');
            f = fieldnames(temp);
            f = f(~strcmp(f, 'FrameNumber') & ~strcmp(f, 'NumberOfFrames'));
            for i=1:length(f)
                obj.(f{i}) = temp.(f{i});
            end
        end
        
        %% save Kinect object
        %% usage: Save(filename)
        function obj = Save(obj, filename)
            f = properties(obj);
            f = f(~strcmp(f, 'FrameNumber') & ~strcmp(f, 'NumberOfFrames') & ~strcmp(f, 'Frame'));
            for i=1:length(f)
                temp.(f{i}) = obj.(f{i}); %#ok<STRNU>
            end
            save(filename, '-struct', 'temp', f{:}, '-v7.3');
        end
        
        %% destructor
        function delete(obj)
            close(obj.ColorFile_);
            close(obj.DepthFile_);
        end
        
        function obj = SetVideoFiles(obj, colorFilename, depthFilename, metaFilename)
            if nargin > 0
                obj.ColorFilename = colorFilename;
                obj.ColorFile_ = VideoReader(obj.ColorFilename);
                if nargin > 2
                    obj.DepthFilename = depthFilename;
                else
                    [pathstr, name, ext] = fileparts(obj.ColorFilename);
                    obj.DepthFilename = [pathstr filesep name '.depth' ext];
                end
                if nargin < 4
                    [pathstr, name, ext] = fileparts(obj.ColorFilename);
                    metaFilename = [pathstr filesep regexprep(name, '\.[0-9]*$', '') '.xml'];
                end
                
                obj.DepthFile_ = VideoReader(obj.DepthFilename);
                obj = obj.ReadMetaFile(metaFilename);
            end
        end
        
        %% read meta data file
        %% curently the meta data files holds Crop information
        function obj = ReadMetaFile(obj, filename)
            metaxml = xmlread(filename);
            try
                CropItems = metaxml.getElementsByTagName('Crop');
                for k = 0:CropItems.getLength-1
                    thisListitem = CropItems.item(k);
                    fields = {'Left', 'Right', 'Top', 'Bottom'};
                    for i=1:length(fields)
                        thisList = thisListitem.getElementsByTagName(fields{i});
                        thisElement = thisList.item(0);
                        val = str2num(thisElement.getFirstChild.getData);
                        obj.([fields{i} 'Margin']) = val;
                    end
                    fields = {'VideoWidth', 'VideoHeight'};
                    for i=1:length(fields)
                        thisList = thisListitem.getElementsByTagName(fields{i});
                        thisElement = thisList.item(0);
                        val = str2num(thisElement.getFirstChild.getData);
                        obj.([fields{i}]) = val;
                    end
                end
                if obj.LeftMargin == 0 && obj.RightMargin == 0
                    x = 1:obj.VideoWidth;
                else
                    x = obj.LeftMargin+1:obj.RightMargin;
                end
                if obj.TopMargin == 0 && obj.BottomMargin == 0
                    y = 1:obj.VideoHeight;
                else
                    y = obj.TopMargin+1:obj.BottomMargin;
                end
                [obj.X, obj.Y] = meshgrid(x, y);
            catch me
                warning me.message
            end
        end
        
        %%
        function obj = SetFrame(obj, num)
            if obj.ColorFile_ == 0 || obj.DepthFile_ == 0
                error
            end
            if nargin < 2
                num = randi(obj.DepthFile_.NumberOfFrames);
            end
            if obj.ChannelsOffset < 0
                frame.Depth = RGB2Depth(read(obj.DepthFile_, num-obj.ChannelsOffset));
                frame.Color = im2double(read(obj.ColorFile_, num));
            else
                frame.Depth = RGB2Depth(read(obj.DepthFile_, num));
                frame.Color = im2double(read(obj.ColorFile_, num+obj.ChannelsOffset));
            end
            
            obj.Frame = frame;
            obj.FrameNumber_ = num;
        end
        
        %%
        function [obj, n] = NextFrame(obj)
            n = obj.FrameNumber + 1;
            if n >= obj.NumberOfFrames
                n = 0;
            else
                obj = obj.SetFrame(n);
            end
        end
        
        %%
        function [xR, yR, zR] = GetRealWorldCoords(obj)
            if isempty(obj.Calibration)
                obj.Calibration = load('Default.calib', '-mat');
                [xR,yR,zR] = KinectSensorMapDepthToSkeleton(obj.Calibration.p, obj.X(:), obj.Y(:), 1000 * obj.Frame.Depth(:));
                xR = reshape(xR, size(obj.X));
                yR = reshape(yR, size(obj.X));
                zR = reshape(zR, size(obj.X));
            end
        end
        
        %%
        function [obj, xR, yR, zR] = GetRectifiedRealWorldCoords(obj)
            if isempty(obj.Calibration)
                obj.Calibration = load('Default.calib', '-mat');
                [xR,yR,zR] = KinectSensorMapDepthToSkeleton(obj.Calibration.p, obj.X(:), obj.Y(:), 1000 * obj.Frame.Depth(:));
                obj.Calibration.b = robustfit([xR(:), yR(:)], zR(:));
            else
                [xR,yR,zR] = KinectSensorMapDepthToSkeleton(obj.Calibration.p, obj.X(:), obj.Y(:), 1000 * obj.Frame.Depth(:));
            end
            zR = zR - obj.Calibration.b(2) * xR - obj.Calibration.b(3)*yR;
            xR = reshape(xR, size(obj.X));
            yR = reshape(yR, size(obj.X));
            zR = reshape(zR, size(obj.X));
        end
        
        %%
        function obj = LoadCalibrationData(obj, filename)
            obj.Calibration = load(filename, '-mat');
        end
        
        %%
        function DepthImage = GetDepthImage(obj)
            DepthImage = zeros([size(obj.Frame.Depth) 3], 'double');
            vals = obj.Frame.Depth(~isnan(obj.Frame.Depth) & obj.Frame.Depth > 1);
            minv = min(vals);
            maxv = max(vals);
            cmap = jet(256);
            for i=1:size(DepthImage, 1)
                for j=1:size(DepthImage, 2)
                    v = round((obj.Frame.Depth(i, j) - minv) / (maxv - minv) * 255 + 1);
                    if v>=1 && v<=256
                        DepthImage(i, j, :) = cmap(v, :);
                    end
                    
                end
            end
        end
        
        %%
        function mapped = GetMappedColorImage(obj)
        end
        
        %%
        function c = Color(obj)
            c = obj.Frame.Color;
        end
        
        function m = Mapped(obj)
            m = obj.GetMappedColorImage();
        end
        
        function d = Depth(obj)
            d = obj.Frame.Depth;
        end
        
        function di = DepthImage(obj)
            di = obj.GetDepthImage();
        end
        
    end
end