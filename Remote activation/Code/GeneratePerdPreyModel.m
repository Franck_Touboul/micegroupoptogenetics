function model = GeneratePerdPreyModel(input)
model = struct();
model.UseSpeedHistogram = false;
model.UsePreyApproach = true;


model.Histogram.minval = min(input(1,:));
model.Histogram.maxval = max(input(1,:));
model.Histogram.nBins = 20;
model.Histogram.dbin = (model.Histogram.maxval - model.Histogram.minval) / model.Histogram.nBins;

model.Speed.Histogram.minval = min(input(3,:));
model.Speed.Histogram.maxval = max(input(3,:));
model.Speed.Histogram.nBins = 100;
model.Speed.Histogram.dbin = ...
    (model.Speed.Histogram.maxval - model.Speed.Histogram.minval) / model.Speed.Histogram.nBins;

%%
model = PPModelDiscretePDFEstimation(input, model);
model.all.posterior = @PPModelDiscretePDF;
if model.UseSpeedHistogram
    for s=1:6
        model.states(s).Speed.histogram = model.all.Speed.histogram;
    end
end

%% state 1: avoidance
s = 1;
%model.states(s).histogram = ones(1, model.Histogram.nBins) / model.Histogram.nBins;
model.states(s).histogram = model.all.histogram;
model.states(s).contact = false;

model.states(s).posterior = @PPModelDiscretePDF;
%model.states(s).estimate  = @PPModelDiscretePDFEstimation;
model.states(s).estimate  = @(x, model, state) model;

model.states(s).title = '-';
model.states(s).type = 0;

%% state 2: predetor
s = 2;
model.states(s).ExpBase.alpha = 2;
model.states(s).ExpBase.theta = 0;
model.states(s).ExpBase.m = .2;

model.states(s).posterior = @PPModelExpBasePDF;
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'pred';
model.states(s).type = 1;

%% state 3: prey
s = 3;
model.states(s).ExpBase.alpha = 2;
model.states(s).ExpBase.theta = pi;
model.states(s).ExpBase.m = .2;

if model.UsePreyApproach
    model.states(s).posterior = @PPModelExpBasePDF;
else
    model.states(s).posterior = @(x, varargin) x(1, :)*0;
end
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'prey';
model.states(s).type = 1;

%% state 4: contact
s = 4;
%model.states(s).histogram = ones(1, model.Histogram.nBins) / model.Histogram.nBins;
model.states(s).histogram = model.all.histogram;
model.states(s).contact = true;

model.states(s).posterior = @PPModelDiscretePDF;
%model.states(s).estimate  = @PPModelDiscretePDFEstimation;
model.states(s).estimate  = @(x, model, state) model;

model.states(s).title = 'cont';
model.states(s).type = 0;

%% state 5: predetor
s = 5;
model.states(s).ExpBase.alpha = 2;
model.states(s).ExpBase.theta = 0;
model.states(s).ExpBase.m = .2;

model.states(s).posterior = @PPModelExpBasePDF;
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'pred';
model.states(s).type = 1;

%% state 6: prey
s = 6;
model.states(s).ExpBase.alpha = 2;
model.states(s).ExpBase.theta = pi;
model.states(s).ExpBase.m = .2;

model.states(s).posterior = @PPModelExpBasePDF;
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'prey';
model.states(s).type = 1;

%%
persistence = 10;
pst = 1-1/persistence;
ptr = 1/persistence;
pst = 1;
ptr = 1;
model.trans = [...
    pst ptr ptr ptr 0.0 0.0; % -
    0.0 ptr 0.0 ptr 0.0 0.0; % predator
    0.0 0.0 ptr ptr 0.0 0.0; % prey
    ptr 0.0 0.0 pst ptr ptr; % contact
    ptr 0.0 0.0 0.0 ptr 0.0; % predator
    ptr 0.0 0.0 0.0 0.0 ptr; % prey
    ];
%model.trans =model.trans > 0;
model = validateModel(model);

model.start = zeros(model.nstates, 1);
model.start(1) = 1;
model.start(2) = 1;
model.start(3) = 1;
model.start(4) = 1;
model.end = zeros(model.nstates, 1);
model.end(1) = 1;
model.end(4) = 1;
model.end(5) = 1;
model.end(6) = 1;
