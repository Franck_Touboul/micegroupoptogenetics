%function SocialAggressiveClassifier(obj)
options.MinLeaf = 5;
options.DAMethod = 'quadratic';
options.AggrThreshold = 2;
options.TestOnTrain = true;
%%
Classifier = obj.Contacts.Behaviors.AggressiveChase.Classifier;
AggrCE = Classifier.MarkID(Classifier.Labels >= options.AggrThreshold);
stat.Detection = 0;
stat.FalseAlarm = 0;
stat.Count = 0;
stat.NonAggrCount = 0;
for loo = AggrCE
    %%
    TrainMap = obj.Contacts.Behaviors.ChaseEscape.Map(1:size(Classifier.Vec, 2)) & Classifier.MarkID ~= loo;
    TestMap  = obj.Contacts.Behaviors.ChaseEscape.Map(1:size(Classifier.Vec, 2));
    for i = AggrCE
        if i == loo; continue; end
        TestMap(Classifier.MarkID == i) = false;
    end
        
    data = Classifier.Vec';
    label = Classifier.Labels';
    train = data(TrainMap, :);
    test  = data(TestMap, :);
    label = label(TrainMap, :);
    label = (label >= options.AggrThreshold);
    for i=1:size(train, 2)
        train(~isfinite(train(:, i)), i) = min(isfinite(train(:, i)));
    end
    %% train classifiers
    %--------------------------------------------------------------------------
    % decision tree classifier
    tree = classregtree(train, label, 'minleaf', options.MinLeaf, 'names', Classifier.LabelNames);
    
    %featidx = [1 4 5 6 7 9 10 12 14 15 16 17  20 22 23 24];
    %classes = classify(train(:, featidx), train(:, featidx), label, 'mahalanobis');
    
    %--------------------------------------------------------------------------
    % LD classifier
    dataTrainG1 = train(label==0,:);
    dataTrainG2 = train(label==1,:);
    [h,p,ci] = ttest2(dataTrainG1,dataTrainG2,[],[],'unequal');
    [p, featidx] = sort(p);
    featlocal = featidx(1:10);
    classes     = classify(train(:, featlocal), train(:, featlocal), label, options.DAMethod);
    testclasses = classify(test(:, featlocal), train(:, featlocal), label, options.DAMethod);
    
    %svm = svmtrain(double(train), label*2-1, 'Method', 'LS');
    %testclasses = svmclassify(svm, test);
    trainknn = knnclassify(train(:, featlocal), train(:, featlocal), label, 5, 'euclidean');    
    testknn = knnclassify(test(:, featlocal), train(:, featlocal), label, 5, 'euclidean');
    %testclasses = knnclassify(test(:, featlocal), train(:, featlocal), label, 3);
    
    %% Test on Train
    if options.TestOnTrain
        %%
        marks = SocialBehaviorLoad(obj);
        map = marks.agresivness < options.AggrThreshold;
        map(AggrCE) = true;
        %map(loo) = false;
        marks = SocialBehaviorLoad(obj, map);
        %data = data((1:size(data, 1)) > N, :);
        
        amarks = marks;
        amarks.ignore = marks.agresivness == 1;
        amarks.chase = marks.agresivness > 1;
        
        s = eval(tree, train);
        olbl = cellfun(@str2num,s);
        obj.Contacts.Behaviors.AggressiveChase2.Map = false(1, length(obj.Contacts.Behaviors.ChaseEscape.Map));
        obj.Contacts.Behaviors.AggressiveChase2.Map(TrainMap) = trainknn' | classes' | olbl';
        
        [results, match] = SocialBehaviorAnalysisScore(obj, amarks, 'AggressiveChase2');
        fprintf('#----------------\n');
        fprintf('# Test on Train: detection=%.1f%% (%d), false-alarm=%.1f%% (%d)\n', results.chase(1) / (results.chase(1) + results.chase(3)) * 100, results.chase(1), results.chase(2) / results.chase(5) * 100, results.chase(2));
    end
    %%
    marks = SocialBehaviorLoad(obj);
    map = marks.agresivness < options.AggrThreshold;
    map(loo) = true;
    marks = SocialBehaviorLoad(obj, map);
    
    amarks = marks;
    amarks.ignore = marks.agresivness == 1;
    amarks.chase = marks.agresivness > 1;
    
    s = eval(tree, test);
    olbl = cellfun(@str2num,s);
    
    obj.Contacts.Behaviors.AggressiveChase2.Map = false(1, length(obj.Contacts.Behaviors.AggressiveChase.Map));
    obj.Contacts.Behaviors.AggressiveChase2.Map(TestMap) = (testclasses' + olbl' + testknn') >= 1;
    
    [results, match] = SocialBehaviorAnalysisScore(obj, amarks, 'AggressiveChase2');
    fprintf('# Test on Train: detection=%.1f%% (%d), false-alarm=%.1f%% (%d)\n', results.chase(1) / (results.chase(1) + results.chase(3)) * 100, results.chase(1), results.chase(2) / results.chase(5) * 100, results.chase(2));
    
    
    stat.Detection = stat.Detection + results.chase(1);
    stat.FalseAlarm = stat.FalseAlarm + results.chase(2);
    stat.Count = stat.Count + 1;
    stat.NonAggrCount = stat.NonAggrCount + sum(~amarks.ignore);
end
%%
marks = SocialBehaviorLoad(obj);

fprintf('#--------------------------------------------------------------------------\n');
fprintf('# Final: detection=%.1f%% (%d), false-alarm=%.1f%% (%d)\n', stat.Detection / sum(marks.agresivness > 1) * 100, stat.Detection, stat.FalseAlarm / stat.NonAggrCount * 100, round(stat.FalseAlarm/stat.Count));

%%
if 1==2
    %%
    h = view(tree);
    fig2 = get(get(h, 'CurrentAxes'),'children');
    figure(1)
    cla(h)
    a1 = gca;
    copyobj(fig2, a1)
    axis off
end