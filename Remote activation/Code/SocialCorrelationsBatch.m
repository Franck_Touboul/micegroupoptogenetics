%%
options.day = 4;
options.nBins = 25;
%%
SocialExperimentData;
%objs = TrackLoadAll(options.day);
%%
data = [];
i = 1;
for id=11:GroupsData.nExperiments
    obj = TrackLoad({id options.day}, {'zones', 'ROI', 'valid'});
    %obj = objs{id};
    [MI, JointEntropy, Entropy] = SocialInformation(obj);
    data.MI(:, :, i) = MI;
    data.JointEntropy(:, :, i) = JointEntropy;
    data.Entropy(i, :) = Entropy(:)';
    data.id(i) = id;
    data.group(i) = GroupsData.group(id);
    i = i + 1;
end
%%
% for id=length(experiments)+1:length(experiments)+8
%     obj = TrackLoad(sprintf('Res/Mix.exp%04d.day02.cam00.obj.mat', id-18));
%     [MI, JointEntropy, Entropy] = SocialInformation(obj);
%     data.MI(:, :, i) = MI;
%     data.JointEntropy(:, :, i) = JointEntropy;
%     data.Entropy(i, :) = Entropy(:)';
%     data.id(i) = id;
%     data.group(i) = 3;
%     i = i + 1;
% end    
%%
groups = 2; % unique(data.group(:))';
for g=groups
    res = [];
    %%
    map = data.group == g;
    MI = data.MI(:, :, map);
    JE = data.JointEntropy(:, :, map);
    E = data.Entropy(map, :);
    %%
    res.full = [];
    for s=1:obj.nSubjects
%        res.full = [res.full; squeeze(MI(s,s,:) ./ JE(s,s,:))];
        res.full = [res.full; squeeze(MI(s,s,:)) ./ E(:, s)];
    end
    %%
    res.indep = [];
    res.add = [];
    seq = 1:obj.nSubjects;
    for s=1:obj.nSubjects
%        c = squeeze(MI(s,seq(seq ~= s),:) ./ JE(s,seq(seq ~= s),:));
        c = squeeze(MI(s,seq(seq ~= s),:)) ./ repmat(E(:, s)', obj.nSubjects - 1, 1);
        res.indep = [res.indep; c(:)];
        res.add = [res.add; sum(c, 1)'];
    end
    %%    
    subplot(length(groups), 1, find(g == groups));
    cmap = MyCategoricalColormap;
    maxval = max([res.full(:); res.indep(:); res.add(:)] * 100);
    x = sequence(0, maxval, options.nBins);
    hfull = histc(res.full * 100, x); hfull = hfull / sum(hfull);
    hindep = histc(res.indep * 100, x); hindep = hindep / sum(hindep);
    hadd = histc(res.add * 100, x); hadd = hadd / sum(hadd);
    plot(x, hindep, 'Color', cmap(1, :)); hon;
    plot(x, hadd, 'Color', cmap(2, :));
    plot(x, hfull, 'Color', cmap(3, :));
    hoff;
    xaxis(0, maxval)
end




%%-------------------------------------------------------------------------
%%-------------------------------------------------------------------------
%%-------------------------------------------------------------------------
return
SocialExperimentData;
%SocialMixExperimentData;
%%
nDays = 1;
days = 2;

index = 1;
MI = zeros(nSubjects, nSubjects, length(experiments) * nDays);
ENTROPY = zeros(nSubjects, length(experiments) * nDays);
ID = zeros(1, length(experiments) * nDays);
DAY = zeros(1, length(experiments) * nDays);
RANK = zeros(nSubjects, length(experiments) * nDays);
for id=1:length(experiments)
    for day = days
        prefix = sprintf(experiments{id}, day);
        fprintf('# -> %s\n', prefix);
        obj = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid', 'Hierarchy'});
        fprintf('# - compute MI\n');
        [q, mi, entropy] = SocialMutualInformation(obj);
       
        ENTROPY(:, index) = entropy(:);
        MI(:, :, index) = mi;
        DAY(index) = day;
        ID(index) = id;
        %RANK(:, index) = obj.Hierarchy.Group.AggressiveChase.rank;
        index = index + 1;
    end
end
%%
clf;
for d=days
    subplot(max(days), 1, d);
    offset = 0;
    res = [];
    errb = [];
    sig = [];
    for g=1:GroupsData.nGroups
        data = {}; data{1}=[]; data{2}=[]; data{3}=[];data{4}=[];
        count = zeros(1, 3);
        for id=GroupsData.index(GroupsData.group == g)
            map = DAY == d & ID == id;
            rank = RANK(:, map);
            level = max(rank) - rank + 1;
            level(level == 4) = 3;
            ent = ENTROPY(:, map);
            for i=1:obj.nSubjects
                data{level(i)} = [data{level(i)}, ent(i)];
            end
        end
        for i=1:3
            res(offset + i) = mean(data{i});
            errb(offset + i) = stderr(data{i});
            bar(offset+i, mean(data{i}), 'facecolor', GroupsData.Colors(g, :)); hon;
            errorbar(offset+i, mean(data{i}), stderr(data{i}), 'k');
            if i>1
                sig = [sig, ttest2(data{i}, data{i-1})];
                if ttest2(data{i}, data{i-1})
                    plot(offset + i - .5, 2.5, '*');
                end
            else
                sig = [sig, false];
            end
        end
        offset = offset + 3;
    end
    %bar(res, errb)
    %hon
    %plot(1:length(sig), sig, 'kx-')
    hoff;
end
%%
clf;
offset = 0;
res = [];
errb = [];
sig = [];
for g=1:GroupsData.nGroups
    data = {}; data{1}=[]; data{2}=[]; data{3}=[];data{4}=[];
    count = zeros(1, 3);
    for id=GroupsData.index(GroupsData.group == g)
        map = DAY == 1 & ID == id;
        rank = RANK(:, map);
        level = max(rank) - rank + 1;
        level(level == 4) = 3;
        ent = 0;
        for day=1:GroupsData.nDays
            map = DAY == day & ID == id;
            ent = ent + ENTROPY(:, map);
        end
        for i=1:obj.nSubjects
            data{level(i)} = [data{level(i)}, ent(i)];
        end
    end
    for i=1:3
        res(offset + i) = mean(data{i});
        errb(offset + i) = stderr(data{i});
        bar(offset+i, mean(data{i}), 'facecolor', GroupsData.Colors(g, :)); hon;
        errorbar(offset+i, mean(data{i}), stderr(data{i}), 'k');
        if i>1
            sig = [sig, ttest2(data{i}, data{i-1})];
            if ttest2(data{i}, data{i-1})
                plot(offset + i - .5, 2.5, '*');
                [h,p] = ttest2(data{i}, data{i-1});
                text(offset + i - .5, 2.5, num2str(p));
            end
        else
            sig = [sig, false];
        end
    end
    offset = offset + 3;
end
%bar(res, errb)
%hon
%plot(1:length(sig), sig, 'kx-')
hoff;
%%
nbins = 25;
m1 = floor(min(ENTROPY(:)));
m2 = ceil(4 *max(ENTROPY(:)));

    index = 1;
cmap = [1 1 1; 0.95 0.95 0.95;];
mice = MyMouseColormap;
%rankmap = MyDefaultColormap(256); rankmap = rankmap(end:-1:1, :);
rankmap = makeColorMap([255,149,0] / 255, [0,156,21] / 255 ,256);
rankmap = makeColorMap([0,0,0] / 255, [255,255,255] / 255 ,256);
rankmap = makeColorMap([1 1 1], [0 0 0] ,256);

%rankmap = makeColorMap([107,0,154]/255, [0,156,21]/256 ,256);
days = [1:4]';

for b=1:length(Groups)
    %%
        group = Groups(b);
    entropy = [];
    dailyEntropy = [];
    figure(1);
    subplot(4,1,1:2);
    x = sequence(m1, m2, nbins);
    offset = (index - 1);
    fill([m1 m1 m2 m2], [0 length(group.idx) * (nDays + 1) length(group.idx) * (nDays + 1) 0] + offset, cmap(mod(b,2)+1, :), 'EdgeColor', 'none');
    hold on;
    
    colorCount = [];
    colorCountIdx = 1;
    %%
    for i=group.idx
        %%
        for day=days %1:nDays
            %%
            idx = false(1, length(ID));
            for k=1:length(day)
                idx = idx | (ID == i & DAY == day(k));
                if k == 1
                    IDX = ID == i & DAY == day(k);
                end
            end
            if length(day) > 1 || day == 2
                entropy = [entropy; sum(ENTROPY(:, idx), 2)];
            end
            rank = RANK(:, IDX);
            rank = round((rank - min(rank)) / (max(rank) - min(rank)) * 255 + 1);
            [sent, sord] = sort(sum(ENTROPY(:, idx), 2));
            plot(sent, ones(1, 4) * index, 'ko-', 'Color', GroupsData.Colors(b, :), 'MarkerEdgeColor', 'none'); hold on;
            for s=1:obj.nSubjects
                plot(sent(s), index, 'o', 'MarkerFaceColor', rankmap(rank(sord(s)), :), 'MarkerEdgeColor', 'k');
%                plot(sent(s), index, 'o', 'MarkerFaceColor', mice(sord(s), :), 'MarkerEdgeColor', 'none');
                if day == 2;
                    figure(2);
                    subplot(1, length(Groups), b);
                    plot(sord(s), sent(s), 'o', 'MarkerFaceColor', mice(sord(s), :), 'MarkerEdgeColor', 'none'); hon
                    colorCount(colorCountIdx, sord(s)) = sent(s);
                    figure(1);
                end
            end
            if day==2
                colorCountIdx = colorCountIdx + 1;
            end
            index = index + 1;
        end
        dailyEntropy = [dailyEntropy; sum(ENTROPY(:, ID == i & DAY == 2), 2)];
        if length(days) > 1
            line([m1 m2], [index index],'Color', 'k', 'LineStyle', ':');
        end
        index = index + 1;
    end
    %line([m1 m2], [index-1 index-1],'Color', 'k', 'LineStyle', ':');
    subplot(4,4,9:12);
    plot(x, histc(entropy, x), 'Color', group.color, 'LineWidth', 2)
    xlabel('location entropy [bits]');
    ylabel('frequency (day 2)');
    hold on;
    subplot(4,4,13);
    MyBar(b, mean(dailyEntropy(:)), stderr(dailyEntropy(:)), group.color);
    a = axis;
    a(4) = max(a(4), ceil(mean(dailyEntropy(:)) + stderr(dailyEntropy(:))));
    axis(a);
    hold on;
    try
    figure(2); 
    errorbar(1:obj.nSubjects, mean(colorCount), stderr(colorCount), 'k');
    str = '';
    for s1=1:obj.nSubjects
        for s2=s1+1:obj.nSubjects
            if ttest2(colorCount(:, s1), colorCount(:, s2))
                str = [str ' (' num2str(s1) ', ' num2str(s2) ')'];
            end
        end
    end
    title([Groups(b).title str]);

    hoff;
    catch
    end
    figure(1);
end


figure(1); hoff;
subplot(4,1,1:2);
set(gca, 'YtickLabel', [])
xlabel('location entropy [bits]');
ylabel('group over four days');
a = axis;
axis([a(1) a(2) 0 index-1]);
hold off;
%subplot(3,1,3);
    subplot(4,4,13);
    a=axis;
    axis([0 GroupsData.nGroups+1 a(3) a(4)]);
set(gca, 'XTick', 1:GroupsData.nGroups);
%%
pairwise = [];
pairwiseP = [];
totalPairwiseP = [];
condP = [];
for i=Groups(2).idx
    for day=days(:)'
        %%
        mi = MI(:, :, ID == i & DAY == day);
        entropy = ENTROPY(:, ID == i & DAY == day);
     
        condP = [condP; diag(mi) ./ entropy];
        
        mi(1:obj.nSubjects+1:end) = nan;
        pairwise = [pairwise; mi(:)];
        
        miP = mi ./ repmat(entropy, 1, obj.nSubjects);
        pairwiseP = [pairwiseP; miP(:)];

        mi(1:obj.nSubjects+1:end) = 0;
        totalPairwiseP = [totalPairwiseP; sum(mi, 2) ./ entropy];
    end
end
%%
% index = 1;
% for id=1:length(experiments)
%     for day = 1:nDays
%         mi=MI(:, :, index);
%         mi(1:5:end)=nan;
%         %
%         subplot(length(experiments), nDays, index);
%         MyImagesc(mi)
%         index = index  +1;
%     end
% end

%%
style = {':'};
nbins = 50;
m1 = min([min(pairwiseP), min(condP)]);
m2 = max(max(pairwiseP), max(condP));
x = sequence(m1, m2, nbins);
hpw = histc(pairwiseP, x);
htpw = histc(totalPairwiseP, x);
hc = histc(condP, x);
% [hpw, xpw] = hist(pairwiseP,25);
% [hc, xc] = hist(condP,25);
cmap = MySubCategoricalColormap;
plot(x*100, hpw/sum(hpw)*100, style{:}, 'Color', cmap(2, :), 'LineWidth', 2);
hold on;
plot(x*100, hc/sum(hc)*100, style{:}, 'Color', cmap(4, :), 'LineWidth', 2);
hold off;
prettyPlot('', 'information percentage [%]', 'percentage that fall in bin [%]');
%legend('Pair: I(x_i; x_j)', 'Multiple: I(x_i; x_{i~=j})');
legend('single', 'multiple');
legend boxoff;

%%
style = {':'};
subplot(2,1,1);

nbins = 15;
m1 = min([min(pairwiseP), min(condP) min(totalPairwiseP)]);
m2 = max([max(pairwiseP), max(condP) max(totalPairwiseP)]);
x = sequence(m1, m2, nbins);
hpw = histc(pairwiseP, x);
htpw = histc(totalPairwiseP, x);
hc = histc(condP, x);
% [hpw, xpw] = hist(pairwiseP,25);
% [hc, xc] = hist(condP,25);
cmap = MySubCategoricalColormap;
plot(x*100, hpw/sum(hpw)*100, style{:}, 'Color', cmap(2, :), 'LineWidth', 2);
hold on;
plot(x*100, hc/sum(hc)*100, style{:}, 'Color', cmap(4, :), 'LineWidth', 2);
plot(x*100, htpw/sum(htpw)*100, style{:}, 'Color', cmap(6, :), 'LineWidth', 2);
hold off;
prettyPlot('', 'information percentage [%]', 'percentage that fall in bin [%]');
%legend('Pair: I(x_i; x_j)', 'Multiple: I(x_i; x_{i~=j})');
legend('single', 'multiple', 'additive');
legend boxoff;

if (1==2)
    %%
    p = 'Graphs/SocialCorrelationBatch/';
    mkdir(p);
    figure(1);
    saveFigure([p obj.FilePrefix '']);
end