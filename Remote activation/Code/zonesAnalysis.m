%  Social behavior zones areas analysis


%Loading an Excel file for each of the 4 mice to be analized
fileName=input('insert file name first mouse: ', 's');
[m1Matrix,yairText1]=xlsread(fileName);
fileName=input('insert file name second mouse: ', 's');
[m2Matrix,yairText2]=xlsread(fileName);
fileName=input('insert file name second mouse: ', 's');
[m3Matrix,yairText2]=xlsread(fileName);
fileName=input('insert file name second mouse: ', 's');
[m4Matrix,yairText2]=xlsread(fileName);

%Unifying the data from 4 mice (taking only the data of Laberynth,
% Big nest, Small nest, Feeder 1, Feeder 2, Water (in this order)
% The matrix is oredered in the following way:

%      Labyrinth      Big nest  ....
%   m1  m2  m3  m4  m1  m2  m3  m4...
%   0   1   0   0    1   0   1   0 
%   1   0   0   0    0   1   0   0  

BigMatrix=[m1Matrix(:,13) m2Matrix(:,13) m3Matrix(:,13) m4Matrix(:,13)...
    m1Matrix(:,14) m2Matrix(:,14) m3Matrix(:,14) m4Matrix(:,14)...
    m1Matrix(:,16) m2Matrix(:,16) m3Matrix(:,16) m4Matrix(:,16)...
    m1Matrix(:,17) m2Matrix(:,17) m3Matrix(:,17) m4Matrix(:,17)...
    m1Matrix(:,19) m2Matrix(:,19) m3Matrix(:,19) m4Matrix(:,19)...
    m1Matrix(:,18) m2Matrix(:,18) m3Matrix(:,18) m4Matrix(:,18)];

% TotalZones is the time spent in each zone by each mouse
TotalZones=reshape(sum(BigMatrix),4,6)';
TotalOpenSpace=repmat(size(BigMatrix,1),1,4)-sum(TotalZones);


LabyrCell=num2cell(num2str(BigMatrix(:,1:4)),2);
BigNestCell=num2cell(num2str(BigMatrix(:,5:8)),2);
SmallNestCell=num2cell(num2str(BigMatrix(:,9:12)),2);
Feeder1Cell=num2cell(num2str(BigMatrix(:,13:16)),2);
Feeder2Cell=num2cell(num2str(BigMatrix(:,17:20)),2);
WaterCell=num2cell(num2str(BigMatrix(:,21:24)),2);


LabyrM1=sum(strcmp(num2str([1,0,0,0]),LabyrCell));
LabyrM2=sum(strcmp(num2str([0,1,0,0]),LabyrCell));
LabyrM3=sum(strcmp(num2str([0,0,1,0]),LabyrCell));
LabyrM4=sum(strcmp(num2str([1,0,0,0]),LabyrCell));
LabyrM1M2=sum(strcmp(num2str([1,1,0,0]),LabyrCell));
LabyrM1M3=sum(strcmp(num2str([1,0,1,0]),LabyrCell));
LabyrM1M4=sum(strcmp(num2str([1,0,0,1]),LabyrCell));
LabyrM2M3=sum(strcmp(num2str([0,1,1,0]),LabyrCell));
LabyrM2M4=sum(strcmp(num2str([0,1,0,1]),LabyrCell));
LabyrM3M4=sum(strcmp(num2str([0,0,1,1]),LabyrCell));

BigNestM1=sum(strcmp(num2str([1,0,0,0]),BigNestCell));
BigNestM2=sum(strcmp(num2str([0,1,0,0]),BigNestCell));
BigNestM3=sum(strcmp(num2str([0,0,1,0]),BigNestCell));
BigNestM4=sum(strcmp(num2str([1,0,0,0]),BigNestCell));
BigNestM1M2=sum(strcmp(num2str([1,1,0,0]),BigNestCell));
BigNestM1M3=sum(strcmp(num2str([1,0,1,0]),BigNestCell));
BigNestM1M4=sum(strcmp(num2str([1,0,0,1]),BigNestCell));
BigNestM2M3=sum(strcmp(num2str([0,1,1,0]),BigNestCell));
BigNestM2M4=sum(strcmp(num2str([0,1,0,1]),BigNestCell));
BigNestM3M4=sum(strcmp(num2str([0,0,1,1]),BigNestCell));


SmallNestM1=sum(strcmp(num2str([1,0,0,0]),SmallNestCell));
SmallNestM2=sum(strcmp(num2str([0,1,0,0]),SmallNestCell));
SmallNestM3=sum(strcmp(num2str([0,0,1,0]),SmallNestCell));
SmallNestM4=sum(strcmp(num2str([1,0,0,0]),SmallNestCell));
SmallNestM1M2=sum(strcmp(num2str([1,1,0,0]),SmallNestCell));
SmallNestM1M3=sum(strcmp(num2str([1,0,1,0]),SmallNestCell));
SmallNestM1M4=sum(strcmp(num2str([1,0,0,1]),SmallNestCell));
SmallNestM2M3=sum(strcmp(num2str([0,1,1,0]),SmallNestCell));
SmallNestM2M4=sum(strcmp(num2str([0,1,0,1]),SmallNestCell));
SmallNestM3M4=sum(strcmp(num2str([0,0,1,1]),SmallNestCell));

Feeder1M1=sum(strcmp(num2str([1,0,0,0]),Feeder1Cell));
Feeder1M2=sum(strcmp(num2str([0,1,0,0]),Feeder1Cell));
Feeder1M3=sum(strcmp(num2str([0,0,1,0]),Feeder1Cell));
Feeder1M4=sum(strcmp(num2str([1,0,0,0]),Feeder1Cell));
Feeder1M1M2=sum(strcmp(num2str([1,1,0,0]),Feeder1Cell));
Feeder1M1M3=sum(strcmp(num2str([1,0,1,0]),Feeder1Cell));
Feeder1M1M4=sum(strcmp(num2str([1,0,0,1]),Feeder1Cell));
Feeder1M2M3=sum(strcmp(num2str([0,1,1,0]),Feeder1Cell));
Feeder1M2M4=sum(strcmp(num2str([0,1,0,1]),Feeder1Cell));
Feeder1M3M4=sum(strcmp(num2str([0,0,1,1]),Feeder1Cell));


Feeder2M1=sum(strcmp(num2str([1,0,0,0]),Feeder2Cell));
Feeder2M2=sum(strcmp(num2str([0,1,0,0]),Feeder2Cell));
Feeder2M3=sum(strcmp(num2str([0,0,1,0]),Feeder2Cell));
Feeder2M4=sum(strcmp(num2str([1,0,0,0]),Feeder2Cell));
Feeder2M1M2=sum(strcmp(num2str([1,1,0,0]),Feeder2Cell));
Feeder2M1M3=sum(strcmp(num2str([1,0,1,0]),Feeder2Cell));
Feeder2M1M4=sum(strcmp(num2str([1,0,0,1]),Feeder2Cell));
Feeder2M2M3=sum(strcmp(num2str([0,1,1,0]),Feeder2Cell));
Feeder2M2M4=sum(strcmp(num2str([0,1,0,1]),Feeder2Cell));
Feeder2M3M4=sum(strcmp(num2str([0,0,1,1]),Feeder2Cell));


WaterM1=sum(strcmp(num2str([1,0,0,0]),WaterCell));
WaterM2=sum(strcmp(num2str([0,1,0,0]),WaterCell));
WaterM3=sum(strcmp(num2str([0,0,1,0]),WaterCell));
WaterM4=sum(strcmp(num2str([1,0,0,0]),WaterCell));
WaterM1M2=sum(strcmp(num2str([1,1,0,0]),WaterCell));
WaterM1M3=sum(strcmp(num2str([1,0,1,0]),WaterCell));
WaterM1M4=sum(strcmp(num2str([1,0,0,1]),WaterCell));
WaterM2M3=sum(strcmp(num2str([0,1,1,0]),WaterCell));
WaterM2M4=sum(strcmp(num2str([0,1,0,1]),WaterCell));
WaterM3M4=sum(strcmp(num2str([0,0,1,1]),WaterCell));




figure
subplot(4,6,1)
pie([LabyrM1 LabyrM1M2 LabyrM1M3 LabyrM1M4], {'alone','m2','m3','m4'});
subplot(4,6,2)
pie([BigNestM1 BigNestM1M2 BigNestM1M3 BigNestM1M4], {'alone','m2','m3','m4'});
subplot(4,6,3)
pie([SmallNestM1 SmallNestM1M2 SmallNestM1M3 SmallNestM1M4], {'alone','m2','m3','m4'});
subplot(4,6,4)
pie([Feeder1M1 Feeder1M1M2 Feeder1M1M3 Feeder1M1M4], {'alone','m2','m3','m4'});
subplot(4,6,5)
pie([Feeder2M1 Feeder2M1M2 Feeder2M1M3 Feeder2M1M4], {'alone','m2','m3','m4'});
subplot(4,6,6)
pie([WaterM1 WaterM1M2 WaterM1M3 WaterM1M4], {'alone','m2','m3','m4'});

subplot(4,6,7)
pie([LabyrM2 LabyrM1M2 LabyrM2M3 LabyrM2M4],{'alone','m1','m3','m4'});
subplot(4,6,8)
pie([BigNestM2 BigNestM1M2 BigNestM2M3 BigNestM2M4],{'alone','m1','m3','m4'});
subplot(4,6,9)
pie([SmallNestM2 SmallNestM1M2 SmallNestM2M3 SmallNestM2M4],{'alone','m1','m3','m4'});
subplot(4,6,10)
pie([Feeder1M2 Feeder1M1M2 Feeder1M2M3 Feeder1M2M4],{'alone','m1','m3','m4'});
subplot(4,6,11)
pie([Feeder2M2 Feeder2M1M2 Feeder2M2M3 Feeder2M2M4],{'alone','m1','m3','m4'});
subplot(4,6,12)
pie([WaterM2 WaterM1M2 WaterM2M3 WaterM2M4],{'alone','m1','m3','m4'});

subplot(4,6,13)
pie([LabyrM3 LabyrM1M3 LabyrM2M3 LabyrM3M4],{'alone','m1','m2','m4'});
subplot(4,6,14)
pie([BigNestM3 BigNestM1M3 BigNestM2M3 BigNestM3M4],{'alone','m1','m2','m4'});
subplot(4,6,15)
pie([SmallNestM3 SmallNestM1M3 SmallNestM2M3 SmallNestM3M4],{'alone','m1','m2','m4'});
subplot(4,6,16)
pie([Feeder1M3 Feeder1M1M3 Feeder1M2M3 Feeder1M3M4],{'alone','m1','m2','m4'});
subplot(4,6,17)
pie([Feeder2M3 Feeder2M1M3 Feeder2M2M3 Feeder2M3M4],{'alone','m1','m2','m4'});
subplot(4,6,18)
pie([WaterM3 WaterM1M3 WaterM2M3 WaterM3M4],{'alone','m1','m2','m4'});

subplot(4,6,19)
pie([LabyrM4 LabyrM1M4 LabyrM2M4 LabyrM3M4],{'alone','m1','m2','m3'});
subplot(4,6,20)
pie([BigNestM4 BigNestM1M4 BigNestM2M4 BigNestM3M4],{'alone','m1','m2','m3'});
subplot(4,6,21)
pie([SmallNestM4 SmallNestM1M4 SmallNestM2M4 SmallNestM3M4],{'alone','m1','m2','m3'});
subplot(4,6,22)
pie([Feeder1M4 Feeder1M1M4 Feeder1M2M4 Feeder1M3M4],{'alone','m1','m2','m3'});
subplot(4,6,23)
pie([Feeder2M4 Feeder2M1M4 Feeder2M2M4 Feeder2M3M4],{'alone','m1','m2','m3'});
subplot(4,6,24)
pie([WaterM4 WaterM1M4 WaterM2M4 WaterM3M4],{'alone','m1','m2','m3'});



