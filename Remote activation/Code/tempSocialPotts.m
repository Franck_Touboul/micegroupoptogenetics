function obj = SocialPotts(obj, param)
% Syntax:
%       obj = SocialPotts(obj)
%       obj = SocialPotts(obj, model)
%       obj = SocialPotts(obj, zones)
% Adds the following information to the obj:
%   obj.Analysis.Potts
%       + Model: a cell contatining the Maximum
%                                   Entropy (ME) model for each level
%                                   (independent, pairwise, etc.)
%       + Entropy: the entropy of each ME level
%       + In: the mutual information
%       + Ik: 
%       + nIters: number of iters used the ME model
%%
fprintf('# computing the potts model\n');
if nargin > 1
    if iscell(param)
        model = param;
    elseif isnumeric(param)
        zones = param;
    end
end
obj = TrackLoad(obj);
%%
if obj.OutputToFile
    savePrefix = [obj.OutputPath obj.FilePrefix '.potts'];
else
    savePrefix = '';
end
%savePrefix = '';
obj.Analysis.Potts.nIters = 500;
%
%% train potts model if needed
if exist('zones', 'var')
    data = zones - 1;
else
    data = zeros(obj.nSubjects, sum(obj.valid));
    for s=1:obj.nSubjects
        currZones = obj.zones(s, :);
        data(s, :) = currZones(obj.valid) - 1;
    end
end
count = max(data(:)) + 1;
if exist('model', 'var')
    me = model;
else
    me = {};
    fprintf('# - training model:\n');
    for level=1:obj.nSubjects
        fprintf('#      . level %d\n', level);
        local = obj;
        local.n = level;
        local.output = true;
        local.count = count;
        local.nIters = obj.Analysis.Potts.nIters;
        me{level} = trainPottsModel(local, data);
    end
end
obj.Analysis.Potts.Model = me;
analysis.me = me;

%% compute independent probabilities
%independentProbs = computeIndependentProbs(data, count);

%% compute joint probabilities
%[jointProbs, jointPci] = computeJointProbs(data, count);

%% compute probability for each sample
fprintf('# - computing joint probabilities\n');
[obj, independentProbs, jointProbs] = SocialComputePatternProbs(obj, false);

% [sampleProbIndep, sampleProbJoint] = computeSampleProb(data, count, independentProbs, jointProbs);
% fprintf('mean log-likelihood = %6.4f\n', mean(log(sampleProbJoint)));
%% compute MaxEnt probabilities for each sample
Entropy = [];
for level=1:obj.nSubjects
    fprintf('# computing max-entropy probabilities for level %d\n', level);
%    p = exp(me{level}.features * me{level}.weights');
    p = exp(me{level}.perms * me{level}.weights');
    perms_p = exp(me{level}.perms * me{level}.weights');
    Z = sum(perms_p);
    p = p / Z;
    Entropy(level) = -p' * log2(p);
    %% plot
    if isempty(savePrefix)
        subplot(3,2,level);
    else
        subplot(2,2,1);
    end
    b1 = jointProbs;
    b2 = independentProbs;
    
    p1 = jointProbs;
    p2 = p;
    % confidence

    PlotProbProb(p1,p2',size(data,2))
           
    Djs = JensenShannonDivergence(jointProbs, p');
            
    tit = ['Maximum-Entropy Model (D_{js}=' num2str(Djs) ')'];
    title(tit);
    xlabel('Joint');
    ylabel('ME');
    
    %prettyPlot(tit, 'Joint', 'ME');
    %legend([h1, h2], 'Independent', 'ME', 'location', 'SouthEast'); legend boxoff
    if ~isempty(savePrefix)
        saveFigure([savePrefix sprintf('.level_%d', level)]);
    end
end
%%
In = Entropy(1) - Entropy(end);
Ik = -diff(Entropy);
if ~isempty(savePrefix)
    subplot(2,2,1);
else
    subplot(3,2,5);
end
%pie(Ik/In, {'pairs', 'triples', 'quadruples'});
myBoxPlot(cumsum(Ik), {'Pairwise (I_{(2)})', 'Triplet (I_{(3)})', 'Quadruplet (I_{(4)})'});
title('K''th-Order Correlations');
if ~isempty(savePrefix)
    saveFigure([savePrefix '.kth_order_corr']);
end
%%
obj.Analysis.Potts.Entropy = Entropy;
obj.Analysis.Potts.In = In;
obj.Analysis.Potts.Ik = Ik;
%%
if obj.OutputToFile
    fprintf('# - saving data\n');
    TrackSave(obj);
end

if obj.OutputInOldFormat
    filename = [obj.OutputPath obj.FilePrefix '.analysis.mat'];
    fprintf(['# - saving to file: ''' filename '''\n']);
    save(filename, 'analysis');
end
