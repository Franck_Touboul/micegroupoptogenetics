function rowTitle(nRows, nCols, row, text, varargin)
xloc = 0.075;
%yloc = 1 - ((row - 1) / (nRows + 1) + .75 / nRows);
margin = 0.06;
zrow = row - 1;
yloc = 1 - (margin + (1 - 2 * margin) * (2 * zrow + 1) / (2 * nRows));
%%
x = [xloc xloc+eps];
y = [yloc yloc+eps];
%% Create the textarrow object: 
annotation('textarrow',x,y,'String',text,'FontSize',14,'HeadStyle', 'none', 'TextRotation', 90, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'Bottom', varargin{:});

