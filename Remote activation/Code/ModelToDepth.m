function [x,y,z] = ModelToDepth(Model, Resolution)
%%
Points = ModelToPointsF2(Model);

if nargout == 0
    subplot(3,2,1:4);
    ModelShow(Model, Points)
end

X = Points.Skin.X(Model.Category ~= 4, :);
Y = Points.Skin.Y(Model.Category ~= 4, :);
Z = Points.Skin.Z(Model.Category ~= 4, :);
valid = Points.Skin.Area(Model.Category ~= 4, :) < 0;

vX = X(valid);
vY = Y(valid);
vZ = Z(valid);

% find mouse edges
edges = ModelFindEdges(X, Y, Z, valid);
if nargout == 0
    subplot(3,2,6);
    plot(vX(edges)', vY(edges)', '-r');
    axis equal;
end

% create triangulation
warning('off', 'MATLAB:DelaunayTri:ConsConsSplitWarnId');
dt = DelaunayTri([vX, vY], edges);
warning('on', 'MATLAB:DelaunayTri:ConsConsSplitWarnId');
dtx = dt.X(:, 1); dty = dt.X(:, 2);
mediandist = sqrt(median(diff(dtx(dt.edges), 1, 2).^2 + diff(dty(dt.edges), 1, 2).^2));

% create grid of mouse background
mnx = min(vX(:))-abs(dtx);
mny = min(vY(:))-abs(dtx);
mxx = max(vX(:))+abs(dtx);
mxy = max(vY(:))+abs(dtx);
[x, y] = meshgrid(mnx:mediandist:mxx, mny:mediandist:mxy);

% create 3D model points
ploc = dt.pointLocation([x(:), y(:)]);
io = false(size(ploc,1), 1);
ios = dt.inOutStatus;
io(~isnan(ploc)) = ios(ploc(~isnan(ploc)));
outer = isnan(ploc) | ~io;

data.X = [vX(:); x(outer)];
data.Y = [vY(:); y(outer)];
data.Z = -[vZ(:); zeros(sum(outer), 1)];

% interpolate missing points
dt = DelaunayTri([data.X, data.Y]);
map = false(1, size(data.X, 1));
o = 1;
for i=1:size(data.X, 1)
    if data.X(i) == dt.X(o, 1) && data.Y(i) == dt.X(o, 2)
        map(i) = true;
        o = o + 1;
    end
end
F = TriScatteredInterp(dt, data.Z(map));
[x, y] = meshgrid(mnx:1/Resolution:mxx, mny:1/Resolution:mxy);
z = reshape(F([x(:),y(:)]), size(x));

if nargout == 0
    subplot(3,2,5);
    surfl(x, y, z)
    hon
    plot3(vX, vY, -vZ, 'r.')
    hoff
    shading interp; 
    colormap(gray); axis equal
    grid on;
end
