function obj = BksPottsUsingSUMMET(obj)
me = trainPottsModelUsingSummet(obj);
obj.Analysis.RPotts = me;
TrackSave(obj);