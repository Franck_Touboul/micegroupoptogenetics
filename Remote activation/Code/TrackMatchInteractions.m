for s1=1:obj.nSubjects
    for s2=1:obj.nSubjects
        if s1==s2; continue; end
        e1 = obj.Hierarchy.ChaseEscape.interactions.events{s1, s2};
        e2 = obj.Hierarchy.ChaseEscape.interactions.events{s2, s1};
        e = {e1{:}, e2{:}};
        begf = zeros(1, length(e));
        endf = zeros(1, length(e));
        for i=1:length(e)
            begf(i) = e{i}.BegFrame;
            endf(i) = e{i}.EndFrame;
        end
        matched = cell(1, length(e));
        for i=1:length(e)
            idx = find(~(begf(i) > endf | endf(i) < begf));
            idx = idx(idx ~= i);
            matched{i} = idx;
        end
    end
end