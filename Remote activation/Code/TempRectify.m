function TempRectify(obj)
%%
[x,y,z] = obj.GetRealWorldCoords;
x(z == 0) = nan;
y(z == 0) = nan;
z(z == 0) = nan;

b = robustfit([x(:), y(:)], z(:));
mesh(x,y,z-b(2) * x - b(3)*y)