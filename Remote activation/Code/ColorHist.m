function ColorHist(img)
nBins = 100;
[rHist, r] = imhist(img(:,:,1), nBins);
[gHist, g] = imhist(img(:,:,2), nBins);
[bHist, b] = imhist(img(:,:,3), nBins);

plot(r, rHist, 'r', g, gHist, 'g', b, bHist, 'b');