function s = sequence(from, to, num)
if from == to
    s = ones(1, num) * from;
else
    s = from:(to-from)/(num-1):to;
end
