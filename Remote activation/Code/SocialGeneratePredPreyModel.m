function model = SocialGeneratePredPreyModel(obj)

%%
x = obj.Interactions.EmissionMatch;
m = obj.Interactions.EmissionMismatch;
z = obj.Interactions.ContactMismatch;

model.emis = [...
              m   x   m   z   z   z; % idel
              m   m   x   m   m   x; % pred
              x   m   m   x   m   m; % prey
              z   z   z   m   x   m; % cont
              m   m   x   m   m   x; % pred
              x   m   m   x   m   m; % prey
              ];

model.trans = [...
               1 1 1 1 0 0;
               0 1 0 1 0 0;
               0 0 1 1 0 0;
               1 0 0 1 1 1;
               1 0 0 0 1 0;
               1 0 0 0 0 1;
               ];
model.names = {'-', 'pred', 'prey', 'cont', 'pred', 'prey'};
model.id = 1:length(model.names);
%obj.Interactions.PredPrey.model = model;


return
%% Repeating states
x = obj.Interactions.EmissionMatch;
m = obj.Interactions.EmissionMismatch;

model.emis = [...
    m   x   m   0.0 0.0 0.0; % idel
    m   m   x   m   m   x; % pred
    x   m   m   x   m   m; % prey
    0.0 0.0 0.0 m   x   m; % cont
    m   m   x   m   m   x; % pred
    x   m   m   x   m   m; % prey
    m   x   m   0.0 0.0 0.0; % wait
    ];

model.trans = [...
    1 1 1 1 0 0 0;
    0 1 0 1 0 0 0;
    0 0 1 1 0 0 0;
    0 0 0 1 1 1 1;
    0 0 0 0 1 0 1;
    0 0 0 0 0 1 1;
    1 0 0 0 0 0 0;
    ];

model.names = {'-', 'pred', 'prey', 'cont', 'pred', 'prey', '+'};
model.id = 1:length(model.names);

obj.Interactions.MinSubInteractionDuration = max(obj.Interactions.MinSubInteractionDuration, 1);
obj.Interactions.MinIdleDuration = max(obj.Interactions.MinIdleDuration, 1);


% obj.Interactions.MinSubInteractionDuration = 2;
% obj.Interactions.MinIdleDuration = 1;

%%
m = struct();
m.emis = [];
m.original = [];
m.start = [];
m.finish = [];
idx = 1;
for i=1:length(model.names)
    if any(strcmp({'pred', 'prey', '+'}, model.names{i}))
        
        if any(strcmp({'pred', 'prey'}, model.names{i}))
            repeats = obj.Interactions.MinSubInteractionDuration;
        else
            repeats = obj.Interactions.MinIdleDuration;
        end
        for r=1:repeats
            m.emis(idx, :) = model.emis(i, :);
            m.id(idx) = model.id(i);
            if r == 1
                m.start(idx) = model.id(i);
            end
            if r == repeats
                m.finish(idx) = model.id(i);
            end
            idx =  idx + 1;
        end
        %     elseif any(strcmp({'wait'}, model.names{i}))
        %         for r=1:obj.Interactions.MinIdleDuration
        %             m.emis(idx, :) = model.emis(i, :);
        %             m.id(idx) = model.id(i);
        %             idx =  idx + 1;
        %         end
    else
        m.emis(idx, :) = model.emis(i, :);
        m.id(idx) = model.id(i);
        m.original(idx) = true;
        m.start(idx) = model.id(i);
        m.finish(idx) = model.id(i);
        idx =  idx + 1;
    end
end
if length(m.start) < length(m.id)
    m.start(end+1:length(m.id)) = 0;
end
if length(m.finish) < length(m.id)
    m.finish(end+1:length(m.id)) = 0;
end

%%
m.trans = zeros(length(m.id));
for i=1:length(m.id)
    if m.finish(i) == 0
        m.trans(i, i+1) = 1;
    else
        target = find(model.trans(m.finish(i), :));
        for j=1:length(target)
            if target(j) == m.finish(i) && target(j) ~= m.start(i)
                m.trans(i, i) = model.trans(m.finish(i), m.finish(i));
                continue;
            end
            m.trans(i, m.start == target(j)) = model.trans(m.finish(i), target(j));
        end
        
    end
end
%%
m.names = model.names;
m.id(m.id == find(strcmp({'+'}, model.names))) = find(strcmp({'-'}, model.names));
model = m;
