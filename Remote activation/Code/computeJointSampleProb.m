function [sampleProbJoint] = computeJointSampleProb(data, range, jointProbs)

[nSubjects, nData] = size(data);

% joint
baseVector = range.^[0:nSubjects-1];
h = baseVector * data + 1;
sampleProbJoint = nan(size(h));
sampleProbJoint(~isnan(h)) = jointProbs(h(~isnan(h)));
