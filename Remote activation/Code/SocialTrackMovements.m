function obj = SocialTrackMovements(obj, fs)
fprintf('# - setting time-scale to %d\n', fs);
obj = TrackLoad(obj);
%func = @(x) (x(1) ~= x(end)) * x(end);
func = @(x) any(x(:, 1) ~= x(:, end)) * x(:, end);
if fs > 2
%    obj.zones = blkproc(obj.zones(:, obj.valid), [1, fs], func);
    obj.zones = blkproc(obj.zones(:, obj.valid), [obj.nSubjects, fs], func);
    obj.valid = true(1, size(obj.zones, 2));
    obj.nFrames = size(obj.zones, 2);
end