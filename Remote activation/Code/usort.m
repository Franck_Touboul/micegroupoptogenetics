function [Q, order] = usort(data, varargin)
[Q, order] = sort(data, varargin{:});
idx = 1;
for q=Q
    order(find(data == q)) = idx;
    idx = idx + 1;
end