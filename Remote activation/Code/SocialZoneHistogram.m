function SocialZoneHistogram(obj, subjects)
%%
if 1==2
maxEvent = 0;
for s=1:obj.nSubjects
    maxEvent = max([maxEvent diff(find(diff(obj.zones(s, :)) ~= 0))]);
end
%%
cmap = MySubCategoricalColormap;
x = exp(sequence(0, log(maxEvent), 100));
for s=1:obj.nSubjects
    for i=1
        %%
        [b,e,l] = FindEvents(obj.zones(s, :) == i);
        h = histc(l, x);
        loglog((x(1:end-1) + x(2:end))/2, h(1:end-1)/sum(h(1:end-1)), '.:', 'Color', cmap(i, :));

        hold on;
    end
end
hoff;
end
%%
%cmap = MySubCategoricalColormap;
subplot(2,2,1);
cmap = [    189 190 200;
    148 216 239;
    0 177 229;
    234 165 193;
    179 213 157;
    53 178 87;
    220 73 89;
    240 191 148;
    240 145 55;
    98 111 179;
    246 237 170;
    248 231 83;
    ]/256;
cmap = MyZonesColormap;
%x = exp(sequence(0, log(maxEvent), 100));
mxx = -inf;
mnx = inf;
for i=1:obj.ROI.nZones
    L = [];
    for s=subjects %:obj.nSubjects
        %%
        [b,e,l] = FindEvents(obj.zones(s, :) == i);
        L = [L, l];
    end
    %x = sequence(min(L), max(L), 100);
    x = exp(sequence(0, log(max(L)), 8));

    h = histc(L, x);
    X = (x(1:end-1) + x(2:end))/2*obj.dt;
    Y = h(1:end-1)/sum(h(1:end-1))./(x(2:end) - x(1:end-1));
    
        loglog(X(Y>0), Y(Y>0), '.-', 'Color', cmap(i, :), 'MarkerSize', 20); hold on;
        %vert_line(mean(L) * obj.dt / 60)
        
        %loglog(x, h, 'x:'); hon;
        
%     x = exp(sequence(0, log(max(L)), 100));
%     h = histc(L, x);
        %loglog((x(1:end-1) + x(2:end))/2*obj.dt/60, h(1:end-1)/sum(h(1:end-1)), 'x:', 'Color', cmap(j, :)); hold on;l
%         loglog(x, h, '.:'); hon;
        %loglog(x, h)
    mxx = max(max(X), mxx);
    mnx = min(min(X), mnx);
end
prettyPlot('Time Spent in Zone Histogram', 'time [secs]', 'pdf');
%legend(obj.ROI.ZoneNames, 'location', 'EastOutside');
%legend boxoff

hoff;
mxx = 7e3;
xaxis(mnx, mxx);
set(gca, 'XTick', [.1 1 6 30 180 1800]);
set(gca, 'YTick', [10^-8 10^-6 0.0001 0.01 1]);
yaxis(10^-8, 1);


if (1==1)
    %%
    p = 'Graphs/SocialZonesHistogram/';
    mkdir(p);
    figure(1);
    saveFigure([p obj.FilePrefix '.subjects' regexprep(num2str(subjects), ' ','')]);
end