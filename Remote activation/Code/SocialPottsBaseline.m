function obj = SocialPottsBaseline(obj)
obj = TrackLoad(obj);
%%
options.nIters = 100;
obj.Analysis.Potts.Baseline = [];
obj.Analysis.Potts.Baseline.Ik = zeros(obj.nSubjects-1, options.nIters);
obj.Analysis.Potts.Baseline.Entropy = zeros(obj.nSubjects-1, options.nIters);
i = 1;
while i<options.nIters
    curr = obj;
    curr = SocialSetZones(curr, obj.zones(:, obj.valid));
    zones = curr.zones;
    try
        fprintf('\n\n\n\n\n-- %3d ------------------------------------------------------------------\n', i);
        for s=1:obj.nSubjects
            r = randi(curr.nFrames);
            zones(s, :) = [zones(s, r:end) zones(s, 1:r-1)];
        end
        curr = SocialSetZones(curr, zones);
        [Ik, Entropy] = SocialFastMultiInformation(curr);
        obj.Analysis.Potts.Baseline.Ik(:, i) = Ik(:);
        obj.Analysis.Potts.Baseline.Entropy(:, i) = Entropy(:);
        i = i + 1;
    catch
        fprintf('# failed!!!!!\n');
    end
end
if obj.OutputToFile
    obj = TrackSave(obj);
end