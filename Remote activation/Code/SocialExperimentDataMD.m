experiments = {...
'MDCtrl.exp0003.day%02d.cam01',...
'MDCtrl.exp0004.day%02d.cam04',...
'MDCtrl.exp0007.day%02d.cam04',...
'MDCtrl.exp0008.day%02d.cam01',...
'MDCtrl.exp0010.day%02d.cam01',...
'MD.exp0001.day%02d.cam01',...
'MD.exp0002.day%02d.cam04',...
'MD.exp0009.day%02d.cam04',...
'MD.exp0013.day%02d.cam04',...
'MD.exp0014.day%02d.cam01',...
'MD.exp0015.day%02d.cam04',...
'MDMix.exp0005.day%02d.cam01',...
'MDMix.exp0006.day%02d.cam01',...
'MDMix.exp0011.day%02d.cam04',...
'MDMix.exp0012.day%02d.cam01',...
    };

seq = 1:length(experiments);

% E.map = logical([1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0]);
% E.idx = find(E.map);
% 
% SC.map = ~E.map;
% SC.idx = find(SC.map);

Groups(1).map = 1 <= seq & seq <= 5;
Groups(1).idx = find(Groups(1).map);
Groups(1).title = 'MD Ctrl';
Groups(1).initials = 'MDCtrl';
Groups(1).color =  [80 171 210] / 255;

Groups(2).map = 6 <= seq & seq <= 11;
Groups(2).idx = find(Groups(2).map);
Groups(2).title = 'MD';
Groups(2).initials = 'MD';
Groups(2).color =  [232 24 46] / 255;

Groups(3).map = 12 <= seq & seq <= 15;
Groups(3).idx = find(Groups(3).map);
Groups(3).title = 'MDMix';
Groups(3).initials = 'MDMix';
Groups(3).color =  [120 182 83] / 255;

titles = {'MD Control', 'MD', 'MD Mix' };

nDays = 4;
nSubjects = 4;

GroupsData.Experiments = experiments;
GroupsData.Initials = {};
GroupsData.Titles = {};
GroupsData.nGroups = length(Groups);
GroupsData.Colors = [];
GroupsData.index = [];
GroupsData.group = [];
for i=1:length(Groups)
    GroupsData.Titles{i} = Groups(i).title;
    GroupsData.Initials{i} = Groups(i).initials;
    GroupsData.Colors(i, :) = Groups(i).color;
    name = regexprep(Groups(i).title, ' ', '');
    GroupsData.(name) = Groups(i);
    GroupsData.index = [GroupsData.index, Groups(i).idx];
    GroupsData.group = [GroupsData.group, Groups(i).idx * 0 + i];
end
GroupsData.nDays = nDays;
GroupsData.nSubjects = nSubjects;
if isunix
    GroupsData.OutputPath = 'Res/';
    GroupsData.MoviesPath = '../Movies/';
else
    GroupsData.OutputPath = 'Z:/tests/SocialMice/base/Res/';
    GroupsData.MoviesPath = 'Z:/tests/SocialMice/Movies/';
end
