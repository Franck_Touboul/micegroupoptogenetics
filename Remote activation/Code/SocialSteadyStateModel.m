other = obj;
%z = obj.zones(:, obj.valid);
%other.zones(:, obj.valid) = z(:, randperm(sum(obj.valid)));

mm = [];
for s=1:other.nSubjects
    %%
    z = other.zones(s, other.valid);
    [z(1:end-1); z(2:end)];
    ind = sub2ind([other.ROI.nZones, other.ROI.nZones], z(1:end-1), z(2:end));
    count = reshape(histc(ind, 1:other.ROI.nZones^2), [other.ROI.nZones, other.ROI.nZones]);
    mm.TransProb{s} = count ./ repmat(sum(count, 2), 1, other.ROI.nZones);
    mm.Prob{s} = histc(z, 1:other.ROI.nZones) / length(z);
end
%%
p = ones(1, sum(obj.valid));
for s=1:other.nSubjects
    p = p .* mm.Prob{s}(other.zones(s, obj.valid));
end
fprintf('# indendent mean log_2 likelihood: %.3f\n', mean(log2(p)))
%
base = other.ROI.nZones;
baseVector = base.^(other.nSubjects-1:-1:0);
p = obj.Analysis.Potts.Model{1}.jointProb(baseVector * (obj.zones(:, obj.valid) - 1) + 1);
fprintf('# joint mean log_2 likelihood: %.3f\n', mean(log2(p)))
%
p = ones(1, sum(other.valid));
idx = zeros(1, other.nFrames);
for s=1:other.nSubjects
    %%
    z = other.zones(s, other.valid);
    [z(1:end-1); z(2:end)];
    ind = sub2ind([other.ROI.nZones, other.ROI.nZones], z(1:end-1), z(2:end));
    p(2:end) = p(2:end) .* mm.TransProb{s}(ind);
    p(1) = p(1) * mm.Prob{s}(z(1));
end
fprintf('# markov mean log_2 likelihood: %.3f\n', mean(log2(p)))

%% simulate data
sobj = other;
fields = {'x', 'y', 'time', 'regions', 'hidden', 'sheltered', 'speedCMperSec', 'Contacts', 'speed', 'Analysis'};
for i=1:length(fields)
    try
        sobj = rmfield(sobj, fields{i});
    catch
    end
end
sobj.OutputToFile = false;
sobj.OutputInOldFormat = false;
sobj.nFrames = sum(obj.valid);
sobj.valid = true(1, sobj.nFrames);
sobj.zones = zeros(obj.nSubjects, sobj.nFrames);
sobj.zones(:, 1) = obj.zones(:, find(obj.valid, 1));
for s=1:obj.nSubjects
    z = sobj.zones(s, 1);
    ctp = cumsum(mm.TransProb{s}, 2);
    r = rand(1, sobj.nFrames);
    for i=2:sobj.nFrames
        nz = find(r(i) < ctp(z, :), 1);
        sobj.zones(s, i) = nz;
        z = nz;
    end
end


%%
mm.Alpha = 1;
base = other.ROI.nZones;
baseVector = base.^(other.nSubjects-1:-1:0);
p = zeros(1, sum(other.valid));
idx = zeros(1, other.nFrames);
for s=1:other.nSubjects
    %%
    z = other.zones(s, other.valid);
    [z(1:end-1); z(2:end)];
    ind = sub2ind([other.ROI.nZones, other.ROI.nZones], z(1:end-1), z(2:end));
    p(2:end) = p(2:end) + log(mm.TransProb{s}(ind));
    p(1) = p(1) + log(mm.Prob{s}(z(1)));
end
mm.SampleLogProbs = zeros(1, other.nFrames);
mm.SampleLogProbs(other.valid) = p;
idx(other.valid) = baseVector * (other.zones(:, other.valid) - 1) + 1;
uidx = unique(idx);
uidx = uidx(uidx > 0);
mm.JointProb = zeros(1, other.ROI.nZones ^ other.nSubjects);
for i=uidx
    mm.JointProb(i) = sum(1 - (1 - mm.Alpha) * exp(mm.SampleLogProbs(idx == i)));
end
mm.JointProb = mm.JointProb / sum(mm.JointProb);
%%
p2 = log(exp(mm.SampleLogProbs(other.valid)) * (1 - mm.Alpha) + mm.JointProb(idx(idx > 0)) * mm.Alpha);
exp(mean(p))
exp(mean(p2))

%%
mm.Allowed = {};
for s=1:obj.nSubjects
    mm.Allowed{s} = false(0);
    for i=1:obj.ROI.nZones
        a = find(mm.TransProb{s}(i, :));
        m = false(1, obj.ROI.nZones^obj.nSubjects);
        for j=1:length(a)
            m = m | (obj.Analysis.Potts.Model{1}.inputPerms(:, s)' == a(j));
        end
        mm.Allowed{s}(i, :) = m;
    end
end
%%
    
b = find(obj.valid, 1);
e = find(obj.valid, 1, 'last');

base = other.ROI.nZones;
baseVector = base.^(other.nSubjects-1:-1:0);
zoneidx = baseVector * (obj.zones - 1) + 1;

for i=b:e-1
    %%
    for s=1:obj.nSubjects
        z = obj.zones(s, i);
        if s==1
            m = mm.Allowed{s}(z, :);
        else
            m = m & mm.Allowed{s}(z, :);
        end
    end
    mp = zeros(1, length(m));
    mp(m) = obj.Analysis.Potts.Model{1}.jointProb(m);
    mp = mp / sum(mp);
    mp(zoneidx(i+1))
end

p = ones(1, sum(obj.valid));
idx = zeros(1, obj.nFrames);
for s=1:obj.nSubjects
    %%
    z = obj.zones(s, obj.valid);
    [z(1:end-1); z(2:end)];
    ind = sub2ind([obj.ROI.nZones, obj.ROI.nZones], z(1:end-1), z(2:end));
    p(2:end) = p(2:end) .* mm.TransProb{s}(ind);
    p(1) = p(1) .* mm.Prob{s}(z(1));
end
