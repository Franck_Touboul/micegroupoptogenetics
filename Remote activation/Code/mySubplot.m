function mySubplot(nrows, ncols, row, col)
if nargin == 3
    col = 1:ncols;
end
subplot(nrows, ncols, (row - 1) * ncols  + col);

