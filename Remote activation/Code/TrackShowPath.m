function TrackShowPath(obj, offmin, durmin)
%%
figOffset = 2;

duration = durmin * 60;  % [sec]
%offset =160 * 60;
offset = offmin * 60;

start = find(obj.valid, 1);
range = start + offset * obj.FrameRate:start + offset * obj.FrameRate + duration * obj.FrameRate;
%
figure(figOffset + 2);
clf
%
cmap = MyMouseColormap;
for s=1:obj.nSubjects
    r = false(1, length(obj.hidden));
    %r(range) = ~obj.sheltered(s, range);
    r(range) = true;
    [begF, endF] = FindEvents(r);
    for i=1:length(begF)
        figure(figOffset + 1);
        plot(obj.x(s, begF(i):endF(i)), max(obj.y(:)) - obj.y(s, begF(i):endF(i)), 'color', cmap(s, :)); hon;
        axis off
        set(gcf, 'Color', 'w')
        figure(figOffset + 2);
        subplot(2,2,s);
        plot(obj.x(s, begF(i):endF(i)), max(obj.y(:)) - obj.y(s, begF(i):endF(i)), 'color', cmap(s, :)); hon;
        axis off
        set(gcf, 'Color', 'w')
    end
end
figure(figOffset + 1);
hoff
%%
if 1==2
p = 'Graphs/TrackShowPath/';
mkdir(p);
figure(figOffset + 1);
saveFigure([p obj.FilePrefix '.joint']);
figure(figOffset + 2);
saveFigure([p obj.FilePrefix]);
end