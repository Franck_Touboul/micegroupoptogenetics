function res = followTrackWithROI(options, cents, curr, roi)
%%
options.jumpVar = 3^2;
options.confProb = 0.1;
options.hideJump = options.jumpVar;

table = cents.logprob(:,:,curr);
%% find sehelters
shelter = struct();
index = 1;
for i=find(roi.sheltered)
    %id = getZoneId(roi, ['(' roi.fields{i} ')']);
    curr = eval(['roi.' roi.fields{i}]);
    [y, x] = find(curr);
    x = x * options.scale;
    y = y * options.scale;
    shelter.x(index) = mean(x);
    shelter.y(index) = mean(y);
%     shelter.std.x(index) = options.jumpVar; %var(x);
%     shelter.std.y(index) = options.jumpVar; %var(y);
    shelter.std.x(index) = var(x);
    shelter.std.y(index) = var(y);
    index = index + 1;
end
shelter.count = length(shelter.x);
%%
cents.std.x = ones(1, size(cents.label, 2)) * options.jumpVar;
cents.std.y = ones(1, size(cents.label, 2)) * options.jumpVar;
index = 1;
for i=find(roi.sheltered)
    cents.x = [repmat(shelter.x(index), length(cents.x), 1), cents.x];
    cents.y = [repmat(shelter.y(index), length(cents.x), 1), cents.y];
    cents.std.x = [shelter.std.x(index), cents.std.x];
    cents.std.y = [shelter.std.y(index), cents.std.y];
    cents.label = [ones(length(cents.x), 1), cents.label];
    table = [zeros(length(cents.x), 1), table];
    index = index + 1;
end

%%
[N, nS] = size(cents.label);
dim = sum(cents.label ~= 0, 2);

%%
%covarMat = eye(2) * options.jumpVar;

table(cents.label == 0)    = nan;
table(:, nS+1) = flog(options.confProb);
table = table';

path = zeros(nS+1, N, 'uint8');
lastPos = [inf inf];
nValid = max([find(cents.label(1, :), 1, 'last'), 0]);
defaultCovarMat = eye(2) * options.jumpVar;

for i=2:N
    nPrevValid = nValid;
    nValid = max([find(cents.label(i, :), 1, 'last'), 0]);

    col = table(1:nPrevValid, i-1);
    hid = table(nS+1, i-1);
    vec = [cents.x(i-1, 1:nPrevValid); cents.y(i-1, 1:nPrevValid)];
    hide_trans = log_gauss([0, 0], defaultCovarMat, [options.hideJump, 0]);
    %jump_trans = log_gauss([0, 0], defaultCovarMat, [2 * options.hideJump, 0]);
    jump_trans = hide_trans;
    for j=1:nValid
        covarMat = [cents.std.x(j), 0; 0, cents.std.y(j)];
%        p = [gauss([cents.x(i, j), cents.y(i, j)], options.jumpVar, [cents.x(i-1, 1:nS); cents.y(i-1, 1:nS)]'); 1];
        p_jump = log_gauss([cents.x(i, j), cents.y(i, j)], covarMat, lastPos);
        if j<=shelter.count
            p = [ones(1,shelter.count)*hide_trans log_gauss([cents.x(i, j), cents.y(i, j)], covarMat, vec(:, shelter.count+1:end)')']';
            [val, from] = max([p + col; p_jump + hid]);
        else
            p = log_gauss([cents.x(i, j), cents.y(i, j)], covarMat, vec');
            [val, from] = max([p + col; p_jump + hid]);
        end
        if from > nPrevValid
            from = nS + 1;
        end
        path(j, i) = from;
        table(j ,i) = val + table(j ,i);
    end
    %%
    [val, from] = max(jump_trans + [col; hid]);
    if from > nPrevValid
        from = nS + 1;
    else
        lastPos = [cents.x(i-1, from), cents.y(i-1, from)];
    end
    path(nS + 1, i) = from;
    table(nS + 1, i) = val + table(nS + 1 ,i);
end

% backtrack 
backtrack = zeros(1, N);
[val, start] = max(table(:, end));
backtrack(end) = start;
res.x = zeros(1, N);
res.y = zeros(1, N);
res.prob = zeros(1, N);
res.id = zeros(1, N, 'uint8');

if backtrack(end) <= nS && backtrack(end) > shelter.count
    res.x(end) = cents.x(end, backtrack(end));
    res.y(end) = cents.y(end, backtrack(end));
    res.id(end) = backtrack(end);
else
    res.x(end) = nan;
    res.y(end) = nan;
    res.id(end) = 0;
end
res.prob(end) = table(end, backtrack(end));
for i=N-1:-1:1
    backtrack(i) = path(backtrack(i+1), i + 1);
    if backtrack(i) <= nS && backtrack(i) > shelter.count
        res.x(i) = cents.x(i, backtrack(i));
        res.y(i) = cents.y(i, backtrack(i));
        res.id(i) = backtrack(i);
    else
        res.x(i) = nan;
        res.y(i) = nan;
        res.id(i) = 0;
    end
    res.prob(i) = table(backtrack(i), i);
end

