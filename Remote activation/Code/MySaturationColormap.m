function cmap = MySaturationColormap()
%%
from = [1 1 1];
to = [170 55 73]/255;

cto = rgb2hsv(reshape(to, [1 1 3]));
cfrom = cto;
cfrom(2) = 0;
cfrom(3) = 1;
cmap = MyMakeColormap(cfrom(:)', cto(:)', 256);
cmap = hsv2rgb(reshape(cmap, [size(cmap, 1), 1, 3]));
cmap = reshape(cmap, [size(cmap, 1), 3, 1]);

