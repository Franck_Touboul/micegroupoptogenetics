fprintf('# reading movie\n');
xyloObj = mmreader('Trial     1.mpg');

nFrames = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;

% Preallocate movie structure.
mov(1:nFrames) = ...
    struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
           'colormap', []);

% Read one frame at a time.
fprintf('# setting frames... (    0.00% )');
for k = 1 : nFrames
    fprintf('\b\b\b\b\b\b\b\b\b\b\b( %6.2f%% )', k / nFrames * 100);
    mov(k).cdata = read(xyloObj, k);
end
fprintf('[done]\n');

% Size a figure based on the video's width and height.
hf = figure;
set(hf, 'position', [150 150 vidWidth vidHeight])

% Play back the movie once at the video's frame rate.
movie(hf, mov, 1, xyloObj.FrameRate);
