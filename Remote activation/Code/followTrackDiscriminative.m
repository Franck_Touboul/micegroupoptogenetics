function tracks = followTrackDiscriminative(options, cents)

[N, nS] = size(cents.label);
dim = sum(cents.label ~= 0, 2);

%%
options.jumpVar = eye(2) * 5^2;
options.confProb = 0.1;
options.hideProb = 0.1;

for curr=1:options.nSubjects
    t = zeros(N, nS+1);
    t(cents.label == curr) = 1;
    t(cents.label ~= curr) = options.confProb;
    t(cents.label == 0)    = nan;
    t(:, nS+1) = options.hideProb;
    table{curr} = flog(t');
    path{curr} = zeros(nS+1, N, 'uint8');
    lastPos{curr} = [inf, inf];
end

for i=2:N
    for curr=1:options.nSubjects
        col = table{curr}(:, i-1);
        for j=1:nS
            %        p = [gauss([cents.x(i, j), cents.y(i, j)], options.jumpVar, [cents.x(i-1, 1:nS); cents.y(i-1, 1:nS)]'); 1];
            vec = [cents.x(i-1, :) lastPos{curr}(1); cents.y(i-1, :) lastPos{curr}(2)];
            p = gauss([cents.x(i, j), cents.y(i, j)], options.jumpVar, vec');
            [val, from] = max(flog(p) + col);
            if from <= nS
                path{curr}(j, i) = from;
            else
                path{curr}(j, i) = nS + 1;
            end
            table{curr}(j ,i) = val + table{curr}(j ,i);
        end
        [val, from] = max(col);
        if from <= nS
            path{curr}(nS + 1, i) = from;
            lastPos{curr} = [cents.x(i-1, from), cents.y(i-1, from)];
        else
            path{curr}(nS + 1, i) = nS + 1;
        end
        table{curr}(nS + 1, i) = val + table{curr}(nS + 1 ,i);
    end
    
    for curr=1:options.nSubjects
        s{curr} = zeros(nS+1, 1);
        for other=1:options.nSubjects
            if other ~= curr
                s{curr} = s{curr} + table{other}(:, i);
            end
        end
    end
    for curr=1:options.nSubjects
        table{curr}(:, i) = table{curr}(:, i) - s{curr};
    end
end

% backtrack
for curr=1:options.nSubjects
    backtrack = zeros(1, N);
    [val, start] = max(table{curr}(:, end));
    backtrack(end) = start;
    res.x = zeros(1, N);
    res.y = zeros(1, N);
    
    if backtrack(end) <= nS
        res.x(end) = cents.x(end, backtrack(end));
        res.y(end) = cents.y(end, backtrack(end));
    else
        res.x(end) = nan;
        res.y(end) = nan;
    end
    for i=N-1:-1:1
        backtrack(i) = path{curr}(backtrack(i+1), i + 1);
        if backtrack(i) <= nS
            res.x(i) = cents.x(i, backtrack(i));
            res.y(i) = cents.y(i, backtrack(i));
        else
            res.x(i) = nan;
            res.y(i) = nan;
        end
    end
    tracks{curr} = res;
end