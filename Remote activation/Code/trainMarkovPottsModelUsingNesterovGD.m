function me = trainMarkovPottsModelUsingNesterovGD(options, data, confidence)
options.param_report = false;
options = setDefaultParameters(options, ...
    'Statistics', false, ...
    'KLConvergenceRatio', 1e-7, ...
    'tk', 1, 'nIters', 20000);

me.tk = options.tk;
me.alpha = .5;
me.beta = .5;
useLineSearch = true;
me.LineSearchMaxSteps = 50;
me.KLConvergenceRatio = options.KLConvergenceRatio;

options.NeutralZone = 1; % Open
%%
use_sparse = true;
if nargin < 3
    confidence = -.05;
end
me.confidence = confidence;
me.neutralZone = options.NeutralZone;

if isfield(options, 'Output')
    output = options.Output;
else
    options.Output = true;
    output = true;
end
%
if output; fprintf('# Training Potts Model\n'); end
%
if output; fprintf('# - finding all permutation of max dimension %d...\n', options.n); end;
%%
allPerms = nchoose(1:options.nSubjects);
nPerms = {};
j = 1;
for i=1:length(allPerms)
    if length(allPerms{i}) <= options.n 
        nPerms{j} = allPerms{i};
        j = j + 1;
    end
end

%%
if output; fprintf('# - computing constraints...\n'); end
[me.constraints, pci] = PottsComputeConstraints(data' + 1, options.count, nPerms, abs(confidence));
C = length(nPerms);
%
me.inputPerms = myPerms(options.nSubjects, options.count);
[me.perms, me.labels] = assignFeature(me.inputPerms, options.count, nPerms, use_sparse);

valid = false(1, length(me.labels));
% if options.count == 2
%     for i=1:length(me.labels)
%         if all(me.labels{i}(2, :) ~= 2)
%             valid(i) = true;
%         end
%     end
%     me.labels = me.labels(valid);
%     me.perms = me.perms(:, valid);
%     me.constraints = me.constraints(valid);
%     pci = pci(valid);
% end
%
me.nSubjects = options.nSubjects;
me.range = options.count;
me.momentum = randn(1, size(me.labels, 2)) / size(me.labels, 2);
orig = me;
%% filter neutral zone
neutral = false(1, length(orig.labels));
for l=1:length(me.labels)
    neutral(l) = any(orig.labels{l}(2, :) == options.NeutralZone);
end
me.constraints = orig.constraints(~neutral);
me.perms = orig.perms(:, ~neutral);
me.labels = orig.labels(~neutral);
% initialize the weights
if isfield(options, 'initialPottsWeights') && ~isempty(options.initialPottsWeights)
    me.momentum = options.initialPottsWeights;
else
    me.momentum = orig.momentum(~neutral);
end
%
me.weights = me.momentum;
pci = pci(~neutral, :);

%% compute empirical probabilities
base = me.range;
baseVector = base.^(size(data, 1)-1:-1:0);
pEmp = histc(baseVector * data+1, 1:base ^ size(data, 1));
pEmp = pEmp / sum(pEmp);
%% initial halfKL
p = exp(me.perms * me.weights');
Z = sum(p);
p = p / Z;
kl2 = -pEmp * log2(p);
kl1 = pEmp * log2(pEmp' + (pEmp' == 0));
%%
if options.Statistics; me.Statistics = struct(); end
tic;
me.converged = false;
me.nconverged = inf;
me.KLconverged = false;
me.LSconverged = false;
best = me;
ticTime = 0;
p = [];
for iter=1:options.nIters
    currTime = toc;
    if output
        fprintf('# - NestrovGD, order %d, iter (%4d/%4d) [x%d ps] : ', options.n, iter, options.nIters, round(1 / (currTime - ticTime)));
    end
    ticTime = currTime;
    % compute the (un-normalized) pdf for each sample
    if isempty(p)
        p = exp(me.perms * me.weights');
        % normalize the pdf (the Z)
        Z = sum(p);
        p = p / Z;
    end
    prev_kl = kl1 + kl2;
    kl2 = -pEmp * log2(p + (pEmp' == 0));
    me.Dkl = kl1 + kl2;
    E = p' * me.perms;
    p = [];
    if confidence ~= 0
        nconverged = sum(E'  > pci(:, 1) & E' < pci(:, 2));
        if output 
                fprintf('KL = %6.4f, convergence = %5.2f%%, KL-convergence = %6.4e', kl1 + kl2, (nconverged / length(me.momentum) * 100), abs((kl1 + kl2) - prev_kl)); 
        end
        if options.Statistics;
            me.Statistics(iter).convergence = nconverged / length(me.momentum);
        end
        me.nconverged = nconverged;
        if nconverged == length(me.momentum) && iter >= options.MinNumberOfIters 
            %%
            if output && ~me.converged;
                fprintf('\n # converged to confidence interval (alpha = %3.2f)\n', confidence);
            end
            me.converged = true;
            if me.KLConvergenceRatio > 0
                if abs((kl1 + kl2) - prev_kl) <= me.KLConvergenceRatio
                    if me.KLconverged
                        if output;
                            fprintf('\n# converged to KL convergence ratio (ratio = %3.2f)\n', me.KLConvergenceRatio);
                        end
                        best = me;
                        break;
                    else
                        me.KLconverged = true;
                    end
                elseif me.KLconverged
                    me.KLconverged = false;
                end
            else
                best = me;
                break;
            end
        end
        if me.nconverged > best.nconverged
            best = me;
            best.nIters = iter;
            best.Dkl = kl1 + kl2;
        end
    else
        if output; fprintf('KL = %6.4f (%.3f)', kl1 + kl2, abs((kl1 + kl2 - prev_kl)/prev_kl)); end
    end
    %%
    t = me.tk;
    pWeights = me.momentum;
    if useLineSearch
        df = (me.constraints - E);
        found = false;
        nsteps = 1;
        while ~found
            lsWeights = me.weights + t * df;
            lsMomentum = lsWeights + (iter - 1)/(iter+2) * (lsWeights - pWeights);
            p = exp(me.perms * lsMomentum');
            p = p / sum(p);
            found = -pEmp * log2(p) < kl2 - me.alpha * t * sum(df.^2);
            if ~found
                t = me.beta * t;
            end
            if nsteps >= me.LineSearchMaxSteps
                break;
            end
            nsteps = nsteps + 1;
        end
    end
%     if nsteps >= me.LineSearchMaxSteps
%         if output;
%             fprintf('\n# converged after reaching maximal number of line searches (limit = %d)\n', me.LineSearchMaxSteps);
%         end
%         me.LSconverged = true;
%         break;
%     end
    
    %fprintf('# line-search factor = 2^(%g)\n', log2(t));
    if output; fprintf(', alpha = %.2f', t); end;
    
    me.momentum = me.weights + t * (me.constraints - E);
    me.weights = me.momentum + (iter - 1)/(iter+2) * (me.momentum - pWeights);

    if confidence == 0
        best = me;
    end
    
    %%
    if options.Statistics;
        me.Statistics(iter).times = toc; 
        me.Statistics(iter).KL = kl1 + kl2;
    end
    %%
    if output; fprintf('\n'); end
end
me = best;
me.nIters = iter;
me.Dkl = kl1 + kl2;
% if me.nconverged < best.nconverged || (me.nconverged == best.nconverged && me.Dkl > best.Dkl)
%     me = best;
% end

%me = rmfield(me, 'features');
