function obj = TrackSetZones(obj)
%%
sheltered{1} = '';
regions{1} = 'Open';
for r=1:obj.ROI.nRegions
    regions{r} = obj.ROI.RegionNames{r};
    sheltered{r} = ['(' obj.ROI.RegionNames{r} ')'];
end
%%
for s=1:obj.nSubjects
    for r=1:obj.ROI.nZones
        %%
        x = min(max(round(obj.x(s, :)), 1), obj.VideoWidth);
        y = min(max(round(obj.y(s, :)), 1), obj.VideoHeight);
        zoneID = strcmp(obj.ROI.ZoneNames{r}, regions);
        inshelter = false;
        if ~any(zoneID)
            zoneID = strcmp(obj.ROI.ZoneNames{r}, sheltered);
           inshelter = true;
        end
        if ~any(zoneID)
            continue;
        end
        if inshelter;
            obj.zones(s, obj.sheltered(s,:) & obj.ROI.Regions{zoneID}(sub2ind(size(obj.ROI.Regions{zoneID}), y, x))) = r;
        else
            obj.zones(s, ~obj.sheltered(s,:) & obj.ROI.Regions{zoneID}(sub2ind(size(obj.ROI.Regions{zoneID}), y, x))) = r;
        end
    end
    
end