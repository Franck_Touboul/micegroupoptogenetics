function obj = SocialPackageObjForPotts(obj)
temp = struct();
fields = {'zones', 'nSubjects', 'ROI', 'valid', 'nFrames', 'FilePrefix', 'OutputPath', 'Output', 'OutputToFile', 'OutputInOldFormat'};
for i=1:length(fields)
    f = fields{i};
    temp.(f) = obj.(f);
end
obj = temp;