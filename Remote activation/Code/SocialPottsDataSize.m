%function SocialPottsDataSize(obj)
%%
Options.ShrinkFactor = .9;
Options.nShrinks = 40;
Options.nIters = 10;
%%
obj.OutputToFile = false;
obj.OutputInOldFormat = false;
obj.Output = true;
%%
r = randperm(obj.nFrames); 
ind1 = r(1:ceil(obj.nFrames/2));
map1 = false(1, obj.nFrames);
map1(ind1) = true;
map2 = ~map1;
ind2 = find(map2);
%%
obj1 = obj;
obj1.nFrames = sum(map1);
obj1.valid = obj.valid(map1);
obj1.zones = obj.zones(:, map1);
obj1 = SocialPotts(obj1);
%%
s_ = 0:Options.nShrinks;
idx = 1;
data = [];
for s = s_
    niters = min(s+1, Options.nIters);
    fprintf('#--------------------------------------------------------\n');
    fprintf('# shrink no. %02d\n', s);
    for iter=1:niters
        %%
        try
        r = randperm(length(ind2)); 
        currind = ind2(r(1:floor(obj.nFrames/2 * Options.ShrinkFactor^s)));
        currmap = false(1, obj.nFrames);
        currmap(currind) = true;
        fprintf('#  iter no. %0d/%2d: %d/%d samples\n', iter, niters, sum(currmap), sum(map2));
        %

        obj2 = obj;
        obj2.nFrames = sum(currmap);
        obj2.valid = obj.valid(currmap);
        obj2.zones = obj.zones(:, currmap);
        obj2 = SocialPotts(obj2);
        
        %
        for i=1:obj.nSubjects
            data(idx).order = i;
            data(idx).probs = obj2.Analysis.Potts.Model{i}.prob;  
            data(idx).nSamples = sum(currmap);  
            idx = idx + 1;
        end
        catch me
            warning(me.message);
        end
    end
end
if isunix
	save(['mat/SocialPottsDataSize.' obj.FilePrefix], 'data');
else
	save(['d:/SocialPottsDataSize.' obj.FilePrefix], 'data');
end

