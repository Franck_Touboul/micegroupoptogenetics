function obj = SocialPrediction(obj)
SocialExperimentData;
obj = TrackLoad(obj);
objs = TrackGroupLoad(obj, 1:GroupsData.nDays, 'Res/');

Obj = struct();
Obj.zones = {};

f = fieldnames(objs);
idx = 1;
for i=1:length(f)
    obj = objs.(f{i});
    Obj.zones{idx} = obj.zones;
    idx = idx + 1;
end
Obj.rank =  obj.Hierarchy.Group.rank;


%%
steps = 2.^(0:10);
dt_ = [-steps(end:-1:1) 0 steps];
seq = 1:obj.nSubjects;
stat = struct();
count = 0;

mutualInfoFunc = @ComputeExactMutualInformation;
%mutualInfoFunc = @ComputeMutualInformation;

for dt=dt_
    for s=1:obj.nSubjects
        count = Reprintf(count, '# subject %d, delay %d (%d / %d)', s, dt, find(dt == dt_), length(dt_));
        zonesX = [];
        zonesY = [];
        if dt >= 0
            for i=1:length(Obj.zones)
                zonesX = [zonesX, Obj.zones{i}(s, 1:end-dt)];
                zonesY = [zonesY, Obj.zones{i}(seq(seq ~= s), 1+dt:end)];
            end
%            [I, H] = mutualInfoFunc(obj.zones(s, 1:end-dt),obj.zones(seq(seq ~= s), 1+dt:end));
        else
            for i=1:length(Obj.zones)
                zonesX = [zonesX, Obj.zones{i}(s, abs(dt)+1:end)];
                zonesY = [zonesY, Obj.zones{i}(seq(seq ~= s), 1:end-abs(dt))];
            end
%            [I, H] = mutualInfoFunc(obj.zones(s, abs(dt)+1:end),obj.zones(seq(seq ~= s), 1:end-abs(dt)));
        end
        [I, H] = mutualInfoFunc(zonesX, zonesY);
        stat.I(s, dt == dt_) = I;
        stat.H1(s, dt == dt_) = H(1);
        stat.H2(s, dt == dt_) = H(2);
    end
end
fprintf('\n');
%%
% obj.Prediction = stat;
% obj.Prediction.times = dt_;
% TrackSave(obj);
fprintf(['@name ' obj.FilePrefix '\n']);
fprintf('@times '); fprintf('%f ', dt_); fprintf('\n');
for i=1:size(stat.I, 1)
    fprintf('@I '); fprintf('%f ', stat.I(i, :)); fprintf('\n');
end
for i=1:size(stat.I, 1)
    fprintf('@H1 '); fprintf('%f ', stat.H1(i, :)); fprintf('\n');
end
for i=1:size(stat.I, 1)
    fprintf('@H2 '); fprintf('%f ', stat.H2(i, :)); fprintf('\n');
end

%%
cmap = MyMouseColormap;
socialStat = SocialRankToStatus(obj);
for s=1:obj.nSubjects
    subplot(2,1,1);
    plot(dt_, stat.I(s, :), '.-', 'color', cmap(s, :), 'LineWidth', 2)
    hon;
    plot(-dt_(dt_ > 0), stat.I(s, (dt_ > 0)), ':', 'color', cmap(s, :), 'LineWidth', 2,'HandleVisibility','off')
    subplot(2,1,2);
    plot(dt_, stat.I(s, :)./stat.H1(s, :)*100, '.-', 'color', cmap(s, :), 'LineWidth', 2)
    hon;
    plot(-dt_(dt_ > 0), stat.I(s, (dt_ > 0))./stat.H1(s, (dt_ > 0))*100, ':', 'color', cmap(s, :), 'LineWidth', 2,'HandleVisibility','off')
end
subplot(2,1,1);
legend(socialStat);
legend boxoff;
xaxis(dt_(1), dt_(end));
set(gca, 'XTickLabel', get(gca, 'XTick') / obj.FrameRate);
xlabel('time lag [secs]');
ylabel('mutual information [bits]');
hoff;
subplot(2,1,2);
legend(socialStat);
legend boxoff;
xaxis(dt_(1), dt_(end));
set(gca, 'XTickLabel', get(gca, 'XTick') / obj.FrameRate);
xlabel('time lag [secs]');
ylabel('explained information I(x_i|x_j x_k x_l)/H(x_i) [%]');
hoff;
