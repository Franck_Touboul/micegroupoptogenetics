SocialExperimentData
options.Day = 1:4;
res = [];
res.entropy = [];
res.rank = [];
for id=1:GroupsData.nExperiments
    %%
    h = zeros(GroupsData.nSubjects, 10);
    for d = options.Day
        obj = TrackLoad({id d}, {'zones', 'valid', 'ROI', 'Hierarchy'});
        g = GroupsData.group(id);
        %%
        for s=1:obj.nSubjects
            %%
            h(s, :) = h(s, :) + histc(obj.zones(s, obj.valid), 1:obj.ROI.nZones);
%            h = histc(obj.zones(s, obj.valid), 1:obj.ROI.nZones);
        end
    end
    p = h ./ repmat(sum(h, 2), 1, obj.ROI.nZones);
    res.entropy(id, :) = diag(-p * log2(p' + (p' == 0)))';
    
    res.rank(id, :) = obj.Hierarchy.Group.AggressiveChase.rank;
end
%%
for id=1:GroupsData.nExperiments
    g = GroupsData.group(id);
    rank = res.rank(id, :);
    entropy = res.entropy(id, :);
    status = max(rank) - rank + 1;
    status(status > 3) = 3;
    plot([min(entropy) max(entropy)], [id id], 'color', GroupsData.Colors(g, :)); hon
    for s=1:obj.nSubjects
        plot(entropy(s), id, 'o', 'MarkerFaceColor', ones(1, 3) * (status(s) - 1)/2, 'MarkerEdgeColor', 'k');
    end
end

hoff;
%%
res.status = repmat(max(res.rank, [], 2), 1, obj.nSubjects) - res.rank + 1;
idx = 1;
for g=1:GroupsData.nGroups
    map = GroupsData.group == g;
    r = res.status(map, :);
    e = res.entropy(map, :);
    for i=1:3
        E = e(r == i);
        errorbar(idx, mean(E), stderr(E));
        hon
        idx = idx + 1;
    end
end
hoff