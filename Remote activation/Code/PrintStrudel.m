function PrintStrudel(var, data)
sz = size(data);
szstr = '';
szstr = num2str(sz(1));
for i=2:length(sz)
    szstr = [szstr, ',', num2str(sz(i))];
end

fprintf('@%s[%s(%s)] =', var, class(data), szstr);
switch class(data)
    case {'double'}
        fprintf(' %.6g', data);
end

fprintf('\n');
