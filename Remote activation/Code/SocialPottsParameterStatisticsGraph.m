titles = {'independent', 'pairs', 'triplets', 'quadruplets'};
res.m = {};
res.err = {};
for day=1:4
    for i=1:4
        if day==1
            res.m{i} = [];
            res.err{i} = [];
        end
        res.m{i} = [res.m{i}; all{day}{i}.mean];
        res.err{i} = [res.err{i}; all{day}{i}.stderr];
    end
end
%%
for i=1:4
    subplot(4,1,i);
    for s=1:4
        errorbar(1:4, res.m{i}(:, s)', res.err{i}(:, s)', 's--', 'MarkerFaceColor', obj.Colors.Centers(s, :), 'Color', obj.Colors.Centers(s, :)); hold on;
    end
    hold off;
    set(gca, 'XTick', 1:4);
    if i==1;
        title({'Enriched.exp0001.cam04';['average weight: ' titles{i}]});
    else
        title(['average weight: ' titles{i}]);
    end
    if i==4;
        xlabel('day');
    end
end