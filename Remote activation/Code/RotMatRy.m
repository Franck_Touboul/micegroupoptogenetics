function m = RotMatRy(theta)
ct = cos(theta);
st = sin(theta);
m = [ct 0 st; 0 1 0; -st 0 ct];
