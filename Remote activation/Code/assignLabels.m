function res = assignLabels(res)
% 

for i=1:length(res)
    for j=i+1:length(res)
        match = res{i}.id == res{j}.id & res{i}.id ~= 0;
        d = diff([0 match 0], 1, 2);
        start  = find(d > 0);
        finish = find(d < 0) - 1;
        for k=1:length(start)
            p1 = res{i}.prob(finish(k));
            p2 = res{j}.prob(finish(k));
            if start(k) > 1
                p1 = p1 - res{i}.prob(start(k) - 1);
                p2 = p2 - res{j}.prob(start(k) - 1);
            end
            if p1 > p2
                res{j}.id(start(k):finish(k)) = 0;
                res{j}.x(start(k):finish(k)) = nan;
                res{j}.y(start(k):finish(k)) = nan;
            else
                res{i}.id(start(k):finish(k)) = 0;
                res{i}.x(start(k):finish(k)) = nan;
                res{i}.y(start(k):finish(k)) = nan;
            end
        end
    end
end
