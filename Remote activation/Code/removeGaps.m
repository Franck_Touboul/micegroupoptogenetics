function data = removeGaps(data, minGap)
c = conv([0 data(:)' 0], [1 -1]);
startTimes = find(c > 0) - 1;
endTimes = find(c < 0) - 2;
gaps = (endTimes - startTimes) < minGap;
startTimes = startTimes(gaps);
endTimes = endTimes(gaps);
for k=1:length(startTimes)
    data(startTimes(k):endTimes(k)) = 0;
end
