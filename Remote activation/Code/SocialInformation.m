function [MI, JointEntropy, Entropy] = SocialInformation(obj)
%%
seq = 1:obj.nSubjects;
MI = zeros(obj.nSubjects);
JointEntropy = zeros(obj.nSubjects);
Entropy = zeros(1, obj.nSubjects);
for s=1:obj.nSubjects
    h = hist(obj.zones(s, obj.valid), 1:obj.ROI.nZones);
    p = h / sum(h);
    Entropy(s) = -p(:)' * log2(p(:) + (p(:) == 0)); 
    %%
    others = obj.zones(seq(seq ~= s), obj.valid);
    vec = obj.ROI.nZones.^(obj.nSubjects-2:-1:0);
    bin = vec * (others - 1) + 1;
    h = hist3([bin; obj.zones(s, obj.valid)]', {1:obj.ROI.nZones^(obj.nSubjects-1), 1:obj.ROI.nZones});
    p = h / sum(h(:));
    %%
    ratio = p ./ (repmat(sum(p, 1), size(p, 1), 1) .* repmat(sum(p, 2), 1, size(p, 2)));
    ratio(p == 0) = 1;
    MI(s, s) = p(:)' * log2(ratio(:));
    JointEntropy(s, s) = -p(:)' * log2(p(:) + (p(:) == 0));
    %%
    for s2=seq(seq ~= s)
        h = hist3([obj.zones(s2, obj.valid); obj.zones(s, obj.valid)]', {1:obj.ROI.nZones, 1:obj.ROI.nZones});
        p = h / sum(h(:));
        ratio = p ./ (repmat(sum(p, 1), size(p, 1), 1) .* repmat(sum(p, 2), 1, size(p, 2)));
        ratio(p == 0) = 1;
        mi = p(:)' * log2(ratio(:));
        MI(s, s2) = mi;
        MI(s2, s) = mi;
        je = -p(:)' * log2(p(:) + (p(:) == 0));
        JointEntropy(s, s2) = je;
        JointEntropy(s2, s) = je;
    end
end
