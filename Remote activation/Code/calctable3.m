fprintf('# - tracking subject %d of %d\n', curr, options.nSubjects);
    currLogprobs = cents.logprob(:, :, curr);
    
    table = ones(options.nSubjects+1, length(res{curr}.id)) * -inf;
    path = ones(size(table), 'uint16') * (options.nSubjects + 1);
    for s=1:options.nSubjects
        prevPos.x(s) = xyloObj.Width/2 * options.scale;
        prevPos.y(s) = xyloObj.Height/2 * options.scale;
        
        range = 1:ndata;
        centids = res{s}.centids;
        
        range = range(~isnan(centids));
        centids = centids(~isnan(centids));
        
        table(s, range) = currLogprobs(sub2ind(size(currLogprobs), range, centids));
    end
    prevPos.x(options.nSubjects+1) = xyloObj.Width/2 * options.scale;
    prevPos.y(options.nSubjects+1) = xyloObj.Height/2 * options.scale;
    table(options.nSubjects+1, :) = flog(prod(1 - exp(table(1:options.nSubjects, :)), 1));
    
    valid = sum(isfinite(table), 1) > 1;
    d = diff([0 valid 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    
%     %%
%     prevPos = struct();
%     currPos = struct();
%     gamma = -flog(res{curr}.meanDistance);
%     for n=1:length(start)
%         for s=1:options.nSubjects
%             prevPos.x(s) = res{s}.x(start(n));
%             prevPos.y(s) = res{s}.y(start(n));
%         end
%         r = start(n)+1:min(finish(n)+1, nframes);
%         for f=r
%             %
%             for s=1:options.nSubjects
%                 if ~isfinite(table(s, f))
%                     continue;
%                 end
%                 x = res{s}.x(f);
%                 y = res{s}.y(f);
%                 distance = sqrt((x - prevPos.x).^2 + (y - prevPos.y).^2)';
%                 trans = [gamma + (-distance/res{curr}.meanDistance); 0];
%                 [val, from] = max(table(:, f-1) + trans);
%                 table(s, f) = table(s, f) + val;
%                 path(s, f) = from;
%                 currPos.x(s) = x;
%                 currPos.y(s) = y;
%             end
%             %
%             [val, from] = max(table(:, f-1));
%             table(end, f) = table(end, f) + val;
%             path(end, f) = from;
%             %
%             prevPos = currPos;
%         end
%     end
    %%
    if 1 == 2
        prevPos.x = ones(1, options.nSubjects) * inf;
        prevPos.y = ones(1, options.nSubjects) * inf;
        currPos = struct();
        gamma = -flog(res{curr}.meanDistance);
        for n=1:length(start)
            r = start(n):finish(n);
            if n>1
                prevColn = table(:, finish(n-1));
            else
                prevColn = ones(options.nSubjects+1, 1) * -inf;
                prevColn(end) = 0;
            end
            for f=r
                %
                for s=1:options.nSubjects
                    if ~isfinite(table(s, f))
                        continue;
                    end
                    x = res{s}.x(f);
                    y = res{s}.y(f);
                    %                 distance = sqrt((x - prevPos.x).^2 + (y - prevPos.y).^2)';
                    %                 trans = [gamma + (-distance/res{curr}.meanDistance); 0];
                    distance = [sqrt((x - prevPos.x).^2 + (y - prevPos.y).^2)'; res{curr}.meanDistance * options.hidePenalty];
                    trans = gamma + (-distance/res{curr}.meanDistance);
                    [val, from] = max(prevColn + trans);
                    table(s, f) = table(s, f) + val;
                    path(s, f) = from;
                    currPos.x(s) = x;
                    currPos.y(s) = y;
                end
                %
                trans = gamma + (-options.hidePenalty);
                [val, from] = max(prevColn);
                table(end, f) = table(end, f) + val + trans;
                path(end, f) = from;
                %
                prevPos = currPos;
                prevColn = table(:, f);
            end
        end
    else
        prevPos.x = ones(1, options.nSubjects + 1) * inf;
        prevPos.y = ones(1, options.nSubjects + 1) * inf;
        currPos = prevPos;
        gamma = -flog(res{curr}.meanDistance);
        for n=1:length(start)
            r = start(n):finish(n);
            if n>1
                prevColn = table(:, finish(n-1));
            else
                prevColn = ones(options.nSubjects+1, 1) * -inf;
                prevColn(end) = 0;
            end
            for f=r
                %
                for s=1:options.nSubjects
                    if ~isfinite(table(s, f))
                        continue;
                    end
                    x = res{s}.x(f);
                    y = res{s}.y(f);
                    %                 distance = sqrt((x - prevPos.x).^2 + (y - prevPos.y).^2)';
                    %                 trans = [gamma + (-distance/res{curr}.meanDistance); 0];
                    distance = sqrt((x - prevPos.x).^2 + (y - prevPos.y).^2)';
                    if ~isfinite(distance(options.nSubjects + 1))
                        distance(options.nSubjects + 1) = res{curr}.meanDistance * options.hidePenalty;
                    end
                    trans = gamma + (-distance/res{curr}.meanDistance);
                    [val, from] = max(prevColn + trans);
                    table(s, f) = table(s, f) + val;
                    path(s, f) = from;
                    currPos.x(s) = x;
                    currPos.y(s) = y;
                end
                %
                trans = gamma + (-options.hidePenalty);
                [val, from] = max(prevColn);
                table(end, f) = table(end, f) + val + trans;
                path(end, f) = from;
                if from <= options.nSubjects
                    currPos.x(options.nSubjects + 1) = res{from}.x(f-1);
                    currPos.y(options.nSubjects + 1) = res{from}.y(f-1);
                else
                    currPos.x(options.nSubjects + 1) = prevPos.x(options.nSubjects + 1);
                    currPos.y(options.nSubjects + 1) = prevPos.y(options.nSubjects + 1);
                end
                %
                prevPos = currPos;
                prevColn = table(:, f);
            end
        end
        
    end
    
    %% in case we want to continue from prev place
%     currPos.x = ones(1, options.nSubjects + 1) * nan;
%     currPos.y = ones(1, options.nSubjects + 1) * nan;
%     gamma = -flog(res{curr}.meanDistance);
%     for n=1:length(start)
%         currPos.x = ones(1, options.nSubjects + 1) * nan;
%         currPos.y = ones(1, options.nSubjects + 1) * nan;
%         
%         r = start(n):min(finish(n)+1, nframes);
%         for f=r
%             prevPos = currPos;
%             %
%             for s=1:options.nSubjects
%                 if ~isfinite(table(s, f))
%                     continue;
%                 end
%                 x = res{s}.x(f);
%                 y = res{s}.y(f);
%                 distance = sqrt((x - prevPos.x).^2 + (y - prevPos.y).^2)';
%                 trans = gamma + (-distance/res{curr}.meanDistance);
%                 trans(isnan(trans)) = 0;
%                 [val, from] = max(table(:, f-1) + trans);
%                 table(s, f) = table(s, f) + val;
%                 path(s, f) = from;
%                 currPos.x(s) = x;
%                 currPos.y(s) = y;
%             end
%             %
%             [val, from] = max(table(:, f-1));
%             table(end, f) = table(end, f) + val;
%             path(end, f) = from;
%             currPos.x(options.nSubjects + 1) = prevPos.x(from);
%             currPos.y(options.nSubjects + 1) = prevPos.y(from);
%         end
%     end
    %% backtrack
    fprintf('#  . backtracking\n');
    track{curr}.src = ones(1, size(table, 2)) * (options.nSubjects + 1);
    track{curr}.logprob = zeros(1, size(table, 2));
    track{curr}.x = zeros(1, size(table, 2));
    track{curr}.y = zeros(1, size(table, 2));

    for n=1:length(start)
        r = start(n):finish(n);
        if n<length(start)
            track{curr}.logprob(finish(n)) = table(track{curr}.src(start(n+1)), start(n+1));
            track{curr}.src(finish(n)) = path(track{curr}.src(start(n+1)), start(n+1));
        else
            [track{curr}.logprob(finish(n)), track{curr}.src(finish(n))] = max(table(:, finish(n)));
        end
        for f=finish(n)-1:-1:start(n)
            track{curr}.logprob(f) = table(track{curr}.src(f+1), f+1);
            track{curr}.src(f) = path(track{curr}.src(f+1), f+1);
        end
    end
    
    for i=1:length(track{curr}.src)
        src = track{curr}.src(i);
        if src <= options.nSubjects
            track{curr}.x(i) = res{src}.x(i);
            track{curr}.y(i) = res{src}.y(i);
        else
            track{curr}.x(i) = nan;
            track{curr}.y(i) = nan;
        end
    end
    track{curr}.valid = track{curr}.src <= options.nSubjects;
    track{curr}.id = track{curr}.valid * curr;