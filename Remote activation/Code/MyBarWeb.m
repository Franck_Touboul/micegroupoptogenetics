function MyBarWeb(x, y, err, colors)
BarWidth = 0.8;
%%
w = BarWidth / size(y, 1);
for i=1:size(y, 1)
    bar(x-BarWidth/2 + w/2 + w*(i-1), y(i, :), w, 'FaceColor', colors(i, :));
    hold on;
end
for i=1:size(y, 1)
    errorbar(x-BarWidth/2 + w/2 + w*(i-1), y(i, :), err(i, :), 'k.');
end
hold off;
set(gca, 'XTick', x);