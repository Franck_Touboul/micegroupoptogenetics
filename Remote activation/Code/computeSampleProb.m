function [sampleProbIndep, sampleProbJoint] = computeSampleProb(data, range, independentProbs, jointProbs)

if nargin < 3
    % compute independent probabilities
    independentProbs = computeIndependentProbs(data, range);
end

if nargin < 4
    % compute joint probabilities
    jointProbs = computeJointProbs(data, range);
end

% independent
[nSubjects, nData] = size(data);
sampleProbIndep = ones(1, nData);
for i=1:nSubjects
    currProbs = independentProbs(i, :);
    sampleProbIndep = sampleProbIndep .* currProbs(data(i, :) + 1);
end
% joint
baseVector = range.^[0:nSubjects-1];
h = baseVector * data + 1;
sampleProbJoint = jointProbs(h);
