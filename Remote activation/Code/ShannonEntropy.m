function h = ShannonEntropy(P)
h = -P * flog2(P)';
