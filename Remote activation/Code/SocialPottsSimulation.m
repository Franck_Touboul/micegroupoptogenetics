function local = SocialPottsSimulation(obj, order, N)
if ~exist('N', 'var')
    N = 1000000;
end
level = order;
%%
me = obj.Analysis.Potts.Model;
p = exp(me{level}.perms * me{level}.weights');
Z = sum(p);
p = p / Z;

cp = cumsum(p);
%%
r = rand(1, N);
[h, bin] = histc(r, [0; cp(1:end-1)]);
bin = bin(bin ~= 0);
z = me{level}.inputPerms(bin, :)';
z = z(:, randperm(size(z, 2)));
%%
local = obj;
local.zones = z;
local.nFrames = size(z, 2);
local.valid = true(1, local.nFrames);
local.Analysis = [];
local.OutputToFile = false;
local.OutputInOldFormat = false;
%local = SocialPotts(local);

% pe = h' / sum(h);
% JensenShannonDivergence(p', pe')
% KullbackLeiblerDivergence(p', pe')
% 
% local = obj;
% local.n = level;
% local.output = true;
% local.count = count;
% local.nIters = 200;
% data = z - 1;
% newme = trainPottsModel(local, data, confidence);