function [features, labels] = assignDelayedFeature(data, range, nPerms, delay)
if nargin < 4
    use_sparse = false;
end

dim = 0;
for i=1:length(nPerms)
    nVals = range^length(nPerms{i});
    dim = dim + nVals;
end
features = false(size(data, 1), double(dim));

offset = 1;
index = 1;
labels = cell(1, dim);
seq = 1:size(data, 1);
for i=1:length(nPerms)
    nVals = range^length(nPerms{i});
    baseVector = range.^(length(nPerms{i})-1:-1:0);
     cdata = data(:, nPerms{i}) - 1;
     if length(nPerms{i}) > 1
        cdata(:, 1) = [cdata(delay:end, 1); ones(delay-1, 1) * cdata(end, 1)];
     end
    
    v = cdata * baseVector'; 
    features(sub2ind(size(features), seq(~isnan(v)), offset + v(~isnan(v))')) = true;
    %features(~isnan(v), offset + v(~isnan(v))) = true;
    offset = offset + nVals;

    for j=0:nVals-1
        values = decimal2base(j, range, length(nPerms{i})) + 1;
        labels{index} = [nPerms{i}; values];
        index = index + 1;
    end
end

features = sparse(features);

