function SocialPottsAllDays(obj)
%%
data = TrackParseName(obj);
SocialExperimentData;
found = false;
id = 0;
for i=1:GroupsData.nExperiments
    curr = TrackParseName(sprintf(GroupsData.Experiments{i}, 1));
    if strcmp(curr.Type, data.Type) && curr.GroupID == data.GroupID && curr.CameraID == data.CameraID
        found = true;
        id = i;
        break;
    end
end
%%
if found
    objs = TrackLoad({id, 2:GroupsData.nDays});
    TrackSave(objs);    
end
