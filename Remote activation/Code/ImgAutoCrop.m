function img = ImgAutoCrop(img, margin)
if nargin == 1
    margin = 10;
end


m = sum(img ~= 255, 3);
h1 = max(find(sum(m, 1), 1, 'first') - margin, 1);
h2 = min(find(sum(m, 1), 1, 'last') + margin, size(img, 2));
% v1 = max(find(sum(m, 2), 1, 'first') - margin, 1);
% v2 = min(find(sum(m, 2), 1, 'last') + margin, size(img, 1));
v1 = 1;
v2 = size(img, 1);
img = img(v1:v2, h1:h2, :);