function obj = SocialPottsWithRegularization(obj)
local = TrackLoad(obj);
local.OutputToFile = false;
local.OutputInOldFormat = false;
local = SocialPotts(local, struct('InParallel', false, 'Regularize', true));
local.Analysis.Regularized = local.Analysis.Potts;
try
    local.Analysis.Potts = obj.Analysis.Potts;
catch
end
TrackSave(local);

