function isRemoved = SocialShowHierarchy(obj, options)
%%
if ~exist('options', 'var')
    options = [];
end
options = setDefaultParameters(options, 'isByDay', true, 'isNormalized', true);
if options.isNormalized
    options = setDefaultParameters(options, ...
        'NodeSize', 25, ...
        'EdgeMinWidth', .25, ...
        'EdgeMaxWidth', 20, ...
        'EdgeMaxValue', 1, ...
        'EdgeShift', .075);
else
    options = setDefaultParameters(options, ...
        'NodeSize', 25, ...
        'EdgeMinWidth', .25, ...
        'EdgeMaxWidth', 8, ...
        'EdgeMaxValue', 50, ...
        'EdgeShift', .075);
end
cmap = MyMouseColormap;
dx = [0 1 -1 0];
titles = {'\alpha', '\beta', '\gamma', '\delta'};
H = obj.Hierarchy.Group.AggressiveChase;
isRemoved = false;
for day=1:length(H.Days)+1
    %%
    %subplot(1,length(H.Days),day);
    offset = (day - 1) * 3;
    if day > length(H.Days)
        h = H;
        level = max(H.rank) - H.rank + 1;
        offset = offset + 1;
    else
        h = H.Days(day);
        %%
        if options.isByDay
            ce = h.ChaseEscape;
            mat = ce - ce';
            %mat(binotest(ce, ce + ce')) = 0;
            mat = mat .* (mat > 0);
            %mat(mat <= 2) = 0;
            [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
            removed;
            level = max(rank) - rank + 1;
        else
            if day == 1
                %%
                CE = H.ChaseEscape;
                CE(eye(size(CE)) > 0) = nan;
                m = CE(~isnan(CE));
                CE(~isnan(CE)) = m(randperm(length(m)));
                CE(eye(size(CE)) > 0) = 0;
                MAT = CE - CE';
                MAT(binotest(CE, CE + CE')) = 0;
                MAT = MAT .* (MAT > 0);
                [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(MAT);
                isRemoved = isRemoved | any(removed(:) > 0);
            end
            level = max(H.rank) - H.rank + 1;
            %level = max(rank) - rank + 1;
            
            %         ce = h.ChaseEscape;
            %         mat = ce - ce';
            %         mat(binotest(ce, ce + ce')) = 0;
            %         mat = mat .* (mat > 0);
            %         count = 0;
            %         for i=1:obj.nSubjects
            %             count = count + sum(level(mat(i, :) > 0) <= level(i));
            %         end
            %         count
        end
    end
    
    X = [];
    Y = [];
    for l=unique(level)
        map = level==l;
        idx = 0;
        for i=find(map)
            a = 2 * mod(l, 2) - 1;
            X(i) = offset + dx(l) + a * idx;
            Y(i) = -l;
            idx = idx + 1;
        end
    end
    for i=1:obj.nSubjects
        for j=1:obj.nSubjects
            if h.ChaseEscape(i, j) > 0
                %plot([X(i) X(j)], [Y(i) Y(j)], '-', 'Color', [.2 .2 .2], 'LineWidth', h.Contacts(i, j) / options.EdgeMaxValue * options.EdgeMaxWidth); hon
                vec = [X(i)-X(j) Y(i)-Y(j)];
                edgeFactor = 2 / (1 + h.ChaseEscape(i, j) / h.ChaseEscape(j, i));
                if isnan(edgeFactor); 
                    edgeFactor = 0; 
                end;
                es = options.EdgeShift * edgeFactor;
                vec = vec / sqrt(vec*vec') * (es * (h.ChaseEscape(j, i) > 0));
                if day == 2 && i == 3
                    disp 'a'
                end
                if options.isNormalized
                    width = h.ChaseEscape(i, j) / h.Contacts(i, j) / options.EdgeMaxValue * (options.EdgeMaxWidth);
                else
                    width = h.ChaseEscape(i, j) / options.EdgeMaxValue * (options.EdgeMaxWidth);
                end
                if width == 0
                    continue;
                end
                width = min(width, options.EdgeMaxWidth);
                width = max(width, options.EdgeMinWidth);
                if day == 2 && i == 3
                    [width h.ChaseEscape(i, j) / h.Contacts(i, j)]
                end
                plot([X(i) X(j)] + vec(2), [Y(i) Y(j)] - vec(1), '-', 'Color', cmap(i, :), 'LineWidth', width); hon
            end
        end
    end
    for i=1:obj.nSubjects
        plot(X(i), Y(i), 'o', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', cmap(i, :), 'MarkerSize', options.NodeSize); hon;
        text(X(i), Y(i), titles{level(i)}, 'verticalAlignment', 'middle', 'horizontalalignment', 'center');
    end
    %axis([-1 1 -1 1]);
end
set(gcf, 'Color', 'w', 'Units', 'normalized');
axis off;
axis equal
hoff;
%%


