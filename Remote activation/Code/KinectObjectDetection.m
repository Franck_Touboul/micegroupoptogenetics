function cents = KinectObjectDetection(obj, z)

blobs = z*1000;
blobs = - (blobs - nanmedian(blobs(:)));
blobs(isnan(blobs)) = 0;
blobs = int32(blobs);
output = false;
%imagesc(bwlabeln(blobs - (obj.Meta.HeightThresh * 1000) > 0));

minNumPixels = (obj.Meta.MinInitEquivDiameter / obj.Meta.Plane.Resolution) ^2 * pi;
cents = regionprops(...
    bwareaopen(blobs - (obj.Meta.HeightThresh * 1000) > 0, round(minNumPixels)),...
    blobs, 'PixelIdxList', 'EquivDiameter', 'Solidity');
for c=1:length(cents)
    cents(c).MinHeight = obj.Meta.HeightThresh;
end
validCents = cents;
valid = false(1, length(cents));
for c=1:length(cents)
    %%
    thresh = int32(obj.Meta.HeightThresh * 1000); 
    curr = cents(c);
    hi = thresh;
    lo = int32(0);
    while hi > lo + 1
        thresh = (hi + lo) / 2;
        labels = bwlabel(blobs - thresh > 0);
        idx = labels(validCents(c).PixelIdxList(1));
        if idx == 0; 
            break; 
        end;
        curr = regionprops(labels == idx, 'PixelIdxList', 'EquivDiameter', 'Solidity');
        %
        if output
            imagesc(labels == idx);
            title(sprintf('thresh = %d mm', thresh));
        end
        %
        if curr.Solidity > obj.Meta.MinSolidity && curr.EquivDiameter * obj.Meta.Plane.Resolution < obj.Meta.MaxEquivDiameter
            hi = thresh;
            curr.MinHeight = double(thresh)/1000;
            validCents(c) = curr;
        else
            lo = thresh;
        end
    end
    if output
        map=labels*0; map(validCents(c).PixelIdxList) = 1; imagesc(map)
    end
    if validCents(c).EquivDiameter * obj.Meta.Plane.Resolution >= obj.Meta.MinEquivDiameter
        valid(c) = true;
    end
end
%%
cents = validCents(valid);
for c=1:length(cents)
    cents(c).MaxHeight = max(blobs(cents(c).PixelIdxList));
end
