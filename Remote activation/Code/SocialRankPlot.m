[q, indepProbs, jointProbs, q, patterns] = SocialComputePatternProbs(obj, false);
%%
shuffeled = obj;
for i=1:obj.nSubjects
    shuffeled.zones(i, :) = obj.zones(i, randperm(obj.nFrames));
end
[q, sIndepProbs, sJointProbs] = SocialComputePatternProbs(shuffeled, false);

%%
cmap = MyCategoricalColormap(128);
thresh = -inf;
sorted = sort(jointProbs, 2, 'descend'); sorted = sorted(sorted > thresh);
sortedShuffle = sort(sJointProbs, 2, 'descend'); sortedShuffle = sortedShuffle(sortedShuffle > thresh);
semilogy(1:length(sorted), sorted, 'Color', cmap(1, :));
hold on
semilogy(1:length(sortedShuffle), sortedShuffle, 'Color', cmap(3, :));
hold off
prettyPlot([], 'pattern rank according to its probability', 'pattern probability')
legend('Independent', 'Observed');
legend boxoff;
a = axis;
axis([a(1) a(2) min(sorted) a(4)]);