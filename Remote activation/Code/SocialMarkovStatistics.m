options.nSubjects = 4; % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts

% 1) Preparing the data matrix for work
% 2) Calculating contacts
% 3) Finding "islands" of "1" or "11" (false contacts)
% 4) Unifying contacts into "engagements" by canceling small gaps
% 5) Creating a cell array of engagements and their respective raw data
% 6) Characterizing the engagements (aggresive or non-aggresive)
% 7) Plots

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1)                 Preparing the data matrix for work
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Loading the Excel file for all mice (if needed)
loadData;

%%
global zones;
global orig;
model = {};
trained = {};
for i=1:options.nSubjects
    model{i}.emis = eye(zones.count);
    model{i}.trans = repmat(1/zones.count, zones.count);
    [trained{i}.trans, trained{i}.emis] = hmmtrain(zones.all(i, :) + 1, model{i}.trans, model{i}.emis, 'verbose', true);
end
%%
p = zones.all * 0 + 1;
for i=1:options.nSubjects
    for j=2:size(zones.all, 2)-1
        p(i, j) = trained{i}.trans(zones.all(i, j-1) + 1, zones.all(i, j) + 1) * ...
            trained{i}.trans(zones.all(i, j) + 1, zones.all(i, j+1) + 1);
    end
end

%%
[sampleProbIndep, sampleProbJoint] = computeSampleProb(zones.all, zones.count);
%%
p1 = sampleProbJoint;
p2 = prod(p);
MI1 = mean(log2(p1)) - mean(log2(p2));

% confidence
up1 = unique(p1);
counts = up1 * size(data, 2);
[phat,pci] = binofit(counts, size(zones.all, 2));
loglog(p1, p2, '.');
myCofidencePlot(up1, pci(:, 1)', pci(:, 2)');

loglog(p1, p2, '.');
hold off;

% minLog = floor(log10(min(min(jSampleProbJoint), min(jSampleProbIndep))));
% maxLog = ceil(log10(max(max(jSampleProbJoint), max(jSampleProbIndep))));
% axis([10^minLog 10^maxLog 10^minLog 10^maxLog]);

minLog = floor(log10(min(min(sampleProbJoint), min(sampleProbIndep))));
maxLog = ceil(log10(max(max(sampleProbJoint), max(sampleProbIndep))));
axis([...s
    10^floor(log10(min(p1))) ...
    10^floor(log10(max(p1))) ...
    10^floor(log10(min(p2))) ...
    10^floor(log10(max(p2))) ...
    ]);

line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);

% title('Permuted Samples');
% xlabel('Joint');
% ylabel('Independent');
prettyPlot('Time Permuted', 'Joint', 'Independent');
