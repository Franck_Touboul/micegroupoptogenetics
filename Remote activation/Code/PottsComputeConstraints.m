function [m, pci] = PottsComputeConstraints(data, range, nPerms, confidence)
if ~exist('confidence', 'var')
    confidence = 0.05;
end

dim = 0;
for i=1:length(nPerms)
    nVals = range^length(nPerms{i});
    dim = dim + nVals;
end

offset = 1;
c = zeros(1, dim);
zData = double(data - 1);
for i=1:length(nPerms)
    nVals = range^length(nPerms{i});
    vec = range.^(length(nPerms{i})-1:-1:0);
    c(offset:offset+nVals-1) = histc(zData(:, nPerms{i}) * vec', 0:nVals-1)';
    offset = offset + nVals;
end
WaitForLicense('statistics_toolbox');
if nargout > 1
    warning('off','stats:betainv:NoConvergence');
    [m, pci] = binofit(c, size(data, 1), confidence);
    warning('on','stats:betainv:NoConvergence');
else
    m = c / size(data, 1);
end

