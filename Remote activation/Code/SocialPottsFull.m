fprintf('# computing the potts model\n');
%% Loading the Excel file for all mice (if needed)
fprintf('# - loading defaults\n');
TrackDefaults;
filename = [options.output_path options.test.name '.social.mat'];
load(filename);

%%
filename = [options.output_path options.test.name '.analysis.mat'];
if exist(filename) ~= 0
    load(filename);
end

%%
options.savePrefix = [options.output_path options.test.name '.potts'];

options.nSubjects = 4;           % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts
options.nIters = 500;
options.confidenceIters = 0;

%data = zones.all(:,1:floor(end/4));
%data = zones.all(:,floor(3/4*end):end);
%
%% train potts model
data = social.zones.all;
options.count = max(social.zones.all(:)) + 1;
%data = testPottsModel(options, size(zones.all, 2));
me = {};
fprintf('# - training model:\n');
for level=1:options.nSubjects
    fprintf('#      . level %d\n', level);
    options.n = level;
    local = options;
    local.output = true;
    me{level} = trainPottsModel(local, data);
end
analysis.me = me;

%% compute independent probabilities
independentProbs = computeIndependentProbs(data, options.count);

%% compute joint probabilities
[jointProbs, jointPci] = computeJointProbs(data, options.count);

%% compute probability for each sample
fprintf('# computing joint probabilities : ');
[sampleProbIndep, sampleProbJoint] = computeSampleProb(data, options.count, independentProbs, jointProbs);
fprintf('mean log-likelihood = %6.4f\n', mean(log(sampleProbJoint)));
%% compute MaxEnt probabilities for each sample
Entropy = [];
for level=1:options.nSubjects
    fprintf('# computing max-entropy probabilities for level %d\n', level);
    p = exp(me{level}.features * me{level}.weights');
    perms_p = exp(me{level}.perms * me{level}.weights');
    Z = sum(perms_p);
    p = p / Z;
    Entropy(level) = mean(log2(p));
    %% plot
    if isempty(options.savePrefix)
        subplot(3,2,level);
    else
        subplot(2,2,1);
    end
    b1 = sampleProbJoint;
    b2 = sampleProbIndep;
    
    p1 = sampleProbJoint;
    p2 = p;
    % confidence
    up1 = unique(p1);
    counts = up1 * size(data, 2);
    [phat,pci] = binofit(counts, size(data, 2));
    myCofidencePlot(up1, pci(:, 1)', pci(:, 2)');
    hold on;
    %
    h1 = plot(b1, b2, '.', 'Color', [.6 .6 .6]);
    hold on;
    h2 = plot(p1, p2, '.');
    hold off;
    set(gca, 'YScale', 'log', 'XScale', 'log');
    
    minLog = floor(log10(min(min(p1), min(p2))));
    maxLog = ceil(log10(max(max(p1), max(p2))));
    axis([10^minLog 10^maxLog 10^minLog 10^maxLog]);
    line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);
    
    axis([...s
        10^floor(log10(min(p1))) ...
        10^ceil(log10(max(p1))) ...
        10^floor(log10(min(p2))) ...
        10^ceil(log10(max(p2))) ...
        ]);
    
    tit = ['Maximum-Entropy Model (me-prob=' num2str(mean(log(p))) ', joint-prob' num2str(mean(log(sampleProbJoint))), ')'];
    title(tit);
    xlabel('Joint');
    ylabel('ME');
    
    %prettyPlot(tit, 'Joint', 'ME');
    legend([h1, h2], 'Independent', 'ME', 'location', 'SouthEast'); legend boxoff
    if ~isempty(options.savePrefix)
        saveFigure([options.savePrefix sprintf('.level_%d', level)]);
    end
end

%%
In = Entropy(end) - Entropy(1);
Ik = diff(Entropy);
if ~isempty(options.savePrefix)
    subplot(2,2,1);
else
    subplot(3,2,5);
end
%pie(Ik/In, {'pairs', 'triples', 'quadruples'});
myBoxPlot(cumsum(Ik), {'Pairwise (I_{(2)})', 'Triplet (I_{(3)})', 'Quadruplet (I_{(4)})'});
title('K''th-Order Correlations');
if ~isempty(options.savePrefix)
    saveFigure([options.savePrefix '.kth_order_corr']);
end

%%
filename = [options.output_path options.test.name '.analysis.mat'];
fprintf(['# - saving to file: ''' filename '''\n']);
save(filename, 'analysis');