function SocialShowHierarchyStructure(W, level, options)
offset = 0;

if ~exist('options', 'var')
    options = struct();
end

options = setDefaultParameters(options, ...
    'NodeSize', 25, ...
    'EdgeMinWidth', .25, ...
    'EdgeMaxWidth', 8, ...
    'EdgeMaxValue', 50, ...
    'EdgeShift', .075);

cmap = MyMouseColormap;
dx = [0 1 -1 0];
titles = {'\alpha', '\beta', '\gamma', '\delta'};

X = [];
Y = [];
for l=unique(level)
    map = level==l;
    idx = 0;
    for i=find(map)
        a = 2 * mod(l, 2) - 1;
        X(i) = offset + dx(l) + a * idx;
        Y(i) = -l;
        idx = idx + 1;
    end
end

for i=1:size(W, 1)
    for j=1:size(W, 2)
        if W(i, j) > 0
            %plot([X(i) X(j)], [Y(i) Y(j)], '-', 'Color', [.2 .2 .2], 'LineWidth', h.Contacts(i, j) / options.EdgeMaxValue * options.EdgeMaxWidth); hon
            vec = [X(i)-X(j) Y(i)-Y(j)];
            vec = vec / sqrt(vec*vec') * (options.EdgeShift * (W(j, i) > 0));
            width = W(i, j) / options.EdgeMaxValue * (options.EdgeMaxWidth);
            if width == 0
                continue;
            end
            width = min(width, options.EdgeMaxWidth);
            width = max(width, options.EdgeMinWidth);
            plot([X(i) X(j)] + vec(2), [Y(i) Y(j)] - vec(1), '-', 'Color', cmap(i, :), 'LineWidth', width); hon
        end
    end
end
for i=1:size(W, 1)
    plot(X(i), Y(i), 'o', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', cmap(i, :), 'MarkerSize', options.NodeSize); hon;
    text(X(i), Y(i), titles{level(i)}, 'verticalAlignment', 'middle', 'horizontalalignment', 'center');
end
set(gcf, 'Color', 'w', 'Units', 'normalized');
axis off;
axis equal
hoff;
