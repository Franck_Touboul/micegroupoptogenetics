function symplot(i, count)
nx = ceil(sqrt(count));
ny = floor(sqrt(count));
subplot(nx, ny, i);