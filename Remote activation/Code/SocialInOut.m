function obj = SocialInOut(obj)
obj = TrackLoad(obj);
%zones = ~obj.sheltered(:, obj.valid) + 1;

zones = ~(obj.zones(:, obj.valid) == 9) + 1;

curr = SocialSetZones(obj, zones);
curr.Output = false;
curr.OutputToFile = false;
curr.OutputInOldFormat = false;
curr.ROI.nZones = 2;
curr.ROI.ZoneNames = {'In', 'Out'};
curr = SocialPottsWithWindow(curr);
obj.InOut = curr.Analysis.Potts.Windowed;
TrackSave(obj);
