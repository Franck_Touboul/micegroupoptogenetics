function obj = SocialSetZones(obj, zones)
obj.zones = zones;
obj.valid = all(~isnan(zones));
obj.nFrames = size(zones, 2);