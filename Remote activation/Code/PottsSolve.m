function PottsSolve(me)
x = me.perms;
%w0 = me.weights;
w0 = randn(1, length(me.weights));
aux = ModelExpectationAux(x);
fun = @(w) HalfKL(w, x, me.constraints, aux);

% options = optimset('GradObj','on', 'Display', 'iter', 'HessUpdate', 'bfgs');
% fminunc(fun, w0,options);

% options = optimset('Algorithm', 'interior-point', 'Display', 'iter', 'Hessian', 'lbfgs');
% fmincon(fun, w0, [], [], [], [], [], [], [], options);
iE = Entropy(w0, x);

% options = struct('Display','iter',...
%     'MaxIter',1000,...
%     'TolFun',1e-6,...
%     'TolX', 1e-10,...
%     'TolGrad',1e-6,...
%     'Convex',0);
% w = lbfgs(fun, w0, options);
options = optimset('GradObj','on', 'Display', 'iter');
%options = optimset('GradObj','on', 'Display', 'iter', 'HessUpdate', 'steepdesc');
[w,fval,exitflag,output] = fminunc(fun, w0,options);

fE = Entropy(w, x);

fprintf('# Entropy: %f (initial %f)\n', fE, iE);


function e = Entropy(w, x)
p0 = exp(x * w');
Z = sum(p0);
p = p0 / Z;
e = -p' * log2(p);

function aux = ModelExpectationAux(x)
aux.u = unique(sum(x));
idx = 1;
for u=aux.u
    aux.p(idx).iu = sum(x,1) == u;
    aux.p(idx).nu = sum(aux.p(idx).iu);
    [i,j] = find(x(:, aux.p(idx).iu));
    aux.p(idx).i = i;
    idx = idx + 1;
end

function E = ModelExpectation(w, x, constraints, aux)
%%
perms_p = exp(x * w');
Z = sum(perms_p);

r = perms_p / Z;
E = zeros(1, length(constraints));
idx = 1;
for u=aux.u
    E(aux.p(idx).iu) = sum(reshape(r(aux.p(idx).i), u, aux.p(idx).nu)', 2);
    idx = idx + 1;
end

function [KL, d] = HalfKL(w, x, constraints, aux)
perms_p = exp(x * w');
Z = sum(perms_p);
%E = full(sum(x .* repmat(perms_p / Z, 1, size(x, 2))));
E = ModelExpectation(w, x, constraints, aux);

KL = -constraints * log(E');
d = -(constraints - E);

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
return;
x = me.perms;
% L-bfgs
options = optimset('Algorithm', 'interior-point', 'Display', 'iter', 'Hessian', 'lbfgs');


%options = optimset('Algorithm', 'sqp', 'Display', 'iter', 'Hessian', 'bfgs');
fun = @(w) MinusEntropy(w, x);
w0 = me.weights;
w0 = randn(1, length(me.weights));

aux = qConstraintsAux(w0, x, me.constraints);
nonlcon = @(w) qConstraints2(w, x, me.constraints, aux);
fmincon(fun, w0, [], [], [], [], [], [], nonlcon, options);

function [e, de] = MinusEntropy(w, x)
p0 = exp(x * w');
%perms_p = exp(x * w');
%Z = sum(p);
Z = sum(p0);
p = p0 / Z;
e = p' * log2(p);
% dp = (Z * x .* repmat(p0, 1, 640) - repmat(p0, 1, 640) .* repmat(sum(x .* repmat(p0, 1, 640), 1), 10000, 1)) / Z^2;
% de = -sum(dp .* (1 + repmat(log(p), 1, 640)));

function [c, ceq] = Constraints(w, x, constraints)
%%
perms_p = exp(x * w');
Z = sum(perms_p);
E = full(sum(x .* repmat(perms_p / Z, 1, size(x, 2))));
c = [];
ceq = (constraints - E).^2;

function [c, ceq] = qConstraints(w, x, constraints)
%%
perms_p = exp(x * w');
Z = sum(perms_p);

r = perms_p / Z;
u_ = unique(sum(x));
E = zeros(1, length(constraints));
for u=u_
    iu = sum(x,1) == u;
    nu = sum(iu);
    [i,j] = find(x(:, iu));
    E(iu) = sum(reshape(r(i), u, nu)', 2);
end
%%
c = [];
ceq = constraints - E;

function aux = qConstraintsAux(w, x, constraints)
aux.u = unique(sum(x));
idx = 1;
for u=aux.u
    aux.p(idx).iu = sum(x,1) == u;
    aux.p(idx).nu = sum(aux.p(idx).iu);
    [i,j] = find(x(:, aux.p(idx).iu));
    aux.p(idx).i = i;
    idx = idx + 1;
end

function [c, ceq] = qConstraints2(w, x, constraints, aux)
%%
perms_p = exp(x * w');
Z = sum(perms_p);

r = perms_p / Z;
E = zeros(1, length(constraints));
idx = 1;
for u=aux.u
    E(aux.p(idx).iu) = sum(reshape(r(aux.p(idx).i), u, aux.p(idx).nu)', 2);
    idx = idx + 1;
end
%%
c = [];
ceq = constraints - E;