SocialExperimentData;

Group = struct;
Group.FamilyNames = GroupsData.Types;
Group.FamilyID = GroupsData.group;

for i=1:GroupsData.nExperiments
    fprintf('# experiment no. %d of %d\n', i, GroupsData.nExperiments);
    try
        objs = cell(1, GroupsData.nDays);
        for d=1:GroupsData.nDays
            fprintf('# - loading day %d of %d\n', d, GroupsData.nDays);
            obj = TrackLoad({i d});
            objs{d} = obj;
        end
        %% common
        pidx = 1;
        Group.FilePrefix{i} = objs{1}.FilePrefix;

        %% In (2nd)
        Group.Fields{pidx} = 'In(2nd)';
        Group.Table(i, pidx) = objs{2}.Analysis.Potts.In;
        pidx = pidx + 1;

        %% I2 (2nd)
        Group.Fields{pidx} = 'I2(2nd)';
        cs = cumsum(objs{2}.Analysis.Potts.Ik);
        Group.Table(i, pidx) = cs(1);
        pidx = pidx + 1;

        %% I3 (2nd)
        Group.Fields{pidx} = 'I3(2nd)';
        cs = cumsum(objs{2}.Analysis.Potts.Ik);
        Group.Table(i, pidx) = cs(2);
        pidx = pidx + 1;
        
        %% Entropy (2nd)
        Group.Fields{pidx} = 'Entropy(2nd)';
        p = objs{2}.Analysis.Potts.Model{1}.jointProb;
        Group.Table(i, pidx) = -p * log2(p(:) + (p(:) == 0));
        pidx = pidx + 1;
    catch e
        error(e.message);
        return;
    end
end
save('Group.mat', 'Group', '-v7.3');



