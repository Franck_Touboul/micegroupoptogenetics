function obj = SocialComputeMutualQuantities(obj)
%%
obj.MaxDistanceForContact = 150; % minimal distance considered as contact
obj.MinContanctDuration = 3; % minimal duration of a contact
obj.MaxShelteredContanctDuration = 25;

fprintf('# computing mutual quantities\n');

%% find contacts
fprintf('# - finding contacts\n');
obj.distance = cell(obj.nSubjects,obj.nSubjects);
obj.contact  = cell(obj.nSubjects,obj.nSubjects);

for i=1:obj.nSubjects-1
    for j=i+1:obj.nSubjects
        obj.distance{i, j} = sqrt((obj.x(i, :) - obj.x(j, :)).^2 + (obj.y(i, :) - obj.y(j, :)).^2);
        sheltered = obj.sheltered(i, :) | obj.sheltered(j, :);
        contact = obj.distance{i, j} < obj.MaxDistanceForContact & ~sheltered;

        [start, finish] = FindEvents(contact);
        for r=1:length(start)-1
            if min(sheltered(finish(r) + 1:start(r+1) - 1)) > 0 && start(r+1) - finish(r) - 1 < obj.MaxShelteredContanctDuration
                contact(finish(r) + 1:start(r+1) - 1) = true;
            end
        end
        obj.contact{i, j} = contact;
    end
end

%% relative angles between subjects
fprintf('# - computing relative angles\n');
obj.angle = cell(obj.nSubjects, obj.nSubjects);
for i=1:obj.nSubjects
    for j=1:obj.nSubjects
        if i == j;
            continue;
        end
        relative = [obj.x(j, :) - obj.x(i, :); obj.y(j, :) - obj.y(i, :)];
        relative = relative ./ repmat(sqrt(sum(relative.^2)), 2, 1);
        
        speed = [obj.x(i, 2:end) - obj.x(i, 1:end-1) 0; obj.y(i, 2:end) - obj.y(i, 1:end-1) 0];
        %chase = sum(speed .* relative);
        %chase(~isfinite(chase)) = 0;
        %data.chase(i, j, :) = chase;
       
        direction = speed ./ repmat(sqrt(sum(speed.^2)), 2, 1);
        angle = acos(sum(direction .* relative));
        %angle(abs(chase) < options.minSpeedForAngle) = pi/2;
        clear speed;
        obj.angle{i, j} = angle;
    end
end

%%

% orthDirection = direction * 0; 
% orthDirection(1, :) =  direction(2,:); 
% orthDirection(2, :) = -direction(1,:); 
% angle = atan2(sum(orthDirection .* relative), sum(direction .* relative));

%%
% angRelative = atan2(relative(2, :), relative(1, :));
%         angRelative(angRelative < 0) = angRelative(angRelative < 0) + 2 * pi;
%         
%         angDirection = atan2(direction(2, :), direction(1, :));
%         angDirection(angDirection < 0) = angDirection(angDirection < 0) + 2 * pi;
%         
%         angle = angRelative - angDirection;
%         angle(angle < 0) = ang(angle < 0) + 2 * pi;
% %%
%         sum(direction .* relative)