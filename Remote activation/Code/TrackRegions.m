TrackDefaults;
%%
if isfield(options, 'ROIFile')
    options.loadROI = true;
elseif ~isfield(options, 'loadROI')
    options.loadROI = false;
end

if ~isfield(options, 'hiddenZoneMinDuration')
    options.hiddenZoneMinDuration = 100;
end
if ~isfield(options, 'minInZoneDuration')
    options.minInZoneDuration = 25;
end
%%
filename = [options.output_path options.test.name '.meta.mat'];
load(filename);
%%
filename = [options.output_path options.test.name '.social.mat'];
if exist(filename, 'file')
    load(filename);
    use_social = true;
else
    use_social = false;
end
    
%%
pos = get(gcf, 'Position');
num = gcf;
close(gcf)
figure(num);
set(gcf, 'Position', pos);
%imshow(meta.bkgFrame);
ebkgFrame = im2double(histeq(rgb2gray(meta.bkgFrame)));
%set(ehandle, 'AlphaData', 0);
%hold off;

%TrackDefineRegionsAux(meta.bkgFrame);

%slider_func = @(hObj,event,ax) TrackDefineRegionsAux(get(hObj, 'Value') / 100);
% slider_func = @(hObj,event,ax) set(ehandle, 'AlphaData', get(hObj, 'Value') / 100);
%
% sh = uicontrol(gcf,'Style','slider',...
%                 'Max',100,'Min',0,'Value',25,...
%                 'SliderStep',[0.05 0.2],...
%                 'Position',[30 20 250 20], 'Callback', slider_func);

% bgh = uibuttongroup('Parent',gcf,...
%     'Position',[0.05 0.91 .90 .08]);
%
% textControl = uicontrol(gcf,'Style','text','String','area for: "Big Nest"',...
%     'Units','normalized',...
%     'HorizontalAlignment','left',...
%     'FontSize', 12, ...
%     'Position',[0.06 0.02 .88 .06]);

textControl = annotation('textbox',[0.05 0.91 .90 .08], ...
    'EdgeColor', [0 0 0], ...
    'Color', [.2 .2 .3],...
    'LineStyle', 'none', ...
    'FontSize', 14);
%
r = ebkgFrame;
g = ebkgFrame;
b = ebkgFrame;
img = cat(3, r, g, b);
img = TrackShowRegion(img, false(size(r)), [1 1 1]);
imshow(img);
hold on;
for curr=1:options.nSubjects
    hidden = social.zones.hidden(curr, :);
    d = diff([0 hidden 0], 1, 2);
    start  = find(d > 0) - 1;
    finish = find(d < 0);
    
    len = finish - start + 1;
    valid = start > 0 & finish <= length(hidden) & len > options.hiddenZoneMinDuration;
    start = start(valid);
    finish = finish(valid);
    plot(social.x(curr, start), social.y(curr, start), 'r.');
    plot(social.x(curr, finish), social.y(curr, finish), 'r.');
end
hold off;
%%
if ~options.loadROI
    imshow(img);
    hold on;
    for curr=1:options.nSubjects
        hidden = social.zones.hidden(curr, :);
        d = diff([0 hidden 0], 1, 2);
        start  = find(d > 0) - 1;
        finish = find(d < 0);
        
        len = finish - start + 1;
        valid = start > 0 & finish <= length(hidden) & len > options.hiddenZoneMinDuration;
        start = start(valid);
        finish = finish(valid);
        plot(social.x(curr, start), social.y(curr, start), 'r.');
        plot(social.x(curr, finish), social.y(curr, finish), 'r.');
    end
    %%
    roi.fields    = {'Feeder1', 'Feeder2', 'Water', 'SmallNest', 'Labyrinth', 'BigNest'};
    roi.sheltered = [        0,         0,       0,           1,           0,         1];
    roi.fieldsIndex = struct();
    cmap = lines(length(roi.fields));
    
    for i=1:length(roi.fields)
        curr = roi.fields{i};
        set(textControl, 'String', ['choose area for: ' curr]);
        map = roipoly;
        roi = setfield(roi, curr, map);
        img = TrackShowRegion(img, map, cmap(i, :));
        imshow(img);
        
        %%
        hold on;
        for curr=1:options.nSubjects
            hidden = social.zones.hidden(curr, :);
            d = diff([0 hidden 0], 1, 2);
            start  = find(d > 0) - 1;
            finish = find(d < 0);
            
            len = finish - start + 1;
            valid = start > 0 & finish <= length(hidden) & len > options.hiddenZoneMinDuration;
            start = start(valid);
            finish = finish(valid);
            plot(social.x(curr, start), social.y(curr, start), 'r.');
            plot(social.x(curr, finish), social.y(curr, finish), 'r.');
        end
        hold off;
        %%
        roi.fieldsIndex = setfield(roi.fieldsIndex, roi.fields{i}, i);
    end
    %%
    fprintf('# - saving ROIs...\n'); set(textControl, 'String', 'Saving ROIs...');
    filename = [options.output_path options.test.name '.roi.mat'];
    save(filename, 'roi');
else
    fprintf('# - loading ROIs...\n');
    if isfield(options, 'ROIFile') && ~isempty(options.ROIFile)
        filename = options.ROIFile;
    else
        filename = [options.output_path options.test.name '.roi.mat'];
    end
    fprintf(['#      . from: ''' filename '''\n']);
    load(filename, 'roi');
    cmap = lines(length(roi.fields));
end
%%
for i=1:length(roi.fields)
    map = getfield(roi, roi.fields{i});
    img = TrackShowRegion(img, map, cmap(i, :));
end
%%
if options.output
    imshow(img);
    hold on;
    for curr=1:options.nSubjects
        hidden = social.zones.hidden(curr, :);
        d = diff([0 hidden 0], 1, 2);
        start  = find(d > 0) - 1;
        finish = find(d < 0);
        
        len = finish - start + 1;
        valid = start > 0 & finish <= length(hidden) & len > options.hiddenZoneMinDuration;
        start = start(valid);
        finish = finish(valid);
        plot(social.x(curr, start), social.y(curr, start), 'r.');
        plot(social.x(curr, finish), social.y(curr, finish), 'r.');
    end
    %%
    fprintf('# - saving ROI image...\n'); set(textControl, 'String', 'Saving ROIs...');
    filename = [options.output_path options.test.name '.roi.png'];
    imwrite(img, filename);
end

%%
% filename = [options.output_path options.test.name '.roi.mat'];
% load(filename, 'roi');
% for i=1:length(roi.fields)
%     roi.fieldsIndex = setfield(roi.fieldsIndex, roi.fields{i}, i);
% end
%%

fprintf('# - setting zones\n'); set(textControl, 'String', 'Setting zones...');
filename = [options.output_path options.test.name '.social.mat'];
load(filename, 'social');
social.roi = roi;
%%
social.zones.all = social.x * 0;
social.zones.sheltered = false(size(social.x));
social.zones.labels{1} = 'Open';
for s=1:social.nSubjects
    sheltered = social.zones.sheltered(s, :);
    currHidden = social.zones.hidden(s, :) == 1;
    index = 1;
    for i=1:length(roi.fields)
        currRoi = getfield(roi, roi.fields{i});
        x = round(social.x(s, :));
        y = round(social.y(s, :));
        x(x < 1) = 1; x(x > size(currRoi, 2)) = size(currRoi, 2);
        y(y < 1) = 1; y(y > size(currRoi, 1)) = size(currRoi, 1);
        currZones = social.zones.all(s, :);
        if roi.sheltered(i)
            currZones(currRoi(sub2ind(size(currRoi), y, x)) & ~currHidden) = index;
            social.zones.labels{index + 1} = roi.fields{i};
            social.zones.shelteredzone(index + 1) = false;
            currZones(currRoi(sub2ind(size(currRoi), y, x)) &  currHidden) = index + 1;
            social.zones.labels{index + 2} = ['(' roi.fields{i} ')'];
            social.zones.shelteredzone(index + 2) = true;
            index = index + 2;
        else
            currZones(currRoi(sub2ind(size(currRoi), y, x))) = index;
            social.zones.labels{index + 1} = roi.fields{i};
            social.zones.sheltered(index + 1) = false;
            index = index + 1;
        end
        social.zones.all(s, :) = currZones;
        sheltered(currZones > 0) = (roi.sheltered(i) & currHidden(currZones > 0));
    end
    social.zones.sheltered(s, :) = sheltered;
end

%% remove short zone epochs
fprintf('# - removing short in-zone events');
for s=1:social.nSubjects
    for i=1:length(roi.fields)
        currZones = social.zones.all(s, :);
        inZone = currZones == i;
        d = diff([0 inZone 0], 1, 2);
        start  = find(d > 0) - 1;
        finish = find(d < 0);
        if isempty(start)
            continue;
        end
        len = finish - start + 1;
        pre  = currZones(start (start > 0) - 1);
        post = currZones(finish(finish <= length(currZones)) + 1);
        if start(1) < 1
            start(1) = 1;
            pre = [post(1), pre];
        end
        if finish(end) > length(currZones)
            finish(end) = length(currZones);
            post = [post, pre(end)];
        end
        invalid = pre == post & len < options.minInZoneDuration;
        for j=find(invalid)
            currZones(start(j):finish(j)) = pre(j);
        end
        social.zones.all(s, :) = currZones;
    end
end

%%
for s=1:social.nSubjects
    sheltered = false(1, social.nData);
    for i=1:length(social.zones.labels)
        if social.zones.sheltered(i)
            sheltered(social.zones.all(s, :) == i) = true;
        end
    end
    social.zones.sheltered(s, :) = sheltered;
end

%% remove long hidden out of shelter epochs
fprintf('# - removing long hidden events');
ROIx = [];
ROIy = [];
ROIz = [];
for i=find(social.roi.sheltered == 1)
    currRoi = getfield(social.roi, social.roi.fields{i});
    [x, y] = find(edge(currRoi));
    ROIx = [ROIx; x(:)]; ROIy = [ROIy; y(:)];
    ROIz = [ROIz; ROIx * 0 + getZoneId(social, ['(' social.roi.fields{i} ')'])];
end

for s=1:social.nSubjects
    events = ~social.zones.sheltered(s, 1:length(social.zones.hidden(s, :))) & social.zones.hidden(s, :);
    d = diff([0 events 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0)-1;
    len = finish - start;
    valid = len > options.maxHiddenOutOfShelter;
    indices = find(valid);
    if isempty(indices)
        continue;
    end
    for i=indices
        range = start(i):finish(i);
        for r = range
            d = (ROIx - social.x(s, r)) .^2 + (ROIy - social.x(s, r)) .^2;
            [min_d, min_d_idx] = min(d);
            social.x(s, r) = ROIx(min_d_idx);
            social.y(s, r) = ROIy(min_d_idx);
            social.zones.all(s, r) = ROIz(min_d_idx);
            social.zones.sheltered(s, r) = 1;
        end
    end
end

%%
fprintf('# - saving zones\n'); set(textControl, 'String', 'Saving zones...');
filename = [options.output_path options.test.name '.social.mat'];
save(filename, 'social');
fprintf('# - done!\n'); set(textControl, 'String', 'Done!');

%%
% curr = 'bigNest';
%
% roi.bigNest = [];
% roi.smallNest = [];
%
% currImage = ebkgFrame;
% while 1~=2
%     map = roipoly;
%     roi = setfield(roi, curr, map);
%     if get(rbh1, 'Value') == 1
%         curr = 'bigNest';
%     elseif get(rbh2, 'Value') == 1
%         curr = 'smallNest';
%     end
%     r = ebkgFrame;
%     g = ebkgFrame;
%     b = ebkgFrame;
%     b(roi.bigNest) = b(roi.bigNest) / 2;
%     r(roi.smallNest) = r(roi.smallNest) / 2;
%     imshow(cat(3, r, g, b));
% end
%
