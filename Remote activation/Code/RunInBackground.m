function bkgobj = RunInBackground(obj, funcstr)
if isa(funcstr, 'function_handle')
    funcstr = func2str(funcstr);
end
obj = TrackLoad(obj);
EnsurePath('../temp');
temp = obj;
temp.OutputPath = '../temp/';
temp.SourceMeta.OutputPath = obj.OutputPath;
rand('twister',sum(100*clock));
randn('seed',sum(100*clock));
temp.FilePrefix = hash(randi(999999999), 'MD5');
temp.SourceMeta.FilePrefix = obj.FilePrefix;
TouchFile(['../temp/' temp.FilePrefix '.output']);
TrackSave(temp);
%%
clustman = ClusterManager;
clustman.WaitForVacancy;

h = [];
h.cmd = 'myTaskBkg';
%h.args = [funcstr ' "''' temp.OutputPath temp.FilePrefix '.obj.mat''"' varargin{:}];
h.args = ['RunInBackgroundAux "''' temp.FilePrefix '''" "''' funcstr '''"'];
%%
[stat, output] = system([h.cmd ' ' h.args]);
fprintf('%s', output);
h.jobid = str2double(regexprep(regexprep(output , '.*job:[ ]*', ''), ' +.*', ''));
logs = regexp(regexprep(regexprep(output, '^[^\(]*\([ ]*', ''), '(^[^\)]*)\).*', '$1'), ' +', 'split');
h.logfile = logs{1};
h.errfile = logs{2};
bkgobj = [];
bkgobj.Type = 'bkgobj';
bkgobj.Process = h;
bkgobj.SourceObj = temp;
bkgobj.FilePrefix = temp.SourceMeta.FilePrefix;
bkgobj.OutputPath = temp.SourceMeta.OutputPath;
bkgobj.ID = temp.FilePrefix;
%[