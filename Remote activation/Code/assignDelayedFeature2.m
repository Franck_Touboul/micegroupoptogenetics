function [features, labels] = assignDelayedFeature2(data, range, nPerms, use_sparse, delay)
if nargin < 4
    use_sparse = false;
end

dim = 0;
for i=1:length(nPerms)
    nVals = range^length(nPerms{i});
    dim = dim + nVals;
end
if use_sparse
    features = sparse(size(data, 1), double(dim));
else
    features = zeros(size(data, 1), double(dim));
end

index = 1;
labels = cell(1, dim);
for i=1:length(nPerms)
    nVals = range^length(nPerms{i});
    for j=0:nVals-1
        values = decimal2base(j, range, length(nPerms{i})) + 1;
        %map = logical(ones(size(data, 1), 1));
        map = data(delay:end, nPerms{i}(1)) == values(1);
        for k=2:length(nPerms{i})
            map = map & data(1:end-delay+1, nPerms{i}(k)) == values(k);
        end
        features(:, index) = [false(delay-1, 1); map];
        
        labels{index} = [nPerms{i}; values];
        index = index + 1;
    end
end


