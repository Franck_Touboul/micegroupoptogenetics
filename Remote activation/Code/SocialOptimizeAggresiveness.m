nbins = 10;
fields = {'PostMaxSpeed', 'PostMaxDistance'};
dims = ones(1, length(fields)) * nbins;
range = [0 50; 50 50];
[vec, nvals] = NumToDims(1e12, dims);
%%
fathresh = .05;
best.id = inf;
best.detection = 0;
best.a = [];
for i=0:nvals
    [vec, i] = NumToDims(i, dims);
    a = range(:, 1) + (range(:, end) - range(:, 1)) / (nbins - 1) .* vec';
    %%
    %a = [18 0 20];
    %a = [20 20];
     a(1) = 20;
     a(2) = 50;
%     a(3) = 20;    
    local.Contacts.Behaviors.ChaseEscape = obj.Contacts.Properties.PostMaxSpeed(2, :) > a(1) & ...
        obj.Contacts.Properties.PostMaxDistance < a(2) ;
    [results, match] = SocialBehaviorAnalysisScore(local, amarks);
    fprintf('# (%d/%d) [%.1f %.1f]: detection=%.1f%% (%d), false-alarm=%.1f%% (%d)\n', i, nvals, a(1), a(2), results.chase(1) / (results.chase(1) + results.chase(3)) * 100, results.chase(1), results.chase(2) / results.chase(5) * 100, results.chase(2));
    if results.chase(2) / results.chase(5) <= fathresh && best.detection < results.chase(1) / (results.chase(1) + results.chase(3))
        best.detection = results.chase(1) / (results.chase(1) + results.chase(3));
        best.a = a;
        best.id = i;
    end
end

%%
%local.Contacts.Behaviors.ChaseEscape = obj.Contacts.Properties.PostMaxSpeed(2, :) > 10 & obj.Contacts.Properties.PostMaxDistance < 10 & obj.Contacts.Behaviors.ChaseEscape(r)';
%local.Contacts.Behaviors.ChaseEscape = obj.Contacts.Behaviors.ChaseEscape(r)' & obj.Contacts.Properties.PostSpeedJitter(2, :) > 2;



local.Contacts.Behaviors.ChaseEscape = obj.Contacts.Properties.PostMaxSpeed(2, :) > 20 & obj.Contacts.Properties.PostMaxDistance < 10;
[results, match] = SocialBehaviorAnalysisScore(local, amarks);
fprintf('# detection=%.1f%% (%d), false-alarm=%.1f%% (%d)\n', results.chase(1) / (results.chase(1) + results.chase(3)) * 100, results.chase(1), results.chase(2) / results.chase(5) * 100, results.chase(2));
