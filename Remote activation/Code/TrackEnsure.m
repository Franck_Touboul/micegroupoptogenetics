function obj = TrackEnsure(obj, fields)
if ~iscell(fields)
    fields = {fields};
end
missing = {};
for i=1:length(fields)
    if ~isfield(obj, fields{i})
        missing = [missing, fields{i}];
    end
end
temp = load(obj.Source, missing{:});
for i=1:length(missing)
    if isfield(temp, missing{i})
        obj.(missing{i}) = temp.(missing{i});
    else
        switch lower(missing{i})
            case 'interactions'
                obj = SocialPredPreyModel(obj);
                obj = SocialAnalysePredPreyModel(obj);
            otherwise
                warning(['unable to load field ''' missing{i} ''' from ' obj.Source]);
        end

    end
end
