function o = SocialApplyMap(obj, map)
fprintf('# - applying map (%d frames)\n', sum(map));
o = obj;
fields = {'zones', 'x', 'y', 'time', 'regions', 'hidden', 'sheltered', 'valid', 'speed'};
for f=1:length(fields)
    try
        o.(fields{f}) = obj.(fields{f})(:, map);
    catch me
        warning(['# could not map field ''' fields{f} '''']);
    end
end
o.nFrames = sum(map);


