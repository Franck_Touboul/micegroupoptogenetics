sFrame = 10000;
data = mmread('Exp1 day 3.mpg', sFrame);
nFrames = abs(data.nrFramesTotal);
tic;
index = 1;
for r=10000:20000
    curr = mmread('Exp1 day 3.mpg', [], r:r+1);
    RT = toc / index;
    fprintf('# - frame %6d/%6d (%6.2fxiRT)\n', r, nFrames, RT);
    if index == 1
        tic;
    end
    index = index + 1;
end
