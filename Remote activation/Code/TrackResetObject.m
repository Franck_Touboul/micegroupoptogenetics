function obj = TrackResetObject(obj, videoFile)
obj = TrackLoad(obj);

obj.VideoFile = videoFile;
obj.FilePrefix = regexprep(regexprep(obj.VideoFile, '\.[^\.]*$', ''), '.*[\\/]', '');
obj = TrackReadVideoFileProperties(obj);
