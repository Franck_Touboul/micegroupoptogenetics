function TrackDefaults = TrackGetDefaults(group, camera)
DefaultGroup = 'SC';

if isunix
    base = '/home/labs/schneidmann/forkosh/tests/SocialMice/base/Res/';
else
    base = 'w:/base/Res/';
end

TrackDefaults.SC.cam1.Prototype = [base 'SC.exp0001.day01.cam01.obj.mat'];
TrackDefaults.SC.cam1.nDays = 4;

TrackDefaults.SC.cam4.Prototype = [base 'SC.exp0004.day01.cam04.obj.mat'];
TrackDefaults.SC.cam4.nDays = 4;

TrackDefaults.ONE = TrackDefaults.SC;
TrackDefaults.ONE.Common.nSubjects = 1;

TrackDefaults.OTRFloxTreat.cam1.Prototype = [base 'OTRFloxTreat.exp0001.day01.cam01.obj.mat'];
TrackDefaults.OTRFloxTreat.cam1.nDays = 4;

TrackDefaults.OTRFloxTreat.cam4.Prototype = [base 'OTRFloxCtrl.exp0001.day01.cam04.obj.mat'];
TrackDefaults.OTRFloxTreat.cam4.nDays = 4;

TrackDefaults.OTRFloxCtrl  = TrackDefaults.OTRFloxTreat;

TrackDefaults.Enriched.cam1.Prototype = [base 'Enriched.exp0003.day01.cam01.obj.mat'];
TrackDefaults.Enriched.cam1.nDays = 4;

TrackDefaults.Enriched.cam4.Prototype = [base 'Enriched.exp0001.day01.cam04.obj.mat'];
TrackDefaults.Enriched.cam4.nDays = 4;

%% MD
TrackDefaults.MD.cam4.Prototype = [base 'MD.exp0002.day01.cam04.obj.mat'];
TrackDefaults.MD.cam4.nDays = 4;

TrackDefaults.MD.cam1.Prototype = [base 'MDMix.exp0005.day01.cam01.obj.mat'];
TrackDefaults.MD.cam1.nDays = 4;

TrackDefaults.MDCtrl = TrackDefaults.MD;
TrackDefaults.MDMix  = TrackDefaults.MD;

TrackDefaults.x5mice.cam1.Prototype = [base '5mice.exp0002.day01.cam01.obj.mat'];
TrackDefaults.x5mice.cam1.nDays = 4;

TrackDefaults.x5mice.cam4.Prototype = [base '5mice.exp0002.day01.cam04.obj.mat'];
TrackDefaults.x5mice.cam4.nDays = 4;

%%
if nargin > 0
    if isfield(TrackDefaults, group)
        TrackGroup = TrackDefaults.(group);
    elseif isfield(TrackDefaults, ['x' group])
        TrackGroup = TrackDefaults.(['x' group]);
    else
        TrackGroup = TrackDefaults.(DefaultGroup);
    end
        
    TrackDefaults = TrackGroup.(sprintf('cam%d', camera));
    if isfield(TrackGroup, 'Common')
        f = fields(TrackGroup.Common);    
        for i=1:length(f)
            TrackDefaults.(f{i}) = TrackGroup.Common.(f{i});
        end
    end
end