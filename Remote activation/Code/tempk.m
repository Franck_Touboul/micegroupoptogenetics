blobs = z*1000;
blobs = - (blobs - nanmedian(blobs(:)));
blobs(isnan(blobs)) = 0;
blobs = int32(blobs);

imagesc(bwlabeln(blobs - (obj.Meta.HeightThresh * 1000) > 0));

minNumPixels = (obj.Meta.MinInitEquivDiameter / obj.Meta.Plane.Resolution) ^2 * pi;
cents = regionprops(...
    bwareaopen(blobs - (obj.Meta.HeightThresh * 1000) > 0, round(minNumPixels)),...
    blobs, 'PixelIdxList', 'EquivDiameter');
validCents = cents;
valid = false(1, length(cents));
for c=1:length(cents)
    %%
    thresh = int32(obj.Meta.HeightThresh * 1000); 
    curr = cents(c);
    hi = thresh;
    lo = int32(0);
    validThresh = thresh;
    while hi > lo + 1
        thresh = (hi - lo) / 2;
        labels = bwlabel(blobs - thresh > 0);
        idx = labels(curr.PixelIdxList(1));
        if idx == 0; 
            validCents(c).EquivDiameter = 0;
            break; 
        end;
        curr = regionprops(labels == idx, blobs, 'PixelIdxList', 'EquivDiameter');
        %
        imagesc(labels == idx);
        title(sprintf('thresh = %d mm', thresh));
        %
        if curr.EquivDiameter * obj.Meta.Plane.Resolution < obj.Meta.MaxEquivDiameter
            hi = thresh;
            validCents(c) = curr;
        else
            lo = thresh;
        end
    end
    map=labels*0; map(validCents(c).PixelIdxList) = 1; imagesc(map)
    if validCents(c).EquivDiameter * obj.Meta.Plane.Resolution >= obj.Meta.MinEquivDiameter
        valid(c) = true;
    end
end
cents = cents(valid);
validCents = validCents(valid);

map = blobs * 0;
for c=1:length(cents)
    map(validCents(c).PixelIdxList) = c;
end
imagesc(map)
colormap([0,0,0; lines(10)]);