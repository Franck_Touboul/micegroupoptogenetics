%% socialcircadianstatisticsbatch
proto.CircadianWindow = obj.FrameRate * 60 * 60 * 2; % 2 hours

%%
experiments = {'SC.exp0001.day%02d.cam01'};
ndays = 4;

index = 1;
width = 5;
for id=1:length(experiments)
    DJS = [];
    for day = 1:ndays
        prefix = sprintf(experiments{id}, day);
        obj = trackload([obj.OutputPath prefix '.obj.mat'], {'zones', 'common'});
        W = cell(1, floor(obj.nFrames/proto.CircadianWindow));
        fprintf('# - analyzing all time windows ');
        ProgressReport();
        for i = 1:length(W)
            ProgressReport(i, length(W));
            r = (i - 1) * proto.CircadianWindow + 1:i*proto.CircadianWindow;
            W{i}.obj = obj;
            W{i}.obj.zones = obj.zones(:, r);
            W{i}.obj.valid = obj.valid(:, r);
            [W{i}.obj, W{i}.IndepProbs, W{i}.JointProbs, W{i}.pci] = SocialComputePatternProbs(W{i}.obj, false);
        end
        fprintf('\n');
        %%
        for i=1:length(W)-1
            subplot(ndays + 1, width, width * (id - 1) + i);
            DJS(day, i) = JensenShannonDivergence(W{i}.JointProbs, W{i+1}.JointProbs);
            PlotProbProb(W{i}.JointProbs, W{i+1}.JointProbs, obj.nValid);
            prettyPlot(sprintf('[%d, %d] (D_{JS}=%f)', i, i+1, DJS(day, i)), 'earlier prob [log10]', 'later prob [log10]');
            index = index + 1;
        end
    end
end
%%