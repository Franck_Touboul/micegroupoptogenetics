function img = drawMarkovModel(hmm, filename)

file = fopen('temp.dot','w');
fprintf(file, 'digraph finite_state_machine {\n');
%fprintf(file, 'graph [ bgcolor = "#cccccc"];');
fprintf(file, 'rankdir=LR;\n');
fprintf(file, 'node [fontname = "Calibri", fontcolor = "white", width="1", penwidth=5, pencolor="white", shape = circle  color="#f0f0f0" style = filled];\n');
fprintf(file, 'size="8,5";\n');
% blue-green
% for i=1:256
%     cmap(i, 1:3) = [48, 165, 256-i];
% end
%%
for i=1:256
    cmap(i, 1:3) = (i - 1) / 255 * [48, 165, 256] + (256 - i) / 255 * [256, 0, 0];
end
cmap = cmap / 256;
cmap = colormap(cmap);
%mat=[]; for i=1:256; mat(i, 1:256) = i; end; imagesc(mat)
%%
for i=1:size(hmm.trans, 1)
    if ~isfield(hmm, 'labels')
        hmm.labels{i} = num2str(i);
    end
    color = rgb2hsv(cmap(floor((hmm.priors(i) / max(hmm.priors)) * (size(cmap, 1) - 1)) + 1, :));
    fprintf(file, '"%s" [fillcolor="%f %f %f", pos="%f,%f"];\n', hmm.labels{i}, color(1), color(2), color(3), hmm.coord(i, 1), hmm.coord(i, 2));
%    fprintf(file, '"%s" [fillcolor = "#30a5f6" pos="%f,%f"];\n', hmm.labels{i}, hmm.coord(i, 1), hmm.coord(i, 2));
    
end

for i=1:size(hmm.trans, 1)
    for j=1:size(hmm.trans, 2)
        if (hmm.trans(i, j) ~= 0)
            currTrans = hmm.trans(i, j) * sum(hmm.trans(i, :) > 0);
            fprintf(file, '"%s" -> "%s" [ penwidth = %f  ];\n', hmm.labels{i}, hmm.labels{j}, 5 * currTrans);
        end
    end
end
% 30a5f6

fprintf(file, '}\n');
fclose(file);
system('neato.exe -n -Tpng -otemp.png  temp.dot');
%system('gs -q -dNOPAUSE -dBATCH -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -sDEVICE=png16m -sOutputFile=file.png file.ps');
img = imread('temp.png');
if nargout < 1
    clf;
    imshow(img);
end
if nargin > 1
    system(['neato.exe -n -Teps -o' filename ' temp.dot']);
end
%drawDot('temp.dot', gca);

% radios = 4;
% for i=1:size(hmm.trans, 1)
%     for j=1:size(hmm.trans, 2)
%         if i~=j && hmm.trans(i, j) ~= 0
%             x1 = hmm.coord(i, 1);
%             y1 = hmm.coord(i, 2);
%             x2 = hmm.coord(j, 1);
%             y2 = hmm.coord(j, 2);
%             v = [x2 - x1, y2 - y1];
%             angle = atan2(v(2), v(1));
%             v = [v(1) - radios * cos(angle), v(2) - radios * sin(angle)];
%             x2 = x1 + v(1);
%             y2 = y1 + v(2);
%             xv = [v(2) v(1)];
%             xv = xv / norm(xv);
%             x1 = x1 + xv(1) / 1;
%             x2 = x2 + xv(1) / 1;
%             y1 = y1 + xv(2) / 1;
%             y2 = y2 + xv(2) / 1;
%             
%             line([x1 x2], [y1 y2], 'Color', 'k', 'LineWidth', hmm.trans(i, j) * 10, 'Marker', 'o', 'MarkerSize', 10, 'MarkerFaceColor', 'k');
%             hold on;
%         end
%     end
% end
% 
% for i=1:size(hmm.coord, 1)
%     x = hmm.coord(i, 1);
%     y = hmm.coord(i, 2);
%     sz = sqrt(hmm.size(i, 1).^2 + hmm.size(i, 2).^2);
%     drawCircle(x, y, radios, 'g')
%     text(x, y, num2str(i - 1), 'HorizontalAlignment','center', 'FontWeight', 'Bold', 'FontSize', 16);
%     hold on;
% end
% 
% 
% hold off;