function MyImagesc(a, showbar, scale, ticks)
nlevels = 256;
cmap = MyDefaultColormap(nlevels);
%cmap = MyCategoricalColormap(nlevels);
%cmap = makeColorMap([255 255 255]/255,[0 0 0]/255,nlevels);
%cmap = makeColorMap([0 0 255]/255, [255 0 0]/255,nlevels);
%cmap = makeColorMap([65 78 80]/255, [255 255 255]/255, [17 167 231]/255,nlevels);
cmap = makeColorMap([255 255 255]/255, [255 255 255]/255, [170 55 73]/255, nlevels);
cmap = makeColorMap([255 255 255]/255, [212 155 164]/255, [170 55 73]/255, nlevels);
cmap = makeColorMap([255 255 255]/255, [180 80 95]/255, [170 55 73]/255, nlevels);
cmap = makeColorMap([255 255 255]/255, [175 67 84]/255, [170 55 73]/255, nlevels);
cmap = makeColorMap([255 255 255]/255, [170 55 73]/255, nlevels);
cmap = MyMakeColormap([255 255 255]/255, [170 55 73]/255, nlevels);
%cmap = MyMakeColormap([255 255 255]/255, [73 55 170]/255, nlevels);
%cmap = MySaturationColormap;
%%
% from = [255 255 255]; 
% to = [170 55 73];
% s = sequence(0, 1, nlevels);
% b = 10;
% s = log(1 + b*s) / log(1 + b);
% cmap(:, 1) = from(:, 1) + (to(:, 1) - from(:, 1)) .* s;
% cmap(:, 2) = from(:, 2) + (to(:, 2) - from(:, 2)) .* s;
% cmap(:, 3) = from(:, 3) + (to(:, 3) - from(:, 3)) .* s;
% cmap = cmap / 255;
%%
%cmap = 

if exist('scale', 'var') && ~isempty(scale)
    m1 = scale(1);
    m2 = scale(2);
else
    m1 = min(a(:));
    m2 = max(a(:));
end

r = (a - m1) / (m2 - m1);
idx = round(r * (nlevels - 1)) + 1;
idx(isnan(idx)) = 1;
m = ind2rgb(idx, cmap);
m(cat(3, isnan(a), cat(3, isnan(a), isnan(a)))) = 0;
image(m);
if exist('showbar', 'var') && showbar
    colormap(cmap);
    h = colorbar('WestOutside');
    if exist('ticks', 'var')
        set(h, 'YTick', (ticks - m1) / (m2 - m1) * 256);
    else
        ticks = get(h, 'YTick') / 256 * (m2 - m1) + m1;
    end
    labels = {};
    for i=1:length(ticks)
        labels{i} = sprintf('%.2f', ticks(i));
    end
    set(h, 'YTickLabel', labels);
end
