function y = MyBilinear(b, x)
y1 = b(1) + b(2) * x;
y2 = b(3) + b(4) * x;

y = (y1>y2) .* y1 + (y1<=y2) .* y2;