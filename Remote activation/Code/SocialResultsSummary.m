SocialExperimentData
idrange = 1:10;
Colors = {'P', 'R', 'O', 'G'};
%%
fprintf('David''s score\n\n');
for id=idrange
    obj = TrackLoad({id 2});
    [NormDSonDij, NormDSonPij] = SocialDavidScore(obj);
    for i=1:obj.nSubjects
        fprintf('\t%s  %s   ', obj.FilePrefix, Colors{i});
        fprintf('%5.2f', NormDSonDij(i));    
        fprintf('\n');
    end
end


%%
fprintf('Fraction of time in shelter\n\n');
for id=idrange
    outframes = zeros(1, obj.nSubjects);
    totalframes = zeros(1, obj.nSubjects);
    for day=1:4
        obj=TrackLoad({id day});
        outframes = outframes + sum(~obj.sheltered(:, obj.valid), 2)';
        totalframes = totalframes + sum(obj.valid);
    end
    fprintf('\t%s  ', obj.FilePrefix);
    fprintf('%5.2f', outframes ./ totalframes);
    fprintf('\n');
end
%%
fprintf('Potts model (day 2)\n\n');
stat.Potts = [];
for id=idrange
    for day=2
        obj = TrackLoad({id day});
        fprintf('\t%s  ', obj.FilePrefix);
        fprintf('\t multi-info: ');
        fprintf('%10.3f [bits]', sum(obj.Analysis.Potts.Ik));
        stat.Potts(id).MultiInfo = sum(obj.Analysis.Potts.Ik);
        fprintf('\t\t %% explained: ');
        fprintf('%10.3f%%', cumsum(obj.Analysis.Potts.Ik)/sum(obj.Analysis.Potts.Ik)*100);
        stat.Potts(id).pExplained = (cumsum(obj.Analysis.Potts.Ik)/sum(obj.Analysis.Potts.Ik)*100)';
        fprintf('\n');
    end
end

%%
fprintf('Entropy (2nd day)\n\n');
stat.Entropy = [];
for id=idrange
    day=2;
    obj = TrackLoad({id day});
    for s=1:obj.nSubjects
        p = histc(obj.zones(s, obj.valid), 1:obj.ROI.nZones);
        p = p / sum(p);
        stat.Entropy(id, s) = -p * log2(p' + (p' == 0));
        %me.addrow({obj.fileprefix, colors{i}, sprintf()});
        fprintf('\t%s  %s   ', obj.FilePrefix, Colors{s});
        fprintf('%.3f\n', stat.Entropy(id, s));
    end
end

%%
fprintf('Contacts (four days)\n\n');
for id=idrange
    c = zeros(obj.nSubjects);
    day = 2;
    obj = TrackLoad({id day});
    
    fprintf('\t%s\n', obj.FilePrefix);
    disp(obj.Hierarchy.Group.AggressiveChase.Contacts);
end

%%
fprintf('Aggressive chase-escape (four days)\n\n');
for id=idrange
    c = zeros(obj.nSubjects);
    day = 2;
    obj = TrackLoad({id day});
    
    fprintf('\t%s\n', obj.FilePrefix);
    disp(obj.Hierarchy.Group.AggressiveChase.ChaseEscape);
end

%%
fprintf('Follow (four days)\n\n');
for id=idrange
    c = zeros(obj.nSubjects);
    for day = 1:4;
        obj = TrackLoad({id day});
        c = c + obj.Hierarchy.ChaseEscape.ChaseEscape;
    end
    fprintf('\t%s\n', obj.FilePrefix);
    disp(c);
end


%%
fprintf('Approaches (four days)\n\n');
for id=idrange
    c = zeros(obj.nSubjects);
    for day = 1:4;
        obj = TrackLoad({id day});
        
        for i=1:length(obj.Contacts.List)
            curr = obj.Contacts.List(i);
            s1 = curr.subjects(1);
            s2 = curr.subjects(2);
            c(s1, s2) = c(s1, s2) + curr.states(1, 2);
            c(s2, s1) = c(s2, s1) + curr.states(2, 2);
        end
    end
    
    fprintf('\t%s\n', obj.FilePrefix);
    disp(c);
end
%%
fprintf('Potts model (daily)\n\n');
stat.Potts = [];
for id=idrange
    for day=1:4
        obj = TrackLoad({id day});
        fprintf('\t%s  ', obj.FilePrefix);
        fprintf('\t multi-info: ');
        fprintf('%10.3f [bits]', sum(obj.Analysis.Potts.Ik));
        stat.Potts(id).MultiInfo = sum(obj.Analysis.Potts.Ik);
        fprintf('\t\t %% explained: ');
        fprintf('%10.3f%%', cumsum(obj.Analysis.Potts.Ik)/sum(obj.Analysis.Potts.Ik)*100);
        stat.Potts(id).pExplained = (cumsum(obj.Analysis.Potts.Ik)/sum(obj.Analysis.Potts.Ik)*100)';
        fprintf('\n');
    end
end
%%
fprintf('MI/duration [bits per hour] (daily)\n\n');
stat.Potts = [];
for id=idrange
    for day=1:4
        obj = TrackLoad({id day});
        fprintf('\t%s  ', obj.FilePrefix);
        fprintf('\t multi-info: ');
        fprintf('%10.3f [bits]\n', sum(obj.Analysis.Potts.Ik) / (sum(obj.valid) / obj.FrameRate / 60 / 60));
    end
end

%%
fprintf('Entropy (daily)\n\n');
stat.Entropy = [];
for id=idrange
    for day=1:4;
        obj = TrackLoad({id day});
        for s=1:obj.nSubjects
            p = histc(obj.zones(s, obj.valid), 1:obj.ROI.nZones);
            p = p / sum(p);
            stat.Entropy(id, s) = -p * log2(p' + (p' == 0));
            %me.addrow({obj.fileprefix, colors{i}, sprintf()});
            fprintf('\t%s  %s   ', obj.FilePrefix, Colors{s});
            fprintf('%.3f\n', stat.Entropy(id, s));
        end
    end
end

%%
fprintf('Tracking duration (daily)\n\n');
stat.Entropy = [];
for id=idrange
    for day=1:4
        obj = TrackLoad({id day});
        fprintf('\t%s  ', obj.FilePrefix);
        fprintf('%10.3f [hours]\n',(sum(obj.valid) / obj.FrameRate / 60 / 60));
    end
end

%%
fprintf('Contacts per hour (four days)\n\n');
for id=idrange
    duration = 0;
    for day=1:4
        obj = TrackLoad({id day});
        duration = duration + sum(obj.valid) / obj.FrameRate / 60 / 60;
    end
    
    fprintf('\t%s\n', obj.FilePrefix);
    disp(obj.Hierarchy.Group.AggressiveChase.Contacts/duration);
end

%%
fprintf('Aggressive chase-escape per hour (four days)\n\n');
for id=idrange
    duration = 0;
    for day=1:4
        obj = TrackLoad({id day});
        duration = duration + sum(obj.valid) / obj.FrameRate / 60 / 60;
    end
    
    fprintf('\t%s\n', obj.FilePrefix);
    disp(obj.Hierarchy.Group.AggressiveChase.ChaseEscape/duration);
end

%%
fprintf('Approaches per hour (four days)\n\n');
for id=idrange
    c = zeros(obj.nSubjects);
    duration = 0;
    for day = 1:4;
        obj = TrackLoad({id day});
        duration = duration + sum(obj.valid) / obj.FrameRate / 60 / 60;
        
        for i=1:length(obj.Contacts.List)
            curr = obj.Contacts.List(i);
            s1 = curr.subjects(1);
            s2 = curr.subjects(2);
            c(s1, s2) = c(s1, s2) + curr.states(1, 2);
            c(s2, s1) = c(s2, s1) + curr.states(2, 2);
        end
    end
    
    fprintf('\t%s\n', obj.FilePrefix);
    disp(c/duration);
end

%%
fprintf('\n\n\n\nContacts\n\n');
fprintf('%-8s', 'Type');
fprintf('%4s', 'ID');
fprintf('%6s', 'Day');
for i=1:obj.nSubjects
    for j=i+1:obj.nSubjects
        fprintf('%6s', [Colors{i} '-' Colors{j}]);
    end
end
fprintf('\n');
%
for id=idrange
    day = 2;
    obj = TrackLoad({id day});
    c = obj.Hierarchy.Group.AggressiveChase.Days;
    %%
    data = TrackParse(obj);
    for d=0:length(c)
        fprintf('%-8s', data.Type);
        fprintf('%4d', data.GroupID);
        if d == 0 
            fprintf('%6s', 'all');
            nC = obj.Hierarchy.Group.AggressiveChase.Contacts;
        else
            fprintf('%6d', d);
            nC = c(d).Contacts;
        end
        for i=1:obj.nSubjects
            for j=i+1:obj.nSubjects
                fprintf('%6d', nC(i, j));
            end
        end
        fprintf('\n');
    end
end

%%
fprintf('\n\n\n\nContacts\n\n');
fprintf('%-8s', 'Type');
fprintf('%4s', 'ID');
fprintf('%6s', 'Day');
for i=1:obj.nSubjects
    for j=i+1:obj.nSubjects
        fprintf('%6s', [Colors{i} '-' Colors{j}]);
    end
end
fprintf('\n');
%
for id=idrange
    day = 2;
    obj = TrackLoad({id day});
    c = obj.Hierarchy.Group.AggressiveChase.Days;
    %%
    data = TrackParse(obj);
    for d=0:length(c)
        fprintf('%-8s', data.Type);
        fprintf('%4d', data.GroupID);
        if d == 0 
            fprintf('%6s', 'all');
            nC = obj.Hierarchy.Group.AggressiveChase.Contacts;
        else
            fprintf('%6d', d);
            nC = c(d).Contacts;
        end
        for i=1:obj.nSubjects
            for j=i+1:obj.nSubjects
                fprintf('%6d', nC(i, j));
            end
        end
        fprintf('\n');
    end
end

%%
fprintf('\n\n\n\nAggressive Chase-Escape\n\n');
fprintf('%-8s', 'Type');
fprintf('%4s', 'ID');
fprintf('%6s', 'Day');
for i=1:obj.nSubjects
    for j=1:obj.nSubjects
        if i==j; continue; end;
        fprintf('%6s', [Colors{i} '-' Colors{j}]);
    end
end
fprintf('\n');
%
for id=idrange
    day = 2;
    obj = TrackLoad({id day});
    c = obj.Hierarchy.Group.AggressiveChase.Days;
    %%
    data = TrackParse(obj);
    for d=0:length(c)
        fprintf('%-8s', data.Type);
        fprintf('%4d', data.GroupID);
        if d == 0 
            fprintf('%6s', 'all');
            nC = obj.Hierarchy.Group.AggressiveChase.ChaseEscape;
        else
            fprintf('%6d', d);
            nC = c(d).ChaseEscape;
        end
        for i=1:obj.nSubjects
            for j=1:obj.nSubjects
                if i==j; continue; end;
                fprintf('%6d', nC(i, j));
            end
        end
        fprintf('\n');
    end
end

%%
for id=1:5
    for day = 1:4;
        obj = TrackLoad({id day});
        if day==1
            fprintf('%s\n\n', obj.FilePrefix);
        end
        %%
        data = TrackParse(obj);
        map = obj.Contacts.Behaviors.AggressiveChase.Map;
        eventTime = sec2time( min([obj.Contacts.List.beg]) / obj.FrameRate);
        eventDate = ['2011-01-0' num2str(day)];
        for i=find(map)
            fprintf('%10s %10s %4s %4s\n', eventDate, eventTime{i}, Colors{obj.Contacts.Behaviors.AggressiveChase.Chaser(i)}, Colors{obj.Contacts.Behaviors.AggressiveChase.Escaper(i)});
        end
        
    end
end
