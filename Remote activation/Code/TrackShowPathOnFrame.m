function TrackShowPathOnFrame(obj, frame)
%%
idx = 1;
Img = {};
% for f=sequencei(frame(1), frame(end), 1)
%     Img{idx} = myMMReader(obj.VideoFile, f);
%     idx = idx + 1;
% end
Img{1} = myMMReader(obj.VideoFile, frame(end));
%%
close
a = .75;
for idx = 1:length(Img);
    if idx == 1
        img = Img{length(Img)};
    else
        img = a * img + (1 - a) * Img{length(Img) - idx + 1}; 
    end
end
imshow(img);
%
hon
cmap = MyMouseColormap;
for s=1:obj.nSubjects
    X = obj.x(s, frame);
    Y = obj.y(s, frame);
    h = obj.hidden(s, frame);
%    Y = interp1(frame(~h), y(~h), frame, 'spline');
%    X = interp1(frame(~h), x(~h), frame, 'spline');
    plot(X, Y, 'o:', 'MarkerFaceColor', cmap(s, :), 'MarkerEdgeColor', 'none', 'color', cmap(s,:)); 
%     for f=frame(1)+5:10:frame(end-1)
%         
%         from = [obj.x(s, f) size(img, 1)- obj.y(s, f)];
%         to = [obj.x(s, f+1) size(img, 1)-obj.y(s, f+1)];
%         MyArrow(from, to + (to - from) * 5);
%     end
end
hoff
