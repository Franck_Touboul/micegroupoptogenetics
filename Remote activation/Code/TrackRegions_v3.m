options.test.name = 'trial_1';
options.output_path = 'res/';
%%
filename = [options.output_path options.test.name '.meta.mat'];
load(filename);
%%
pos = get(gcf, 'Position');
num = gcf;
close(gcf)
figure(num);
set(gcf, 'Position', pos);
%imshow(meta.bkgFrame);
ebkgFrame = im2double(histeq(rgb2gray(meta.bkgFrame)));
%set(ehandle, 'AlphaData', 0);
%hold off;

%TrackDefineRegionsAux(meta.bkgFrame);

%slider_func = @(hObj,event,ax) TrackDefineRegionsAux(get(hObj, 'Value') / 100);
% slider_func = @(hObj,event,ax) set(ehandle, 'AlphaData', get(hObj, 'Value') / 100);
%
% sh = uicontrol(gcf,'Style','slider',...
%                 'Max',100,'Min',0,'Value',25,...
%                 'SliderStep',[0.05 0.2],...
%                 'Position',[30 20 250 20], 'Callback', slider_func);

% bgh = uibuttongroup('Parent',gcf,...
%     'Position',[0.05 0.91 .90 .08]);
% 
% textControl = uicontrol(gcf,'Style','text','String','area for: "Big Nest"',...
%     'Units','normalized',...
%     'HorizontalAlignment','left',...
%     'FontSize', 12, ...
%     'Position',[0.06 0.02 .88 .06]);

textControl = annotation('textbox',[0.05 0.91 .90 .08], ...
    'EdgeColor', [0 0 0], ...
    'Color', [.2 .2 .3],...
    'LineStyle', 'none', ...
    'FontSize', 14, ...
    'String', 'choose area for: "Big Nest"');
%
r = ebkgFrame;
g = ebkgFrame;
b = ebkgFrame;
img = cat(3, r, g, b);
img = TrackShowRegion(img, false(size(r)), [1 1 1]);
imshow(img);

roi.fields = {'Feeder1', 'Feeder2', 'Water', 'SmallNest', 'Labyrinth', 'BigNest'};
roi.fieldsIndex = struct();

cmap = lines(length(roi.fields));
for i=1:length(roi.fields)
    curr = roi.fields{i};
    set(textControl, 'String', ['choose area for: ' curr]);
    map = roipoly;
    roi = setfield(roi, curr, map);
    img = TrackShowRegion(img, map, cmap(i, :));
    imshow(img);
    roi.fieldsIndex = setfield(roi.fieldsIndex, roi.fields{i}, i);
end
%
fprintf('# - saving ROIs...\n'); set(textControl, 'String', 'Saving ROIs...');
filename = [options.output_path options.test.name '.roi.mat'];
save(filename, 'roi');

%%
% filename = [options.output_path options.test.name '.roi.mat'];
% load(filename, 'roi');
% for i=1:length(roi.fields)
%     roi.fieldsIndex = setfield(roi.fieldsIndex, roi.fields{i}, i);
% end
%%

fprintf('# - setting zones\n'); set(textControl, 'String', 'Setting zones...');
filename = [options.output_path options.test.name '.social.mat'];
load(filename, 'social');
social.roi = roi;
%%
social.zones.all = social.x * 0;
for s=1:social.nSubjects
    for i=1:length(roi.fields)
        currRoi = getfield(roi, roi.fields{i});
        x = round(social.x(s, :));
        y = round(social.y(s, :));
        x(x < 1) = 1; x(x > size(currRoi, 2)) = size(currRoi, 2);
        y(y < 1) = 1; y(y > size(currRoi, 1)) = size(currRoi, 1);
        currZones = social.zones.all(s, :);
        currZones(currRoi(sub2ind(size(currRoi), y, x))) = i;
        social.zones.all(s, :) = currZones;
    end
end

%%
fprintf('# - saving zones\n'); set(textControl, 'String', 'Saving zones...');
filename = [options.output_path options.test.name '.social.mat'];
save(filename, 'social');
fprintf('# - done!\n'); set(textControl, 'String', 'Done!');

%%
% curr = 'bigNest';
%
% roi.bigNest = [];
% roi.smallNest = [];
%
% currImage = ebkgFrame;
% while 1~=2
%     map = roipoly;
%     roi = setfield(roi, curr, map);
%     if get(rbh1, 'Value') == 1
%         curr = 'bigNest';
%     elseif get(rbh2, 'Value') == 1
%         curr = 'smallNest';
%     end
%     r = ebkgFrame;
%     g = ebkgFrame;
%     b = ebkgFrame;
%     b(roi.bigNest) = b(roi.bigNest) / 2;
%     r(roi.smallNest) = r(roi.smallNest) / 2;
%     imshow(cat(3, r, g, b));
% end
%
