Path = '../Movies/';
Group = 'OTRFloxTreat';
Id = 1;

usePrototype = false;
Prototype.Path = '../base/Res/';
Prototype.Group = Group;
Prototype.Id = 1;

%%
Cameras = [1 4];
for c=Cameras
    filename = sprintf([Path '%s.exp%04d.day%02d.cam%02d.avi'], Group, Id, 1, c);
    if ~exist(filename, 'file')
        filename = [];
    else
        break;
    end
end
if isempty(filename)
    error('unable to find movie file');
end
obj = TrackGenerateObject(filename);
%%
if usePrototype
    for c = Cameras
        prototype = sprintf([Prototype.Path '%s.exp%04d.day%02d.cam%02d.obj.mat'], Prototype.Group, Prototype.Id, 1, c);
        if ~exist(prototype, 'file')
            prototype = [];
        else
            break;
        end
    end
    if isempty(prototype)
        error('unable to find prototype file');
    end
    pobj = TrackLoad(prototype, {'Colors', 'ROI'});
    obj.Colors = pobj.Colors;

    obj = TrackMarkRegionsGUI(...
        obj, ...
        pobj.ROI.RegionNames,...
        pobj.ROI.IsSheltered);
    close(gcf);
else
    obj = TrackMarkRegionsGUI(...
        obj, ...
        {'Feeder1', 'Feeder2', 'Water', 'SmallNest', 'Labyrinth', 'BigNest', 'Block'},...
        [ false,     false,     false,   true,        false,       true,     false  ]);
    close(gcf);
end
%%
obj.Output = true;

obj = TrackFindBackground(obj);
obj = TrackFindDarkPeriod(obj, .2);
if ~usePrototype
    obj = TrackComputeColorsGUI(obj);
    %obj = TrackComputeColors(obj);
    %obj = TrackComputeColorsAutomatically(obj);
end
obj = TrackSave(obj);
