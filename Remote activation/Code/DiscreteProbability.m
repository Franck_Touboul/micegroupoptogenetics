function [p, idx] = DiscreteProbability(x, histogram, minval, maxval)

nbins = length(histogram);
step = (maxval - minval) / nbins;
idx = floor((x - minval) / step) + 1;
idx(x == maxval) = nbins;
invalid = idx > nbins | idx < 1 | isnan(x);
idx(invalid) = 1;
p = histogram(idx);
p(invalid) = nan;
if nargin > 1
    idx(invalid) = 0;
end