SocialExperimentData;
for i=1:GroupsData.nExperiments
    %%
    obj = TrackLoad({i 2});
    %%
    [obj, img, eth, cmap] = SocialBehaviorEthogram(obj);
    %%
    duration = 1 * 60 * 60 * obj.FrameRate;
    offset = 0;
    idx = 1;
    while offset+duration < obj.nFrames
        imagesc(eth(:, offset+1:offset+duration));
        colormap(cmap);
        axis off;

        %%
        try
            fname = 'temp_SocialBehaviorEthogram.png';
            print(gcf, ['temp_SocialBehaviorEthogram.png'], '-dpng');
            im = imread(fname);
            sim = sum(im ~= 255, 3);
            x1 = find(sum(sim), 1);
            x2 = find(sum(sim), 1, 'last');
            y1 = find(sum(sim, 2), 1);
            y2 = find(sum(sim, 2), 1, 'last');
            img = im(y1:y2, x1:x2, :);
            p = ['Graphs/Ethogram/' num2str(idx) '/'];
            EnsurePath(p);
            imwrite(img, [p 'SocialBehaviorEthogram.' obj.FilePrefix '.png']);
        end
        offset = offset + duration;
        idx = idx + 1;
    end
    
end
