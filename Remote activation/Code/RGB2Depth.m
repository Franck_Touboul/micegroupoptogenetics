function depth = RGB2Depth(im)

if ~isa(im, 'uint8')
    im = im2uint8(im);
end

cmap = HilbertCurve;
%cmap = [cmap(:, 3), cmap(:, 2), cmap(:, 1)];
base = [2048 32 1];
idx = (base * double(cmap'))'+1;
[q, c2d] = sort(idx);
%%
fbase = [2048/8 32/4 1/8];
i = double(reshape(im, [size(im,1)*size(im,2), 3])) * fbase' + 1;
joints = find(round(i) ~= i);
i = floor(i);
i(i > 2^16) = 2^16;
depth = reshape(c2d(i), [size(im, 1), size(im, 2)]);
depth(joints) = 2^16 - depth(joints);
%%
depth = (depth - 1)/8/1000;
depth(depth <= 0) = nan;
%%
if nargout < 1
    imagesc(depth);
%     d = depth;
%     d(depth == 1) = nan;
%     d(depth > 15000) = nan;
%     mesh(d)
%     drawnow
end

%%
% im2 = zeros([size(ref,1), size(ref,2), 3], 'uint8');
% for i=1:size(ref,1)
%     for j=1:size(ref,2)
%         d = ref(i, j);
%         im2(i, j, :) = cmap(d+1, :) .* uint8([8 4 8]);
%     end
% end