function h = myFigCopy(fc,fm, position)
%fc = 3;
%fm = 3;
% FIGCOPY - copies one figure into another figure and lets the
% users move and  resize the copied figure. This utility is great
% if you need a figure in a figure and still would like to use
% MATLAB's export function.
%
% h = FIGCOPY the user selects both figures by mouse clicks. Please
% follow the instructions in the command window. First activate the
% figure to be copied. Second activate the "master" figure and
% select the lower left and upper right corner, where the copied
% figure should appear in the "master" figure. Click on the copied
% figure to move or resize it. (Don't worry, the resize markers
% will not be printed!) h is the handle of the copied figure.
%
% h = FIGCOPY(fc) fc specifies the figure number (handle) to be
% copied. The master figure is selected by mouse click.
%
% h = FIGCOPY(fc,fm) specifies the figure number to be copied and
% the figure number of the master figure.

% Dirk Tenne
% CoDE (Control Dynamics and Estimation)
% web:      "http://code.eng.buffalo.edu"
% created:  "long time ago"
% modified: "December 12. 2002" updated the input handling
%           "November 27. 2007" copy only figure axes ignoring the new
%           uicontrols of Figures.
%

% input handling
% is the figure number given or should it be clicked?
if nargin < 3
    position = 'northeast';
end

%hcfig = get(fc,'Children');
% only look for axes handles as new versions also return uicontrols
hcfig = findobj(fc,'Type','axes');

% specify the position of the figure
figure(fm);

a = axis;
tsx = a(2) - a(1);
tsy = a(4) - a(3);
boundx = tsx / 15; szx = tsx / 4;
boundy = tsy / 15; szy = tsy / 4;
if strcmp(lower(position), 'southwest')
    xi = [a(1) + boundx, a(1) + boundx + szx];
    yi = [a(3) + boundy, a(3) + boundy + szy];
elseif strcmp(lower(position), 'northwest')
    xi = [a(1) + boundx, a(1) + boundx + szx];
    yi = [a(4) - boundy - szy, a(4) - boundy];
elseif strcmp(lower(position), 'northeast')
    xi = [a(2) - boundx - szx, a(2) - boundx];
    yi = [a(4) - boundy - szy, a(4) - boundy];
elseif strcmp(lower(position), 'southeast')
    xi = [a(2) - boundx - szx, a(2) - boundx];
    yi = [a(3) + boundy, a(3) + boundy + szy];
end
%normalizing x and y
m_axis = axis;
m_ax_pos = get(gca,'position');
alphax = m_ax_pos(3); % rescaling the units
alphay = m_ax_pos(4);
x_max = diff(m_axis(1:2)); y_max = diff(m_axis(3:4));
x = (xi-m_axis(1))/x_max*alphax;
y = (yi-m_axis(3))/y_max*alphay;

% placing the figure
pos(1) = m_ax_pos(1) + x(1);
pos(2) = m_ax_pos(2) + y(1);
siz(1) = abs(x(1)-x(2));
siz(2) = abs(y(1)-y(2));
newhcfig = copyobj(hcfig,fm);
for j = 1:length(newhcfig)
    set(newhcfig(j),'Position',[pos siz])
end

% output handling
if nargout > 0
    h = newhcfig;
end

