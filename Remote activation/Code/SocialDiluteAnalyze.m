fs_ = [1 2 3 6 12 25 50 125 250 750 1500 3000 7500 15000 30000 45000];
map = rand(1, obj.nFrames) >= .5;
trainobj = SocialApplyMap(obj, map);
testobj = SocialApplyMap(obj, ~map);

stat = [];
stat.jointProbs = [];
stat.fs = [];
stat.model = [];
[q, independentProbs, jointProbs] = SocialComputePatternProbsFast(testobj);
stat.testJointProbs = jointProbs;
idx = 1;
for fs=fs_
    fs
    q = trainobj;
    q.zones = trainobj.zones(:, trainobj.valid);
    q.zones = q.zones(:, 1:fs:sum(trainobj.valid));
    q.nFrames = size(q.zones, 2);
    q.valid = true(1, q.nFrames);
    q.OutputToFile = false;
    q.OutputInOldFormat = false;
    q = SocialPotts(q, struct('order', 3));
    [q, independentProbs, jointProbs] = SocialComputePatternProbsFast(q);
    stat.jointProbs(idx, :) = jointProbs(:)';
    stat.fs(idx) = fs;
    stat.models(idx).me = q.Analysis.Potts;
    idx = idx + 1;
end
%%
idx = 1;
for fs=fs_
    fs
    q = testobj;
    q.zones = testobj.zones(:, testobj.valid);
    q.zones = q.zones(:, 1:fs:sum(testobj.valid));
    q.nFrames = size(q.zones, 2);
    q.valid = true(1, q.nFrames);
    q.OutputToFile = false;
    q.OutputInOldFormat = false;
    [q, independentProbs, jointProbs] = SocialComputePatternProbsFast(q);
    stat.testProbs(idx, :) = jointProbs(:)';
    idx = idx + 1;
end

%%
idx = 1;
Djs = [];
meDjs = [];
for fs=fs_
%    Djs(idx) = JensenShannonDivergence(stat.testJointProbs, stat.jointProbs(idx, :));
    Djs(idx) = JensenShannonDivergence(stat.testProbs(idx, :), stat.jointProbs(idx, :));

    idx = idx + 1;
end
Djs

ticks = [25 250 1500 15000 45000];
ticklabels = {};
for i=1:length(ticks)
    ticklabels{i} = num2str(ticks(i)/ obj.FrameRate) ;
end
    plot(log2(fs_), Djs, 'x-')
set(gca, 'XTick', log2(ticks), 'XTickLabel', ticklabels);