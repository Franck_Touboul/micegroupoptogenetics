%% parameters
n_runs = 10;
n_iters = 50;
iter_size = 12000; % number of samples used in each iteration
cmd = @TrainOnlineFeedforwardNetwork;
defaults = struct(...
    'layer_size', [2 25 25 1], ...
    'repeat', 4, ...
    'transient', 2, ...
    'learning_rate', 0.01,...
    'bias_learning_rate', 0.01,...
    'iteration_size', iter_size, ...
    'sparse', 0, ...
    'output', 0, ...
    'n', 2000 * n_iters, ...
    'd', 3 ...
    );
%    'n', iter_size * n_iters, ...

%%
p = {};

% p{1}.title = 'Noise Standard Deviation';
% p{1}.name = 'noiseSTD';
% p{1}.value = {0.01 0.02 0.04 0.08 0.16 0.32};
% p{1}.inf = 0;

%p{2}.title = 'Adaptive Noise Ratio';
%p{2}.name = 'adaptiveNoiseFactor';
%p{2}.value = {0.01 0.02 0.04 0.08 0.16 0.32};
%p{2}.inf = 0;

p{1}.title = 'Oja Learning';
p{1}.name = 'oja_learning_rate';
p{1}.value = { 0 0.0001 };
p{1}.inf = 0;
% 
% p{3}.title = 'Runing Average Timescale';
% p{3}.name = 'timeScale';
% p{3}.value = {32 64 128 256 512 1024 100000};
% p{3}.inf = 1;

%% program
s = {};
s = parametrize(s, p);

totalProb = {};

for i=1:length(s)
    curr_cmd = cmd;
    parameters = defaults;
    for j=1:length(p)
        parameters.(p{j}.name) = s{i}{j};
    end
    for run=1:n_runs
        fprintf('\n\n-------------------------------------------------------------\n');
        fprintf('param set no. %d/%d,  run %d:\n', i, length(s), run);
        fprintf('-------------------------------------------------------------\n\n');
        [in, out] = ChessPattern(parameters);
        net = GenerateSparseNetwork(parameters);
        %
        validation_parameters = parameters;
        validation_parameters.correlation = 0;
        validation_parameters.repeat = 0;
        validation_parameters.transient = 0;
        [validation_in, validation_out] = ChessPattern(validation_parameters);
        validation.in = validation_in(1:5000, :);
        validation.out = validation_out(1:5000, :);
        %
        [trained, estimation_error] = cmd(net, parameters, in(1:n_iters * iter_size, :), out(1:n_iters * iter_size, :), validation);
        totalProb{i}{run} = estimation_error;
    end
end

