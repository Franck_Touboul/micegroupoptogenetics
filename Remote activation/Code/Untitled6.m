order = 2;
nobj = obj;
me = nobj.Analysis.Potts.Model{order};
Entropy = [];
Djs = [];
Maps = [];
Removed = [];
idx = 1;
method = 'kl';
%%
p = exp(me.perms * me.weights');
perms_p = exp(me.perms * me.weights');
Z = sum(perms_p);
P = p / Z;
%%
while length(me.labels) > 2
    fprintf('#-------------------------------------------------\n');
    fprintf('# using %d features (%.1f%%)\n', length(me.labels), length(me.labels)/length(obj.Analysis.Potts.Model{order}.labels)*100);
    map = true(1, length(me.labels));
    switch lower(method)
        case 'minabs'
            [minval, minidx] = min(abs(me.weights));
        case 'median'
            [s, o] = sort(me.weights);
            minidx = o(floor(length(o)/2));
        case 'kl'
            KL = zeros(1, length(me.weights));
            w = me.weights;
            for i=1:length(me.weights)
                w(i) = 0;
                if i>1
                    w(i-1) = me.weights(i-1);
                end
                p = exp(me.perms * me.weights');
                p = p / sum(p);
                KL(i) = KullbackLeiblerDivergence(p(:)', P(:)');
            end
            [val, minidx] = min(KL);
    end
    map(minidx) = false;
    Removed(idx) = minidx;
    if idx == 1
        Maps = map;
    else
        Maps(idx, Maps(idx-1, :)) = map;
    end
    nobj = retrainPottsModel(nobj, order, map);
    % Entropy
    p = exp(me.perms * me.weights');
    perms_p = exp(me.perms * me.weights');
    Z = sum(perms_p);
    p = p / Z;
    %
    Entropy(idx) = -p' * log2(p);
    Djs(idx) = JensenShannonDivergence(p(:)', P(:)');
    fprintf('# Djs=%.1f, Entropy=%.1f\n', Djs(idx), Entropy(idx));
    %
    me = nobj.Analysis.Potts.Model{order};
    idx = idx + 1;
end
%%
nParams = sum(Maps, 2);
plot(nParams, Djs)
xlabel('# interactions');
ylabel('Djs');

%%
nInteraction = 100;
me = obj.Analysis.Potts.Model{order};
conn = Removed(end-nInteraction+1:end);
me.order = me.order(conn);
me.weights = me.weights(conn);
me.labels = { me.labels{conn} };

r = 10;
coord = [];
for i=1:obj.ROI.nZones
    for s=1:obj.nSubjects
        R = r + s;
        coord(:, i, s) = [R * sin(i/obj.ROI.nZones * 2*pi), R * cos(i/obj.ROI.nZones * 2*pi)];
        plot(R * sin(i/obj.ROI.nZones * 2*pi), R * cos(i/obj.ROI.nZones * 2*pi), 'ko', 'MarkerFaceColor', 'k', 'MarkerSize', 10);        
        hold on;
    end
    R = r + obj.nSubjects + 1;
    if sin(i/obj.ROI.nZones * 2*pi) < 0
        text(R * sin(i/obj.ROI.nZones * 2*pi), R * cos(i/obj.ROI.nZones * 2*pi), obj.ROI.ZoneNames{i}, 'HorizontalAlignment', 'right');
    else
        text(R * sin(i/obj.ROI.nZones * 2*pi), R * cos(i/obj.ROI.nZones * 2*pi), obj.ROI.ZoneNames{i});
    end
end

[s, ow] = sort(me.weights);
ow = ow(me.order(ow) == 2);
cmap = MyDefaultColormap(length(ow));
idx = 1;
for l=ow
    x = [coord(1, me.labels{l}(2,1), me.labels{l}(1,1)) coord(1, me.labels{l}(2,2), me.labels{l}(1,2))];
    y = [coord(2, me.labels{l}(2,1), me.labels{l}(1,1)) coord(2, me.labels{l}(2,2), me.labels{l}(1,2))];
    plot(x, y, 'Color', cmap(idx, :), 'LineWidth', 2);
    idx = idx + 1;
end
axis off;
hold off;
