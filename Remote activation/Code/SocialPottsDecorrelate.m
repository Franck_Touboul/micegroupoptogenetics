function res = SocialPottsDecorrelate(obj)
%%
rand('twister',sum(100*clock));
randn('seed',sum(100*clock));
%%
obj = TrackLoad(obj);
%%
p = rand(1);
curr = obj;
for s=1:obj.nSubjects
    map = rand(1, curr.nFrames) < p;
    map(~obj.valid) = false;
    z = curr.zones(s, map);
    z = z(randperm(length(z)));
    curr.zones(s, map) = z;
end
curr.OutputToFile = false;
curr.OutputInOldFormat = false;
curr = SocialPotts(curr);
fprintf('@exp=%s\n', obj.FilePrefix);
fprintf('@p=%f\n', p);
fprintf('@Ik=');
fprintf('%g ', curr.Analysis.Potts.Ik);
fprintf('\n');
