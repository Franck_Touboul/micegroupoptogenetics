function [ResultsTable, totalEvents] = findEngagements(yairMatOrig1, yairMatOrig2)
%  Social behavior code - agrresive/non-aggresive

% 1) Preparing the data matrix for work
% 2) Calculating contacts
% 3) Finding "islands" of "1" or "11" (false contacts)
% 4) Unifying contacts into "engagements" by canceling small gaps
% 5) Creating a cell array of engagements and their respective raw data
% 6) Characterizing the engagements (aggresive or non-aggresive)
% 7) Plots


% 1)                 Preparing the data matrix for work


%Loading an Excel file for each of the two mice to be analized
% fileName=input('insert file name first mouse: ', 's');
% fileName=input('insert file name second mouse: ', 's');
% [yairMatOrig1,yairText1]=xlsread(fileName);
% [yairMatOrig2,yairText2]=xlsread(fileName);

% After loading the files we just transpose the matrixes
yairMatOrig1= yairMatOrig1';
yairMatOrig2= yairMatOrig2';

% From the original Excel file we take only the "time" row from mouse
% 1: time
% 3: X coordinate
% 4: Y coordinate
% 8: distance
% 9: velocity
% 10: turn angle
% 11: angular velocity
% 12: Meander
% 13: labyrinth
% 14: big nest
% 16: small nest
% 21:safty strip
% 22: uppersmallnest

yairMatOrig1= yairMatOrig1([1 3 4 8 9 10 11 12 13 14 16 21 22 ], :);
yairMatOrig2= yairMatOrig2([3 4 8 9 10 11 12 13 14 16 21 22], :);

% Checking if the mouse was in the labyrinth, in the small nest or in the big
% nest and adding NaN in the corresponding coordinates. The consequence of
% adding NaNs is canceling contacts in the labyrinth, small and big nest
% so we just deal with contacts in the open space.

nestVec1=find((sum(yairMatOrig1([9 10 11 13],:))>0));
nestVec2=find((sum(yairMatOrig2([8 9 10 12],:))>0));

% just creating a copy of the vectors to keep the originals without NaNs
yairMat1=yairMatOrig1;
yairMat2=yairMatOrig2;

% Inserting NaN in the new yairMats when the mouse is in a "zone"
yairMat1([2 3],nestVec1)=NaN;
yairMat2([1 2],nestVec2)=NaN;

% Unifying both mice into one matrix
yairMat=[yairMat1([1 2 3],:); yairMat2([1 2],:); yairMat1([4 5],:); ...
    yairMat2([3 4],:); yairMat1(12,:); yairMat2(11,:); yairMat1([ 6 7 8],:);yairMat2([5 6 7],:) ];

yairMatOrig=[yairMatOrig1([1 2 3],:); yairMatOrig2([1 2],:); yairMatOrig1([4 5],:); ...
    yairMatOrig2([3 4],:); yairMatOrig1(12,:); yairMatOrig2(11,:); yairMatOrig1([ 6 7 8],:);yairMatOrig2([5 6 7],:) ];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 2)                            Calculating contacts

%Pitagoras to calculate distances between 2 mice in each time bin
DistanceMice=sqrt((yairMat(2,:)- yairMat(4,:)).^2 + (yairMat(3,:)- yairMat(5,:)).^2);
yairMatOrig(18,:)=DistanceMice;

%Calculating contacts (contact is defined as distance<15 cm)
ContactMice=DistanceMice<15;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%3)             Finding "islands" of "1" or "11" (false contacts)


% Noting beginings and ends of gaps between contacts(1 at first cell before
% begining,-1 at the last cell)
gapBeginEnd=ContactMice(1:(end-1))-ContactMice(2:end);

%identifying gap beginings locations
%(note that each member of gapBegin vector is the place of the "1" at the
% begining of the gap and not the place of the first zero)
gapBegin=find(gapBeginEnd==1);

%identifying gap ends locations(note that in contrast to gapBegin,
% each member of gapEnd vector is the place of the last zero of the gap)
gapEnd=find(gapBeginEnd==-1);

% canceling first (false) gap end
%(this happens when the ContactMice vector starts with zeros)
if gapEnd(1)< gapBegin(1);

    gapEnd=gapEnd(2:end);

end

% equaling lenght of vectors (canceling false (last) gap begin)
%(this happens when the ContactMice vector finishes with zeros)
if length(gapBegin)>length(gapEnd);
    gapBegin=gapBegin(1:end-1);
end

% puting 0 instead of "islands" of "1" or "11" in ContactMice

for l=1:length(gapBegin)
    if ContactMice(gapBegin(l))+ ContactMice(gapBegin(l)-1)+ ContactMice(gapBegin(l)-2)~=3
        ContactMice(gapBegin(l)) = 0;
    end
    if ContactMice(gapEnd(l)+1)+ ContactMice(gapEnd(l)+2)+ ContactMice(gapEnd(l)+3)~=3
        ContactMice(gapEnd(l)+1)= 0;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% 4)           Unifying contacts into "engagements" by canceling small gaps


%repeating the above procedure for calculating new gap begins and ends (line 36-57):

% Noting beginings and ends of gaps between contacts(1 at first cell before
% begining,-1 at the last cell)
gapBeginEnd=ContactMice(1:(end-1))-ContactMice(2:end);

%identifying gap beginings locations
RealGapBegin=find(gapBeginEnd==1);

%identifying gap ends locations()

RealGapEnd=find(gapBeginEnd==-1);

% canceling first (false) gap end
if RealGapEnd(1)< RealGapBegin(1);

    RealGapEnd=RealGapEnd(2:end);

end

% equaling lenght of vectors (canceling false (last) gap begin)
if length(RealGapBegin)>length(RealGapEnd);
    RealGapBegin=RealGapBegin(1:end-1);
end

% calculating the lenght of the gaps
gapLength=RealGapEnd-RealGapBegin;

% canceling gaps that are equal or less then 2 sec (50 cells)
% by replacing zeros with ones in a vector engageMice
%
smallGap=find(gapLength<50);

B=[];
for i=1:length(smallGap);
    A=(RealGapBegin(smallGap(i))+1:RealGapEnd(smallGap(i)));
    B=[B,A];
end
engageMice=ContactMice;
engageMice(B)=1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 5)         Creating a cell array of engagements and their respective raw data


%Noting beginings and ends of engagements
engageBeginEnd=engageMice(1:(end-1))-engageMice(2:end);

%identifying engage beginings locations (-1 at first cell befor begining and1 at the last cell)
engageBegin=find(engageBeginEnd==-1);

%identifying engage ends locations

engageEnd=find(engageBeginEnd==1);

% canceling first false contact end
if engageEnd(1)< engageBegin(1);

    engageEnd=engageEnd(2:end);

end
% equaling lengh of vectors (canceling false last begin)
if length(engageBegin)>length(engageEnd);
    engageBegin=engageBegin(1:end-1);
end

%Creating a cell array that will include all the engagements
totalEvents=[];
engageLength=engageEnd-engageBegin;
trueEngages=find(engageLength>10);
NewEngageBegin=engageBegin(trueEngages);
NewEngageEnd=engageEnd(trueEngages);
for ii= 1:length(NewEngageBegin);
    event= engageMice(NewEngageBegin(ii)+1 : NewEngageEnd(ii));
    totalEvents{length(totalEvents)+1}={event};

end
%Adding original (raw data) coordinates under each event vector
%totalEvents:
% row {1}: contacts vector
% row {2}: Matrix including: row 1: X mouse 1
%                            row 2: Y mouse 1
%                            row 3: X mouse 2
%                            row 4: Y mouse 2
%                            row 5: velocity mouse 1
%                            row 6: velocity mouse 2
%                            row 7: big nest strip mouse 1
%                            row 8: big nest strip mouse 2
%                            row 9: turn angle mouse 1
%                            row 10: turn angle mouse 2
%                            row 11: distance (pitagoras)
%                            row 12: time line
% row {3}: velocity results
% row {4}: length of engagement result
% row {5}: aggresive/non-aggresive

for iii=1:length(totalEvents);
    totalEvents{iii}=[totalEvents(iii); yairMatOrig([2 3 4 5 7 9 10 11 12 15 18 1],(NewEngageBegin(iii)+1 : NewEngageEnd(iii)))];
    totalEvents{iii}{2}([13 14 15 16],:)=yairMat([2 3 4 5],(NewEngageBegin(iii)+1 : NewEngageEnd(iii)));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 6)         Characterizing the engagements (aggresive or non-aggresive)


aggresCont=0;
affilCont=0;
unknownCont=0;
NaNVec=0;
NaNInt=0;
immobile=0;

ResultsTable={'','type','start','short/long'};
% Here starts the big loop that analize each engagement

for c=1:length(totalEvents);

    % Calculating engagement length
    engageLength=length(totalEvents{c}{2}(1,:));
    
    % Linear transformation for correlation analysis
    
    %determining speed status of both mice "before" the event
    % immobile=0
    % Slow=1
    % Fast=3

    %mouse 1
    if mean(totalEvents{c}{2}(5,1:7))>35;
        totalEvents{c}{3}(1,1)=3;
    elseif mean(totalEvents{c}{2}(5,1:7))<17 | immobileMouse(totalEvents{c}{2}(1,:),totalEvents{c}{2}(2,:))==1;
        totalEvents{c}{3}(1,1)=0;
    else
        totalEvents{c}{3}(1,1)=1;
    end
    %mouse 2
    if mean(totalEvents{c}{2}(6,1:7))>35;
        totalEvents{c}{3}(2,1)=3;
    elseif mean(totalEvents{c}{2}(6,1:7))<17 | immobileMouse(totalEvents{c}{2}(3,:),totalEvents{c}{2}(4,:))==1;
        totalEvents{c}{3}(2,1)=0;
    else
        totalEvents{c}{3}(2,1)=1;
    end

    %determining speed status of both mice "during" the event
    % immobile=0
    % Slow=1
    % Fast=3

    %mouse 1
    if mean(totalEvents{c}{2}(5,8:end-7))>35;
        totalEvents{c}{3}(1,2)=3;
    elseif mean(totalEvents{c}{2}(5,8:end-7))<17 | immobileMouse(totalEvents{c}{2}(1,:),totalEvents{c}{2}(2,:))==1;
        totalEvents{c}{3}(1,2)=0;
    else
        totalEvents{c}{3}(1,2)=1;
    end

    %mouse 2
    if mean(totalEvents{c}{2}(6,8:end-7))>35;
        totalEvents{c}{3}(2,2)=3;
    elseif mean(totalEvents{c}{2}(6,8:end-7))<17 | immobileMouse(totalEvents{c}{2}(3,:),totalEvents{c}{2}(4,:))==1;
        totalEvents{c}{3}(2,2)=0;
    else
        totalEvents{c}{3}(2,2)=1;
    end

    %determining speed status of both mice "after" the event
    % immobile=0
    % Slow=1
    % Fast=3

    %mouse 1
    if mean(totalEvents{c}{2}(5,end-6:end))>35;
        totalEvents{c}{3}(1,3)=3;
    elseif mean(totalEvents{c}{2}(5,end-6:end))<17 | immobileMouse(totalEvents{c}{2}(1,:),totalEvents{c}{2}(2,:))==1;
        totalEvents{c}{3}(1,3)=0;
    else
        totalEvents{c}{3}(1,3)=1;
    end

    %mouse 2
    if mean(totalEvents{c}{2}(6,end-6:end))>35;
        totalEvents{c}{3}(2,3)=3;
    elseif mean(totalEvents{c}{2}(6,end-6:end))<17 | immobileMouse(totalEvents{c}{2}(3,:),totalEvents{c}{2}(4,:))==1;
        totalEvents{c}{3}(2,3)=0;
    else
        totalEvents{c}{3}(2,3)=1;
    end

    totalEvents{c}{3}(3,:)= totalEvents{c}{3}(1,:)+ totalEvents{c}{3}(2,:);
    totalEvents{c}{3}(1,4)=totalEvents{c}{3}(1,1)+totalEvents{c}{3}(1,2)+totalEvents{c}{3}(1,3);
    totalEvents{c}{3}(2,4)=totalEvents{c}{3}(2,1)+totalEvents{c}{3}(2,2)+totalEvents{c}{3}(2,3);

    %testing if the event is short (0) or long (1)
    if length(totalEvents{c}{2}(1,:))<24
        totalEvents{c}{4}=0;
    else totalEvents{c}{4}=1;
    end


    % Finding if in a "significant" part of the engagement the mouse was lost
    % and the Ethovision fits with a linear interpolation

    %     engageDiff1=totalEvents{c}{2}(5,1:end-1)- totalEvents{c}{2}(5,2:end);
    %     engageDiff2=totalEvents{c}{2}(6,1:end-1)- totalEvents{c}{2}(6,2:end);
    %
    %     engDiff1=engageDiff1~=0;
    %     engDiff2=engageDiff2~=0;
    %
    %     engageGap1=engDiff1(1:end-1)-engDiff1(2:end);
    %     engageGap2=engDiff2(1:end-1)-engDiff2(2:end);
    %
    %     engGapBegin1=find(engageGap1==1);
    %     engGapEnd1=find(engageGap1==-1);
    %
    %     engGapBegin2=find(engageGap2==1);
    %     engGapEnd2=find(engageGap2==-1);
    %
    %     if engGapEnd1< engGapBegin1;
    %     engGapEnd1=engGapEnd1(2:end);
    %     end
    %     if engGapEnd2< engGapBegin2;
    %     engGapEnd2=engGapEnd2(2:end);
    %     end
    %
    %     if length(engGapBegin1)>length(engGapEnd1);
    %     engGapBegin1=engGapBegin1(1:end-1);
    %     end
    %     if length(engGapBegin2)>length(engGapEnd2);
    %     engGapBegin2=engGapBegin2(1:end-1);
    %     end
    %
    %     engGapLength1=engGapEnd1-engGapBegin1;
    %     engGapLength2=engGapEnd2-engGapBegin2;
    %
    %     engBigGap1=find(engGapLength1>10);
    %     engBigGap2=find(engGapLength2>10);

    %testing kind of contact based on the behavior

    if sum(sum(isnan(totalEvents{c}{2}(13,:))))/length(totalEvents{c}{2}(13,:))>0.1 |...
            sum(sum(isnan(totalEvents{c}{2}(15,:))))/length(totalEvents{c}{2}(15,:))>0.1
        totalEvents{c}{5}='NaN';
        NaNVec=NaNVec+1;
       
    elseif length(find(totalEvents{c}{2}(5,1:end-1)- totalEvents{c}{2}(5,2:end)==0))>length(totalEvents{c}{2}(5,:))*0.8 |...
            length(find(totalEvents{c}{2}(6,1:end-1)- totalEvents{c}{2}(6,2:end)==0))>length(totalEvents{c}{2}(6,:))*0.8;

        totalEvents{c}{5}='NaN interpolation';
        NaNInt=NaNInt+1;

    elseif sum(totalEvents{c}{2}(7,:))/length(totalEvents{c}{2}(7,:))>0.49 |...
            sum(totalEvents{c}{2}(8,:))/length(totalEvents{c}{2}(8,:))>0.49

        totalEvents{c}{5}='unknown engagement';

        unknownCont=unknownCont + 1;

    elseif   ((totalEvents{c}{3}(3,1)>2 | totalEvents{c}{3}(3,3)>2) &...
            totalEvents{c}{3}(1,4)>0 & totalEvents{c}{3}(2,4)>0 &...
            totalEvents{c}{4}==1) | ...
            ((corrcoef(totalEvents{c}{2}(1,engageLength/2:end),totalEvents{c}{2}(3,engageLength/2:end))>0.8 |...
            corrcoef(totalEvents{c}{2}(2,engageLength/2:end),totalEvents{c}{2}(4,engageLength/2:end))>0.8) &...
            totalEvents{c}{4}==1) |...
            sum(totalEvents{c}{3}(:,4))>=12;

        totalEvents{c}{5}='aggresive';

        aggresCont=aggresCont + 1;

    elseif totalEvents{c}{3}(1,4)==0 | totalEvents{c}{3}(2,4)==0

        totalEvents{c}{5}='one mouse immobile';
        immobile=immobile + 1;
    else
        totalEvents{c}{5}='Non agressive';

        affilCont=affilCont+1;

        
    end
        ResultsTable{c+1,1}=c;
        ResultsTable{c+1,2}=totalEvents{c}{5};
        ResultsTable{c+1,3}=totalEvents{c}{2}(12,1);
        ResultsTable{c+1,4}=totalEvents{c}{4};
        
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 7)                                  Plots

% coordX=(yairMat(2,:)+ yairMat(4,:))/2;
% coordY=(yairMat(3,:)+ yairMat(5,:))/2;
% contactXY=find(ContactMice==1);
% plotX=coordX(contactXY);
% plotY=coordY(contactXY);
% 
% plot(plotX,plotY,'.');
% axis([-20 25 -30 35]);
% hold all
% 
% figure
% hBar=bar3([aggresCont  affilCont unknownCont NaNVec immobile ]);
% 
% figure
% m1Plot=plot(yairMat(2,:),yairMat(3,:));
% m2Plot=plot(yairMat(4,:),yairMat(5,:));
% 
% figure
% m1XYmatrix=yairMat([2 3],:);
% m2XYmatrix=yairMat([4 5],:);
% 

% {'aggresive';'non-aggresive';'unknown';'NaN';'Nan immobile'}


