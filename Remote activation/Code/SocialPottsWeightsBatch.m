SocialExperimentData
%%
weights = {};
for g=1:length(Groups)
    weights{g, 1} = [];
    weights{g, 2} = []; 
    weights{g, 3} = []; 
    group=Groups(g);
    for id=group.idx
        for day = 1:nDays
            prefix = sprintf(experiments{id}, day);
            fprintf('# -> %s\n', prefix);
            obj = TrackLoad(['../base/Res/' prefix '.obj.mat'], {'Analysis'});
            for j=1:3
                w = [];
                for i=1:length(obj.Analysis.Potts.Model{3}.weights)
                    if size(obj.Analysis.Potts.Model{3}.labels{i}, 2) == j
                        w = [w, obj.Analysis.Potts.Model{3}.weights(i)];
                    end
                end
                weights{g, j} = [weights{g, j}; w(:)'];
            end
        end
    end
end
%%
nbins = 30;
x = {};
for i=1:3
    [h,x{i}] = hist([weights{1, i}(:); weights{2, i}(:)], nbins);
end
%%
clf
for g=1:length(Groups)
    for i=1:3
        h = [];
        for j=1:size(weights{g, i}, 1)
            h(j, :) = hist(weights{g, i}(j, :), x{i});
        end
        subplot(3,1,i);
        errorbar(x{i}, mean(h), stderr(h), 'Color', Groups(g).color);
        hold on;
    end
end
subplot(3,1,1);
a = axis;
a(1) = -6;
a(2) = 1;
axis(a);

subplot(3,1,2);
a = axis;
a(1) = -6;
a(2) = 1;
axis(a);


%

%%
clf;
nbins = 30;
for g=1:length(Groups)
    for i=1:3
        subplot(3,1,i);
%        [h,x] = hist(exp(weights{g, i}), nbins);
        [h,x] = hist((weights{g, i}), nbins);
        plot(x, h, 'Color', Groups(g).color);
        hold on;
    end
end