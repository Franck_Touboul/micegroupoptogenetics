SocialExperimentDataMD;

O = struct();

idx = 1;
for id=GroupsData.index
    for day=1:GroupsData.nDays
        try
            prefix = sprintf(experiments{id}, day);
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Hierarchy', 'Analysis'});
            O.Rank(idx, :) = obj.Hierarchy.ChaseEscape.rank;
            O.ChaseEscape(:, :, idx) = obj.Hierarchy.ChaseEscape.interactions.PostPredPrey;
            Contacts = zeros(obj.nSubjects);
            for i=1:obj.nSubjects
               for j=1:obj.nSubjects
                   Contacts(i, j) = length(obj.Hierarchy.ChaseEscape.interactions.events{i,j});
               end
            end
            O.Contacts(:, :, idx) = Contacts;
            %O.PottsOrder(idx, :) = cumsum(obj.Analysis.Potts.Ik) / sum(obj.Analysis.Potts.Ik);
            
            data = TrackParse(obj);
            O.Day(idx) = data.Day;
            O.GroupID(idx) = data.GroupID;
            O.Type{idx} = data.Type;
            idx = idx + 1;
        end
    end
end