function obj = SocialPredPreyModel_v4(obj)
fprintf('# Predator-Prey model\n');
%%
obj = TrackLoad(obj);
%%
obj.Interactions.PredObjectLength = 75;
obj.Interactions.PreyObjectLength = 150;
obj.Interactions.MinSubInteractionDuration = 5;

obj.Interactions.ZoneOfProximity = 200;
obj.Interactions.ZoneOfContact = 100;

obj = SocialFindInteractions(obj);
%%
IdelState = 1;
ContactState = 4;

%%
converge = zeros(obj.nSubjects, obj.nSubjects, obj.nFrames);
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1 == m2; continue; end;
        q = tan(obj.Interactions.angle{m1, m2}) .* obj.Interactions.distance{min(m1, m2), max(m1, m2)};
        converge(m1, m2, q > 0 & abs(q) < obj.Interactions.PredObjectLength) =  1;
        converge(m1, m2, q < 0 & abs(q) < obj.Interactions.PredObjectLength) = -1;
    end
end
converge(isnan(converge)) = 0;

%%
speed = obj.speed(:);
minspeed = FindProbabilityCutoffPoint(speed(speed>0), .75);
%%
features = cell(obj.nSubjects);
index = 1;
fprintf('# - processing subjects ');
ProgressReport();
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1 == m2; continue; end;
        ProgressReport(index, obj.nSubjects * (obj.nSubjects - 1));
        %%
        others = true(1, obj.nSubjects);
        others(m1) = 0;
        others(m2) = 0;
        sheltered = obj.sheltered(m1, :) | obj.sheltered(m2, :);
        %%
        features{m1, m2} = ones(1, obj.nFrames) * 2;
        features{m1, m2}(converge(m1, m2, :) == -1) = 1;
        features{m1, m2}(converge(m1, m2, :) ==  0) = 2;
        features{m1, m2}(converge(m1, m2, :) ==  1) = 3;
        features{m1, m2}(obj.sheltered(m1, :) | obj.sheltered(m2, :)) = 2;
        features{m1, m2}(obj.speed(m1, :) < minspeed) = 2;
        
        features{m1, m2}(obj.Interactions.proximity(m1, m2, :)) = features{m1, m2}(obj.Interactions.proximity(m1, m2, :)) + 3;
        
        %%
        %
        %        (pred)        (pred)
        %       /      \      /      \
        % (idel) ------ (cont) ------ (idel)
        %       \      /      \      /
        %        (prey)        (prey)
        %
        
        x = .5;
        m = .25;
        u = 1/3;
        model.emis = [...
            m   x   m   0.0 0.0 0.0; % idel
            m   m   x   m   m   x; % pred
            x   m   m   x   m   m; % prey
            0.0 0.0 0.0 m   x   m; % cont
            m   m   x   m   m   x; % pred
            x   m   m   x   m   m; % prey
            ];
        
        model.trans = [...
            1 1 1 1 0 0;
            0 1 0 1 0 0;
            0 0 1 1 0 0;
            1 0 0 1 1 1;
            1 0 0 0 1 0;
            1 0 0 0 0 1;
            ];
        model.names = {'-', 'pred', 'prey', 'cont', 'pred', 'prey'};
        obj.Interactions.PredPrey.model = model;
        
        states = hmmviterbi(features{m1, m2},model.trans,model.emis);
        
        %% remove short events
        [begF, endF, len] = FindEvents(states, ...
            states ~= IdelState & states ~= ContactState);
        for i=find(len < obj.Interactions.MinSubInteractionDuration)
            states(begF(i):endF(i)) = ContactState;
        end
        %%
        [begF, endF, len, events] = FindEvents(states, states > 1);
        valid = true(1, length(events));
        for i=1:length(events)
            events{i}.desc = [];
            events{i}.bounds = [];
            range = events{i}.BegFrame:events{i}.EndFrame;
            nContacts = sum(obj.Interactions.contact(m1, m2, range));
            valid(i) = nContacts > 0;
            if ~valid(i)
                continue;
            end
            events{i}.open = any(obj.zones(m1, range) == obj.zones(m2, range) & obj.zones(m1, range) == 1);
            valid(i) = any(obj.zones(m1, range) == obj.zones(m2, range) & obj.zones(m1, range) == 1);
            if ~valid(i)
                continue;
            end
            valid(i) = valid(i) && all(sum(obj.Interactions.contact(m2, others, range), 3) < nContacts);
            if ~valid(i)
                continue;
            end
            if events{i}.data(1) == 4
                f = find(obj.Interactions.contact(m1, m2, range), 1);
                events{i}.data = events{i}.data(f:end);
                events{i}.BegFrame = events{i}.BegFrame + f - 1;
                range = range(f:end);
            end
            if events{i}.data(end) == 4
                f = find(obj.Interactions.contact(m1, m2, range), 1, 'last');
                events{i}.data = events{i}.data(1:f);
                events{i}.EndFrame = events{i}.BegFrame + f - 1;
                range = range(1:f);
            end
            
            for j=1:length(model.names)
                r = GetRange(events{i}.data == j);
                if ~isempty(r)
                    if ~isempty(events{i}.desc)
                        events{i}.desc = [events{i}.desc ' -> '];
                    end
                    %            events{i}.desc = [events{i}.desc, model.names{j} '(' num2str(r(2)-r(1)+1) ')'];
                    events{i}.desc = [events{i}.desc, model.names{j} '(' num2str(events{i}.BegFrame -1 + r(1)) ')'];
                    events{i}.bounds = [events{i}.bounds; events{i}.BegFrame -1 + [r(1), r(2)]];
                end
            end
        end
        events = events(valid);
        obj.Interactions.PredPrey.states{m1, m2} = states;
        obj.Interactions.PredPrey.events{m1, m2} = events;
        index = index + 1;
    end
end

fprintf('\n');
try
    TrackSave(obj);
catch
end