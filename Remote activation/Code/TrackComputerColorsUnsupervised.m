%function TrackComputerColorsUnsupervised(ob)

pixels = {};
pixels{1} = [];
pixels{2} = [];
pixels{3} = [];

hsv{1} = [];
hsv{2} = [];
hsv{3} = [];

index = 1;
while index < 100
    img = [];
    while isempty(img)
        f = randi(obj.nFrames, 1, 1);
        [obj, img] = TrackSimpleSegmentFrame(obj, f);
        if ~isempty(img) && ~any(img.segmented(:))
            img = [];
        end
    end
    img.scaled = im2double(imresize(img.orig, obj.VideoScale));
    for i=1:3
        component = img.scaled(:, :, i);
        pixels{i} = [pixels{i}; component(img.segmented)];
        cHSV = img.hsv(:, :, i);
        hsv{i} = [hsv{i}; cHSV(img.segmented)];
    end
    index = index + 1
end
%%
% path(path,'../netlab/')
% mix = gmm(2, 5, 'full');
% gmmem(mix, [pixels{1} pixels{2}]);
[c, cents] = kmeans(double([pixels{1}, pixels{2}, pixels{3}]), 5)
%%
plot3(pixels{1}, pixels{2}, '.')
hold on;
plot3(cents(:,1), cents(:,2), cents(:,3), 'rx')
