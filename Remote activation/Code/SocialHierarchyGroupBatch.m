S = '';
SocialExperimentData;
Colors   = {'P', 'R', 'O', 'G'};
%%
for id=1:GroupsData.nExperiments
    try
        rank = SocialHierarchyGroup(id);
        %%
        s = '';
        for j=obj.nSubjects:-1:1
            g = {Colors{find(rank == j)}};
            if ~isempty(g)
                s = [s sprintf('(')];
                s = [s sprintf('%s', Colors{find(rank == j)})];
                s = [s sprintf(') ')];
            end
        end
        %%
        %fprintf([experiments{i} ' ' s '\n']);
        fprintf([regexprep(experiments{id}, '%02d', '01020304') '\t ' s '\n'], 1);
        S = [S sprintf([regexprep(experiments{id}, '%02d', '01020304') '\t ' s '\n'], 1)];
    catch
    end
end
%% Entropy
SE = '';
SocialExperimentData;
Colors   = {'P', 'R', 'O', 'G'};
for id=1:GroupsData.nExperiments
    try
        group = TrackLoad({id 0});
        %%
        H = [];
        for s=1:obj.nSubjects
            z = group.zones(s, :);
            h = histc(z, 1:group.ROI.nZones);
            h = h / sum(h);
            H(s) = sum(-h .* log(h));
        end
        %%
        [sH, o] = sort(H, 2, 'descend');
        s = sprintf('%s ', Colors{o});
        fprintf([regexprep(experiments{id}, '%02d', '01020304') '\t ' s '\n'], 1);
        SE = [SE, sprintf([regexprep(experiments{id}, '%02d', '01020304') '\t ' s '\n'], 1)];
    catch
    end
end


