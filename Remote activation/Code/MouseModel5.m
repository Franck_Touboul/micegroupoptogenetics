opt.HiddenAxis = 3;

Params = struct();

Params.AxisMundi.Yaw = 200/180*pi;
Params.AxisMundi.Pitch = 0;
Params.AxisMundi.Roll = 0/180*pi;
Params.AxisMundi.Scale = .02;

Model = ModelCreateF(Params);
Model.SkinMemory = .9;

Points = ModelToPointsF(Model);
ModelShow(Model, Points);

X = Points.Skin.X; Y = Points.Skin.Y; Z = Points.Skin.Z;
%X(Points.Skin.Area > 0) = NaN;
plot3(X,Y,-Z,'.');
hon

plot3(Depth.X,Depth.Y,Depth.Z,'.');

shading interp; colormap(gray);
axis square;
axis equal;
%%
if opt.HiddenAxis == 2
%     sc = 120;
%     of1 = 15;
%     of2 = 144;
%     img = imread('mouse-xray.jpg');
    sc = 85;
    of1 = 100;
    of2 = 200;

    img = imread('stock-photo-white-rat-isolated-on-white-background-72448069.jpg');
    img = img(:, end:-1:1, :);

else
    Params.AxisMundi.Yaw = 200/180*pi;
    Params.AxisMundi.Pitch = 0;
    Params.AxisMundi.Roll = 0/180*pi;
    Params.AxisMundi.Scale = 1;
    
    Params.Neck.Pitch = 0.1;
    Params.Neck.Yaw = -0.1;
    Params.Body.Yaw = -0.03;
    Params.Body.FatScale = 1.3;
    Params.Tail.Pitch = 1.6;
    Params.Tail.Yaw = [0.3 0.13 -0.1 -0.03];
    Params.Tail.Scale = 1.44;
    Params.Tail.Roll = 0;
    
    Model = ModelCreate(Params);
    Model.SkinMemory = .9;
    
    Points = ModelToPoints(Model);
    ModelShow(Model, Points);
    %surfl(Points.Skin.X,Points.Skin.Y,1-Points.Skin.Z); shading interp; colormap(gray);
    %axis equal;
    %     Projection = [1 0 0; 0 1 0];
    %     D = Projection * [Points.Skin.X(:), Points.Skin.Y(:), Points.Skin.Z(:)]';
    %     X = reshape(D(1, :), size(Points.Skin.X));
    %     Y = reshape(D(2, :), size(Points.Skin.X));
    %% based on: 
    %%      http://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
    X = Points.Skin.X;
    Y = Points.Skin.Y;
    Z = Points.Skin.Z;
    e1 = convn(X(:, 1:end-1) .* Y(:, 2:end) - X(:, 2:end) .* Y(:, 1:end-1), [1;  -1], 'valid');
    e2 = convn(X(1:end-1, :) .* Y(2:end, :) - X(2:end, :) .* Y(1:end-1, :), [-1,  1], 'valid');
    v = e1 + e2;
    v(:, end+1) = v(:, end);
    v(end+1, :) = v(end, :);
    valid = v<0;
    plot3(Points.Skin.X(valid),Points.Skin.Y(valid),1-Points.Skin.Z(valid), '.'); 
    hon
    plot(X(valid), Y(valid), 'b.'); hon; plot(X(~valid), Y(~valid), 'r.'); hoff

    
    %%
    plot(X, Y, '.');
    axis equal;
    %return
    %return

    sc = 50;
    of1 = 330;
    of2 = 140;
    img = imread('mouse-top.jpg');
end
A = 1:3; A = A(A ~= opt.HiddenAxis);
imshow(img);
hon
cmap = MySubCategoricalColormap;
SkeletonStyle = {'-o', 'Color', ones(1,3)*.5, 'LineWidth', 3};
if opt.HiddenAxis == 2
    plot(Points.Bone.X * sc + of1, Points.Bone.Z * sc + of2, SkeletonStyle{:})
else
    plot(Points.Bone.X * sc + of1, Points.Bone.Y * sc + of2, SkeletonStyle{:})
end
for i=unique(Model.Types(:)')
    S = Points.Skin;
    map = S.Type == i;
    
    if opt.HiddenAxis == 2
        plot(S.X(map) * sc + of1, S.Z(map) * sc + of2, 'o', 'MarkerFaceColor', cmap(i+1, :), 'MarkerEdgeColor', 'none');
    else
        plot(S.X(map) * sc + of1, S.Y(map) * sc + of2, 'o', 'MarkerFaceColor', cmap(i+1, :), 'MarkerEdgeColor', 'none');
    end
end
hoff