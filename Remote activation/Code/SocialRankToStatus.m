function [status, name] = SocialRankToStatus(rank)
names = {'\alpha', '\beta', '\gamma', '\delta'};
name = cell(1, length(rank));
status = max(rank) - rank + 1;
for s=1:length(rank)
    name{s} = names{status(s)};
end

return;
%names = {'1', '2', '3', '4'};
i = 1;
if isstruct(obj)
    rank = obj.Hierarchy.ChaseEscape.rank;
else
    rank = obj;
end
stat = cell(1, length(rank));
while any(rank)
    m = max(rank);
    map = rank == m;
    for j=find(map)
        stat{j} = names{i};
    end
    rank(map) = 0;
    i = i + 1;
end