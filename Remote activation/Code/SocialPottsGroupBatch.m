SocialExperimentData
nDays = 1;
%%
IkAll = cell(GroupsData.nGroups, nDays);
groupids = [];
days = [];
ids = [];
day = 1;
for g=1:length(Groups)
    group=Groups(g);
    for id=group.idx
        obj = TrackLoad({id 0}, {'Analysis'});
        groupids = [groupids, g];
        days = [days, day];
        ids = [ids, id];
        if isfield(obj, 'Analysis') && isfield(obj.Analysis, 'Potts')
        else
            obj = TrackLoad(['../base/Res/' prefix '.obj.mat']);
            obj = SocialPotts(obj);
        end
        IkAll{g, day} = [IkAll{g, day}; obj.Analysis.Potts.Ik];
        %Ik(IndexOf(data.Conditions, obj.Condition), obj.Day, :) = reshape(obj.Analysis.Potts.Ik, 1, 1, obj.nSubjects-1);
    end
end
%%
cmap = MySubCategoricalColormap;
cmap = cmap([2,4,6], :);
IK = {};
IKp = {};
for g=1:length(Groups)
    group=Groups(g);
    m = [];
    s = [];
    sum_m = [];
    sum_s = [];
    e_m = [];
    e_s = [];
    IK{g} = zeros(size(IkAll{g, 1}, 1), obj.nSubjects);
    IKp{g} = zeros(size(IkAll{g, 1}, 1), obj.nSubjects - 2);
    for day = 1:nDays
        m(day, :) = mean(IkAll{g, day});
        s(day, :) = stderr(IkAll{g, day});
        sum_m(day) = mean(sum(IkAll{g, day}, 2));
        sum_s(day) = stderr(sum(IkAll{g, day}, 2));
        e = cumsum(IkAll{g, day},2) ./ repmat(sum(IkAll{g, day}, 2), 1, obj.nSubjects-1) * 100; e = e(:, 1:end-1);
        IKp{g} = IKp{g} + e;
        e_m(day, :) = mean(e);
        e_s(day, :) = stderr(e);
        IK{g} = IK{g} + [IkAll{g, day}, sum(IkAll{g, day}, 2)];
    end
    %%
    subplot(2,GroupsData.nGroups + 1,g);
    for i=1:obj.nSubjects-1
        errorbar(m(:,i), s(:, i), 'Color', cmap(i, :));
        hold on;
    end
    errorbar(sum_m, sum_s, 'k:');
    %axis square
    %% significance
    %     for day = 1:nDays
    %         sig = ttest2(IkAll{1, day}, IkAll{2, day});
    %         subplot(2,3,g);
    %         for i=find(sig)
    %             plot(day, m(day, i), 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', 10);
    %         end
    %         sig = ttest2(sum(IkAll{1, day}, 2), sum(IkAll{2, day}, 2));
    %         if sig
    %             plot(day, sum_m(day), 'o', 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'none', 'MarkerSize', 10);
    %         end
    %     end
    %%
    subplot(2,GroupsData.nGroups+1,GroupsData.nGroups+1+g);
    for i=1:obj.nSubjects-2
        errorbar(e_m(:,i), e_s(:, i), 'Color', cmap(i, :));
        hold on;
    end
    %axis square
    %% significance
    %     for day = 1:nDays
    %         e1 = cumsum(IkAll{1, day},2) ./ repmat(sum(IkAll{1, day}, 2), 1, obj.nSubjects-1) * 100; e1 = e1(:, 1:end-1);
    %         e2 = cumsum(IkAll{2, day},2) ./ repmat(sum(IkAll{2, day}, 2), 1, obj.nSubjects-1) * 100; e2 = e2(:, 1:end-1);
    %         sig = ttest2(e1, e2);
    %         subplot(2,3,3+g);
    %         for i=find(sig)
    %             plot(day, e_m(day, i), 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', 10);
    %         end
    %     end
end
%
y = [];
gid = [];
days = [];
for g=1:length(Groups)
    for day = 1:nDays
        y = [y; IkAll{g, day}];
        gid = [gid; IkAll{g, day}(:, 1) * 0 + g];
        days = [days; IkAll{g, day}(:, 1) * 0 + day];
    end
end
% p = [];
% y(:, end+1) = sum(y,2);
% for i=1:size(y, 2)
%     p(:, i) = anovan(y(:, i), {gid, days});
% end
%
TieAxis(2,length(Groups)+1,1:length(Groups));
TieAxis(2,length(Groups)+1,length(Groups)+2:length(Groups)+1+length(Groups));
for g=1:length(Groups)
    subplot(2,GroupsData.nGroups+1,g);
    a = axis;
    axis([.5 GroupsData.nDays + .5 a(3) a(4)]);
    set(gca, 'XTick', 1:nDays);
    title(Groups(g).title);
    if g == 1;
        ylabel('multiple information [bits]');
        legend('pair', 'tri', 'quad', 'total');
        legend boxoff;
    end
    hold off;
    subplot(2,GroupsData.nGroups+1,GroupsData.nGroups+1+g);
    a = axis;
    axis([.5 GroupsData.nDays+.5 a(3) 100])
    xlabel('time [days]');
    set(gca, 'XTick', 1:nDays);
    if g == 1;
        ylabel('explained information [%]');
        legend('pair', 'tri');
        legend boxoff;
    end
    hold off;
end
%%
subplot(2,GroupsData.nGroups+1,GroupsData.nGroups+1);
meanval = [];
stderrval = [];
for g=1:GroupsData.nGroups
    meanval = [meanval; mean(IK{g}/nDays)];
    stderrval = [stderrval; stderr(IK{g}/nDays)];
    %sig = ttest2(v1, v2);
end
MyBarWeb(1:GroupsData.nSubjects, meanval, stderrval, GroupsData.Colors);
%colormap(GroupsData.Colors);
hold on;
a = axis;
% for i=find(sig)
%     %    plot(i, max(mean(v1(:, i)), mean(v2(:, i))) + .25, 'k*');
%     plot(i, a(4), 'k*');
% end
set(gca, 'XTickLabel', {'pair', 'tri', 'quad', 'total'});
hold off;
legend(GroupsData.Titles)
legend boxoff;
%axis square
%
subplot(2,GroupsData.nGroups+1,2*GroupsData.nGroups+2);
meanval = [];
stderrval = [];
for g=1:GroupsData.nGroups
    meanval = [meanval; mean(IKp{g}/nDays)];
    stderrval = [stderrval; stderr(IKp{g}/nDays)];
    %sig = ttest2(v1, v2);
end
MyBarWeb(1:GroupsData.nSubjects-2, meanval, stderrval, GroupsData.Colors);
% barweb([mean(v1); mean(v2)]', [stderr(v1); stderr(v2)]');
% colormap([Groups(1  ).color; Groups(2).color]);
hold on;
a = axis;
% for i=find(sig)
%     %    plot(i, max(mean(v1(:, i)), mean(v2(:, i))) + .25, 'k*');
%     plot(i, a(4), 'k*');
% end
set(gca, 'XTickLabel', {'pair', 'tri'});
hold off;
%axis square

