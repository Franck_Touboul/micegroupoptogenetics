objs = cell(1, 3);
for i=1:3
    objs{i} = TrackLoad({i -1});
    
end
%%
obj = objs{1};
Dkl = zeros(3, 5, length(obj.Analysis.Diluted));
Count = zeros(3, 5, length(obj.Analysis.Diluted));
nZero = zeros(3, 5, length(obj.Analysis.Diluted));
DklZero = [];
CountZero = [];
nZeroZero = [];
IK = [];
IN = [];
minOcc = [];
for i=1:length(objs)
    curr = objs{i};
    for level=1:length(curr.Analysis.Potts.Model)
        DklZero(i, level) = KullbackLeiblerDivergence(objs{i}.Analysis.Potts.Model{level}.jointProb(:)', objs{i}.Analysis.Potts.Model{level}.prob(:)');
        CountZero(i, level) = length(objs{i}.Analysis.Potts.Model{level}.constraints);
        nZeroZero(i, level) = sum(objs{i}.Analysis.Potts.Model{level}.constraints == 0);
    end
    mset = curr.Analysis.Diluted;
    for j=1:length(mset)
        minOcc(j) = mset{j}.MinOccurances;
        me = mset{j}.Model;
        for level=1:length(me)
            fprintf('# computing max-entropy probabilities for level %d\n', level);
            %    p = exp(me{level}.features * me{level}.weights');
            p = exp(me{level}.perms * me{level}.weights');
            perms_p = exp(me{level}.perms * me{level}.weights');
            Z = sum(perms_p);
            p = p / Z;
            me{level}.prob = p;
            Entropy(level) = -p' * log2(p);
            %Dkl(i, level, j) = KullbackLeiblerDivergence(obj.Analysis.Potts.Model{level}.prob(:)', p(:)');
            op = objs{i}.Analysis.Potts.Model{level}.jointProb(:)';
            Dkl(i, level, j) = KullbackLeiblerDivergence(op, p(:)');
            Count(i, level, j) = length(me{level}.constraints);
            nZero(i, level, j) = sum(me{level}.constraints == 0);
        end
        In = Entropy(1) - Entropy(end);
        Ik = -diff(Entropy);
        IK(i, :, j) = Ik;
        IN(i, j) = In;
    end
end
%%
clf
stat.Dkl.mean = squeeze(mean(Dkl, 1));
stat.Dkl.err = squeeze(stderr(Dkl, 1));
cmap = MyCategoricalColormap;
entries = {};
for i=1:5
    errorbar(minOcc, stat.Dkl.mean(i, :), stat.Dkl.err(i, :), 'o-', 'Color', cmap(i, :));
    hon;
    entries{i} = num2str(i);
end
for i=1:5
    mdklz = mean(DklZero);
    edklz = stderr(DklZero);
    plot([.5 max(minOcc)*2], mdklz(i) * ones(1, 2), ':', 'Color', cmap(i, :));
end

hoff
set(gca, 'XScale', 'log')
set(gca, 'XTick', minOcc)
xaxis([.5 max(minOcc)*2]);
legend(entries);
xlabel('Minimal occurrences of a group configuration');
ylabel('KL distance relative to the empirical distribution');
%%
clf
stat.Count.mean = squeeze(mean(Count, 1));
stat.Count.err = squeeze(stderr(Count, 1));
cmap = MyCategoricalColormap;
entries = {};
for i=1:5
    errorbar(minOcc, stat.Count.mean(i, :), stat.Count.err(i, :), 'o-', 'Color', cmap(i, :));
    hon;
    entries{i} = num2str(i);
end
for i=1:5
    mdklz = mean(CountZero);
    edklz = stderr(CountZero);
    errorbar([.5 max(minOcc)*2], mdklz(i) * ones(1, 2), edklz(i) * ones(1, 2), ':', 'Color', cmap(i, :));
end

hoff
set(gca, 'XScale', 'log')
set(gca, 'XTick', minOcc)
xaxis([.5 max(minOcc)*2]);
legend({'5'});
xlabel('Minimal occurrences of a group configuration');
ylabel('Number of constraints in model');

%%
clf
mdklz = mean(CountZero);
fullC = repmat(mdklz, [3, 1, 7]);
stat.Count.mean = squeeze(mean((fullC - Count) ./ (fullC - nZero), 1));
stat.Count.err = squeeze(stderr((fullC - Count) ./ (fullC - nZero), 1));
cmap = MyCategoricalColormap;
entries = {};
for i=1:5
    errorbar(minOcc, stat.Count.mean(i, :), stat.Count.err(i, :), 'o-', 'Color', cmap(i, :));
    hon;
    entries{i} = num2str(i);
end
% for i=5
%     mdklz = mean(CountZero);
%     edklz = stderr(CountZero);
%     errorbar([.5 max(minOcc)*2], mdklz(i) * ones(1, 2), edklz(i) * ones(1, 2), ':', 'Color', cmap(i, :));
% end

hoff
set(gca, 'XScale', 'log')
set(gca, 'XTick', minOcc)
xaxis([.5 max(minOcc)*2]);
legend(entries);
xlabel('Minimal occurrences of a group configuration');
ylabel('Percentage of non-zero configurations removed');
%%

%%
clf
Contrib = cumsum(IK, 2) ./ repmat(sum(IK, 2), [1, 4, 1]);

stat.Dkl.mean = squeeze(mean(Contrib, 1));
stat.Dkl.err = squeeze(stderr(Contrib, 1));
cmap = MyCategoricalColormap;
entries = {};
for i=1:4
    errorbar(minOcc, stat.Dkl.mean(i, :), stat.Dkl.err(i, :), 'o-', 'Color', cmap(i, :));
    hon;
    entries{i} = num2str(i+1);
end
% for i=5
%     mdklz = mean(DklZero);
%     edklz = stderr(DklZero);
%     errorbar([.5 max(minOcc)*2], mdklz(i) * ones(1, 2), edklz(i) * ones(1, 2), ':', 'Color', cmap(i, :));
% end

hoff
set(gca, 'XScale', 'log')
set(gca, 'XTick', minOcc)
xaxis([.5 max(minOcc)*2]);
legend(entries);
xlabel('Minimal occurrences of a group configuration');
ylabel('% explained correlation');
%%
clf
for i=1:3
    plot(minOcc, IN(i, :), 'o-', 'Color', cmap(i, :));
    hon;
end
hoff
legend({'group 1', 'group 2', 'group 3'});
xlabel('Minimal occurrences of a group configuration');
ylabel('multi-info');
yaxis([0 1.3])

%%
obj = objs{1};
for q=1:7
for i=1:5
    me = obj.Analysis.Diluted{q}.Model{i};
    subplot(3,2,i);
    p = exp(me.perms * me.weights');
            perms_p = exp(me.perms * me.weights');
            Z = sum(perms_p);
            p = p / Z;
    op = obj.Analysis.Potts.Model{1}.jointProb(:)';
    PlotProbProb(op(:)', p(:)', sum(obj.valid));
end
    drawnow;
saveFigure(['Graphs/MinOccProbProb' num2str(obj.Analysis.Diluted{q}.MinOccurances) '_Occ']);
end