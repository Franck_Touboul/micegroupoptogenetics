function y = laplace(mu, b, x)
y = exp(-abs(x-mu)/b)/2/b;