objs = cell(1, 18);
for id=1:18
    %%
    for d=1:4
        curr = TrackLoad({id d}, {'ROI', 'zones', 'nFrames', 'valid', 'Hierarchy'});
        if d == 1
            obj = curr;
        end
        curr = SocialInOut(curr);
        drawnow;
        obj.InOut.Days{d} = curr.InOut;
    end
    objs{id} = obj;
end
%%
level = 2;
stat = [];
for d=1:4
    stat(d).W = zeros(4);
end

for id=1:18
    for d=1:4
        me  = objs{id}.InOut.Days{d}.Model{level};

        [q, ord] = sort(objs{id}.Hierarchy.Group.AggressiveChase.rank);        
        seq = 1:4;
        seq(ord) = seq;
        
        me.order = cellfun(@(x) size(x,2), me.labels);
        w = zeros(objs{id}.nSubjects);
        weights = PottsVectorize_v2(obj, me, seq);
        for i=1:length(me.weights)
            if me.order > 2
                continue;
            end
            s1 = me.labels{i}(1, 1);
            s2 = me.labels{i}(1, end);
            w(s1, s2) = weights(i);
            w(s2, s1) = weights(i);
        end
        w(1:size(w, 2)+1:end) = nan;
        subplot(19,4, (id-1)*4+d);
        w(tril(ones(size(w))) == 1) = nan;
        VecImage(w, [-2 2], MyDefaultColormap);
        %imagesc(w, [-2 2]);
        colormap(MyDefaultColormap);
        axis square;
        axis off
        if GroupsData.group(id) == 1
            stat(d).W = stat(d).W + w;
        end
    end
end
%%
SocialExperimentData
res = [];
stat = [];
for d=1:4
    for g=1:2
        res(g).Ik = [];if options.drawNan
    end
    for id=1:18
        %%
        Ent = [];
        for level=1:4
            me = obj.InOut.Days{d}.Model;
            p = exp(me{level}.perms * me{level}.weights');
            perms_p = exp(me{level}.perms * me{level}.weights');
            Z = sum(perms_p);
            p = p / Z;
            Ent(level) = -p' * log2(p);
        end
        Ik = -diff(Ent);
        %%
        obj = objs{id};
        io = obj.InOut.Days{d};
        g = GroupsData.group(id);
        res(g).Ik = [res(g).Ik; cumsum(Ik) / sum(Ik)];
    end
    for g=1:2
        stat(g).Ik.mean(d, :) = mean(res(g).Ik * 100, 1);
        stat(g).Ik.stderr(d, :) = stderr(res(g).Ik * 100, 1);
    end
end
%
for g=1:2
    subplot(1,2,g);
    for o=2:4
        errorbar(1:4, stat(g).Ik.mean(:, o-1), stat(g).Ik.stderr(:, o-1));
        hon;
    end
    xaxis(.5, 4.5);
    yaxis(50, 100);
    hoff;
end
%%
obj = objs{18};
zones = ~(obj.zones == 9) + 1;
curr = SocialSetZones(obj, zones);
curr.Output = false;
curr.OutputToFile = false;
curr.OutputInOldFormat = false;
curr.ROI.nZones = 2;
curr.ROI.ZoneNames = {'In', 'Out'};
[curr, indepProbs, jointProbs] = SocialComputePatternProbsFast(curr);

%%
for level=1:4
    subplot(2,2,level);
    me = objs{16}.InOut.Days{2}.Model{level};
    PlotProbProb(objs{16}.InOut.Days{2}.testProbs, me.prob', round(curr.nFrames/6));
    xaxis(5e-3,.2e-0)
    yaxis(5e-3,.2e-0)
end

%%
for id=11:18
    obj = objs{id};
    %%
    for d=2:4
        subplot(2,2,d);
        plot(obj.InOut.Days{d}.Model{3})
    end
end
%% weight shift by days
level = 2;
order = 2;
clf;
cmap = MyCategoricalColormap;
for d=2:4
    W = [];
    pW = [];
    for id=11:18
        %[q, ord] = sort(objs{id}.Hierarchy.Group.AggressiveChase.rank);        
        [q, ord] = sort(me.weights(me.order == 1));
        seq = 1:4;
        seq(ord) = seq;
        me  = objs{id}.InOut.Days{d}.Model{level};
        me.order = cellfun(@(x) size(x,2), me.labels);
        pme = objs{id}.InOut.Days{d-1}.Model{level};
        pme.order = cellfun(@(x) size(x,2), pme.labels);
        W = [W; PottsVectorize_v2(obj, me, 1:4)];
        pW = [pW; PottsVectorize_v2(obj, pme, 1:4)];
        %W = [W; PottsVectorize_v2(obj, me, 1:4)];
        %pW = [pW; PottsVectorize_v2(obj, pme, 1:4)];
        %        w  = me.weights(me.order == order);
        %        pw = pme.weights(pme.order == order);
        %        plot(w, pw, '.')
    end
    subplot(2,2,d);
    str = '';
    for o=1:level
        % errorxy([mean(pW(:, me.order == o)); mean(W(:, me.order == o)); stderr(pW(:, me.order == o)); stderr(W(:, me.order == o))]', 'ColXe', 3, 'ColYe', 4, ...
         %    'FaceColor', cmap(o, :), 'EdgeColor', cmap(o, :));
        plot(pW(:, me.order == o), W(:, me.order == o), '.', 'color', cmap(o, :))
        hon;
        aW = W(:, me.order == o); aW = aW(:);
        apW = pW(:, me.order == o); apW = apW(:);
        [c, pval] = corr(apW, aW);
        str = [str, ', c=' num2str(c) ' (pval=' num2str(pval) ') '];
    end
    xlabel(['day ' num2str(d-1)]);
    ylabel(['day ' num2str(d)]);
    title(str)
    axis square;
    TieAxis(2,2, 2:d)
    a=axis;
    plot([min(a) max(a)], [min(a) max(a)], 'k:');
    hoff
end
%%
res = [];
res.IOcorr = [];
res.CE = [];
res.Aggrs = [];
res.group = [];
stat = [];
for g=1:2
    stat(g).weights = [];
end
%%
for id=3
    %%        subplot(7,1,7);
    
    %obj = TrackLoad({id 4}); obj = SocialInOut(obj);
    obj = objs{id};
    %%
    clf;
    weights = [];
    for d=1:4
        me = obj.InOut.Days{d}.Model{3};
        data.subjs = false(obj.nSubjects, length(me.labels));
        for i=1:length(me.labels)
            data.subjs(me.labels{i}(1, :), i) = true;
        end
        %data.order = sum(daobjs = cell(1, 18);
    end
end
for id=1:18
    %%
    for d=1:4
        curr = TrackLoad({id d}, {'ROI', 'zones', 'nFrames', 'valid', 'Hierarchy'});
        if d == 1
            obj = curr;
        end
        curr = SocialInOut(curr);
        drawnow;
        obj.InOut.Days{d} = curr.InOut;
    end
    objs{id} = obj;
end
%%
SocialExperimentData
res = [];
stat = [];
for d=1:4
    for g=1:2
        res(g).Ik = [];
    end
    for id=1:18
        %%
        Ent = [];
        for level=1:4
            me = obj.InOut.Days{d}.Model;
            p = exp(me{level}.perms * me{level}.weights');
            perms_p = exp(me{level}.perms * me{level}.weights');
            Z = sum(perms_p);
            p = p / Z;
            Ent(level) = -p' * log2(p);
        end
        Ik = -diff(Ent);
        %%
        obj = objs{id};
        io = obj.InOut.Days{d};
        g = GroupsData.group(id);
        res(g).Ik = [res(g).Ik; cumsum(Ik) / sum(Ik)];
    end
    for g=1:2
        stat(g).Ik.mean(d, :) = mean(res(g).Ik * 100, 1);
        stat(g).Ik.stderr(d, :) = stderr(res(g).Ik * 100, 1);
    end
end
%
for g=1:2
    subplot(1,2,g);
    for o=2:4
        errorbar(1:4, stat(g).Ik.mean(:, o-1), stat(g).Ik.stderr(:, o-1));
        hon;
    end
    xaxis(.5, 4.5);
    yaxis(50, 100);
    hoff;
end
%%
obj = objs{18};
zones = ~(obj.zones == 9) + 1;
curr = SocialSetZones(obj, zones);
curr.Output = false;
curr.OutputToFile = false;
curr.OutputInOldFormat = false;
curr.ROI.nZones = 2;
curr.ROI.ZoneNames = {'In', 'Out'};
[curr, indepProbs, jointProbs] = SocialComputePatternProbsFast(curr);

%%
for level=1:4
    subplot(2,2,level);
    me = objs{16}.InOut.Days{2}.Model{level};
    PlotProbProb(objs{16}.InOut.Days{2}.testProbs, me.prob', round(curr.nFrames/6));
    xaxis(5e-3,.2e-0)
    yaxis(5e-3,.2e-0)
end

%%
for id=11:18
    obj = objs{id};
    %%
    for d=2:4
        subplot(2,2,d);
        plot(obj.InOut.Days{d}.Model{3})
    end
end
%%
level = 3;
order = 2;
W = [];
pW = [];
for d=2
    for id=11:18
        me  = objs{id}.InOut.Days{d}.Model{level};
        pme = objs{id}.InOut.Days{d-1}.Model{level};
        W = [W; me.weights];
        pW = [W; pme.weights];
        %        w  = me.weights(me.order == order);
        %        pw = pme.weights(pme.order == order);
        %        plot(w, pw, '.')
    end
    clf;
    errorxy([mean(pW); mean(W); stderr(pW); stderr(W)]', 'ColXe', 3, 'ColYe', 4);
    hon;
    a=axis;
    plot([min(a) max(a)], [min(a) max(a)], 'k');
    hoff
end

%%
res = [];
res.IOcorr = [];
res.CE = [];
res.Aggrs = [];
res.group = [];
stat = [];
for g=1:2
    stat(g).weights = [];
end
%%
for id=3
    %%        subplot(7,1,7);
    
    %obj = TrackLoad({id 4}); obj = SocialInOut(obj);
    obj = objs{id};
    %%
    clf;
    weights = [];
    for d=1:4
        me = obj.InOut.Days{d}.Model{3};
        data.subjs = false(obj.nSubjects, length(me.labels));
        for i=1:length(me.labels)
            data.subjs(me.labels{i}(1, :), i) = true;
        end
        data.order = sum(data.subjs);
        data.weights = zeros(obj.nSubjects);
        
        CE = obj.Hierarchy.Group.AggressiveChase.ChaseEscape;
        for s1=1:obj.nSubjects
            data.weights(s1, s1) = nan;
            CE(s1, s1) = nan;
            for s2=s1+1:obj.nSubjects
                data.weights(s1, s2) = mean(me.weights(data.subjs(s1, :) & data.subjs(s2, :) & data.order == 2));
                data.weights(s2, s1) = data.weights(s1, s2);
            end
        end
        subplot(7,4,d:4:20);
        SocialOverlayHierarchy(obj, triu(data.weights), struct('ColorEdges', false))
        subplot(7,4,20+d);
        bar(validmean(data.weights(:)), 'FaceColor', 'b');
        yaxis(-2, 2)
        xaxis(0, 2)
        weights(d) = validmean(data.weights(:));
    end
    stat(GroupsData.group(id)).weights = [stat(GroupsData.group(id)).weights; weights];
    
    %     subplot(7,4,1:4:20);
    %     SocialOverlayHierarchy(obj, obj.Hierarchy.Group.AggressiveChase.Days(2).ChaseEscape .* sign(data.weights), struct('ColorEdges', false))
    %     title('day 2');
    %     subplot(7,4,2:4:20);
    %     SocialOverlayHierarchy(obj, obj.Hierarchy.Group.AggressiveChase.Days(3).ChaseEscape .* sign(data.weights), struct('ColorEdges', false))
    %     title('day 3');
    %     subplot(7,4,3:4:20);
    %     SocialOverlayHierarchy(obj, obj.Hierarchy.Group.AggressiveChase.Days(4).ChaseEscape .* sign(data.weights), struct('ColorEdges', false))
    %     title('day 4');
    %     subplot(7,4,4:4:20);
    %     SocialOverlayHierarchy(obj, triu(data.weights), struct('ColorEdges', false))
    %%
    for d=1:4
        subplot(7,1,7);
        %imagesc(obj.InOut.Days{d}.zones);
        %colormap([1 1 1; .7 .7 .7]);
        colormap([.86 .29 .350; .9 .9 .9]);
        %SocialOverlayHierarchy(obj, data.weights )
        %%
        saveFigure(['Graphs/InOut/' obj.FilePrefix '.' num2str(d)]);
        axis off
    end
    %%
    %saveFigure(['Graphs/InOut/' obj.FilePrefix]);
end


%%



%%
clf
for id=3
    for d=2
        curr = TrackLoad({id d}, {'zones'});
        objs{id}.InOut.Days{d}.zones = curr.zones == 9;
        
        %%
        subplot(7,1,7);
        imagesc(curr.zones == 9);
        colormap([.86 .29 .350; .9 .9 .9]);
        axis off
        %%
        saveFigure(['Graphs/InOutMap/' curr.FilePrefix]);
    end
end