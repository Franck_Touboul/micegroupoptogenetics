function [vec, i] = NumToDims(num, dims)
%%
vec = zeros(1, length(dims));
overflow = false;
for i=1:num
    j = 1;
    vec(j) = vec(j) + 1;
    while vec(j) >= dims(j)
        vec(j) = 0;
        j = j + 1;
        if j > length(dims)
            overflow = true;
            break;
        end
        vec(j) = vec(j) + 1;
    end
    if overflow
        break;
    end
end