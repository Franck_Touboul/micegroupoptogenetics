function CirclePatch(x, y, C, style, radios, N)

if nargin < 5

    radios = 1;

end

if nargin < 6

    N = 50;

end

 

X = zeros(1, N);

Y = zeros(1, N);

for i=1:N

    X(i) = x + radios * cos(2 * pi * i / N);

    Y(i) = y + radios * sin(2 * pi * i / N);

end

patch(X, Y, C, style{:});