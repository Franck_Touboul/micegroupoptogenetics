function res = followTrack_v6(options, cents, curr)

[N, nS] = size(cents.label);
dim = sum(cents.label ~= 0, 2);

%%
options.jumpVar = 3^2;
options.confProb = 0.1;
options.hideJump = options.jumpVar;

covarMat = eye(2) * options.jumpVar;

table = cents.logprob(:,:,curr);
table(cents.label == 0)    = nan;
table(:, nS+1) = flog(options.confProb);
table = table';

path = zeros(nS+1, N, 'uint8');
lastPos = [inf inf];
nValid = max([find(cents.label(1, :), 1, 'last'), 0]);

for i=2:N
    nPrevValid = nValid;
    nValid = max([find(cents.label(i, :), 1, 'last'), 0]);

    col = table(1:nPrevValid, i-1);
    hid = table(nS+1, i-1);
    vec = [cents.x(i-1, 1:nPrevValid); cents.y(i-1, 1:nPrevValid)];
    p_hide = log_gauss([0, 0], covarMat, [options.hideJump, 0]);
    for j=1:nValid
%        p = [gauss([cents.x(i, j), cents.y(i, j)], options.jumpVar, [cents.x(i-1, 1:nS); cents.y(i-1, 1:nS)]'); 1];
        p      = log_gauss([cents.x(i, j), cents.y(i, j)], covarMat, vec');
        p_jump = log_gauss([cents.x(i, j), cents.y(i, j)], covarMat, lastPos);
        [val, from] = max([p + col; p_hide + hid; p_jump + hid]);
        if from > nPrevValid
            from = nS + 1;
        end
        path(j, i) = from;
        table(j ,i) = val + table(j ,i);
    end
    %%
    [val, from] = max(p_hide + [col; hid]);
    if from > nPrevValid
        from = nS + 1;
    else
        lastPos = [cents.x(i-1, from), cents.y(i-1, from)];
    end
    path(nS + 1, i) = from;
    table(nS + 1, i) = val + table(nS + 1 ,i);
end

% backtrack 
backtrack = zeros(1, N);
[val, start] = max(table(:, end));
backtrack(end) = start;
res.x = zeros(1, N);
res.y = zeros(1, N);
res.prob = zeros(1, N);
res.id = zeros(1, N, 'uint8');

if backtrack(end) <= nS
    res.x(end) = cents.x(end, backtrack(end));
    res.y(end) = cents.y(end, backtrack(end));
    res.id(end) = backtrack(end);
else
    res.x(end) = nan;
    res.y(end) = nan;
    res.id(end) = 0;
end
res.prob(end) = table(end, backtrack(end));
for i=N-1:-1:1
    backtrack(i) = path(backtrack(i+1), i + 1);
    if backtrack(i) <= nS
        res.x(i) = cents.x(i, backtrack(i));
        res.y(i) = cents.y(i, backtrack(i));
        res.id(i) = backtrack(i);
    else
        res.x(i) = nan;
        res.y(i) = nan;
        res.id(i) = 0;
    end
    res.prob(i) = table(backtrack(i), i);
end

