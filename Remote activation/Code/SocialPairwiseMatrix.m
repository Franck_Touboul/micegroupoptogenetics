function SocialPairwiseMatrix(obj, me)

%%
ZoneMap  = [1 7 4 2 3 10 8 5 9 6];
zmap = MyZonesColormap;
w = zeros(obj.nSubjects * obj.ROI.nZones) +  nan;
for i=1:length(me.labels)
    if me.order(i) ~= 2
        continue;
    end
    l = me.labels{i};
    l(2,1) = find(l(2, 1) == ZoneMap);
    l(2,2) = find(l(2, 2) == ZoneMap);
    coord = (l(1, :) - 1) * obj.ROI.nZones + l(2, :);
    w(coord(1), coord(2)) = me.weights(i);
    w(coord(2), coord(1)) = me.weights(i);
end
%%
clf
mmap = MyMouseColormap;
VecImage(w, [-3, 3], MyDefaultColormap)
colorbar
for s=1:obj.nSubjects
    for z=1:obj.ROI.nZones
        y = (s-1)*obj.ROI.nZones + z - .5;
        patch([-1 0 0 -1]+.5, [y y y+1 y+1], zmap(ZoneMap(z), :), 'edgecolor', 'none');
        patch([y y+1 y+1 y], [-1 -1 0 0]+.5, zmap(ZoneMap(z), :), 'edgecolor', 'none');
        
        patch([-2 -1 -1 -2]+.5, [y y y+1 y+1], mmap(s, :), 'edgecolor', 'none');
        patch([y y+1 y+1 y], [-2 -2 -1 -1]+.5, mmap(s, :), 'edgecolor', 'none');
    end
end
xaxis(-1.5, obj.nSubjects * obj.ROI.nZones+.5)
yaxis(-1.5, obj.nSubjects * obj.ROI.nZones+.5)
axis square