function p = PPModelDiscretePDF(x, model, state)

p = zeros(1, size(x, 2));

if nargin > 2
    valid = x(2, :) == model.states(state).contact;
    histogram = model.states(state).histogram;
else
    valid = true(1, size(x, 2));
    histogram = model.all.histogram;
end
input = x(1, :);

p(valid) = DiscreteProbability(...
    input(valid), ...
    histogram, ...
    model.Histogram.minval, ...
    model.Histogram.maxval);

p(isnan(input) & valid) = 1/pi;

if model.UseSpeedHistogram
    ps = ones(1, size(x, 2));
    if nargin > 2
        ps(valid) = DiscreteProbability(...
            x(3, valid), ...
            model.states(state).Speed.histogram, ...
            model.Speed.Histogram.minval, ...
            model.Speed.Histogram.maxval);
    else
        ps(valid) = DiscreteProbability(...
            x(3, valid), ...
            model.all.Speed.histogram, ...
            model.Speed.Histogram.minval, ...
            model.Speed.Histogram.maxval);
    end
    p = p .* ps;
end
