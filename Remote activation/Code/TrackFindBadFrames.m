%function TrackFindBadFrames(nruns)
%%
fprintf('# finding paths\n');
options = struct();
TrackDefaults;

nruns = 140;
%options.output_path = 'Z:/tests/SocialMice/NewArena.exp0002.day02.cam04/Res/';
%options.output_path = 'Z:/tests/SocialMice/Open.exp0001.day02.cam01/Res/';
options.output_path = 'Z:/tests/SocialMice/4Core.exp0003.day01.cam01/Res/';
%%
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nframes = xyloObj.NumberOfFrames;
dt = 1/xyloObj.FrameRate;
%%
if ~exist('id', 'var')
    id = 0;
end
%%
cents.x       = [];
for i=1:nruns
    fprintf('#      . segment no. %d\n', i);
    filename = [options.output_path options.test.name '.bad.' sprintf('%03d', i) '.mat'];
    h = fopen(filename);
    succ = h ~= -1;
    if succ
        currSegm = load(filename);
        cents.x = [cents.x; currSegm.cents.x];
    fclose(h);
    else
    fprintf('#          - failed!\n');
    end
end
segm.start = 1;
segm.finish = nframes;


%%
x=cents.x(:,1)';
f = x>50;

d = diff([0 f 0], 1, 2);
start  = find(d > 0);
finish = find(d < 0) - 1;
gap = start(2:end) - finish(1:end-1) - 1;
loc = gap < 5;
for l=find(loc)
    range = finish(l)+1:start(l+1)-1;
    f(range) = true;
end

d = diff([0 f 0], 1, 2);
start  = find(d > 0);
finish = find(d < 0) - 1;
[start', finish']
