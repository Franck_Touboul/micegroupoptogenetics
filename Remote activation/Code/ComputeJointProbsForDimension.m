function [jointProbs, pci] = ComputeJointProbsForDimension(data, range, dims)
% computeJointProbs(data, max_val, fixed)
%   computed the Joint probability for a set data with discrete values
%   in the range [1:max_val].
%   If fixed is set to true then assume at least one observation for
%   each event.
if ~exist('range', 'var')
    range = max(data(:)) + 1;
end
%%
baseVector = range.^[0:size(data,1)-1];

validVector = baseVector * 0; validVector(dims) = baseVector(dims);
invalidVector = baseVector(validVector == 0);

h = validVector * data + 1;

count = histc(h, 1:range^size(data,1));
if nargout > 1
    [jointProbs, pci] = binofit(count, sum(count));
else
    jointProbs = count / sum(count);
end
%%
if ~isempty(invalidVector)
    perms = myPerms(length(invalidVector), range) - 1;
    jumps = invalidVector * perms';
    
    for i=find(count)
        jointProbs(i + jumps) = jointProbs(i);
    end
    jointProbs = jointProbs(1:length(count));
    if nargout > 1
        for i=1:length(count)
            pci(i + jumps) = pci(i);
        end
        pci = pci(1:length(count));
    end
end