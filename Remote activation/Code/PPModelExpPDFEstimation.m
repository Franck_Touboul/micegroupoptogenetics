function model = PPModelExpPDFEstimation(x, model, state)
minweight = 0.1;

if nargin > 2
    curr = model.states(state);
else
    curr = model.all;
end
valid = (~curr.check | x(2, :) == curr.contact) & ~isnan(x(1, :));
input = x(1, valid);

prev = curr;

curr.pmu = mean(input(input > 0));
curr.nmu = abs(mean(input(input < 0)));
curr.weights = [sum(input > 0) sum(input < 0)];
curr.weights = curr.weights / sum(curr.weights);
if isnan(curr.pmu)
    curr.pmu = prev.pmu;
    curr.weights = [minweight, 1-minweight];
end
if isnan(curr.nmu)
    curr.nmu = prev.nmu;
    curr.weights = [1-minweight, minweight];
end

if nargin > 2
    model.states(state) = curr;
else
    model.all = curr;
end
