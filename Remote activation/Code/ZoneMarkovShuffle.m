function zones = ZoneMarkovShuffle(obj)
%%
zones = obj.zones(:, obj.valid);
for s=1:obj.nSubjects
    %%
    z = zones(s, :);
    chnage = [true z(1:end-1) ~= z(2:end)];
    beg = find(chnage);
    len = [diff(beg), length(z) - beg(end)];
    id = z(beg);
    probs = EstimateMarkov(obj, id);
    %%
    data = [];
    for i=1:obj.ROI.nZones
        data(i).len = len(id == i);
    end
    %%
    nz = zeros(1, length(z));
    offset = 1;
    done = false;
    while ~done
        r = hmmgenerate(2 * length(id), probs.cond, eye(obj.ROI.nZones));
        for i=1:length(r)
            clen = data(r(i)).len(randi(length(data(r(i)).len)));
            fr = offset;
            to = min(offset+clen-1, size(z, 2));
            nz(fr:to) = r(i);
            offset = to + 1;
            if to == size(z, 2)
                done = true;
                break;
            end
        end
    end
    zones(s, :) = nz;
end