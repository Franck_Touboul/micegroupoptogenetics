function [me, sampleProbMe, sampleProbJoint, confidence] = findPotts(level)
options.n = level;

options.nSubjects = 4;           % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts
options.nIters = 50;
options.confidenceIters = 0;

%% Loading the Excel file for all mice (if needed)
loadData;
options.count = zones.count;
%data = zones.all(:,1:floor(end/4));
%data = zones.all(:,floor(3/4*end):end);
%

%% train potts model
data = zones.all;
%data = testPottsModel(options, size(zones.all, 2));
me = trainPottsModel(options, data);

%% compute independent probabilities
independentProbs = computeIndependentProbs(data, zones.count);

%% compute joint probabilities
[jointProbs, jointPci] = computeJointProbs(data, zones.count);

%% compute probability for each sample
fprintf('# computing joint probabilities : ');
[sampleProbIndep, sampleProbJoint] = computeSampleProb(data, zones.count, independentProbs, jointProbs);
fprintf('mean log-likelihood = %6.4f\n', mean(log(sampleProbJoint)));
%% compute MaxEnt probabilities for each sample
fprintf('# computing max-entropy probabilities\n');
p = exp(me.features * me.weights');
perms_p = exp(me.perms * me.weights');
Z = sum(perms_p);
p = p / Z;
%% plot
subplot(2,2,1);
p1 = sampleProbJoint;
p2 = p;
sampleProbMe = p2;
% confidence
up1 = unique(p1);
counts = up1 * size(data, 2);
[phat,pci] = binofit(counts, size(data, 2));
myCofidencePlot(up1, pci(:, 1)', pci(:, 2)');
confidence.p = up1;
confidence.max = pci(:, 1)';
confidence.min = pci(:, 2)';

hold on;
%
h2 = plot(p1, p2, '.');
set(gca, 'YScale', 'log', 'XScale', 'log');
%
minLog = floor(log10(min(min(p1), min(p2))));
maxLog = ceil(log10(max(max(p1), max(p2))));
axis([10^minLog 10^maxLog 10^minLog 10^maxLog]);
line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);

axis([...s
    10^floor(log10(min(p1))) ...
    10^ceil(log10(max(p1))) ...
    10^floor(log10(min(p2))) ...
    10^ceil(log10(max(p2))) ...
    ]);

tit = ['Maximum-Entropy Model (me-prob=' num2str(mean(log(p))) ', joint-prob' num2str(mean(log(sampleProbJoint))), ')'];
title(tit);
xlabel('Joint');
ylabel('ME');


%prettyPlot(tit, 'Joint', 'ME');
%legend([h1, h2], 'Independent', 'ME', 'location', 'SouthEast'); legend boxoff

