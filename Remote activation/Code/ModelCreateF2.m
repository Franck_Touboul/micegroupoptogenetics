function Model = ModelCreateF2(Params, opt)

Defaults.AxisMundi.Yaw = 0;
Defaults.AxisMundi.Pitch = 0;
Defaults.AxisMundi.Roll = 0;
Defaults.AxisMundi.Scale = 1;
Defaults.Body.Yaw = 0;
Defaults.Body.FatScale = 1;
Defaults.Body.RearScale = 1;

Defaults.Neck.Yaw = 0;
Defaults.Neck.Pitch = 0;

Defaults.Tail.Yaw = [0 0 0 0];
Defaults.Tail.Pitch = pi/2;
Defaults.Tail.Roll = 0;
Defaults.Tail.Scale = 1;

if nargin == 0
    Params = struct();
end

%%
f = fields(Defaults);
for i=1:length(f)
    if ~isfield(Params, f{i})
        Params.(f{i}) = Defaults.(f{i});
    end
end

%%
if nargin == 1
    opt = struct();
end
if ~isfield(opt, 'Resolution')
    opt.Resolution = 20;
end
opt.NumVerteb = 4 + 7 + 13 + 6 + 4 + 30; % +...
    %3 + 3 + 3 + 3; % skull + 7 cervical + 12-14 thoracic + 5-6 lumbar + 4 sacral + 27-30 caudal
opt.Scale = 0.1;
opt.Draw3D = 1;
opt.HiddenAxis = 2;

%%
Model.Bones = struct();
Model.Types = [0 * ones(1,4), 1 * ones(1,7), 2 * ones(1,13), 3 * ones(1,6), 4 * ones(1,4), 5 * ones(1,7), 6 * ones(1,13), 7 * ones(1,10)];
Model.CenterOfMass = find(Model.Types == 3, 1, 'last');
Model.Axis = [find(Model.Types == 4, 1, 'last') find(Model.Types == 0, 1, 'first')];
Model.Dependence = Model.Types - 1;
Model.NumVerteb = length(Model.Types);
Model.Resolution = opt.Resolution;
Model.MaxMass = 26;

for i=1:opt.NumVerteb
    Bone = struct();
    Bone.Skin = [];
    f2b = i - find(Model.Types == Model.Types(i), 1);
    b2f = find(Model.Types == Model.Types(i), 1, 'last') - i;
    if Model.Types(i) > 0
        prevBoneID = find(Model.Types == Model.Types(i) - 1, 1, 'last');
        pBone = Model.Bones(prevBoneID);
        pSkin = pBone.Skin;
        pSkinPoints = pBone.SkinPoints;
    else
        pBone = [];
        pSkin = [];
        pSkinPoints = [];
    end
    switch Model.Types(i)
        case 0 %% skull
            switch f2b
                case 0
                    SkinPoints = [0.1, .04, .15];
                    SkinPoints(2) = SkinPoints(2) * Params.Body.FatScale;
                    Bone.Skin = MakeSkinF(opt.Resolution, SkinPoints);
                case 1
                    SkinPoints = [0.15, .16, .3];
                    SkinPoints(2) = SkinPoints(2) * Params.Body.FatScale;
                    Bone.Skin = MakeSkinF(opt.Resolution, SkinPoints);
                case 2
                    SkinPoints = [0.15, .24, .35];
                    SkinPoints(2) = SkinPoints(2) * Params.Body.FatScale;
                    Bone.Skin = MakeSkinF(opt.Resolution, SkinPoints);
                case 3
                    SkinPoints = [0.1, .24, .3];
                    SkinPoints(2) = SkinPoints(2) * Params.Body.FatScale;
                    Bone.Skin = MakeSkinF(opt.Resolution, SkinPoints);
            end
        case 1 %% cervical (neck)
            SkinPoints = [0.1 + 0.01 * f2b, .25 + 0.015 * f2b, 0.4 + 0.025 * f2b];
            SkinPoints(2) = SkinPoints(2) * Params.Body.FatScale;
            Bone.Skin = MakeSkinF(opt.Resolution, SkinPoints);
        case 2 %% thoracic (ribs)
            SkinPoints = [pSkinPoints(1) - 0.005 * f2b, .48 - 0.001 * (b2f - 1).^2, pSkinPoints(3) + 0.035 * f2b];
            SkinPoints(2) = SkinPoints(2) * Params.Body.FatScale;
            Bone.Skin = MakeSkinF(opt.Resolution, SkinPoints);
        case 3 %% lumbar (stomach)
            SkinPoints = [pSkinPoints(1), 0.52 - 0.015 * (f2b - 2).^2, pSkinPoints(3) - 0.05 * f2b];
            SkinPoints(2) = SkinPoints(2) * Params.Body.RearScale;
            Bone.Skin = MakeSkinF(opt.Resolution, SkinPoints);
        case 4 %% sacral (bottom)
            SkinPoints = [pSkinPoints(1), (0.52 - 0.005 * (f2b + 3).^2) , pSkinPoints(3) - .1 * (f2b + 1)];
            SkinPoints(2) = SkinPoints(2) * Params.Body.FatScale;
            Bone.Skin = MakeSkinF(opt.Resolution, SkinPoints);
        case 5 %% tail seg. no 1
            SkinPoints = .04-f2b*(.04 / 30);
            Bone.Skin = MakeSkinF(opt.Resolution, SkinPoints);
        case 6 %% tail seg. no 2
            SkinPoints = pSkinPoints(1)-f2b*(.04 / 30);
            Bone.Skin = MakeSkinF(opt.Resolution, SkinPoints);
        case 7 %% tail seg. no 3
            SkinPoints = pSkinPoints(1)-f2b*(.04 / 30);
            Bone.Skin = MakeSkinF(opt.Resolution, SkinPoints);
    end
    
    Bone.SkinPoints = SkinPoints;
    Bone.Roll = 0;
    Bone.Dependence = i - 1;
    Bone.Memory = true;
    switch Model.Types(i)
        case 0 % skull
            Bone.Length = .18;
            Bone.Yaw = 0;
            if f2b == 0
                Bone.Pitch = .9;
                %Bone.Pitch = .2;
            else
                Bone.Pitch = 0;
            end
        case 1 %% cervical (neck)
            Bone.Length = .09;
            Bone.Yaw = Params.Body.Yaw + Params.Neck.Yaw;
            if f2b == 0
                Bone.Pitch = -1.4 + Params.Neck.Pitch;
                Bone.Memory = false;
                %Bone.Pitch = -.8;
            else
                Bone.Pitch = 0.18;
            end
        case 2 %% thoracic (ribs)
            Bone.Length = .075;
            Bone.Yaw = Params.Body.Yaw;
            Bone.Pitch = -.04;
        case 3 %% lumbar (stomach)
            Bone.Length = .15;
            Bone.Yaw = Params.Body.Yaw;
            Bone.Pitch = -0.13;
        case 4 %% sacral (bottom)
            Bone.Length = .15;
            Bone.Yaw = Params.Body.Yaw;
            Bone.Pitch = -.2;
        case 5 %% tail seg. no 1
            Bone.Memory = false;
            Bone.Length = (.15-f2b*0.005) * Params.Tail.Scale;
            if f2b == 0
                Bone.Roll = Params.Tail.Roll;
                Bone.Yaw = Params.Tail.Yaw(1);
                Bone.Pitch = Params.Tail.Pitch;
            else
                Bone.Yaw = Params.Tail.Yaw(2);
%                Bone.Pitch = -0.36;
                Bone.Pitch = 0;
            end
        case 6 %% tail seg. no 2
            Bone.Length = (.15-(f2b + 7)*0.005) * Params.Tail.Scale;
            Bone.Yaw = Params.Tail.Yaw(3);
            Bone.Pitch = 0;
        case 7 %% tail seg. no 3
            Bone.Length = (.15-(f2b + 20)*0.005) * Params.Tail.Scale;
            Bone.Yaw = Params.Tail.Yaw(4);
            Bone.Pitch = 0;
    end
    if i==1
        Model.Bones = Bone;
    else
        Model.Bones(i) = Bone;
    end
end
Model.AxisMundi = Params.AxisMundi;
