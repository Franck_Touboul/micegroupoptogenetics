function [obj, mi, entropy] = SocialMutualInformation(obj)
pAll = ComputeJointProbsForDimension(obj.zones-1, obj.ROI.nZones, 1:obj.nSubjects);
%pAll = computeJointSampleProb(obj.zones-1, obj.ROI.nZones, jpAll);


mi = zeros(obj.nSubjects);
entropy = zeros(1, obj.nSubjects);
pONE = cell(1, obj.nSubjects);
for m1=1:obj.nSubjects
    pONE{m1} = ComputeJointProbsForDimension(obj.zones - 1, obj.ROI.nZones, m1);
    entropy(m1) = pAll * flog2(pONE{m1})';
end

for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1 == m2
            p1 = histc(obj.zones(m1, :), unique(obj.zones(~isnan(obj.zones))));
            p1 = p1 / sum(p1);
            entropy(m1) = -p1 * flog2(p1)';

            idx = 1:obj.nSubjects;
            idx = idx(idx ~= m1);

            pLOO = ComputeJointProbsForDimension(obj.zones - 1, obj.ROI.nZones, idx);
            mi(m1,m1) = pAll * (flog2(pAll) - flog2(pLOO) - flog2(pONE{m1}))';
            
        else
            if m1<m2
                curr = obj;
                curr.nSubjects = 2;
                pTWO = ComputeJointProbsForDimension(obj.zones - 1, obj.ROI.nZones, [m1, m2]);
                cmi = pAll * (flog2(pTWO) - flog2(pONE{m1}) - flog2(pONE{m2}))';
                mi(m1, m2) = cmi;
                mi(m2, m1) = cmi;
            end
        end
    end
end
