function obj = TrackPathNaive(obj, nruns)
%%
if ~exist('nruns', 'var')
    nruns = 100;
end
obj = TrackLoad(obj, {'ROI', 'FilePrefix', 'OutputPath', 'VideoScale', 'ColorMatchThresh', 'StartTime', 'EndTime'});
%%
fprintf('# - loading segmentations\n');
cents = struct();
cents.x       = [];
for i=1:nruns
    fprintf('#      . segment no. %d\n', i);
    filename = [obj.OutputPath obj.FilePrefix '.segm.' sprintf('%03d', i) '.mat'];
    waitforfile(filename);
    currSegm = load(filename);
    currSegm.cents = rmfield(currSegm.cents, 'area');
    currSegm.cents = rmfield(currSegm.cents, 'solidity');
    cents = structcat(cents, currSegm.cents, 1, true, false);
end
cents.x = single(cents.x);
cents.y = single(cents.y);
cents.logprob = single(cents.logprob);

