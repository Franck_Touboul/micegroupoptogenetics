function s = structcat(s1, s2, dim, can_resize, sparsify)
if nargin < 3
    dim = 1;
end
if nargin < 4
    can_resize = false;
end
if nargin < 5
    sparsify = false;
end

fields = fieldnames(s2);

s = s1;
for i=1:length(fields)
    if isfield(s1, fields{i})
        m1 = getfield(s1, fields{i});
        m2 = getfield(s2, fields{i});
        if sparsify && isfloat(m1) && length(size(m1)) == 2 && length(size(m2)) == 2
            m1 = sparse(m1);
            m2 = sparse(m2);
        end
        if can_resize
            m1 = matmatch(m1, m2, dim);
            m2 = matmatch(m2, m1, dim);
        end
        s = setfield(s, fields{i}, ...
            cat(dim, m1, m2));
    else
        s = setfield(s, fields{i}, getfield(s2, fields{i}));
    end
end
