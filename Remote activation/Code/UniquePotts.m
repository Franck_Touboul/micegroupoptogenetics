function obj = UniquePotts(obj, order)
potts = obj.Analysis.Potts.Model{order};
%%
m = ones(1, 2*order);
for i=2:order
    m(i) = m(i-1) * (obj.nSubjects+1);
end
for i=1:order
    m(order+i) = m(order+i-1) * (obj.ROI.nZones+1);
end
potts.id = potts.vec * m';
%%
obj.Analysis.Potts.Model{order} = potts;