function [features, labels, observed] = AssignObservedFeatures(data, range, nPerms, use_sparse)
if nargin < 4
    use_sparse = false;
end

dim = 0;
full = 0;
validValues = cell(1, length(nPerms));
for i=1:length(nPerms)
    valid = ~any(isnan(data'));
    validValues{i} = unique((data(valid, nPerms{i}) - 1) * range.^[length(nPerms{i}) - 1:-1:0]');
    dim = dim + length(validValues{i});

    nVals = range^length(nPerms{i});
    full = full + nVals;
end
observed = false(1, full);

if use_sparse
    features = sparse(size(data, 1), double(dim));
else
    features = zeros(size(data, 1), double(dim));
end

index = 1;
labels = cell(1, dim);
offset = 1;
for i=1:length(nPerms)
    nVals = range^length(nPerms{i});
    observed(offset + validValues{i}(:)') = true;
    offset = offset + nVals;
    for j=validValues{i}(:)'
        values = decimal2base(j, range, length(nPerms{i})) + 1;
        map = true(size(data, 1), 1);
        for k=1:length(nPerms{i})
            map = map & data(:, nPerms{i}(k)) == values(k);
        end
        features(:, index) = map;
        
        labels{index} = [nPerms{i}; values];
        index = index + 1;
    end
end


