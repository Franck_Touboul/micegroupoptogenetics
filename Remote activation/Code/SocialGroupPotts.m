function SocialGroupPotts(objs)
%%
[objs, data, first] = SocialParseName(objs);
tests = fieldnames(objs);
m = 2;
n = data.nConditions;
%%
IkAll = cell(data.nConditions, data.nDays);
for i=1:length(tests)
    obj = getfield(objs, tests{i});
    try
        IkAll{IndexOf(data.Conditions, obj.Condition), obj.Day} = [IkAll{IndexOf(data.Conditions, obj.Condition), obj.Day}; obj.Analysis.Potts.Ik];
    end
    %Ik(IndexOf(data.Conditions, obj.Condition), obj.Day, :) = reshape(obj.Analysis.Potts.Ik, 1, 1, obj.nSubjects-1);
end

Ik = zeros(data.nConditions, data.nDays, obj.nSubjects-1);
IkSTDErr = zeros(data.nConditions, data.nDays, obj.nSubjects-1);

In = zeros(data.nConditions, data.nDays);
InSTDErr = zeros(data.nConditions, data.nDays);

InfoFraction = zeros(data.nConditions, data.nDays, obj.nSubjects-1);
InfoFractionSTDErr = zeros(data.nConditions, data.nDays, obj.nSubjects-1);

for i=1:data.nConditions
    for d=1:data.nDays
        Ik(i, d, :)       = reshape(mean(IkAll{i, d}, 1), [1, 1, first.nSubjects-1]);
        IkSTDErr(i, d, :) = reshape(stderr(IkAll{i, d}, 1), [1, 1, first.nSubjects-1]);

        In(i, d)       = mean(sum(IkAll{i, d}, 2));
        IkSTDErr(i, d, :) = stderr(sum(IkAll{i, d}, 2));

        InfoFraction(i, d, :) = reshape(mean(100 * cumsum(IkAll{i, d}, 2) ./ repmat(sum(IkAll{i, d}, 2), 1, obj.nSubjects - 1), 1), [1, 1, first.nSubjects-1]);
        InfoFractionSTDErr(i, d, :) = reshape(stderr(100 * cumsum(IkAll{i, d}, 2) ./ repmat(sum(IkAll{i, d}, 2), 1, obj.nSubjects - 1), 1), [1, 1, first.nSubjects-1]);
    end
end

%InfoFraction = cumsum(Ik, 3) ./ repmat(sum(Ik, 3), [1, 1, first.nSubjects-1]);
%%
clf;
map = {'rx:', 'g^:', 'bs:', 'k.:'};
sampleMap = {'r.', 'g.', 'b.', 'k.'};
style = {'LineWidth', 2, 'MarkerSize', 10};
for i=1:data.nConditions
    for j=1:first.nSubjects-1
        %
        subplot(m, n, i);
        rowTitle(m, n, 1, 'I_{(k)}');
        errorbar(data.Days, Ik(i, :, j), IkSTDErr(i, :, j), map{j}, style{:});
        title(data.Conditions{i});
        set(gca, 'XTick', 1:data.nDays);
        hold on;
        for d=data.Days
            currIkAll = IkAll{i, d};
            plot(d, currIkAll(:, j), sampleMap{j}, style{:},'HandleVisibility','off');
        end
        if j==first.nSubjects-1
            errorbar(data.Days, In(i, :), InSTDErr(i, :), map{first.nSubjects}, style{:});
        end
        %
        if j<first.nSubjects-1
            subplot(m, n, n + i);
            rowTitle(m, n, 2, 'info fraction [%]');
            %plot(data.Days, InfoFraction(i, :, j) * 100, map{j}, style{:});
            errorbar(data.Days, InfoFraction(i, :, j), InfoFractionSTDErr(i, :, j), map{j}, style{:});
            xlabel('Day');
            set(gca, 'XTick', 1:data.nDays); 
            hold on;
        end
    end
    hold off;
end

legendEntries = {};
for j=1:first.nSubjects-1
    legendEntries{j} = num2str(j+1);
end
legendEntries{first.nSubjects} = 'total';
legend(legendEntries);
legend boxoff;
TieAxis(m, n, 1:data.nConditions);
TieAxis(m, n, n + [1:data.nConditions]);
