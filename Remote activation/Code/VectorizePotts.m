function obj = VectorizePotts(obj, order)
potts = obj.Analysis.Potts.Model{order};
%%
potts.vec = zeros(length(potts.labels), 2 * max(potts.order));
for i=1:length(potts.labels)
    potts.vec(i, 1:potts.order(i)) = potts.labels{i}(1, :);
    potts.vec(i, order+1:order+potts.order(i)) = potts.labels{i}(2, :);
end
obj.Analysis.Potts.Model{order} = potts;
obj = UniquePotts(obj, order);
