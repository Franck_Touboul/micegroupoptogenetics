function m = matmatch(s1, s2, dim)
sz1 = size(s1);
sz2 = size(s2);
sz = sz1 * 0;
str = 'm(';
for i=1:length(sz)
    if i~=dim
        sz(i) = max(sz1(i), sz2(i));
    else
        sz(i) = sz1(i);
    end
    if i > 1
        str = [str, ','];
    end
    str = [str, sprintf('1:%d', size(s1, i))]; 
end
if any(sz > sz1)
    m = zeros(sz);
    str = [str, ') = s1;'];
    eval(str);
else
    m = s1;
end
%m = zeros(sz);
