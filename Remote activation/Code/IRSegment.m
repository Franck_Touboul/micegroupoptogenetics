function IRSegment(map, prototype, n)
nVals = n * 10;
nAngles = 18;
minDist = 20;
%%
[mini, h1, h2, v1, v2] = trim(map);
valid = ~isnan(mini);
mini(isnan(mini)) = min(mini(:));
vals = zeros(1, nAngles * 10);
idxs = zeros(1, nAngles * 10);
angles = zeros(1, nAngles * 10);
offset = 1;
for a=1:nAngles
    angle = 360/nAngles*(a-1);
    if angle~=0
        curr = imrotate(prototype, angle);
    else
        curr = prototype;
    end
    sz = size(curr);
    c = normxcorr2(curr, mini);
    c = c(ceil(sz(1)/2):end-ceil(sz(1)/2)+1, ceil(sz(2)/2):end-ceil(sz(2)/2)+1);
    regmax = imregionalmax(c);
    regmax(~valid) = false;
    idx = find(regmax);
    val = c(idx);
    vals(offset:offset + length(val) - 1) = val;
    idxs(offset:offset + length(val) - 1) = idx;
    angles(offset:offset + length(val) - 1) = angle;
    offset = offset + length(val);
end
vals = vals(1:offset-1);
idxs = idxs(1:offset-1);
angles = angles(1:nVals);
[vals, o] = sort(vals, 'descend');
idxs = idxs(o);
idxs = idxs(1:nVals);
vals = vals(1:nVals);
angles = angles(1:nVals);
%%
[xi,xj] = ind2sub(size(mini), idxs);
valid = true(1, length(vals));
for i=1:length(vals)
    if ~valid(i)
        continue;
    end
    valid(i+1:end) = valid(i+1:end) & (xi(i) - xi(i+1:end)).^2 + (xj(i) - xj(i+1:end)).^2 > minDist;
end
vals = vals(valid);
idxs = idxs(valid);
angles = angles(valid);
%%
subplot(1,2,1);
imagesc(mini);
subplot(1,2,2);
maxmap = zeros(size(mini));
maxmap(idxs) = vals;
imagesc(maxmap);
%%
p = nchoosek(1:length(vals), n);
for i=1:size(p, 1)
    
end