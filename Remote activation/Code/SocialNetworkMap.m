function [obj, network] = SocialNetworkMap(obj, model)
%obj = TrackEnsure(obj, {'BkgImage', 'Analysis'});
%%
ZoneMap  = [1 7 4 2 3 10 8 5 9 6];
options.MaxWeight = 3;
options.Radios = 10;
%%
ZoneCenters = zeros(obj.nSubjects, obj.ROI.nZones, 2);
MouseIndicators = zeros(obj.nSubjects, 2);
index = 1;
offset = 60 / pi;
for i=1:obj.nSubjects
    a = ((index + floor(obj.ROI.nZones / 2))  / (obj.ROI.nZones + 2) / obj.nSubjects * 2 * pi) + offset;
    MouseIndicators(i, 1) = (options.Radios + options.Radios/6) * sin(a);
    MouseIndicators(i, 2) = (options.Radios + options.Radios/6) * cos(a);
    for j=ZoneMap
        a = (index / (obj.ROI.nZones + 2) / obj.nSubjects * 2 * pi) + offset;
        ZoneCenters(i, j, 1) = options.Radios * sin(a);
        ZoneCenters(i, j, 2) = options.Radios * cos(a);
        index = index + 1;
    end
    index = index + 2;
end
%%
markerSize = 8;

clf
hold on;

percentage = .99;
weights = model.weights;
weights(model.order ~= 2) = nan;
[s, o] = sort(weights);
o = o(1:sum(~isnan(s)));
s = s(1:sum(~isnan(s)));

lthresh = s(round(length(s) * percentage));
lmax = s(1);

uthresh = s(round(length(s) * (1-percentage)));
umax = s(end);

lidx = o(s <= lthresh); lidx = lidx(end:-1:1);
uidx = o(s >= uthresh); %uidx = uidx(end:-1:1);

for i=1:length(weights)
    model.order(i) = size(model.labels{i}, 2);
end

valid = [];
marked = [];
[qqq, idx_] = sort(abs(model.weights));
idx_ = idx_(model.order(idx_) == 2);

lcmap = MyDefaultColormap;
for idx = idx_
    %%
    r = [model.labels{idx}(2, 1), model.labels{idx}(2, 2)];
    if r(1) == r(2)
        if model.weights(idx) >= uthresh
            fprintf('# pos-marked: location %d (between %d, %d)\n', r(1), model.labels{idx}(1,1), model.labels{idx}(1,2));
        else
            fprintf('# neg-marked: location %d (between %d, %d)\n', r(1), model.labels{idx}(1,1), model.labels{idx}(1,2));
        end
        marked = unique([marked, r(1)]);
    end
    valid = unique([valid, r]);
    for s=1:2
        frx = ZoneCenters(model.labels{idx}(1, s), r(s), 1);
        fry = ZoneCenters(model.labels{idx}(1, s), r(s), 2);
        tox = ZoneCenters(model.labels{idx}(1, 3-s), r(3-s), 1);
        toy = ZoneCenters(model.labels{idx}(1, 3-s), r(3-s), 2);
        dfrx = sin(atan2(frx,fry)) * 3;
        dfry = cos(atan2(frx,fry)) * 3;
        dtox = sin(atan2(tox,toy)) * 3;
        dtoy = cos(atan2(tox,toy)) * 3;
        [Q] = bezierInterp([frx fry], [frx fry] - [dfrx dfry], [tox toy] - [dtox dtoy], [tox toy]);
        lQ = ceil(size(Q, 1) / 2);
        
        wcolor = lcmap(128 + sign(model.weights(idx)) * min(round((abs(model.weights(idx)) / options.MaxWeight) * 127), 127), :);
        if model.weights(idx) >= 0
            w = abs(model.weights(idx)) * 1;
            if w > 0
%            plot(Q(1:lQ, 1), Q(1:lQ, 2), '-', 'Color', cmap(model.labels{idx}(1, s), :), 'LineWidth', w);
            plot(Q(1:lQ, 1), Q(1:lQ, 2), '-', 'Color', wcolor, 'LineWidth', w);
            end
        else
            w = abs(model.weights(idx)) * 1;
            if w > 0
%            plot(Q(1:lQ, 1), Q(1:lQ, 2), '--', 'Color', cmap(model.labels{idx}(1, s), :), 'LineWidth', w);
            plot(Q(1:lQ, 1), Q(1:lQ, 2), '-', 'Color', wcolor, 'LineWidth', w);
            end
        end
    end
end

zmap = MyZonesColormap;
mmap = MyMouseColormap;
weightsIndep = model.weights(model.order == 1);
labelsIndep = model.labels(model.order == 1);
labelsIndep = [labelsIndep{:}];
for i=1:obj.nSubjects
    for j=1:obj.ROI.nZones
        if j==model.neutralZone
            continue;
        end
        w = weightsIndep(labelsIndep(1, :) == i & labelsIndep(2, :) == j);
        currMarkerSize = abs(markerSize * w);
        currMarkerSign = sign(markerSize * w);
        currMarkerSign = -1;
        currMarkerSize = 14;
        %if ~issheltered(j)
        plot(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2), 'o', 'MarkerEdgeColor', zmap(j, :), 'MarkerFaceColor', zmap(j, :), 'MarkerSize', currMarkerSize, 'LineWidth', 2);
        if currMarkerSize < 10
            %text(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2),num2str(find(ZoneMap == j)), 'color', 'w', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center')
        else
            %text(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2),num2str(find(ZoneMap == j)), 'color', 'w', 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center')
        end
        %else
        %    plot(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2), 's', 'MarkerEdgeColor', cmap(i, :), 'MarkerFaceColor', zmap(j, :), 'MarkerSize', currMarkerSize);
        %end
    end
    plot(MouseIndicators(i, 1), MouseIndicators(i, 2), 'o', 'MarkerEdgeColor', mmap(i, :), 'MarkerFaceColor', 'w', 'MarkerSize', currMarkerSize, 'LineWidth', 2);
end
axis off;
hoff;
set(gcf, 'Color', 'w');
axis equal

%%
network.w = [];
for i=1:length(model.weights)
    if size(model.labels{i}, 2) == 2
        z1 = model.labels{i}(1, 1);
        z2 = model.labels{i}(1, 2);
        l1 = model.labels{i}(2, 1);
        l2 = model.labels{i}(2, 2);
        a = (z1 - 1) * obj.ROI.nZones + l1;
        b = (z2 - 1) * obj.ROI.nZones + l2;
        network.w(a , b) = model.weights(i);
        network.w(b , a) = model.weights(i);
    end
end
%%
hon;
lcmap = MyDefaultColormap;
for v = -options.MaxWeight:1:options.MaxWeight;
    wcolor = lcmap(128 + sign(v) * min(round((abs(v) / options.MaxWeight) * 127), 127), :);
    dx = options.Radios/3;
    w = options.Radios/15;
    h = .75;
    dy = options.Radios/20;
    y = options.Radios - options.MaxWeight * h + v * h - dy;
    if v~=0
        plot([options.Radios, options.Radios + w] + dx, [y, y], 'LineWidth', abs(v), 'Color', wcolor);
    end
    text(options.Radios + dx, y, sprintf('%3d  ', v), 'HorizontalAlignment', 'right');
end
hoff;
%%
if (1==2)
    %%
    p = 'Graphs/SocialInteractionMap/';
    mkdir(p);
    figure(1);
    saveFigure([p '' obj.FilePrefix '']);
end