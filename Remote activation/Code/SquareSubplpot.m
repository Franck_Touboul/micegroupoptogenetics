function [pr, pc, h, w] = SquareSubplpot(count, i)
w = ceil(sqrt(count));
h = ceil(count / w);
subplot(h, w, i);
pr = floor((i - 1) / w) / (h-1);
pc = mod(i-1, w) / (w-1);
