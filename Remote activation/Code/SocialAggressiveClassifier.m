%function SocialAggressiveClassifier(obj)
options.MinLeaf = 5;
options.DAMethod = 'linear';
%%
Classifier = obj.Contacts.Behaviors.AggressiveChase.Classifier;
N = 339;
TrainMap = obj.Contacts.Behaviors.ChaseEscape.Map(1:size(Classifier.Vec, 2)) & Classifier.MarkID <= N;
TestMap  = obj.Contacts.Behaviors.ChaseEscape.Map(1:size(Classifier.Vec, 2)) & Classifier.MarkID > N;

data = Classifier.Vec';
label = Classifier.Label';
train = data(TrainMap, :);
test  = data(TestMap, :);
label = label(TrainMap, :);
label = (label > 1);
%%
for i=1:size(train, 2)
    train(~isfinite(train(:, i)), i) = min(isfinite(train(:, i)));
end

tree = classregtree(train, label, 'minleaf', options.MinLeaf);


%featidx = [1 4 5 6 7 9 10 12 14 15 16 17  20 22 23 24];
%classes = classify(train(:, featidx), train(:, featidx), label, 'mahalanobis');

dataTrainG1 = train(label==0,:);
dataTrainG2 = train(label==1,:);
[h,p,ci,stat] = ttest2(dataTrainG1,dataTrainG2,[],[],'unequal');
[p, featidx] = sort(p);
featlocal = featidx(1:10);
classes = classify(train(:, featlocal), train(:, featlocal), label, options.DAMethod);

testclasses = classify(test(:, featlocal), train(:, featlocal), label, options.DAMethod);

svm = svmtrain(double(train(:, featlocal)), label*2-1, 'Method', 'SMo', 'Kernel_Function', 'mlp');
%testclasses = (svmclassify(svm, test(:, featlocal)) + 1) / 2;

testclasses = testclasses | knnclassify(test(:, featlocal), train(:, featlocal), label, 5, 'euclidean');

% classf = @(xtrain,ytrain,xtest,ytest) ...
%     sum(~strcmp(ytest,classify(xtest,xtrain,ytrain,'quadratic')));
% tenfoldCVP = cvpartition(label,'kfold',10);
% fsLocal = sequentialfs(classf,train(:, featlocal),label,'cv',tenfoldCVP);


%% Test on Train
marks = SocialBehaviorLoad(obj, (1:size(data, 1)) <= N);
%data = data((1:size(data, 1)) > N, :);

amarks = marks;
amarks.ignore = marks.agresivness == 1;
amarks.chase = marks.agresivness > 1;

s = eval(tree, train);
olbl = cellfun(@str2num,s);
obj.Contacts.Behaviors.AggressiveChase2.Map = false(1, length(obj.Contacts.Behaviors.AggressiveChase.Map));
obj.Contacts.Behaviors.AggressiveChase2.Map(TrainMap) = classes' | olbl';

[results, match] = SocialBehaviorAnalysisScore(obj, amarks, 'AggressiveChase2');
fprintf('#----------------\n');
fprintf('# Test on Train: detection=%.1f%% (%d), false-alarm=%.1f%% (%d)\n', results.chase(1) / (results.chase(1) + results.chase(3)) * 100, results.chase(1), results.chase(2) / results.chase(5) * 100, results.chase(2));

%%
marks = SocialBehaviorLoad(obj, (1:500) > N);

amarks = marks;
amarks.ignore = marks.agresivness == 1;
amarks.chase = marks.agresivness > 1;

s = eval(tree, test);
olbl = cellfun(@str2num,s);

obj.Contacts.Behaviors.AggressiveChase2.Map = false(1, length(obj.Contacts.Behaviors.AggressiveChase.Map));
obj.Contacts.Behaviors.AggressiveChase2.Map(TestMap) = testclasses' | olbl';

[results, match] = SocialBehaviorAnalysisScore(obj, amarks, 'AggressiveChase2');
fprintf('# Test on Train: detection=%.1f%% (%d), false-alarm=%.1f%% (%d)\n', results.chase(1) / (results.chase(1) + results.chase(3)) * 100, results.chase(1), results.chase(2) / results.chase(5) * 100, results.chase(2));
