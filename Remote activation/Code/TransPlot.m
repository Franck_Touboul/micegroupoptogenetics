function TransPlot(x, y)

z = zeros(size(x));
S = surface([x;x],[y;y],[z;z],...
            'facecol','no',...
            'edgecol','interp',...
            'linew',2,...
            'edgealpha',.2,...
            'edgecolor','b');