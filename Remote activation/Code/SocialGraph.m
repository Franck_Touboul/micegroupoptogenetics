function [obj, bg] = SocialGraph(obj, mat, thresh, rank, nodesize, labels)
if ~exist('nodesize', 'var')
    nodesize = [];
end

if ~exist('labels', 'var')
    labels = {};
end

if ~exist('thresh', 'var')
    thresh = [];
end
if ~exist('rank', 'var')
    rank = [];
end
maxval = max(mat(:));
minval = min(mat(:));

horizSort = false;

names =  {'1', '2', '3', '4'};
if ~isempty(rank)
   if max(rank) < obj.nSubjects
       d = obj.nSubjects - max(rank);
       rank = rank + d;
       mat = padarray(mat, d, 0, 'post');
       mat = padarray(mat', d, 0, 'post')';
       for i=1:d
           names = {names{:}, num2str(obj.nSubjects + i)};
       end
       rank(end+1:end+d) = 1:d;
   end
end

bg = biograph(mat, names, 'LayoutType', 'Radial', 'ShowWeights', 'on', 'ArrowSize', 4);

%set(bg, 'EdgeType', 'Straight', 'EdgeTextColor', [.5 .5 .5], 'EdgeFontSize', 16)
set(bg, 'EdgeTextColor', [.5 .5 .5], 'EdgeFontSize', 28)

%bg = biograph(mat, {'1', '2', '3', '4'}, 'LayoutType', 'Radial');
for i=1:length(bg.edges)
    edge = bg.edges(i);
    if edge.weight ~= 0
        set(edge, 'LineWidth', max((edge.weight - minval) / (maxval - minval) * 15, 1))
        set(edge, 'Label', num2str(edge.weight));
%         [S, E] = regexp(edge.ID, '[0-9]*');
%         m1 = str2num(edge.ID(S(1):E(1)));
%         m2 = str2num(edge.ID(S(2):E(2)));
%         set(edge, 'LineColor', obj.Colors.Centers(m1, :));
        if ~isempty(thresh)
            if isscalar(thresh)
            if edge.weight > thresh
                set(edge, 'LineColor', [1 0 0]);
            else
                set(edge, 'LineColor', [0 0 1]);
            end
            else
                [S, E] = regexp(edge.ID, '[0-9]*');
                m1 = str2num(edge.ID(S(1):E(1)));
                m2 = str2num(edge.ID(S(2):E(2)));
                if thresh(m1, m2)
                    set(edge, 'LineColor', [1 0 0]);
                end
            end
        end
    end
end
cmap = MyMouseColormap;
idstr = '';
for i=1:length(bg.nodes)
    node = bg.nodes(i);
    node.Shape = 'circle';
    id = str2double(node.ID);
    if id <= obj.nSubjects
        node.TextColor = [1 1 1];
        node.FontSize = 18;
        node.Color = cmap(id, :);
        node.LineColor = cmap(id, :);
    if ~isempty(labels)
        node.ID = labels{i};
    else
        node.TextColor = cmap(id, :);
    end
    else
        node.Color = [1 1 1];
        node.LineColor = [1 1 1];
        node.TextColor = [1 1 1];
    end
    if ~isempty(nodesize)
        node.Size = [nodesize(i) nodesize(i)];
    end
end

%%
dolayout(bg);
dolayout(bg, 'Paths', true)
width = 20;
if ~isempty(rank)
    if horizSort
        index = 1;
        for r=unique(rank(:))'
            currnodes = bg.nodes(rank == r);
            currnodeIds = find(rank == r);
            s = 2 * mod(index, 2) - 1;
            offset = s * width;
            if index == 1 || index == length(unique(rank(:)))
                offset = 0;
            end
            for i=1:length(currnodes)
                currnodes(i).Position = [currnodeIds(i) * width, (index - 1) * 25 + 10];
            end
            index = index + 1;
        end
    else
        index = 1;
        for r=unique(rank(:))'
            currnodes = bg.nodes(rank == r);
            s = 2 * mod(index, 2) - 1;
            offset = s * width;
            if index == 1 || index == length(unique(rank(:)))
                offset = 0;
            end
            for i=1:length(currnodes)
                currnodes(i).Position = [offset - s * (length(currnodes) - i) * width, (index - 1) * 20 + 10];
            end
            index = index + 1;
        end
    end
end

dolayout(bg, 'Paths', true);
sz=nodesize; sz = (sz - min(sz)) / (max(sz) - min(sz)) * 10 + 10;
if ~isempty(nodesize)
    for i=1:length(bg.nodes)
        bg.nodes(i).Size = [sz(i) sz(i)];
%        bg.nodes(i).ID = [bg.nodes(i).ID '(' num2str(nodesize(i)) ')'];
    end
end

% for m1=1:obj.nSubjects
%     for m2=1:obj.nSubjects
%         if m1 == m2; continue; end;
%         edge = getedgesbynodeid(bg, num2str(m1), num2str(m2));
%         set(edge, 'LineWidth', (mat(m1, m2) - minval) / (maxval - minval) * 10)
%     end
% end
%view(bg)
%g = biograph.bggui(bg);
%copyobj(g.biograph.hgAxes,gcf);
%close(g.hgFigure);
