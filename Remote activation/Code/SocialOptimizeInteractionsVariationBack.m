function SocialOptimizeInteractions(obj)
%if 1==2
    %%
    data = dlmread('aux/SC.exp0001.day02.cam01.marks', '\t', [1 0 500 14]);
    marks.ids = data(:, 1);
    marks.beg = data(:, 2);
    marks.end = data(:, 3);
    marks.subjects = data(:, 4:5);
    marks.approach = data(:, 6:7);
    marks.leave = data(:, 8:9);
    marks.chase = data(:, 10);
    marks.pred = data(:, 11:12);
    marks.prey = data(:, 13:14);
    marks.artifact = data(:, 15);
    %%
%end
%%
rand('twister',sum(100*clock));
randn('seed',sum(100*clock));
%%
range.PredObjectLength = 50:10:150;
range.PreyObjectLength = 50:10:150;
range.MinSubInteractionDuration = 1:1:12;
range.MinIdleDuration = 0:8:16;
range.ZoneOfContact = 90:10:130;
range.EmissionMatch = .5;
range.EmissionMismatch = .1:0.05:.45;
range.RelativeSpeedInChase = 0:0.1:0.8;
range.ContactMismatch = 0:.1:.2;
range.ChaseMinOverlap = 0:2:10;
range.ChaseMinOverlapPercentage = 0:.05:.2;

defaults.PredObjectLength = 100;
defaults.PreyObjectLength = 50;
defaults.MinSubInteractionDuration = 5;
defaults.MinIdleDuration = 8;
defaults.ZoneOfContact = 90;
defaults.EmissionMatch = .5;
defaults.EmissionMismatch = .2;
defaults.RelativeSpeedInChase = .5;
defaults.ContactMismatch = 0;
defaults.ChaseMinOverlap = 6;
defaults.ChaseMinOverlapPercentage = 0;

f = {'PredObjectLength', 'PreyObjectLength', 'MinSubInteractionDuration', 'ZoneOfContact', 'EmissionMismatch', 'RelativeSpeedInChase', 'ChaseMinOverlap', 'ChaseMinOverlapPercentage'};
r = randi(length(f));
defaults.(f{r}) = range.(f{r});

%defaults.EmissionMismatch = defaults.EmissionMatch * r / (r+1);

arg = struct();
f = fieldnames(defaults);
for i=1:length(f)
    arg.(f{i}) = defaults.(f{i})(randi(length(defaults.(f{i}))));
end
% arg.EmissionMismatch = defaults.EmissionMismatch(randi(sum(defaults.EmissionMismatch < arg.EmissionMatch)));

f = fieldnames(arg);
for i=1:length(f)
    fprintf('@arg %s=%f\n', f{i}, arg.(f{i}));
end
%%

obj = TrackLoad(obj);
obj.OutputToFile = false;
obj = SocialHierarchy(obj, true, arg);
%%
results = SocialBehaviorAnalysisScore(obj, marks)

f = fieldnames(results);
for i=1:length(f)
    fprintf('@res %s=', f{i});
	fprintf(' %f', results.(f{i}));
	fprintf('\n');
end

