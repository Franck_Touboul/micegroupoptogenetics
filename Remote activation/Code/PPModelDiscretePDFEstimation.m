function model = PPModelDiscretePDFEstimation(x, model, state)

if nargin > 2
    valid = x(2, :) == model.states(state).contact;
else
    valid = true(1, size(x, 2));
end
input = x(1, :);

h = DiscreteProbabilityEstimate(...
    input(valid), ...
    model.Histogram.nBins, ...
    model.Histogram.minval, ...
    model.Histogram.maxval, true);
h = h / model.Histogram.dbin;

if model.UseSpeedHistogram
    sh = DiscreteProbabilityEstimate(...
        x(3, valid), ...
        model.Speed.Histogram.nBins, ...
        model.Speed.Histogram.minval, ...
        model.Speed.Histogram.maxval, true);
    sh = sh / model.Speed.Histogram.dbin;
end

if nargin > 2
    model.states(state).histogram = h;
    %
    if model.UseSpeedHistogram
        model.states(state).Speed.histogram = sh;
    end
    %%
    SquareSubplpot(model.nstates, state);
    bins = sequence(model.Histogram.minval, model.Histogram.maxval, model.Histogram.nBins + 1);
    centers = (bins(1:end-1) + bins(2:end)) / 2;
    cplot(centers, h, 'k:');
else
    model.all.histogram = h;
    %
    if model.UseSpeedHistogram
        model.all.Speed.histogram = sh;
    end
end

%%
%%
