function PlotPersonas(data, showTotal, cmap, titles, groups)
if ~ishold
    cla
end
if ~exist('showTotal', 'var')
    showTotal = true;
end

if ~exist('titles', 'var') || isempty(titles)
    titles = {};
end

if ~exist('groups', 'var') || isempty(groups)
    groups = {};
end

if ~exist('cmap', 'var') || isempty(cmap)
    cmap = [

        98 111 179;
        240 145 55;
        240 191 148;
        220 73 89;
        53 178 87;
        179 213 157;
        234 165 193;
        0 177 229;
        148 216 239;
        189 190 200;
        
        
        246 237 170;
        248 231 83;
        ]/256;
%    cmap = cmap(10:-1:1, :);
end
if ~iscell(data)
    data = { data };
end

height = 1;
gap = 0.25;
offset = 0;
idx = 1;
group = 1;
groupSize = 0;
for d = 1:length(data)
    cdata = cumsum(data{d}, 2);
    for i=1:size(cdata, 1)
        prev = 0;
        if all(cdata(i, :) == inf)
            if ~isempty(groups) && length(groups) >= group 
                text(0, -(offset + idx - .5) + groupSize/2, {groups{group} ''}, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center', 'Rotation', 90)
            end
            offset = offset + gap;
            group = group + 1;
            groupSize = 0;
            continue;
        else
            groupSize = groupSize + 1;
        end
        for j=1:size(cdata, 2)
            X = [prev cdata(i, j) cdata(i, j) prev prev];
            Y = offset + [idx - height / 2 idx - height / 2 idx + height / 2 idx + height / 2 idx - height / 2];
            fill(X, -Y, cmap(j, :), 'EdgeColor', 'none');
            prev = cdata(i, j);
            hold on;
        end
        if showTotal
            text(prev, -(offset + idx), [' ' num2str(prev)]);
        end
        idx = idx + 1;
    end
    if ~isempty(groups) && length(groups) >= group
        text(0, -(offset + idx - .5) + groupSize/2, {groups{group} ''}, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center', 'Rotation', 90)
    end
    group = group + 1;
    groupSize = 0;
end
hold off;
axis off;