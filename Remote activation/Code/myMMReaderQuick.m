function obj = myMMReaderQuick(filename, frame)

persistent videoFilename;
persistent videoObj;

%% using mmread:
if ~strcmp(videoFilename, filename)
    videoFilename = filename;
    obj = mmread(videoFilename, 1);
    videoObj.Height = obj.height;
    videoObj.Width = obj.width;
    videoObj.NumberOfFrames = abs(obj.nrFramesTotal);
    videoObj.FrameRate = round(obj.rate);
end

if nargin > 1
    videoChunk = mmread(videoFilename, frame);
    obj = videoChunk.frames(1).cdata;
else
    obj = videoObj;
end

end
%xyloObj = mmreader(options.MovieFile);
