function VecImage(x, range, cmap)
options = [];
options.edge = false;
options.drawNan = true;
options.drawBkg = false;

idx = round((x - range(1)) / (range(2) - range(1)) * 255 + 1);
idx(idx < 1) = 1;
idx(idx > 256) = 256;
if options.edge
    edgeColor = 'k';
else
    edgeColor = 'none';
end
if options.drawBkg
    patch([1 size(x, 2)+1 size(x, 2)+1 1], [1 1 size(x, 1)+1 size(x, 1)+1], [0 0 0], 'EdgeColor', 'none', 'FaceColor', [.9 .9 .9]);
end
for i=1:size(x, 2)
    for j=1:size(x, 1)
        if isnan(x(j, i))
            if options.drawNan
                patch([i i+1 i+1 i]-.5, [j j j+1 j+1]-.5, [.95 .95 .95], 'EdgeColor', [1 1 1]);
            end
        else
            patch([i i+1 i+1 i]-.5, [j j j+1 j+1]-.5, cmap(idx(j, i), :), 'EdgeColor', edgeColor);
        end
    end
end
axis([.5 size(x,1)+1 .5 size(x,2)+1]);
set(gca, 'YDir', 'reverse');
axis off;