function TrackMovie(nruns, id)
%
TrackDefaults;
options.movieChunkDuration = 25 * 5;
%%
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nframes = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;
dt = 1/xyloObj.FrameRate;

%%
step = floor(nframes / nruns);
curr = 0;
for i=1:id
    prev = curr + 1;
    if i == nruns
        curr = nframes;
    else
        curr = prev + step - 1;
    end
end
startframe = prev;
endframe = curr;
nframes = endframe - startframe + 1;

%%
filename = [options.output_path options.test.name '.social.mat'];
load(filename);

%%
%options.outputMovieFile = '';

if exist('aviobj') && strcmp(aviobj.CurrentState, 'Open')
    aviobj = close(aviobj);
end
options.outputMovieFile = sprintf([options.output_path options.test.name '.social.%03d.avi'], id);
aviobj = avifile(options.outputMovieFile, 'fps', xyloObj.FrameRate);

%% map ethovision coordinates 
options.x1 = -40;
options.x2 =  40;
options.y1 = -33;
options.y2 =  33;
options.w = options.x2 - options.x1;
options.h = options.y2 - options.y1;

%%
fprintf('# comparing tracks\n');
nchars = 0;
nchars = RePrintf('# - frame %6d [%d-%d] (%6.2fxiRT)', startframe, startframe, endframe, 0); 
cmap = lines;
cmap  = [0 1 1; 1 1 1; 1 0 1; 1 0 0];
cmap2 = [0 1 1; 1 1 1; 1 0 0; 1 0 1];
%cmap2 = data.meta.subject.color;
%cmap = subject.color;
window = 10;
bkg = zeros(xyloObj.Width, xyloObj.Height, 3);
tic
clf
for r=1:nframes
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d [%d-%d] (%6.2fxiRT)', r+startframe-1, startframe, endframe, RT);
    m = myMMReader(options.MovieFile, r+startframe-1);
    imagesc(m); hold on;
    for curr = 1:options.nSubjects;
        r1 = max(r-window, 1);
        currx = social.x(curr, r1:r);
        curry = social.y(curr, r1:r);
        plot(currx, curry, '.-', 'Color', cmap(curr, :));
        hold on;
        %nanStart  = find(isnan(currx)) - 1;
        %nanFinish = find(isnan(currx)) + 1;
        %plot(currx(nanStart(nanStart > 0)), curry(nanStart(nanStart > 0)), 'o', 'Color', cmap(curr, :), 'MarkerSize', 10);
        %plot(currx(nanFinish(nanFinish <= window)), curry(nanFinish(nanFinish <= window)), 'x', 'Color', cmap(curr, :), 'MarkerSize', 10, 'LineWidth', 4);
    end
%    axis([0 xyloObj.Width, 0 xyloObj.Height] * options.scale);
%    set(gca, 'YDir', 'rev', 'Color', [0 0 0]);
    title(sprintf('frame %d/%d', r, nframes),'fontsize', 17);
    hold off;
    %%
    F = getframe(gcf);
    aviobj = addframe(aviobj,F);
end
fprintf('\n');
aviobj = close(aviobj);
