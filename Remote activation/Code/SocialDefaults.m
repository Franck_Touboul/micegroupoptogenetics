function obj = SocialDefaults(obj, varargin)
% usage: SocialDefaults(obj, 'param1', value1, ...)
% set default values for parameters IF THEY WERE NOT SET
if (mod(nargin-1, 2) ~= 0)
    error 'usage: setDefaultParameters(obj, ''param1'', value1, ...)'
end

param_report = true;
if (isfield(obj, 'param_report'))
    param_report = obj.param_report;
end
optionsmem = obj;
for i=1:2:length(varargin)
    if ~isfield(obj, varargin{i})
        if isnumeric(varargin{i + 1})
            s = num2str(varargin{i + 1});
        else
            s = ['''' (varargin{i + 1}) ''''];
        end
        if (param_report)
            fprintf('# setting %-25s = %-15s ( <a href=" ">default</a>  )\n', varargin{i}, mat2str(s));
        end
        eval(['obj.' varargin{i} ' = [' s '];']);
    else
        optionsmem = rmfield(optionsmem, varargin{i});
        if (param_report)
            fprintf('# using   %-25s = %-15s ( <a href=" ">assigned</a> )\n', varargin{i}, mat2str(eval(['obj.' varargin{i}])));
        end
    end
end
% if (param_report)
%     if (isstruct(optionsmem))
%         fn_ = fieldnames(optionsmem);
%         for fn = 1:length(fn_)
%             fprintf('# using   %-25s = %-15s (<a href=" ">not tested</a>)\n', fn_{fn}, mat2str(obj.(fn_{fn})));
%         end
%     end
% end