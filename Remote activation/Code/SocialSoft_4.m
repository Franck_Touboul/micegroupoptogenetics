clf;
TrackDefaults;
filename = [options.output_path options.test.name '.social.mat'];
load(filename);

options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts
%
options.nBins = 15;
options.circadianMins = 120; % [mins]
options.circadianNormalize = true;

%%
output.nrows = 5;
output.ncols = 2;

%%
dt = social.time(2) - social.time(1);
circadianFrames = min(options.circadianMins * 60 / dt, social.nData);
nCircEpochs = max(floor(social.nData / circadianFrames), 1);

aux.hidden = social.zones.all == 4 | social.zones.all == 6;
aux.nearHidden = aux.hidden | ...
    [zeros(options.nSubjects, 1) aux.hidden(:, 1:end-1)] | ...
    [aux.hidden(:, 2:end) zeros(options.nSubjects, 1)];
aux.farFromHidden = aux.hidden & ...
    [zeros(options.nSubjects, 1) aux.hidden(:, 1:end-1)] & ...
    [aux.hidden(:, 2:end) zeros(options.nSubjects, 1)];
%%
aux.colormap = [];
for i=1:length(social.colors)
    aux.colormap(i, :) = social.colors(i, :);
end

%% velocities .............................................................
currRow = 1;
mySubplot(output.nrows, output.ncols, currRow, 1:2);
rowTitle(output.nrows, output.ncols, currRow, 'Velocities')

social.velocity = [zeros(options.nSubjects, 1) sqrt(diff(social.x, 1, 2).^2 + diff(social.y, 1, 2).^2)];
%
allVelocities = [];
velocities = [];
for s=1:options.nSubjects
    currVelocity = social.velocity(s, :);
%    currVelocity = currVelocity(aux.farFromHidden(s, :));
    velocities(s) = mean(currVelocity(currVelocity > 0));
    allVelocities(s) = mean(currVelocity);
end
%%
colormap(jet);
bar([velocities; allVelocities]','grouped');
prettyPlot('', 'subject');
legend('non-zero', 'all');
legend boxoff
freezeColors;
%%
currRow = 2;
circVelocities = [];
circAllVelocities = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:options.nSubjects
        currVelocity = social.velocity(s, circEpoch);
        %currHidden = aux.hidden(s, circEpoch);
        circVelocities(c, s) = mean(currVelocity > 0);
        circAllVelocities(c, s) = mean(currVelocity);
    end
end

mySubplot(output.nrows, output.ncols, currRow, 1);
for s=1:options.nSubjects
    plot(.5:nCircEpochs, circVelocities(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', aux.colormap(s, :));
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
prettyPlot('circadian (non-zero)', 'time [mins]');
freezeColors;

mySubplot(output.nrows, output.ncols, currRow, 2);
for s=1:options.nSubjects
    plot(.5:nCircEpochs, circAllVelocities(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', aux.colormap(s, :));
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
prettyPlot('circadian (all)', 'time [mins]');
freezeColors;

%% locations ..............................................................
currRow = 3;
mySubplot(output.nrows, output.ncols, currRow, 1:output.ncols);

zoneHist = zeros(length(social.zones.labels), options.nSubjects);
for i=1:length(social.zones.labels)
    for s=1:options.nSubjects
        zoneHist(i, s) = sum(social.zones.all(s, :) == i - 1);
    end
end

zoneHist = zoneHist ./ repmat(sum(zoneHist), length(social.zones.labels), 1);
%
colormap(aux.colormap);
bar(zoneHist);
set(gca, 'XTickLabel', social.zones.labels);
prettyPlot('location histogram');
freezeColors

%% food\water ...........................................................
currRow = 4;
mySubplot(output.nrows, output.ncols, currRow, 1);
rowTitle(output.nrows, output.ncols, currRow, 'Zones')

circFoodWater = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:options.nSubjects
        curr = social.zones.all(s, circEpoch);
        circFoodWater(c, s) = sum(curr == 1 | curr == 2 | curr == 3);
    end
end
if options.circadianNormalize
    circFoodWater = circFoodWater ./ repmat(sum(circFoodWater, 1), nCircEpochs, 1);
end

for s=1:options.nSubjects
    plot(.5:nCircEpochs, circFoodWater(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', aux.colormap(s, :));
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
prettyPlot('food\\water (circadian)', 'time [mins]');
freezeColors;

%% hidden ...............................................................
currRow = 4;
mySubplot(output.nrows, output.ncols, currRow, 2);

circHidden = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:options.nSubjects
        curr = social.zones.all(s, circEpoch);
        circHidden(c, s) = sum(curr == 5 | curr == 8);
    end
end
if options.circadianNormalize
    circHidden = circHidden ./ repmat(sum(circHidden, 1), nCircEpochs, 1);
end

for s=1:options.nSubjects
    plot(.5:nCircEpochs, circHidden(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', aux.colormap(s, :));
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
prettyPlot('hidden place (circadian)', 'time [mins]');
freezeColors;

%% labyrinth ..............................................................
currRow = 5;
mySubplot(output.nrows, output.ncols, currRow, 1);

circHidden = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:options.nSubjects
        curr = social.zones.all(s, circEpoch);
        circHidden(c, s) = sum(curr == 6);
    end
end
if options.circadianNormalize
    circHidden = circHidden ./ repmat(sum(circHidden, 1), nCircEpochs, 1);
end

for s=1:options.nSubjects
    plot(.5:nCircEpochs, circHidden(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', aux.colormap(s, :));
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
prettyPlot('labyrinth (circadian)', 'time [mins]');
freezeColors;

%% openspace ..............................................................
currRow = 5;
mySubplot(output.nrows, output.ncols, currRow, 2);

circHidden = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:options.nSubjects
        curr = social.zones.all(s, circEpoch);
        circHidden(c, s) = sum(curr == 0);
    end
end
if options.circadianNormalize
    circHidden = circHidden ./ repmat(sum(circHidden, 1), nCircEpochs, 1);
end

for s=1:options.nSubjects
    plot(.5:nCircEpochs, circHidden(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', aux.colormap(s, :));
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
prettyPlot('open-space (circadian)', 'time [mins]');
freezeColors;
