function SocialHierarchyFullPlot(obj)
SocialExperimentData;
obj = TrackLoad(obj);
% rankChange = zeros(length(experiments), nDays-1);
% levelCount = zeros(length(experiments), nDays);
% hierarchyStrength = zeros(length(experiments), nDays);
output = true;
rank = zeros(1, GroupsData.nSubjects);
%    prefix = sprintf(experiments{id}, day);
%    fprintf('#-------------------------------------\n# -> %s\n', prefix);
%obj = TrackLoad({id, day}, {'zones', 'Interactions', 'Hierarchy'});
%obj = SocialAnalysePredPreyModel(obj);
%%
%curr = obj.Interactions.PredPrey;
mat = obj.Hierarchy.Group.AggressiveChase.ChaseEscape - obj.Hierarchy.Group.AggressiveChase.ChaseEscape';
mat(binotest(obj.Hierarchy.Group.AggressiveChase.ChaseEscape, ...
    obj.Hierarchy.Group.AggressiveChase.ChaseEscape + obj.Hierarchy.Group.AggressiveChase.ChaseEscape')) = 0;
mat = mat .* (mat > 0);

prevRank = rank;

[rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);

%levelCount(id, day) = max(rank) - min(rank);
%hierarchyStrength(id, day) = sum(mat(:) ~= 0);
if output
    [q, bg] = SocialGraph(obj, mat, removed, obj.Hierarchy.Group.AggressiveChase.rank);
    g = biograph.bggui(bg);
    f = get(g.biograph.hgAxes, 'Parent');
    ppfile = ['Graphs/Hier/' 'predprey.' obj.FilePrefix '.SocialHierarchyBatch.png'];
    saveFigure(ppfile);
    close(g.hgFigure);
else
    %%
    [q, bg] = SocialGraph(obj, mat, removed, rank);
    g = biograph.bggui(bg);
    fig2 = get(g.biograph.hgAxes,'children');
    figure(1)
    h = subplot(4,2,(day-1)*2+1);
    cla(h)
    a1 = gca;
    copyobj(fig2, a1)
    axis off;
    set(gcf, 'Color', 'w');
    title(['Day ' num2str(day)]);
    close(g.hgFigure);
    
    h = subplot(4,2,(day-1)*2+2);
    imagesc(obj.Hierarchy.Group.AggressiveChase.Days(day).ChaseEscape);
    for i=1:obj.nSubjects
        for j=1:obj.nSubjects
            text(j, i, num2str(obj.Hierarchy.Group.AggressiveChase.Days(day).ChaseEscape(i, j)), 'color', 'w', 'verticalAlignment', 'middle', 'horizontalAlignment', 'center');
        end
    end
end
