function [obj, roi] = TrackMarkRegionsGUI(obj, fields, sheltered)

if ~exist('fields', 'var')
    fields = {};
end

if ~exist('sheltered', 'var')
    sheltered = [];
end

if isfield(obj, 'BkgImage') && ~isempty(obj.BkgImage)
    img = im2double(obj.BkgImage);
else
    img = myMMReader(obj.VideoFile, round(obj.nFrames/2));
end

%% 
for i=1:size(img, 3)
    img(:, :, i) = histeq(img(:, :, i));
end
%%
clf;
imshow(img);
index = 1;
roi = {};
cmap = lines(16);

while true
    if isempty(fields)
        title(sprintf('mark region no. %d...', index));
    else
        title(sprintf('mark region no. %d (%s)...', index, fields{index}));
    end
    roi{index} = roipoly;
    img = TrackShowRegion(img, roi{index}, cmap(mod(index-1, 16)+1, :));
    imshow(img);
    if isempty(fields)
        title(sprintf('continue?'));
        k = 0;
        while k ~= 1
            k = waitforbuttonpress;
        end
        key = get(gcf, 'CurrentCharacter');
        if char(key) ~= 'y' && char(key) ~= 'Y'
            break;
        end
    else
        if index == length(fields)
            break;
        end
    end
    index = index + 1;
end
title(sprintf('Done!'));

if isempty(sheltered)
    for i=1:length(roi)
        sheltered(i) = false;
    end
end

if isempty(fields)
    for i=1:length(roi)
        fields{i} = num2str(i);
    end
end

obj.ROI.Regions = roi;
obj.ROI.nRegions = length(roi);
obj.ROI.RegionNames = fields;
obj.ROI.IsSheltered = sheltered;

%% Hidden Areas
index = 1;
obj.ROI.Hidden = {};
obj.ROI.HiddenImage = false(obj.VideoHeight, obj.VideoWidth);
for i=1:obj.ROI.nRegions
    if obj.ROI.IsSheltered(i)
        obj.ROI.Hidden{index} = obj.ROI.Regions{i};
        obj.ROI.HiddenImage = obj.ROI.HiddenImage | obj.ROI.Regions{i};
        index = index + 1;
    end
end
obj.ROI.nHidden = length(obj.ROI.Hidden);
%
obj.ROI.HiddenCenters = zeros(obj.ROI.nHidden, 2);
obj.ROI.HiddenSCenters = zeros(obj.ROI.nHidden, 2);
for i=1:obj.ROI.nHidden
    [x, y] = find(obj.ROI.Hidden{i});
    obj.ROI.HiddenCenters(i, :) = [mean(y), mean(x)];
    obj.ROI.HiddenSCenters(i, :) = obj.ROI.HiddenCenters(i, :) * obj.VideoScale;
end
% compute bounderies of roi's in scaled coordinates 
obj.ROI.HiddenBoundarySCoordinates = {};
for i=1:obj.ROI.nHidden
    img = imresize(obj.ROI.Hidden{i}, obj.VideoScale);
    [x, y] = find((imdilate(img ~= 0, ones(3,3)) - (img ~= 0)) ~= 0);
    obj.ROI.HiddenBoundarySCoordinates{i} = [y, x];
end
% compute min distance between roi's
obj.ROI.HiddenSDistances = zeros(obj.ROI.nHidden);
for i=1:obj.ROI.nHidden
    for j=i:obj.ROI.nHidden
        if i~=j
            p = mypdist2(obj.ROI.HiddenBoundarySCoordinates{i}, obj.ROI.HiddenBoundarySCoordinates{j});
            obj.ROI.HiddenSDistances(i,j) = min(p(:));
            obj.ROI.HiddenSDistances(j,i) = min(p(:));
        end
    end
end
