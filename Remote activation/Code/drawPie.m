function drawPie(x, y, radios, angle, color, varargin)
res = 0.02;
cord = [];
center = [x, y];
cord = [x, y];
for angle=0:res:angle
    cord = [cord; center + [radios * sin(angle), radios * cos(angle)]];
end
cord = [cord; x, y];
fill(cord(:,1), cord(:,2), color, varargin{:});
