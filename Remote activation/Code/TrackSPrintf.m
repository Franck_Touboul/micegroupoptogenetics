function [obj, s] = TrackSPrintf(obj, varargin)

if ~isnumeric(varargin{1})
    level = 0;
else
    level = varargin{1};
    varargin = {varargin{2:end}};
end

switch level
    case 0
        s = ['# ' sprintf(varargin{:})];
    case 1
        s = ['# - ' sprintf(varargin{:})];
    case 2
        s = ['#  . ' sprintf(varargin{:})];
    otherwise
        s = ['#     ' sprintf(varargin{:})];
end
