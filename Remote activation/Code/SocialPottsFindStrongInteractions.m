function SocialPottsFindStrongInteractions(obj ,order, N)
%%
me = obj.Analysis.Potts.Model{order};
map = me.order == order;
weights = abs(me.weights - median(me.weights(map)));
weights(~map) = -inf;clc
[q, o] = sort(weights, 'descend');
%%
for i=1:N
    fprintf('#---------------------------\n');
    fprintf('# mice: '); fprintf(' %d', me.labels{o(i)}(1, :)); fprintf('\n');
    fprintf('# areas: '); fprintf(' %s', obj.ROI.ZoneNames{me.labels{o(i)}(2, :)}); fprintf('\n');
    fprintf('# weight: %.3f\n', me.weights(o(i)));
end
