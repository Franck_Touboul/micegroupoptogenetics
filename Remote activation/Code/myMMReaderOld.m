function obj = myMMReader(filename, frame)

persistent videoFilename;
persistent videoObj;


if ~strcmp(videoFilename, filename)
    videoFilename = filename;
    videoObj = mmreader(videoFilename);
end

if nargin > 1
    obj = read(videoObj, frame);
else
    obj = videoObj;
end
