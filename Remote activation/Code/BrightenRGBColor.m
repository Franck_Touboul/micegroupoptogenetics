function CMAP = BrightenRGBColor(cmap, factor)
for i=1:size(cmap, 1)
    c = rgb2hsv(reshape(cmap(i, :), [1 1 3]));
    c(2) = min(c(2) * factor, 1);
    r = hsv2rgb(reshape(c(1, :), [1 1 3]));
    CMAP(i, :) = r(:);
end