function TrackFindPaths(nruns, id)
%%
fprintf('# finding paths\n');
options = struct();
TrackDefaults;
%%
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nframes = xyloObj.NumberOfFrames;
dt = 1/xyloObj.FrameRate;
%%
fprintf('# - loading segmentations\n');
if ~exist('nruns', 'var')
    filename = [options.output_path options.test.name '.segm.mat'];
    waitforfile(filename);
    load(filename);
elseif ~exist('id', 'var')
    id = 0;
    cents.x       = [];
    for i=1:nruns
        fprintf('#      . segment no. %d\n', i);
        filename = [options.output_path options.test.name '.segm.' sprintf('%03d', i) '.mat'];
        waitforfile(filename);
        currSegm = load(filename);
        cents = structcat(cents, currSegm.cents, 1, true, false);
    end
    segm.start = 1;
    segm.finish = nframes;
else
    step = floor(nframes / nruns);
    curr = 0;
    range = 1:nruns;
    cents.x = [];
    if id > 0
        from = max(id-1, 1);
        to = min(id+1, nruns);
        range = from:to;
        fprintf('#      . segment id: %d\n', id);
    else
        segm.start = 1;
        segm.finish = nframes;
    end
    for i=range
        filename = [options.output_path options.test.name '.segm.' sprintf('%03d', i) '.mat'];
        waitforfile(filename);
        currSegm = load(filename);
        if i == id
%             segm.start  = currSegm.cents.startframe;
%             segm.finish = currSegm.cents.endframe;
            segm.start  = size(cents.x, 1) + 1;
            segm.finish = size(cents.x, 1) + size(currSegm.cents.x, 1);
            segm.cents.startframe = currSegm.cents.startframe;
            segm.cents.endframe = currSegm.cents.endframe;
        end
        cents = structcat(cents, currSegm.cents, 1, true, false);
    end
%     for i=1:nruns
%         prev = curr + 1;
%         if i == nruns
%             curr = nframes;
%         else
%             curr = prev + step - 1;
%         end
%         if ismember(i, range)
%             segmRange = sprintf('%d-%d', prev, curr);
%             filename = [options.output_path options.test.name '.segm.' segmRange '.mat'];
%             currSegm = load(filename);
%             if i == id
%                 segm.start  = size(cents.x, 1) + 1;
%                 segm.finish = size(cents.x, 1) + size(currSegm.cents.x, 1);
%             end
%             cents = structcat(cents, currSegm.cents, 1, true, true);
%         end
%     end
    %% pad track
    fprintf('#      . setting padding to %d seconds\n', options.parallelTrackPadding);
    if id > 0
        chunk.start  = max(segm.start  - options.parallelTrackPadding / dt, 1);
        chunk.finish = min(segm.finish + options.parallelTrackPadding / dt, size(cents.x, 1));
        
        cents.x       = cents.x(chunk.start:chunk.finish, :);
        cents.y       = cents.y(chunk.start:chunk.finish, :);
        cents.label   = cents.label(chunk.start:chunk.finish, :);
        cents.area    = cents.area(chunk.start:chunk.finish, :);
        cents.logprob = cents.logprob(chunk.start:chunk.finish, :, :);
        
        segm.start  = segm.start  - chunk.start + 1;
        segm.finish = segm.finish - chunk.start + 1;
    end
end

%%
fprintf('# - tracing ');
ROIloaded = false;
if isfield(options, 'loadROI') && options.loadROI
    filename = [options.output_path options.test.name '.roi.mat'];
    load(filename);
    ROIloaded = true;
elseif isfield(options, 'ROIFile') && ~isempty(options.ROIFile)
    options.loadROI = true;
    load(options.ROIFile);
    ROIloaded = true;
end

% if ROIloaded
%     if isfield(social.roi, 'Ignore')
%         ignored = social.roi.Ignore(round(cents.x(cents.label~=0) / options.scale), round(cents.y(cents.label~=0) / options.scale));
%         cents.label(cents.label~=0) = cents.label(cents.label~=0) .* ignored;
%     end
% end

options.jumpVar = eye(2) * 5^2;
options.confProb = 0.1;
options.hideProb = 0.0;

res = {};
nchars = RePrintf('(%2d/%2d)', 0, options.nSubjects);
for curr = 1:options.nSubjects;
    nchars = RePrintf(nchars, '(%2d/%2d)', curr, options.nSubjects);

    % res{curr} = followTrack_v5(options, cents, curr); % a slower version of
    % v6
%     if ROIloaded
%         res{curr} = followTrackWithROI(options, cents, curr, roi);
%     else
        res{curr} = followTrack_v7(options, cents, curr); 
%     end
end
fprintf('\n');

if id > 0
    for curr = 1:options.nSubjects;
        res{curr}.x     = res{curr}.x(segm.start:segm.finish);
        res{curr}.y     = res{curr}.y(segm.start:segm.finish);
        res{curr}.prob  = res{curr}.prob(segm.start:segm.finish);
        res{curr}.id    = res{curr}.id(segm.start:segm.finish);
    end
    cents.x       = cents.x(segm.start:segm.finish, :);
    cents.y       = cents.y(segm.start:segm.finish, :);
    cents.label   = cents.label(segm.start:segm.finish, :);
    cents.area    = cents.area(segm.start:segm.finish, :);
    cents.logprob = cents.logprob(segm.start:segm.finish, :, :);
end

fprintf('# - saving raw track\n');
if id > 0
    filename = sprintf([options.output_path options.test.name '.raw-track.%03d.mat'], id);
else
    filename = [options.output_path options.test.name '.raw-track.mat'];
end
save(filename, 'res');
rawTrack = res;

%%
filename = [options.output_path options.test.name '.meta.mat'];
load(filename);

% checks for double assigned segments, and chooses the best matching subject
fprintf('# - ensuring labels are unique\n');
res = reassignLabels(res, cents, options.minDistance);
%res = reassignLabels(res);

% remove false alarms
fprintf('# - removing false alarms:\n');


% - big jumps
fprintf('#   a. connecting neighboring visible areas\n');
for curr = 1:options.nSubjects;
    visible = res{curr}.id > 0;
    d = diff([0 visible 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    distance = sqrt(...
        (res{curr}.x(start(2:end)) - res{curr}.x(finish(1:end-1))).^2 + ...
        (res{curr}.y(start(2:end)) - res{curr}.y(finish(1:end-1))).^2);
    gap = start(2:end) - finish(1:end-1) - 1;
    loc = gap < options.minHiddenDuration & distance < options.maxJumpPerStep * gap & distance < options.maxTotalJump;
    for l=find(loc)
        range = finish(l)+1:start(l+1)-1;
        res{curr}.id(range) = inf;
%        res{curr}.x(range) = interp1([range(1)-1 range(end)+1], ([range(1)-1 range(end)+1]), range);
%        res{curr}.y(range) = interp1([range(1)-1 range(end)+1], res{curr}.y([range(1)-1 range(end)+1]), range);
        [res{curr}.x(range), res{curr}.y(range)] = MotionInterp(res{curr}.x, res{curr}.y, range, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
    end    
end

% - small visibility epochs
fprintf('#   b. removing small visible areas\n');
for curr = 1:options.nSubjects;
    visible = res{curr}.id ~= 0;
    d = diff([0 visible 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    loc = find(finish - start + 1 < options.minVisibleDuration);
    for l=loc
        if start(l) > 1
            res{curr}.x(start(l):finish(l)) = nan;
            res{curr}.y(start(l):finish(l)) = nan;
            res{curr}.id(start(l):finish(l)) = 0;
        end
    end
end

fprintf('#   c. removing distant areas\n');
for curr = 1:options.nSubjects;
    x = res{curr}.x; x(isnan(x)) = inf;
    y = res{curr}.y; y(isnan(y)) = inf;
    fullDistance = [0 sqrt(diff(x).^2 + diff(y).^2)];
    visible = fullDistance > options.maxJumpPerStep;
    
    start  = [1 find(visible > 0)];
    finish = [find(visible > 0) - 1, length(visible)];
    
    start  = start (isfinite(x(start)));
    finish = finish(isfinite(x(finish)));

    loc = find(finish - start < options.minVisibleDuration);
    for l=loc
        if start(l) > 1
            res{curr}.x(start(l):finish(l)) = nan;
            res{curr}.y(start(l):finish(l)) = nan;
            res{curr}.id(start(l):finish(l)) = 0;
        end
    end
end

% for curr = 1:options.nSubjects;
%     visible = res{curr}.id > 0;
%     d = diff([0 visible 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     distance = sqrt(...
%         (res{curr}.x(start(2:end)) - res{curr}.x(finish(1:end-1))).^2 + ...
%         (res{curr}.y(start(2:end)) - res{curr}.y(finish(1:end-1))).^2);
%     skipDistance = sqrt(...
%         (res{curr}.x(start(3:end)) - res{curr}.x(finish(1:end-2))).^2 + ...
%         (res{curr}.y(start(3:end)) - res{curr}.y(finish(1:end-2))).^2);
%     isBigJump  = distance > options.maxJumpPerStep * (start(2:end) - finish(1:end-1));
%     isSmallGap = skipDistance < options.maxJumpPerStep * (start(3:end) - finish(1:end-2));
%     isBigJump = isBigJump & [isSmallGap false];
%     loc = find(isBigJump);
%     for l=loc
%         
%     end
% end

% remove small areas where the subject is hidden
% fprintf('# - removing misdetections\n');
% for curr = 1:options.nSubjects;
%     hidden = res{curr}.id == 0;
%     d = diff([0 hidden 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     loc = find(finish - start + 1 < options.minHiddenDuration & start > 1 & finish < length(hidden));
%     for l=loc
%         res{curr}.x(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.x([start(l)-1, finish(l)+1]), start(l):finish(l));
%         res{curr}.y(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.y([start(l)-1, finish(l)+1]), start(l):finish(l));
%     end
% end

fprintf('# - interpolate position\n');
for curr = 1:options.nSubjects;
    hidden = res{curr}.id == 0;
    d = diff([0 hidden 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    loc = find(start > 1 & finish < length(hidden));
    for l=loc
        range = start(l):finish(l);
%         res{curr}.x(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.x([start(l)-1, finish(l)+1]), start(l):finish(l));
%         res{curr}.y(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.y([start(l)-1, finish(l)+1]), start(l):finish(l));
        [res{curr}.x(range), res{curr}.y(range)] = MotionInterp(res{curr}.x, res{curr}.y, range, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
    end
    if length(finish) > 0 && finish(end) == length(hidden) && start(end) > 1
        res{curr}.x(start(end):finish(end)) = res{curr}.x(start(end)-1);
        res{curr}.y(start(end):finish(end)) = res{curr}.y(start(end)-1);
    end
end

%%
fprintf('# - saving track\n');
%             segm.start  = currSegm.cents.startframe;
%             segm.finish = currSegm.cents.endframe;

if id > 0
    filename = sprintf([options.output_path options.test.name '.track.%03d.mat'], id);
else
    filename = [options.output_path options.test.name '.track.mat'];
end

save(filename, 'res');


return;
%%
social = struct('nSubjects', 0, 'nData', 0, 'x', [], 'y', [], 'time', [], 'dt', 0, ...
    'options', struct(), 'meta', struct(), 'colors', [], 'roi', struct(), 'zones', struct());

fprintf('# finding paths\n');
social.options = options;
social.dt = dt;
for i=1:length(res)
    social.x(i, :) = res{i}.x;
    social.y(i, :) = res{i}.y;
end
social.x = social.x / options.scale;
social.y = social.y / options.scale;
social = kalmanFilterTrack(social, options.kalman.q, options.kalman.r);
% 
fprintf('# finding dark zones\n');
grayBkgFrame = rgb2gray(meta.bkgFrame);
grayBkgFrameEq = im2double(histeq(grayBkgFrame)) > .4;
se = strel('disk',4);
se2 = strel('disk',15);
hiddenMask = imclose(imopen(grayBkgFrameEq, se), se2);
%
fprintf('# organizing results\n');
social.nData = size(social.x, 2);
social.time = (1:social.nData) *  dt;
% zones.labyrinth = false(size(social.x));
% zones.smallNest = false(size(social.x));
% zones.bigNest = false(size(social.x));
orig = social;
%
for i=1:options.nSubjects
    x = round(social.x(i, :)); x(x == 0) = 1;
    y = round(social.y(i, :)); y(y == 0) = 1;
    %
    startnan = find(isnan(x), 1); 
    if startnan > 1
        x(startnan:end) = x(startnan - 1);
        y(startnan:end) = y(startnan - 1);
    else
        x(startnan:end) = 1;
        y(startnan:end) = 1;
    end
    %
    ind = sub2ind(size(hiddenMask), y, x);
    zones.dark(i, :) = hiddenMask(ind);
    
    zones.hidden(i, :) = res{i}.id == 0;
end
%
scaledCents = cents;
scaledCents.x = cents.x / options.scale;
scaledCents.y = cents.y / options.scale;
scaledCents.area = cents.area / options.scale^2;

social.cents = scaledCents;
social.zones = zones;
social.meta = meta;
social.colors = social.meta.subject.color;
social.meta.hiddenMask = hiddenMask;
social.nSubjects = options.nSubjects;

if id > 0
    filename = [options.output_path options.test.name '.social.' id '.mat'];
else
    filename = [options.output_path options.test.name '.social.mat'];
end
fprintf('# - saving data\n');
save(filename, 'social');

return;

%%
fprintf('# showing tracks\n');
nchars = RePrintf(nchars, '# - frame %6d/%6d', 0, nFrames);
cmap = lines;
cmap = [1 0 0; 1 0 1; 0 1 1; 1 1 1];
window = 10;
for r=1:nFrames
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d/%6d', r, nFrames);
    
%      m = imsubtract(read(xyloObj, r), bkgFrame);
%      m = imresize(m, options.scale);
%      hold off; imagesc(m); hold on;
      hold off; imagesc(m*0); hold on;
    for curr = 1:options.nSubjects;
        r1 = max(r-window, 1);
        currx = res{curr}.x(r1:r);
        curry = res{curr}.y(r1:r);
        plot(currx, curry, '.-', 'Color', cmap(curr, :));
        nanStart  = find(isnan(currx)) - 1;
        nanFinish = find(isnan(currx)) + 1;
        plot(currx(nanStart(nanStart > 0)), curry(nanStart(nanStart > 0)), 'o', 'Color', cmap(curr, :), 'MarkerSize', 10);
        plot(currx(nanFinish(nanFinish <= window)), curry(nanFinish(nanFinish <= window)), 'x', 'Color', cmap(curr, :), 'MarkerSize', 10, 'LineWidth', 4);
    end
    title(sprintf('frame %6d/%6d', r, nFrames));
    hold off;
    drawnow;
end
fprintf('\n');
return
