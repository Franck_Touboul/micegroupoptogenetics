function [vec1, vec2, Djs] = SocialPottsMatch(obj1, obj2)
objs = {};
objs{1} = obj1;
objs{2} = obj2;

SocialExperimentData
subjPerms = perms(1:GroupsData.nSubjects);
%Vecs2 = {};
%Ids = 1:length(experiments);
Ids = [1 11];
order = 3;

for id=1:2
    try
        %%
        obj = TrackLoad(objs{id});
        %%
        fprintf('# - extracting weights... ');
        fprintf([num2str(order) '... ']);
        vecs(id).weights = [];
        vecs(id).order = [];
        for i=1:size(subjPerms, 1)
            [vecs(id).weights(i, :), vecs(id).order(i, :)] = PottsVectorize_v2(obj, obj.Analysis.Potts.Model{order}, subjPerms(i, :));
        end
        %            Vecs2{order}{id} = SocialPottsVector(obj);
        fprintf(' done!\n');
    catch
    end
end
%%
fprintf('# - computing probabilities for each model ');
Probs = {};
for id=1:length(vecs)
    ProgressReport((order - 1)*length(vecs) + id, length(vecs) * obj.nSubjects)
    Probs{id} = [];
    for i=1:size(vecs(id).weights, 1)
        p = exp(obj.Analysis.Potts.Model{order}.perms * vecs(id).weights(i, :)');
        Z = sum(p);
        p = p / Z;
        Probs{id}(i, :) = p;
    end
end
ProgressReport;
fprintf('\n');

%%
match = struct();
fprintf('# - computing Djs between groups ');
d = zeros(length(Probs));
match.i = zeros(length(Probs));
match.j = zeros(length(Probs));
match.Djs = zeros(length(Probs));
match.idx = zeros(length(Probs));
idx = 1;
for i=1:length(Probs)
    if isempty(Probs{i}); continue; end
    d(i,i) = nan;
    for j=i+1:length(Probs)
        if isempty(Probs{j}); continue; end
        ProgressReport((order - 1)*.5 * length(Probs) *  (length(Probs) + 1) + idx, .5 * length(Probs) *  (length(Probs) + 1) * obj.nSubjects)
        try
            D = zeros(size(Probs{i}, 1), size(Probs{j}, 1));
            for i1=1:size(Probs{i}, 1)
                for i2=1:size(Probs{j}, 1)
                    D(i1, i2) = JensenShannonDivergence(Probs{i}(i1, :), Probs{j}(i2, :));
                end
            end
            %D = corr(vecs{i}', vecs{j}');
            [d(i, j), match.idx(i, j)] = min(D(:));
            [match.Djs(i, j), match.i(i, j) match.j(i, j)] = minn(D);
            match.i(j, i) = match.i(i, j);
            match.j(j, i) = match.j(i, j);
            match.Djs(j, i) = match.Djs(i, j);
            match.idx(j, i) = match.idx(i, j);
            d(j, i) = d(i, j);
        catch
            match.idx(i, j) = nan;
            match.idx(j, i) = match.idx(i, j);
            
            d(i, j) = nan;
            d(j, i) = d(i, j);
        end
        idx = idx + 1;
    end
end
ProgressReport(1, 1);
ProgressReport;
fprintf('\n');

%%
vec1.weights = vecs(1).weights(match.i(1, 2), :);
vec2.weights = vecs(2).weights(match.j(1, 2), :);
vec1.order = vecs(1).order(match.i(1, 2), :);
vec2.order = vecs(2).order(match.j(1, 2), :);
Djs = match.Djs(1, 2);