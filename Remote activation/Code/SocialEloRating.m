function Elo = SocialEloRating(objs)

% Based upon :
%    Christof Neumann, Julie Duboscq, Constance Dubuc, Andri Ginting,
%    Ade Maulana Irwan, Muhammad Agil, Anja Widdig, Antje Engelhardt,
%    Assessing dominance hierarchies: validation and advantages of
%    progressive evaluation with Elo-rating, Animal Behaviour,
%    Volume 82, Issue 4, October 2011, Pages 911-921, ISSN 0003-3472,
%    10.1016/j.anbehav.2011.07.016.

if ~isa(objs, 'cell')
    objs{1} = objs;
end
obj = objs{1};

Elo.WindowSize = 4; % hours
Elo.k = 100;
Elo.Scores = ones(obj.nSubjects, 1) * 1000;
Elo.Times = find(obj.valid, 1);
Elo.Day = 1;
Elo.Map = false(obj.nSubjects, 1);
starttime = 0;

%%
idx = 1;
for oid=1:length(objs)
    obj = objs{oid};
    map = obj.Contacts.Behaviors.AggressiveChase.Map;
    Elo.Scores = [Elo.Scores, zeros(obj.nSubjects, sum(map))];
    Elo.Times = [Elo.Times, zeros(1, sum(map))];
    Elo.Day = [Elo.Day, zeros(1, sum(map))];
    Elo.Map = [Elo.Map, false(obj.nSubjects, sum(map))];
    for i=find(map)
        %   obj.Contacts.List.beg
        %    obj.Contacts.Behaviors.AggressiveChase.Chaser(i)
        Pred = obj.Contacts.Behaviors.AggressiveChase.Chaser(i);
        Prey = obj.Contacts.Behaviors.AggressiveChase.Escaper(i);
        
        d = Elo.Scores(Pred, idx) - Elo.Scores(Prey, idx);
        ploss = 1 ./ (1 + 10 .^ (d/400));
        Elo.Scores(Pred, idx + 1) = Elo.Scores(Pred, idx) + ploss * Elo.k;
        Elo.Scores(Prey, idx + 1) = Elo.Scores(Prey, idx) - ploss * Elo.k;
        other = 1:obj.nSubjects;
        other = other(other ~= Pred);
        other = other(other ~= Prey);
        Elo.Scores(other, idx + 1) = Elo.Scores(other, idx);
        Elo.Times(idx + 1) = min(obj.Contacts.List(i).beg) + starttime;
        Elo.Day(idx + 1) = oid;
        Elo.Map(Pred, idx + 1) = true;
        Elo.Map(Prey, idx + 1) = true;
        idx = idx + 1;
    end
    %%
    % for s=obj.nSubjects
    %     plot(Elo.Times, Elo.Scores', '.-')
    % end
    starttime = starttime + obj.nFrames;
end
%%
% C = sum(abs(diff(Elo.Scores, 1, 2)));
% weighted = Elo.Scores - repmat(min(Elo.Scores), obj.nSubjects, 1);
% weighted = weighted ./ repmat(max(weighted), obj.nSubjects, 1);
% weighted(Elo.Map) = 0;
% w = max(weighted);
%
% N = ones(1, size(C, 2)) * 2;
% Elo.StabilityIndex = C*w(2:end)' / sum(N);

% w =
Hours = Elo.Times/obj.FrameRate/60/60;
idx = 1;
Elo.Median.Scores = zeros(obj.nSubjects, round(max(Hours) / Elo.WindowSize));
for i=0:Elo.WindowSize:max(Hours)
    if max(Hours) - i < Elo.WindowSize / 2
        break
    end
    Elo.Median.Scores(:, idx) = median(Elo.Scores(:, Hours >= i & Hours < i + Elo.WindowSize), 2);
    Elo.Median.Times(idx) = (i + Elo.WindowSize / 2) * obj.FrameRate * 60 * 60;
    idx = idx + 1;
end
%%
Elo.Rank = zeros(size(Elo.Scores));
seq = 1:obj.nSubjects;
r = zeros(1, obj.nSubjects);
for i=1:size(Elo.Scores, 2)
    [~, o] = sort(Elo.Scores(:, i), 'descend');
    r(o) = seq;
    Elo.Rank(:, i) = r;
end



%%
if nargout == 0
    %     plot(Elo.Times, Elo.Scores, 'color');
    %     axis;
    
    cmap = MyMouseColormap;
    for s=1:obj.nSubjects
        subplot(2,1,1);
        plot(Elo.Times/obj.FrameRate/60/60, Elo.Scores(s, :), 'color', cmap(s, :));
        hon;
        subplot(2,1,2);
        plot(Elo.Median.Times/obj.FrameRate/60/60, Elo.Median.Scores(s, :), 'o-', 'MarkerFaceColor', cmap(s, :), 'MarkerEdgeColor', 'k', 'color', 'k');
        hon
    end
    subplot(2,1,1);
    hoff;
    subplot(2,1,2);
    hoff
end