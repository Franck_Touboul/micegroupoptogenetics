function obj = TrackResaveObject(obj, videoFile, writeToFile)
if nargin == 2;
    writeToFile = true;
end
obj = TrackLoad(obj);

obj.VideoFile = videoFile;
obj.FilePrefix = regexprep(regexprep(obj.VideoFile, '\.[^\.]*$', ''), '.*[\\/]', '');
obj = TrackReadVideoFileProperties(obj);

filename = [obj.OutputPath obj.FilePrefix '.obj.mat'];
if writeToFile
    fprintf(['# saving tracking object to ''' filename '''\n']);
    TrackSave(obj);
end
