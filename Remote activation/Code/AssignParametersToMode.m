function model = AssignParametersToMode(field, params)
model.parameters.(field) = params;