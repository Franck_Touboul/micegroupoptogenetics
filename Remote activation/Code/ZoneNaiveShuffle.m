function zones = ZoneNaiveShuffle(obj)
zones = obj.zones(:, obj.valid);
for s=1:obj.nSubjects
    %%
    z = zones(s, :);
    chnage = [true z(1:end-1) ~= z(2:end)];
    beg = find(chnage);
    len = [diff(beg), length(z) - beg(end)];
    r = randperm(length(beg));
    %%
    nz = zeros(1, length(z));
    offset = 1;
    for i=r
        nz(offset:offset+len(i)-1) = z(beg(i));
        offset = offset + len(i);
    end
    zones(s, :) = nz;
end