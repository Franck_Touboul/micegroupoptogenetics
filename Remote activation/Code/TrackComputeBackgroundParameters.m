function obj = TrackComputeBackgroundParameters(obj)
obj = TrackLoad(obj);

fprintf('# finding background image\n');
cdata = zeros(obj.VideoHeight, obj.VideoWidth, 3, obj.nBkgFrames);
fprintf('# - using random frames\n');
i = 0;
n = RePrintf('# - frame %d of %d', i, obj.nBkgFrames);
while i < obj.nBkgFrames
    m1 = 1; %nFrames * 1/3;
    m2 = obj.nFrames; % * 2/3;
    
    r = floor(rand(1) * (m2 - m1) + m1);
    if r * obj.dt < obj.StartTime; continue; end
    if obj.EndTime > 0 && r * obj.dt > obj.EndTime; continue; end
    frame = myMMReader(obj.VideoFile, r);
    if ~isempty(frame)
        i = i +1;
        n = RePrintf(n, '# - frame %d of %d', i, obj.nBkgFrames);
        cdata(:,:,:,i) = im2double(frame);
    end
end
fprintf('\n');
obj.Bkg.Mean = median(cdata, 4);
obj.Bkg.Noise = sqrt(median((cdata - repmat(obj.Bkg.Mean, [1 1 1 obj.nBkgFrames])).^2, 4));
if obj.Output
    imagesc(obj.Bkg.Mean);
end
