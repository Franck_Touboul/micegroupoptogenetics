function model = GeneratePerdPreyModel_v2(input)
model = struct();
%%
model.all.posterior = @PPModelExpPDF;
model.all.estimate  = @PPModelExpPDFEstimation;
model.all.check = false;
model.all.contact = false;
model = model.all.estimate(input, model);
for s=1:6
    model.states(s) = model.all;
end
%% state 1: avoidance
s = 1;
%model.states(s).histogram = ones(1, model.Histogram.nBins) / model.Histogram.nBins;
model.states(s).contact = false;
model.states(s).check = true;
model.states(s).title = '-';
model.states(s).type = 0;

%% state 2: predetor
s = 2;
model.states(s).weights = [.25 .75];
model.states(s).title = 'pred';
model.states(s).type = 1;

%% state 3: prey
s = 3;
model.states(s).weights = [.75 .25];
model.states(s).title = 'prey';
model.states(s).type = 1;

%% state 4: contact
s = 4;
model.states(s).contact = true;
model.states(s).check = true;
model.states(s).title = 'cont';
model.states(s).type = 0;

%% state 5: predetor
s = 5;
model.states(s).weights = [.25 .75];
model.states(s).title = 'pred';
model.states(s).type = 1;

%% state 6: prey
s = 6;
model.states(s).weights = [.75 .25];
model.states(s).title = 'prey';
model.states(s).type = 1;

%%
persistence = 10;
pst = 1-1/persistence;
ptr = 1/persistence;
pst = 1;
ptr = 1;
model.trans = [...
    pst ptr ptr ptr 0.0 0.0; % -
    0.0 pst 0.0 ptr 0.0 0.0; % predator
    0.0 0.0 pst ptr 0.0 0.0; % prey
    ptr 0.0 0.0 pst ptr ptr; % contact
    ptr 0.0 0.0 0.0 pst 0.0; % predator
    ptr 0.0 0.0 0.0 0.0 pst; % prey
    ];
model = validateModel(model);

model.start = zeros(model.nstates, 1);
model.start(1) = 1;
model.start(2) = 1;
model.start(3) = 1;
model.start(4) = 1;
model.end = zeros(model.nstates, 1);
model.end(1) = 1;
model.end(4) = 1;
model.end(5) = 1;
model.end(6) = 1;
