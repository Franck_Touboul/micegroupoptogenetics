function obj = TrackComputeColorsHSV(obj, coordinates)
if ischar(obj)
    load(obj);
end

colorVec = cell(1, obj.nSubjects);
colorHistVec = cell(1, obj.nSubjects);
bkgHistVec = [];
fillerHistVec = [];
bins = sequence(0, 1, obj.nColorBins);

frames = unique(coordinates.frame);
for f=frames(:)'
    [obj, img] = TrackSimpleSegmentFrame(obj, f);
    labeledOrig    = bwlabel(img.segmented);
    labeledMatched = zeros(size(img.segmented), 'uint16');
    %%
    inframe = find(coordinates.frame == f);
    x = round(coordinates.x(inframe) * obj.VideoScale);
    y = round(coordinates.y(inframe) * obj.VideoScale);
    label = coordinates.label(inframe);
    for i=1:length(x)
        if x(i) > size(labeledOrig, 1) || x(i) < 1 || y(i) > size(labeledOrig, 2) || y(i) < 1
            fprintf('#   ! pixel (%3d, %3d) is out of range\n', label(i), x(i), y(i));
            continue;
        end
        labelId = labeledOrig(x(i), y(i));
        if labelId == 0
            fprintf('#   ! no pixels found for subject %d in (%3d, %3d)\n', label(i), x(i), y(i));
        else
            currSeg = (labeledOrig == labelId);
            colorVec{label(i)} = [colorVec{label(i)}; reshape(img.hsv(x(i), y(i), :), 1, 3, 1)];
            if sum(sum(labeledMatched(currSeg) ~= 0)) > 0
                fprintf('#   ! segment for subject %d in (%3d, %3d) matches also segment for subject %d\n', label(i), x(i), y(i), max(max(labeledMatched(currSeg))));
                labeledMatched(currSeg) = 0;
            else
                fprintf('# - added label for subject %d in (%3d, %3d)\n', label(i), x(i), y(i));
                if obj.Output
                    img.scaled = im2double(imresize(img.orig, obj.VideoScale));
                    emph = img.scaled * 0;
                    for c=1:3
                        slice = img.scaled(:,:,c);
                        black = slice * 0;
                        black(currSeg) = slice(currSeg);
                        emph(:, :, c) = black;
                        colors(c) = slice(x(i), y(i)); %mean(slice(currSeg));
                    end
                    subplot(2,3,[1:2, 4:5]);
                    imagesc(emph);
                    subplot(2,3,6);
                    imagesc(reshape(colors, 1, 1, 3));
                    subplot(2,3,3);
                    tempColorsImage = zeros(obj.nSubjects, 3);
                    for l=1:length(colorVec)
                        if ~isempty(colorVec{l})
                            tempColorsImage(l, :) = mean(colorVec{l}, 1);
                        end
                    end
                    tempColorsImage = hsv2rgb(reshape(tempColorsImage, 1, obj.nSubjects, 3));
                    imagesc(tempColorsImage);
                    drawnow;
                end
                if label(i) ~= 0
                    labeledMatched(currSeg) = label(i);
                else
                    labeledMatched(currSeg) = -1;
                end
            end
        end
    end
    %%
    h = img.hsv(:, :, 1);
    s = img.hsv(:, :, 2);
    v = img.hsv(:, :, 3);
    for l=unique(labeledMatched(labeledMatched ~= 0))'
        qh_ = histc(h(labeledMatched == l), bins)';
        qs_ = histc(s(labeledMatched == l), bins)';
        qv_ = histc(v(labeledMatched == l), bins)';
        if l>0
            colorHistVec{l} = [colorHistVec{l}; [qh_, qs_, qv_]];
        else
            bkgHistVec = [bkgHistVec; [qh_, qs_, qv_]];
        end
    end
    qh_ = histc(h, bins)';
    qs_ = histc(s, bins)';
    qv_ = histc(v, bins)';
    fillerHistVec = [fillerHistVec; [qh_, qs_, qv_]];
end

obj.Colors.Histrogram.H = zeros(obj.nSubjects, obj.nColorBins);
obj.Colors.Histrogram.S = zeros(obj.nSubjects, obj.nColorBins);
obj.Colors.Histrogram.V = zeros(obj.nSubjects, obj.nColorBins);
for l=1:length(colorHistVec)
    subjectHist = sum(colorHistVec{l}, 1) + 1;
    r = 0;
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.Histrogram.H(l, :) = subjectHist(r) / sum(subjectHist(r));
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.Histrogram.S(l, :) = subjectHist(r) / sum(subjectHist(r));
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.Histrogram.V(l, :) = subjectHist(r) / sum(subjectHist(r));
end

obj.Colors.BkgHistogram = [];
if ~isempty(bkgHistVec)
    obj.Colors.BkgHistogram = sum(bkgHistVec, 1) + 1;
    obj.Colors.BkgHistogram = obj.Colors.BkgHistogram / sum(obj.Colors.BkgHistogram);
end

obj.Colors.Image = zeros(obj.nSubjects, 3);
for l=1:length(colorVec)
    obj.Colors.Image(l, :) = mean(colorVec{l}, 1);
end
obj.Colors.Image = hsv2rgb(reshape(obj.Colors.Image, 1, obj.nSubjects, 3));
obj.Colors.Centers = reshape(obj.Colors.Image, obj.nSubjects, 3, 1);

obj.Colors.Bins = bins;
obj.Coordinates = coordinates;

%% Output to file
if obj.OutputToFile 
    if ~obj.OutputInOldFormat
        filename = [obj.OutputPath obj.FilePrefix '.meta.mat'];
        try
            fprintf('# - saving results to %s\n', label(i), x(i), y(i));
            save(filename, 'obj');
        catch me
            fprintf('#  . save failed: %s\n', me.message);
        end
    else
        meta = struct();
        meta.bkgFrame = obj.BkgImage;
        meta.subject.centerColors = obj.Colors.Centers;
        meta.subject.colorBins = obj.Colors.Bins;
        meta.subject.h = obj.Colors.Histrogram.H;
        meta.subject.s = obj.Colors.Histrogram.S;
        meta.subject.v = obj.Colors.Histrogram.V;
        meta.options = obj;
        filename = [obj.OutputPath obj.FilePrefix '.meta.mat'];
        try
            fprintf('# - saving in old meta file format to %s\n', filename);
            save(filename, 'meta');
        catch me
            fprintf('#  . save failed: %s\n', me.message);
        end
    end
end
