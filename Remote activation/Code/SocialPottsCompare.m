SocialExperimentData;
%%
weights = {};
for g=1:GroupsData.nGroups
    weights{g} = [];
end

for id=GroupsData.index
    %%
    g = GroupsData.group(id);
    obj = TrackLoad({id, 2});
    weights{g} = [weights{g}; obj.Analysis.Potts.Model{3}.weights];
end

%%
cmap = MySubCategoricalColormap;
cmap = cmap([2,4,6], :);
%%
stat = struct();
stat(1).mean = mean(weights{1}(1:5, :));
stat(2).mean = mean(weights{1}(6:10, :));
plot(stat(1).mean, stat(2).mean, '.', 'Color', cmap(1,:))
hon
%
stat = struct();
stat(1).mean = mean(weights{2}(1:4, :));
stat(2).mean = mean(weights{2}(5:8, :));
plot(stat(1).mean, stat(2).mean, '.', 'Color', cmap(3,:))
%
stat = struct();
for g=1:GroupsData.nGroups
    stat(g).mean = mean(weights{g});
    stat(g).stderr = stderr(weights{g});
end

plot(stat(1).mean, stat(2).mean, '.', 'Color', cmap(2,:))
xlabel('Complex weights');
ylabel('Standard weights');
hoff