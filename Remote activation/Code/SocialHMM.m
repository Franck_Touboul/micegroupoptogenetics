options.n = 2;
options.nSubjects = 4;           % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts
options.nIters = 500;
options.confidenceIters = 10;

%% Loading the Excel file for all mice (if needed)
loadData;
%%
