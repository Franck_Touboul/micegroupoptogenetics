d = dlmread('rtPCRHier.txt');
ids = unique(d(:, 3))';
for id=ids
    idx = d(:, 3) == id;
    [q, o] = sort(d(idx, 1));
    rank(o) = 1:obj.nSubjects;
    d(idx, 4) = rank;
end
%%
w = .1;

OTRRank.mean = [];
OTRRank.stderr = [];
for i=1:obj.nSubjects
    OTRRank.mean(i) = mean(d(d(:, 2) == i, 4));
    OTRRank.stderr(i) = stderr(d(d(:, 2) == i, 4));
end
cmap = MyCategoricalColormap;
bar(OTRRank.mean, 'facecolor', cmap(2, :))
hon;
%errorbar(1:obj.nSubjects, OTRRank.mean, OTRRank.stderr, zeros(1, obj.nSubjects), 'w.', 'LineWidth', 4)
%errorbar(1:obj.nSubjects, OTRRank.mean, zeros(1, obj.nSubjects), OTRRank.stderr, 'k.', 'LineWidth', 4)
%errorbar(1:obj.nSubjects, OTRRank.mean, OTRRank.stderr, zeros(1, obj.nSubjects), 'k.', 'LineWidth', 4)
errorbar(1:obj.nSubjects, OTRRank.mean, OTRRank.stderr, 'k.')
for i=1:obj.nSubjects
    vals = d(d(:, 2) == i, 4);
    for j=1:length(vals)
        plot(i - w + 2*w*(j - 1)/(length(vals) - 1), vals(j), 'ko', 'MarkerFaceColor', 'w', 'MarkerSize', 6, 'LineWidth', 1, 'MarkerEdgeColor', cmap(2, :));
    end
    
end
hoff;
xlabel('hierarchical level [ranked from most to least dominant]');
ylabel('Oxytocin Level [ranked from lowest to highest level]');

%%
OTRLevel.mean = [];
OTRLevel.stderr = [];

for i=1:obj.nSubjects
    OTRLevel.mean(i) = mean(d(d(:, 2) == i, 1));
    OTRLevel.stderr(i) = stderr(d(d(:, 2) == i, 1));
end

bar(OTRLevel.mean, 'facecolor', cmap(2, :))
hon;
errorbar(1:obj.nSubjects, OTRLevel.mean, OTRLevel.stderr, 'k.')
for i=1:obj.nSubjects
    vals = d(d(:, 2) == i, 1);
    for j=1:length(vals)
        plot(i, vals(j), 'ko', 'MarkerFaceColor', 'w', 'MarkerSize', 6, 'LineWidth', 1, 'MarkerEdgeColor', cmap(2, :));
    end
    
end
xlabel('hierarchical level [ranked from most to least dominant]');
ylabel('Oxytocin Level [au]');

hoff
