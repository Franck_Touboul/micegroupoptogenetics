function PottsCompare(id)
if ischar(id)
    id = str2double(id);
end
obj = TrackLoad('Res/SC.exp0001.day02.cam01.obj.mat');
%%
local = obj;
local.output = true;
local.count = obj.ROI.nZones;
local.nIters = 20000;
local.Statistics = true;
%%
data = zeros(obj.nSubjects, sum(obj.valid));
for s=1:obj.nSubjects
    currZones = obj.zones(s, :);
    data(s, :) = currZones(obj.valid == 1) - 1;
end
%%
local.tk = 1;
tid = mod(id, 10);
local.n = (id - tid) / 10;
switch tid
    case 1
        type = 'GIS';
        m = trainPottsModel(local, data);
    case 2
        type = 'GD';
        m = trainPottsModelUsingGD(local, data);
    case 3
        type = 'NGD';
        m = trainPottsModelUsingNesterovGD(local, data);
    case 4
        type = 'GISnNGD';
        m = trainPottsModelUsingNesterovGDandGIS(local, data);
end
%%
fprintf('@order=%f\n', local.n);
fprintf('@tk=%f\n', local.tk);
fprintf('@times='); fprintf(' %.100g', m.Statistics.times); fprintf('\n');
fprintf('@convergence='); fprintf(' %.100g', m.Statistics.convergence); fprintf('\n');
fprintf('@halfKL='); fprintf(' %.100g', m.Statistics.halfKL); fprintf('\n');
fprintf('@type='); fprintf(' %s', type); fprintf('\n');
fprintf('@date='); fprintf('%g ', datevec(now)); fprintf('\n');
