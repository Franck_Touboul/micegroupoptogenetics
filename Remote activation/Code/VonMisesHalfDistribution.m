function p = VonMisesHalfDistribution(x, m, v)
k = 1 ./ v;
b = besseli(0, k);
p1 = exp(k .* cos(x - m)) ./ ( 2 * pi * b);
p2 = exp(k .* cos(-x - m)) ./ ( 2 * pi * b);
p = (p1 + p2);
