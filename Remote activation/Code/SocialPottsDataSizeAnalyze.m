matpath = 'mat/SocialPottsDataSize/';
list = dir([matpath obj.FilePrefix '*']);

%%
data = [];
fields = {'order', 'probs' ,'nSamples', 'DjsByOrder', 'DjsByObserved'};
for f=1:length(fields)
    data.(fields{f}) = [];
end
data.id = [];
for i=1:length(list)
    src = load('-mat', [matpath list(i).name]);
    for f=1:length(fields)
        data.(fields{f}) = [data.(fields{f}), src.data.(fields{f})];
    end
    data.id = [data.id, i + ([src.data.order] * 0)];
end
%%
clf
s_ = unique(data.nSamples);
s_ = s_(s_ > 0);
for o=1:obj.nSubjects
    for s=s_
        %%
        map = data.nSamples == s & data.order == o;
        djs = data.DjsByObserved(map);
        if o == 1
            t = 0;
        else
            t = 0;
        end
        djs1 = djs(djs <= t);
        djs2 = djs(djs >= t);
        if ~isempty(djs1)
            subplot(obj.nSubjects, 2, 2*o-1);
            plot(s, djs1, '.','color', [0 0 1]); hon;
            title([num2ordinal(o) ' order']);
        end
        if ~isempty(djs2)
            subplot(obj.nSubjects, 2, 2*o);
            plot(s, djs2, '.', 'color', [0 0 1]); hon;
        end
    end
    hoff;
end
