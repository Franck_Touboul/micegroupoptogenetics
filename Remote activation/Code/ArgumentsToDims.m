function [dims, fields] = ArgumentsToDims(s)
fields = fieldnames(s);
dims = zeros(1, length(fields));
for i=1:length(fields)
    dims(i) = length(s.(fields{i}));
end
