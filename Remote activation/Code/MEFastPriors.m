function count = MEFastPriors(data, patterns)
MEPrint('Computing priors\n');
%%
[ndata, dim] = size(data);
nvals = max(patterns(:));
map = true(ndata, dim * nvals);
tic
for d=1:dim
    for v=1:nvals
        map(:, (d - 1) * nvals + v) = data(:, d) == v;
    end
end
%%
count = zeros(1, size(patterns, 1));
for i=1:size(patterns, 1)
    %%
    idx = find(~isnan(patterns(i, :)));
    smap = zeros(1, length(idx));
    k = 1;
    for j=idx
        smap(k) = (j - 1) * nvals + patterns(i, j);
        k = k + 1;
    end
    count(i) = sum(all(map(:, smap), 2));
end
