MinNumberOfEvents = 0;
%obj = TrackEnsure(obj, {'Analysis'});
img = imread('arena.png');
%%
obj.ROI.ZoneCenters = [
    320, 200;
    339, 56;
    55, 405;
    55, 177;
    778, 438;
    710, 513;
    494, 294;
    778, 184;
    698, 96;
    226, 294];
ZoneCenters = repmat(reshape(obj.ROI.ZoneCenters, [1, size(obj.ROI.ZoneCenters, 1), size(obj.ROI.ZoneCenters, 2)]), [obj.nSubjects, 1, 1]);

%%
model = obj.Analysis.Potts.Model{2};
weights = model.weights(model.order == 2);
weigths = sort(weights);
percentage = .075;
lthresh = weigths(round(length(weigths) * percentage));
uthresh = weigths(round(length(weigths) * (1-percentage)));
%%
clf;
seq = 1:obj.nSubjects;
cmap = MyMouseColormap;
MarkerSize = 5;
zMarkerSize = 30;
fontSize = 18;
LineWidth = 5;

percentage = .05;
weights = model.weights;
weights(model.order ~= 2) = nan;
nEvents = weights * 0;
if MinNumberOfEvents > 0
    for i=find(~isnan(weights))
        sid1 = model.labels{i}(1, 1);
        sid2 = model.labels{i}(1, 2);
        z1 = model.labels{i}(2, 1);
        z2 = model.labels{i}(2, 2);
        start = FindEvents(obj.zones(sid1, :) == z1 & obj.zones(sid2, :) == z2);
        if length(start) < MinNumberOfEvents
            weights(i) = nan;
        end
    end
end
[s, o] = sort(weights);
o = o(1:sum(~isnan(s)));
s = s(1:sum(~isnan(s)));

lthresh = s(round(length(s) * percentage));
lmax = s(1);

uthresh = s(round(length(s) * (1-percentage)));
umax = s(end);

lidx = o(s <= lthresh); lidx = lidx(end:-1:1);
uidx = o(s >= uthresh); %uidx = uidx(end:-1:1);
%%
for curr=1:obj.nSubjects
    SquareSubplpot(obj.nSubjects, curr)
    imshow(img); hold on; axis off;
    
    valid = [];
    marked = [];
    for idx = [lidx, uidx]
        %%
        r = [model.labels{idx}(2, 1), model.labels{idx}(2, 2)];
        sid1 = model.labels{idx}(1, 1);
        sid2 = model.labels{idx}(1, 2);
        sid = [sid1 sid2];
        if ~(curr == sid1 || curr == sid2)
            continue;
        end
        if r(1) == r(2)
            if model.weights(idx) >= uthresh
                fprintf('# pos-marked: location %d (between %d, %d)\n', r(1), model.labels{idx}(1,1), model.labels{idx}(1,2));
            else
                fprintf('# neg-marked: location %d (between %d, %d)\n', r(1), model.labels{idx}(1,1), model.labels{idx}(1,2));
            end
            marked = [marked, [r(1); sid(sid ~= curr); model.weights(idx)]];
        end
        valid = unique([valid, r]);
        for s=1:2
            frx = ZoneCenters(model.labels{idx}(1, s), r(s), 1);
            fry = ZoneCenters(model.labels{idx}(1, s), r(s), 2);
            tox = ZoneCenters(model.labels{idx}(1, 3-s), r(3-s), 1);
            toy = ZoneCenters(model.labels{idx}(1, 3-s), r(3-s), 2);
            if model.weights(idx) >= uthresh
                w = (model.weights(idx) - uthresh) / (umax - uthresh) * LineWidth + 1;
                plot([frx frx + (tox-frx)/2], [fry fry + (toy-fry)/2], '-', 'Color', cmap(model.labels{idx}(1, s), :), 'LineWidth', w);
            else
                w = (model.weights(idx) - lthresh) / (lmax - lthresh) * LineWidth + 1;
                plot([frx frx + (tox-frx)/2], [fry fry + (toy-fry)/2], ':', 'Color', cmap(model.labels{idx}(1, s), :), 'LineWidth', w);
            end
        end
    end
    zhist = histc(obj.zones(curr, :), 1:obj.ROI.nZones); zhist = zhist / sum(zhist);
    zhist = -.5 ./ log(zhist);
    for i=1:length(zhist)
        %plot(obj.ROI.ZoneCenters(i, 1), obj.ROI.ZoneCenters(i, 2), 'o', 'MarkerFaceColor', cmap(curr, :), 'MarkerEdgeColor', 'none', 'MarkerSize', zMarkerSize * zhist(i) / max(zhist));
    end
    %     valid = setdiff(valid, marked);
    %     out = valid(~issheltered(valid));
    %     in = valid(issheltered(valid));
    %     plot(ZoneCenters(1, out, 1), ZoneCenters(1, out, 2), 'o', 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'g', 'MarkerSize', markerSize);
    %     plot(ZoneCenters(1, in, 1), ZoneCenters(1, in, 2), 's', 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'g', 'MarkerSize', markerSize);
    %     for k=valid
    %         text(ZoneCenters(1, k, 1), ZoneCenters(1, k, 2), num2str(k), 'Color', 'b', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'fontsize', fontSize);
    %     end
    %
    for i=1:size(marked, 2)
        if marked(3, i) > 0
            plot(obj.ROI.ZoneCenters(marked(1, i), 1), obj.ROI.ZoneCenters(marked(1, i), 2), 'o', 'MarkerEdgeColor', cmap(marked(2, i), :), 'MarkerFaceColor', 'none', 'MarkerSize', MarkerSize * abs(marked(3, i)), 'LineWidth', 3);
        else
            plot(obj.ROI.ZoneCenters(marked(1, i), 1), obj.ROI.ZoneCenters(marked(1, i), 2), 'o', 'MarkerEdgeColor', cmap(marked(2, i), :), 'MarkerFaceColor', 'none', 'MarkerSize', MarkerSize * abs(marked(3, i)), 'LineWidth', 1.5);
        end
    end
    %     out = marked(~issheltered(marked));
    %     in = marked(issheltered(marked));
    %     plot(ZoneCenters(1, out, 1), ZoneCenters(1, out, 2), 'o', 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'r', 'MarkerSize', markerSize);
    %     plot(ZoneCenters(1, in, 1), ZoneCenters(1, in, 2), 's', 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'r', 'MarkerSize', markerSize);
    %     for k=marked
    %         text(ZoneCenters(1, k, 1), ZoneCenters(1, k, 2), num2str(k), 'Color', 'k', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'fontsize', fontSize);
    %     end
    %     %return;
    %     axis off;
    %     hoff;
end
