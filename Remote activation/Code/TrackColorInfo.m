cmap = MyMouseColormap;
channels = {'H', 'S', 'V'};
names = {'Hue (H)', 'Saturation (S)', 'Lightness (V)'};
for i=1:length(channels)
    for s=1:obj.nSubjects
        subplot(2,2,i);
        plot(obj.Colors.Bins, obj.Colors.Histrogram.(channels{i})(s, :)*100, 'color', cmap(s, :)); hon
    end
    plot(obj.Colors.Bins, obj.Colors.FillerHistogram.(channels{i})(s, :)*100, 'color', cmap(s, :)); hon
    xlabel(names{i});
    ylabel('Probability');
    hoff
end
