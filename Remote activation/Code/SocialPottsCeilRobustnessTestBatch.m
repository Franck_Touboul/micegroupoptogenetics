SocialExperimentData
for id=GroupsData.Standard.idx
    prefix = sprintf(experiments{id}, 2);
    fprintf('# -> %s\n', prefix);
    SocialPottsCeilRobustnessTest(['Res/' prefix '.obj.mat'], 1);
    SocialPottsCeilRobustnessTest(['Res/' prefix '.obj.mat'], 60);
end