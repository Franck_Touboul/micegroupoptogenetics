function MyWarning(me)
fprintf(['# Warning: ' me.message '\n']);
for j=1:length(me.stack)
    fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
end
