function obj = TrackFindBackground(obj)
obj = TrackLoad(obj);

fprintf('# finding background image\n');
cdata = uint8(zeros(obj.VideoHeight, obj.VideoWidth, 3, obj.nBkgFrames));
fprintf('# - using random frames\n');
i = 0;
n = Reprintf('# - frame %d of %d', i, obj.nBkgFrames);
while i < obj.nBkgFrames
    m1 = 1; %nFrames * 1/3;
    m2 = obj.nFrames; % * 2/3;

%     m1 = obj.nFrames * 1/3;
%     m2 = obj.nFrames * 2/3;
    
    r = floor(rand(1) * (m2 - m1) + m1);
    if r * obj.dt < obj.StartTime; continue; end
    if obj.EndTime > 0 && r * obj.dt > obj.EndTime; continue; end
    frame = myMMReader(obj.VideoFile, r);
    if ~isempty(frame) && TrackIsDark(obj, frame, .2)
        i = i +1;
        n = Reprintf(n, '# - frame %d of %d', i, obj.nBkgFrames);
        subplot(1,2,2); imagesc(frame); title('accepted');
        drawnow;
        cdata(:,:,:,i) = frame;
    else
        subplot(1,2,1); imagesc(frame); title('rejected');
        drawnow;
    end
end
fprintf('\n');
obj.BkgImage = uint8(median(double(cdata), 4));
if obj.Output
    imagesc(obj.BkgImage);
end
