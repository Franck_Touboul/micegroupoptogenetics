function obj = TrackRegister(obj, prototype)
if nargin == 1
    %%
    meta = regexp(obj.VideoFile, '(?<path>.*[\\\/])?(?<group>.*)\.exp(?<number>[0-9]*)\.day(?<day>[0-9]*)\.cam(?<camera>[0-9]*)(?<ext>.*)', 'names');
    fprintf(['#    group: ' meta.group '\n']);
    TrackDefaults = TrackGetDefaults(meta.group, str2double(meta.camera));
    prototype = TrackLoad(TrackDefaults.Prototype, {'BkgImage', 'ROI'}); 
end
%%
transform='affine';

if strcmp(transform,'affine')
    % warp example for affine case
    init=[eye(2) zeros(2,1)];
end


if strcmp(transform,'homography')
    % warp example for homography case
    init=eye(3);
end

%%
img1 = rgb2gray(obj.BkgImage);
img2 = rgb2gray(prototype.BkgImage);
e1 = img1 > median(img1(:));
e2 = img2 > median(img2(:));
results = ecc(e1, e2, 1, 35, transform, init);
%%
obj.ROI = prototype.ROI;
for i=1:obj.ROI.nRegions
    img = spatial_interp(prototype.ROI.Regions{i}, results(end).warp, 'linear', transform, results(end).nx, results(end).ny) > .5;
    dh = obj.VideoHeight - size(img,1);
    dw = obj.VideoWidth - size(img,2);
    img = padarray(img, [floor(dh/2) floor(dw/2)], 'post');
    obj.ROI.Regions{i} = padarray(img, [ceil(dh/2) ceil(dw/2)], 'pre');
end
%%
TrackShowAllRegions(prototype)