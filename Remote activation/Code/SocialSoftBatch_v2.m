%%function SocialSoftBatch_v2(func)
experiments = {...
    'Enriched.exp0006.day%02d.cam01',...
    'Enriched.exp0005.day%02d.cam04',...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam01',...
    'SC.exp0007.day%02d.cam04',...
    };

E.map = logical([1 1 1 0 1 0 1 0 1 0 0 0 0]);
E.idx = find(E.map);

SC.map = ~E.map;
SC.idx = find(SC.map);

nDays = 4;
%%
X = [];
Y = [];
Z = [];
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        obj = TrackLoad(['Res/' prefix '.obj.mat'], {'x', 'y', 'zones'});
        X = [X, obj.x(:)'];
        Y = [Y, obj.y(:)'];
        Z = [Z, obj.zones(:)'];
    end
end
%%
%obj2 = TrackLoad(['../base/Res/' prefix '.obj.mat']);
% obj1 = TrackLoad('../base/Res/SC.exp0001.day02.cam01.obj.mat');
% obj2 = TrackLoad('../base/Res/Enriched.exp0001.day02.cam04.obj.mat');
%hist3([X', Y'], [10 20]);
[h, x] = hist3([X', Y'], [100 200]);
%%
[qx, qy] = meshgrid(x{1}, x{2});
m=10000; 
q = h; 
q(q>m) = m; 
subplot(1,2,1); 
contourf(qx, size(obj1.BkgImage,1)-qy, q', 'LineColor', 'none'); 

colorbar
%%
subplot(1,2,2); 
imagesc(obj1.BkgImage); 
hold on;
contour(qx, qy, q'); 
hold off
colormap(MyDefaultColormap(255));
