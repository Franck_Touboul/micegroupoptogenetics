classdef MyVideoReader
    properties
        Filename = '';
        FrameRate = 0;
        Height = 0;
        Width = 0;
        NumberOfFrames = 0;
        RealNumberOfFrames = 0;
        Duration = 0;
    end
    
    properties (Dependent)
        FrameNumber = 0;
        Time = 0;
    end
    properties (Access=private)
        Time_ = 0;
        Buffer_ = [];
        BufferFrameRange_ = [];
        BufferSize_ = 5;
    end
    
    methods
        function obj = MyVideoReader(varargin)
            if nargin < 1
                error 'Not enough input arguments.'
            end
            switch class(varargin{1})
                case 'char'
                    obj.Filename = varargin{1};
                case 'struct'
                    obj.Filename = varargin{1}.VideoFile;
                otherwise
                    error(['MyVideoReader cannot except input argument of type ' class(varargin{1})]);
            end
        end
        
        function obj = set.FrameNumber(obj, value)
            obj.Time_ = value / obj.FrameRate;
            obj = UpdateBuffer(obj);
        end

        function value = get.FrameNumber(obj)
            value = max(round(obj.Time_ * obj.FrameRate + 1), 1);
        end

        function obj = set.Time(obj, value)
            obj.Time_ = value;
            obj = UpdateBuffer(obj);
        end
        
        function value = get.Time(obj)
            value = obj.Time_;
        end
        
        function obj = set.Filename(obj, value)
            obj.Filename = value;
            meta = mmread(obj.Filename, 1);
            obj.Height = meta(end).height;
            obj.Width = meta(end).width;
            obj.FrameRate = ceil(meta(end).rate);
            obj.Duration = meta(end).totalDuration;
            obj.NumberOfFrames = obj.FrameRate * meta(end).totalDuration;
            obj.RealNumberOfFrames = abs(meta(end).nrFramesTotal);
        end

        function obj = set.Height(obj, value)
            obj.Height = value;
        end
        
        function frame = CurrentFrame(obj)
            currFrame = obj.FrameNumber;
            obj = UpdateBuffer(obj);
            num = currFrame - obj.BufferFrameRange_(1) + 1;
            if length(obj.Buffer_) >= num
                frame = obj.Buffer_(num).cdata;
            else
                frame = zeros(obj.Height, obj.Width, 3);
            end
            
        end
        
        function Show(obj)
            imagesc(obj.CurrentFrame);
            axis off;
        end
            
        function Play(obj)
            h = tic;
            rt = 0;
            rtfactor = 0.95;
            for i=1:obj.NumberOfFrames
                obj.FrameNumber = i;
                imagesc(obj.CurrentFrame);
                time = toc(h);
                if rt == 0
                    rt = obj.Time/time;
                else
                    rt = rtfactor * rt + (1 - rtfactor) * obj.Time/time;
                end
                title(sprintf('%d (x%.1f RT)', i, rt));
                axis off;
                drawnow;
            end
        end
    end
    
    methods (Access = protected)
        function obj = UpdateBuffer(obj)
            currFrame = obj.FrameNumber;
            if ~isempty(obj.Buffer_) && ...
                    obj.BufferFrameRange_(1) <= currFrame && ...
                    obj.BufferFrameRange_(2) >= currFrame
            else
                meta = mmread(obj.Filename, [], [obj.Time_ obj.Time_+(obj.BufferSize_ + .5)/obj.FrameRate]);
                if isempty(meta(end).frames)
                    pause(.2);
                    meta = mmread(obj.Filename, [], [obj.Time_ obj.Time_+(obj.BufferSize_ + .5)/obj.FrameRate]);
                end
                if ~isempty(meta(end).frames)
                    obj.Buffer_ = meta(end).frames;
                    obj.BufferFrameRange_ = [currFrame, currFrame + length(obj.Buffer_) - 1];
                else
                    obj.Buffer_ = [];
                    obj.BufferFrameRange_ = [-1 -1];
                end
            end
        end
    end
end