function n = Reprintf(varargin)
if isnumeric(varargin{1})
    prev = varargin{1};
    for i=1:prev
        fprintf('\b');
    end
    s = sprintf(varargin{2:end});
else
    s = sprintf(varargin{:});
end
n = length(s);
s = regexprep(s, '%', '%%');
fprintf(s);