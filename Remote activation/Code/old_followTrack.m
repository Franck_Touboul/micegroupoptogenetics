function res = followTrack(options, cents, curr)

[N, nS] = size(cents.label);
%%
options.jumpVar = eye(2) * 5^2;
options.confProb = 0.1;
options.hideProb = 0.01;

table = zeros(N, nS+1);
table(cents.label == curr) = 1;
table(cents.label ~= curr) = options.confProb;
table(cents.label == 0)    = options.hideProb;
table(:, nS+1) = options.hideProb;
table = flog(table');

path = zeros(nS+1, N, 'uint8');
% table(:, 1) = table(:, 1) .* model.start(:);
lastPos = [inf, inf];
for i=2:N
    for j=1:nS
        p = gauss([cents.x(i, j), cents.y(i, j)], options.jumpVar, [cents.x(i-1, 1:nS) lastPosition(1); cents.y(i-1, 1:nS) lastPosition(2)]');
        %p(end) = 2 * p(end);
        p(end) = 0;
        [val, from] = max(flog(p) + table(:, i-1));
        path(j, i) = from;
    end
    [val, from] = max(table(:, i-1));
    path(nS + 1, i) = from;
    if from <= nS
        lastPos = [cents.x(i-1, from), cents.y(i-1, from)];
    end
end
% backtrack
backtrack = zeros(1, N);
[val, start] = max(table(:, end));
backtrack(end) = start;
res.x = zeros(1, N);
res.y = zeros(1, N);

res.x(end) = cents.x(end, backtrack(end));
res.y(end) = cents.y(end, backtrack(end));
for i=N-1:-1:1
    backtrack(i) = path(backtrack(i+1), i + 1);
    if backtrack(i) <= nS
        res.x(i) = cents.x(i, backtrack(i));
        res.y(i) = cents.y(i, backtrack(i));
    else
        res.x(i) = nan;
        res.y(i) = nan;
    end
end

