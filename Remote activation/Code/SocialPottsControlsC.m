function SocialPottsControlsC(obj)
obj = TrackLoad(obj);
%%
Options.EvenOddTimescales = [1 60];
obj.OutputToFile = false;
obj.OutputInOldFormat = false;
obj.Analysis.Potts.nIters = [2000 20000];
obj.Analysis.Potts.MinNumberOfIters = [500 1000];
obj.Analysis.Potts.Confidence = 0.05;
%%
oidx = 1;
objs = {};
%% half-half
try
    first = (1:obj.nFrames) < obj.nFrames / 2;
    second = ~first;
    %
    Types = {'first', 'second'};
    for i=1:length(Types);
        map = eval(Types{i});
        curr = obj;
        curr.zones = obj.zones(:, map);
        curr.valid = obj.valid(:, map);
        curr.nFrames = sum(map);
%        curr.me = TrainPottsModel(curr, 3);
        curr = SocialPotts(curr);
        objs{oidx}.(Types{i}) = curr;
    end
    oidx = oidx + 1;
catch me
    warning(me.message);
end
%%
save('SocialPottsControlsC', 'objs', '-v7.3');

