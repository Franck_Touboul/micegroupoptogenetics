function objs = SocialPottsControlsParameterStatistics(objs)

% SocialGroupCollect(objs, {...
%     'Analysis.PottsControl.Ik', ...
%     'Analysis.PottsControl.In', ...
%     'Analysis.PottsControl.Entropy', ...
%     'Analysis.PottsControl.Resolution'});

[objs, data, first] = SocialParseName(objs);

%%
clf
tests = fieldnames(objs);

cmap = jet(length(tests));
style = {'LineWidth', 2, 'MarkerSize', 10};

for i=1:length(tests)
    obj = objs.(tests{i});
    potts = obj.Analysis.PottsControl;
    new = true;
    for j=unique(potts.Resolution)
        curr = potts.Resolution == j;
        nframes = obj.nFrames / j;
        if new
            semilogx(nframes, potts.In(curr), '.', 'Color', cmap(i, :), style{:}); hold on;
        else
            semilogx(nframes, potts.In(curr), '.', 'Color', cmap(i, :), style{:}, 'HandleVisibility', 'off'); hold on;
        end
        for k=1:first.nSubjects-1
        %    plot(j, potts.In(curr', k), 'k.'); hold on;
        end
        new = false;
    end
end
prettyPlot('multi-information', '# frames', 'information [bits]');
legend(tests);
legend boxoff;

