function [obj, img] = TrackSimpleSegmentFrame(obj, frame)
%%
if ischar(obj)
    load(obj);
end

if isscalar(frame)
    img.orig = myMMReader(obj.VideoFile, frame);
else
    img.orig = frame;
end
%%
try
    m = imsubtract(img.orig, obj.BkgImage);
    m = imresize(m, obj.VideoScale);
    %%
    img.hsv = rgb2hsv(m);
    vm = img.hsv(:,:,3);
    %%
    meanBKG = mean(vm(:));
    stdBKG = std(vm(:));
    if obj.UseAdaptiveThresh
        upper = obj.NoiseThresh;
        lower = 1;
        prev_thresh = round((upper + lower)/2);
        while true
            thresh = round((upper + lower)/2);
            bw = vm > meanBKG + thresh * stdBKG;
            if ~isempty(obj.ValidROI)
                bw(~obj.ValidROI) = false;
            end
            cc = bwconncomp(bw);
            if cc.NumObjects < obj.MaxNumOfObjects
                upper = thresh - 1;
                prev_thresh = thresh;
            else
                lower = thresh + 1;
            end
            if lower > upper
                break
            end
        end
        if thresh ~= prev_thresh
            thresh = prev_thresh;
            bw = vm > meanBKG + thresh * stdBKG;
        end
    else
        thresh = obj.NoiseThresh;
        bw = vm > meanBKG + thresh * stdBKG;
        cc = bwconncomp(bw);
        if cc.NumObjects > obj.MaxNumOfObjects
            return;
        end
    end
    img.boundries = bwareaopen(bw, obj.MinNumOfPixels);
    img.segmented = imerode(img.boundries, strel('disk', obj.ErodeRadios));
catch
    img = [];
    if isscalar(frame)
        fprintf('# - unable to segmente video frame no. %d', frame);
    else
        fprintf('# - unable to segmente video frame');
    end
end