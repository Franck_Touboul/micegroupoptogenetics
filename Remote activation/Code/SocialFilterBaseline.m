function obj = SocialFilterBaseline(obj)
obj = TrackLoad(obj);
fobj = SocialFilterRepeats(obj);
[obj.Analysis.Potts.Filter.Ik, obj.Analysis.Potts.Filter.Entropy] = SocialFastMultiInformation(fobj);
%%
options.nIters = 50;
obj.Analysis.Potts.Filter.Baseline = [];
obj.Analysis.Potts.Filter.Baseline.Ik = zeros(obj.nSubjects-1, options.nIters);
obj.Analysis.Potts.Filter.Baseline.Entropy = zeros(obj.nSubjects-1, options.nIters);
i = 1;
while i<options.nIters
    curr = fobj;
    curr = SocialSetZones(curr, fobj.zones(:, fobj.valid));
    zones = curr.zones;
    try
        fprintf('-- %3d ------------------------------------------------------------------\n', i);
        for s=1:obj.nSubjects
            r = randi(curr.nFrames);
            zones(s, :) = [zones(s, r:end) zones(s, 1:r-1)];
        end
        curr = SocialSetZones(curr, zones);
        [Ik, Entropy] = SocialFastMultiInformation(curr);
        obj.Analysis.Potts.Filter.Baseline.Ik(:, i) = Ik(:);
        obj.Analysis.Potts.Filter.Baseline.Entropy(:, i) = Ik(:);
        i = i + 1;
    catch
        fprintf('# failed!!!!!\n');
    end
end
if obj.OutputToFile
    obj = TrackSave(obj);
end