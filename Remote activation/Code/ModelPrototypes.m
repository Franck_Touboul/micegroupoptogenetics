%%
clf
Scale.BodyYaw = (-8:2:8)/180*pi;
Scale.HeadYaw = (-16:2:16)/180*pi;
Scale.Rotation = (0:20:360)/180*pi;
Scale.nPixels = 500;
Scale.nPixelsTol = .1;
Scale.Dim = [20, 20];
Scale.nPixesIters = 20;

idx = 1;
num = 1;
rpc = 0;
count = length(Scale.BodyYaw) * length(Scale.HeadYaw) * length(Scale.Rotation);
Prototypes = struct();
Prototypes(count).BodyYaw = 0;
Prototypes(count).HeadYaw = 0;
Prototypes(count).Rotation = 0;
Prototypes(count).Img = [];

for BodyYaw=Scale.BodyYaw
    for HeadYaw = Scale.HeadYaw
        for Rotation = Scale.Rotation
            rpc = reprintf(rpc, '# prototype no. %d/%d', num, count);
            [x,y,z] = ModelBasic(BodyYaw, HeadYaw, Rotation);
            nPix = sum(z(:) > 0);
            Z = z;
            scale = sqrt(Scale.nPixels/sum(z(:) > 0));
            for i=1:Scale.nPixesIters
                Z = imresize(z, scale);
                nPix = sum(Z(:) > 0);
                if nPix > Scale.nPixels + Scale.nPixels * Scale.nPixelsTol
                    scale = scale / 1.5;
                elseif nPix < Scale.nPixels - Scale.nPixels * Scale.nPixelsTol
                    scale = scale * 1.5;
                else
                    break;
                end
            end
            z = Z;
            %%
%             r = regionprops(z > 0);
%             [~, cid] = max([r.Area]);
%             cent = r(cid).Centroid;
            %plot3(x,y,z,'x');
            %%
            if rand < 0.1
                %%
                %subplot(10,10,mod(idx,100)+1);
                %%
                imagesc(z);
                axis equal
                axis off;
                idx = idx + 1;
                drawnow;
            end
            Prototypes(num).BodyYaw = BodyYaw;
            Prototypes(num).HeadYaw = HeadYaw;
            Prototypes(num).Rotation = Rotation;
            Prototypes(num).Img = z;
            num = num + 1;
        end
    end
end

