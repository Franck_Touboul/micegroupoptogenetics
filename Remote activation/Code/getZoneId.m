function [id, sheltered] = getZoneId(obj, zone)
sheltered = false;
if (zone(1) == '(' && zone(end) == ')')
    sheltered = true;
end
for i=1:length(obj.ROI.ZoneNames)
    if strcmpi(zone,obj.ROI.ZoneNames{i})
        id = i;
        return;
    end
end

error('zone id not found');
id = 0;