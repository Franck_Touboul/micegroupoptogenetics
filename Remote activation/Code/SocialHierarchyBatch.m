SocialExperimentData;
%%
ranks = [];
nLevels = [];
days = [];
ids = [];
strength = [];
maps = [];
removed = [];
interactions = [];
corrupt = {};
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        try
            obj = TrackLoad([obj.OutputPath prefix '.obj.mat'], {'Hierarchy'});
        catch me
            MyWarning(me)
            corrupt = {corrupt{:}, prefix};
            continue;
        end
        if ~isfield(obj, 'Hierarchy') || ~isfield(obj.Hierarchy.ChaseEscape, 'removed')
            obj = TrackLoad([obj.OutputPath prefix '.obj.mat']);
            obj = SocialHierarchy(obj);
        end
        days = [days, day];
        ids = [ids, id];
        ranks = [ranks; obj.Hierarchy.ChaseEscape.rank];
        nLevels = [nLevels, obj.Hierarchy.ChaseEscape.nLevels];
        strength = [strength, obj.Hierarchy.ChaseEscape.strength];
        removed = cat(3, removed, obj.Hierarchy.ChaseEscape.removed);
        interactions = cat(3, interactions, obj.Hierarchy.ChaseEscape.interactions);
        maps = cat(3, maps, obj.Hierarchy.ChaseEscape.map);
        
    end
end
corrupt
%%
hierarchy = [];
hierarchySame = [];
hierarchyDiff = [];
for id=1:length(experiments)
    pmat = [];
    for day = 1:nDays
        idx = find(ids == id & days == day);
        mat = interactions(idx).PostPredPrey - interactions(idx).PostPredPrey';
        mat(binotest(interactions(idx).PostPredPrey, interactions(idx).PostPredPrey + interactions(idx).PostPredPrey')) = 0;
        mat = sign(mat);
        hierarchy(:, :, idx) = mat;
        if day > 1
            eq = (mat == pmat) & (mat ~= 0 | pmat ~= 0);
            neq = (mat ~= pmat) & (mat ~= 0 | pmat ~= 0);
            hierarchySame(idx) = sum(eq(:));
            hierarchyDiff(idx) = sum(neq(:));
        else
            hierarchySame(idx) = 0;
            hierarchyDiff(idx) = 0;
        end
        pmat = mat;
    end
end
hierarchyChange = hierarchySame ./ (hierarchySame + hierarchyDiff);
hierarchyChange(isnan(hierarchyChange)) = 0;
%%
clf
property = {'nLevels', 'strength', 'hierarchyChange'};
w = length(property);
for currp = 1:length(property)
    style = {'linewidth', 2};
    for g=1:length(Groups)
        data = [];
        i = 1;
        for id=Groups(g).idx
            for day=1:nDays
                idx = ids == id & day == days;
                data(i, day) = evalin('base', [property{currp} '(idx)']);
            end
            i = i + 1;
        end
        subplot(w,3,(currp-1)*3+1:(currp-1)*3+2);
        errorbar(1:nDays, mean(data), stderr(data), style{:}, 'Color', Groups(g).color); hon;
        if currp == 1
            yaxis(0, 4);
        elseif currp == 2
            yaxis(0, 5);
        else
            yaxis(0, 1);
        end
        title(property{currp});
        subplot(w,3,(currp-1)*3+3);
        MyBar(g, mean(data(:)), stderr(data(:)), Groups(g).color); hon;
    end
    subplot(w,3,(currp-1)*3+1:(currp-1)*3+2);
    set(gca, 'XTick', 1:nDays);
    xlabel('day');
    subplot(w,3,(currp-1)*3+3);
    set(gca, 'XTick', 1:length(Groups));
    xaxis(.2, GroupsData.nGroups + .8);
    set(gca, 'XTickLabel', GroupsData.Initials);
end
return;
%%
W = [];
corrupt = {};
index = 1;
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        obj = TrackLoad(['../base/Res/' prefix '.obj.mat'], {'Analysis'});
        if isfield(obj.Analysis, 'Potts')
            me = obj.Analysis.Potts.Model{2};
            if isempty(W)
                W = nan(length(experiments), length(me.weights));
            end
            W(index, :) = me.weights;
        else
            corrupt = {corrupt{:}, prefix};
            
        end
        index = index + 1;
    end
end
W(all(W == 0, 2), :) = nan;
%%
me = obj.Analysis.Potts.Model{2};
iMap = false(obj.nSubjects, length(me.weights));
for s=1:obj.nSubjects
    w = [];
    for i=1:length(me.labels)
        if any(me.labels{i}(1,:) == s) && size(me.labels{i}, 2) == 2
            iMap(i) = true;
        end
    end
end
%%
for id=1:length(experiments)
    m = ids == id;
    arank = mean(ranks(m, :));
    for s=1:obj.nSubjects
        w = W(m, iMap(s, :));
        plot(arank(s), mean(w(:)), 'x'); hold on;
    end
end
%%
groups = [];
for id=1:length(experiments)
    for day = 1:nDays
        group = 1;
        for g=1:length(Groups)
            if any(id == Groups(g).idx)
                group = g;
                break;
            end
        end
        groups = [groups, group];
    end
end
%%
mean(strength(groups == 1))
mean(nLevels(groups == 2))
return;
%%

W = {};
for i=1:obj.nSubjects
    W{i} = [];
end
index = 1;
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        obj = TrackLoad(['../base/Res/' prefix '.obj.mat'], {'Analysis'});
        if isfield(obj.Analysis, 'Potts')
            me = obj.Analysis.Potts.Model{2};
            for s=1:obj.nSubjects
                w = [];
                for i=1:length(me.labels)
                    if any(me.labels{i}(1,:) == s)
                        w = [w, me.weights(i)];
                    end
                end
                W{ranks(index, s)} = [W{ranks(index, s)}, w];
            end
        end
        index = index + 1;
    end
end
%%


return;
%%
SocialExperimentData;
output = false;
rankChange = zeros(length(experiments), nDays-1);
levelCount = zeros(length(experiments), nDays);
hierarchyStrength = zeros(length(experiments), nDays);
output = false;
rank = zeros(1, obj.nSubjects);
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        obj = TrackLoad([obj.OutputPath prefix '.obj.mat'], {'zones', 'Interactions'});
        %obj = SocialAnalysePredPreyModel(obj);
        %%
        curr = obj.Interactions.PredPrey;
        mat = curr.PostPredPrey - curr.PostPredPrey';
        mat(binotest(curr.PostPredPrey, curr.PostPredPrey + curr.PostPredPrey')) = 0;
        mat = mat .* (mat > 0);
        
        prevRank = rank;
        
        [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
        
        if day > 1
            r1 = max(rank) - rank;
            r2 = max(prevRank) - prevRank;
            rankChange(id, day-1) = sum(r2 - r1);
        end
        levelCount(id, day) = max(rank) - min(rank);
        hierarchyStrength(id, day) = sum(mat(:) ~= 0);
        if output
            g = biograph.bggui(bg);
            f = get(g.biograph.hgAxes, 'Parent');
            ppfile = [obj.OutputPath 'predprey.' prefix '.SocialHierarchyBatch.png'];
            print(f, ppfile, '-dpng');
            close(g.hgFigure);
        else
            %%
            [q, bg] = SocialGraph(obj, mat, removed, rank);
            g = biograph.bggui(bg);
            fig2 = get(g.biograph.hgAxes,'children');
            figure(1)
            h = subplot(4,2,(day-1)*2+1);
            cla(h)
            a1 = gca;
            copyobj(fig2, a1)
            axis off;
            set(gcf, 'Color', 'w');
            title(['Day ' num2str(day)]);
            close(g.hgFigure);
            
            h = subplot(4,2,(day-1)*2+2);
            imagesc(curr.PostPredPrey);
            for i=1:obj.nSubjects
                for j=1:obj.nSubjects
                    text(j, i, num2str(curr.PostPredPrey(i, j)), 'color', 'w', 'verticalAlignment', 'middle', 'horizontalAlignment', 'center');
                end
            end
        end
    end
    if ~output
        disp 'press any key...'
        pause
    end
end
%%
mean(rankChange(E.idx, :))

