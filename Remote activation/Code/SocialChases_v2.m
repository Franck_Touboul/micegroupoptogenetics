function [events, state] = SocialChases_v2(obj, m1, m2)
%%
fprintf('# finding ''Chases''\n');
nIters = 10;
obj.Interactions.MinEventDuration = 8;
obj.MaxShelteredContanctDuration = 25;
%%
obj = TrackLoad(obj);
obj = SocialFindInteractions(obj);

%%
invalid = obj.sheltered(m1, :) | obj.sheltered(m2, :);
[start, finish, len] = FindEvents(invalid);
for i=find(len <= obj.MaxShelteredContanctDuration)
    invalid(start(i):finish(i)) = 0;
end

%%
model = GeneratePerdPreyModel(5, 1, 5, 1);

currAngles = obj.angle{m1, m2};
currContacts = obj.contact{min(m1, m2), max(m1, m2)};
input = [...
    currAngles;
    currContacts
    ];
%valid = ~obj.hidden(m1, :) & ~obj.hidden(m2, :);

[begF, endF] = FindEvents(~invalid);
inputs = cell(1, length(begF));
for i=1:length(begF)
    inputs{i} = [input(1, begF(i):endF(i)); input(2, begF(i):endF(i))];
end

%%
nBins = 100;
allbins = sequence(0, pi, nBins + 1);
bins = allbins(1:nBins);
dbin = bins(2) - bins(1);

count = Reprintf('# - iter %d/%d', 0, nIters);
for iter=0:nIters
    count = Reprintf(count, '# - iter %d/%d', iter, nIters);
    backtrack = ModelViterbiSequence(model, inputs, false);
    
    state = ones(1, obj.nFrames);
    for i=1:length(begF)
        currBacktrack = backtrack{i};
        %currBacktrack(currBacktrack == 1 & currContacts(begF(i):endF(i)) == 1) = 4;
        state(begF(i):endF(i)) = currBacktrack;
    end
    
    %%
    if iter < nIters
        for s=1:length(model.states)
            if isfield(model.parameters, model.states(s).title)
                currParameters = model.parameters.(model.states(s).title);
                x = histc(obj.angle{m1,m2}(state == s), allbins);
                x = x(1:nBins);
                p = x / sum(x) / dbin;
                
                func = model.states(s).func;
                b = nlinfit(bins, p, @(b, X) func(X, AssignParametersToMode(model.states(s).title, b)), currParameters, statset('FunValCheck', 'off'));
                model.parameters.(model.states(s).title) = b;
            end
        end
    end
end
fprintf('\n');
statesBackup = state;
%% remove non contact events
[start, finish] = FindEvents(state > 1);
for s=1:length(model.states)
    if strcmp(model.states(s).title, 'cont')
        for i=1:length(start)
            if ~any(state(start(i):finish(i)) == s)
                state(start(i):finish(i)) = 1;
            end
        end
    end
end

%% remove short events
for i=2:model.nstates
    if model.states(i).type == 1
        c = conv([0 (state==i)  0], [1 -1]);
        startFrame = find(c > 0) - 1;
        endFrame   = find(c < 0) - 2;
        duration = endFrame - startFrame + 1;
        for l=find(duration < obj.Interactions.MinEventDuration)
            state(startFrame(l):endFrame(l)) = 1;
        end
    end
end

%%
[start, finish, len] = FindEvents(state~=1);
epochs = cell(1, length(start));
for i=1:length(start)
    prev = -1;
    index = 1;
    for f=start(i):finish(i)
        if state(f) ~= prev 
            epochs{i}.title{index} = model.states(state(f)).title;
            epochs{i}.start(index) = f;
            if index > 1
                epochs{i}.finish(index-1) = f-1;
            end
            prev = state(f);
            index = index + 1;
        end
    end
    epochs{i}.finish(index-1) = finish(i);
end

% %%
% c = conv([0 (state~=1)  0], [1 -1]);
% events.startFrame = find(c > 0) - 1;
% events.endFrame   = find(c < 0) - 2;
% events.startTime  = events.startFrame * obj.dt;
% events.endTime    = events.endFrame * obj.dt;
% 
% for i=1:length(events.startFrame)
%     prev = -1;
%     events.title{i} = '';
%     index = 1;
%     for j=events.startFrame(i):events.endFrame(i)
%         if prev ~= state(j);
%             if index == 1;
%                 events.title{i} = model.states(state(j)).title;
%                 events.epoch{i}.startFrame(index) = j;
%             else
%                 events.title{i} = [events.title{i} ' -> ' model.states(state(j)).title];
%                 events.epoch{i}.startFrame(index) = j;
%                 events.epoch{i}.endFrame(index-1) = j - 1;
%             end
%             events.epoch{i}.title{index} = model.states(state(j)).title;
%             index = index + 1;
%             prev = state(j);
%         end
%     end
%     events.epoch{i}.endFrame(index-1) = events.endFrame(i);
% end

%%
% f = @(x, angle, beta) (x < angle) * (1 - (pi-angle) / pi * beta * sin(angle)) / angle + (x >= angle) * beta * sin(angle) / pi;
% dx = 0.01;
% x=0:dx:pi; 
% clf
% cmap = lines;
% index = 1;
% entries = {};
% for a=0:pi/6:pi
%     beta = .5;
%     plot(x, f(x, a, beta), 'color', cmap(index, :));
%     hold on;
%     sum(f(x, a, beta)) * dx
%     entries{index} = num2str(a);
%     index = index + 1;
% end
% legend(entries);