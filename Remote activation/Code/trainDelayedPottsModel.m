function me = trainDelayedPottsModel(options, data)
n = 2;
use_sparse = true;
if isfield(options, 'output')
    output = options.output;
else
    output = true;
end
%
if output; fprintf('# Training Potts Model\n'); end
%
if output; fprintf('# - finding all permutation of max dimension %d...\n', n); end;
allPerms = nchoose(1:options.nSubjects);
nPerms = {};
j = 1;
for i=1:length(allPerms)
    if length(allPerms{i}) <= n
        nPerms{j} = allPerms{i};
        j = j + 1;
        if length(allPerms{i}) == n
            nPerms{j} = allPerms{i}(end:-1:1);
            j = j + 1;
        end
    end
end

if output; fprintf('# - computing features...\n'); end;
%[me.features, me.labels] = assignDelayedFeature(data' + 1, options.ROI.nZones, nPerms, use_sparse, 12);
[me.features, me.labels] = assignFeature(data' + 1, options.ROI.nZones, nPerms, use_sparse);
[me.features2, me.labels2] = assignFeature2(data' + 1, options.ROI.nZones, nPerms, use_sparse);

%
me.inputPerms = myPerms(options.nSubjects, options.count);
me.perms = assignFeature(me.inputPerms, options.count, nPerms, use_sparse);
%
if output; fprintf('# - computing constraints...\n'); end
me.constraints = full(mean(me.features));
me.nSubjects = options.nSubjects;
me.range = options.count;
% initialize the weights
me.weights = rand(1, size(me.features, 2)) / size(me.features, 2);
%
C = unique(sum(me.features, 2));
if length(C) > 1
    error 'number of features in each line most be consistent'
end

loglikelihood = 0;
for iter=1:options.nIters
    if output; fprintf('# - iter (%4d/%4d) : ', iter, options.nIters); end
    % compute the (un-normalized) pdf for each sample
    p = exp(me.features * me.weights');
    % normalize the pdf (the Z)
    perms_p = exp(me.perms * me.weights');
    Z = sum(perms_p);
    p = p / Z;
    %
    E = sum(me.perms .* repmat(perms_p / Z, 1, size(me.perms, 2)));
    dw = log(me.constraints ./ E);
    %dw = log(me.constraints ./ E');
    dw(me.constraints == 0) = 0;
    me.weights = me.weights + 1/C * dw;
    prev_loglikelihood = loglikelihood;
    loglikelihood = mean(log(p));
    if output; fprintf('mean log-likelihood = %6.4f (%.3f)\n', loglikelihood, abs((loglikelihood-prev_loglikelihood)/prev_loglikelihood)); end
end
