curr = 1;
data = [social.x(curr, :)', [0;diff(social.x(curr, :))']];
O = [2 -1; 1 0];
H = [1 0; 1 -1];
Q = 2;
R = 2;

%myKalman(data, O, H, Q, R)
s = [];
Z = [social.x(curr, 2:end); social.x(curr, 1:end-1)];

Zx = [social.x(curr, :); [0,diff(social.x(curr, :))]];
Zy = [social.x(curr, :); [0,diff(social.x(curr, :))]];

s.A = O;
s.H = H;
s.Q = Q;
s.R = R;

x = Z * 0;
for i=1:size(Z, 2)
    s.z = Z(:, i);
    s = kalmanf(s);
    x(:, i) = s.x;
end

