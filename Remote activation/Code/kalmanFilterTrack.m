function social = kalmanFilterTrack(social, q, r)
fprintf('# - filtering tracks\n');

% r =  1;
% q =  5;
dt = social.time(2) - social.time(1);
s.A = [1 0 dt 0; 0 1 0 dt; 0 0 1 0; 0 0 0 1];
s.H = [1 0 0 0; 0 1 0 0];
s.Q = [0 0 0 0; 0 0 0 0; 0 0 q^2 0; 0 0 0 q^2];
s.R = [r^2 0; 0 r^2];

%for curr = 1:social.nSubjects;
for curr = 3
    fprintf(['#   . subject ' num2str(curr) '\n']);
    Z = [social.x(curr, 2:end); social.y(curr, 2:end)];
    
    iP = s.Q;
    ix = [Z(:, 1); diff(social.x(curr, 1:2))/dt; diff(social.y(curr, 1:2))/dt];
    
    x = kalman_filter(Z(1:2, :), s.A, s.H, s.Q, s.R, ix, iP);
    
    social.x(curr, :) = [ix(1), x(1, :)];
    social.y(curr, :) = [ix(2), x(2, :)];
    social.speed(curr, :) = [0, sqrt(x(3, :) .^ 2 + x(4, :) .^ 2)];
end
% s.A = [2 -1; 1 0];
% s.H = [1 0; 1 -1];
% s.Q = 10^2;
% s.R = 10^2;
%
% X = social.x * 0;
% Y = social.y * 0;
% %for curr = 1:social.nSubjects;
% for curr = 3;
%     curr
%     Zx = [social.x(curr, :); [0,diff(social.x(curr, :))]];
%     Zy = [social.y(curr, :); [0,diff(social.y(curr, :))]];
%     iP = inv(s.H)*s.R*inv(s.H');
%
%     iz = Zx(:, 1);
%     ix = inv(s.H)*iz;
%
%     x = kalman_filter(Zx, s.A, s.H, s.Q, s.R, ix, iP);
%     X(curr, :) = x(1, :);
%
%     iz = Zy(:, 1);
%     iy = inv(s.H)*iz;
%     y = kalman_filter(Zy, s.A, s.H, s.Q, s.R, iy, iP);
%     Y(curr, :) = y(1, :);
% end
%
return;
X = social.x * 0;
Y = social.y * 0;

for curr = 1:social.nSubjects;
    curr
    Zx = [social.x(curr, :); [0,diff(social.x(curr, :))]];
    Zy = [social.x(curr, :); [0,diff(social.x(curr, :))]];
    
    sX = s;
    for i=1:size(Zx, 2)
        sX.z = Zx(:, i);
        sX = kalmanf(sX);
        x(:, i) = sX.x;
    end
    
    sY = s;
    for i=1:size(Zy, 2)
        sY.z = Zy(:, i);
        sY = kalmanf(sY);
        y(:, i) = sY.x;
    end
    X(curr, :) = x(:, curr)';
    Y(curr, :) = y(:, curr)';
end
