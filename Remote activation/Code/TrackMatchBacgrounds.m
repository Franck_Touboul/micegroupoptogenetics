function TrackMatchBacgrounds(obj1, obj2)
%%
subplot(2,2,1);
i1 = rgb2gray(obj1.BkgImage);
e1 = i1 > median(i1(:));
imagesc(i1 > median(i1(:)));

subplot(2,2,2);
i2 = rgb2gray(obj2.BkgImage);
e2 = i2 > median(i2(:));
imagesc(i2 > median(i2(:)));
%%
image_registr_MI(i1, i2, -20:2:20, 50, 0);

%%
scales = 80:5:120;
angles = -15:3:15;
shift = -40:10:40;

padsize = ceil(max(abs(shift)) + max(size(i2)) * (1 - min(scales) / 100));

best = -inf;
vals = [];
for s=scales
    s
    sc1 = imresize(i1, s/100);
    s1 = padarray(sc1, [padsize padsize]);
    for x=shift
        for y=shift
            dx = round(size(sc1, 1)-size(i2, 1)) + x + padsize;
            dy = round(size(sc1, 2)-size(i2, 2)) + y + padsize;
            ds1 = s1(dx:dx+size(i2,1)-1, dy:dy+size(i2,2)-1,:);
            for a=angles
                r1 = imrotate(ds1, a,'crop');
                m = mean(r1(:) == i2(:));
                if m > best
                    best = m;
                    vals = [s, x, y, a];
                end
            end
        end
    end
end
