function varargout = OptoRun(varargin)
% OPTORUN MATLAB code for OptoRun.fig
%      OPTORUN, by itself, creates a new OPTORUN or raises the existing
%      singleton*.
%
%      H = OPTORUN returns the handle to a new OPTORUN or the handle to
%      the existing singleton*.
%
%      OPTORUN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OPTORUN.M with the given input arguments.
%
%      OPTORUN('Property','Value',...) creates a new OPTORUN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OptoRun_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OptoRun_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OptoRun

% Last Modified by GUIDE v2.5 24-Aug-2015 10:53:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @OptoRun_OpeningFcn, ...
                   'gui_OutputFcn',  @OptoRun_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
global RTGuiData;



function Message(varargin)
global RTGuiData;
msg = sprintf(varargin{:});
t = get(RTGuiData.handles.MainBox, 'String');
t{end+1}=['# ' msg];
set(RTGuiData.handles.MainBox, 'String', t)
ScrollDown(RTGuiData.handles.MainBox);


% --- Executes just before OptoRun is made visible.
function OptoRun_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OptoRun (see VARARGIN)

% Choose default command line output for OptoRun
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OptoRun wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global RTGuiData;
RTGuiData.handles = handles;
predevices = cell(1,2); % analog digital
predevices{2} = 'Dev5';

Message('- using session based interface (64bit)');
d = daq.getDevices;
d=d(2);
if length(d) == 0
    Message('- no devices found');
else
    Message('- devices:');
    dev = '';
    doneAnalog = false;
    doneDigital = false;
    for i=1:length(d)
        Message(['  . ' d(i).Description '(' d(i).ID ')']);
        s = d(i).Subsystems;
        isdigital = false;
        isanalog = false;
        for j=1:length(s)
            if ~isempty(regexpi( s(1).SubsystemType, 'digital'))
                isdigital = true;
            end
            if ~isempty(regexpi( s(1).SubsystemType, 'analog'))
                isanalog = true;
            end
        end
        dev = d(i).ID;
        if (isanalog && ~doneAnalog && ~any(strcmpi(dev, predevices))) || (~isempty(predevices{1}) && strcmpi(predevices{1}, dev))
            Message('- creating session')
            s = daq.createSession('ni');
            Message('- adding analog channel');
            try
                s.addAnalogInputChannel(dev, [1], 'Voltage');
                addlistener(s,'DataAvailable', @LightDetect);
                s.IsContinuous = true;
                Message('- [DONE]');
                startBackground(s);
                doneAnalog = true;
                RTGuiData.aDAQ = s;
            catch
                Message('- [FAILED]');
            end
        elseif (isdigital && ~doneDigital && ~any(strcmpi(dev, predevices))) || (~isempty(predevices{2}) && strcmpi(predevices{2}, dev))
            Message('- creating session')
            s = daq.createSession('ni');
            Message('- adding digital channel');
            try
                s.addDigitalChannel(dev, 'port0/line0:3', 'OutputOnly');
                outputSingleScan(s,zeros(1,4));
                Message('- [DONE]');
                doneDigital = true;
                RTGuiData.DAQ = s;
            catch
                Message('- [FAILED]');
            end
        end
    end
end
RTGuiData.DAQ32bit = false;

% --- Outputs from this function are returned to the command line.
function varargout = OptoRun_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function MainBox_Callback(hObject, eventdata, handles)
% hObject    handle to MainBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MainBox as text
%        str2double(get(hObject,'String')) returns contents of MainBox as a double


% --- Executes during object creation, after setting all properties.
function MainBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MainBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ActivateButton.
function ActivateButton_Callback(hObject, eventdata, handles)
% hObject    handle to ActivateButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global RTGuiData;
Message('Activating for 10 seconds...');
outputSingleScan(RTGuiData.DAQ,ones(1,4));
set(RTGuiData.handles.ActivateButton, 'Enable', 'off');
pause(10)
outputSingleScan(RTGuiData.DAQ,zeros(1,4));
set(RTGuiData.handles.ActivateButton, 'Enable', 'on');
Message('- done');
