function obj = SocialStatisticsPotts(obj)
obj = TrackLoad(obj);
RunOnGroup = true;
nIters = 200;
order = 3;

%% independent
fprintf('# - independent\n');
jointProbs = obj.Analysis.Potts.Model{3}.prob';
indepProbs = obj.Analysis.Potts.Model{1}.prob';
IndepDkl = jointProbs * (log2(jointProbs) - log2(indepProbs))';
IndepJSD = JensenShannonDivergence(jointProbs, indepProbs);

%% subject permuted
fprintf('# - subject permuted\n');
SPObj = obj;
SPObj.zones = obj.zones([2:obj.nSubjects, 1], :);
[SPObj, SPJointProbs] = SocialComputePatternPottsProbs(SPObj, order);
SPDkl = jointProbs * (log2(jointProbs) - log2(SPJointProbs))';
SPJSD = JensenShannonDivergence(jointProbs, SPJointProbs);

if RunOnGroup
    SPPerms = perms(1:obj.nSubjects);
    SPAllDkl = zeros(1, size(SPPerms, 1));
    SPAllJSD = zeros(1, size(SPPerms, 1));
    nchars = 0;
    for i=1:size(SPPerms, 1)
        nchars = Reprintf(nchars, '#  . perm no. %d/%d', i, size(SPPerms, 1));
        CurrSPObj = obj;
        CurrSPObj.zones = obj.zones(SPPerms(i, :), :);
        [CurrSPObj, CurrSPJointProbs] = SocialComputePatternPottsProbs(CurrSPObj, order);
        SPAllDkl(i) = jointProbs * (log2(jointProbs) - log2(CurrSPJointProbs))';
        SPAllJSD(i) = JensenShannonDivergence(jointProbs, CurrSPJointProbs);
    end
    fprintf('\n');
    SPPermOrder = sum(SPPerms ~= repmat(1:obj.nSubjects, size(SPPerms, 1), 1), 2);
end

%% subject permuted each frame
fprintf('# - subject permuted\n');
if RunOnGroup
    SPeFAllJSD = zeros(1, nIters);
    nchars = 0;
    for iter=1:nIters
        nchars = Reprintf(nchars, '#  . iter no. %d/%d', iter, nIters);
        CurrSPeFObj = obj;
        for f=1:obj.nFrames
            CurrSPeFObj.zones(:, f) = obj.zones(randperm(obj.nSubjects), f);
        end
        [CurrSPeFObj, CurrSPeFJointProbs] = SocialComputePatternPottsProbs(CurrSPeFObj, order);
        %SPeFAllDkl(iter) = jointProbs * (log2(jointProbs) - log2(CurrTSJointProbs))';
        SPeFAllJSD(iter) = JensenShannonDivergence(jointProbs, CurrSPeFJointProbs);
    end
    fprintf('\n');
end
%% time shifted
fprintf('# - time shifted\n');
TSObj = obj;
for i=1:obj.nSubjects
    start = round(obj.nFrames / obj.nSubjects * (i-1) + 1);
    TSObj.zones(i, start:end) = obj.zones(i, 1:end-start+1);
    TSObj.zones(i, 1:start-1) = obj.zones(i, end-start+2:end);
end
[TSObj, TSJointProbs] = SocialComputePatternPottsProbs(TSObj, order);
TSDkl = jointProbs * (log2(jointProbs) - log2(TSJointProbs))';
TSJSD = JensenShannonDivergence(jointProbs, TSJointProbs)';

if RunOnGroup
    TSAllDkl = zeros(1, nIters);
    TSAllJSD = zeros(1, nIters);
    nchars = 0;
    for iter=1:nIters
        nchars = Reprintf(nchars, '#  . iter no. %d/%d', iter, nIters);
        CurrTSObj = obj;
        for i=1:obj.nSubjects
            start = randi(obj.nFrames);
            CurrTSObj.zones(i, start:end) = obj.zones(i, 1:end-start+1);
            CurrTSObj.zones(i, 1:start-1) = obj.zones(i, end-start+2:end);
        end
        [CurrTSObj, CurrTSJointProbs] = SocialComputePatternPottsProbs(CurrTSObj, order);
        TSAllDkl(iter) = jointProbs * (log2(jointProbs) - log2(CurrTSJointProbs))';
        TSAllJSD(iter) = JensenShannonDivergence(jointProbs, CurrTSJointProbs);
    end
    fprintf('\n');
end

%% interlaced
H1Obj = obj;

quarter = floor(obj.nFrames/4);
map1 = false(1, obj.nFrames);
map1([1:quarter, 2*quarter+1:3*quarter]) = true;
H1Obj.zones = obj.zones(:, map1);
H1Obj.valid = obj.valid(:, map1);
[H1Obj, H1JointProbs] = SocialComputePatternPottsProbs(H1Obj, order);

H2Obj = obj;
map2 = false(1, obj.nFrames);
map2([quarter+1:2*quarter, 4*quarter+1:4*quarter]) = true;
H2Obj.zones = obj.zones(:, map2);
H2Obj.valid = obj.valid(:, map2);
[H2Obj, H2JointProbs] = SocialComputePatternPottsProbs(H2Obj, order);

H12JSD = JensenShannonDivergence(H1JointProbs, H2JointProbs)';

%% halves
HaObj = obj;
HaObj.zones = obj.zones(:, 1:floor(end/2));
HaObj.valid = obj.valid(:, 1:floor(end/2));
[HaObj, HaJointProbs] = SocialComputePatternPottsProbs(HaObj, order);

HbObj = obj;
HbObj.zones = obj.zones(:, floor(end/2)+1:end);
HbObj.valid = obj.valid(:, floor(end/2)+1:end);
[HbObj, HbJointProbs] = SocialComputePatternPottsProbs(HbObj, order);

HabJSD = JensenShannonDivergence(HaJointProbs, HbJointProbs)';

%% output
% observed
fig = 1;

figure %(5,2,1);
PlotProbProb(jointProbs, indepProbs, obj.nValid, true);
prettyPlot(sprintf('independent (D_{KL}=%f, D_{JS}=%f)', IndepDkl, IndepJSD), 'observed pattern prob [log10]', 'independent approximation prob [log10]');
saveFigure(['Graphs/SocialStatistics.' num2str(fig)]); fig = fig + 1;

% subject permuted
figure %(5,2,3);
PlotProbProb(jointProbs, SPJointProbs, obj.nValid, true);
prettyPlot(sprintf('subject permuted (D_{KL}=%f, D_{JS}=%f)', SPDkl, SPJSD), 'observed pattern prob [log10]', 'permuted pattern prob [log10]');
saveFigure(['Graphs/SocialStatistics.' num2str(fig)]); fig = fig + 1;

if RunOnGroup
    figure %(5,2,4);
    SPDklMean = zeros(1, obj.nSubjects - 1);
    SPDklSTDErr = zeros(1, obj.nSubjects - 1);

    SPJSDMean = zeros(1, obj.nSubjects - 1);
    SPJSDSTDErr = zeros(1, obj.nSubjects - 1);
    
    for i=2:obj.nSubjects
        SPDklMean(i-1) = mean(SPAllDkl(SPPermOrder == i));
        SPDklSTDErr(i-1) = stderr(SPAllDkl(SPPermOrder == i));

        SPJSDMean(i-1) = mean(SPAllJSD(SPPermOrder == i));
        SPJSDSTDErr(i-1) = stderr(SPAllJSD(SPPermOrder == i));
    end
%    barweb([SPDklMean', SPJSDMean'], [SPDklSTDErr', SPJSDSTDErr'], .8, 2:obj.nSubjects);
    barweb([SPJSDMean'], [SPJSDSTDErr'], .8, 2:obj.nSubjects);
    hold on;
    for i=2:obj.nSubjects
%        plot(i-1, SPAllDkl(SPPermOrder == i)', 'kx');
        plot(i-1, SPAllJSD(SPPermOrder == i)', 'kx');
    end
    hold off;
    prettyPlot('for all permutations', 'no. of changes', 'D_{JS}');
    saveFigure(['Graphs/SocialStatistics.' num2str(fig)]); fig = fig + 1;
end

% subject permuted each frame
if RunOnGroup
    figure %(5,2,6);
    hist(SPeFAllJSD, 20);
    prettyPlot(sprintf('subject permuted each step (mean=%f)', mean(SPeFAllJSD)), 'D_{JS}', 'count');
    saveFigure(['Graphs/SocialStatistics.' num2str(fig)]); fig = fig + 1;
end

% time shifted
figure %(5,2,7);
PlotProbProb(jointProbs, TSJointProbs, obj.nValid);
prettyPlot(sprintf('time shifted (D_{KL}=%f, D_{JS}=%f)', TSDkl, TSJSD), 'observed pattern prob [log10]', 'shifted pattern prob [log10]');
saveFigure(['Graphs/SocialStatistics.' num2str(fig)]); fig = fig + 1;

if RunOnGroup
    figure %(5,2,8);
%    hist(TSAllDkl, 20);
    hist(TSAllJSD, 20);
    prettyPlot(sprintf('random shifts (mean=%f)', mean(TSAllJSD)), 'D_{JS}', 'count');
    saveFigure(['Graphs/SocialStatistics.' num2str(fig)]); fig = fig + 1;
end

% interlaced
figure %(5,2,9);
PlotProbProb(H1JointProbs, H2JointProbs, ceil(obj.nValid/2));
prettyPlot(sprintf('interlaced (D_{JS}=%f)', H12JSD), 'odd pattern prob [log10]', 'even pattern prob [log10]');
saveFigure(['Graphs/SocialStatistics.' num2str(fig)]); fig = fig + 1;

% halves
figure %(5,2,10);
PlotProbProb(HaJointProbs, HbJointProbs, obj.nValid);
prettyPlot(sprintf('halves (D_{JS}=%f)', HabJSD), '1st half pattern prob [log10]', '2nd half pattern prob [log10]');
saveFigure(['Graphs/SocialStatistics.' num2str(fig)]); fig = fig + 1;
