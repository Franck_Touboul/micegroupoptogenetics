function obj2 = SocialSimulatePottsModel(obj, level)
obj2 = obj;
%%
me = obj.Analysis.Potts.Model;
p = exp(me{level}.perms * me{level}.weights');
Z = sum(p);
p = p / Z;

cp = cumsum(p);
%%
r = rand(1, obj.nFrames);
l = zeros(1, obj.nFrames);
for i=1:length(r)
    l(i) = sum(r(i) > cp) + 1;
end
%%
obj2.zones = me{level}.inputPerms(l, :)';
