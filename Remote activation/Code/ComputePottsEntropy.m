function Entropy = ComputePottsEntropy(model, weights)
if nargin == 1
    weights = model.weights;
end

p = exp(model.perms * weights');
perms_p = exp(model.perms * weights');
Z = sum(perms_p);
p = p / Z;
Entropy = -p' * log2(p);