function newObj = SocialObjectDilute(obj, fields)

newObj = struct();
all = fieldnames(obj);
for i=1:length(all)
    if any(strcmp(all{i}, fields))
        newObj.(all{i}) = obj.(all{i});
    else
        if isstruct(obj.(all{i})) || (length(obj.(all{i})) > 1 && ~ischar(obj.(all{i})))
        else
            newObj.(all{i}) = obj.(all{i});
        end
    end
end
