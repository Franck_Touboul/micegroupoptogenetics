function [features, labels] = ComputeFeatures(data, range, nPerms, use_sparse)
if nargin < 4
    use_sparse = false;
end

dim = 0;
for i=1:length(nPerms)
    nVals = range^length(nPerms{i});
    dim = dim + nVals;
end

offset = 0;
labels = cell(1, dim);
NonZeros = cell(1, length(nPerms));
count = 0;
index = 1;
for i=1:length(nPerms)
    nVals = range^length(nPerms{i});
    baseVector = range.^[length(nPerms{i})-1:-1:0];
    h =  data(:, nPerms{i}) * baseVector' + 1;
    
    NonZeros{i} = [(1:size(data, 1))', offset + h];
    count = count + size(NonZeros{i}, 1);
    offset = offset + nVals;
    for j=0:nVals-1
         values = decimal2base(j, range, length(nPerms{i})) + 1;
         labels{index} = [nPerms{i}; values];
         index = index + 1;
    end
end

nonzeros = zeros(count, 2);
offset = 0;
for i=1:length(nPerms)
    nonzeros(offset+1:offset+size(NonZeros{i}, 1), :) = NonZeros{i};
    offset = offset + size(NonZeros{i}, 1);
end    
if use_sparse
    features = sparse(nonzeros(:, 1), nonzeros(:, 2), ones(size(nonzeros, 1), 1), size(data, 1), double(dim));
else
    error
    %features = zeros(size(data, 1), double(dim));
end

%     nVals = range^length(nPerms{i});
%     for j=0:nVals-1
%         values = decimal2base(j, range, length(nPerms{i})) + 1;
%         map = true(size(data, 1), 1);
%         for k=1:length(nPerms{i})
%             map = map & data(:, nPerms{i}(k)) == values(k);
%         end
%         features(:, index) = map;
%         
%         labels{index} = [nPerms{i}; values];
%         index = index + 1;
%     end
% end


