function Points = ModelToPointsF(Model)
Points = struct();
P0 = [0; 0; 0];

Points.Skin.X = zeros(Model.NumVerteb, Model.Resolution);
Points.Skin.Y = zeros(Model.NumVerteb, Model.Resolution);
Points.Skin.Z = zeros(Model.NumVerteb, Model.Resolution);

Points.Bone.X = zeros(Model.NumVerteb, 2);
Points.Bone.Y = zeros(Model.NumVerteb, 2);
Points.Bone.Z = zeros(Model.NumVerteb, 2);

Joints = cell(1, length(unique(Model.Types(:)'))+1);
for i=unique(Model.Types(:)')
    Joints{i+1} = [];
end
    
sidx = 1;
skinrot = eye(3);
MundiMat = RotMat(Model.AxisMundi.Yaw, Model.AxisMundi.Pitch, Model.AxisMundi.Roll) * Model.AxisMundi.Scale;
for i=1:Model.NumVerteb
    Bone = Model.Bones(i);
    tid = Model.Types(i);
    if isempty(Joints{tid+1})
        dep = Model.Dependence(i);
        if dep >= 0
            Joints{tid + 1} = Joints{dep + 1} * RotMat(Bone.Yaw, Bone.Pitch, Bone.Roll);
        else
            Joints{tid + 1} = RotMat(Bone.Yaw, Bone.Pitch, Bone.Roll);
        end
    else
        Joints{tid + 1} = frwdrot;
    end
    rot = Joints{tid + 1};
    if i < Model.NumVerteb
        m = RotMat(Model.Bones(i+1).Yaw, Model.Bones(i+1).Pitch, Model.Bones(i+1).Roll);
        frwdrot = rot * m;
    end
    if Bone.Memory
        if i==1
            skinrot = (rot + frwdrot) / 2;
        else
            skinrot = Model.SkinMemory * skinrot + (1 - Model.SkinMemory) * (rot + frwdrot) / 2;
        end
    else
        skinrot = rot;
    end
    P1 = P0 + rot * [Bone.Length; 0; 0];
    %%
    rP0 = MundiMat * P0;
    rP1 = MundiMat * P1;
    Points.Bone.X(i, 1) = rP0(1);
    Points.Bone.Y(i, 1) = rP0(2);
    Points.Bone.Z(i, 1) = rP0(3);
    Points.Bone.X(i, 2) = rP1(1);
    Points.Bone.Y(i, 2) = rP1(2);
    Points.Bone.Z(i, 2) = rP1(3);
    %%
    nP = skinrot(:, 2:3) * ([sin(Bone.Skin(1, :)) .* Bone.Skin(2, :); cos(Bone.Skin(1, :)).* Bone.Skin(2, :)]);
    S = bsxfun(@plus, nP, P0);
    rS = MundiMat * S;
    Points.Skin.X(i, :) = rS(1, :);
    Points.Skin.Y(i, :) = rS(2, :);
    Points.Skin.Z(i, :) = rS(3, :);
    %%    
    P0 = P1;
end

X = Points.Skin.X;
Y = Points.Skin.Y;
e1 = convn(X(:, 1:end-1) .* Y(:, 2:end) - X(:, 2:end) .* Y(:, 1:end-1), [1;  -1], 'valid');
e2 = convn(X(1:end-1, :) .* Y(2:end, :) - X(2:end, :) .* Y(1:end-1, :), [-1,  1], 'valid');
v = e1 + e2;
v(:, end+1) = v(:, end);
v(end+1, :) = v(end, :);
Points.Skin.Area = v;

if nargout == 0
    surfl(Points.Skin.X,Points.Skin.Y,1-Points.Skin.Z); 
end

