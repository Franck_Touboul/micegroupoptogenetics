function [pStates, logpseq, alpha, beta, scale, table] = ModelProbs(model, data)
data = [inf, data];
[dim, nsamples] = size(data);

%% initialize tables
table = zeros(model.nstates, nsamples);
alpha = zeros(model.nstates, nsamples);
beta  = ones(model.nstates, nsamples);
scale = ones(1, nsamples);

%% compute emission & forward probabilities
alpha(:, 1) = model.start;
for i=2:nsamples
    for s=1:model.nstates
        table(s, i) = model.states(s).func(data(:, i));
        alpha(s, i) = table(s, i) .* (sum(alpha(:,i-1) .* model.trans(:,s)));
    end
    % scale factor normalizes sum(fs,count) to be 1. 
    scale(i) =  sum(alpha(:, i));
    alpha(:, i) =  alpha(:,i)./scale(i);
end

%% compute backward probability
for i=nsamples-1:-1:1
    for s=1:model.nstates
        beta(s, i) = (1 / scale(i+1)) * sum( model.trans(s, :)' .* table(:, i+1) .* beta(:, i+1) );
    end
end

%%
pStates = alpha.*beta;

% get rid of the column that we stuck in to deal with the alpha and beta
pStates(:,1) = [];

%%
logpseq = sum(log(scale));
