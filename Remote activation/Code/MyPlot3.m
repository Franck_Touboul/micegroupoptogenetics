function MyPlot3(x, y, colormap_name)
if nargin==2
    colormap_name = 'jet';
end
y = (y + 1) / 2;
%r1 = min(x);
%r2 = max(x);

N = 20;
if isstr(colormap_name)
    c = colormap(colormap_name);
else
    c = colormap_name;
end
for n=1:N
    r1 = (n - 1)/N;
    if (n==1); r1 = -inf; end
    r2 = n/N;
    if (n==N); r2 =  inf; end
    f = find(y > r1 & y <= r2);
    plot(x(f, 1), x(f, 2), 's', 'MarkerFaceColor', c(floor(n/N * length(c)), :), 'Color', c(floor(n/N * length(c)), :));
    if (n == 1)
        hold on;
    end
end
axis([-1 1 -1 1]);
hold off;

