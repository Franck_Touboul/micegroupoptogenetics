function [c, ceq] = PottsConstraints(model, weights)
%%
p = exp(model.perms * weights');
% normalize the pdf (the Z)
perms_p = p;
Z = sum(p);
p = p / Z;
ceq = full(sum(model.perms .* repmat(perms_p / Z, 1, size(model.perms, 2)))) - model.constraints;
c = [];

