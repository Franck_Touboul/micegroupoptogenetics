load('Models/cage 1 day 1 analysis (Trial     1)-Arena 1-Potts(2)');

aux.zoneCoord = [...
    -40 40;
    60 150;
    -100 150;
    150 80;
    -160 -60;
    150 -30;
    80 -120;
    ...
    ];

plot(aux.zoneCoord(:, 1), aux.zoneCoord(:, 2), 'x'); hold on;


%%
clf
options.maxRadios = 20;
options.radios = 8;
options.distance = 30;
options.maxWeight = 4;
options.smallRadios = 10;

for s1=1:options.nSubjects
    for s2=s1+1:options.nSubjects
        subplot(options.nSubjects-1, options.nSubjects-1, (s1 - 1) * (options.nSubjects - 1) + s2 - 1)
        weights = zeros(socialZones.count);
        for z1=1:socialZones.count
            for z2=1:socialZones.count
                weight = nan;
                for i=1:length(me.labels)
                    curr = me.labels{i};
                    if size(curr, 2) == 2 && ...
                            (curr(1,1) == s1 && curr(2,1) == z1) && ...
                            (curr(1,2) == s2 && curr(2,2) == z2)
                        weights(z1, z2) = abs(me.weights(i));
                        break;
                    end
                end
                %        plot([aux.zoneCoord(z1, 1), aux.zoneCoord(z2, 1)], [aux.zoneCoord(z1, 2), aux.zoneCoord(z2, 2)]); hold on;
            end
        end
        
        for z=1:socialZones.count
            %    plot(aux.zoneCoord(z, 1), aux.zoneCoord(z, 2), 'o', 'MarkerSize', options.maxRadios*weights(z, z), 'MarkerEdgeColor', 'k', 'LineWidth', 2); hold on;
            cindex = round(weights(z, z) * 125);
            if cindex > 256; cindex = 256; end;
            if cindex <   1; cindex =   1; end;
            color = cmap(cindex, :);
            plot(aux.zoneCoord(z, 1), aux.zoneCoord(z, 2), 'o', 'MarkerSize', options.radios*2, 'MarkerEdgeColor', 'k', 'MarkerFaceColor', color, 'LineWidth', 2); hold on;
        end
        axis([-200 200 -150 200]);
        %
        for z1=1:socialZones.count
            for z2=z1+1:socialZones.count
                if z1 == z2
                    continue
                end
                
                from = [aux.zoneCoord(z1, 1), aux.zoneCoord(z1, 2)];
                to = [aux.zoneCoord(z2, 1), aux.zoneCoord(z2, 2)];
                unit = to - from; unit = unit ./ norm(unit);
                from = from + unit * options.distance;
                to = to - unit * options.distance;
                x = [from(1), to(1)];
                y = [from(2), to(2)];
                color = 'g';
                plot(x, y, 'k', 'LineWidth', 2);
            end
        end
        
        cmap = hot(256);
        for z1=1:socialZones.count
            for z2=1:socialZones.count
                if z1 == z2
                    continue
                end
                
                from = [aux.zoneCoord(z1, 1), aux.zoneCoord(z1, 2)];
                to = [aux.zoneCoord(z2, 1), aux.zoneCoord(z2, 2)];
                unit = to - from; unit = unit ./ norm(unit);
                from = from + unit * options.distance;
                to = to - unit * options.distance;
                x = [from(1), to(1)];
                y = [from(2), to(2)];
                cindex = round(weights(z1, z2) * 125);
                if cindex > 256; cindex = 256; end;
                if cindex <   1; cindex =   1; end;
                color = cmap(cindex, :);
                %        plot(to(1), to(2), 'o', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [1 1 1] * weights(z1, z2) * .3);
                plot(to(1), to(2), 'o', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', color, 'MarkerSize', options.smallRadios);
            end
        end
        
        hold off;
    end
end