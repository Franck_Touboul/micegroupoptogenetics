function Color = NameColors(im, mask)
% Based on:
%     Example code on color naming as described in:
%     Learning Color Names for Real-World Applications
%     J. van de Weijer, C. Schmid, J. Verbeek, D. Larlus.
%     IEEE Transactions in Image Processing 2009.
%     
%     Run the example_color_naming.m demo for an exmample of color naming image pixels. The programm calls the im2c function which annotates image pixels with color names. The function takes a color image as input and returns the probability for eleven color names---black, blue, brown, gray, green, orange, pink, purple, red, white, yellow---for all pixels in the image. The mapping from RGB values to color names (w2c.mat) has been learned from Google images (2500).
%     
%     A second rougher quanitization of the sRGB space, w2c_4096.mat, has also been provided. The files are also provided in .txt format where the first three columns indicate the sRGB value and column 4-15 the probability over the color names.
if nargin < 2
    mask = [];
end

im = double(im2uint8(im));
%%
persistent w2c
if isempty(w2c)
    load('ColorNamingMap.mat');
end
[max1,w2CNI]=max(w2c,[],2);      % w2cCNI contains the color-name-index ranging from 1-11 
Color.Names = {'black', 'blue', 'brown', 'grey', 'green', 'orange', 'pink', 'purple', 'red', 'white', 'yellow', 'beige', 'gold', 'olive', 'crimson', 'indigo', 'violet', 'cyan', 'azure', 'goluboi', 'siniy'};

RR=im(:,:,1);GG=im(:,:,2);BB=im(:,:,3);
if isempty(mask)
    index_im = 1+floor(RR(:)/8)+32*floor(GG(:)/8)+32*32*floor(BB(:)/8);
    Color.Index = w2CNI(index_im(:));
    Color.Hist = histc(Color.Index, 1:11);
elseif islogical(mask)
    index_im = 1+floor(RR(mask)/8)+32*floor(GG(mask)/8)+32*32*floor(BB(mask)/8);
    Color.Index = w2CNI(index_im(:));
    Color.Hist = histc(Color.Index, 1:11);
else
    index_im = 1+floor(RR(:)/8)+32*floor(GG(:)/8)+32*32*floor(BB(:)/8);
    Color.Index = w2CNI(index_im(:));
    Color.Weights = mask(:);
    for u=unique(Color.Index(:)')
        Color.Hist(u) = sum(Color.Weights(Color.Index == u));
    end
end
[Color.Count, Color.Order] = sort(Color.Hist, 'descend');
Color.Common = mode(Color.Index);
