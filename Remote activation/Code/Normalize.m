function [x, m1, m2] = Normalize(x, m1, m2)
% [x, m1, m2] = Normalize(x)
% [x, m1, m2] = Normalize(x, y)
%   normalize x according to y
% [x, m1, m2] = Normalize(x, m1, m2)
if nargin == 1
    m2 = max(x);
    m1 = min(x);
elseif nargin == 2
    y = m1;
    m2 = max(y);
    m1 = min(y);
end
x = (x - m1) / (m2 - m1);
