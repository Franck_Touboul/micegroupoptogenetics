SocialExperimentData
%%
fs = 5;
IkAll = cell(GroupsData.nGroups, nDays);
groupids = [];
days = [];
ids = [];
for g=1:length(Groups)
    group=Groups(g);
    for id=group.idx
        for day = 1:nDays
            prefix = sprintf(experiments{id}, day);
            fprintf('# -> %s\n', prefix);
            obj = TrackLoad(['../base/Res/' prefix '.obj.mat'], {'common'});
            obj.zones = blkproc(obj.zones(:, obj.valid), [1, fs], @Majority);
            obj.valid = true(1, size(obj.zones, 2));
            obj.nFrames = size(obj.zones, 2);
            groupids = [groupids, g];
            days = [days, day];
            ids = [ids, id];
            obj.OutputToFile = false;
            obj.OutputInOldFormat = false;
            obj = SocialPotts(obj);
            IkAll{g, day} = [IkAll{g, day}; obj.Analysis.Potts.Ik];
            %Ik(IndexOf(data.Conditions, obj.Condition), obj.Day, :) = reshape(obj.Analysis.Potts.Ik, 1, 1, obj.nSubjects-1);
        end
    end
end
%%
cmap = MySubCategoricalColormap;
cmap = cmap([2,4,6], :);
IK = {};
IKp = {};
for g=1:length(Groups)
    group=Groups(g);
    m = [];
    s = [];
    sum_m = [];
    sum_s = [];
    e_m = [];
    e_s = [];
    IK{g} = zeros(size(IkAll{g, 1}, 1), obj.nSubjects);
    IKp{g} = zeros(size(IkAll{g, 1}, 1), obj.nSubjects - 2);
    for day = 1:nDays
        m(day, :) = mean(IkAll{g, day});
        s(day, :) = stderr(IkAll{g, day});
        sum_m(day) = mean(sum(IkAll{g, day}, 2));
        sum_s(day) = stderr(sum(IkAll{g, day}, 2));
        e = cumsum(IkAll{g, day},2) ./ repmat(sum(IkAll{g, day}, 2), 1, obj.nSubjects-1) * 100; e = e(:, 1:end-1);
        IKp{g} = IKp{g} + e;
        e_m(day, :) = mean(e);
        e_s(day, :) = stderr(e);
        IK{g} = IK{g} + [IkAll{g, day}, sum(IkAll{g, day}, 2)];
    end
    %%
    subplot(2,GroupsData.nGroups + 1,g);
    for i=1:obj.nSubjects-1
        errorbar(m(:,i), s(:, i), 'Color', cmap(i, :), 'LineWidth', 2);
        hold on;
    end
    errorbar(sum_m, sum_s, 'k:', 'LineWidth', 2);
    %axis square
    %% significance
    %     for day = 1:nDays
    %         sig = ttest2(IkAll{1, day}, IkAll{2, day});
    %         subplot(2,3,g);
    %         for i=find(sig)
    %             plot(day, m(day, i), 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', 10);
    %         end
    %         sig = ttest2(sum(IkAll{1, day}, 2), sum(IkAll{2, day}, 2));
    %         if sig
    %             plot(day, sum_m(day), 'o', 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'none', 'MarkerSize', 10);
    %         end
    %     end
    %%
    subplot(2,GroupsData.nGroups+1,GroupsData.nGroups+1+g);
    for i=1:obj.nSubjects-2
        errorbar(e_m(:,i), e_s(:, i), 'Color', cmap(i, :), 'LineWidth', 2);
        hold on;
    end
    %axis square
    %% significance
    %     for day = 1:nDays
    %         e1 = cumsum(IkAll{1, day},2) ./ repmat(sum(IkAll{1, day}, 2), 1, obj.nSubjects-1) * 100; e1 = e1(:, 1:end-1);
    %         e2 = cumsum(IkAll{2, day},2) ./ repmat(sum(IkAll{2, day}, 2), 1, obj.nSubjects-1) * 100; e2 = e2(:, 1:end-1);
    %         sig = ttest2(e1, e2);
    %         subplot(2,3,3+g);
    %         for i=find(sig)
    %             plot(day, e_m(day, i), 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', 10);
    %         end
    %     end
end
%
y = [];
gid = [];
days = [];
for g=1:length(Groups)
    for day = 1:nDays
        y = [y; IkAll{g, day}];
        gid = [gid; IkAll{g, day}(:, 1) * 0 + g];
        days = [days; IkAll{g, day}(:, 1) * 0 + day];
    end
end
% p = [];
% y(:, end+1) = sum(y,2);
% for i=1:size(y, 2)
%     p(:, i) = anovan(y(:, i), {gid, days});
% end
%
TieAxis(2,length(Groups)+1,1:length(Groups));
TieAxis(2,length(Groups)+1,length(Groups)+2:length(Groups)+1+length(Groups));
for g=1:length(Groups)
    subplot(2,GroupsData.nGroups+1,g);
    a = axis;
    axis([.5 GroupsData.nDays + .5 a(3) a(4)]);
    set(gca, 'XTick', 1:nDays);
    title(Groups(g).title);
    if g == 1;
        ylabel('multiple information [bits]');
        AX=legend('pair', 'tri', 'quad', 'total');
        LEG = findobj(AX,'type','text');
        set(LEG,'FontSize',10)
        
        legend boxoff;
    end
    hold off;
    subplot(2,GroupsData.nGroups+1,GroupsData.nGroups+1+g);
    a = axis;
    axis([.5 GroupsData.nDays+.5 a(3) 100])
    xlabel('time [days]');
    set(gca, 'XTick', 1:nDays);
    if g == 1;
        ylabel('explained information [%]');
        AX = legend('pair', 'tri', 'location', 'SouthEast');
        LEG = findobj(AX,'type','text');
        set(LEG,'FontSize',10)
        legend boxoff;
    end
    hold off;
end
%%
subplot(2,GroupsData.nGroups+1,GroupsData.nGroups+1);
meanval = [];
stderrval = [];
for g=1:GroupsData.nGroups
    meanval = [meanval; mean(IK{g}/nDays)];
    stderrval = [stderrval; stderr(IK{g}/nDays)];
    %sig = ttest2(v1, v2);
end
MyBarWeb(1:GroupsData.nSubjects, meanval, stderrval, GroupsData.Colors);
%colormap(GroupsData.Colors);
hold on;
a = axis;
% for i=find(sig)
%     %    plot(i, max(mean(v1(:, i)), mean(v2(:, i))) + .25, 'k*');
%     plot(i, a(4), 'k*');
% end
set(gca, 'XTickLabel', {'pair', 'tri', 'quad', 'total'});
hold off;
legend(GroupsData.Titles)
legend boxoff;
%axis square
%
subplot(2,GroupsData.nGroups+1,2*GroupsData.nGroups+2);
meanval = [];
stderrval = [];
for g=1:GroupsData.nGroups
    meanval = [meanval; mean(IKp{g}/nDays)];
    stderrval = [stderrval; stderr(IKp{g}/nDays)];
    %sig = ttest2(v1, v2);
end
MyBarWeb(1:GroupsData.nSubjects-2, meanval, stderrval, GroupsData.Colors);
% barweb([mean(v1); mean(v2)]', [stderr(v1); stderr(v2)]');
% colormap([Groups(1  ).color; Groups(2).color]);
hold on;
a = axis;
% for i=find(sig)
%     %    plot(i, max(mean(v1(:, i)), mean(v2(:, i))) + .25, 'k*');
%     plot(i, a(4), 'k*');
% end
set(gca, 'XTickLabel', {'pair', 'tri'});
hold off;
%axis square

return

%%
% name = {};
% index = 1;
% for g=1:2
%     if g==1; group=E; else group=SC; end
%     for id=group.idx
%         for day = 1:nDays
%             prefix = sprintf(experiments{id}, day);
%             name{index} = prefix;
%             index = index + 1;
%         end
%     end
% end
%
%%
Ik = zeros(data.nConditions, data.nDays, obj.nSubjects-1);
IkSTDErr = zeros(data.nConditions, data.nDays, obj.nSubjects-1);

In = zeros(data.nConditions, data.nDays);
InSTDErr = zeros(data.nConditions, data.nDays);

InfoFraction = zeros(data.nConditions, data.nDays, obj.nSubjects-1);
InfoFractionSTDErr = zeros(data.nConditions, data.nDays, obj.nSubjects-1);

for i=1:data.nConditions
    for d=1:data.nDays
        Ik(i, d, :)       = reshape(mean(IkAll{i, d}, 1), [1, 1, first.nSubjects-1]);
        IkSTDErr(i, d, :) = reshape(stderr(IkAll{i, d}, 1), [1, 1, first.nSubjects-1]);
        
        In(i, d)       = mean(sum(IkAll{i, d}, 2));
        IkSTDErr(i, d, :) = stderr(sum(IkAll{i, d}, 2));
        
        InfoFraction(i, d, :) = reshape(mean(100 * cumsum(IkAll{i, d}, 2) ./ repmat(sum(IkAll{i, d}, 2), 1, obj.nSubjects - 1), 1), [1, 1, first.nSubjects-1]);
        InfoFractionSTDErr(i, d, :) = reshape(stderr(100 * cumsum(IkAll{i, d}, 2) ./ repmat(sum(IkAll{i, d}, 2), 1, obj.nSubjects - 1), 1), [1, 1, first.nSubjects-1]);
    end
end

%InfoFraction = cumsum(Ik, 3) ./ repmat(sum(Ik, 3), [1, 1, first.nSubjects-1]);
%%
clf;
map = {'rx:', 'g^:', 'bs:', 'k.:'};
sampleMap = {'r.', 'g.', 'b.', 'k.'};
style = {'LineWidth', 2, 'MarkerSize', 10};
for i=1:data.nConditions
    for j=1:first.nSubjects-1
        %
        subplot(m, n, i);
        rowTitle(m, n, 1, 'I_{(k)}');
        errorbar(data.Days, Ik(i, :, j), IkSTDErr(i, :, j), map{j}, style{:});
        title(data.Conditions{i});
        set(gca, 'XTick', 1:data.nDays);
        hold on;
        for d=data.Days
            currIkAll = IkAll{i, d};
            plot(d, currIkAll(:, j), sampleMap{j}, style{:},'HandleVisibility','off');
        end
        if j==first.nSubjects-1
            errorbar(data.Days, In(i, :), InSTDErr(i, :), map{first.nSubjects}, style{:});
        end
        %
        if j<first.nSubjects-1
            subplot(m, n, n + i);
            rowTitle(m, n, 2, 'info fraction [%]');
            %plot(data.Days, InfoFraction(i, :, j) * 100, map{j}, style{:});
            errorbar(data.Days, InfoFraction(i, :, j), InfoFractionSTDErr(i, :, j), map{j}, style{:});
            xlabel('Day');
            set(gca, 'XTick', 1:data.nDays);
            hold on;
        end
    end
    hold off;
end

legendEntries = {};
for j=1:first.nSubjects-1
    legendEntries{j} = num2str(j+1);
end
legendEntries{first.nSubjects} = 'total';
legend(legendEntries);
legend boxoff;
TieAxis(m, n, 1:data.nConditions);
TieAxis(m, n, n + [1:data.nConditions]);
return
%function SocialPottsBatch(objs)
%%
SocialExperimentData;
corrupt = {};
weights = {};
ids = [];
days = [];
groups = [];
maps = [];
ranks = [];
postPP = [];
contacts  = [];
for id=1:length(experiments)
    groupId = 1;
    localId = id;
    for g=1:length(Groups)
        if any(id == Groups(g).idx)
            localId = find(id == Groups(g).idx, 1);
            groupId = g;
            break;
        end
    end
    group = Groups(groupId);
    for day = 1:nDays
        index = (id - 1) * nDays + day;
        ids(index) = id;
        groups(index) = groupId;
        days(index) = day;
        
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        try
            obj = TrackLoad(['../base/Res/' prefix '.obj.mat'], {'Analysis', 'common', 'Hierarchy', 'Interactions'});
        catch me
            MyWarning(me)
            corrupt = {corrupt{:}, prefix};
            continue;
        end
        if ~isfield(obj, 'Hierarchy') || ~isfield(obj, 'Interactions')
            try
                obj2 = TrackLoad(['../interactions/Res/' prefix '.obj.mat'], {'common', 'Hierarchy', 'Interactions'});
            catch
                obj2 = SocialHierarchy(obj);
            end
            if ~isfield(obj2, 'Hierarchy')
                obj2 = SocialHierarchy(obj2);
            end
            if ~isfield(obj2, 'Interactions')
                obj2 = SocialPredPreyModel(obj2);
                obj2 = SocialAnalysePredPreyModel(obj2);
            end
        else
            obj2 = obj;
        end
        maps(:, :, index) = obj2.Hierarchy.ChaseEscape.map;
        ranks(index, :) = obj2.Hierarchy.ChaseEscape.rank;
        postPP(:, :, index) = obj2.Interactions.PredPrey.PostPredPrey;
        contacts(:, :, index) = obj2.Interactions.PredPrey.Contacts;
        if isfield(obj, 'Analysis') && isfield(obj.Analysis, 'Potts')
        else
            obj = SocialPotts(obj);
        end
        for i=1:length(obj.Analysis.Potts.Model)
            weights{i}(index, :) = obj.Analysis.Potts.Model{i}.weights;
        end
    end
end

%%
weightIndex = {};
singleWeightIndex = {};
for i=1:length(obj.Analysis.Potts.Model)
    for s=1:obj.nSubjects
        for j=1:length(obj.Analysis.Potts.Model{i}.labels)
            if size(obj.Analysis.Potts.Model{i}.labels{j}, 2) == 1
                singleWeightIndex{i}(s, j) = any(obj.Analysis.Potts.Model{i}.labels{j}(1, :) == s);
            end
            
            if size(obj.Analysis.Potts.Model{i}.labels{j}, 2) ~= i
                continue;
            end
            weightIndex{i}(s, j) = any(obj.Analysis.Potts.Model{i}.labels{j}(1, :) == s);
        end
    end
end
%%
hold off;
for id = 1:3;
    pairweights = zeros(obj.nSubjects);
    for s1=1:obj.nSubjects
        for s2=1:obj.nSubjects
            if s1 == s2
                continue;
            end
            m = weightIndex{2}(s1, :) & weightIndex{2}(s2, :);
            pairweights(s1, s2) = exp(mean(mean(weights{2}(ids == id, m))));
        end
    end
    Y = mdscale(pairweights, 2);
    arank = mean(ranks(ids == id, :));
    [q, o] = sort(arank);
    plot(Y(:, 1), Y(:, 2), 'k:');
    hold on;
    for i=1:obj.nSubjects
        plot(Y(i, 1), Y(i, 2), 'o', 'MarkerFaceColor', obj.Colors.Centers(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', 40);
        text(Y(i, 1), Y(i, 2), num2str(find(o == i)), 'VerticalAlignment', 'middle' ,'HorizontalAlignment', 'center', 'Color', 'w', 'FontSize', 10, 'FontWeight', 'bold');
        hold on;
    end
end
hold off;
%%
hold off;
for id=1:length(experiments)
    pchase = sum(sum(postPP(:, :, ids == id), 3), 2) ./ sum(sum(contacts(:, :, ids == id), 3), 2);
    pcont = sum(sum(contacts(:, :, ids == id), 3), 2);
    aweight = [];
    for s=1:obj.nSubjects
        v = weights{2}(ids == id, weightIndex{2}(s, :));
        aweight(s) = mean(v(:));
    end
    plot(aweight, pchase, '.');
    hold on;
end
hold off;
return;
