function vec = PottsVector(obj, me, perm)
if ~exist('perm', 'var')
    perm = 1:obj.nSubjects;
end

u_ = unique(double(me.order));
sz = 0;
for u=u_(:)'
    sz = sz + double(obj.nSubjects)^u * double(obj.ROI.nZones)^u;
end
vec = zeros(1, sz);
offset = 1;
for u=u_(:)'
    map = me.order == u;
    w = me.weights(map);
    l = {me.labels{map}};
    svec = double(obj.nSubjects.^(u-1:-1:0));
    zvec = double(obj.ROI.nZones.^(u-1:-1:0));
    for i=1:length(l)
        s = perm(l{i}(1, :))';
        coord = (svec * (s - 1)) * double(obj.ROI.nZones)^u + zvec * (l{i}(2, :)' - 1);
        vec(offset + coord) = w(i);
    end
    offset = offset + double(obj.nSubjects)^u * double(obj.ROI.nZones)^u;
end
