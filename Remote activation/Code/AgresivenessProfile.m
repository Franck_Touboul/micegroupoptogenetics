clf
data = dlmread(['aux/' obj.FilePrefix '.marks'], '\t', [1 0 500 15]);
agresivnessThreshold = 2;

amarks.ids = data(:, 1);
amarks.beg = data(:, 2);
amarks.end = data(:, 3);
amarks.subjects = data(:, 4:5);
amarks.approach = data(:, 6:7);
amarks.leave = data(:, 8:9);
amarks.chase = data(:, 10);
amarks.pred = data(:, 11:12);
amarks.prey = data(:, 13:14);
amarks.artifact = data(:, 15);
amarks.agresivness = data(:, 16);
amarks.chase = amarks.agresivness >= agresivnessThreshold;
%%
local = obj;
r = 1:size(obj.Contacts.Properties.PostMaxSpeed, 2);
%local.Contacts.Behaviors.ChaseEscape = (obj.Contacts.Properties.PostMaxDistance < 10 &  obj.Contacts.Behaviors.ChaseEscape(r)') + (obj.Contacts.Properties.PostMaxSpeed(2, :) > 20) > 1;
%local.Contacts.Behaviors.ChaseEscape = obj.Contacts.Properties.PostMaxSpeed(2, :) > 10 & obj.Contacts.Properties.PostMaxDistance < 10 & obj.Contacts.Behaviors.ChaseEscape(r)';
%local.Contacts.Behaviors.ChaseEscape = obj.Contacts.Behaviors.ChaseEscape(r)' & obj.Contacts.Properties.PostSpeedJitter(2, :) > 2;
[results, match] = SocialBehaviorAnalysisScore(local, amarks, 'AggressiveChase');
fprintf('# detection=%.1f%% (%d), false-alarm=%.1f%% (%d)\n', results.chase(1) / (results.chase(1) + results.chase(3)) * 100, results.chase(1), results.chase(2) / results.chase(5) * 100, results.chase(2));
local = obj;
[results, match] = SocialBehaviorAnalysisScore(local, amarks, 'AggressiveChase');
nbins = 20;
%obj = SocialChaseEscapeProperties(obj);
stat = {};
for ai=1:2
    stat{ai} = [];
end
Stat.X = [];
Stat.Y = [];
for ai = 1:2
    statX = [];
    if ai == 1 
        d = match.analysis(:, find(abs(match.analysis(end, :)) == 1 & match.analysis(6, :) <= agresivnessThreshold));
        style1 = {'b-'};
        style2 = {'b-', 'LineWidth', 2};
    else
        d = match.analysis(:, find(abs(match.analysis(end, :)) == 1 & match.analysis(6, :) >= agresivnessThreshold));
        style1 = {'r-'};
        style2 = {'r-', 'LineWidth', 2};
    end
    %d = match.analysis(:, find(match.analysis(end, :) == 1));
    fields = fieldnames(obj.Contacts.Properties);
    for f=1:length(fields)
        SquareSubplpot(length(fields), f);
        mat = obj.Contacts.Properties.(fields{f});
        if ~strcmp(class(mat), 'double')
            mat = double(mat);
        end
        mat = mat(:, 1:size(obj.Contacts.Properties.Speed, 2));
        statX = [statX, mat'];
        if isvector(mat)
            val = mat(d(1, :));
            [h, x] = hist(val(isfinite(val)), nbins);
        plot(x, h/sum(h), style1{:}); hon;
        else
            [h1, x1] = hist(mat(1, d(1, :)), nbins);
            [h2, x2] = hist(mat(2, d(1, :)), nbins);
        %plot(x1, h1/sum(h1), style1{:}); hon;
        plot(x2, h2/sum(h2), style2{:}); hon;
        end
%         for i=d(1, :)
%             %%
%             if isvector(mat)
%                 plot(ai - 1.5, mat(i), style{:})
%             else
%                 plot(ai - 1.5, mat(:, i), style{:})
%             end
%             hon;
%         end
            title(fields{f})
    end
    Stat.X = [Stat.X; statX];
    Stat.Y = [Stat.Y; ones(size(obj.Contacts.Properties.Speed, 2), 1) * ai];
end