function edges = ModelFindEdges(varargin)
if nargin == 1
    X = Points.Skin.X; Y = Points.Skin.Y; Z = Points.Skin.Z;
    valid = Points.Skin.Area < 0;
else
    X = varargin{1};
    Y = varargin{2};
    Z = varargin{3};
    valid = varargin{4};
end

nX = X; nX(~valid) = nan;
nY = Y; nY(~valid) = nan;
vID = reshape(cumsum(valid(:)), size(valid));

meanX = nanmean(nX, 2);
meanY = nanmean(nY, 2);

diffX = bsxfun(@minus, nX, meanX);
diffY = bsxfun(@minus, nY, meanY);

d = diffX.^2 + diffY.^2;
[mxVal1, mxIdx1] = max(d, [], 2);

diffX = bsxfun(@minus, nX, nX(sub2ind(size(nX), 1:size(nX, 1), mxIdx1(:)'))');
diffY = bsxfun(@minus, nY, nY(sub2ind(size(nY), 1:size(nY, 1), mxIdx1(:)'))');

d = diffX.^2 + diffY.^2;
[~, mxIdx2] = max(d, [], 2);

edges = zeros(size(nX, 1) * 2, 2);
idx = 1;
edges(idx, :) = [vID(1, mxIdx1(1)) vID(1, mxIdx2(1))]; idx = idx + 1;
endID = find(~isnan(mxVal1), 1, 'last');
edges(idx, :) = [vID(endID, mxIdx1(endID)) vID(endID, mxIdx2(endID))]; idx = idx + 1;
i = 1;
while i<endID
    xa1 = nX(i, mxIdx1(i));
    ya1 = nY(i, mxIdx1(i));
    xb1 = nX(i, mxIdx2(i));
    yb1 = nY(i, mxIdx2(i));
    
    offset = 1;
    vis = false;
    finished = false;
    while ~vis
        xa2 = nX(i + offset, mxIdx1(i + offset));
        ya2 = nY(i + offset, mxIdx1(i + offset));
        xb2 = nX(i + offset, mxIdx2(i + offset));
        yb2 = nY(i + offset, mxIdx2(i + offset));
        if isnan(xa2)
            offset = offset + 1;
            if i + offset + 1 > size(nX, 1)
                finished = true;
                break;
            end
        else
            break;
        end
    end
    
    if finished
        break;
    end
    
    if (xa1 - xa2)^2 + (ya1 - ya2)^2 < (xa1 - xb2)^2 + (ya1 - yb2)^2
        edges(idx, :) = [vID(i, mxIdx1(i)) vID(i + offset, mxIdx1(i + offset))]; idx = idx + 1;
        edges(idx, :) = [vID(i + offset, mxIdx2(i + offset)) vID(i, mxIdx2(i))]; idx = idx + 1;
    else
        edges(idx, :) = [vID(i, mxIdx1(i)) vID(i + offset, mxIdx2(i + offset))]; idx = idx + 1;
        edges(idx, :) = [vID(i + offset, mxIdx1(i + offset)) vID(i, mxIdx2(i))]; idx = idx + 1;
    end
    i = i + offset;
end
edges = edges(1:idx-1, :);
