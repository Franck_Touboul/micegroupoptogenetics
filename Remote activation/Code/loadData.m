%% Loading the Excel file for all mice (if needed)
fprintf('# loading data: \n');
global data;
global rawData;
global zones;
global orig;
global socialData;

if (~exist('data') || isempty(data))
    orig.x = [];
    orig.y = [];
    orig.distance = [];
    orig.velocity = [];
    orig.area = [];
    orig.elongation = [];
    
    zones.feeder1 = [];
    zones.feeder2 = [];
    zones.water = [];
    zones.labyrinth = [];
    zones.bigNest = [];
    zones.smallNest = [];
    zones.saftyStrip = [];
    zones.upperSmallNest = [];
    
    for i=1:options.nSubjects
        fprintf('# - subject %d/%d\n', i, options.nSubjects);
        data = xlsread(['subject ' num2str(i)])';
        %
        orig.time       = data(1, :);
        orig.x          = [orig.x; data(3, :)];
        orig.y          = [orig.y; data(4, :)];
        orig.distance   = [orig.distance; data(8, :)];
        orig.velocity   = [orig.velocity; data(9, :)];
        orig.area       = [orig.area;     data(5, :)];
        orig.elongation = [orig.elongation; data(7, :)];
        
        zones.feeder1     = [zones.feeder1; data(18, :)];
        zones.feeder2     = [zones.feeder2; data(20, :)];
        zones.water       = [zones.water; data(19, :)];
        zones.smallNest   = [zones.smallNest; data(16, :)];
        zones.labyrinth   = [zones.labyrinth; data(13, :)];
        zones.bigNest     = [zones.bigNest; data(14, :)];
        zones.saftyStrip  = [zones.saftyStrip; data(21, :)];
        zones.upperSmallNest = [zones.upperSmallNest; data(22, :)];
    end
    orig.nData = length(orig.time);
    zones.all = zones.labyrinth * 0;
    zones.all(zones.feeder1         > 0) = 1;
    zones.all(zones.feeder2         > 0) = 2;
    zones.all(zones.water           > 0) = 3;
    zones.all(zones.smallNest       > 0) = 4;
    zones.all(zones.labyrinth       > 0) = 5;
    zones.all(zones.bigNest         > 0) = 6;
    zones.labels = {'Open Space', 'Feeder #1', 'Feeder #2', 'Water', 'Small-Nest', 'Labyrinth', 'Big-Nest'};
    zones.count = 7;
    fprintf('# - done\n');
    
    orig.colors = {[172 190 206]/255, [208 179 189]/255, [181 73 157]/255, [144 90 209]/255};
    socialData = orig;
else
    fprintf('# - data already loaded\n');
end

rawData = data;