function cmap = red2blue
for i=1:256
%    cmap(i, 1:3) = (i - 1) / 255 * [48, 165, 256] + (256 - i) / 255 * [256, 0, 0];
    cmap(i, 1:3) = (i - 1) / 255 * [0, 0, 256] + (256 - i) / 255 * [256, 0, 0];
end
cmap = cmap / 256;
cmap = colormap(cmap);
