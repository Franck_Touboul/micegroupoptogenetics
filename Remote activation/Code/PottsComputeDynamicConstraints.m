function [m, pci] = PottsComputeDynamicConstraints(data, range, nPerms, confidence)
if ~exist('confidence', 'var')
    confidence = 0.05;
end

dim = 0;
for i=1:length(nPerms)
    if length(nPerms{i}) == 1
        nVals = range^2;
    else
        nVals = range^length(nPerms{i});
    end
    dim = dim + nVals;
end

offset = 1;
c = zeros(1, dim);
zData = double(data - 1);
for i=1:length(nPerms)
    if length(nPerms{i}) == 1
        nVals = range^2;
        c(offset:offset+nVals-1) = histc(zData(2:end, nPerms{i}) + zData(1:end-1, nPerms{i}) * range, 0:nVals-1)';
    else
        nVals = range^length(nPerms{i});
        vec = range.^(length(nPerms{i})-1:-1:0);
        c(offset:offset+nVals-1) = histc(zData(:, nPerms{i}) * vec', 0:nVals-1)';
    end        
    offset = offset + nVals;
end
WaitForLicense('statistics_toolbox');
if nargout > 1
    [m, pci] = binofit(c, size(data, 1), confidence);
else
    m = c / size(data, 1);
end

