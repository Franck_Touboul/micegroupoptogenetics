%function SocialWeightsCompare(obj1, obj2)
%%
options.Beta = 1;
options.Beta = 2^-7;
%options.Beta = 0;
options.IsBestFit = false;
%%
me1 = SocialGetRegPotts(obj1, options.Beta, 3);
me2 = SocialGetRegPotts(obj2, options.Beta, 3);
%me1 = obj1.Analysis.Potts.Model{3};
%me2 = obj2.Analysis.Potts.Model{3};
%%
res = [];
subjPerms = perms(1:obj1.nSubjects);
curr.me = me1;
curr.obj = obj1;
res.vecs = [];
res.probs = [];
for i=1:size(subjPerms, 1)
    res.vecs(i, :) = PottsVectorize_v2(curr.obj, curr.me, subjPerms(i, :));
    p = exp(curr.me.perms * res.vecs(i, :)');
    Z = sum(p);
    p = p / Z;
    res.probs(i, :) = p;
end
res.other.vec = PottsVectorize_v2(obj2, me2, 1:obj2.nSubjects);
%%
if options.IsBestFit
    
    p = exp(me2.perms * res.other.vec');
    Z = sum(p);
    p = p / Z;
    res.other.prob = p;
    %%
    res.d = [];
    for i=1:size(subjPerms, 1)
        res.d(i) = JensenShannonDivergence(res.probs(i, :), res.other.prob');
    end
    [qqq, res.best] = min(res.d);
    res.model(1).vec = res.vecs(res.best, :);
    res.model(2).vec = res.other.vec;
else
    %%
    for i=1:2
        if i==1
            curr.obj = obj1;
            curr.me = me1;
        else
            curr.obj = obj2;
            curr.me = me2;
        end
        nChases = sum(curr.obj.Hierarchy.AggressiveChase.ChaseEscape, 2);
        nChases = nChases + rand(obj.nSubjects, 1) * .5;
        [qqq, order] = sort(nChases);
        seq(order) = 1:4;
        res.model(i).vec = PottsVectorize_v2(curr.obj, curr.me, seq);
        p = exp(curr.me.perms * res.model(i).vec');
        Z = sum(p);
        
        p = p / Z;
        res.model(i).prob = p;
    end
end
%%
W1 = res.model(1).vec;
W2 = res.model(2).vec;
%
cmap = MyCategoricalColormap;
for o=1:3
    subplot(2,2,o);
    w1 = W1(me1.order == o);
    w2 = W2(me2.order == o);
    valid = ~(w1 == 0 | w2 == 0);
    [c, pval] = corr(w1(valid)', w2(valid)');
    plot(w1(valid), w2(valid), '.', 'color', cmap(o, :));
    hon
    a = axis;
    for i=1:size(subjPerms, 1)
        lw1 = res.vecs(i, me1.order == o);
        lvalid = ~(lw1 == 0 | w2 == 0);
        p = polyfit(lw1(valid), w2(valid), 1);
        plot([a(1) a(2)], polyval(p, [a(1) a(2)]), 'color', [.8 .8 .8], 'LineWidth', 2);
    end
    p = polyfit(w1(valid), w2(valid), 1);
    plot([a(1) a(2)], polyval(p, [a(1) a(2)]), 'color', cmap(o, :), 'LineWidth', 2);
    hoff;
    title(['corr=' num2str(c) ', pval=' num2str(pval)]);
end