clf
SocialExperimentData;
ididx = 1;
for ID_ = 1:3;
    %%
    nDays = 1;
    days = 1;
    
    index = 1;
    MI = zeros(nSubjects, nSubjects, length(experiments) * nDays);
    ENTROPY = zeros(nSubjects, length(experiments) * nDays);
    ID = zeros(1, length(experiments) * nDays);
    %RANK = zeros(nSubjects, length(experiments) * nDays);
    for id=ID_
        fprintf('# experiment #%d\n', id);
        first = TrackLoad({id 1}, {'Hierarchy'});
        obj = TrackLoad({id days}, {'zones', 'ROI', 'valid', 'Hierarchy'});
        %obj.Hierarchy.Group = first.Hierarchy.Group;
        fprintf('# - compute MI\n');
        [q, mi, entropy] = SocialMutualInformation(obj);
        
        ENTROPY(:, index) = entropy(:);
        MI(:, :, index) = mi;
        ID(index) = id;
        %RANK(:, index) = obj.Hierarchy.Group.AggressiveChase.rank;
        index = index + 1;
    end
    %%
    pairwise = [];
    pairwiseP = [];
    totalPairwiseP = [];
    condP = [];
    for i=ID_
        mi = MI(:, :, ID == i);
        entropy = ENTROPY(:, ID == i);
        
        condP = [condP; diag(mi) ./ entropy];
        
        mi(1:obj.nSubjects+1:end) = nan;
        pairwise = [pairwise; mi(:)];
        
        miP = mi ./ repmat(entropy, 1, obj.nSubjects);
        pairwiseP = [pairwiseP; miP(:)];
        
        mi(1:obj.nSubjects+1:end) = 0;
        totalPairwiseP = [totalPairwiseP; sum(mi, 2) ./ entropy];
    end
    %%
    cmap = MyCategoricalColormap;
    %plot(pairwiseP, ididx, 'o', 'MarkerEdgeColor', 'none','MarkerFaceColor', cmap(2, :), 'LineWidth', 2); hon;
    %plot(totalPairwiseP, ididx, 'o', 'MarkerEdgeColor', 'none','MarkerFaceColor', cmap(6, :), 'LineWidth', 2);
    %plot(condP, ididx, 'o', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', cmap(4, :), 'LineWidth', 2);
    plot(totalPairwiseP, condP, 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none'); 
    a = axis; axis([0 max(a(2), a(4)), 0, max(a(2), a(4))]);
    axis square; 
    hon;
    drawnow;
    
%     style = {':'};
%     subplot(2,1,1);
%     
%     nbins = 30;
%     m1 = min([min(pairwiseP), min(condP) min(totalPairwiseP)]);
%     m2 = max([max(pairwiseP), max(condP) max(totalPairwiseP)]);
%     x = sequence(m1, m2, nbins);
%     hpw = histc(pairwiseP, x);
%     htpw = histc(totalPairwiseP, x);
%     hc = histc(condP, x);
%     % [hpw, xpw] = hist(pairwiseP,25);
%     % [hc, xc] = hist(condP,25);
%     cmap = MySubCategoricalColormap;
%     plot(x*100, hpw/sum(hpw)*100, style{:}, 'Color', cmap(2, :), 'LineWidth', 2);
%     hold on;
%     plot(x*100, hc/sum(hc)*100, style{:}, 'Color', cmap(4, :), 'LineWidth', 2);
%     plot(x*100, htpw/sum(htpw)*100, style{:}, 'Color', cmap(6, :), 'LineWidth', 2);
%     hold off;
%     prettyPlot('', 'information percentage [%]', 'percentage that fall in bin [%]');
%     %legend('Pair: I(x_i; x_j)', 'Multiple: I(x_i; x_{i~=j})');
%     legend('single', 'multiple', 'additive');
%     legend boxoff;
    
    ididx = ididx + 1;
end
a = axis; plot([a(1) a(2)], [a(3) a(4)], 'k:');
xlabel('Additive pairwise information');
ylabel('Group information');
hoff;
%%
if (1==2)
    %%
    p = 'Graphs/SocialCorrelationBatch/';
    mkdir(p);
    figure(1);
    saveFigure([p obj.FilePrefix '']);
end
