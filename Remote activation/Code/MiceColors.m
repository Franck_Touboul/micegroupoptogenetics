function [hsv, rgb, histogram] = MiceColors(prototype)
if nargin == 1
    usePrototype = true;
end
%%
persistent phsv;
persistent prgb;
persistent phistogram;
persistent pprototype;
if usePrototype
    if isempty(phsv) || ~strcmp(pprototype, prototype)
        if isempty(prototype)
            obj = TrackLoad('Res/Enriched.exp0001.day01.cam04.obj.mat', {'Colors'});
        else
            obj = TrackLoad(prototype, {'Colors'});
        end
        histogram.h = obj.Colors.Histrogram.H;
        histogram.s = obj.Colors.Histrogram.S;
        histogram.v = obj.Colors.Histrogram.V;
        histogram.bins = obj.Colors.Bins;
        pprototype = prototype;
%         hsv = [];
%         for i=1:obj.nSubjects
%             h = sum(obj.Colors.Histrogram.H(i, :) .* obj.Colors.Bins);
%             s = sum(obj.Colors.Histrogram.S(i, :) .* obj.Colors.Bins);
%             v = sum(obj.Colors.Histrogram.V(i, :) .* obj.Colors.Bins);
%             hsv = [hsv; h s v];
%         end
%         rgb = reshape(hsv2rgb(reshape(hsv, 1, 4, 3)), [4, 3, 1]);
        rgb = obj.Colors.Centers;
        hsv = reshape(rgb2hsv(reshape(rgb, 1, 4, 3)), [4, 3, 1]);
        phsv = hsv;
        prgb = rgb;
        phistogram = histogram;
    else
        hsv = phsv;
        rgb = prgb;
        histogram = phistogram;
    end
else
    rgb = [148, 54, 194; 135, 46, 100; 252, 182, 177; 163, 255, 230] / 255;
    hsv = reshape(rgb2hsv(reshape(rgb, 1, 4, 3)), [4, 3, 1]);
    histogram = [];
end
%%
