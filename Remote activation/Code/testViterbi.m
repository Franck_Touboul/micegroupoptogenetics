model = struct;

model.states(1).func = @(x) .5;
model.states(2).func = @(x) x==2;
w = 3;
model.trans = [w  1; 1 w];

model = validateModel(model);

backtrack = ModelViterbiSequence(model, [1 1 1 2 2 2 1 1 1])
