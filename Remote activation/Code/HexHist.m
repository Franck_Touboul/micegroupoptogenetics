function [bins, range] = HexHist(data, count, varargin)
%
%
%     |    |
%     |    |
% ____| \__/  \______________________________
%     |_/  \__/
%    /  \__/  \
% ___\__/__\__/______________________________
%     |    |
%     |    |
x = data(:, 1);
y = data(:, 2);
%%
i = find(strcmpi(varargin, 'range'));
if isempty(i)
    mx1 = min(x);
    mx2 = max(x);
    my1 = min(y);
    my2 = max(y);
else
    range = varargin{i+1};
    mx1 = range(1);
    mx2 = range(2);
    my1 = range(3);
    my2 = range(4);
end

%%
%mx1 = 0;
%mx2 = 1;
dxs = (mx2 - mx1) / (count * 3 - 1);
%%
% my1 = 0;
% my2 = 20;
dys = (my2 - my1) / (count * 2 - 1);
%%
bx = floor((x - mx1) / dxs) + 1;
by = floor((y - my1) / dys) + 1;

ix = x - mx1 - (bx - 1) * dxs;
iy = y - my1 - (by - 1) * dys;

underSlash = (ix / dxs) > (iy / dys);
underBSlash = (ix / dxs) < ((dys - iy) / dys);

%%
border1 = mod(bx, 3) == 0 & mod(bx, 6)/3 == mod(by, 2);
bx(border1) = bx(border1) + (2 * underSlash(border1) - 1);

border2 = mod(bx, 3) == 0 & mod(bx, 6)/3 == 1-mod(by, 2);
bx(border2) = bx(border2) - (2 * underBSlash(border2) - 1);

%%
binx = floor(bx / 3) + 1;
biny = floor((by - mod(binx, 2)) / 2) + 1;
%%
bins = zeros(count, count);
biny(biny > count) = count;
binx(binx > count) = count;
binx(binx < 1) = 1;
biny(biny < 1) = 1;
%idx = sub2ind([count count], biny(biny <= count & binx <= count), binx(biny <= count & binx <= count));
idx = sub2ind([count count], biny, binx);
bins(1:length(bins(:))) = histc(idx, 1:length(bins(:)));
range = [mx1, mx2, my1, my2];
%%
if nargout == 0
    HexPlot(bins, range, varargin{:});
%     hold on;
%     plot(x, y, '.');
%     hold off;
end