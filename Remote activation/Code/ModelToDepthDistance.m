function [d, pos] = ModelToDepthDistance(Model, Data)

if isvector(Model)
    fields = {...
        'Angle',0,...
        'MajorAxis',1,...
        'DX',0,...
        'DY',0,...
        'Height', 0.05,...
        'BodyYaw', 0 ...
        'BodyFat', 1, ...
        'RearFat', 1 ...
        };
    
    Param = struct(fields{:});
    p = Model;
    for i=1:length(p)
        Param.(fields{2*i-1}) = p(i);
    end
else
    Param = Model;
end
pos = ModelInSpace(Param);
%%
if nargin > 1
    warning('off', 'MATLAB:DelaunayTri:DupPtsWarnId')
    dt = DelaunayTri(pos);
    id = dt.nearestNeighbor(Data);
    d1 = mean(sqrt(sum((Data - pos(id, :)).^2, 2)));

    dt = DelaunayTri(Data);
    id = dt.nearestNeighbor(pos);
    d2 = mean(sqrt(sum((pos - Data(id, :)).^2, 2)));
    d = (d1 + d2) / 2;
else
    d = [];
end
if nargout == 0
    plot3(pos(:, 1), pos(:, 2), pos(:, 3), '.')
end