function [SkinDistance, overflow] = SymmModel(vX, vY, vec)
initPos = vec(1:2);
totalLen = vec(3);
angles = vec(4:end);
output = nargout == 0;

if output
    cmap = MySubCategoricalColormap;
    a = axis;
end
%%
%vX = X(map);
%vY = Y(map);
Sym = struct();
Sym.nSegments = length(angles);
Sym.Angle = angles; 
Sym.Length = ones(1, Sym.nSegments) * totalLen / (Sym.nSegments);
Sym.RelAngle = zeros(1, Sym.nSegments);

Sym.RelAngle(1) = Sym.Angle(1);
Sym.Orth = zeros(2, Sym.nSegments);
Sym.Pos = zeros(2, Sym.nSegments-1);

distToOrth = zeros(length(vX), Sym.nSegments-1);
distToSegm = zeros(length(vX), Sym.nSegments-1);

% compute segments
Sym.Edges = struct();
Sym.Edges.Pos = zeros(2, Sym.nSegments+1);
Sym.Edges.Pos(:, 1) = initPos(:);
for i=2:Sym.nSegments+1
    if i<=Sym.nSegments
        Sym.RelAngle(i) = Sym.RelAngle(i-1) + Sym.Angle(i);
    end
    Sym.Edges.Pos(:, i) = Sym.Edges.Pos(:, i-1) + Sym.Length(i-1) * [cos(Sym.RelAngle(i-1)); sin(Sym.RelAngle(i-1))];
    Sym.Pos(:, i-1) = Sym.Edges.Pos(:, i-1) + (Sym.Edges.Pos(:, i) - Sym.Edges.Pos(:, i-1))/2;
    Sym.Orth(:, i-1) = [cos(Sym.RelAngle(i-1) + pi/2); sin(Sym.RelAngle(i-1) + pi/2)];
    Sym.Orth(:, i-1) = Sym.Orth(:, i-1) / norm(Sym.Orth(:, i-1));
    %
    distToOrth(:, i-1) = (vX - Sym.Pos(1, i-1)) * Sym.Orth(2, i-1) - (vY - Sym.Pos(2, i-1)) * Sym.Orth(1, i-1);
    distToSegm(:, i-1) = -(vX - Sym.Pos(1, i-1)) * sin(Sym.RelAngle(i-1)) + (vY - Sym.Pos(2, i-1)) * cos(Sym.RelAngle(i-1));
    %
    if output
        hon
        plot([Sym.Edges.Pos(1, i-1) Sym.Edges.Pos(1, i)], [Sym.Edges.Pos(2, i-1) Sym.Edges.Pos(2, i)], 'ro-', 'Color', cmap(i-1, :), 'LineWidth', 2, 'MarkerFaceColor', cmap(i-1, :));
        plot(...
            [Sym.Pos(1, i-1) Sym.Pos(1, i-1)+Sym.Orth(1, i-1)], ...
            [Sym.Pos(2, i-1) Sym.Pos(2, i-1)+Sym.Orth(2, i-1)], 'Color', cmap(i-1, :), 'LineWidth', 2);
        plot(...
            [Sym.Pos(1, i-1) Sym.Pos(1, i-1)-Sym.Orth(1, i-1)], ...
            [Sym.Pos(2, i-1) Sym.Pos(2, i-1)-Sym.Orth(2, i-1)], '--', 'Color', cmap(i-1, :), 'LineWidth', 2);
        hoff
        axis(a);
    end
end

[~, idx] = min(abs(distToOrth), [], 2);
skindist = distToSegm(sub2ind(size(distToOrth), (1:size(distToOrth,1))', idx));
if output; hon; end
SkinDistance = zeros(2, Sym.nSegments);
for i=1:Sym.nSegments
    if output
        plot(vX(idx == i), vY(idx == i), 'o', 'Color', cmap(i, :), 'MarkerFaceColor', cmap(i, :));
        plot(vX(idx == i & skindist<0), vY(idx == i & skindist<0), 'ko');
    end
    s = skindist(idx == i);
    poss = s > 0;
    SkinDistance(1, i) = mean( s( poss));
    SkinDistance(2, i) = mean(-s(~poss));
end
SkinDistance(isnan(SkinDistance)) = 0;
if output; hoff; end

overflow = ...
    any((vX - Sym.Edges.Pos(1, 1)) * Sym.Orth(2, 1) - (vY - Sym.Edges.Pos(2, 1)) * Sym.Orth(1, 1) < 0) || ...
    any((vX - Sym.Edges.Pos(1, end)) * Sym.Orth(2, end) - (vY - Sym.Edges.Pos(2, end)) * Sym.Orth(1, end) > 0);

