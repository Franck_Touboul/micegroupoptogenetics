sFrame = 10000;
data = mmread('Exp1 day 3.mpg', sFrame);
nFrames = abs(data.nrFramesTotal);
tic;
for r=sFrame:nFrames
    curr = mmread('Exp1 day 3.mpg', r);
    RT = toc / (r - sFrame + 1) * data.rate;
    fprintf('# - frame %6d/%6d (%6.2fxiRT)\n', r, nFrames, RT);
end
