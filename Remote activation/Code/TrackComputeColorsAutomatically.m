function obj = TrackComputeColorsAutomatically(obj)
obj.MinNumberOfColorFrames = 1;
obj.MinNumberOfColorClusters = obj.MinNumberOfColorFrames * 4;
obj.MinNumberOfClustersPerSubject = 25;
obj.MinColorHomogeneity = 0.8;
bins = sequence(0, 1, obj.nColorBins);
%%
Center.pixels = [];
Center.clusters = [];
Center.frames = [];
Body.pixels = [];
Body.clusters = [];
Body.frames = [];

BkgSum = zeros(1,3);
BkgCount = 0;
currCluster = 0;
frame = 0;
count = zeros(1, obj.nSubjects);
n = reprintf(0, '# - frame no. %d (%d/%d frame, %d/%d clusters, %d %d %d %d cps)', 0, frame, obj.MinNumberOfColorFrames, currCluster, obj.MinNumberOfColorClusters, count(1), count(2), count(3), count(4));
sorted = [];
while currCluster < obj.MinNumberOfColorClusters || frame < obj.MinNumberOfColorFrames || any(obj.MinNumberOfClustersPerSubject > count(1:obj.nSubjects))
    img = [];
    while isempty(img)
        f = randi(obj.nFrames, 1, 1);
        [obj, img] = TrackSimpleSegmentFrame(obj, f);
        if ~isempty(img) && ~any(img.segmented(:))
            img = [];
        end
    end
    n = reprintf(0, '# - frame no. %d (%d/%d frame, %d/%d clusters, %d %d %d %d cps)\n', f, frame, obj.MinNumberOfColorFrames, currCluster, obj.MinNumberOfColorClusters, count(1), count(2), count(3), count(4));
    conn = regionprops(img.segmented, 'Solidity', 'Centroid', 'PixelIdxList');
    labels = zeros(size(img.segmented, 1), size(img.segmented, 2));
    nLabels = 0;
    for i=1:length(conn)
        nLabels = nLabels + 1;
        labels(conn(i).PixelIdxList) = nLabels;
    end
    img.scaled = im2double(imresize(img.orig, obj.VideoScale));
    for c=1:3
        component = img.hsv(:, :, c);
        if c==1
            obj.Colors.FillerHistogram.H = obj.Colors.FillerHistogram.H + histc(component(~img.segmented), bins)' / sum(~img.segmented(:));
        elseif c==2
            obj.Colors.FillerHistogram.S = obj.Colors.FillerHistogram.S + histc(component(~img.segmented), bins)' / sum(~img.segmented(:));
        else
            obj.Colors.FillerHistogram.V = obj.Colors.FillerHistogram.V + histc(component(~img.segmented), bins)' / sum(~img.segmented(:));
        end
        BkgSum(c) = BkgSum(c) + sum(component(~img.segmented));
    end
    BkgCount = BkgCount + sum(~img.segmented(:));
    %%
    for i=unique(labels(labels > 0))'
        pixels = [];
        for c=1:3
            component = img.hsv(:, :, c);
            pixels = [pixels; component(labels == i)'];
        end
        Body.clusters = [Body.clusters, ones(1, size(pixels, 2)) * currCluster];
        Body.frames = [Body.frames, ones(1, size(pixels, 2)) * f];
        x = round(conn(i).Centroid(2));
        y = round(conn(i).Centroid(1));
        if labels(x, y) == i
            Center.pixels = [Center.pixels, reshape(img.scaled(x, y, :), 3, 1)];
            Center.clusters = [Center.clusters, currCluster];
            Center.frames = [Center.frames, f];
        end
        Body.pixels = [Body.pixels, pixels];
        currCluster = currCluster + 1;
    end
    %% find colors by prototypes
    %if ~(currCluster < obj.MinNumberOfColorClusters || frame < obj.MinNumberOfColorFrames)
        obj.Colors.Filler = BkgSum / BkgCount;
        [obj, sorted, center, count] = MatchColorsToSubjects(obj, Body, Center, bins);
    %end
    %%
    %pause;
    if obj.Output
        scaled = imresize(labels > 0, 1/obj.VideoScale);
        boundries = (imdilate(scaled, ones(5,5)) - scaled) ~= 0;
        orig = img.orig;
        for i=1:3
            slice = img.orig(:, :, i);
            slice(boundries) = (i ~= 3) * 255;
            orig(:, :, i) = slice;
        end
        subplot(1,5,1:4);
        imagesc(orig);
        subplot(1,5,5);
%        imagesc(reshape(Center.pixels', size(Center.pixels, 2), 1, 3));
        imagesc(sorted);
        drawnow;
        title(f);
    end
    %%
    frame = frame + 1;
    %break;
end
reprintf(n, '# - frame %d/%d (found %d clusters)\n', frame, obj.nColorFrames, currCluster);
%%
% bkg = BkgSum / BkgCount;
% valid = true(1, size(Body.pixels, 2));
% prevValid = false(valid);
% miceHSV = MiceColors;
% while any(prevValid ~= valid)
%     prevValid = valid;
%     [center, clusters, d, dis] = kmeans(Body.pixels(:, valid)', obj.nSubjects+1, 'start', [miceHSV; bkg]);
%     dis = [dis, sqrt(sum((Body.pixels(:, valid) - repmat(bkg', 1, sum(valid))).^2))'];
%     [m, center] = min(dis, [], 2);
%     %%
%     for i=unique(Body.clusters(valid))'
%         c = center(Body.clusters(valid) == i);
%         id = nan;
%         for k=1:obj.nSubjects
%             if mean(c == k) > obj.MinColorHomogeneity
%                 id = k;
%                 break;
%             end
%         end
%         center(Body.clusters(valid) == i) = id;
%     end
%     valid(valid) = ~isnan(center);
% end

%% plot
imagesc(sorted);
if 1==2
validClusters = Body.clusters;
for i=1:obj.nSubjects+1
    count = 1;
    u = unique(validClusters(center == i));
    for j=u(:)'
        %        text(i, count, num2str(Center.frames(:, Center.clusters == j)), 'color', 'w');
        c = Center.pixels(:, Center.clusters == j);
        if any(Center.clusters == j)
            text(i, count, sprintf('%.2f ', reshape(rgb2hsv(reshape(c, [1 1 3])), [1 3 1])), 'color', 'w');
            count = count + 1;
        end
    end
end
end
%%
if obj.OutputToFile
    fprintf('# - saving data\n');
    TrackSave(obj);
end

if obj.OutputInOldFormat
    filename = [obj.OutputPath obj.FilePrefix '.colors.png'];
    fprintf(['# - saving to file: ''' filename '''\n']);
    print(gcf, filename, '-dpng');
end
%%
% [miceHSV, miceRGB] = MiceColors;
% C = makecform('srgb2lab');
%
% miceLAB = reshape(applycform(reshape(miceRGB, [1,4,3]), C), [obj.nSubjects, 3, 1]);
% PixelsLAB = reshape(applycform(reshape(Body.pixels', [1,size(Body.pixels, 2),3]), C), [size(Body.pixels, 2), 3, 1]);
% d = [];
% for i=1:size(miceLAB, 1)
%     for j=1:size(PixelsLAB,1)
%  %      d(i,j) = deltaE2000(miceLAB(i,:), PixelsLAB(j,:));
%         d(i,j) = sum((miceLAB(i,:) - PixelsLAB(j,:)).^2);
%     end
% end
% [m, center] = min(d, [], 1);

function [obj, sorted, center, count] = MatchColorsToSubjects(obj, Body, Center, bins)
obj.Colors.FillerHistogram.H = zeros(1, obj.nColorBins);
obj.Colors.FillerHistogram.S = zeros(1, obj.nColorBins);
obj.Colors.FillerHistogram.V = zeros(1, obj.nColorBins);
%%
valid = true(1, size(Body.pixels, 2));
if isfield(obj, 'Prototype') && ~isempty(obj.Prototype)
    [miceHSV, miceRGB, miceHist]  = MiceColors(obj.Prototype);
else
    [miceHSV, miceRGB, miceHist] = MiceColors('');
end
if ~isempty(miceHist)
    %%
    [q, idx] = histc(Body.pixels(1, :), miceHist.bins); p = miceHist.h(:, idx);
    [q, idx] = histc(Body.pixels(2, :), miceHist.bins); p = p .* miceHist.s(:, idx);
    [q, idx] = histc(Body.pixels(3, :), miceHist.bins); p = p .* miceHist.v(:, idx);

    [q, idx] = histc(Body.pixels(1, :), bins); bp = obj.Colors.FillerHistogram.H(:, idx);
    [q, idx] = histc(Body.pixels(2, :), bins); bp = bp .* obj.Colors.FillerHistogram.S(:, idx);
    [q, idx] = histc(Body.pixels(3, :), bins); bp = bp .* obj.Colors.FillerHistogram.V(:, idx);
    [m, center]    = max([p; bp], [], 1);
else
    d1 = dist2(Body.pixels(1, :)', miceHSV(:, 1));
    d2 = dist2(Body.pixels(1, :)', miceHSV(:, 1)-1);
    d3 = dist2(Body.pixels(1, :)', miceHSV(:, 1)+1);
    d = min(min(d1, d2), d3);
    bkgd = dist2(Body.pixels(3, :)', [miceHSV(:, 3); obj.Colors.Filler(3)]);
    [m, bkgCenter] = min(bkgd, [], 2);
    [m, center]    = min(d, [], 2);
    center(bkgCenter == obj.nSubjects +1) = obj.nSubjects +1;
end
%% filter centers (to be unique)
for i=unique(Body.clusters(valid))
    c = center(Body.clusters(valid) == i);
    id = nan;
    for k=1:obj.nSubjects+1
        if mean(c == k) > obj.MinColorHomogeneity
            id = k;
            break;
        end
    end
    center(Body.clusters(valid) == i) = id;
end
%% compute colors
obj.Colors.FillerHistogram.H = obj.Colors.FillerHistogram.H / sum(obj.Colors.FillerHistogram.H);
obj.Colors.FillerHistogram.S = obj.Colors.FillerHistogram.S / sum(obj.Colors.FillerHistogram.S);
obj.Colors.FillerHistogram.V = obj.Colors.FillerHistogram.V / sum(obj.Colors.FillerHistogram.V);

validClusters = Body.clusters(valid);
sorted = [];
count = zeros(1, obj.nSubjects+1);
for i=1:obj.nSubjects+1
    u = unique(validClusters(center == i));
    subject = [];
    for j=u(:)'
        subject = [subject, Body.pixels(:, Body.clusters == j)];
        if any(Center.clusters == j)
            count(i) = count(i) + 1;
            sorted(count(i), i, :) = Center.pixels(:, Center.clusters == j);
        end
    end
    if ~isempty(subject)
        if i<= obj.nSubjects
            obj.Colors.Histrogram.H(i, :) = histc(subject(1, :), bins)' / size(subject, 2);
            obj.Colors.Histrogram.S(i, :) = histc(subject(2, :), bins)' / size(subject, 2);
            obj.Colors.Histrogram.V(i, :) = histc(subject(3, :), bins)' / size(subject, 2);
        end
    else
        obj.Colors.Histrogram.H(i, :) = 0;
        obj.Colors.Histrogram.S(i, :) = 0;
        obj.Colors.Histrogram.V(i, :) = 0;
    end
    
    if count(i) > 1
        obj.Colors.Centers(i, :) = reshape(mean(sorted(1:count(i), i, :), 1), [1 3]);
    end
end
