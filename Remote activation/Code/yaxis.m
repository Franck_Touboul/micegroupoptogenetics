function yaxis(y1, y2)
a = axis;
if nargin == 1
    if length(y1) == 2
        y2 = y1(2);
        y1 = y1(1);
    else
        y2 = a(2);
    end
else
    if isnan(y1)
        y1 = a(1);
    end
end
axis([a(1) a(2) y1 y2]);