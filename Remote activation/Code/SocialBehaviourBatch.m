experiments = {...
    'Enriched.exp0006.day%02d.cam01',...
    'Enriched.exp0005.day%02d.cam04',...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam01',...
    'SC.exp0007.day%02d.cam04',...
    };

E.map = logical([1 1 1 0 1 0 1 0 1 0 0 0 0]);
E.idx = find(E.map);

SC.map = ~E.map;
SC.idx = find(SC.map);

colors = [0 0 1; 1 0 0];
colors = [80 171 210; 232 24 46] / 255;

titles = {'Enriched', 'Control'};

nrows = 4;
%%
% for id=1:length(experiments)
%     DJS = [];
%     for day = 1:nDays
%         prefix = sprintf(experiments{id}, day);
%         fprintf('# -> %s\n', prefix);
%         obj = trackload([obj.OutputPath prefix '.obj.mat'], {'zones', 'common', 'Interactions'});
%         obj = SocialPredPreyModel2(obj);
%         
%     end
% end

%%
nDays = 4;

all.PostPred = [];
all.PostPrey = [];
all.PrePred = [];
all.PrePrey = [];

all.PostPredPrey = [];

all.Contacts = [];
all.id = [];
all.day = [];


%%
index = 1;
for id=1:length(experiments)
    DJS = [];
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        obj = trackload([obj.OutputPath prefix '.obj.mat'], {'zones', 'common', 'Interactions'});
        obj = SocialAnalysePredPreyModel(obj);
        curr = obj.Interactions.PredPrey;
        
        all.PostPred(:, :, index) = curr.PostPred;
        all.PostPrey(:, :, index) = curr.PostPrey;
        all.PrePred(:, :, index) = curr.PrePred;
        all.PrePrey(:, :, index) = curr.PrePrey;
        all.PostPredPrey(:, :, index) = curr.PostPredPrey;
        all.Contacts(:, :, index) = curr.Contacts;
        all.id(index)  = id;
        all.day(index)  = day;
        index = index + 1;
    end
end
%%
E.fullmap = zeros(1, length(E.map) * nDays);
for i=1:length(E.idx)
    E.fullmap = E.fullmap | all.id ==  E.idx(i);
end

SC.fullmap = zeros(1, length(E.map) * nDays);
for i=1:length(SC.idx)
    SC.fullmap = SC.fullmap | all.id ==  SC.idx(i);
end
%% chase map
for id=1 %:length(experiments)
    for day = 1:nDays
        %%
        prefix = sprintf(experiments{id}, day);
        obj = trackload([obj.OutputPath prefix '.obj.mat'], {'zones', 'common', 'Interactions'});
        obja = trackload(['../base/Res/' prefix '.obj.mat'], 'BkgImage');
        obj.BkgImage = obja.BkgImage > mean(obja.BkgImage(:));
        X = [];
        Y = [];
        label = [];
        for m1=1:obj.nSubjects
            for m2=1:obj.nSubjects
                events = obj.Interactions.PredPrey.events{m1,m2};
                for i = 1:length(events)
                    c1 = events{i}.BegFrame + find(events{i}.data == 4, 1) - 1;
                    X = [X, obj.x(m1, c1)];
                    Y = [Y, obj.y(m1, c1)];
                    label = [label, m1];
                end
            end
        end
        p = randperm(length(X));
        X = X(p);
        Y = Y(p);
        label = label(p);
        %%
        clf
        img = repmat(double(sum(obja.BkgImage, 3) > 3 * mean(obja.BkgImage(:))) * .5, [1, 1, 3]);
        imshow(img);
        hold on;
        [c, n] = hist3([X', Y'], [10 20]);
        [x, y] = meshgrid(n{1}, n{2});
        contour(x', y', c);
        colormap cool
        %
        for i=1:length(label)
            plot(X(i), Y(i), 'o', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', obj.Colors.Centers(label(i), :), 'MarkerSize', 3);
        end
        hold off;
    end
end

%% number of chases
value = [];
err = [];

dayvalue = [];
dayerr = [];
for n=1:2
    if n==1
        curr = E;
    else
        curr = SC;
    end
    currvalue = [];
    currDayvalue = [];
    for i=1:length(curr.idx)
        map = all.id ==  curr.idx(i);
        chaseEsc = sum(all.PostPredPrey(:, :, map), 3);
        count = sum(all.Contacts(:, :, map), 3); count = max(count, count');
        currvalue(i) = sum(chaseEsc(:)) ./ sum(count(:)) * 100;
        %% by day
        chaseEsc = sum(sum(all.PostPredPrey(:, :, map), 1), 2);
        count = sum(sum(all.Contacts(:, :, map), 1), 2);
        currDayvalue(i, :) = reshape(chaseEsc ./ count,1,nDays) * 100;
    end    
    value(n) = mean(currvalue);
    err(n) = stderr(currvalue);
    
    dayvalue(n, :) = mean(currDayvalue, 1);
    dayerr(n, :) = stderr(currDayvalue);
end
%
subplot(nrows, 3, 1);
bar(1, value(1), 'FaceColor', colors(1,:)); hold on;
bar(2, value(2), 'FaceColor', colors(2,:));
set(gca, 'XTick', 1:2, 'XTickLabel', titles)
errorbar(value, err, 'k.');
hold off;
a = axis;
axis([.4 2.6 a(3) a(4)]);
ylabel('percentage [%]');
title('% of contacts that end with a chase');
box off
%
subplot(nrows, 3, 2:3);
for n=1:2
    nvalues = size(dayvalue, 2);
    for i=1:nvalues
        bar(n-.3 + .6*(i-1)/(nvalues-1), dayvalue(n, i), .6/nvalues, 'FaceColor', colors(n,:));
        hold on;
        errorbar(n-.3 + .6*(i-1)/(nvalues-1), dayvalue(n, i), dayerr(n, i), 'k.');
    end
end
hold off;
axis([0.4 2.6 a(3) a(4)]);
set(gca, 'XTick', 1:2, 'XTickLabel', titles)
title('by days');
box off

%% number of contacts
value = [];
err = [];

dayvalue = [];
dayerr = [];
for n=1:2
    if n==1
        curr = E;
    else
        curr = SC;
    end
    currvalue = [];
    currDayvalue = [];
    for i=1:length(curr.idx)
        map = all.id ==  curr.idx(i);
        count = sum(all.Contacts(:, :, map), 3); count = max(count, count');
        currvalue(i) = sum(count(:));
        %% by day
        count = sum(sum(all.Contacts(:, :, map), 1), 2);
        currDayvalue(i, :) = reshape(count,1,nDays);
    end    
    value(n) = mean(currvalue);
    err(n) = stderr(currvalue);
    
    dayvalue(n, :) = mean(currDayvalue, 1);
    dayerr(n, :) = stderr(currDayvalue);
end
%
subplot(nrows, 3, 4);
bar(1, value(1), 'FaceColor', colors(1,:)); hold on;
bar(2, value(2), 'FaceColor', colors(2,:));
set(gca, 'XTick', 1:2, 'XTickLabel', titles)
errorbar(value, err, 'k.');
hold off;
a = axis;
axis([.4 2.6 a(3) a(4)]);
ylabel('count');
title('number of contacts');
box off
%
subplot(nrows, 3, 5:6);
for n=1:2
    nvalues = size(dayvalue, 2);
    for i=1:nvalues
        bar(n-.3 + .6*(i-1)/(nvalues-1), dayvalue(n, i), .6/nvalues, 'FaceColor', colors(n,:));
        hold on;
        errorbar(n-.3 + .6*(i-1)/(nvalues-1), dayvalue(n, i), dayerr(n, i), 'k.');
    end
end
hold off;
a = axis;
axis([0.4 2.6 a(3) a(4)]);
set(gca, 'XTick', 1:2, 'XTickLabel', titles)
title('by days');
box off


%% percentage chase-escape
for n=1:2
    if n==1
        curr = E;
    else
        curr = SC;
    end
    map = all.id * 0;
    sym = {'x', 's', 'o', '^', '*', 'd', 'h'};
    stat.pred = [];
    stat.prey = [];
    %stat.pChase = 
    for i=1:length(curr.idx)
        c = all.Contacts(:, :, all.id ==  curr.idx(i));
        %c(c == 0) = 1;
        c = max(sum(c, 3), sum(c, 3)');
        c(1:obj.nSubjects+1:end) = 1;
        aggr = sum(all.PostPredPrey(:, :, all.id ==  curr.idx(i)), 3);
        initiate = sum(all.PrePred(:, :, all.id ==  curr.idx(i)), 3);
%         cchase = mean(sum(aggr ./ c, 2), 3);
%         cescape = mean(sum(aggr ./ c, 1), 3);
        cchase = sum(aggr, 2) ./ (sum(c, 2) - obj.nSubjects);
        cescape = sum(aggr, 1) ./ (sum(c, 1)  - obj.nSubjects);
        cinit = sum(initiate, 2) ./ (sum(c, 2) - obj.nSubjects);
%        [i n cchase' + cescape]
         %sum(sum(aggr, 3) + sum(aggr, 3)', 2) ./sum(sum(c, 3), 2)
%        plot(cpred, cprey, [sym{i}, ':'], 'Color', colors(n, :));
        [~, order] = sort(cchase);

        ncchase = sum(sum(aggr, 3), 2);
        ncescape = sum(sum(aggr, 3), 1);
        
%         subplot(nrows,3,7);
%         plot(ncchase(order), ncescape(order), [sym{i}, '-'], 'Color', colors(n, :));
%         hold on;
        
        subplot(nrows,3,7:12);
        plot(cchase(order) * 100, cescape(order) * 100, [sym{i}, '-'], 'Color', colors(n, :));
        hold on;

        
%         subplot(nrows,3,10:12);
%         plot(cchase(order) * 100, cinit(order) * 100, [sym{i}, '-'], 'Color', colors(n, :));
%         p = polyfit(cchase(order) * 100, cinit(order) * 100, 1);
%         hold on;

%         subplot(nrows,3,12);
%         c1 = cchase(order);
%         c2 = cinit(order);
%         plot(c1(end) - c1(1), c2(end) - c1(end), 'x', 'Color', colors(n, :));
%         hold on;
        
        stat.pred(i) = var(caggr);
        stat.prey(i) = var(cprey);
    end
    
end
subplot(nrows,3,7:12);
xlabel('% chase');
ylabel('% escape');
r = 30;
for r = 5:5:r*2;
    line([0 r], [r 0], 'LineStyle', ':', 'Color', 'k');
end
axis([0 30 0 30])
hold off;

% subplot(nrows,3,10:12);
% xlabel('% chase');
% ylabel('% initiate');
% hold off;

% subplot(nrows,3,12);
% hold off;
