function WriteStrudel(data, filename)
fid = fopen(filename, 'w');
addclass(fid, '', data, 0);
fclose(fid);
    
function addclass(fid, var, data, count)
if ~isstruct(data)
    addvar(fid, var, data, count)
else
    if ~isempty(var)
        fprintf(fid, [tabs(count) '@%s[struct]\n'], var);
        count = count + 1;
    end
    f = fieldnames(data);
    for i=1:length(f)
        addclass(fid, f{i}, data.(f{i}), count);
    end
    if ~isempty(var)
        fprintf(fid, [tabs(count) '/@%s[struct]\n']);
    end
end
    
function addvar(fid, var, data, count)
sz = size(data);
szstr = '';
szstr = num2str(sz(1));
for i=2:length(sz)
    szstr = [szstr, ',', num2str(sz(i))];
end

fprintf(fid, [tabs(count) '@%s[%s(%s)] ='], var, class(data), szstr);
switch class(data)
    case {'double'}
        fprintf(fid, ' %g', data);
    case {'char'}
        fprintf(fid, ' %s', data);
end

fprintf(fid, '\n');

function s=tabs(count)
s = '';
for i=1:count
    s = [s, '  '];
end