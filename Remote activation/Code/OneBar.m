function OneBar(x, y, c, err, width)
if nargin < 3
    c = [1 0 0];
end
if nargin < 4
    err = [];
end
if nargin < 5
    width = 1;
end

fill([x-width/2 x+width/2 x+width/2 x-width/2], [0 0 y y], c);
if ~isempty(err)
    hon
    errorbar(x, y, err, 'k');
    hoff
end