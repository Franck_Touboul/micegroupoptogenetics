function res = SocialPottsExtrapolate(obj)
%%
rand('twister',sum(100*clock));
randn('seed',sum(100*clock));
%%
obj = TrackLoad(obj);

options.thresh = 1000;
levels_ = 0:floor(log2(obj.nFrames / options.thresh));
nIters_ = levels_ * 2 + 1;
level = levels_(find(rand < cumsum(nIters_ / sum(nIters_)), 1));
fprintf('# level: %d/%d\n', level, length(levels_));
%%
curr = obj;
curr = SocialApplyMap(curr, rand(1, curr.nFrames) < 1 / 2^level);
curr.OutputToFile = false;
curr.OutputInOldFormat = false;
curr = SocialPotts(curr);
%%
fprintf('@exp=%s\n', obj.FilePrefix);
fprintf('@level=%d\n', level);
fprintf('@nFrames=%d\n', curr.nFrames);
fprintf('@Ik=');
fprintf('%g ', curr.Analysis.Potts.Ik);
fprintf('\n');
