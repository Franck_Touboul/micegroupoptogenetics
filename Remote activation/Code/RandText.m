N1 = 2000;
N2 = 4000;

ascii = [65:90, 97:122];

ascii1 = [65:90];
ascii2 = [97:122];

axis([0, 1, 0, 1]);

clf
    

[x1, y1] = meshgrid(0:1/sqrt(N1):1, 0:1/sqrt(N1):1);
[x2, y2] = meshgrid(0:1/sqrt(N2):1, 0:1/sqrt(N2):1);

for i=1:length(x2(:))
    r = randn(1) * 90;
    l = randi(length(ascii2), 1);
    x = x2(i);
    y = y2(i);
    sz = max(round(randn(1) * 3 + 10), 10);
    text(x,y,char(ascii2(l)), 'rotation', r, 'FontSize', sz, 'Color', 'b')
end

for i=1:length(x1(:))
    r = randn(1) * 90;
    l = randi(length(ascii1), 1);
    x = x1(i);
    y = y1(i);
    sz = max(round(randn(1) * 3 + 8), 8);
    
    text(x,y,char(ascii1(l)), 'rotation', r, 'FontSize', sz, 'Color', 'g')
end
box off;
axis off