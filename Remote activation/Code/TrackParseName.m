function data = TrackParseName(obj)

if isstruct(obj)
    name = obj.FilePrefix;
else
    name = obj;
    name = regexprep(name, '^.*\/', '');
    name = regexprep(name, '^.*\\', '');
end
data.Type = regexprep(name, '^([^\.]*)\..*', '$1');
data.GroupID = str2double(regexprep(name, '.*exp([0-9]*).*', '$1'));
data.Day = str2double(regexprep(name, '.*day([0-9]*).*', '$1'));
data.CameraID = str2double(regexprep(name, '.*cam([0-9]*).*', '$1'));
