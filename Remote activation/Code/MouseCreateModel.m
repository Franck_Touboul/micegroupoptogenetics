function MouseCreateModel(Param)
Param = SetDefaults(Param, ...
    'Angle', 0, ...
    'MajorAxis', 1, ...
    'DX', 0,...
    'DY', 0,...
    'Height', 0.05...
    );
%%
Model = ModelCreateF;
Model.SkinMemory = .9;
Points = ModelToPointsF(Model);
%% center model
X = Points.Skin.X; Y = Points.Skin.Y; Z = Points.Skin.Z;
cmX = Points.Bone.X(Model.Axis(1), 1);
cmY = Points.Bone.Y(Model.Axis(1), 1);
X = X - cmX;
Y = Y - cmY;
Res.Model.Beg = [Points.Bone.X(Model.Axis(1), 1), Points.Bone.Y(Model.Axis(1), 1)];
Res.Model.End = [Points.Bone.X(Model.Axis(2), 1), Points.Bone.Y(Model.Axis(2), 1)];

%% rotate & scale
Res.Model.MajorAxis = sqrt(...
    (Points.Bone.X(Model.Axis(1), 1) - Points.Bone.X(Model.Axis(2), 1)).^2 + ...
    (Points.Bone.Y(Model.Axis(1), 1) - Points.Bone.Y(Model.Axis(2), 1)).^2);
Res.Model.Orientation = atan2(Res.Model.End(2) - Res.Model.Beg(2), Res.Model.End(1) - Res.Model.Beg(1));

R = RotMatRz(Param.Angle - Res.Model.Orientation) * ...
    Param.MajorAxis / Res.Model.MajorAxis;
pos = [X(:), Y(:), Z(:)] * R';

%% align position
pos(:, 1) = pos(:, 1) + Param.DX;
pos(:, 2) = pos(:, 2) + Param.DY;

%% fix height
Res.Model.MaxHeight = max(-pos(:, 3));
pos(:, 3) = -pos(:, 3) + Param.Height - Res.Model.MaxHeight;

%%
dt = DelaunayTri(pos(:, 1), pos(:, 2));
id = dt.nearestNeighbor(pos2d(:, 1), pos2d(:, 2));
z = pos(id, 3);
