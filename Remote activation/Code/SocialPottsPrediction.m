function SocialPottsPrediction
%%
res = struct();
for o=1:4
    res(o).pred = [];
end
for id=11:18
    obj = TrackLoad({id 2});
    %%
    for o=1:4
        [id, o]
        me = obj.Analysis.Potts.Model{o};
        %%
        for s=1:obj.nSubjects
            %%
            vec = obj.ROI.nZones.^(obj.nSubjects-1:-1:0);
            ovec = vec;
            ovec(s) = 0;
            oidx = double(me.inputPerms - 1) * ovec' + 1;
            
            p3 = [];
            p3emp = [];
            for i=unique(oidx)'
                map = oidx == i;
                p3(map) = sum(me.prob(map));
                p3emp(map) = sum(me.jointProb(map));
            end
            p = me.prob' ./ p3;
            pemp = me.jointProb ./ p3emp;
            pemp(me.jointProb == 0) = 0;
            
%             ptot = 0;
%             for i=unique(oidx)'
%                 map = oidx == i;
%                 f = find(map);
%                 [q, j] = max(p(map));
%                 ptot = ptot + pemp(f(j)) * p3emp(f(j));
%             end
%             ptot
%             res(o).pred = [res(o).pred, ptot];

            ptot = 0;
            for i=unique(oidx)'
                map = oidx == i;
                pcurr = p3emp(map);
                ptot = ptot + JensenShannonDivergence(p(map), p3emp(map)) * pcurr(1);
            end
            ptot
            res(o).pred = [res(o).pred, ptot];

        end
    end
end
%%
cmap = MyCategoricalColormap;
bins = 0:2.5:80;
for o=1:obj.nSubjects
    h = histc(res(o).pred * 100, bins)
    plot(bins, h, 'color', cmap(o, :)); hon
end
hoff