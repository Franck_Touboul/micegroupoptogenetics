function objs = TrackGroupLoad(name, days, path)
%%
prototype = SocialParseName(name);
for d=days
    expName = sprintf('%s.exp%04d.day%02d.cam%02d', prototype.Condition, prototype.GroupID, d, prototype.CameraID);
    id = ['x' regexprep(expName, '\.', '_')];
    currFilename = [path '/' expName '.obj.mat'];
    fprintf(['# - loading from: ' currFilename '\n']);
    try
        objs.(id) = TrackLoad(currFilename);
    catch
        fprintf(['#    + failed!\n']);
    end
end