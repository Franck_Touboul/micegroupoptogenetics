function validateMarkovModel(data, hmm)
for i=2:length(data)
    if hmm.trans(data(i-1), data(i)) == 0
        if i < length(data) && data(i-1) == data(i+1)
            data(i) = data(i-1);
        else
            i
        end
    end
end