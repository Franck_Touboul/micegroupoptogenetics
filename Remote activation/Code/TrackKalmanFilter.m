function obj = TrackKalmanFilter(obj)
fprintf('# running Kalman filter\n');
RunParallel = true;

if RunParallel
    isOpen = matlabpool('size') > 0;
    if ~isOpen
        matlabpool open;
    end
end

% r =  1;
% q =  5;
s.A = [1 0 obj.dt 0; 0 1 0 obj.dt; 0 0 1 0; 0 0 0 1];
s.H = [1 0 0 0; 0 1 0 0];
s.Q = [0 0 0 0; 0 0 0 0; 0 0 obj.Kalman.Q^2 0; 0 0 0 obj.Kalman.Q^2];
s.R = [obj.Kalman.R^2 0; 0 obj.Kalman.R^2];

X = cell(1, obj.nSubjects);
Y = cell(1, obj.nSubjects);
SPEED = cell(1, obj.nSubjects);
for i=1:obj.nSubjects
    X{i} = obj.x(i, :);
    Y{i} = obj.y(i, :);
    SPEED{i} = obj.x(i, :) * nan;
end
dt = obj.dt;

iP = s.Q;
parfor curr = 1:obj.nSubjects;
    fprintf(['#   . subject ' num2str(curr) '\n']);
    [start, finish, len] = FindEvents(~isnan(X{curr}));
    for i = 1:length(start)
        if len(i) < 2
            X{curr}(start(i):finish(i)) = nan;
            Y{curr}(start(i):finish(i)) = nan;
            SPEED{curr}(start(i):finish(i)) = nan;
            continue;
        end
        Z = [X{curr}(start(i)+1:finish(i)); Y{curr}(start(i)+1:finish(i))];
        
        ix = [Z(:, 1); diff(X{curr}(start(i):start(i)+1))/dt; diff(Y{curr}(start(i):start(i)+1))/dt];
        
        x = kalman_filter(Z(1:2, :), s.A, s.H, s.Q, s.R, ix, iP);
        
        X{curr}(start(i):finish(i)) = [ix(1), x(1, :)];
        Y{curr}(start(i):finish(i)) = [ix(2), x(2, :)];
        SPEED{curr}(start(i):finish(i)) = [0, sqrt(x(3, :) .^ 2 + x(4, :) .^ 2)];
    end
end

for i=1:obj.nSubjects
    obj.x(i, :) = X{i};
    obj.y(i, :) = Y{i};
    obj.speed(i, :) = SPEED{i};
end

% s.A = [2 -1; 1 0];
% s.H = [1 0; 1 -1];
% s.Q = 10^2;
% s.R = 10^2;
%
% X = social.x * 0;
% Y = social.y * 0;
% %for curr = 1:social.nSubjects;
% for curr = 3;
%     curr
%     Zx = [social.x(curr, :); [0,diff(social.x(curr, :))]];
%     Zy = [social.y(curr, :); [0,diff(social.y(curr, :))]];
%     iP = inv(s.H)*s.R*inv(s.H');
%
%     iz = Zx(:, 1);
%     ix = inv(s.H)*iz;
%
%     x = kalman_filter(Zx, s.A, s.H, s.Q, s.R, ix, iP);
%     X(curr, :) = x(1, :);
%
%     iz = Zy(:, 1);
%     iy = inv(s.H)*iz;
%     y = kalman_filter(Zy, s.A, s.H, s.Q, s.R, iy, iP);
%     Y(curr, :) = y(1, :);
% end
%
return;
X = social.x * 0;
Y = social.y * 0;

for curr = 1:social.nSubjects;
    curr
    Zx = [social.x(curr, :); [0,diff(obj.x(curr, :))]];
    Zy = [social.x(curr, :); [0,diff(obj.x(curr, :))]];
    
    sX = s;
    for i=1:size(Zx, 2)
        sX.z = Zx(:, i);
        sX = kalmanf(sX);
        x(:, i) = sX.x;
    end
    
    sY = s;
    for i=1:size(Zy, 2)
        sY.z = Zy(:, i);
        sY = kalmanf(sY);
        y(:, i) = sY.x;
    end
    X(curr, :) = x(:, curr)';
    Y(curr, :) = y(:, curr)';
end
