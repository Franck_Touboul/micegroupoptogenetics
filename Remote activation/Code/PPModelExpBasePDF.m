function p = PPModelExpBasePDF(x, model, state)

alpha = abs(model.states(state).ExpBase.alpha);
m = abs(model.states(state).ExpBase.m);
input = abs(model.states(state).ExpBase.theta - x(1, :));

p = halfCircularUniformExp(input, alpha, m);
p(isnan(input)) = 1 / pi;

if model.UseSpeedHistogram && size(x, 1) >= 3
    ps = DiscreteProbability(...
        x(3, :), ...
        model.states(state).Speed.histogram, ...
        model.Speed.Histogram.minval, ...
        model.Speed.Histogram.maxval);
    p = p .* ps;
end
