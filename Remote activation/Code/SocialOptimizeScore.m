function SocialOptimizeScore(obj)
%%
%refs = {'SC.exp0001.day02.cam01', 'SC.exp0008.day02.cam01'};
refs = {'SC.exp0001.day02.cam01'};
for r=1:length(refs)
    try
        ref = refs{r};
        data = dlmread(['aux/' ref '.marks'], '\t', [1 0 500 14]);
        marks.ids = data(:, 1);
        marks.beg = data(:, 2);
        marks.end = data(:, 3);
        marks.subjects = data(:, 4:5);
        marks.approach = data(:, 6:7);
        marks.leave = data(:, 8:9);
        marks.chase = data(:, 10);
        marks.pred = data(:, 11:12);
        marks.prey = data(:, 13:14);
        marks.artifact = data(:, 15);
        results = SocialBehaviorAnalysisScore(obj, marks);
        
        fprintf('# ref: %s\n', ref);
        fprintf('# - [correct=%.2f, false-positive=%.2f, false-negative=%.2f, count=%.2f]\n', results.chase(1), results.chase(2), results.chase(3),results.chase(4));
        fprintf('# - Detection %.2f%%\n', results.chase(1)/(results.chase(1) + results.chase(3)) * 100);
        fprintf('# - False alarm %.2f%%\n', results.chase(2) / results.chase(4) * 100);
    catch
    end
end
%results.chase
%
%
% f = fieldnames(results);
% o = 1;
% for i=1:length(f)
%     fprintf('@%s=', o, f{i});
%     fprintf(' %f', results.(f{i}));
%     fprintf('\n');
% end
