function SocialHierarchyGraph(obj)
%%
showMap = true;
showAsImage = true;

if isstruct(obj)
    prototype = SocialParseName(obj.FilePrefix);
    path = obj.OutputPath;
else
    prototype = SocialParseName(name);
    path = 'Res/';
end
curr = obj;
nBins = 20;
%%
maxmat = -inf;
minmat = inf;
for day=1:length(obj.Hierarchy.Group.Days)
    ce = curr.Hierarchy.Group.Days(day).ChaseEscape;
    mat = ce - ce';
    mat(binotest(ce, ce + ce')) = 0;
    mat = mat .* (mat > 0);
    maxmat = max(maxmat, max(mat(:)));
    minmat = min(minmat, min(mat(:)));
end
%%
for day=1:length(obj.Hierarchy.Group.Days)
    if showMap
        expName = sprintf('%s.exp%04d.day%02d.cam%02d', prototype.Condition, prototype.GroupID, day, prototype.CameraID);
        id = regexprep(expName, '\.', '_');
        currFilename = [path '/' expName '.obj.mat'];
        fprintf(['# - loading from: ' currFilename '\n']);
        curr = TrackLoad(currFilename);
    end
    %%
    ce = curr.Hierarchy.Group.Days(day).ChaseEscape;
    mat = ce - ce';
    mat(binotest(ce, ce + ce')) = 0;
    mat = mat .* (mat > 0);

    [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
    mat(1,1) = maxmat;
    mat(2,2) = minmat;
    diluted = mat;
    diluted(removed ~= 0) = 0;


    %%
    [q, bg] = SocialGraph(curr, diluted, removed, rank);
    g = biograph.bggui(bg);
    fig2 = get(g.biograph.hgAxes,'children');
    %%
    if showAsImage
        %%
        figure(2);
        clf;
        a1 = gca;
        copyobj(fig2, a1)
        axis off;
        set(gcf, 'Color', 'w');
        %frame.cdata = imcapture(gcf, 'all', 300);
        %set (gca,'units','pixels','position',[0 0 1000 1000])

        frame = CaptureFrame;
        sz=size(frame.cdata); frame.cdata = frame.cdata(round(sz(1)*.05):round(sz(1)*.95), round(sz(2)*.05):round(sz(2)*.95), :);
        %%
        figure(1);
        set(gcf, 'Color', 'w');
        h = subplot(2, length(curr.Hierarchy.Group.Days),day);
        nDays = length(curr.Hierarchy.Group.Days);
        hp = .08;
        vp = .01;
        set(h, 'pos', [(day-1)/nDays+vp, .4+hp, 1/nDays-2*vp, .6-2*hp]);

        imagesc(frame.cdata);
        axis off;
        title(['Day ' num2str(day)]);
    else
        figure(1)
        %
        h = subplot(length(curr.Hierarchy.Group.Days),2,2*day-1);
        cla(h)
        a1 = gca;
        copyobj(fig2, a1)
        axis off;
        set(gcf, 'Color', 'w');
        title(['Day ' num2str(day)]);
    end
    %%
    close(g.hgFigure);
    %
        h = subplot(2, length(curr.Hierarchy.Group.Days),length(curr.Hierarchy.Group.Days) + day);
    set(h, 'pos', [(day-1)/nDays+vp, hp, 1/nDays-2*vp, .4-2*hp]);

    range = [min(curr.x(:)), max(curr.x(:)), min(curr.VideoHeight - curr.y(:)), max(curr.VideoHeight - curr.y(:))];
    h={};
    map = zeros(nBins);
    mapi = zeros(nBins);
    for i=1:curr.nSubjects;
        [h{i}, range] = HexHist([curr.x(i, ~curr.hidden(i, :))', curr.VideoHeight - curr.y(i, ~curr.hidden(i, :))'], nBins, 'range', range);
        c = map < h{i};
        mapi(c) = i;
        map(c) = h{i}(c);
    end
    HexPlot(mapi, range, 'Colormap', [.8 .8 .8; MyMouseColormap])
end
