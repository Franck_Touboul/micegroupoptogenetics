function obj = SocialVerifiedPotts(obj)
obj = TrackLoad(obj);
map = rand(1, obj.nFrames) >= .5;
%%
trainobj = SocialApplyMap(obj, map);
trainobj.OutputToFile = false;
trainobj.OutputInOldFormat = false;

testobj = SocialApplyMap(obj, ~map);
testobj.OutputToFile = false;
testobj.OutputInOldFormat = false;
%%
trainobj = SocialPotts(trainobj);
testobj = SocialPotts(testobj);
trainobj.Analysis.Potts.TrainMap = map;
trainobj.Analysis.Potts.nTrainFrames = trainobj.nFrames;
trainobj.Analysis.Potts.nTestFrames = testobj.nFrames;
trainobj.Analysis.Potts.Reference = testobj.Analysis.Potts.Model;
obj.Analysis.Potts.Verified = trainobj.Analysis.Potts;
%%
if obj.OutputToFile
    fprintf('# - saving data\n');
    TrackSave(obj);
end
