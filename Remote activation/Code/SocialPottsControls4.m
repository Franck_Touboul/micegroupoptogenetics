function SocialPottsControls(obj)
obj = TrackLoad(obj);
%%
Options.EvenOddTimescales = [1 60];
obj.OutputToFile = false;
obj.OutputInOldFormat = false;
obj.Analysis.Potts.nIters = [2000 20000];
obj.Analysis.Potts.MinNumberOfIters = [500 1000];
obj.Analysis.Potts.Confidence = 0.05;
%%
oidx = 1;
objs = {};
%% day-day
try
    data = TrackParse(obj);
    data.Day = data.Day + 1;
    next = TrackLoad([obj.OutputPath TrackName(obj, data) '.obj.mat']);
    %
    CObj = obj;
    CObj.zones = [obj.zones(:, obj.valid), next.zones(:, next.valid)];
    CObj.valid = true(1, size(CObj.zones, 2));
    CObj.nFrames = size(CObj.zones, 2);
    %
    first = [true(1, sum(obj.valid)) false(1, sum(next.valid))];
    second = ~first;
    %
    Types = {'first', 'second'};
    for i=1:length(Types);
        map = eval(Types{i});
        curr = CObj;
        curr.zones = CObj.zones(:, map);
        curr.valid = CObj.valid(:, map);
        curr.nFrames = sum(map);
%        curr.me = TrainPottsModel(curr, 3);
        curr = SocialPotts(curr);
        objs{oidx}.(Types{i}) = curr;
    end
    oidx = oidx + 1;
catch me
    warning(me.message);
end
%%
save('SocialPottsControls4', 'objs', '-v7.3');

