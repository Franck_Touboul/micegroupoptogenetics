function d = mypdist2(a,b)
d = squareform(pdist([a;b]));
d = d(1:size(a, 1), size(a, 1)+[1:size(b,1)]);
