function obj = SocialChaseEscapeProperties(obj)
%%
obj.Contacts.Behaviors.PostSpeed = zeros(2, length(obj.Contacts.Behaviors.ChaseEscape));
obj.Contacts.Behaviors.PostRelativeSpeed = ones(1, length(obj.Contacts.Behaviors.ChaseEscape)) * inf;
obj.Contacts.Behaviors.PostDistance = zeros(2, length(obj.Contacts.Behaviors.ChaseEscape));
obj.Contacts.Behaviors.EndInNest = false(2, length(obj.Contacts.Behaviors.ChaseEscape));

for i=1:length(obj.Contacts.Behaviors.ChaseEscape)
    e = obj.Contacts.List(i);
    %%
    if all(e.end < obj.nFrames)
        obj.Contacts.Behaviors.EndInNest(:, i) = [obj.sheltered(e.subjects(1), e.end(1) + 1); obj.sheltered(e.subjects(2), e.end(2) + 1)];
    end
    if obj.Contacts.Behaviors.ChaseEscape(i)
        %%
        b1 = find(e.data{1} == 5 | e.data{1} == 6, 1);
        b2 = find(e.data{2} == 5 | e.data{2} == 6, 1);

        overlap = max(e.beg(1)+b1-1, e.beg(2)+b2-1):min(e.end(1), e.end(2));
        
%         b1 = find(e.data{1} == 5 | e.data{1} == 6, 1);
%         s1 = obj.speedCMperSec(e.subjects(1), e.beg(1)+b1-1:e.end(1));
%         
%         b2 = find(e.data{2} == 5 | e.data{2} == 6, 1);
%         s2 = obj.speedCMperSec(e.subjects(2), e.beg(2)+b2-1:e.end(2));

        s1 = obj.speedCMperSec(e.subjects(1), overlap);
        s2 = obj.speedCMperSec(e.subjects(2), overlap);
        
        rs = obj.speedCMperSec(e.subjects(1), overlap) - obj.speedCMperSec(e.subjects(2), overlap);
        %%
        obj.Contacts.Behaviors.PostSpeed(:, i) = [median(s1); median(s2)];
        obj.Contacts.Behaviors.PostRelativeSpeed(:, i) = median(rs);
        obj.Contacts.Behaviors.PostDistance(:, i) = [sum(s1 * obj.dt); sum(s2 * obj.dt)];
    end
end