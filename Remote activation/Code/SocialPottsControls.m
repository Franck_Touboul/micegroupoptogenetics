function SocialPottsControls(obj)
obj = TrackLoad(obj);
%%
Options.EvenOddTimescales = [1 60];
obj.OutputToFile = false;
obj.OutputInOldFormat = false;
obj.Analysis.Potts.nIters = [2000 20000];
obj.Analysis.Potts.MinNumberOfIters = [500 1000];
obj.Analysis.Potts.Confidence = 0.05;
%%
oidx = 1;
objs = {};
%% even-odd
try
    for ts = Options.EvenOddTimescales
        %%
        frames = ts * obj.FrameRate;
        map = ceil((1:obj.nFrames) / frames);
        even = mod(map, 2) == 1;
        odd = ~even;
        %%
        Types = {'even', 'odd'};
        for i=1:length(Types);
            map = eval(Types{i});
            curr = obj;
            curr.zones = obj.zones(:, map);
            curr.valid = obj.valid(:, map);
            curr.nFrames = sum(map);
            %curr.me = TrainPottsModel(curr, 1:obj.nSubjects);
            curr = SocialPotts(curr);
            objs{oidx}.(Types{i}) = curr;
        end
        oidx = oidx + 1;
    end
catch me
    warning(me.message);
end
%% half-half
try
    first = (1:obj.nFrames) < obj.nFrames / 2;
    second = ~first;
    %
    Types = {'first', 'second'};
    for i=1:length(Types);
        map = eval(Types{i});
        curr = obj;
        curr.zones = obj.zones(:, map);
        curr.valid = obj.valid(:, map);
        curr.nFrames = sum(map);
%        curr.me = TrainPottsModel(curr, 3);
        curr = SocialPotts(curr);
        objs{oidx}.(Types{i}) = curr;
    end
    oidx = oidx + 1;
catch me
    warning(me.message);
end
%% day-day
try
    data = TrackParse(obj);
    data.Day = data.Day + 1;
    next = TrackLoad([obj.OutputPath TrackName(obj, data) '.obj.mat']);
    %
    CObj = obj;
    CObj.zones = [obj.zones(:, obj.valid), next.zones(:, next.valid)];
    CObj.valid = true(1, size(CObj.zones, 2));
    CObj.nFrames = size(CObj.zones, 2);
    %
    first = [true(1, sum(obj.valid)) false(1, sum(next.valid))];
    second = ~first;
    %
    Types = {'first', 'second'};
    for i=1:length(Types);
        map = eval(Types{i});
        curr = CObj;
        curr.zones = CObj.zones(:, map);
        curr.valid = CObj.valid(:, map);
        curr.nFrames = sum(map);
%        curr.me = TrainPottsModel(curr, 3);
        curr = SocialPotts(curr);
        objs{oidx}.(Types{i}) = curr;
    end
    oidx = oidx + 1;
catch me
    warning(me.message);
end
%%
save('SocialPottsControls', 'objs', '-v7.3');
