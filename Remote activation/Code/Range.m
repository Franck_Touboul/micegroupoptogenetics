function [mn, mx, md] = Range(x)
mn = min(x(:));
mx = max(x(:));
md = mn + (mx - mn) / 2;