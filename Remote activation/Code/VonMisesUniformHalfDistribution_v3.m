function p = VonMisesUniformHalfDistribution_v3(x, m, v, base)
if nargin < 4
    base = .5/pi;
end
k = 1 ./ v;
b = besseli(0, k);
p1 = exp(k .* cos(x - m)) ./ ( 2 * pi * b);
p2 = exp(k .* cos(-x - m)) ./ ( 2 * pi * b);
p = p1 + p2;

mx = 2 * exp(k) / ( 2 * pi * b);
ratio = (pi*base - 1) / (pi*mx-1);
p = p * ratio + (1 - ratio) / pi;
%p = p + ;
