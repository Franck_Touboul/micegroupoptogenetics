function obj = TrackPathPar(obj, nruns)
%%
if ~exist('nruns', 'var')
    nruns = 100;
end
obj = TrackLoad(obj);
%%
fprintf('# finding paths\n');
%%
% obj.ColorMatchThresh = 0.1;
%obj.MaxHiddenDuration = 5;
 obj.NumOfProbBins = 100;
 obj.MinJumpProb = 0.0;
% obj.AllowHiddenTransitions = false;
% obj.MaxIsolatedDistance = 12;

%%
fprintf('# - loading segmentations\n');
cents = struct();
cents.x       = [];
for i=1:nruns
    fprintf('#      . segment no. %d\n', i);
    filename = [obj.OutputPath obj.FilePrefix '.segm.' sprintf('%03d', i) '.mat'];
    waitforfile(filename);
    currSegm = load(filename);
    cents = structcat(cents, currSegm.cents, 1, true, false);
end

%%
if isfield(obj, 'ROI.Ignore')
    ignore = uint8(imread(obj.ROI.Ignore));
    fprintf('#  . removing unwanted cents...');
    for i=1:size(cents.label, 1)
        labels = cents.label(i, :);
        y = cents.y(i,:);
        x = cents.x(i,:);
        y = y(labels~=0) / obj.VideoScale;
        if isempty(y)
            continue;
        end
        x = x(labels~=0) / obj.VideoScale;
        labels(labels~=0) = (1 - ignore( sub2ind(size(ignore), round(y), round(x)) )) .* labels(labels~=0);
        cents.label(i, :) = labels;
    end
    fprintf('[done]\n');
end
%%
StartFrame = obj.StartTime / obj.dt;
EndFrame = obj.EndTime / obj.dt;
if EndFrame < 0
   EndFrame = obj.nFrames + 1;
end
%%
prev = cents.label;
[q, cents.label] = max(cents.logprob, [], 3);
cents.label(prev == 0) = 0;

thresh = obj.ColorMatchThresh;
ndata = size(cents.x, 1);

for i=1:obj.nSubjects
    logprobs = cents.logprob(:, :, i);
    logprobs(cents.label ~= i) = -inf;
    [maxlogprobs, centids] = max(logprobs, [], 2);

    maxprobs = exp(maxlogprobs);
    res{i}.x = cents.x(sub2ind(size(cents.x), 1:ndata, centids'));
    res{i}.y = cents.y(sub2ind(size(cents.y), 1:ndata, centids'));
    res{i}.prob = maxprobs';
    res{i}.logprob = maxlogprobs';
    res{i}.centids = centids';
    res{i}.prev = double([true; cents.prev(sub2ind(size(cents.prev), 2:ndata, centids(2:end)'))' == centids(1:end-1)]);
    
    res{i}.valid = maxprobs' > thresh;
    %% remove isolated apperances
    d = diff([0 res{i}.valid 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    len = finish - start + 1;
    isolated = start - [1 finish(1:end-1)] > obj.MaxIsolatedDistance & ...
    [start(2:end) size(res{i}.valid, 1)] - finish > obj.MaxIsolatedDistance;
    idx = find(len == 1 & isolated);
    for j=idx
        res{i}.valid(start(j):finish(j)) = false;
    end
    %%
    res{i}.valid(1:StartFrame) = false;
    res{i}.valid(EndFrame:end) = false;
    %%
    res{i}.id = (maxprobs' > thresh) * i;
    res{i}.x(~res{i}.valid) = nan;
    res{i}.y(~res{i}.valid) = nan;
    res{i}.prob(~res{i}.valid) = nan;
    res{i}.logprob(~res{i}.valid) = -inf;
    res{i}.centids(~res{i}.valid) = nan;
    res{i}.prev(~res{i}.valid) = nan;
    %%
end
%%
distances_ = [];
dx_ = [];
dy_ = [];
% distances for matching blobs
for i=1:obj.nSubjects
    m = res{i}.prev(2:end) == res{i}.id(1:end-1)';
    dx = diff(res{i}.x);
    dy = diff(res{i}.y);
    dx_ = [dx_, dx(m)];
    dy_ = [dy_, dy(m)];
    distances = sqrt(diff(res{i}.x).^2 + diff(res{i}.y).^2);
    distances = distances(m);
    distances = distances(distances > 0 & ~isnan(distances));
    distances_ = [distances_, distances];
end
% distances for different colors
distances = [];
idistances = [];
for i=1:obj.nSubjects
    for j=1:obj.nSubjects
        if j~=i
            distances = [distances, sqrt((res{i}.x - res{j}.x).^2 +(res{i}.y - res{j}.y).^2)];
        else
            idistances = [idistances, sqrt((res{i}.x(2:end) - res{j}.x(1:end-1)).^2 +(res{i}.y(2:end) - res{j}.y(1:end-1)).^2)];
        end
    end
end

for i=1:obj.nSubjects
    res{i}.probBins = sequence(0, max([distances idistances]) + 0.1, obj.NumOfProbBins+1); 
    res{i}.probBinDiff = res{i}.probBins(2) - res{i}.probBins(1);
    
    h1 = histc(distances, res{i}.probBins) + 1;
    h1=h1/sum(h1); 
    
    h2=histc(distances_, res{i}.probBins);
    h2=h2/sum(h2); 
    
    res{i}.jumpProb = h2./(h1+h2);
    res{i}.jumpProb(res{i}.jumpProb < obj.MinJumpProb) = 0;
    res{i}.jumpLogprob = log(res{i}.jumpProb);
end

ores = res;
%%
nClusters = size(cents.x, 2);
clusters = 1:nClusters;
nFrames = length(res{1}.id);
Cents = cell(1, obj.nSubjects);
for curr = 1:obj.nSubjects
    Cents{curr} = cents;
end
Track = {};   

for curr = 1:obj.nSubjects
    currCents = cents;
    currClusters = clusters;
    currObj = obj;
    
    fprintf('# - tracking subject %d of %d\n', curr, currObj.nSubjects);
    currLogprobs = currCents.logprob(:, :, curr);
    currLogprobs(currCents.label == 0) = -inf;
    %% build observation probability table
    table = ones(nClusters+obj.ROI.nHidden+1, nFrames) * -inf;
    scoordX = zeros(nClusters, nFrames);
    scoordY = zeros(nClusters, nFrames);
    scoordHidingNeighbourX = scoordX;
    scoordHidingNeighbourY = scoordY;
    scoordHidingDistance = zeros(nClusters, nFrames, obj.ROI.nHidden);
    path = zeros(size(table), 'uint16');
    nchar = 0;
    fprintf('#   . computing emission probabilities\n');
    for s=1:nClusters
        range = 1:ndata;
        
        table(s, range) = currLogprobs(:, s);
        scoordX(s, range) = currCents.x(:, s);
        scoordY(s, range) = currCents.y(:, s);
        for h=1:obj.ROI.nHidden
            nchar = Reprintf(nchar, '#       processing segment %d / %d', ((s - 1) * obj.ROI.nHidden) + h, nClusters*obj.ROI.nHidden);
            hcoord = obj.ROI.HiddenBoundarySCoordinates{h};
            d = pdist2([scoordX(s, range)', scoordY(s, range)'], hcoord);
            [hvalues, hidx] = min(d, [], 2);
            scoordHidingNeighbourX(s, range) = hcoord(hidx, 1);
            scoordHidingNeighbourY(s, range) = hcoord(hidx, 2);
            scoordHidingDistance(s, range, h) = hvalues';
        end
    end
    fprintf('\n');
    
    for s=1:obj.ROI.nHidden+1
        table(nClusters+s, :) = flog(prod(1 - exp(table(1:nClusters, :)), 1));
    end
    emitlogprobs = table;
    
    valid = sum(isfinite(table(1:nClusters, :)), 1) > 0;
    d = diff([0 valid 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    
    %%
    prev = 1;
    nchar = 0;
    
    scoordHidingPosX = ones(1, nFrames) * inf;
    scoordHidingPosY = ones(1, nFrames) * inf;
    scoordHidingTime = zeros(1, nFrames);
    
    zeroprob = flog(1);
    zeroprobvec = [repmat(zeroprob, nClusters, 1); zeros(obj.ROI.nHidden+1, 1)];
    
    hiddenprobvec = [repmat(zeroprob, nClusters, 1); zeros(obj.ROI.nHidden, 1); zeroprob];
    nohiddenprobvec = [repmat(zeroprob, nClusters, 1); zeros(obj.ROI.nHidden, 1); flog(0)];
    
    fprintf('#   . building viterbi table\n');
    for n=1:length(start)
        nchar = Reprintf(nchar, '#       processing segment %d / %d', n, length(start));
        r = start(n):finish(n);
        for f=r
            %%
            for s=1:nClusters
                if ~isfinite(table(s, f))
                    continue;
                end
                x = currCents.x(f, s);
                y = currCents.y(f, s);
                
                distance = scoordHidingDistance(s, f, :);
                jump = sqrt((x - scoordX(:, prev)).^2 + (y - scoordY(:, prev)).^2);
                if prev+1==f && currCents.prev(f, s) > 0
                    jump(currCents.prev(f, s)) = 0;
                    jump(currClusters(currClusters ~= currCents.prev(f, s))) = inf;
                end
                distance = [...
                    jump; ...
                    distance(:);...
                    sqrt((x - scoordHidingPosX(prev)).^2 + (y - scoordHidingPosY(prev)).^2) / (f - scoordHidingTime(prev))];
                idx = floor(distance / res{curr}.probBinDiff) + 1;
                idx(idx < 1) = 1;
                idx(idx > obj.NumOfProbBins) = obj.NumOfProbBins;
                trans = res{curr}.jumpLogprob(idx)';
                
                if f>prev+1
                    trans = trans + zeroprobvec * (f - prev - 1);
                    if f - prev - 1 > obj.MaxHiddenDuration
                        trans(nClusters + obj.ROI.nHidden + 1) = flog(0);
                        trans(1:nClusters) = flog(0);
                    end
                end
                
                [val, from] = max(table(:, prev) + trans);
                table(s, f) = table(s, f) + val;
                path(s, f) = from;
            end
            % hidden zones
            for h=1:obj.ROI.nHidden
                if obj.AllowHiddenTransitions
                    distance = [scoordHidingDistance(:, prev, h); obj.ROI.HiddenSDistances(:, h); inf];
                else
                    distance = [scoordHidingDistance(:, prev, h); ones(obj.ROI.nHidden,1) * inf; inf];
                    distance(nClusters + h) = 0;
                end
                idx = floor(distance / res{curr}.probBinDiff) + 1;
                idx(idx < 1) = 1;
                idx(idx > obj.NumOfProbBins) = obj.NumOfProbBins;
                trans = res{curr}.jumpLogprob(idx)';
                
                [val, from] = max(table(:, prev) + trans);
                table(nClusters + h, f) = table(nClusters + h, f) + val;
                path(nClusters + h, f) = from;
            end
            %
            
            if f - scoordHidingTime(prev) <= obj.MaxHiddenDuration
                [val, from] = max(table(:, prev) + hiddenprobvec);
            else
                [val, from] = max(table(:, prev) + nohiddenprobvec);
            end
            
            table(nClusters + obj.ROI.nHidden + 1, f) = table(nClusters + obj.ROI.nHidden + 1, f) + val;
            path(nClusters + obj.ROI.nHidden + 1, f) = from;
            if from <= nClusters
                scoordHidingPosX(f) = scoordX(from, prev);
                scoordHidingPosY(f) = scoordY(from, prev);
                scoordHidingTime(f) = f;
            else
                scoordHidingPosX(f) = scoordHidingPosX(prev);
                scoordHidingPosY(f) = scoordHidingPosY(prev);
                scoordHidingTime(f) = scoordHidingTime(prev);
            end
            %
            prev = f;
        end
    end
    fprintf('\n');
    %% backtrack
    fprintf('#   . backtracking\n');
    Track{curr}.src = zeros(1, size(table, 2));
    Track{curr}.logprob = zeros(1, size(table, 2));
    Track{curr}.emitlogprob = zeros(1, size(table, 2));
    Track{curr}.x = zeros(1, size(table, 2));
    Track{curr}.y = zeros(1, size(table, 2));
    
    for n=length(start):-1:1
        r = start(n):finish(n);
        f=finish(n);
        if n<length(start)
            idx = path(Track{curr}.src(start(n+1)), start(n+1));
            
            Track{curr}.src(finish(n)) = idx;
            Track{curr}.logprob(f) = table(idx, f);
            Track{curr}.emitlogprob(f) = emitlogprobs(idx, f);
        else
            [Track{curr}.logprob(finish(n)), Track{curr}.src(finish(n))] = max(table(:, finish(n)));
            Track{curr}.emitlogprob(finish(n)) = emitlogprobs(Track{curr}.src(finish(n)), finish(n));
        end
        for f=finish(n)-1:-1:start(n)
            idx = path(Track{curr}.src(f+1), f+1);
            
            Track{curr}.src(f) = idx;
            Track{curr}.logprob(f) = table(idx, f);
            Track{curr}.emitlogprob(f) = emitlogprobs(idx, f);
        end
        if n>1
            Track{curr}.logprob(finish(n-1)+1:start(n)-1) = Track{curr}.logprob(start(n));
            Track{curr}.emitlogprob(finish(n-1)+1:start(n)-1) = Track{curr}.emitlogprob(start(n));
        end
    end
    
    fprintf('#   . computing path\n');
    Track{curr}.valid = Track{curr}.src <= nClusters & Track{curr}.src > 0;
    Track{curr}.id = Track{curr}.valid * curr;
    
    range = 1:length(Track{curr}.valid);
    range = range(Track{curr}.valid);
    
    src = Track{curr}.src(Track{curr}.valid);
    
    Track{curr}.x = zeros(1, length(Track{curr}.valid));
    Track{curr}.x(Track{curr}.valid) = scoordX(sub2ind(size(scoordX), src, range));
    Track{curr}.x(~Track{curr}.valid) = nan;
    
    Track{curr}.y = zeros(1, length(Track{curr}.valid));
    Track{curr}.y(Track{curr}.valid) = scoordY(sub2ind(size(scoordY), src, range));
    Track{curr}.y(~Track{curr}.valid) = nan;
    
    %if curr == 1; break;end
end
origTrack = Track;
if obj.OutputInOldFormat
    filename = [obj.OutputPath obj.FilePrefix '.raw-Track.mat'];
    save(filename, 'Track');
end

%%
fprintf('# - setting additional properties & fixes\n');
for i=1:obj.nSubjects;
    % ignore invalid frames
    nValidFrames = length(Track{i}.src);
    Track{i}.src(nValidFrames+1:obj.nFrames) = nan;
    Track{i}.id(nValidFrames+1:obj.nFrames) = nan;
    Track{i}.x(nValidFrames+1:obj.nFrames) = nan;
    Track{i}.y(nValidFrames+1:obj.nFrames) = nan;
    
    Track{i}.hidden = Track{i}.src > nClusters;
    
    % remove skipped areas
    d = diff([0 Track{i}.src == 0 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    for j=1:length(start)
        if start(j) <= 1
            continue;
        end
        %Track{i}.src(start(j)-1)
        if Track{i}.src(start(j)-1) > nClusters
            Track{i}.src(start(j):finish(j)) = Track{i}.src(start(j)-1);
        else
            Track{i}.src(start(j):finish(j)) = nClusters + obj.ROI.nHidden + 1;
        end
    end    

    % set zones coordinates
    valid = Track{i}.src > nClusters & Track{i}.src <= nClusters + obj.ROI.nHidden;
    Track{i}.x(valid) = obj.ROI.HiddenSCenters(Track{i}.src(valid) - nClusters, 1);
    Track{i}.y(valid) = obj.ROI.HiddenSCenters(Track{i}.src(valid) - nClusters, 2);
    
    % interpolate hidden epochs
    d = diff([0 Track{i}.src == 0 | Track{i}.src == nClusters + obj.ROI.nHidden + 1  0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    for j=1:length(start)
        if start(j) <= 1 || finish(j) >= nValidFrames || isnan(Track{i}.x(start(j) - 1)) || isnan(Track{i}.x(finish(j) + 1))
            continue;
        end
        r = start(j):finish(j);
        [Track{i}.x(r), Track{i}.y(r)] = MotionInterp(Track{i}.x, Track{i}.y, r, [obj.VideoWidth * obj.VideoScale, obj.VideoHeight * obj.VideoScale]);
    end
    
    % setting regions
    x = min(max(round(Track{i}.x), 1), obj.VideoWidth) / obj.VideoScale;
    y = min(max(round(Track{i}.y), 1), obj.VideoHeight) / obj.VideoScale;
    Track{i}.regions = zeros(1, obj.nFrames);
    for r=1:obj.ROI.nRegions
        Track{i}.regions(obj.ROI.Regions{r}(sub2ind(size(obj.ROI.Regions{r}), y, x))) = r;
    end
    Track{i}.sheltered = Track{i}.src > nClusters & Track{i}.src <= nClusters + obj.ROI.nHidden;
    
    % setting zones
    Track{i}.zones = ones(1, obj.nFrames);
    obj.ROI.ZoneNames{1} = 'Open';
    index = 2;
    for r=1:obj.ROI.nRegions
        idx = Track{i}.regions == r & ~Track{i}.sheltered;
        Track{i}.zones(idx) = index;
        if i==1; obj.ROI.ZoneNames{index} = obj.ROI.RegionNames{r}; end
        if obj.ROI.IsSheltered(r)
            % set long invisible periods as sheltered
            invisibleInRegion = Track{i}.regions == r & Track{i}.hidden & ~Track{i}.sheltered;
            d = diff([0 invisibleInRegion  0], 1, 2);
            start  = find(d > 0);
            finish = find(d < 0) - 1;
            len = finish - start + 1;
            for q = find(len > obj.MaxHiddenDuration)
                Track{i}.sheltered(start(q):finish(q)) = true;
            end
            %
            idx = Track{i}.regions == r & Track{i}.sheltered;
            Track{i}.zones(idx) = index + 1;
            if i==1; obj.ROI.ZoneNames{index + 1} = ['(' obj.ROI.RegionNames{r} ')']; end
            index = index + 2;
        else
            index = index + 1;
        end
    end
    
    % ignore invalid frames
    Track{i}.id(isnan(Track{i}.x)) = nan;
    Track{i}.zones(isnan(Track{i}.x)) = nan;
    Track{i}.regions(isnan(Track{i}.x)) = nan;
    Track{i}.hidden(isnan(Track{i}.x)) = true;
    Track{i}.sheltered(isnan(Track{i}.x)) = false;
end
%%
fprintf('# - finding paths\n');
prevtrack = Track;
social.options = obj;
social.dt = obj.dt;
social.x = zeros(length(Track), length(Track{1}.x), 'single');
social.y = zeros(length(Track), length(Track{1}.x), 'single');
for i=1:length(Track)
    social.x(i, :) = Track{i}.x / obj.VideoScale;
    social.y(i, :) = Track{i}.y / obj.VideoScale;
end
%
fprintf('# - organizing results\n');
social.nData = size(social.x, 2);
social.time = (1:social.nData) * obj.dt;
zones.all = zeros(obj.nSubjects, obj.nFrames);
zones.hidden = zeros(obj.nSubjects, obj.nFrames);
zones.sheltered = zeros(obj.nSubjects, obj.nFrames);
zones.regions = zeros(obj.nSubjects, obj.nFrames);
%
for i=1:obj.nSubjects
    x = round(social.x(i, :)); x(x == 0) = 1;
    y = round(social.y(i, :)); y(y == 0) = 1;
    %
    startnan = find(isnan(x), 1);
    if startnan > 1
        x(startnan:end) = x(startnan - 1);
        y(startnan:end) = y(startnan - 1);
    else
        x(startnan:end) = 1;
        y(startnan:end) = 1;
    end
    %
    zones.hidden(i, :) = Track{i}.id == 0;
    zones.all(i, :) = Track{i}.zones;
    zones.sheltered(i, :) = Track{i}.sheltered;
    zones.regions(i, :) = Track{i}.regions;
end
social.zones = zones;
social.colors = obj.Colors.Centers;
social.nSubjects = obj.nSubjects;
social.zones.labels = obj.ROI.ZoneNames;

%%
fprintf('# - computing kalman filter\n');
% if isfield(obj, 'Kalman')
%     social = kalmanFilterTrack(social, options.kalman.q, options.kalman.r);
% end
%%
if obj.OutputInOldFormat
    fprintf('# - saving data in old format\n');
    filename = [obj.OutputPath obj.FilePrefix '.social.mat'];
    save(filename, 'social', '-v7.3');
end

%%
obj.x = social.x;
obj.y = social.y;
obj.time = social.time;
obj.zones = social.zones.all;
obj.regions = social.zones.regions;
obj.hidden = social.zones.hidden;
obj.sheltered = social.zones.sheltered;
obj.valid = ~isnan(sum(obj.x, 1)); 

if obj.OutputToFile
    fprintf('# - saving data\n');
    TrackSave(obj);

    filename = [obj.OutputPath obj.FilePrefix '.Track.mat'];
    save(filename, 'obj');

    filename = [regexprep(obj.VideoFile, '\.[^\.]*$', '') '.mat'];
    saveSocialForTracking(filename, social);
end

