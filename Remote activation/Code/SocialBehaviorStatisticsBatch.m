SocialExperimentData;
%%
stat = struct();
for i=1:GroupsData.nExperiments; 
    for day=1:GroupsData.nDays
        obj = TrackLoad({i, day}); 
        stat(i).nContacts(day, 1) = length(obj.Contacts.List);
        stat(i).nChases(day, 1) = sum(obj.Contacts.Behaviors.ChaseEscape);
        stat(i).Sheltered(day, 1) = sum(obj.sheltered(:));
        stat(i).Visible(day, 1) = sum(obj.valid) * 4 - sum(obj.sheltered(:));
        stat(i).Valid(day, 1) = sum(obj.valid) * 4;
        if day == 2
            stat(i).Durations = max([obj.Contacts.List.end]) - min([obj.Contacts.List.beg]) + 1;
        end
    end
    stat(i).Group = GroupsData.group(i);
end
%%
figure(1);

visibleHours = [stat.Visible] ./ obj.FrameRate / 3600;
nContacts = [stat.nContacts];
nChases = [stat.nChases];
group = [stat.Group];
sheltered = [stat.Sheltered];
visible = [stat.Visible];
valid = [stat.Valid];
validHours = [stat.Valid] ./ obj.FrameRate / 3600;
%
subplot(2,3,1);
for g=unique(group)
    val = sheltered(:, group == g) ./ valid(:, group == g) * 100;
    errorbar(1:GroupsData.nDays, mean(val, 2), stderr(val, 2), 'color', GroupsData.Colors(g, :)); hon
end
xaxis(.8, GroupsData.nDays+.2);
set(gca, 'XTick', 1:GroupsData.nDays); 
 
xlabel('day');
ylabel('time in shelter [%]');
hoff;
%
subplot(2,3,2);
for g=unique(group)
    val = nContacts(:, group == g) ./ validHours(:, group == g);
    errorbar(1:GroupsData.nDays, mean(val, 2), stderr(val, 2), 'color', GroupsData.Colors(g, :)); hon
end
xaxis(.8, GroupsData.nDays+.2);
set(gca, 'XTick', 1:GroupsData.nDays); 
 
xlabel('day');
ylabel('no. of contacts per hour');
hoff;
%
subplot(2,3,3);
for g=unique(group)
    val = nChases(:, group == g) ./ nContacts(:, group == g) * 100;
    errorbar(1:GroupsData.nDays, mean(val, 2), stderr(val, 2), 'color', GroupsData.Colors(g, :)); hon
end
xaxis(.8, GroupsData.nDays+.2);
set(gca, 'XTick', 1:GroupsData.nDays); 
 
xlabel('day');
ylabel('probability of chase-escape [%]');
hoff;
%
subplot(2,3,4);
for g=unique(group)
    val = nChases(:, group == g)./ validHours(:, group == g);
    errorbar(1:GroupsData.nDays, mean(val, 2), stderr(val, 2), 'color', GroupsData.Colors(g, :)); hon
end
xaxis(.8, GroupsData.nDays+.2);
set(gca, 'XTick', 1:GroupsData.nDays); 
 
xlabel('day');
ylabel('no. of chase-escapes per hour');
hoff;
%%
figure(2);
for g=unique(group)
    durations = [stat(group == g).Durations];
    [h, x] = hist(durations(durations < obj.FrameRate * 20), 20); 
    h = h / sum(h) * 100;
    plot(x / obj.FrameRate, h, 'color', GroupsData.Colors(g, :)); hon
end
xlabel('duration [sec]');
ylabel('frequency [%]');
hoff