function DepthShow(Depth)
surfl(Depth.X, Depth.Y, Depth.Z); shading interp; colormap(gray); axis equal
hon
a = axis;
w = zeros(size(Depth.X));
plot3(Depth.X, Depth.Y, w+a(5));
plot3(w+a(2), Depth.Y, Depth.Z);
plot3(Depth.X, w+a(4), Depth.Z);
hoff;