function obj = SocialFilterRepeats(obj)
%%
vec = obj.ROI.nZones.^(obj.nSubjects-1:-1:0);
zones = obj.zones(:, obj.valid);
d = [true diff(vec * zones) ~= 0];
zones = zones(:, d);
obj = SocialSetZones(obj, zones);