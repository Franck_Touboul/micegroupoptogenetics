%%
prev = cents.label;
[q, cents.label] = max(cents.logprob, [], 3);
cents.label(prev == 0) = 0;

thresh = options.colorMatchThresh;
ndata = size(cents.x, 1);

mx = max(cents.x(:));
mx = mx + mx / 100;
my = max(cents.y(:));
my = my + my / 100;
mapBinsX = sequence(0, mx, options.NumOfMapBins+1);
mapBinsY = sequence(0, my, options.NumOfMapBins+1);
mapBins = 1:options.NumOfMapBins^2+1;
for i=1:options.nSubjects
    logprobs = cents.logprob(:, :, i);
    logprobs(cents.label ~= i) = -inf;
    [maxlogprobs, centids] = max(logprobs, [], 2);
    maxprobs = exp(maxlogprobs);
    res{i}.x = cents.x(sub2ind(size(cents.x), 1:ndata, centids'));
    res{i}.y = cents.y(sub2ind(size(cents.y), 1:ndata, centids'));
    res{i}.prob = maxprobs';
    res{i}.logprob = maxlogprobs';
    res{i}.centids = centids';
    
    res{i}.valid = maxprobs' >= thresh;
    res{i}.x(maxprobs < thresh) = nan;
    res{i}.y(maxprobs < thresh) = nan;
    res{i}.prob(maxprobs < thresh) = nan;
    res{i}.logprob(maxprobs < thresh) = -inf;
    res{i}.id = (maxprobs' > thresh) * i;
    res{i}.centids(maxprobs < thresh) = nan;
    % divide into bins
    [hx, binLocX] = histc(res{i}.x, mapBinsX);
    [hy, binLocY] = histc(res{i}.y, mapBinsY);
    res{i}.bin = (binLocY - 1) * options.NumOfMapBins + binLocX;
    res{i}.bin(~res{i}.valid) = 0;
    %% compute statistics
    distance = sqrt(diff(res{i}.x).^2+diff(res{i}.y).^2);
    res{i}.distance = distance;
    res{i}.distanceBins = sequence(0, max(res{i}.distance), options.NumOfDistanceBins);
    res{i}.distanceDisc = histc(res{i}.distance, res{i}.distanceBins);
    res{i}.logpdist = res{i}.distanceDisc(1:options.NumOfDistanceBins) + 1;
    res{i}.logpdist = flog(res{i}.logpdist / sum(res{i}.logpdist));
    % p11:
    if options.MeanDistance > 0
        res{i}.meanDistance = options.MeanDistance;
    else
        res{i}.meanDistance = mean(distance(~isnan(distance)));
    end
    res{i}.pareto = gpfit(distance(distance>0));
    res{i}.logp1 = flog(mean(res{i}.valid(2:end) == 1));
    % p0g0: stay hidden
    res{i}.logp0g0 = flog(mean(diff(res{i}.valid) == 0 & res{i}.valid(2:end) == 0) / mean(res{i}.valid(2:end) == 0));
    % p1g0: appear
    res{i}.logp1g0 = flog(mean(diff(res{i}.valid) == 1)  / mean(res{i}.valid(2:end) == 0));
    % p0g1: disappear
    dissappear = [diff(res{i}.valid) == -1 false];
    h01 = histc(res{i}.bin(dissappear), mapBins);
    h_1 = histc(res{i}.bin(res{i}.valid(2:end) == 1), mapBins);
    prob = h01 ./ (h_1 + 1);
    prob(prob > options.MaxProbOfDissappearing) = options.MaxProbOfDissappearing;
    res{i}.logp0g1gl = flog(prob);
end
