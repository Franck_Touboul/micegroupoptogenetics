function SocialRegularizedPottsAnalyse(obj)
%%
cmap = MyCategoricalColormap;
model = obj.Analysis.Potts.Regularized;
ref = model.testProbs;
%ref = other.Analysis.Potts.Regularized.testProbs;

entries = {};
for s=1:obj.nSubjects
    entries{s} = num2str(s);
    map = model.order == s;
    map(s) = false;
    idx = find(map);
    stat = [];
    for k=1:length(idx)
        i = idx(k);
        stat.Djs(k) = JensenShannonDivergence(ref, model.Model{i}.prob');
        stat.beta(k) = model.RegBeta(i);
        stat.sumabs(k) = sum(abs(model.Model{i}.weights));
        stat.meanabs(k) = mean(abs(model.Model{i}.weights));
        stat.stdabs(k) = std(abs(model.Model{i}.weights));
        stat.zeros(k) = sum(model.Model{i}.weights == 0);
    end
    stat.log2beta = log2(stat.beta);
    subplot(2,2,1);
    semilogx(stat.beta, stat.Djs, '-o', 'color', cmap(s, :), 'MarkerFaceColor', cmap(s, :)); hon
    
    subplot(2,2,2);
    plot(stat.beta, stat.zeros / length(obj.Analysis.Potts.Model{s}.weights), '-o', 'color', cmap(s, :), 'MarkerFaceColor', cmap(s, :) ); hon;
    subplot(2,2,3);
    plot(stat.beta, stat.meanabs, '-o', 'color', cmap(s, :), 'MarkerFaceColor', cmap(s, :) ); hon;
    subplot(2,2,4);
    plot(stat.beta, stat.sumabs, '-o', 'color', cmap(s, :), 'MarkerFaceColor', cmap(s, :) ); hon;
    if s == 3
    %    errorbar(stat.beta, stat.meanabs, stat.stdabs, '-o', 'color', cmap(s, :), 'MarkerFaceColor', cmap(s, :) ); hon
    end
    set(gca, 'Xscale', 'log');
end
%
subplot(2,2,1);
hoff
%xaxis(min(stat.log2beta), 0);
prettyPlot('', 'beta', 'Djs [bits]');
%set(gca, 'YTick', [-3:0]);
%set(gca, 'YTickLabel', 10.^get(gca, 'YTick'));
legend(entries); legend boxoff
%
subplot(2,2,2);
hoff
set(gca, 'XScale', 'log');
%prettyPlot('', 'beta [log2]', 'number of zero weights');
prettyPlot('', 'beta', '% of zero weights');
%xaxis(min(stat.log2beta), 0);
subplot(2,2,3);
hoff
set(gca, 'XScale', 'log');
prettyPlot('', 'beta', 'mean absolute weight');

subplot(2,2,4);
hoff
set(gca, 'XScale', 'log');
prettyPlot('', 'beta', 'sum of absolute weights');
