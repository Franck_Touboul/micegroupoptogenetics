clear obj;

obj = Kinect('d:\5cage.1months.2012-12-02 125904.1.avi');
%%
obj.Meta.MaxEquivDiameter = 0.080; % m
obj.Meta.MinEquivDiameter = 0.040; % m
obj.Meta.HeightThresh = 0.020; % m
obj.Meta.MinInitEquivDiameter = 0.010; % m
obj.Meta.MinSolidity = .9;
obj.Meta.NumBkgFrames = 40;
%%
nchars = 0;
for i=1:obj.Meta.NumBkgFrames
    nchars = Reprintf(nchars, '# - frame no. %d/%d', i, obj.Meta.NumBkgFrames);
    obj.FrameNumber = randi(obj.NumberOfFrames);
    [obj, x,y,z] = obj.GetRectifiedRealWorldCoords;
    x(z == 0) = nan;
    y(z == 0) = nan;
    z(z == 0) = nan;
    if i == 1
        Z = z;
    else
        Z = cat(3, Z, z);
    end
end
obj.Meta.Bkg = nanmedian(Z, 3);
%%
for i=1:obj.NumberOfFrames
    %%
    obj.FrameNumber = i;
    [obj, x,y,z] = obj.GetRectifiedRealWorldCoords;
    x(z == 0) = nan;
    y(z == 0) = nan;
    z(z == 0) = nan;
    curr = obj.Meta.Bkg - z;
    curr(curr < 0) = nan;
    curr(curr > 0.10) = nan;
    curr(isnan(z) & ~isnan(obj.Meta.Bkg)) = 0.02;
    ax(1) = subplot(2,2,1);
    imagesc(curr);
    ax(2) = subplot(2,2,2);
    imagesc(z);
    ax(3) = subplot(2,2,3);
    imagesc(obj.Meta.Bkg);
    linkaxes(ax);    
end
%%
first = true;
for i=1:obj.NumberOfFrames
    obj.FrameNumber = i;
    %%
    [obj, x,y,z] = obj.GetRectifiedRealWorldCoords;
    x(z == 0) = nan;
    y(z == 0) = nan;
    z(z == 0) = nan;
    
    if first 
        obj.Meta.Plane.Depth   = nanmedian(z(:));
        onplane = z < obj.Meta.Plane.Depth   + .01 & z > obj.Meta.Plane.Depth - .01;
        onplaneIdx = find(onplane & ~isnan(x) & ~isnan(y));
        r = randi(length(onplaneIdx), [2, 5000]);
        r = onplaneIdx(r);
        pxdist = sqrt(diff(obj.X(r)).^2 + diff(obj.Y(r)).^2);
        rwdist = sqrt(diff(x(r)).^2 + diff(y(r)).^2);
        obj.Meta.Plane.Resolution = median((rwdist) ./ pxdist); % [m/pix]
        first = false;
    end
    
    %h = sum(~isnan(z), 1) > obj.VideoHeight * .4;
    %v = sum(~isnan(z), 2) > obj.VideoWidth * .4;
    %z = z(find(v, 1):find(v, 1, 'last'), find(h, 1):find(h, 1, 'last'));
    z = z(35:435, 35:590);
    z(z < .65) = nan;
    z(z > .75) = nan;
    %z(z < depth - .1) = nan;
    %z(z > depth + .1) = nan;
    
    %%
    cents = KinectObjectDetection(obj, z);
    map = zeros(size(z));
    for c=1:length(cents)
        map(cents(c).PixelIdxList) = z(cents(c).PixelIdxList) - obj.Meta.Plane.Depth;
    end
    %imagesc(map);
    N = 6;
    gwin = repmat(gausswin(N)', N, 1) .* repmat(gausswin(N), 1, N);
    imagesc(convn(map, gwin, 'same'));
    title(sprintf('%d', i));
    colormap(gray);
    %%
    axis off;
    drawnow;
end