opt.WinSize = 10;
%%
mdl = [];
%%
s = 1;
x = obj.x(s, obj.valid & ~obj.sheltered(s, :));
y = obj.y(s, obj.valid & ~obj.sheltered(s, :));
res.datax = zeros(length(x) - opt.WinSize, opt.WinSize);
res.datay = zeros(length(x) - opt.WinSize, opt.WinSize);
for i=1:opt.WinSize
    res.datax(:, i) = x(i:end-opt.WinSize+i-1);
    res.datay(:, i) = y(i:end-opt.WinSize+i-1);
end
res.valx = x(opt.WinSize+1:end)';
res.valy = y(opt.WinSize+1:end)';
%%
mdl(s).linX = mvregress(res.datax, res.valx)
mdl(s).linY = mvregress(res.datay, res.valy)
%%




%%
s1 = 1;
s2 = 2;
x = [...
    obj.x(s1, obj.valid & ~obj.sheltered(s1, :) & ~obj.sheltered(s2, :));
    obj.x(s2, obj.valid & ~obj.sheltered(s1, :) & ~obj.sheltered(s2, :))];
y = [...
    obj.y(s1, obj.valid & ~obj.sheltered(s1, :) & ~obj.sheltered(s2, :));
    obj.y(s2, obj.valid & ~obj.sheltered(s1, :) & ~obj.sheltered(s2, :))];
% velocity
v = [[0;0], sqrt(diff(x, 1, 2).^2 + diff(y, 1, 2).^2) / obj.dt];
% angle
a = [[0;0], atan2(diff(y, 1, 2), diff(x, 1, 2))];
% joint axis angle
r = atan2(diff(y), diff(x));
% distance between mice
d = sqrt(diff(y).^2 + diff(x).^2);
% angle wrt joint axis
ra = a - [pi-r;r]; 
ra(ra < -pi) = ra(ra < -pi) + 2*pi;
ra(ra >  pi) = ra(ra >  pi) - 2*pi;
% speed wrt joint axis
rv = v .* cos(ra);
%%
a = zeros(obj.VideoWidth, obj.VideoHeight);
for r=1:obj.ROI.nRegions
    a = a + obj.ROI.Regions{i};
end

cmap = MyMouseColormap;
for i=1:length(x)
    plot(x(1, i), y(1, i), 'o', 'MarkerFaceColor', cmap(s1, :), 'MarkerEdgeColor', 'none'); hon
    plot(x(2, i), y(2, i), 'o', 'MarkerFaceColor', cmap(s2, :), 'MarkerEdgeColor', 'none'); hoff
    
    axis([0, obj.VideoWidth 0, obj.VideoHeight]);
    drawnow;
end