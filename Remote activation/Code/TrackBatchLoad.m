function objs = TrackBatchLoad(objs, path, fields)
if nargin < 2
    path = '';
end
if nargin < 3
    allFields = true;
    fields = {};
else
    allFields = false;
end

fprintf('# loading experiments...\n');
if ischar(objs)
    filename = objs;
    try
        load(filename);
    catch
        fid = fopen(filename, 'r');
        list = {};
        index = 1;
        tline = fgetl(fid);
        while ischar(tline)
            list{index} = tline;
            tline = fgetl(fid);
            index = index + 1;
        end
        list = unique(list);
        fclose(fid);
        objs = LoadFromList(list, path, allFields, fields);
    end
elseif iscell(objs)
    objs = LoadFromList(objs, path, allFields, fields);
end

function objs = LoadFromList(list, path, allFields, fields)
objs = struct();
for i=1:length(list)
    field = regexprep(list{i}, '\.', '_');
    try
        currFilename = [path '/' list{i} '.obj.mat'];
        fprintf(['# - loading from: ' currFilename '\n']);
        currObj = load(currFilename);
        if ~allFields
            currObj.obj = SocialObjectDilute(currObj.obj, fields);
        end
        objs.(field) = currObj.obj;
    catch
        fprintf(['#    + failed!\n']);
    end
end
