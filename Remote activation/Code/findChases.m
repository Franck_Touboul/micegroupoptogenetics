function [events, state] = findChases(m1, m2)
analyzed = SocialBehaviour;

%%
model.states(1).func = @(x) halfCircularUniform(x(1));
model.states(1).title = '-';
model.states(2).func = @(x) halfCircularExp(x(1), pi/2);
model.states(2).title = 'predator';
model.states(3).func = @(x) halfCircularExp(pi - x(1), pi/2);
model.states(3).title = 'prey';

persistence = 1000;
pst = 1-1/persistence;
ptr = 1/persistence;
model.trans = [...
    pst ptr ptr; 
    ptr pst ptr; 
    ptr ptr pst
    ];
model = validateModel(model);

% dx= 0.01;
% x = 0:dx:pi; 
% y1 = model.states(1).func(x);
% y2 = model.states(2).func(x);
% y3 = model.states(3).func(x);
% %
% plot(x1, n_mean / (x1(2)-x1(1)), 'r', x2, n_contact/ (x2(2)-x2(1)), 'g')
% hold on;
% plot(x, y1, 'r', x, y2, 'g', x, y3, 'm');
% hold off;

%%
%[pStates, logpseq, alpha, beta, scale, table] = ModelProbs(model, shiftdim(analyzed.angle(m1, m2, :))');
input = [...
    shiftdim(analyzed.angle(m1, m2, :))'; 
    shiftdim(analyzed.contacts(min(m1, m2), max(m1, m2), :))'
    ];

valid = ~isnan(input);
input = input(valid);
backtrack = ModelViterbiSequence(model, input);

state = ones(1, analyzed.nData);
state(valid) = backtrack;
%%
c = conv([0 (state~=1)  0], [1 -1]);
events.startFrame = find(c > 0) - 1;
events.endFrame   = find(c < 0) - 2;
events.startTime  = events.startFrame * (analyzed.time(2) - analyzed.time(1));
events.endTime    = events.endFrame * (analyzed.time(2) - analyzed.time(1));

for i=1:length(events.startFrame)
    events.title{i} = num2str(state(events.startFrame(i)));
end
