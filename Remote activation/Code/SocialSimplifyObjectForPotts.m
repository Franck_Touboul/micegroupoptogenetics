function obj = SocialSimplifyObjectForPotts(obj)
commonSubs = {'zones', 'ROI', 'valid', 'nFrames', 'FrameRate', 'dt'};
f = fieldnames(obj);
for i=1:length(f)
    if ~any(strcmp(f{i}, commonSubs))
        %d = whos(['obj.' f{i}]);
        %if d.byts > 
        if isstruct(obj.(f{i}))
            obj = rmfield(obj, f{i});
        elseif ischar(obj.(f{i}))
        elseif isscalar(obj.(f{i}))
        elseif length(obj.(f{i})) <= 1
        elseif isempty(obj.(f{i}))
        else
            obj = rmfield(obj, f{i});
        end
    end
end
roi = obj.ROI;
obj.ROI = [];
obj.ROI.nZones = roi.nZones;
obj.ROI.nZones = roi.ZoneNames;


