function plot_binary(b, f)
if (nargin == 1)
    f = '';
end
% plots a binary sequence of numbers
nb = length(b);
y = zeros(1, 2*nb+2);
y(2:2:end-1) = b(1:end);
y(3:2:end-1) = b(1:end);
y(1) = 0;
y(end) = 0;
t = 0.5:nb+0.5;
t = zeros(1, 2*nb+2);
t(2:2:end-1) = 0.5:nb-0.5;
t(3:2:end-1) = t(2:2:end-1)+1;
t(1) = 0.5;
t(end) = t(end-1);
plot(t,y, f);
axis([0.5, nb+0.5, -0.2, 1.2]);
nTicks = min(nb, 10);
dTicks = floor(nb / nTicks);
set(gca, 'XTick', [1:dTicks:nb])
