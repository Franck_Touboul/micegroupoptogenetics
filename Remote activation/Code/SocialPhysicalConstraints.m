SocialExperimentData;
%%
count = zeros(obj.ROI.nZones, obj.nSubjects + 1);
p = zeros(obj.ROI.nZones, 1);
tot = 0;
for i=GroupsData.Standard.idx
    obj = TrackLoad({i});
    %%
    for z=1:obj.ROI.nZones; 
        s=sum(obj.zones(:, obj.valid) == z);
        count(z, :) = count(z, :) + histc(s, 0:obj.nSubjects);
        p(z) = p(z) + sum(sum(obj.zones(:, obj.valid) == z));
        tot = tot + length(obj.valid) * obj.nSubjects;
    end
end
res = [];
res.p = p / sum(p);
res.count = count;
%%
order = [1 7 4 2 3 5 8 10 6 9];
cmap = MyZonesColormap;
idx = 1;
for z=order
    subplot(2,5,idx);
    r = res.count(z, 2:end) / sum(res.count(z, :));
    bar(r * 100, 'FaceColor', cmap(z, :), 'edgecolor', 'none');
    p = r(1);
    hon
    for i=1:obj.nSubjects
        plot(i, nchoosek(obj.nSubjects, i) * res.p(z)^i*(1 - res.p(z))^(obj.nSubjects-i) * 100, 'ko', 'MarkerFaceColor', 'k');
    end
    hoff;
    title(obj.ROI.ZoneNames{z});
    xaxis(0, 5)
    idx = idx + 1;
end
