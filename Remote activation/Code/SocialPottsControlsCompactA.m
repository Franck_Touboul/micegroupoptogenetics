function SocialPottsControlsCompactA(obj)
obj = TrackLoad(obj);
%%
Options.EvenOddTimescales = [60];
obj.OutputToFile = false;
obj.OutputInOldFormat = false;
obj.Analysis.Potts.nIters = [2000 20000];
obj.Analysis.Potts.MinNumberOfIters = [500 1000];
obj.Analysis.Potts.Confidence = 0.05;
%%
oidx = 1;
objs = {};
%% even-odd
try
    for ts = Options.EvenOddTimescales
        %%
        frames = ts * obj.FrameRate;
        map = ceil((1:obj.nFrames) / frames);
        even = mod(map, 2) == 1;
        odd = ~even;
        %%
        Types = {'even', 'odd'};
        for i=1:length(Types);
            map = eval(Types{i});
            curr = obj;
            curr.zones = obj.zones(:, map);
            curr.valid = obj.valid(:, map);
            curr.nFrames = sum(map);
            %curr.me = TrainPottsModel(curr, 1:obj.nSubjects);
            curr = SocialPotts(curr);
            curr.Analysis.Potts.nSamples = sum(curr.valid);
            objs{oidx}.(Types{i}) = curr.Analysis.Potts;
        end
        oidx = oidx + 1;
    end
catch me
    warning(me.message);
end
%%
save(['SocialPottsControlsCompactA.' obj.FilePrefix], 'objs', '-v7.3');

