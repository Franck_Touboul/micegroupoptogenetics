SocialExperimentData
options.Day = 0;
res = [];
res.entropy = [];
res.rank = [];
for id=1:GroupsData.nExperiments
    obj = TrackLoad({id options.Day});
    o = TrackLoad({id 1});
    g = GroupsData.group(id);
    %%
    for s=1:obj.nSubjects
        h = histc(obj.zones(s, obj.valid), 1:obj.ROI.nZones);
        p = h / sum(h);
        res.entropy(id, s) = -p * log2(p');
    end
    res.rank(id, :) = o.Hierarchy.Group.AggressiveChase.rank;
end
%%
for id=1:GroupsData.nExperiments
    g = GroupsData.group(id);
    rank = res.rank(id, :);
    entropy = res.entropy(id, :);
    status = max(rank) - rank + 1;
    status(status > 3) = 3;
    plot([min(entropy) max(entropy)], [id id], 'color', GroupsData.Colors(g, :)); hon
    for s=1:obj.nSubjects
        plot(entropy(s), id, 'o', 'MarkerFaceColor', ones(1, 3) * (status(s) - 1)/2, 'MarkerEdgeColor', 'k');
    end
end
hoff;
%%
stat = [];
for g=1:GroupsData.nGroups
    map = GroupsData.group == g;
    rank = res.rank(map, :);
    status = repmat(max(rank, [], 2), 1, obj.nSubjects) - rank + 1;
    status(status > 3) = 3;
    entropy = res.entropy(map, :);
    for s=1:3
        stat(g).val{s} = entropy(status == s);
        for sp = 1:s-1
            stat(g).sig(s, sp) = ttest2(stat(g).val{s}, stat(g).val{sp});
        end
        stat(g).m(s) = mean(entropy(status == s));
        stat(g).err(s) = stderr(entropy(status == s));
    end
end
barweb([stat(1).m'; stat(2).m'], [stat(1).err'; stat(2).err']);