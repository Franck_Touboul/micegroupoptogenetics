function [x,y,z] = ModelBasic(BodyYaw, HeadYaw, Rotation)
Opt = struct();
Opt.Resolution = 100;

fields = {...
    'MajorAxis',1,...
    'DX',0,...
    'DY',0,...
    'Height', 0.5,...
    'BodyYaw', 0 ...
    'BodyFat', 1, ...
    'RearFat', 1 ...
    };
Param = struct(fields{:});
%%
ModelParam.Body.Yaw = BodyYaw;
ModelParam.Body.FatScale = Param.BodyFat;
ModelParam.Body.RearScale = Param.RearFat;
ModelParam.Neck.Yaw = HeadYaw;
ModelParam.Neck.Pitch = 0;
ModelParam.AxisMundi.Pitch = 0;
ModelParam.AxisMundi.Yaw = Rotation;
ModelParam.AxisMundi.Roll = 0;
ModelParam.AxisMundi.Scale = 1;


Model = ModelCreateF(ModelParam, Opt);
Model.SkinMemory = .9;

[x,y,z] = ModelToDepth(Model, Opt.Resolution);
