function probs = EstimateMarkov(obj, z)
z1 = reshape(z(1:end-mod(length(z),2)), 2, floor(length(z)/2));
z2 = reshape(z(2:end-(1 - mod(length(z),2))), 2, floor((length(z) - 1)/2));
mp = [];
mp(:, 1:2:(length(z1)*2)) = z1;
mp(:, 2:2:(length(z2)*2+1)) = z2;
%%
h = hist3(mp', {1:obj.ROI.nZones, 1:obj.ROI.nZones});
probs.joint = h / sum(h(:));
probs.ind = sum(probs.joint, 1);
probs.cond = probs.joint ./ repmat(probs.ind', 1, obj.ROI.nZones);
