function xaxis(x1, x2)
a = axis;
if nargin == 1
    if length(x1) == 2
        x2 = x1(2);
        x1 = x1(1);
    else
        x2 = a(2);
    end
else
    if isnan(x1)
        x1 = a(1);
    end
end
axis([x1 x2 a(3) a(4)]);