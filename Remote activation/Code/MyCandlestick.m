function [outliersIndex] = MyCandlestick(data)
%%
cmap = MySubCategoricalColormap;
colors.inner = cmap(6, :);
colors.outer = cmap(2, :);
colors.median = [0 0 0];
%
cmap = MySubCategoricalColormap;
colors.inner = [0 180 225]/256;
colors.outer = [218 218 218]/256;
colors.median = [81 81 81]/256;

%
%outcmap = MyDefaultColormap(size(data, 2));
nData = 1;
if iscell(data)
    Q = [];
    for i=1:length(data);
        Q(:, i) = quantile(data{i}, [.25 .5 .75]);
    end
    nData = length(data);
else
    nData = size(data, 1);
    Q = quantile(data', [.25 .50 .75]);
end
if isvector(Q)
    Q = Q(:);
end
IQR = Q(3, :) - Q(1, :);
R = [];
R(1, :) = Q(1,:) - IQR * 1.5;
R(2, :) = Q(3,:) + IQR * 1.5;

width = 0.8;
outliersIndex = [];


for i=1:nData
    if iscell(data)
        curr = data{i};
    else
        curr = data(i, :);
    end
    valid = curr <= R(2, i) & curr >= R(1, i);
    ivalidIdx = find(~valid);
    outliers = curr(~valid);
    inliers = curr(valid);
    MyFilledBox([i-width/2 i+width/2], [min(curr(valid)) max(curr(valid))], colors.outer);
    hold on;
    MyFilledBox([i-width/2 i+width/2], [Q(1, i) Q(3, i)], colors.inner);
    plot([i-width/2 i+width/2], [Q(2, i) Q(2, i)], '-', 'LineWidth', 2, 'color', colors.median);
    for j=1:length(outliers)
        plot(i, outliers(j), 'o', 'MarkerFaceColor', 'k',  'MarkerEdgeColor', 'none');
        text(i, outliers(j), [' ' num2str(ivalidIdx(j))]);
        outliersIndex = [outliersIndex; i, ivalidIdx(j), outliers(j)];
    end
end
hold off;

return
function myBoxPlot(x, labels, colors)
if nargin < 2
    labels = {};
end
if nargin < 3
    colors = colormap(lines);
end
%x = x / x(end);
width = 0.2;
for i=length(x):-1:1
    drawBox(x(i), width, colors(i, :))
    hold on;
end
hold off;

prev = 0;
for i=1:length(x)
    if i <= length(labels)
        text(width/2, prev + (x(i) - prev) / 2, labels{i}, 'Color', 'w', 'VerticalAlignment', 'Middle', 'HorizontalAlignment', 'center');
    end
    prev = x(i);
end
axis([0 width 0 x(end)]);
try
    set(gca, 'XTick', [], 'YTick', x);
end
tickLabels = {};
for i=1:length(x)
    tickLabels{i} = sprintf('%5.2f (%5.1f%%)', x(i), x(i)/x(end)*100);
end
try
set(gca, 'XTick', [], 'YTick', x, 'YTickLabel', tickLabels, 'YAxisLocation', 'right');
end
function drawBox(height, width, color)
fill([0 width width 0], [0 0 height height], color)
