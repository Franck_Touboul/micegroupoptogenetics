function str = StructToXML(obj, filename, root, depth)
useBase64 = false;
writeAsBytes = true;
if useBase64
    encoder = org.apache.commons.codec.binary.Base64;
end

str = '';
if ~exist('root', 'var')
    str = [str, '<?xml version="1.0" encoding="UTF-8" ?>\n'];
    root = 'root';
end
if ~exist('depth', 'var')
    depth = 0;
end

tab = ''; for d=1:depth; tab = [tab, '\t']; end
xtab = [tab '\t'];
str = [str, tab, '<', root, '>\n'];
chunk = 50000;

f = fieldnames(obj);
for i=1:length(f)
    if isstruct(obj.(f{i}))
        nstr = StructToXML(obj.(f{i}), [], f{i}, depth + 1);
    else
        nstr = [xtab, '<' f{i} ' type="' class(obj.(f{i})) '" size="' num2str(size(obj.(f{i}))) '"> '];
        if ~isempty(obj.(f{i}))
            if useBase64
                switch class(obj.(f{i}))
                    case {'char'}
                        nstr = [nstr, sprintf('%s ', obj.(f{i}))];
                    case {'logical'}
                        bin2dec(sprintf('%d', obj.(f{i})));
                        nstr = [nstr, sprintf('%g ', obj.(f{i}))];
                    otherwise
                        varlen = length(obj.(f{i})(:));
                        for c=1:chunk:varlen
                            nstr = [nstr, char(encoder.encode(typecast(obj.(f{i})(c:min(c+chunk-1, varlen))', 'uint8'))'), ' '];
                        end
                end
            elseif writeAsBytes
                switch class(obj.(f{i}))
                    case {'char'}
                        nstr = [nstr, sprintf('%s ', obj.(f{i}))];
                    case {'logical'}
                        bin2dec(sprintf('%d', obj.(f{i})));
                        nstr = [nstr, sprintf('%g ', obj.(f{i}))];
                    otherwise
                    nstr = [nstr, num2str(typecast(obj.(f{i})(:)', 'uint8'))];
                end
            else
                switch class(obj.(f{i}))
                    case {'double', 'single'}
                        nstr = [nstr, sprintf('%g ', obj.(f{i}))];
                    case {'logical'}
                        nstr = [nstr, sprintf('%g ', obj.(f{i}))];
                    case {'char'}
                        nstr = [nstr, sprintf('%s ', obj.(f{i}))];
                    case {'int8', 'uint8', 'int16', 'uint16', 'int32', 'uint32', 'int64', 'uint64'}
                        nstr = [nstr, sprintf('%d ', obj.(f{i}))];
                    otherwise
                end
            end
        end
        nstr = [nstr, xtab, '</' f{i} '>\n'];
    end
    str = [str, nstr];
end
str = [str, tab, '</', root, '>\n'];
%%
% base64string = char(encoder.encode(pi))'
% encoder.decode(encoder.encode(pi))
% fprintf('%.20f', typecast(encoder.decode(encoder.encode(typecast(pi, 'uint8'))), 'double'));

