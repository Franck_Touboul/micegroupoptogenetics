function [obj, pairwiseJointProbs, me] = SocialComputePatternPairwiseProbs(obj)
nIters = 250;
%%
data = obj.zones(:, obj.valid) - 1;
count = obj.ROI.nZones;
%%
local = obj;
local.n = 2;
local.output = true;
local.count = count;
local.nIters = nIters;
me = trainPottsModel(local, data);
%%
p = exp(me.perms * me.weights');
perms_p = exp(me.perms * me.weights');
Z = sum(perms_p);
pairwiseJointProbs = p / Z;
