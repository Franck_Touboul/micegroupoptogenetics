function me = trainPottsModelWithSelectiveRegularization(options, data, confidence)
% Sources: 1.   Maximum entropy density estimation with generalized regulari-
%               zation and an application to species distribution modeling, 
%               with S. J. Phillips and R. E. Schapire, Journal of Machine 
%               Learning Research 8, 2007, 1217-1260
%          2.   A maximum entropy approach to species distribution modeling, 
%               with S. J. Phillips and R. E. Schapire, Proceedings of the 
%               21st International Conference on Machine Learning, 2004, 655-662

options.param_report = false;
options = setDefaultParameters(options, ...
    'Statistics', false, ...
    'tk', 1, ...
    'beta', 0.01, ...
    'KLConvergenceRatio', 1e-4, ...
    'nIters', 20000);

me.tk = options.tk;
me.LineFactor = .5;
useLineSearch = true;
me.LineSearchMaxSteps = 20;
me.beta = options.beta;
me.KLConvergenceRatio = -options.KLConvergenceRatio;
me.order = options.n;
options.NeutralZone = 1; % Open
%%
use_sparse = true;
if nargin < 3
    confidence = -.05;
end
me.confidence = confidence;
me.neutralZone = options.NeutralZone;

if isfield(options, 'Output')
    output = options.Output;
else
    output = true;
end
%
if output; fprintf('# Training Potts Model\n'); end
%
if output; fprintf('# - finding all permutation of max dimension %d...\n', options.n); end;
%%
allPerms = nchoose(1:options.nSubjects);
nPerms = {};
j = 1;
for i=1:length(allPerms)
    if length(allPerms{i}) <= options.n
        nPerms{j} = allPerms{i};
        j = j + 1;
    end
end

%%
if output; fprintf('# - computing constraints...\n'); end
[me.constraints, pci] = PottsComputeConstraints(data' + 1, options.count, nPerms, abs(confidence));
C = length(nPerms);
%
me.inputPerms = myPerms(options.nSubjects, options.count);
[me.perms, me.labels] = assignFeature(me.inputPerms, options.count, nPerms, use_sparse);

valid = false(1, length(me.labels));
if options.count == 2
    for i=1:length(me.labels)
        if all(me.labels{i}(2, :) ~= 2)
            valid(i) = true;
        end
    end
    me.labels = me.labels(valid);
    me.perms = me.perms(:, valid);
    me.constraints = me.constraints(valid);
    pci = pci(valid);
end
%
me.nSubjects = options.nSubjects;
me.range = options.count;
me.weights = randn(1, size(me.labels, 2)) / size(me.labels, 2);
orig = me;
%% filter neutral zone
neutral = false(1, length(orig.labels));
for l=1:length(me.labels)
    neutral(l) = any(orig.labels{l}(2, :) == options.NeutralZone);
end
me.constraints = orig.constraints(~neutral);
me.perms = orig.perms(:, ~neutral);
me.labels = orig.labels(~neutral);
% initialize the weights
if isfield(options, 'initialPottsWeights')
    me.weights = options.initialPottsWeights;
else
    me.weights = orig.weights(~neutral);
end
%
pci = pci(~neutral, :);

%% compute empirical probabilities
base = me.range;
baseVector = base.^(size(data, 1)-1:-1:0);
pEmp = histc(baseVector * data+1, 1:base ^ size(data, 1));
pEmp = pEmp / sum(pEmp);
%% initial halfKL
currBeta = me.beta * sqrt(me.constraints - me.constraints.^2) / sqrt(size(data, 1));
P = exp(me.perms * me.weights');
Z = sum(P);
p = P / Z;
E = full(sum(me.perms .* repmat(p, 1, size(me.perms, 2))));
kl2 = -pEmp * log2(p) + sum(currBeta .* abs(me.weights));
kl1 = pEmp * log2(pEmp' + (pEmp' == 0));
%%
if options.Statistics; me.Statistics = struct(); end
tic;
me.converged = false;
me.KLconverged = false;
currTk = ones(1, length(me.weights)) * me.tk;
tic
for iter=1:options.nIters
    if output; fprintf('# - iter (%4d/%4d) : ', iter, options.nIters); end
    df = log(...
        ( (me.constraints - currBeta) .* (1 - E)) ./ ...
        ((1 - me.constraints + currBeta) .* E)...
        );
    map = imag(df) | df <= 0 | ~isfinite(df);
    df(map) = log(...
                ( (me.constraints(map) + currBeta(map)) .* (1 - E(map))) ./ ...
                ((1 - me.constraints(map) - currBeta(map)) .* E(map))...
                );
    map = (imag(df) | df >= 0 | ~isfinite(df)) & map;
    df(map) = -me.weights(map);
    target = -log(1 + (exp(df) - 1) .* E) + df .* me.constraints - currBeta .* (abs(me.weights + df) - abs(me.weights));
    [DF, J] = max(target);
     for j=J
        E(j) = me.perms(:, j)' * p;
        %%
        t = min(currTk(j) / me.LineFactor, me.tk);
        df = DF;
        found = true;
        if useLineSearch
            found = false;
            nsteps = 1;
            while ~found
                update = exp(me.perms(:, j) * (t * df));
                pt = P .* update;
                pt = pt / sum(pt);
                %found = -pEmp * log2(pt) < kl2 - me.alpha * t * sum(df.^2);
                ckl2 = -pEmp * log2(pt);
                found = ckl2 + sum(currBeta .* abs(me.weights)) < kl2;
                if ~found
                    t = me.LineFactor * t;
                end
                if nsteps >= me.LineSearchMaxSteps
                    break;
                end
                nsteps = nsteps + 1;
            end
        end
        currTk(j) = t;
        if found
            me.weights(j) = me.weights(j) + t * df;
            %
            update = exp(me.perms(:, j) * t * df);
            P = P .* update;
            Z = sum(P);
            p = P / Z;
        end
    end
    prev_kl = kl1 + kl2;
    kl2 = -pEmp * log2(p + (pEmp' == 0)) + sum(currBeta .* abs(me.weights));

    if confidence ~= 0
        %[sum(E'  > pci(:, 1) & E' < pci(:, 2)) (1 - confidence) * length(me.weights)]
        nconverged = sum(E'  > pci(:, 1) & E' < pci(:, 2));
        if output; fprintf('KL = %6.4f, convergence = %5.2f%%, time=%s\n', kl1 + kl2, (nconverged / length(me.weights) * 100), sec2time(toc)); end
        me.nconverged = nconverged;
%         if nconverged == length(me.weights) && iter >= options.MinNumberOfIters
%             if output;
%                 fprintf('# converged to confidence interval (alpha = %3.2f)\n', confidence);
%             end
%             me.converged = true;
%             break;
%         end
    else
        if output; fprintf('KL = %6.4f (%.3f), time=%s\n', kl1 + kl2, abs((kl1 + kl2 - prev_kl)/prev_kl), sec2time(toc)); end
    end
    
    if me.KLConvergenceRatio > 0
        if abs((kl1 + kl2) - prev_kl) / prev_kl <= me.KLConvergenceRatio && iter >= options.MinNumberOfIters 
            if me.KLconverged
                break;
            else
                me.KLconverged = true;
            end
        elseif me.KLconverged
            me.KLconverged = false;
        end
    end
end
me.nIters = iter;
me.Dkl = kl1 + kl2;
%me = rmfield(me, 'features');
