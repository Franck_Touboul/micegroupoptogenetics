function obj = TrackComputeNoiseStatistics(obj)
obj = TrackLoad(obj);
nFrames = 100;

fprintf('# finding noise statistics\n');
fprintf('# - using random frames\n');
i = 0;
n = RePrintf('# - frame %d of %d', i, nFrames);
bkgFrame = im2double(imresize(obj.BkgImage, obj.VideoScale));
Lum = [];
while i < nFrames
    m1 = 1; %nFrames * 1/3;
    m2 = obj.nFrames; % * 2/3;
    
    r = floor(rand(1) * (m2 - m1) + m1);
    if r * obj.dt < obj.StartTime; continue; end
    if obj.EndTime > 0 && r * obj.dt > obj.EndTime; continue; end
    orig = myMMReader(obj.VideoFile, r, obj.BkgImage);
    if ~isempty(orig)
        i = i +1;
        n = RePrintf(n, '# - frame %d of %d', i, nFrames);
        orig = im2double(imresize(orig, obj.VideoScale));
        m = imsubtract(orig, bkgFrame);
        lum = max(m,[],3);
        Lum = [Lum, lum(:)];
        %%
    end
end
fprintf('\n');
obj.Noise.mean = mean(lum(:));
obj.Noise.std = std(lum(:));
obj.Noise.median = median(lum(:));
obj.Noise.mad = RobustStd(lum(:));
obj.Noise.q = quantile(lum(:), [.25 .75]);
