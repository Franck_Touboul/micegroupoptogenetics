function obj = SocialPottsVectors(obj)
obj = TrackLoad(obj);

p = perms(1:obj.nSubjects);
obj.Analysis.PottsVectors.Perms = p;
obj.Analysis.PottsVectors.V = cell(1, obj.nSubjects);
for order = 1:obj.nSubjects;
    w = [];
    for i=1:size(p, 1)
        newobj = SocialRemapPottsWeights(obj, p(i, :), order);
        w = [w; newobj.Analysis.Potts.Model{order}.weights];
    end
    obj.Analysis.PottsVectors.V{order} = w;
end
if obj.OutputToFile
    fprintf('# - saving data\n');
    TrackSave(obj);
end
