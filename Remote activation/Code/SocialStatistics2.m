function obj = SocialStatistics(obj)
obj = TrackLoad(obj);
RunOnGroup = true;
nIters = 200;

%% independent
fprintf('# - independent\n');
[obj, indepProbs, jointProbs, pci, patterns] = SocialComputePatternProbs(obj, true);
IndepDkl = jointProbs * (log2(jointProbs) - log2(indepProbs))';

%% subject permuted
fprintf('# - subject permuted\n');
SPObj = obj;
SPObj.zones = obj.zones([2:obj.nSubjects, 1], :);
[SPObj, SPIndepProbs, SPJointProbs, SPpci] = SocialComputePatternProbs(SPObj, true);
SPDkl = jointProbs * (log2(jointProbs) - log2(SPJointProbs))';

if RunOnGroup
    SPPerms = perms(1:obj.nSubjects);
    SPAllDkl = zeros(1, size(SPPerms, 1));
    nchars = 0;
    for i=1:size(SPPerms, 1)
        nchars = Reprintf(nchars, '#  . perm no. %d/%d', i, size(SPPerms, 1));
        CurrSPObj = obj;
        CurrSPObj.zones = obj.zones(SPPerms(i, :), :);
        [CurrSPObj, CurrSPIndepProbs, CurrSPJointProbs] = SocialComputePatternProbs(CurrSPObj, true);
        SPAllDkl(i) = jointProbs * (log2(jointProbs) - log2(CurrSPJointProbs))';
    end
    fprintf('\n');
    SPPermOrder = sum(SPPerms ~= repmat(1:obj.nSubjects, size(SPPerms, 1), 1), 2);
end

%% time shifted
fprintf('# - time shifted\n');
TSObj = obj;
for i=1:obj.nSubjects
    start = round(obj.nFrames / obj.nSubjects * (i-1) + 1);
    TSObj.zones(i, start:end) = obj.zones(i, 1:end-start+1);
    TSObj.zones(i, 1:start-1) = obj.zones(i, end-start+2:end);
end
[TSObj, TSIndepProbs, TSJointProbs, TSpci] = SocialComputePatternProbs(TSObj, true);
TSDkl = jointProbs * (log2(jointProbs) - log2(TSJointProbs))';

if RunOnGroup
    TSAllDkl = zeros(1, nIters);
    nchars = 0;
    for iter=1:nIters
        nchars = Reprintf(nchars, '#  . iter no. %d/%d', iter, nIters);
        CurrTSObj = obj;
        for i=1:obj.nSubjects
            start = randi(obj.nFrames);
            CurrTSObj.zones(i, start:end) = obj.zones(i, 1:end-start+1);
            CurrTSObj.zones(i, 1:start-1) = obj.zones(i, end-start+2:end);
        end
        [CurrTSObj, CurrTSIndepProbs, CurrTSJointProbs] = SocialComputePatternProbs(CurrTSObj, true);
        TSAllDkl(iter) = jointProbs * (log2(jointProbs) - log2(CurrTSJointProbs))';
    end
    fprintf('\n');
end

%% output
% observed
subplot(3,2,1);
PlotProbProb(jointProbs, indepProbs, obj.nValid);
prettyPlot(sprintf('independent (D_{KL}=%f)', IndepDkl), 'observed pattern prob [log10]', 'independent approximation prob [log10]');

% subject permuted
subplot(3,2,3);
PlotProbProb(jointProbs, SPJointProbs, obj.nValid);
prettyPlot(sprintf('subject permuted (D_{KL}=%f)', SPDkl), 'observed pattern prob [log10]', 'permuted pattern prob [log10]');

if RunOnGroup
    subplot(3,2,4);
    SPDklMean = zeros(1, obj.nSubjects - 1);
    SPDklSTDErr = zeros(1, obj.nSubjects - 1);
    for i=2:obj.nSubjects
        SPDklMean(i-1) = mean(SPAllDkl(SPPermOrder == i));
        SPDklSTDErr(i-1) = stderr(SPAllDkl(SPPermOrder == i));
    end
    barweb(SPDklMean', SPDklSTDErr', .8, 2:obj.nSubjects);
    hold on;
    for i=2:obj.nSubjects
        plot(i-1, SPAllDkl(SPPermOrder == i)', 'kx');
    end
    hold off;
    prettyPlot('for all permutations', 'no. of changes', 'D_{KL}');
end

% subject permuted
subplot(3,2,5);
PlotProbProb(jointProbs, TSJointProbs, obj.nValid);
prettyPlot(sprintf('time shifted (D_{KL}=%f)', TSDkl), 'observed pattern prob [log10]', 'shifted pattern prob [log10]');

if RunOnGroup
    subplot(3,2,6);
    hist(TSAllDkl, 20);
    prettyPlot('random shifts', 'D_{KL}', 'count');
end