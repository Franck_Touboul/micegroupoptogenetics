function obj = myReader(filename, frame)

persistent videoFilename;
persistent videoObj;

if ~strcmp(videoFilename, filename)
    videoFilename = filename;
    videoObj = mmreader(options.MovieFile);
end

if nargin > 1
    obj = read(videoObj, frame);
else
    obj = videoObj;
end
%xyloObj = mmreader(options.MovieFile);
