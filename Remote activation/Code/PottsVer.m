nPoints = 100000;
dim = 4;
%%
objs = {};
for dim=1:5
    if dim == 5
        m = rand([10, 10, 10, 10, 10]);
        m = m / sum(m(:));
        m = reshape(cumsum(m(:)), size(m));
        %%
        r = rand(1, nPoints);
        cm = m(:);
        id = zeros(1, nPoints);
        for i=1:nPoints
            id(i) = find(r(i) < cm, 1);
        end
        [i1, i2, i3, i4, i5] = ind2sub(size(m), id);
        zones = [i1; i2; i3; i4; i5];
    else
        %%
        cm = cumsum(obj.Analysis.Potts.Model{dim}.prob);
        r = rand(1, nPoints);
        zones = zeros(5, nPoints);
        for i=1:nPoints
            zones(:, i) = obj.Analysis.Potts.Model{4}.inputPerms(find(r(i) < cm, 1), :)';
        end
    end
    %%
    obj.zones = zones;
    obj.nFrames = nPoints;
    obj.valid = true(1, nPoints);
    objs{dim} = SocialPotts(obj);
end

%%
nPoints = 1000000;
me = obj.Analysis.Potts.Model{5};
me.weights = randn(1, 100000-1) * 10;
p = exp(me.perms * me.weights');
p = p / sum(p);
r = rand(1, nPoints);
zones = zeros(5, nPoints);
for i=1:nPoints
    zones(:, i) = me.inputPerms(find(r(i) < cm, 1), :)';
end
%
obj.zones = zones;
obj.nFrames = nPoints;
obj.valid = true(1, nPoints);
objs{dim} = SocialPotts(obj);
me2 = TrainPottsModel(obj, 5, zones);
