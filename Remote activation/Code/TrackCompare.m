options.test.name = 'trial_1';
options.output_path = 'res/';

options.nSubjects = 4;          % the number of subjects in the arena
options.MovieFile= 'C:\Documents and Settings\USER\Desktop\Hezi\Trial     1.mpg';
%%
xyloObj = mmreader(options.MovieFile);
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;
nFrames = xyloObj.NumberOfFrames;
sFrame = 1;
eFrame = 20000;
%nFrames = 100;

%%
filename = [options.output_path options.test.name '.results.mat'];
load(filename);

%%
options.outputMovieFile = 'res\analysis_compare.avi';
%options.outputMovieFile = '';

if exist('aviobj') && strcmp(aviobj.CurrentState, 'Open')
    aviobj = close(aviobj);
end

if ~isempty(options.outputMovieFile)
    aviobj = avifile(options.outputMovieFile, 'fps', xyloObj.FrameRate);
%    aviobj = avifile(options.outputMovieFile, 'fps', 1);
    aviobj.compression = 'wmv3';
%    aviobj.compression = 'SVQ3';
    %aviobj.compression = 'Cinepak';
end
%% map ethovision coordinates 
options.x1 = -40;
options.x2 =  40;
options.y1 = -33;
options.y2 =  33;
options.w = options.x2 - options.x1;
options.h = options.y2 - options.y1;

%%
fprintf('# comparing tracks\n');
global socialData;
nchars = 0;
nchars = RePrintf('# - frame %6d/%6d (%6.2fxRT)', 0, nFrames, 1);
cmap = lines;
cmap  = [0 1 1; 1 1 1; 1 0 1; 1 0 0];
cmap2 = [0 1 1; 1 1 1; 1 0 0; 1 0 1];
%cmap2 = data.meta.subject.color;
%cmap = subject.color;
window = 10;
bkg = zeros(xyloObj.Width, xyloObj.Height, 3);
tic
clf
for r=sFrame:eFrame
%for r=76:174
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d/%6d (%6.2fxiRT)', r, nFrames, RT);
    subplot(1,2,1);
    m = read(xyloObj, r);
    imagesc(m); hold on;
    for curr = 1:options.nSubjects;
        r1 = max(r-window, 1);
        currx = data.x(curr, r1:r);
        curry = data.y(curr, r1:r);
        plot(currx, curry, '.-', 'Color', cmap(curr, :));
        hold on;
        %nanStart  = find(isnan(currx)) - 1;
        %nanFinish = find(isnan(currx)) + 1;
        %plot(currx(nanStart(nanStart > 0)), curry(nanStart(nanStart > 0)), 'o', 'Color', cmap(curr, :), 'MarkerSize', 10);
        %plot(currx(nanFinish(nanFinish <= window)), curry(nanFinish(nanFinish <= window)), 'x', 'Color', cmap(curr, :), 'MarkerSize', 10, 'LineWidth', 4);
    end
%    axis([0 xyloObj.Width, 0 xyloObj.Height] * options.scale);
%    set(gca, 'YDir', 'rev', 'Color', [0 0 0]);
    title(sprintf('frame %d/%d', r, nFrames),'fontsize', 17);
    hold off;
    %%
    subplot(1,2,2);
    imagesc(m); hold on;
    for curr = 1:options.nSubjects;
        r1 = max(r-window, 1);
        currx = (socialData.x(curr, r1:r) - options.x1) / options.w * xyloObj.Width;
        curry = (1 - (socialData.y(curr, r1:r) - options.y1) / options.h) * xyloObj.Height;
        %hidden = data.zones.hidden(curr, r1:r);
        plot(currx, curry, '.-', 'Color', cmap2(curr, :));
        hold on;
        %nanStart  = find(isnan(currx)) - 1;
        %nanFinish = find(isnan(currx)) + 1;
        %plot(currx(nanStart(nanStart > 0)), curry(nanStart(nanStart > 0)), 'o', 'Color', cmap(curr, :), 'MarkerSize', 10);
        %plot(currx(nanFinish(nanFinish <= window)), curry(nanFinish(nanFinish <= window)), 'x', 'Color', cmap(curr, :), 'MarkerSize', 10, 'LineWidth', 4);
    end
%    axis([0 xyloObj.Width, 0 xyloObj.Height]);
%    set(gca, 'YDir', 'rev', 'Color', [0 0 0]);
%    set(gca, 'Color', [0 0 0]);
    title(sprintf('ethovision - frame %d/%d', r, nFrames),'fontsize', 17);
    hold off;
    %%
    %    
    if ~isempty(options.outputMovieFile)
        F = getframe(gcf);
        aviobj = addframe(aviobj,F);
    else
        drawnow;
    end
end
if ~isempty(options.outputMovieFile)
    aviobj = close(aviobj);
end
fprintf('\n');
