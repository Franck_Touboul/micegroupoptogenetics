function TrackShowAllRegions(obj, frame)
clf;
cmap = lines(16);
if (nargin == 1 || isempty(frame)) && isfield(obj, 'BkgImage') && ~isempty(obj.BkgImage)
    img = obj.BkgImage;
    for i=1:size(img, 3)
        img(:, :, i) = histeq(img(:, :, i));
    end
elseif nargin > 1
    img = myMMReader(obj.VideoFile, frame);
    for i=1:size(img, 3)
        img(:, :, i) = histeq(img(:, :, i));
    end
else
    img = myMMReader(obj.VideoFile, 1);
end

%% 
%%
for i=1:length(obj.ROI.RegionNames)
    color = cmap(mod(i-1, 16)+1, :);
    img = TrackShowRegion(img, obj.ROI.Regions{i}, 256 * color);
end
imagesc(img);

for i=1:length(obj.ROI.RegionNames)
    color = cmap(mod(i-1, 16)+1, :);
    [x, y] = find(obj.ROI.Regions{i});
    [zzz, q] = min(x+y);
    x = x(q);
    y = y(q);
    h = text(y + 5, x + 5, [num2str(i)], 'Color', color, 'FontSize', 30, 'HorizontalAlignment', 'Left', 'VerticalAlignment', 'Top');
end
set(gca, 'Position', [0 0 1 1]);
set(gcf, 'ToolBar', 'none');
set(gcf, 'MenuBar', 'none');
