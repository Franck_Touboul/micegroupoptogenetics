function TrackMovie_v2(nruns, id)
%
options.showMovie = false;

rand('twister',sum(100*clock));
randn('seed',sum(100*clock));
unique_id = sprintf('%09d', randi(999999999));
%
TrackDefaults;
options.movieChunkDuration = 25 * 5;
options.window = 10;
options.radios = 4;
options.bradios = 8;
%%
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nframes = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;
dt = 1/xyloObj.FrameRate;

emptyFrame = uint8(zeros(vidHeight, vidWidth, 3));
%%
parallel = false;
if exist('nruns') && exist('id')
    step = floor(nframes / nruns);
    curr = 0;
    for i=1:id
        prev = curr + 1;
        if i == nruns
            curr = nframes;
        else
            curr = prev + step - 1;
        end
    end
    startframe = prev;
    endframe = curr;
    nframes = endframe - startframe + 1;
    parallel = true;
else
    startframe = 14000;
    endframe = nframes;
    if ~exist('nruns')
        nruns = 1;
    end
    id = 1;
    step = floor(nframes / nruns);
end

%%
filename = [options.output_path options.test.name '.social.mat'];
load(filename);

%%
%options.outputMovieFile = '';
if options.outputToFile
    if exist('aviobj') && strcmp(aviobj.CurrentState, 'Open')
        aviobj = close(aviobj);
    end
    options.outputMovieFile = sprintf([options.output_path options.test.name '.social.%03d.avi'], id);
    aviobj = avifile(options.outputMovieFile, 'fps', xyloObj.FrameRate);
end
%%
if options.output
    clf
    subplot(3,4,1);
    mx = floor(sqrt(options.nSubjects));
    my = ceil(sqrt(options.nSubjects));
    imshow(reshape(social.meta.subject.centerColors, mx, my, 3));
    for i=1:options.nSubjects
        text(floor((i-1)/my)+1,mod(i-1, my) + 1, num2str(i), 'FontSize', 20, 'HorizontalAlignment','center','FontWeight', 'bold');
        %    text(floor((i-1)/my)+10.05,mod(i-1, my) + 1, num2str(i), 'FontSize', 20, 'HorizontalAlignment','center','FontWeight', 'bold', 'Color', 'white');
    end
end
%%
fprintf('# comparing tracks\n');
nchars = 0;
nchars = RePrintf('# - frame %6d [%d-%d] (%6.2fxiRT)', startframe, startframe, endframe, 0);
cmap = lines;
cmap  = [0 1 1; 1 1 1; 1 0 1; 1 0 0];
cmap2 = [0 1 1; 1 1 1; 1 0 0; 1 0 1];
cmap = social.meta.subject.centerColors;
%cmap2 = data.meta.subject.color;
%cmap = subject.color;
window = 10;
bkg = zeros(xyloObj.Width, xyloObj.Height, 3);
tic
for r=1:nframes
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d [%d-%d] (%6.2fxiRT)', r+startframe-1, startframe, endframe, RT);
    if options.showMovie
        m = myMMReader(options.MovieFile, r+startframe-1);
    else
        m = emptyFrame;
    end
    %     imagesc(m); hold on;
    %     for curr = 1:options.nSubjects;
    %         r1 = max(r-window, 1);
    %         currx = social.x(curr, r1:r);
    %         curry = social.y(curr, r1:r);
    %         plot(currx, curry, '.-', 'Color', cmap(curr, :));
    %         hold on;
    %         %nanStart  = find(isnan(currx)) - 1;
    %         %nanFinish = find(isnan(currx)) + 1;
    %         %plot(currx(nanStart(nanStart > 0)), curry(nanStart(nanStart > 0)), 'o', 'Color', cmap(curr, :), 'MarkerSize', 10);
    %         %plot(currx(nanFinish(nanFinish <= window)), curry(nanFinish(nanFinish <= window)), 'x', 'Color', cmap(curr, :), 'MarkerSize', 10, 'LineWidth', 4);
    %     end
    %    axis([0 xyloObj.Width, 0 xyloObj.Height] * options.scale);
    %    set(gca, 'YDir', 'rev', 'Color', [0 0 0]);

    
%     for curr = 1:options.nSubjects
%         frame = r+startframe-1;
%         wframe = max(frame-options.window, 1);
%         for point=wframe:frame
%             x = social.x(curr, point);
%             y = social.y(curr, point);
%             for i=floor(max(y-options.radios,1)):ceil(min(y+options.radios, size(m,1)))
%                 for j=floor(max(x-options.radios,2)):ceil(min(x+options.radios, size(m,2)))
%                     dist = sqrt((x - j).^2 + (y-i).^2);
%                     if dist < options.radios
%                             m(i, j, :) = cmap(curr, :) * 255;
% %                        m(i, j, :) = social.colors(curr, :) * 255;
%                     end
%                 end
%             end
% %             if point == frame
% %                 m(round(y), round(x), :) = 255;
% %             end
% %             if point > wframe
% %                 slope = (y - prev.y) / (x - prev.x);
% %                 for cx=min(x, prev.x):max(x, prev.x)
% %                     m(round(y + slope * (x - cx)), round(cx), :) = social.colors(curr, :) * 255;
% %                 end
% %             end
% %             prev.x = x;
% %             prev.y = y;
%         end
%     end
    %imagesc(m)
    %drawnow
    subplot(3,4,[2:4,6:8,10:12]);
    imagesc(m)
    title(sprintf('frame %d (%d/%d)',  r+startframe-1, r, nframes),'fontsize', 17);
    hold on;
    for curr = 1:options.nSubjects
        frame = r+startframe-1;
        wframe = max(frame-options.window, 1);
        point = frame;
        %for point=wframe:frame
            x = social.x(curr, point);
            y = social.y(curr, point);
            text('Position',[x y], 'String', num2str(curr), 'FontSize', 10, 'Color', social.meta.subject.centerColors(curr, :), 'FontWeight', 'Bold');
        %end
    end
    hold off;
    drawnow
    
    %hold off;
    %%
    %F = getframe(gcf);
    %     saveas(gcf, ['tmp/' unique_id '.jpg']);
    if options.outputToFile
        F.cdata = m;
        F.colormap = [];
        aviobj = addframe(aviobj,F);
        if ~parallel && mod(r, step) == 0 && r < nframes
            id = id+1;
            aviobj = close(aviobj);
            options.outputMovieFile = sprintf([options.output_path options.test.name '.social.%03d.avi'], id);
            aviobj = avifile(options.outputMovieFile, 'fps', xyloObj.FrameRate);
        end
    end
end
fprintf('\n');
if options.outputToFile
    fprintf(['# - writing to video file: ' options.outputMovieFile '\n']);
    aviobj = close(aviobj);
end
%%
