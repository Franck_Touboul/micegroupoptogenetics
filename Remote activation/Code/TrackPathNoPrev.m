function obj = TrackPathNoPrev(obj, nruns)
%%
if ~exist('nruns', 'var')
    nruns = 100;
end
obj = TrackLoad(obj);
%%
fprintf('# finding paths\n');
%%
% obj.ColorMatchThresh = 0.1;
% obj.MaxHiddenDuration = 25;
% obj.NumOfProbBins = 20;
% obj.MinJumpProb = 0.01;
% obj.AllowHiddenTransitions = false;
% obj.MaxIsolatedDistance = 12;
%%
fprintf('# - loading segmentations\n');
cents = struct();
cents.x       = [];
for i=1:nruns
    fprintf('#      . segment no. %d\n', i);
    filename = [obj.OutputPath obj.FilePrefix '.segm.' sprintf('%03d', i) '.mat'];
    waitforfile(filename);
    currSegm = load(filename);
    cents = structcat(cents, currSegm.cents, 1, true, false);
end
%%
if isfield(obj, 'ROI.Ignore')
    ignore = uint8(imread(obj.ROI.Ignore));
    fprintf('#  . removing unwanted cents...');
    for i=1:size(cents.label, 1)
        labels = cents.label(i, :);
        y = cents.y(i,:);
        x = cents.x(i,:);
        y = y(labels~=0) / obj.VideoScale;
        if isempty(y)
            continue;
        end
        x = x(labels~=0) / obj.VideoScale;
        labels(labels~=0) = (1 - ignore( sub2ind(size(ignore), round(y), round(x)) )) .* labels(labels~=0);
        cents.label(i, :) = labels;
    end
    fprintf('[done]\n');
end
%%
StartFrame = obj.StartTime / obj.dt;
EndFrame = obj.EndTime / obj.dt;
if EndFrame < 0
   EndFrame = obj.nFrames + 1;
end
%%
prev = cents.label;
[q, cents.label] = max(cents.logprob, [], 3);
cents.label(prev == 0) = 0;

thresh = obj.ColorMatchThresh;
ndata = size(cents.x, 1);

for i=1:obj.nSubjects
    logprobs = cents.logprob(:, :, i);
    logprobs(cents.label ~= i) = -inf;
    [maxlogprobs, centids] = max(logprobs, [], 2);

    maxprobs = exp(maxlogprobs);
    res{i}.x = cents.x(sub2ind(size(cents.x), 1:ndata, centids'));
    res{i}.y = cents.y(sub2ind(size(cents.y), 1:ndata, centids'));
    res{i}.prob = maxprobs';
    res{i}.logprob = maxlogprobs';
    res{i}.centids = centids';
    
    res{i}.valid = maxprobs' > thresh;
    %% remove isolated apperances
    d = diff([0 res{i}.valid 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    len = finish - start + 1;
    isolated = start - [1 finish(1:end-1)] > obj.MaxIsolatedDistance & ...
    [start(2:end) size(res{i}.valid, 1)] - finish > obj.MaxIsolatedDistance;
    idx = find(len == 1 & isolated);
    for j=idx
        res{i}.valid(start(j):finish(j)) = false;
    end
    %%
    res{i}.valid(1:StartFrame) = false;
    res{i}.valid(EndFrame:end) = false;
    %%
    res{i}.id = (maxprobs' > thresh) * i;
    res{i}.x(~res{i}.valid) = nan;
    res{i}.y(~res{i}.valid) = nan;
    res{i}.prob(~res{i}.valid) = nan;
    res{i}.logprob(~res{i}.valid) = -inf;
    res{i}.centids(~res{i}.valid) = nan;
    %%
end
%%
distances = [];
idistances = [];
for i=1:obj.nSubjects
    for j=1:obj.nSubjects
        if j~=i
            distances = [distances, sqrt((res{i}.x - res{j}.x).^2 +(res{i}.y - res{j}.y).^2)];
        else
            idistances = [idistances, sqrt((res{i}.x(2:end) - res{j}.x(1:end-1)).^2 +(res{i}.y(2:end) - res{j}.y(1:end-1)).^2)];
        end
    end
end

for i=1:obj.nSubjects
    res{i}.probBins = sequence(0, max([distances idistances]) + 0.1, obj.NumOfProbBins+1); 
    res{i}.probBinDiff = res{i}.probBins(2) - res{i}.probBins(1);
    
    h1 = histc(distances, res{i}.probBins) + 1;
    h1=h1/sum(h1); 
    
    h2=histc(idistances, res{i}.probBins);
    h2=h2/sum(h2); 
    
    res{i}.jumpProb = h2./(h1+h2);
    res{i}.jumpProb(res{i}.jumpProb < obj.MinJumpProb) = 0;
    res{i}.jumpLogprob = log(res{i}.jumpProb);
end

ores = res;
%%
for curr = 1:obj.nSubjects
    fprintf('# - tracking subject %d of %d\n', curr, obj.nSubjects);
    currLogprobs = cents.logprob(:, :, curr);
    %% build observation probability table
    table = ones(obj.nSubjects+obj.ROI.nHidden+1, length(res{curr}.id)) * -inf;
    scoord.x = zeros(obj.nSubjects, length(res{curr}.id));
    scoord.y = zeros(obj.nSubjects, length(res{curr}.id));
    scoord.hidingNeighbour.x = scoord.x;
    scoord.hidingNeighbour.y = scoord.y;
    scoord.hidingDistance = zeros(obj.nSubjects, length(res{curr}.id), obj.ROI.nHidden);
    path = zeros(size(table), 'uint16');
    nchar = 0;
    fprintf('#   . computing emission probabilities\n');
    for s=1:obj.nSubjects
        range = 1:ndata;
        
        range = range(res{s}.valid);
        centids = res{s}.centids(res{s}.valid);
        
        table(s, range) = currLogprobs(sub2ind(size(currLogprobs), range, centids));
        scoord.x(s, range) = res{s}.x(res{s}.valid);
        scoord.y(s, range) = res{s}.y(res{s}.valid);
        for h=1:obj.ROI.nHidden
            nchar = Reprintf(nchar, '#       processing segment %d / %d', ((s - 1) * obj.ROI.nHidden) + h, obj.nSubjects*obj.ROI.nHidden);
            hcoord = obj.ROI.HiddenBoundarySCoordinates{h};
            d = pdist2([scoord.x(s, range)', scoord.y(s, range)'], hcoord);
            [hvalues, hidx] = min(d, [], 2);
            scoord.hidingNeighbour.x(s, range) = hcoord(hidx, 1);
            scoord.hidingNeighbour.y(s, range) = hcoord(hidx, 2);
            scoord.hidingDistance(s, range, h) = hvalues';
        end
    end
    fprintf('\n');
    
    for s=1:obj.ROI.nHidden+1
        table(obj.nSubjects+s, :) = flog(prod(1 - exp(table(1:obj.nSubjects, :)), 1));
    end
    emitlogprobs = table;
    
    valid = sum(isfinite(table(1:obj.nSubjects, :)), 1) > 0;
    d = diff([0 valid 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    
    %%
    prev = 1;
    nchar = 0;
    
    scoord.hidingPos.x = ones(1, length(res{curr}.id)) * inf;
    scoord.hidingPos.y = ones(1, length(res{curr}.id)) * inf;
    scoord.hidingTime = zeros(1, length(res{curr}.id));
    
    zeroprob = flog(1);
    zeroprobvec = [repmat(zeroprob, obj.nSubjects, 1); zeros(obj.ROI.nHidden+1, 1)];
    
    hiddenprobvec = [repmat(zeroprob, obj.nSubjects, 1); zeros(obj.ROI.nHidden, 1); zeroprob];
    nohiddenprobvec = [repmat(log(0), obj.nSubjects, 1); zeros(obj.ROI.nHidden, 1); flog(0)];
    
    fprintf('#   . building viterbi table\n');
    for n=1:length(start)
        nchar = Reprintf(nchar, '#       processing segment %d / %d', n, length(start));
        r = start(n):finish(n);
        for f=r
            %
            for s=1:obj.nSubjects
                if ~isfinite(table(s, f))
                    continue;
                end
                x = res{s}.x(f);
                y = res{s}.y(f);
                
                distance = scoord.hidingDistance(s, f, :);
                distance = [...
                    sqrt((x - scoord.x(:, prev)).^2 + (y - scoord.y(:, prev)).^2); ...
                    distance(:);...
                    sqrt((x - scoord.hidingPos.x(prev)).^2 + (y - scoord.hidingPos.y(prev)).^2) / (f - scoord.hidingTime(prev))];
                idx = floor(distance / res{curr}.probBinDiff) + 1;
                idx(idx < 1) = 1;
                idx(idx > obj.NumOfProbBins) = obj.NumOfProbBins;
                trans = res{curr}.jumpLogprob(idx)';
                
                if f>prev+1
                    trans = trans + zeroprobvec * (f - prev - 1);
                    if f - prev - 1 > obj.MaxHiddenDuration
                        trans(obj.nSubjects + obj.ROI.nHidden + 1) = flog(0);
                        trans(1:obj.nSubjects) = flog(0);
                    end
                end
                
                [val, from] = max(table(:, prev) + trans);
                table(s, f) = table(s, f) + val;
                path(s, f) = from;
            end
            % hidden zones
            for h=1:obj.ROI.nHidden
                if obj.AllowHiddenTransitions
                    distance = [scoord.hidingDistance(:, prev, h); obj.ROI.HiddenSDistances(:, h); inf];
                else
                    distance = [scoord.hidingDistance(:, prev, h); ones(obj.ROI.nHidden,1) * inf; inf];
                    distance(obj.nSubjects + h) = 0;
                end
                idx = floor(distance / res{curr}.probBinDiff) + 1;
                idx(idx < 1) = 1;
                idx(idx > obj.NumOfProbBins) = obj.NumOfProbBins;
                trans = res{curr}.jumpLogprob(idx)';
                
                [val, from] = max(table(:, prev) + trans);
                table(obj.nSubjects + h, f) = table(obj.nSubjects + h, f) + val;
                path(obj.nSubjects + h, f) = from;
            end
            %
            
            if f - scoord.hidingTime(prev) <= obj.MaxHiddenDuration
                [val, from] = max(table(:, prev) + hiddenprobvec);
            else
                [val, from] = max(table(:, prev) + nohiddenprobvec);
            end
            
            table(obj.nSubjects + obj.ROI.nHidden + 1, f) = table(obj.nSubjects + obj.ROI.nHidden + 1, f) + val;
            path(obj.nSubjects + obj.ROI.nHidden + 1, f) = from;
            if from <= obj.nSubjects
                scoord.hidingPos.x(f) = scoord.x(from, prev);
                scoord.hidingPos.y(f) = scoord.y(from, prev);
                scoord.hidingTime(f) = f;
            else
                scoord.hidingPos.x(f) = scoord.hidingPos.x(prev);
                scoord.hidingPos.y(f) = scoord.hidingPos.y(prev);
                scoord.hidingTime(f) = scoord.hidingTime(prev);
            end
            %
            prev = f;
        end
    end
    fprintf('\n');
    %% backtrack
    fprintf('#   . backtracking\n');
    track{curr}.src = zeros(1, size(table, 2));
    track{curr}.logprob = zeros(1, size(table, 2));
    track{curr}.emitlogprob = zeros(1, size(table, 2));
    track{curr}.x = zeros(1, size(table, 2));
    track{curr}.y = zeros(1, size(table, 2));
    
    for n=length(start):-1:1
        r = start(n):finish(n);
        f=finish(n);
        if n<length(start)
            idx = path(track{curr}.src(start(n+1)), start(n+1));
            
            track{curr}.src(finish(n)) = idx;
            track{curr}.logprob(f) = table(idx, f);
            track{curr}.emitlogprob(f) = emitlogprobs(idx, f);
        else
            [track{curr}.logprob(finish(n)), track{curr}.src(finish(n))] = max(table(:, finish(n)));
            track{curr}.emitlogprob(finish(n)) = emitlogprobs(track{curr}.src(finish(n)), finish(n));
        end
        for f=finish(n)-1:-1:start(n)
            idx = path(track{curr}.src(f+1), f+1);
            
            track{curr}.src(f) = idx;
            track{curr}.logprob(f) = table(idx, f);
            track{curr}.emitlogprob(f) = emitlogprobs(idx, f);
        end
        if n>1
            track{curr}.logprob(finish(n-1)+1:start(n)-1) = track{curr}.logprob(start(n));
            track{curr}.emitlogprob(finish(n-1)+1:start(n)-1) = track{curr}.emitlogprob(start(n));
        end
    end
    
    fprintf('#   . computing path\n');
    track{curr}.valid = track{curr}.src <= obj.nSubjects & track{curr}.src > 0;
    track{curr}.id = track{curr}.valid * curr;
    
    range = 1:length(track{curr}.valid);
    range = range(track{curr}.valid);
    
    src = track{curr}.src(track{curr}.valid);
    
    track{curr}.x = zeros(1, length(track{curr}.valid));
    track{curr}.x(track{curr}.valid) = scoord.x(sub2ind(size(scoord.x), src, range));
    track{curr}.x(~track{curr}.valid) = nan;
    
    track{curr}.y = zeros(1, length(track{curr}.valid));
    track{curr}.y(track{curr}.valid) = scoord.y(sub2ind(size(scoord.y), src, range));
    track{curr}.y(~track{curr}.valid) = nan;
    
    %if curr == 1; break;end
end
origTrack = track;
if obj.OutputInOldFormat
    filename = [obj.OutputPath obj.FilePrefix '.raw-track.mat'];
    save(filename, 'track');
end

%%
fprintf('# - setting additional properties & fixes\n');
for i=1:obj.nSubjects;
    % ignore invalid frames
    nValidFrames = length(track{i}.src);
    track{i}.src(nValidFrames+1:obj.nFrames) = nan;
    track{i}.id(nValidFrames+1:obj.nFrames) = nan;
    track{i}.x(nValidFrames+1:obj.nFrames) = nan;
    track{i}.y(nValidFrames+1:obj.nFrames) = nan;
    
    track{i}.hidden = track{i}.src > obj.nSubjects;
    
    % remove skipped areas
    d = diff([0 track{i}.src == 0 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    for j=1:length(start)
        if start(j) <= 1
            continue;
        end
        %track{i}.src(start(j)-1)
        if track{i}.src(start(j)-1) > obj.nSubjects
            track{i}.src(start(j):finish(j)) = track{i}.src(start(j)-1);
        else
            track{i}.src(start(j):finish(j)) = obj.nSubjects + obj.ROI.nHidden + 1;
        end
    end    

    % set zones coordinates
    valid = track{i}.src > obj.nSubjects & track{i}.src <= obj.nSubjects + obj.ROI.nHidden;
    track{i}.x(valid) = obj.ROI.HiddenSCenters(track{i}.src(valid) - obj.nSubjects, 1);
    track{i}.y(valid) = obj.ROI.HiddenSCenters(track{i}.src(valid) - obj.nSubjects, 2);
    
    % interpolate hidden epochs
    d = diff([0 track{i}.src == 0 | track{i}.src == obj.nSubjects + obj.ROI.nHidden + 1  0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    for j=1:length(start)
        if start(j) <= 1 || finish(j) >= nValidFrames || isnan(track{i}.x(start(j) - 1)) || isnan(track{i}.x(finish(j) + 1))
            continue;
        end
        r = start(j):finish(j);
        [track{i}.x(r), track{i}.y(r)] = MotionInterp(track{i}.x, track{i}.y, r, [obj.VideoWidth * obj.VideoScale, obj.VideoHeight * obj.VideoScale]);
    end
    
    % setting regions
    x = min(max(round(track{i}.x), 1), obj.VideoWidth) / obj.VideoScale;
    y = min(max(round(track{i}.y), 1), obj.VideoHeight) / obj.VideoScale;
    track{i}.regions = zeros(1, obj.nFrames);
    for r=1:obj.ROI.nRegions
        track{i}.regions(obj.ROI.Regions{r}(sub2ind(size(obj.ROI.Regions{r}), y, x))) = r;
    end
    track{i}.sheltered = track{i}.src > obj.nSubjects & track{i}.src <= obj.nSubjects + obj.ROI.nHidden;
    
    % setting zones
    track{i}.zones = ones(1, obj.nFrames);
    obj.ROI.ZoneNames{1} = 'Open';
    index = 2;
    for r=1:obj.ROI.nRegions
        idx = track{i}.regions == r & ~track{i}.sheltered;
        track{i}.zones(idx) = index;
        if i==1; obj.ROI.ZoneNames{index} = obj.ROI.RegionNames{r}; end
        if obj.ROI.IsSheltered(r)
            % set long invisible periods as sheltered
            invisibleInRegion = track{i}.regions == r & track{i}.hidden & ~track{i}.sheltered;
            d = diff([0 invisibleInRegion  0], 1, 2);
            start  = find(d > 0);
            finish = find(d < 0) - 1;
            len = finish - start + 1;
            for q = find(len > obj.MaxHiddenDuration)
                track{i}.sheltered(start(q):finish(q)) = true;
            end
            %
            idx = track{i}.regions == r & track{i}.sheltered;
            track{i}.zones(idx) = index + 1;
            if i==1; obj.ROI.ZoneNames{index + 1} = ['(' obj.ROI.RegionNames{r} ')']; end
            index = index + 2;
        else
            index = index + 1;
        end
    end
    
    % ignore invalid frames
    track{i}.id(isnan(track{i}.x)) = nan;
    track{i}.zones(isnan(track{i}.x)) = nan;
    track{i}.regions(isnan(track{i}.x)) = nan;
    track{i}.hidden(isnan(track{i}.x)) = true;
    track{i}.sheltered(isnan(track{i}.x)) = false;
end
%%
fprintf('# - finding paths\n');
prevtrack = track;
social.options = obj;
social.dt = obj.dt;
social.x = zeros(length(track), length(track{1}.x), 'single');
social.y = zeros(length(track), length(track{1}.x), 'single');
for i=1:length(track)
    social.x(i, :) = track{i}.x / obj.VideoScale;
    social.y(i, :) = track{i}.y / obj.VideoScale;
end
%
fprintf('# - organizing results\n');
social.nData = size(social.x, 2);
social.time = (1:social.nData) * obj.dt;
zones.all = zeros(obj.nSubjects, obj.nFrames);
zones.hidden = zeros(obj.nSubjects, obj.nFrames);
zones.sheltered = zeros(obj.nSubjects, obj.nFrames);
zones.regions = zeros(obj.nSubjects, obj.nFrames);
%
for i=1:obj.nSubjects
    x = round(social.x(i, :)); x(x == 0) = 1;
    y = round(social.y(i, :)); y(y == 0) = 1;
    %
    startnan = find(isnan(x), 1);
    if startnan > 1
        x(startnan:end) = x(startnan - 1);
        y(startnan:end) = y(startnan - 1);
    else
        x(startnan:end) = 1;
        y(startnan:end) = 1;
    end
    %
    zones.hidden(i, :) = track{i}.id == 0;
    zones.all(i, :) = track{i}.zones;
    zones.sheltered(i, :) = track{i}.sheltered;
    zones.regions(i, :) = track{i}.regions;
end
social.zones = zones;
social.colors = obj.Colors.Centers;
social.nSubjects = obj.nSubjects;
social.zones.labels = obj.ROI.ZoneNames;

%%
fprintf('# - computing kalman filter\n');
% if isfield(obj, 'Kalman')
%     social = kalmanFilterTrack(social, options.kalman.q, options.kalman.r);
% end
%%
if obj.OutputInOldFormat
    fprintf('# - saving data in old format\n');
    filename = [obj.OutputPath obj.FilePrefix '.social.mat'];
    save(filename, 'social', '-v7.3');
end

%%
obj.x = social.x;
obj.y = social.y;
obj.time = social.time;
obj.zones = social.zones.all;
obj.regions = social.zones.regions;
obj.hidden = social.zones.hidden;
obj.sheltered = social.zones.sheltered;
obj.valid = ~isnan(sum(obj.x, 1)); 

if obj.OutputToFile
    fprintf('# - saving data\n');
    TrackSave(obj);

    filename = [obj.OutputPath obj.FilePrefix '.track.mat'];
    save(filename, 'obj');

    filename = [regexprep(obj.VideoFile, '\.[^\.]*$', '') '.mat'];
    saveSocialForTracking(filename, social);
end
