function H = JointEntropy(data)

jprobs = JointProbability(data);
H = -sum(jprobs .* flog2(jprobs));
