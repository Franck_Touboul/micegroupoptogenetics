function cmap = prettyBlue
for i=1:256
    cmap(i, 1:3) = (i - 1) / 255 * [48, 165, 256];
end
cmap = cmap / 256;
cmap = colormap(cmap);
