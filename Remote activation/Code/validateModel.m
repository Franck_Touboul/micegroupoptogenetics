function model = validateModel(model)

model.nstates = length(model.states);
model.trans = model.trans ./ repmat(sum(model.trans, 2), 1, size(model.trans, 2));

if ~isfield(model, 'start')
    model.start = zeros(model.nstates, 1);
    model.start(1) = 1;
end