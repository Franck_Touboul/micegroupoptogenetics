function r = MyRandi(n, x, y)
if nargin == 1
    x = 1;
    y = 1;
elseif nargin == 2
    y = x;
end
if length(n) == 1    
    r = ceil(rand(x, y) * n);
else
%     p = cumsum(n(:)) / sum(n)
%     l = length(n);
%     c = repmat(rand(1, x * y), l, 1) < repmat(p, 1, x * y);
%     r = l - sum(c, 1) + 1;
%     r = n(r);
    %r = reshape(r, x, y);
    r = ceil(rand(x, y) * length(n(:)));
    r = n(r);
end
