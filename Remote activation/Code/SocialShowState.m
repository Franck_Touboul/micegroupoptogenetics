function SocialShowState(obj, label)
issheltered = false(1, obj.ROI.nZones);
for i=1:obj.ROI.nZones
    name = regexprep(obj.ROI.ZoneNames{i}, '[()]', '');
    p = strcmp(name, obj.ROI.RegionNames);
    if ~any(p)
        m = true(size(obj.ROI.Regions{1}));
        for j=1:obj.ROI.nRegions
            m = m & ~obj.ROI.Regions{j};
        end
    else
        m = obj.ROI.Regions{p};
    end
    
    [x,y] = find(m);
    obj.ROI.ZoneCenters(i, :) = [mean(y), mean(x)];
    issheltered(i) = ~strcmp(name, obj.ROI.ZoneNames{i});
end
obj.ROI.ZoneCenters(1, :) = [300, 300];
obj.ROI.ZoneCenters(5, :) = obj.ROI.ZoneCenters(5, :) + [40,  -50];
obj.ROI.ZoneCenters(8, :) = obj.ROI.ZoneCenters(8, :) + [40,  50];
%
% imagesc(obj.BkgImage); hold on;
% for i=1:obj.ROI.nZones
%     plot(obj.ROI.ZoneCenters(i, 1), obj.ROI.ZoneCenters(i, 2), 'x');
%     text(obj.ROI.ZoneCenters(i, 1), obj.ROI.ZoneCenters(i, 2),num2str(i), 'color', 'w')
% end
% hoff;
%%
range = 3;
b=im2double(rgb2gray(obj.BkgImage)); mn = mean(b(:)) - range * std(b(:)); mx = mean(b(:)) + range * std(b(:)); 
b = min(max((b-mn) / (mx - mn), 0), 1);

cmap = MyMouseColormap;
%plot(obj.ROI.ZoneCenters(l(2, :), 1), obj.ROI.ZoneCenters(l(2, :), 2), 'w:');
%imagesc(obj.BkgImage);
imshow(b);
hon;
for j=1:size(label, 2)
     x = obj.ROI.ZoneCenters(label(2, j), 1) + 10 * (2 * mod(label(1, j) - 1, 2) - 1);
     y = obj.ROI.ZoneCenters(label(2, j), 2) + 10 * (2 * floor((label(1, j) - 1)/2) - 1);
%    x = obj.ROI.ZoneCenters(label(2, j), 1);
    %y = obj.ROI.ZoneCenters(label(2, j), 2);
    if issheltered(label(2, j))
        plot(x, y, 's', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', cmap(label(1, j), :), 'MarkerSize', 5);
    else
        plot(x, y, 'o', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', cmap(label(1, j), :), 'MarkerSize', 5);
    end
end
hoff;
axis off;