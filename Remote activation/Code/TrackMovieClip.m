function TrackMovieClip(startframe, endframe)
%
TrackDefaults;
options.movieChunkDuration = 25 * 5;
options.window = 10;
options.radios = 4;
%%
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nframes = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;
dt = 1/xyloObj.FrameRate;

nframes = endframe - startframe + 1;

%%
filename = [options.output_path options.test.name '.social.mat'];
load(filename);

%%
%options.outputMovieFile = '';

if exist('aviobj') && strcmp(aviobj.CurrentState, 'Open')
    aviobj = close(aviobj);
end
options.outputMovieFile = sprintf([options.output_path options.test.name '.clip.%06d-%06d.avi'], startframe, endframe);
aviobj = avifile(options.outputMovieFile, 'fps', xyloObj.FrameRate);

%%
fprintf('# comparing tracks\n');
nchars = 0;
nchars = RePrintf('# - frame %6d [%d-%d] (%6.2fxiRT)', startframe, startframe, endframe, 0);
cmap = lines;
cmap  = [0 1 1; 1 1 1; 1 0 1; 1 0 0];
cmap2 = [0 1 1; 1 1 1; 1 0 0; 1 0 1];
%cmap2 = data.meta.subject.color;
%cmap = subject.color;
window = 10;
bkg = zeros(xyloObj.Width, xyloObj.Height, 3);
tic
clf
for r=1:nframes
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d [%d-%d] (%6.2fxiRT)', r+startframe-1, startframe, endframe, RT);
    m = myMMReader(options.MovieFile, r+startframe-1);
    %     imagesc(m); hold on;
    %     for curr = 1:options.nSubjects;
    %         r1 = max(r-window, 1);
    %         currx = social.x(curr, r1:r);
    %         curry = social.y(curr, r1:r);
    %         plot(currx, curry, '.-', 'Color', cmap(curr, :));
    %         hold on;
    %         %nanStart  = find(isnan(currx)) - 1;
    %         %nanFinish = find(isnan(currx)) + 1;
    %         %plot(currx(nanStart(nanStart > 0)), curry(nanStart(nanStart > 0)), 'o', 'Color', cmap(curr, :), 'MarkerSize', 10);
    %         %plot(currx(nanFinish(nanFinish <= window)), curry(nanFinish(nanFinish <= window)), 'x', 'Color', cmap(curr, :), 'MarkerSize', 10, 'LineWidth', 4);
    %     end
    %    axis([0 xyloObj.Width, 0 xyloObj.Height] * options.scale);
    %    set(gca, 'YDir', 'rev', 'Color', [0 0 0]);
    title(sprintf('frame %d/%d', r, nframes),'fontsize', 17);
    for curr = 1:options.nSubjects
        frame = r+startframe-1;
        wframe = max(frame-options.window, 1);
        for point=wframe:frame
            x = social.x(curr, point);
            y = social.y(curr, point);
            for i=floor(max(y-options.radios,1)):ceil(min(y+options.radios, size(m,1)))
                for j=floor(max(x-options.radios,2)):ceil(min(x+options.radios, size(m,2)))
                    dist = sqrt((x - j).^2 + (y-i).^2);
                    if dist < options.radios
                        m(i, j, :) = social.colors(curr, :) * 255;
                    end
                end
            end
%             if point > wframe
%                 slope = (y - prev.y) / (x - prev.x);
%                 for cx=min(x, prev.x):max(x, prev.x)
%                     m(round(y + slope * (x - cx)), round(cx), :) = social.colors(curr, :) * 255;
%                 end
%             end
%             prev.x = x;
%             prev.y = y;
        end
    end
    %imagesc(m)
    %drawnow
    
    %hold off;
    %%
    %F = getframe(gcf);
    %     saveas(gcf, ['tmp/' unique_id '.jpg']);
    F.cdata = m;
    F.colormap = [];
    aviobj = addframe(aviobj,F);
end
fprintf('\n');
fprintf(['# - writing to video file: ' options.outputMovieFile '\n']);
aviobj = close(aviobj);
%%
