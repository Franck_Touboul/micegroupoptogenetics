function [obj, img] = TrackColorSegmentFrame(obj, frame)
%%
if ischar(obj)
    load(obj);
end

[obj, img] = TrackSimpleSegmentFrame(obj, frame);
if isempty(img)
    return;
end

%% compute probabilities based on the color histograms
hm = img.hsv(:,:,1);
sm = img.hsv(:,:,2);
vm = img.hsv(:,:,3);

[b, idx_h] = histc(hm(img.boundries), obj.Colors.Bins);
[b, idx_s] = histc(sm(img.boundries), obj.Colors.Bins);
[b, idx_v] = histc(vm(img.boundries), obj.Colors.Bins);

prob_h = zeros(obj.nSubjects, length(idx_h));
prob_s = zeros(obj.nSubjects, length(idx_s));
prob_v = zeros(obj.nSubjects, length(idx_v));
for i=1:obj.nSubjects
    prob_h(i, :) = obj.Colors.Histrogram.H(i, idx_h);
    prob_s(i, :) = obj.Colors.Histrogram.S(i, idx_s);
    prob_v(i, :) = obj.Colors.Histrogram.V(i, idx_v);
end
joint_prob = prob_h .* prob_s .* prob_v;
joint_prob = joint_prob ./ repmat(sum(joint_prob, 1), obj.nSubjects, 1);

[m, idx] = max(joint_prob, [], 1);
nlabels = zeros(size(img.boundries, 1),size(img.boundries,2),'uint8');
nlabels(img.boundries) = idx;

logprobmap = cell(1, obj.nSubjects);
for k=1:obj.nSubjects
    logprobmap{k} = zeros(size(img.boundries));
    logprobmap{k}(img.boundries) = flog(joint_prob(k, :));
end
if obj.Output || nargout > 1
    flabels = zeros(size(img.boundries, 1),size(img.boundries,2),'uint8');
end
%% filter small regions
rejected = false(size(img.boundries));
conn = cell(1, obj.nSubjects);
validmap = cell(1, obj.nSubjects);
for i=1:obj.nSubjects
    reg = bwconncomp(nlabels == i);
    conn{i} = regionprops(reg, 'Solidity', 'Centroid', 'PixelIdxList');
    validmap{i} = false(size(img.boundries));
    for j=1:length(conn{i})
        if conn{i}(j).Solidity < obj.MinSolidity || ...
                length(reg.PixelIdxList{j}) < obj.MinNumOfPixels
            rejected(reg.PixelIdxList{j}) = true;
            conn{i}(j).PixelIdxList = [];
        else
            validmap{i}(conn{i}(j).PixelIdxList) = true;
        end
    end
end
%% reassign rejected regions to clusters
lrejected = bwlabel(rejected);
dlrejected = imdilate(lrejected, ones(3,3));
for i=1:obj.nSubjects
    u_ = unique(dlrejected(validmap{i}))';
    if ~isempty(u_)
        for u=u_
            if u > 0
                validmap{i}(lrejected == u) = true;
                rejected(lrejected == u) = false;
            end
        end
    end
end

%% compute the new region props
for i=1:obj.nSubjects
    currmap = imerode(validmap{i}, strel('disk', obj.ErodeRadios));
    reg = bwconncomp(currmap);
    conn{i} = regionprops(reg, 'Solidity', 'Centroid', 'PixelIdxList');
end
currmap = imerode(rejected, strel('disk', obj.ErodeRadios));
reg = bwconncomp(currmap);
conn{obj.nSubjects+1} = regionprops(reg, 'Solidity', 'Centroid', 'PixelIdxList');

%% save centers
centIndex = 1;
cents = struct(...
    'x', zeros(1, obj.MaxNumOfCents),...
    'y', zeros(1, obj.MaxNumOfCents), ...
    'label', zeros(1, obj.MaxNumOfCents, 'uint8'), ...
    'area', zeros(1, obj.MaxNumOfCents, 'uint16'), ...
    'solidity', zeros(1, obj.MaxNumOfCents, 'single'), ...
    'logprob', zeros(obj.MaxNumOfCents, obj.nSubjects));
for i=1:obj.nSubjects+1
    for j=1:length(conn{i})
        if length(conn{i}(j).PixelIdxList) > obj.MinNumOfPixels
            cents.x(centIndex) = conn{i}(j).Centroid(1);
            cents.y(centIndex) = conn{i}(j).Centroid(2);
            cents.label(centIndex) = i;
            cents.area(centIndex) = length(conn{i}(j).PixelIdxList);
            cents.solidity(centIndex) = conn{i}(j).Solidity;
            for k=1:options.nSubjects
                cents.logprob(centIndex, k) = sum(logprobmap{k}(conn{i}(j).PixelIdxList));
            end
            cents.logprob(centIndex, :) = cents.logprob(centIndex, :) - flog(sum(exp(cents.logprob(centIndex, :))));
            centIndex = centIndex + 1;
            if centIndex > options.maxNumCents
                break;
            end
        end
    end
    if centIndex > obj.MaxNumOfCents
        cents.label(:) = 0;
    end
    
    if obj.Output || nargout > 1
        if i <= obj.nSubjects
            reg = bwconncomp(validmap{i});
        else
            reg = bwconncomp(rejected);
        end
        flabels(labelmatrix(reg) > 0) = i;
    end
end
if obj.Output || nargout > 1
    img.segmented = label2rgb(flabels, obj.Colors.Centers);
end
if obj.Output
    subplot(1,2,2);
    imagesc(img.segmented);
    subplot(1,2,1);
    imagesc(img.orig);
    drawnow
end
