AllGroups = {...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam01',...
    };

EnrichedGroups = {...
    'Enriched.exp0001.day%02d.cam04',...
    'Enriched.exp0002.day%02d.cam04',...
    'Enriched.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    };

%objs = {};
%%
a = EnrichedGroups;
all = {};
parfor i=1:length(a)
    curr = {};
    for day = 1:4;
        prefix = sprintf(a{i}, day);
        fprintf('# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions', 'Common'});
            %obj = SocialPredPreyModel(obj);
            obj = SocialAnalysePredPreyModel(obj);
            curr{day} = obj.Interactions.PredPrey;
            %saveFigure(['Graphs/' prefix '.SocialAnalysePredPreyModel']);
        catch me
           fprintf(['#   . FAILED! ' me.message '\n']); 
           for j=1:length(me.stack)
               fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
           end
        end
    end
    all{i} = curr;
end

%%
PostPred = [];
PostPrey = [];
for i=1:length(a)
    for day = 1:4;
        p = all{i}{day}.PostPred;
        p(1:first.nSubjects+1:end) = nan;
        PostPred = cat(3, PostPred, p);
        p = all{i}{day}.PostPrey;
        p(1:first.nSubjects+1:end) = nan;
        PostPrey = cat(3, PostPrey, p);
    end
end

%%
a = AllGroups;
for i=1 %:length(a)
    all = {};
    PostPred = zeros(4);
    PostPrey = zeros(4);
    objs = {};
    parfor day = 1:4;
        prefix = sprintf(a{i}, day);
        fprintf('# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions', 'Common'});
            if day==1
                objs{day} = obj;
            end
            %obj = SocialPredPreyModel(obj);
            obj = SocialAnalysePredPreyModel(obj);
            all{day} = obj.Interactions.PredPrey;
            PostPred = PostPred + obj.Interactions.PredPrey.PostPred;
            PostPrey = PostPrey + obj.Interactions.PredPrey.PostPrey;
            %saveFigure(['Graphs/' prefix '.SocialAnalysePredPreyModel']);
            objs{day} = obj;
        catch me
           fprintf(['#   . FAILED! ' me.message '\n']); 
           for j=1:length(me.stack)
               fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
           end
        end
    end
    %%
    first = objs{1};
    matPred = PostPred - PostPred';
    matPrey = PostPrey - PostPrey';
    mat = matPred - matPrey;
    mat(binotest(PostPred, PostPred + PostPred') & binotest(PostPrey, PostPrey + PostPrey')) = 0;
    mat = mat .* (mat > 0);    
    [rank, removed] = TopoFeedbackArcSetOrder(mat);
    
    nDays = 4;
    rank = zeros(nDays, first.nSubjects);
    for day = 1:nDays
        matPred = all{day}.PostPred - all{day}.PostPred';
        matPrey = (all{day}.PostPrey - all{day}.PostPrey') * 0;
        mat = matPred - matPrey;
        mat(binotest(all{day}.PostPred, all{day}.PostPred + all{day}.PostPred') & ...
            binotest(all{day}.PostPrey, all{day}.PostPrey + all{day}.PostPrey')) = 0;
        mat = mat .* (mat > 0);
        [rank(day, :), removed] = TopoFeedbackArcSetOrder(mat);
    end
    rank = sum(rank) ./ sum(rank > 0);
    
    parfor day = 1:nDays
        prefix = sprintf(a{i}, day);

        matPred = all{day}.PostPred - all{day}.PostPred';
        matPrey = (all{day}.PostPrey - all{day}.PostPrey') * 0;
        mat = matPred;
%         mat(binotest(all{day}.PostPred, all{day}.PostPred + all{day}.PostPred') & ...
%             binotest(all{day}.PostPrey, all{day}.PostPrey + all{day}.PostPrey')) = 0;
        mat(binotest(all{day}.PostPred, all{day}.PostPred + all{day}.PostPred')) = 0;
        mat = mat .* (mat > 0);
        
        szc = sum(all{day}.Contacts);
        sz = sum(all{day}.PostPred, 2)';
        
        removed = ((mat > 0) .* repmat(rank, first.nSubjects, 1)) > repmat(rank', 1, first.nSubjects);
        [~, bg] = SocialGraph(first, mat, removed, rank, sz);
        for k=1:length(bg.nodes)
            bg.nodes(k).ID = sprintf('%s(%d/%d)', bg.nodes(k).ID, sz(k), szc(k));
        end
        
        g = biograph.bggui(bg);
        f = get(g.biograph.hgAxes, 'Parent');
        print(f, [first.OutputPath 'pred.' prefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');
        close(g.hgFigure);
    end
end
fprintf('# done\n');
return;
%%
for i=1
    for day = 2;
        prefix = sprintf(a{i}, day);
        fprintf('# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions'});
            %obj = SocialPredPreyModel(obj);
            obj = SocialAnalysePredPreyModel_v2(obj);
            
            %saveFigure(['Graphs/' prefix '.SocialAnalysePredPreyModel']);
        catch me
           fprintf(['#   . FAILED! ' me.message '\n']); 
           for j=1:length(me.stack)
               fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
           end
        end
    end
end
%%
a = {...
    'Enriched.exp0001.day01.cam04',...
    'Enriched.exp0002.day01.cam04',...
    'Enriched.exp0003.day01.cam01',...
    'Enriched.exp0004.day01.cam01',...
    'SC.exp0001.day01.cam01',...
    'SC.exp0002.day01.cam01',...
    'SC.exp0003.day01.cam01',...
    'SC.exp0004.day01.cam04',...
    };

%objs1 = {};
%
for i=1:length(objs1)
    fprintf('# -> %s\n', objs1{1}.FilePrefix);
    try
        obj = objs1{i};
        %curr =load(['../base/Res/' a{i} '.obj.mat']);
        %curr.obj = SocialPredPreyModel(curr.obj);
        obj = SocialAnalysePredPreyModel(obj);
        %objs1{i} = curr.obj;
        saveFigure(['Graphs/' objs1{1}.FilePrefix '.SocialAnalysePredPreyModel2']);
    catch
    end
end