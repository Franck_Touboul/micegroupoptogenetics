SocialExperimentData
confidence_ = 0.05;
for confidence = confidence_
    index = 1;
    for id=GroupsData.Standard.idx
        if index > 2
            break;
        end
        %%
        Obj = struct();
        for day=1:4
            prefix = sprintf(experiments{id}, day);
            if day == 1
                Obj = TrackLoad(['Res/' prefix '.obj.mat'], {'common'});
            else
                obj = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'valid', 'nFrames'});
                Obj.zones = [Obj.zones, obj.zones];
                Obj.valid = [Obj.valid, obj.valid];
                Obj.nFrames = Obj.nFrames + obj.nFrames;
            end
            fprintf('# -> %s\n', prefix);
        end
        %%
        SocialPottsRobustnessTest(Obj, 1, confidence);
        SocialPottsRobustnessTest(Obj, 60, confidence);
        index = index + 1;
    end
end