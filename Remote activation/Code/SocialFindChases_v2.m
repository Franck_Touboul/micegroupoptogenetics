%%
obj = TrackLoad(obj);
obj = SocialFindInteractions(obj);
%%
obj.Interactions.ObjectLength = 150;

%%
converge = zeros(obj.nSubjects, obj.nSubjects, obj.nFrames);
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1 == m2; continue; end;
        q = tan(obj.Interactions.angle{m1, m2}) .* obj.Interactions.distance{min(m1, m2), max(m1, m2)};
        converge(m1, m2, :) = (abs(q) < obj.Interactions.ObjectLength) .* sign(q);
    end
end
converge(isnan(converge)) = 0;

%%
speed = obj.speed(:);
minspeed = FindProbabilityCutoffPoint(speed(speed>0), .75);
%%
features = {};
index = 1;
m1 = 2;
m2 = 1;
features{index} = ones(1, obj.nFrames) * 2;
features{index}(converge(m1, m2, :) == -1) = 1;
features{index}(converge(m1, m2, :) ==  0) = 2;
features{index}(converge(m1, m2, :) ==  1) = 3;
features{index}(obj.sheltered(m1, :) | obj.sheltered(m2, :)) = 2;
features{index}(obj.speed(m1, :) < minspeed) = 2;

contact = obj.Interactions.contact{min(m1, m2), max(m1,m2)};
features{index}(contact) = features{index}(contact) + 3;

%%
%
%        (pred)        (pred)
%       /      \      /      \
% (idel) ------ (cont) ------ (idel)
%       \      /      \      /
%        (prey)        (prey)
%

x = .5;
m = .25;
model.emis = [...
    m   x   m   0  0  0; % idel
    m   m   x   m  m  m; % pred
    x   m   m   x  m  m; % prey
    0   0   0   x  ; % cont
    m   m   x   m  ; % pred
    x   m   m   m  ; % prey
];

model.trans = [...
    1 1 1 1 0 0;
    0 1 0 1 0 0;
    0 0 1 1 0 0;
    1 0 0 1 1 1;
    1 0 0 0 1 0;
    1 0 0 0 0 1;
    ];
model.names = {'-', 'pred', 'prey', 'cont', 'pred', 'prey'};

states = hmmviterbi(features{index},model.trans,model.emis);
%%
[begF, endF, len, events] = FindEvents(states, states > 1); 
for i=1:length(events)
    events{i}.desc = [];
    events{i}.bounds = [];
    for j=1:length(model.names)
        r = GetRange(events{i}.data == j);
        if ~isempty(r)
            if ~isempty(events{i}.desc)
                events{i}.desc = [events{i}.desc ' -> '];
            end
%            events{i}.desc = [events{i}.desc, model.names{j} '(' num2str(r(2)-r(1)+1) ')'];
            events{i}.desc = [events{i}.desc, model.names{j} '(' num2str(events{i}.BegFrame -1 + r(1)) ')'];
            events{i}.bounds = [events{i}.bounds; events{i}.BegFrame -1 + [r(1), r(2)]];
        end
    end
end
