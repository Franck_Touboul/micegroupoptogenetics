function saveSocialForTracking(filename, social)
s.x = double(social.x);
s.y = double(social.y);
s.time = double(social.time);
s.meta.subject.centerColors = double(social.colors);
s.zones = double(social.zones.all);
s.labels = social.zones.labels;
%%
% s.Events(1).Title = 'contacts';
% for i=1:length(obj.Hierarchy.ChaseEscape.interactions.events{1,2})
%     s.Events(1).instances(i).Begin = obj.Hierarchy.ChaseEscape.interactions.events{1,2}{i}.BegFrame;
%     s.Events(1).instances(i).End = obj.Hierarchy.ChaseEscape.interactions.events{1,2}{i}.EndFrame;
%     s.Events(1).instances(i).Desc = obj.Hierarchy.ChaseEscape.interactions.events{1,2}{i}.desc;
% end

%%
social = s;
save(filename, 'social', '-v6');

