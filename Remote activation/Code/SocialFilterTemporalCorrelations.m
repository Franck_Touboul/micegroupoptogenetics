function obj = SocialFilterTemporalCorrelations(obj)
%%
baseVector = obj.ROI.nZones.^(obj.nSubjects-1:-1:0);
v = baseVector * (obj.zones - 1);
shifts = find([true v(2:end) ~= v(1:end-1)]);
obj.valid = obj.valid(shifts);
obj.zones = obj.zones(:, shifts);
obj.nFrames = length(shifts);


