%function TrackPath(nruns)
nruns = 120;
%%
fprintf('# finding paths\n');
options = struct();
TrackDefaults;
%%
options.colorMatchThresh = 0.9;
options.MaxHiddenDuration = 25;
options.MinProb = .4;
options.hidePenalty = 3;
options.appearPenalty = 3;

options.hidePercent = .99;
options.appearPercent = .995;
options.NumOfMapBins = 10;
options.NumOfDistanceBins = 10;

options.MaxProbOfDissappearing = 0.1;
%%
if 1==2
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nframes = xyloObj.NumberOfFrames;
dt = 1/xyloObj.FrameRate;
%%
fprintf('# - loading segmentations\n');
cents = struct();
cents.x       = [];
for i=1:nruns
    fprintf('#      . segment no. %d\n', i);
    filename = [options.output_path options.test.name '.segm.' sprintf('%03d', i) '.mat'];
    waitforfile(filename);
    currSegm = load(filename);
    cents = structcat(cents, currSegm.cents, 1, true, false);
end
%%
end
if isfield(options, 'ROIIgnore')
    ignore = uint8(imread(options.ROIIgnore));
    fprintf('#  . removing unwanted cents...');
    for i=1:size(cents.label, 1)
        labels = cents.label(i, :);
        y = cents.y(i,:);
        x = cents.x(i,:);
        y = y(labels~=0) / options.scale;
        if isempty(y)
            continue;
        end
        x = x(labels~=0) / options.scale;
        labels(labels~=0) = (1 - ignore( sub2ind(size(ignore), round(y), round(x)) )) .* labels(labels~=0);
        cents.label(i, :) = labels;
    end
    fprintf('[done]\n');
end
%%
prev = cents.label;
[q, cents.label] = max(cents.logprob, [], 3);
cents.label(prev == 0) = 0;

thresh = options.MinProb;
ndata = size(cents.x, 1);

mx = max(cents.x(:));
mx = mx + mx / 100;
my = max(cents.y(:));
my = my + my / 100;
mapBinsX = sequence(0, mx, options.NumOfMapBins+1);
mapBinsY = sequence(0, my, options.NumOfMapBins+1);
mapBins = 1:options.NumOfMapBins^2+1;
for i=1:options.nSubjects
    logprobs = cents.logprob(:, :, i);
    logprobs(cents.label ~= i) = -inf;
    [maxlogprobs, centids] = max(logprobs, [], 2);
    maxprobs = exp(maxlogprobs);
    res{i}.x = cents.x(sub2ind(size(cents.x), 1:ndata, centids'));
    res{i}.y = cents.y(sub2ind(size(cents.y), 1:ndata, centids'));
    res{i}.prob = maxprobs';
    res{i}.logprob = maxlogprobs';
    res{i}.centids = centids';
    
    res{i}.valid = maxprobs' >= thresh;
    res{i}.x(maxprobs < thresh) = nan;
    res{i}.y(maxprobs < thresh) = nan;
    res{i}.prob(maxprobs < thresh) = nan;
    res{i}.logprob(maxprobs < thresh) = -inf;
    res{i}.id = (maxprobs' > thresh) * i;
    res{i}.centids(maxprobs < thresh) = nan;
    % divide into bins
    [hx, binLocX] = histc(res{i}.x, mapBinsX);
    [hy, binLocY] = histc(res{i}.y, mapBinsY);
    res{i}.bin = (binLocY - 1) * options.NumOfMapBins + binLocX;
    res{i}.bin(~res{i}.valid) = 0;
    %% compute statistics
    distance = sqrt(diff(res{i}.x).^2+diff(res{i}.y).^2);
    res{i}.distance = distance;
    res{i}.distanceBins = sequence(0, max(res{i}.distance), options.NumOfDistanceBins);
    res{i}.distanceDisc = histc(res{i}.distance, res{i}.distanceBins);
    res{i}.logpdist = res{i}.distanceDisc(1:options.NumOfDistanceBins) + 1;
    res{i}.logpdist = flog(res{i}.logpdist / sum(res{i}.logpdist));
    % p11:
    res{i}.meanDistance = mean(distance(~isnan(distance)));
    res{i}.pareto = gpfit(distance(distance>0));
    res{i}.logp1 = flog(mean(res{i}.valid(2:end) == 1));
    % p0g0: stay hidden
    res{i}.logp0g0 = flog(mean(diff(res{i}.valid) == 0 & res{i}.valid(2:end) == 0) / mean(res{i}.valid(2:end) == 0));
    % p1g0: appear
    res{i}.logp1g0 = flog(mean(diff(res{i}.valid) == 1)  / mean(res{i}.valid(2:end) == 0));
    % p0g1: disappear
    dissappear = [diff(res{i}.valid) == -1 false];
    h01 = histc(res{i}.bin(dissappear), mapBins);
    h_1 = histc(res{i}.bin(res{i}.valid(2:end) == 1), mapBins);
    prob = h01 ./ (h_1 + 1);
    prob(prob > options.MaxProbOfDissappearing) = options.MaxProbOfDissappearing;
    res{i}.logp0g1gl = flog(prob);
end
%%
h01 = 0;
h_1 = 0;
h_0 = 0;
h10 = 0;
for i=1:options.nSubjects
    % p0g1: disappear
    dissappear = [diff(res{i}.valid) == -1 false];
    h01 = h01 + histc(res{i}.bin(dissappear), mapBins);
    h_1 = h_1 + histc(res{i}.bin(res{i}.valid == 1), mapBins);
end

for i=1:options.nSubjects
    % p0g1: disappear
    prob = h01 ./ (h_1 + 1);
    %prob(prob > options.MaxProbOfDissappearing) = options.MaxProbOfDissappearing;
    res{i}.logp0g1gl = flog(prob);
end

%%
for i=1:options.nSubjects
    distances = [];
    for j=1:options.nSubjects
        if j~=i
            distances = [distances, sqrt((res{i}.x - res{j}.x).^2 +(res{i}.y - res{j}.y).^2)];
        end
    end
    res{i}.allDistMean = mean(distances(~isnan(distances)));
    res{i}.allDistVar = var(distances(~isnan(distances)));
end
    
ores = res;
%%
for curr = 1:options.nSubjects
    fprintf('# - tracking subject %d of %d\n', curr, options.nSubjects);
    currLogprobs = cents.logprob(:, :, curr);
    
    %% build observation probability table
    table = ones(options.nSubjects+1, length(res{curr}.id)) * -inf;
    path = ones(size(table), 'uint16') * (options.nSubjects + 1);
    for s=1:options.nSubjects
        prevPos.x(s) = xyloObj.Width/2 * options.scale;
        prevPos.y(s) = xyloObj.Height/2 * options.scale;
        
        range = 1:ndata;
        centids = res{s}.centids;
        
        range = range(~isnan(centids));
        centids = centids(~isnan(centids));
        
        table(s, range) = currLogprobs(sub2ind(size(currLogprobs), range, centids));
    end
    prevPos.x(options.nSubjects+1) = xyloObj.Width/2 * options.scale;
    prevPos.y(options.nSubjects+1) = xyloObj.Height/2 * options.scale;
    table(options.nSubjects+1, :) = flog(prod(1 - exp(table(1:options.nSubjects, :)), 1));
    emitlogprobs = table;
    
    valid = sum(isfinite(table), 1) > 1;
    d = diff([0 valid 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    
    %%
    prevPos.x = ones(1, options.nSubjects) * inf;
    prevPos.y = ones(1, options.nSubjects) * inf;
    prevPos.bin = ones(1, options.nSubjects);
    hiddenPos.x = inf;
    hiddenPos.y = inf;
    currPos = prevPos;
    for n=1:length(start)
        r = start(n):finish(n);
        if n>1
            prevColn = table(:, finish(n-1));
        else
            prevColn = ones(options.nSubjects+1, 1) * -inf;
            prevColn(end) = 0;
        end
        for f=r
            %
            for s=1:options.nSubjects
                if ~isfinite(table(s, f))
                    continue;
                end
                x = res{s}.x(f);
                y = res{s}.y(f);
                bin = res{s}.bin(f);
                %                 distance = sqrt((x - prevPos.x).^2 + (y - prevPos.y).^2)';
                %                 trans = [gamma + (-distance/res{curr}.meanDistance); 0];
                
                distance = sqrt((x - prevPos.x).^2 + (y - prevPos.y).^2)';
                prob = gppdf(distance, res{curr}.pareto(2), res{curr}.pareto(1));
                prob = flog(prob ./ (gauss(res{curr}.allDistMean, res{curr}.allDistVar, distance) + prob));
                
                hdistance = sqrt((x - hiddenPos.x).^2 + (y - hiddenPos.y).^2)';
                if isfinite(hdistance)
                    hprob = gppdf(hdistance, res{curr}.pareto(2), res{curr}.pareto(1));
                    hprob = flog(hprob ./ (gauss(res{curr}.allDistMean, res{curr}.allDistVar, hdistance) + hprob));
                else
                    hprob = res{curr}.logp0g1gl(bin);
                end
                
                trans = [prob; hprob];

%                 distancebin = ceil([sqrt((x - prevPos.x).^2 + (y - prevPos.y).^2)']/ (res{curr}.distanceBins(2) - res{curr}.distanceBins(1)));
%                 invalid = ~isfinite(distancebin) | distancebin>length(distancebin);
%                 distancebin(invalid) = 1;
%                 distancebin(distancebin == 0) = 1;
%                 trans = [res{curr}.logpdist(distancebin)'; res{curr}.logp1g0];
%                 trans(invalid) = -inf;

                [val, from] = max(prevColn + trans);
                table(s, f) = table(s, f) + val;
                path(s, f) = from;
                currPos.x(s) = x;
                currPos.y(s) = y;
                currPos.bin(s) = bin;
            end
            %
            trans = [res{curr}.logp0g1gl(prevPos.bin)'; res{curr}.logp0g0];
            [val, from] = max(prevColn + trans);
            table(end, f) = table(end, f) + val;
            path(end, f) = from;
            if from <= options.nSubjects
                hiddenPos.x = prevPos.x(from);
                hiddenPos.y = prevPos.y(from);
            end
            %
            prevPos = currPos;
            prevColn = table(:, f);
        end
    end
    %% in case we want to continue from prev place
%     currPos.x = ones(1, options.nSubjects + 1) * nan;
%     currPos.y = ones(1, options.nSubjects + 1) * nan;
%     gamma = -flog(res{curr}.meanDistance);
%     for n=1:length(start)
%         currPos.x = ones(1, options.nSubjects + 1) * nan;
%         currPos.y = ones(1, options.nSubjects + 1) * nan;
%         
%         r = start(n):min(finish(n)+1, nframes);
%         for f=r
%             prevPos = currPos;
%             %
%             for s=1:options.nSubjects
%                 if ~isfinite(table(s, f))
%                     continue;
%                 end
%                 x = res{s}.x(f);
%                 y = res{s}.y(f);
%                 distance = sqrt((x - prevPos.x).^2 + (y - prevPos.y).^2)';
%                 trans = gamma + (-distance/res{curr}.meanDistance);
%                 trans(isnan(trans)) = 0;
%                 [val, from] = max(table(:, f-1) + trans);
%                 table(s, f) = table(s, f) + val;
%                 path(s, f) = from;
%                 currPos.x(s) = x;
%                 currPos.y(s) = y;
%             end
%             %
%             [val, from] = max(table(:, f-1));
%             table(end, f) = table(end, f) + val;
%             path(end, f) = from;
%             currPos.x(options.nSubjects + 1) = prevPos.x(from);
%             currPos.y(options.nSubjects + 1) = prevPos.y(from);
%         end
%     end
    %% backtrack
    fprintf('#  . backtracking\n');
    track{curr}.src = ones(1, size(table, 2)) * (options.nSubjects + 1);
    track{curr}.logprob = zeros(1, size(table, 2));
    track{curr}.emitlogprob = zeros(1, size(table, 2));
    track{curr}.x = zeros(1, size(table, 2));
    track{curr}.y = zeros(1, size(table, 2));

    for n=length(start):-1:1
        r = start(n):finish(n);
        f=finish(n);
        if n<length(start)
            idx = path(track{curr}.src(start(n+1)), start(n+1));
            
            track{curr}.src(finish(n)) = idx;
            track{curr}.logprob(f) = table(idx, f);
            track{curr}.emitlogprob(f) = emitlogprobs(idx, f);
        else
            [track{curr}.logprob(finish(n)), track{curr}.src(finish(n))] = max(table(:, finish(n)));
            track{curr}.emitlogprob(finish(n)) = emitlogprobs(track{curr}.src(finish(n)), finish(n));
        end
        for f=finish(n)-1:-1:start(n)
            idx = path(track{curr}.src(f+1), f+1);
            
            track{curr}.src(f) = idx;
            track{curr}.logprob(f) = table(idx, f);
            track{curr}.emitlogprob(f) = emitlogprobs(idx, f);
        end
        if n>1
            track{curr}.logprob(finish(n-1)+1:start(n)-1) = track{curr}.logprob(start(n));
            track{curr}.emitlogprob(finish(n-1)+1:start(n)-1) = track{curr}.emitlogprob(start(n));
        end
    end
    
    for i=1:length(track{curr}.src)
        src = track{curr}.src(i);
        if src <= options.nSubjects
            track{curr}.x(i) = res{src}.x(i);
            track{curr}.y(i) = res{src}.y(i);
        else
            track{curr}.x(i) = nan;
            track{curr}.y(i) = nan;
        end
    end
    track{curr}.valid = track{curr}.src <= options.nSubjects;
    track{curr}.id = track{curr}.valid * curr;
end
% %%
if 1==2
    filtered = track;
    for curr=1:options.nSubjects
        start = [];
        finish = [];
        for j=1:options.nSubjects+1
            valid = track{curr}.src == j;
            d = diff([0 valid 0], 1, 2);
            start  = [start, find(d > 0)];
            finish = [finish, find(d < 0) - 1];
        end
        for n=1:length(start)
            r = start(n):finish(n);
            post = min(finish(n)+1, length(track{curr}.src));
            prob = track{curr}.logprob(start(n):post) - track{curr}.emitlogprob(post);
            pcurr = exp(prob / sum(track{curr}.valid(r)));
            pother = 0;
            for s=1:options.nSubjects
                if s~=curr
                    len = sum(track{s}.valid(r));
                    if len > 0
                        prob = track{s}.logprob(start(n):post) - track{s}.emitlogprob(post);
                        prob = prob / len;
                        pother = pother + (1 - exp(prob));
                    end
                end
            end
            r = pcurr / pother * (options.nSubjects - 1);
            if r < 1
                filtered{curr}.x(r) = nan;
                filtered{curr}.y(r) = nan;
                filtered{curr}.valid(r) = false;
                filtered{curr}.id(r) = 0;
            end
        end
    end
end
%%
%pres = res;
res = track;
%
fprintf('# intepolating short epochs\n');
for i=1:options.nSubjects
    d = diff([0 res{i}.id == 0 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    len = finish - start + 1;
    valid = len < options.MaxHiddenDuration;
    start = start(valid);
    finish = finish(valid);
    for j=1:length(start)
        if start(j) <= 1 || finish(j) > nframes
            continue;
        end
        r = start(j):finish(j);
%        [res{i}.x(r), res{i}.y(r)] = MotionInterp(res{i}.x, res{i}.y, r, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
        [res{i}.x(r), res{i}.y(r)] = MotionInterp(res{i}.x, res{i}.y, r, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
%             if finish(1) < nFrames
%                 res{i}.x(r) = res{i}.x(finish(j)+1);
%                 res{i}.y(r) = res{i}.y(finish(j)+1);
%             end
%        end
    end
end

for i=1:options.nSubjects
    d = diff([0 res{i}.id == 0 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    for j=1:length(start)
        r = start(j):finish(j);
        if start(j) > 1
            res{i}.x(r) = res{i}.x(start(j)-1);
            res{i}.y(r) = res{i}.y(start(j)-1);
        else
%             if finish(1) < nFrames
%                 res{i}.x(r) = res{i}.x(finish(j)+1);
%                 res{i}.y(r) = res{i}.y(finish(j)+1);
%             end
        end
    end
end


filename = [options.output_path options.test.name '.simple-track.mat'];
save(filename, 'res');



% fprintf('# - interpolate position\n');
% for curr = 1:options.nSubjects;
%     hidden = res{curr}.id == 0;
%     d = diff([0 hidden 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     loc = find(start > 1 & finish < length(hidden));
%     for l=loc
%         range = start(l):finish(l);
%         %         res{curr}.x(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.x([start(l)-1, finish(l)+1]), start(l):finish(l));
%         %         res{curr}.y(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.y([start(l)-1, finish(l)+1]), start(l):finish(l));
%         [res{curr}.x(range), res{curr}.y(range)] = MotionInterp(res{curr}.x, res{curr}.y, range, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
%     end
%     if length(finish) > 0 && finish(end) == length(hidden) && start(end) > 1
%         res{curr}.x(start(end):finish(end)) = res{curr}.x(start(end)-1);
%         res{curr}.y(start(end):finish(end)) = res{curr}.y(start(end)-1);
%     end
% end

filename = [options.output_path options.test.name '.track.mat'];
save(filename, 'res');

