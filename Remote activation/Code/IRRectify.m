function r = IRRectify(map)
%%
[x,y] = find(~isnan(map));
m = mean([std(x) std(y)]);
x = (x - mean(x)) / m;
y = (y - mean(y)) / m;
coeff = pca([x,y]);
r = imrotate(map, atan2(coeff(1,1), coeff(1,2)) / pi * 180);
r = trim(r);