function data = findRange(x)

c = conv([0 x(:)' 0], [1 -1]);
data.start = find(c > 0) - 1;
data.end   = find(c < 0) - 2;

