res.ranks = [];
res.x = [];
res.h = [];
%%
for i=11:18
    obj = TrackLoad({i});
    %%
    res.ranks = [res.ranks, max(obj.Hierarchy.Group.rank) - obj.Hierarchy.Group.rank + 1];
    [h, x] = TimePSTH (obj);
    res.x = x;
    res.h = [res.h; h];
    
end
%%
for i=11:18
    obj = TrackLoad({i 2}, {'Hierarchy'});
    %%
    res.ranks = [res.ranks, max(obj.Hierarchy.Group.rank) - obj.Hierarchy.Group.rank + 1];
end
%%
%%
colors = [...
    1 0 0;
    1 1 0;
    0 1 1;
    0 0 1];
for i=1:4
    map = res.ranks == i;
    range = 4;
    subplot(4, 1, i);
    h = res.h(map, :);
    for j=1:size(h, 1)
        rmap = res.x >= -range & res.x <= range;
        ch = h(j, rmap);
        plot(res.x(rmap),  (ch - quantile(ch, .2)) / (quantile(ch, .8) - quantile(ch, .2)), 'color', colors(i, :)); hon;
    end
    vert_line(0, 'color', 'k');
    hoff
end