%%function SocialMeter(obj)
%%
options.DefaultZone = '(BigNest)';
options.Beta = 2^-7;
%%
%me = obj.Analysis.Potts.Model{3};
me = SocialGetRegPotts(obj, options.Beta, 3);
%w = abs(me.weights);
w1 = (me.weights);
w1(me.order ~= 1) = 0;


w2 = (me.weights);
w2(me.order ~= 2) = 0;

w3 = (me.weights);
w3(me.order ~= 3) = 0;

sociality = [];
sociality(1).map = me.perms * w1';
sociality(2).map = me.perms * w2';
sociality(3).map = me.perms * w3';
defZoneIndex = find(strcmp(obj.ROI.ZoneNames, options.DefaultZone));
defPermIndex = find(all(me.inputPerms == defZoneIndex, 2));
% sociality(2).map = sociality(2).map - sociality(2).map(defPermIndex);
% sociality(3).map = sociality(3).map - sociality(3).map(defPermIndex);
%%
socialityMap1 = sociality(1).map;
socialityMap2 = sociality(2).map;
socialityMap3 = sociality(3).map;
%% measure total weights 
base = obj.ROI.nZones;
data = obj.zones(:, obj.valid) - 1;
baseVector = base.^(size(data, 1)-1:-1:0);
zidx = baseVector * data + 1;
obj.sociality = [];
obj.sociality.indep = zeros(1, obj.nFrames);
obj.sociality.indep(obj.valid) = socialityMap1(zidx);
obj.sociality.pairs = zeros(1, obj.nFrames);
obj.sociality.pairs(obj.valid) = socialityMap2(zidx);
obj.sociality.triplets = zeros(1, obj.nFrames);
obj.sociality.triplets(obj.valid) = socialityMap3(zidx);

bsociality1 = blkproc(obj.sociality.indep, [1, options.res], @median);
bsociality2 = blkproc(obj.sociality.pairs, [1, options.res], @median);
bsociality3 = blkproc(obj.sociality.triplets, [1, options.res], @median);
%% violence
options.res = obj.FrameRate * 60 * 5;

violence = zeros(1, obj.nFrames);
for i=find(obj.Contacts.Behaviors.AggressiveChase.Map)
    c = obj.Contacts.List(i);
    begf = min(c.beg);
    endf = max(c.end);
    violence(begf:endf) = violence(begf:endf) + 1;
end
bviolence =  blkproc(violence, [1, options.res], @sum);
subplot(3,1,1);
bar(bviolence)
xaxis(0, obj.nFrames / options.res)
%%
cmap = [220    73    89; 239 144 53; 178 211 155; 0 0 0]/256;
t = MyCategoricalColormap;
cmap(4, :) = t(1,:);
countCmap = repmat(flipdim(sequence(.7, 1, obj.nSubjects+1)', 1), 1, 3);
subplot(3,1,1);
count = sum(~obj.sheltered);
count(~obj.valid) = 0;
bcount =  round(blkproc(count, [1, options.res], @median));
%imagesc(bcount);
BackShading(bcount + 1, [-1, 1], countCmap)
%colormap(countCmap)
%
hon;
bar(bviolence / max(bviolence), 'FaceColor', cmap(1, :), 'EdgeColor', 'none')
hon;
%plot(bsociality1 / max(abs(bsociality1)), '-o', 'MarkerFaceColor', cmap(4, :), 'MarkerEdgeColor', 'none', 'color', cmap(4, :), 'MarkerSize', 3);
plot(bsociality2 / max(abs(bsociality2)), '-o', 'MarkerFaceColor', cmap(2, :), 'MarkerEdgeColor', 'none', 'color', cmap(2, :), 'MarkerSize', 3);
plot(bsociality3 / max(abs(bsociality3)), '-o', 'MarkerFaceColor', cmap(3, :), 'MarkerEdgeColor', 'none', 'color', cmap(3, :), 'MarkerSize', 3);
hoff;
xaxis(0, obj.nFrames / options.res)
%set(gca, 'XTickLabel', get(gca, 'XTick') * options.res / (60 * obj.FrameRate));
set(gca, 'XTick', (0:2:12) * 60 * 60 * obj.FrameRate / options.res);
set(gca, 'XTickLabel', (0:2:12));
set(gca, 'Box', 'off')
xlabel('Time [hour]');
