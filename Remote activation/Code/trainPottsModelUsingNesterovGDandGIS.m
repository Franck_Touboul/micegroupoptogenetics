function me = trainPottsModelUsingNesterovGDandGIS(options, data, confidence)
options.param_report = false;
options = setDefaultParameters(options, ...
    'Statistics', false, ...
    'tk', 1, 'nIters', 2000);

me.tk = options.tk;
me.alpha = .5;
me.beta = .5;
useLineSearch = false;

options.NeutralZone = 1; % Open
%%
use_sparse = true;
if nargin < 3
    confidence = -.05;
end
me.confidence = confidence;
me.neutralZone = options.NeutralZone;

if isfield(options, 'Output')
    output = options.Output;
else
    output = true;
end
%
if output; fprintf('# Training Potts Model\n'); end
%
if output; fprintf('# - finding all permutation of max dimension %d...\n', options.n); end;
%%
allPerms = nchoose(1:options.nSubjects);
nPerms = {};
j = 1;
for i=1:length(allPerms)
    if length(allPerms{i}) <= options.n 
        nPerms{j} = allPerms{i};
        j = j + 1;
    end
end

%%
if output; fprintf('# - computing constraints...\n'); end
[me.constraints, pci] = PottsComputeConstraints(data' + 1, options.count, nPerms, abs(confidence));
C = length(nPerms);
%
me.confidence = confidence;
me.converged = false;
%
me.inputPerms = myPerms(options.nSubjects, options.count);
[me.perms, me.labels] = assignFeature(me.inputPerms, options.count, nPerms, use_sparse);

valid = false(1, length(me.labels));
if options.count == 2
    for i=1:length(me.labels)
        if all(me.labels{i}(2, :) ~= 2)
            valid(i) = true;
        end
    end
    me.labels = me.labels(valid);
    me.perms = me.perms(:, valid);
    me.constraints = me.constraints(valid);
    pci = pci(valid);
end
%
me.nSubjects = options.nSubjects;
me.range = options.count;
% initialize the weights
me.weights = randn(1, size(me.labels, 2)) / size(me.labels, 2);
orig = me;
%% filter neutral zone
neutral = false(1, length(orig.labels));
for l=1:length(me.labels)
    neutral(l) = any(orig.labels{l}(2, :) == options.NeutralZone);
end
me.constraints = orig.constraints(~neutral);
me.perms = orig.perms(:, ~neutral);
me.labels = orig.labels(~neutral);
me.weights = orig.weights(~neutral);
me.momentum = me.weights;
pci = pci(~neutral, :);

%% compute empirical probabilities
base = me.range;
baseVector = base.^(size(data, 1)-1:-1:0);
pEmp = histc(baseVector * data+1, 1:base ^ size(data, 1));
pEmp = pEmp / sum(pEmp);
%% initial halfKL
p = exp(me.perms * me.momentum');
Z = sum(p);
p = p / Z;
halfkl = -pEmp * log2(p);
%%
if options.Statistics; me.Statistics = struct(); end
tic;
for iter=1:options.nIters
    if output; fprintf('# - iter (%4d/%4d) : ', iter, options.nIters); end
    % compute the (un-normalized) pdf for each sample
    p = exp(me.perms * me.momentum');
    perms_p = p;
    % normalize the pdf (the Z)
    Z = sum(p);
    p = p / Z;
    prev_halfkl = halfkl;
    halfkl = -pEmp * log2(p);
    E = full(sum(me.perms .* repmat(p, 1, size(me.perms, 2))));
    if confidence ~= 0
        %[sum(E'  > pci(:, 1) & E' < pci(:, 2)) (1 - confidence) * length(me.weights)]
        nconverged = sum(E'  > pci(:, 1) & E' < pci(:, 2));
        if output; fprintf('half KL = %6.4f, convergence = %3d%%\n', halfkl, round(nconverged / length(me.weights) * 100)); end
        if options.Statistics;
            me.Statistics(iter).convergence = nconverged / length(me.weights);
        end
        if nconverged >= (1 - confidence) * length(me.weights)
            if output; 
                fprintf('# converged to confidence interval (alpha = %3.2f)\n', confidence); 
            end
            break;
        end
    else
        if output; fprintf('half KL = %6.4f (%.3f)\n', halfkl, abs((halfkl-prev_halfkl)/prev_halfkl)); end
    end
    %%
    t = me.tk;
    if useLineSearch
        df = (me.constraints - E);
        f = -pEmp * log2(p);
        found = false;
        while ~found
            pt = exp(me.perms * (me.weights' + t * df'));
            pt = pt / sum(pt);
            found = -pEmp * log2(pt) < f + me.alpha * t * sum(df.^2);
            if ~found
                t = me.beta * t;
            end
        end
    end
    
    dw = log(me.constraints ./ E);
    dw(me.constraints == 0) = 0;

    pWeights = me.weights;
    
    me.weights = me.momentum + .5 * t * (me.constraints - E) + .5 * 1/C * dw;
    me.momentum = me.weights + (iter - 1)/(iter+2) * (me.weights - pWeights);
    %%
    if options.Statistics;
        me.Statistics(iter).times = toc; 
        me.Statistics(iter).halfKL = halfkl;
    end
end
%me = rmfield(me, 'features');
