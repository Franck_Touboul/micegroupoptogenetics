function p = halfCircularUniformExp(x, alpha, m)
alpha = abs(alpha);
m = min(max(m, 0), 1);

p = alpha * exp(-alpha*x) / (1-exp(-alpha*pi));
%p = (p + m) / (1+pi*m); 
p = (1-m)*p + m/pi;