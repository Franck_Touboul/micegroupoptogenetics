function [n, edges] = hist2(x, y, nbins)

edges = [sequence(min(x), max(x), nbins); sequence(min(y), max(y), nbins)];
%[X, Y] = meshgrid(edges(1, :), edges(2, :));
[nX, binX] = histc(x, edges(1, :));
[nY, binY] = histc(y, edges(2, :));
n = zeros(nbins);
for i=1:length(binX)
    n(binX(i), binY(i)) = n(binX(i), binY(i)) + 1;
end