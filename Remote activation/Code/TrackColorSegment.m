function [obj, cents] = TrackColorSegment(obj, varargin)
matchCents = true;
%%
if ischar(obj)
    obj = TrackLoad(obj);
end
obj.ErodeRadios = 2;
obj.MaxNumOfCents = obj.nSubjects * 3; % @@@
%%
fprintf('# segmenting movie frames:\n');
if ~license('checkout', 'image_toolbox')
    fprintf('# - waiting for image processing toolbox license\n');
    while ~license('checkout', 'image_toolbox'); pause(5); end
end
obj.UseRGB = false;
SaveFullSegmentData = true;

%%
fprintf('# segmenting frames\n');
nFrames = obj.nFrames;
%%
nColors = obj.nSubjects;
if isfield(obj.Colors, 'FillerHistogram')
    nColors = obj.nSubjects + 1;
end
%%
output = false;
if isempty(varargin)
    startframe = randi(nFrames);
    endframe = startframe;
    output = true;
elseif length(varargin) == 1
    startframe = varargin{1}(1);
    endframe = varargin{1}(end);
    output = true;
else
    nruns = varargin{1};
    id = varargin{2};
    step = floor(nFrames / nruns);
    curr = 0;
    for i=1:id
        prev = curr + 1;
        if i == nruns
            curr = nFrames;
        else
            curr = prev + step - 1;
        end
    end
    startframe = prev;
    endframe = curr;
end
%output = true;

nFrames = endframe - startframe + 1;
%%
if matchCents
    realStartframe = startframe;
    if nFrames > 1
        startframe = max(1, startframe - 1);
    end
    nFrames = endframe - startframe + 1;
    cents.prev = zeros(nFrames, obj.MaxNumOfCents, 'uint8');
end
%%
sx = []; sy = [];

cents.x = zeros(nFrames, obj.MaxNumOfCents);
cents.y = zeros(nFrames, obj.MaxNumOfCents);
cents.label = zeros(nFrames, obj.MaxNumOfCents, 'uint8');
cents.area = zeros(nFrames, obj.MaxNumOfCents, 'uint16');
cents.solidity = zeros(nFrames, obj.MaxNumOfCents, 'single');
cents.logprob = zeros(nFrames, obj.MaxNumOfCents, obj.nSubjects);
cents.threshold = zeros(nFrames, 1, 'uint8');
if SaveFullSegmentData
    Properties = {'Solidity', 'Centroid', 'PixelIdxList', 'MajorAxisLength', 'MinorAxisLength', 'Orientation'};
    cents.minorAxisLength = zeros(nFrames, obj.MaxNumOfCents, 'uint16');
    cents.majorAxisLength = zeros(nFrames, obj.MaxNumOfCents, 'uint16');
    cents.orientation = zeros(nFrames, obj.MaxNumOfCents, 'uint8');
else
    Properties = {'Solidity', 'Centroid', 'PixelIdxList'};

end
%%
%%
cmap = [obj.Colors.Centers; 0 0 0];
bkgFrame = im2double(imresize(obj.BkgImage, obj.VideoScale));
%%
nchars = reprintf('# - frame %6d [%d-%d] (%6.2fxiRT)', startframe, startframe, endframe, 0); 
tic;
for r=1:nFrames
    RT = toc / r * obj.FrameRate;
    nchars = reprintf(nchars, '# - frame %6d [%d-%d] (%6.2fxiRT)', r+startframe-1, startframe, endframe, RT);
    currTime = (r+startframe-1) * obj.dt;
    if currTime < obj.StartTime;
        continue;
    end
    if obj.EndTime > 0 && currTime > obj.EndTime
        continue;
    end
    
    orig = myMMReader(obj.VideoFile, r+startframe-1, obj.BkgImage);
    orig = im2double(imresize(orig, obj.VideoScale));
    m = imsubtract(orig, bkgFrame);
    
    if isempty(sx)
        [sx, sy, nc] = size(m);
    end
    %%
    if obj.UseRGB
        rm = orig(:, :, 1);
        gm = orig(:, :, 2);
        bm = orig(:, :, 3);
    else
        hsv = rgb2hsv(orig);
        hm = hsv(:,:,1);
        sm = hsv(:,:,2);
        vm = hsv(:,:,3);
    end
    
%    hsv_m = rgb2hsv(m);
%     hm = hsv_m(:,:,1);
%     sm = hsv_m(:,:,2);
%     sm = hsv_m(:,:,2);
    lum = max(m,[],3);
    srclum = lum;
    %%
    meanBKG = mean(lum(:)); 
    stdBKG = std(lum(:));
    if obj.UseAdaptiveThresh
        upper = obj.NoiseThresh;
        lower = 1;
        prev_thresh = round((upper + lower)/2);
        while true
            thresh = round((upper + lower)/2);
            bw = lum > meanBKG + thresh * stdBKG;
            %bw(ignoreRegion) = false;
            cc = bwconncomp(bw);
            if cc.NumObjects < obj.MaxNumOfObjects
                upper = thresh - 1;
                prev_thresh = thresh;
            else
                lower = thresh + 1;
            end
            if lower > upper
                break
            end
        end
        if thresh ~= prev_thresh
            thresh = prev_thresh;
            bw = lum > meanBKG + thresh * stdBKG;
        end
    else
        thresh = obj.NoiseThresh;
        bw = lum > meanBKG + thresh * stdBKG;
        cc = bwconncomp(bw);
        if cc.NumObjects > obj.MaxNumOfObjects
            continue;
        end
    end
    bw = bwareaopen(bw, obj.MinNumOfPixels);
    cents.threshold(r) = thresh;
    %%
    %labels = bwlabel(bw);
    %nobjects = max(labels(:));
    %%
    if obj.UseRGB
        [b, idx_r] = histc(rm(bw), obj.Colors.Bins);
        [b, idx_g] = histc(gm(bw), obj.Colors.Bins);
        [b, idx_b] = histc(bm(bw), obj.Colors.Bins);
        
        prob_r = zeros(nColors, length(idx_r));
        prob_g = zeros(nColors, length(idx_g));
        prob_b = zeros(nColors, length(idx_b));
        
        for i=1:nColors
            if i <= obj.nSubjects
                idx = i;
                source = obj.Colors.Histrogram;
            else
                idx = 1;
                source = obj.Colors.FillerHistogram;
            end
            prob_r(i, :) = source.R(idx, idx_r);
            prob_g(i, :) = source.G(idx, idx_g);
            prob_b(i, :) = source.B(idx, idx_b);
        end
        joint_prob = prob_r .* prob_g .* prob_b;
    else
        [b, idx_h] = histc(hm(bw), obj.Colors.Bins);
        [b, idx_s] = histc(sm(bw), obj.Colors.Bins);
        [b, idx_v] = histc(vm(bw), obj.Colors.Bins);
        
        prob_h = zeros(nColors, length(idx_h));
        prob_s = zeros(nColors, length(idx_s));
        prob_v = zeros(nColors, length(idx_v));
        
        for i=1:nColors
            if i <= obj.nSubjects
                idx = i;
                source = obj.Colors.Histrogram;
            else
                idx = 1;
                source = obj.Colors.FillerHistogram;
            end
            prob_h(i, :) = source.H(idx, idx_h);
            prob_s(i, :) = source.S(idx, idx_s);
            prob_v(i, :) = source.V(idx, idx_v);
        end
        joint_prob = prob_h .* prob_s .* prob_v;
    end
    
    [m, idx] = max(joint_prob, [], 1);
    nlabels = zeros(sx,sy,'uint8');
    nlabels(bw) = idx;
    
    logprobmap = cell(1, nColors);
    for k=1:nColors
        logprobmap{k} = zeros(sx,sy);
        logprobmap{k}(bw) = flog(joint_prob(k, :));
    end
    if output
        flabels = zeros(sx,sy,'uint8');
    end
    %% filter small regions
    rejected = nlabels == nColors;
    conn = cell(1, obj.nSubjects);
    validmap = cell(1, obj.nSubjects);
    for i=1:obj.nSubjects
        reg = bwconncomp(nlabels == i);
        conn{i} = regionprops(reg, Properties{:});
        validmap{i} = false(size(bw));
        for j=1:length(conn{i})
            if conn{i}(j).Solidity * length(reg.PixelIdxList{j}) < obj.MinNumOfPixels
				rejected(reg.PixelIdxList{j}) = true;
                conn{i}(j).PixelIdxList = [];
            else
                validmap{i}(conn{i}(j).PixelIdxList) = true;
            end
        end
    end
    %% reassign rejected regions to clusters
    lrejected = bwlabel(rejected);
    dlrejected = imdilate(lrejected, ones(3,3));
    for i=1:obj.nSubjects
        u_ = unique(dlrejected(validmap{i}))';
        if ~isempty(u_)
            for u=u_
                if u > 0
                    validmap{i}(lrejected == u) = true;
                    rejected(lrejected == u) = false;
                    changed = true;
                end
            end
        end
    end
    
    %% compute the new region props
    for i=1:obj.nSubjects
        validmap{i} = AutoErode(validmap{i}, obj.ErodeRadios);
        reg = bwconncomp(validmap{i});
        conn{i} = regionprops(reg, Properties{:});
    end    
    rejected = AutoErode(rejected, obj.ErodeRadios);
    reg = bwconncomp(rejected);
    conn{obj.nSubjects+1} = regionprops(reg, Properties{:});
    currConn = cell(1, obj.MaxNumOfCents);
    
    %% save centers
    if matchCents
        centIndex = 1;
    end
    for i=1:obj.nSubjects+1
        %%
        if i <= obj.nSubjects
            currmap = validmap{i};
        else
            currmap = rejected;
        end
        for j=1:length(conn{i})
            if length(conn{i}(j).PixelIdxList) > obj.MinNumOfPixelsAfterErode
                cents.x(r, centIndex) = conn{i}(j).Centroid(1);
                cents.y(r, centIndex) = conn{i}(j).Centroid(2);
                cents.area(r, centIndex) = length(conn{i}(j).PixelIdxList);
                cents.solidity(r, centIndex) = conn{i}(j).Solidity;
                if SaveFullSegmentData
                    cents.minorAxisLength(r, centIndex) = conn{i}(j).MinorAxisLength;
                    cents.majorAxisLength(r, centIndex) = conn{i}(j).MajorAxisLength;
                    cents.orientation(r, centIndex) = conn{i}(j).Orientation;
                end
                logsum = -inf;
                for k=1:nColors
                    %cents.logprob(r, centIndex, k) = sum(logprobmap{k}(conn{i}(j).PixelIdxList));
                    logsum = add_lns(logsum, logprobmap{k}(conn{i}(j).PixelIdxList));
                end
                for k=1:nColors
                    cents.logprob(r, centIndex, k) = log(mean(exp(logprobmap{k}(conn{i}(j).PixelIdxList) - logsum)));
                end
                
                if i <= obj.nSubjects
                    currlabel = i;
                    cents.label(r, centIndex) = i;
                else
                    [q, currlabel] = max(cents.logprob(r, centIndex, 1:obj.nSubjects));
                    cents.label(r, centIndex) = currlabel;
                end
                if matchCents
                    currConn{centIndex} = conn{i}(j).PixelIdxList;
                    if r > 1
                        a = zeros(1, obj.MaxNumOfCents);
                        for k=find(cents.label(r-1, :) == currlabel)
                            a(k) = length(intersect(prevConn{k}, currConn{centIndex}));
                        end
                        [m, k] = max(a);
                        if m > 0
                            cents.prev(r, centIndex) = k;
                        end
                    end
                end
                    
                if output && i~= cents.label(r, centIndex)
                    validmap{i}(conn{i}(j).PixelIdxList) = false;
                    validmap{cents.label(r, centIndex)}(conn{i}(j).PixelIdxList) = true;
                end
                centIndex = centIndex + 1;
                if centIndex > obj.MaxNumOfCents
                    break;
                end
            else
                currmap(conn{i}(j).PixelIdxList) = 0;
                if output
                    validmap{i}(conn{i}(j).PixelIdxList) = false;
                end
           end
        end
        if centIndex > obj.MaxNumOfCents
             cents.label(r, :) = 0;
        end

    end
    if matchCents
        prevConn = currConn;
    end
    %%
     if output
        for i=1:obj.nSubjects
            reg = bwconncomp(validmap{i});
            img = labelmatrix(reg);
            flabels(img > 0) = i;
        end
        subplot(1,2,2);
        rgblbls = label2rgb(flabels, cmap, 'k');
        imshow(rgblbls);
        subplot(1,2,1);
        boundries = (imdilate(flabels ~= 0, ones(3,3)) - (flabels ~= 0)) ~= 0;
        for i=1:3
            slice = orig(:, :, i);
            slice(boundries) = 1;
            orig(:, :, i) = slice;
        end
        imagesc(orig);
        title(num2str(r));
        drawnow
    end
    %%
end
fprintf('\n');
fprintf(['# - total time: ' sec2time(toc) '\n']);
%%
if matchCents
    if realStartframe > startframe
        startframe = realStartframe;
        cents.x = cents.x(2:end, :);
        cents.y = cents.y(2:end, :);
        cents.label = cents.label(2:end, :);
        cents.area = cents.area(2:end, :);
        cents.solidity = cents.solidity(2:end, :);
        cents.prev = cents.prev(2:end, :);
        cents.logprob = cents.logprob(2:end, :, :);
    end
end
%%
cents.startframe = startframe;
cents.endframe   = endframe;
%
if ~isdesktop || (obj.Output && length(varargin) > 1)
    filename = [obj.OutputPath obj.FilePrefix '.segm.' sprintf('%03d', id) '.mat'];
    fprintf(['# - saving segmentation: "' filename '"']);
    save(filename, 'cents');
end
