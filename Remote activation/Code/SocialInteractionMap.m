function SocialInteractionMap(obj, order, N)
options.RegBeta = 2^-5;
options.ModelOrder = 3;
%%
if ~exist('order', 'var'); order = 2; end
if ~exist('N', 'var'); N = 6; end
%%
img = imread('arena.png');
obj.ROI.ZoneCenters(1, :) = [375, 385];
obj.ROI.ZoneCenters(2, :) = [239, 60];
obj.ROI.ZoneCenters(3, :) = [60, 405];
obj.ROI.ZoneCenters(4, :) = [60, 177];
obj.ROI.ZoneCenters(5, :) = [710, 513];
obj.ROI.ZoneCenters(6, :) = [752, 468];
obj.ROI.ZoneCenters(7, :) = [516, 290];
obj.ROI.ZoneCenters(8, :) = [698, 96];
obj.ROI.ZoneCenters(9, :) = [753, 157];
obj.ROI.ZoneCenters(10, :) = [213, 290];
obj.ROI.IsShelteredZone = false(1, obj.ROI.nZones);
obj.ROI.IsShelteredZone(find(cellfun(@length, regexp(obj.ROI.ZoneNames, '^\(.*\)$')))) = true;
%%
regBeta = obj.Analysis.Potts.Regularized.RegBeta;
regBeta(obj.Analysis.Potts.Regularized.order ~= options.ModelOrder) = inf;
[q, i] = min(abs(regBeta - options.RegBeta));
me = obj.Analysis.Potts.Regularized.Model{i};
f = @(x) size(x, 2); 
me.order = cellfun(f, me.labels);
absw = abs(me.weights);
absw(me.order ~= order) = -inf;
[absw, i_] = sort(absw, 2, 'descend');
cmap = MyMouseColormap;
for k=1:N
    i = i_(k);
    SquareSubplpot(N, k);
    imagesc(img); hon;
    axis off;
    l = me.labels{i};
    str = '';
    for o=1:size(l, 2)
        z = l(2, o);
        s = l(1, o);
        x = obj.ROI.ZoneCenters(z, 1) + (2 * mod(s-1, 2) - 1) * 0;
        y = obj.ROI.ZoneCenters(z, 2) + (2 * floor((s-1) / 2) - 1) * 0;
        if sum(l(2, o) == l(2, :)) > 1
            ls = sum(find(l(2, o) == l(2, :)) >= o);
            count = sum(l(2, o) == l(2, :));
            x = obj.ROI.ZoneCenters(z, 1) + (2 * mod(ls-1, 2) - 1) * 20;
            if count > 2
                y = obj.ROI.ZoneCenters(z, 2) + (2 * floor((ls-1) / 2) - 1) * 20;
            end
        end
        if obj.ROI.IsShelteredZone(z)
            marker = 's';
        else
            marker = 'o';
        end
        if me.weights(i) < 0
            plot(x, y, marker, 'MarkerFaceColor', cmap(s, :), 'MarkerEdgeColor', 'none')
        else
            plot(x, y, marker, 'MarkerEdgeColor', cmap(s, :), 'MarkerFaceColor', 'none')
        end
        str = [str, num2str(s) ':' obj.ROI.ZoneNames{z} ' '];
    end
    hoff;
    title(sprintf('weight=%.2f\n%s', me.weights(i), str));
    axis tight
end

%function obj = SocialInteractionMap(obj)
%obj = TrackEnsure(obj, {'BkgImage', 'Analysis'});
%%
return;
markerSize = 4;
%ZoneMap  = [1 7 2 3 4 8 5 10 9 6];
%ZoneMap  = [1 7 2 3 4 10 8 5 9 6];
ZoneMap  = [1 7 4 2 3 10 8 5 9 6];

issheltered = false(1, obj.ROI.nZones);
for i=1:obj.ROI.nZones
    name = regexprep(obj.ROI.ZoneNames{i}, '[()]', '');
    p = strcmp(name, obj.ROI.RegionNames);
    if ~any(p)
        m = true(size(obj.ROI.Regions{1}));
        for j=1:obj.ROI.nRegions
            m = m & ~obj.ROI.Regions{j};
        end
    else
        m = obj.ROI.Regions{p};
    end
    
    [x,y] = find(m);
    obj.ROI.ZoneCenters(i, :) = [mean(y), mean(x)];
    issheltered(i) = ~strcmp(name, obj.ROI.ZoneNames{i});
end
% obj.ROI.ZoneCenters(1, :) = [300, 300];
% obj.ROI.ZoneCenters(5, :) = obj.ROI.ZoneCenters(5, :) + [40,  -50];
% obj.ROI.ZoneCenters(8, :) = obj.ROI.ZoneCenters(8, :) + [40,  50];

obj.ROI.ZoneCenters(1, :) = [375, 385];
obj.ROI.ZoneCenters(2, :) = [340, 60];
obj.ROI.ZoneCenters(3, :) = [60, 405];
obj.ROI.ZoneCenters(4, :) = [60, 175];
obj.ROI.ZoneCenters(5, :) = [710, 513];
obj.ROI.ZoneCenters(6, :) = [752, 468];
obj.ROI.ZoneCenters(7, :) = [492, 290];
obj.ROI.ZoneCenters(8, :) = [698, 96];
obj.ROI.ZoneCenters(9, :) = [753, 157];
obj.ROI.ZoneCenters(10, :) = [224, 290];

obj.ROI.ZoneCenters(1, :) = [375, 385];
obj.ROI.ZoneCenters(2, :) = [239, 60];
obj.ROI.ZoneCenters(3, :) = [60, 405];
obj.ROI.ZoneCenters(4, :) = [60, 177];
obj.ROI.ZoneCenters(5, :) = [710, 513];
obj.ROI.ZoneCenters(6, :) = [752, 468];
obj.ROI.ZoneCenters(7, :) = [516, 290];
obj.ROI.ZoneCenters(8, :) = [698, 96];
obj.ROI.ZoneCenters(9, :) = [753, 157];
obj.ROI.ZoneCenters(10, :) = [213, 290];


%
imagesc(obj.BkgImage); hold on;
for i=1:obj.ROI.nZones
    plot(obj.ROI.ZoneCenters(i, 1), obj.ROI.ZoneCenters(i, 2), 'x');
    text(obj.ROI.ZoneCenters(i, 1), obj.ROI.ZoneCenters(i, 2),num2str(i), 'color', 'w')
end
hoff;
%%
range = 3;

b=im2double(rgb2gray(obj.BkgImage)); mn = mean(b(:)) - range * std(b(:)); mx = mean(b(:)) + range * std(b(:));
b = min(max((b-mn) / (mx - mn), 0), 1);
cmap = MyMouseColormap;
model = obj.Analysis.Potts.Model{2};
weights = model.weights(model.order == 2);

byPairs = false;
onMap = false;

%ZoneMap = [1:obj.ROI.nZones];

if onMap
    ZoneCenters = repmat(reshape(obj.ROI.ZoneCenters, [1, size(obj.ROI.ZoneCenters, 1), size(obj.ROI.ZoneCenters, 2)]), [obj.nSubjects, 1, 1]);
else
    %%
    radios = 10;
    ZoneCenters = zeros(obj.nSubjects, obj.ROI.nZones, 2);
    index = 0;
    for i=1:obj.nSubjects
        for j=ZoneMap
            a = (index / obj.ROI.nZones / obj.nSubjects * 2 * pi);
            ZoneCenters(i, j, 1) = radios * sin(a);
            ZoneCenters(i, j, 2) = radios * cos(a);
            index = index + 1;
        end
    end
end

if byPairs
    fontSize = 6;
    o = sort(weights);
    percentage = .075;
    lthresh = o(round(length(o) * percentage));
    uthresh = o(round(length(o) * (1-percentage)));
    %%
    for i=1:obj.nSubjects
        for j=i+1:obj.nSubjects
            subplot(obj.nSubjects-1, obj.nSubjects-1, (i-1)*(obj.nSubjects-1) + j - 1);
            %        imagesc(obj.BkgImage); hold on;
            imshow(img); hold on;
            %imshow(b); hold on;
            valid = [];
            marked = [];
            markedWeight = [];
            for k=1:length(model.labels)
                if any(model.labels{k}(1, :) == i) && any(model.labels{k}(1, :) == j) && (model.weights(k) >= uthresh || model.weights(k) <= lthresh) && model.order(k) == 2
                    r = [model.labels{k}(2, 1), model.labels{k}(2, 2)];
                    if r(1) == r(2)
                        %[r(1) k]
                        marked = [marked, r(1)];
                        markedWeight = [markedWeight, model.weights(k)];
                    end
                    valid = unique([valid, r]);
                    for s=1:2
                        frx = ZoneCenters(i, r(s), 1);
                        fry = ZoneCenters(i, r(s), 2);
                        tox = ZoneCenters(j, r(3-s), 1);
                        toy = ZoneCenters(j, r(3-s), 2);
                        if model.weights(k) >= uthresh
                            plot([frx frx + (tox-frx)/2], [fry fry + (toy-fry)/2], '-', 'Color', cmap(model.labels{k}(1, s), :), 'LineWidth', model.weights(k) * 1);
                        else
                            plot([frx frx + (tox-frx)/2], [fry fry + (toy-fry)/2], ':', 'Color', cmap(model.labels{k}(1, s), :), 'LineWidth', abs(model.weights(k)) * 1);
                        end
                    end
                end
            end
%             valid = setdiff(valid, marked);
%             out = valid(~issheltered(valid));
%             in = valid(issheltered(valid));
%             plot(ZoneCenters(1, out, 1), ZoneCenters(1, out, 2), 'o', 'Color', cmap(model.labels{k}(1, s), :), 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'g', 'MarkerSize', markerSize);
%             plot(ZoneCenters(1, in, 1), ZoneCenters(1, in, 2), 's', 'Color', cmap(model.labels{k}(1, s), :), 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'g', 'MarkerSize', markerSize);
%             for k=valid
%                 text(ZoneCenters(1, k, 1), ZoneCenters(1, k, 2), num2str(k), 'Color', 'b', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'fontsize', fontSize);
%             end
%             out = marked(~issheltered(marked));
            %in = marked(issheltered(marked));
            for k=1:length(marked)
                if markedWeight(k) >=0
                    plot(ZoneCenters(1, marked(k), 1), ZoneCenters(1, marked(k), 2), 'o', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', 'r', 'MarkerSize', abs(markedWeight(k)) * 1);
%                     plot(ZoneCenters(1, marked(k), 1), ZoneCenters(1, marked(k), 2), 'o', 'MarkerEdgeColor', 'r', 'MarkerFaceColor', 'r', 'MarkerSize', abs(markedWeight(k)) * 1, 'LineWidth', abs(markedWeight(k)) * 1);
                else
                    plot(ZoneCenters(1, marked(k), 1), ZoneCenters(1, marked(k), 2), 'o', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', 'b', 'MarkerSize', abs(markedWeight(k)) * 1);
%                     plot(ZoneCenters(1, marked(k), 1), ZoneCenters(1, marked(k), 2), 'o', 'MarkerEdgeColor', 'b', 'MarkerFaceColor', 'b', 'MarkerSize', abs(markedWeight(k)) * 1, 'LineWidth', abs(markedWeight(k)) * 1);
                end
            end
            %plot(ZoneCenters(1, in, 1), ZoneCenters(1, in, 2), 's', 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'r', 'MarkerSize', markerSize);
            for k=valid
                text(ZoneCenters(1, k, 1), ZoneCenters(1, k, 2), num2str(k), 'Color', 'k', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'fontsize', fontSize);
            end
            axis off;
            hoff;
            %return;
        end
    end
else % if byPairs
    %%
    markerSize = 8;
    fontSize = 18;
    
    clf
    if onMap
        imshow(b); hold on;
    end
    hold on;
    
    percentage = .02;
    weights = model.weights;
    weights(model.order ~= 2) = nan;
    [s, o] = sort(weights);
    o = o(1:sum(~isnan(s)));
    s = s(1:sum(~isnan(s)));
    
    lthresh = s(round(length(s) * percentage));
    lmax = s(1);
    
    uthresh = s(round(length(s) * (1-percentage)));
    umax = s(end);
    
    lidx = o(s <= lthresh); lidx = lidx(end:-1:1);
    uidx = o(s >= uthresh); %uidx = uidx(end:-1:1);
    
    valid = [];
    marked = [];
    for idx = [lidx, uidx]
        %%
        r = [model.labels{idx}(2, 1), model.labels{idx}(2, 2)];
        if r(1) == r(2)
            if model.weights(idx) >= uthresh
                fprintf('# pos-marked: location %d (between %d, %d)\n', r(1), model.labels{idx}(1,1), model.labels{idx}(1,2));
            else
                fprintf('# neg-marked: location %d (between %d, %d)\n', r(1), model.labels{idx}(1,1), model.labels{idx}(1,2));
            end
            marked = unique([marked, r(1)]);
        end
        valid = unique([valid, r]);
        for s=1:2
            frx = ZoneCenters(model.labels{idx}(1, s), r(s), 1);
            fry = ZoneCenters(model.labels{idx}(1, s), r(s), 2);
            tox = ZoneCenters(model.labels{idx}(1, 3-s), r(3-s), 1);
            toy = ZoneCenters(model.labels{idx}(1, 3-s), r(3-s), 2);
            if model.weights(idx) >= uthresh
                w = (model.weights(idx) - uthresh) / (umax - uthresh) * 9 + 1;
                plot([frx frx + (tox-frx)/2], [fry fry + (toy-fry)/2], '-', 'Color', cmap(model.labels{idx}(1, s), :), 'LineWidth', w);
            else
                w = (model.weights(idx) - lthresh) / (lmax - lthresh) * 9 + 1;
                plot([frx frx + (tox-frx)/2], [fry fry + (toy-fry)/2], ':', 'Color', cmap(model.labels{idx}(1, s), :), 'LineWidth', w);
            end
        end
    end
    if onMap
        valid = setdiff(valid, marked);
        out = valid(~issheltered(valid));
        in = valid(issheltered(valid));
        plot(ZoneCenters(1, out, 1), ZoneCenters(1, out, 2), 'o', 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'g', 'MarkerSize', markerSize);
        plot(ZoneCenters(1, in, 1), ZoneCenters(1, in, 2), 's', 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'g', 'MarkerSize', markerSize);
        for k=valid
            text(ZoneCenters(1, k, 1), ZoneCenters(1, k, 2), num2str(k), 'Color', 'b', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'fontsize', fontSize);
        end
        
        out = marked(~issheltered(marked));
        in = marked(issheltered(marked));
        plot(ZoneCenters(1, out, 1), ZoneCenters(1, out, 2), 'o', 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'r', 'MarkerSize', markerSize);
        plot(ZoneCenters(1, in, 1), ZoneCenters(1, in, 2), 's', 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'r', 'MarkerSize', markerSize);
        for k=marked
            text(ZoneCenters(1, k, 1), ZoneCenters(1, k, 2), num2str(k), 'Color', 'k', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'fontsize', fontSize);
        end
        %return;
        axis off;
        hoff;
    else
        zmap = MyZonesColormap;
        weightsIndep = model.weights(model.order == 1);
        labelsIndep = model.labels(model.order == 1);
        labelsIndep = [labelsIndep{:}];
        for i=1:obj.nSubjects
            for j=1:obj.ROI.nZones
                if j==model.neutralZone
                    continue;
                end
                w = weightsIndep(labelsIndep(1, :) == i & labelsIndep(2, :) == j);
                currMarkerSize = abs(markerSize * w);
                currMarkerSign = sign(markerSize * w);
                %if ~issheltered(j)
                if currMarkerSign > 0
                    plot(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2), 'o', 'MarkerEdgeColor', zmap(j, :), 'MarkerFaceColor', 'w', 'MarkerSize', currMarkerSize, 'LineWidth', 2);
                    currMarkerSize
                    if currMarkerSize < 10
                        text(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2),num2str(find(ZoneMap == j)), 'color', 'w', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center')
                    else
                        text(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2),num2str(find(ZoneMap == j)), 'color', zmap(j, :), 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center')
                    end
                else
                    currMarkerSize
                    plot(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2), 'o', 'MarkerEdgeColor', zmap(j, :), 'MarkerFaceColor', zmap(j, :), 'MarkerSize', currMarkerSize, 'LineWidth', 2);
                    if currMarkerSize < 10
                        text(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2),num2str(find(ZoneMap == j)), 'color', 'w', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center')
                    else
                        text(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2),num2str(find(ZoneMap == j)), 'color', 'w', 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center')
                    end
                end
                %else
                %    plot(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2), 's', 'MarkerEdgeColor', cmap(i, :), 'MarkerFaceColor', zmap(j, :), 'MarkerSize', currMarkerSize);
                %end
            end
        end
        axis off;
        hoff;
        set(gcf, 'Color', 'w');
        axis equal
    end
    %%
    %     o = sort(weights);
    %     lthresh = o(round(length(o) * percentage));
    %     uthresh = o(round(length(o) * (1-percentage)));
    %
    %     clf
    %     imshow(b); hold on;
    %     valid = [];
    %     marked = [];
    %     for i=1:obj.nSubjects
    %         for j=i+1:obj.nSubjects
    %             for k=1:length(model.labels)
    %                 if any(model.labels{k}(1, :) == i) && any(model.labels{k}(1, :) == j) && (model.weights(k) >= uthresh || model.weights(k) <= lthresh)
    %                     r = [model.labels{k}(2, 1), model.labels{k}(2, 2)];
    %                     if r(1) == r(2)
    %                         marked = unique([marked, r(1)]);
    %                     end
    %                     valid = unique([valid, r]);
    %                     for s=1:2
    %                         frx = ZoneCenters(r(s), 1);
    %                         fry = ZoneCenters(r(s), 2);
    %                         tox = ZoneCenters(r(3-s), 1);
    %                         toy = ZoneCenters(r(3-s), 2);
    %                         if model.weights(k) >= uthresh
    %                             plot([frx frx + (tox-frx)/2], [fry fry + (toy-fry)/2], '-', 'Color', cmap(model.labels{k}(1, s), :), 'LineWidth', 1);
    %                         else
    %                             plot([frx frx + (tox-frx)/2], [fry fry + (toy-fry)/2], ':', 'Color', cmap(model.labels{k}(1, s), :), 'LineWidth', 1);
    %                         end
    %                     end
    %                 end
    %             end
    %         end
    %     end
    %     valid = setdiff(valid, marked);
    %     out = valid(~issheltered(valid));
    %     in = valid(issheltered(valid));
    %     plot(ZoneCenters(out, 1), ZoneCenters(out, 2), 'o', 'Color', cmap(model.labels{k}(1, s), :), 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'g', 'MarkerSize', markerSize);
    %     plot(ZoneCenters(in, 1), ZoneCenters(in, 2), 's', 'Color', cmap(model.labels{k}(1, s), :), 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'g', 'MarkerSize', markerSize);
    %     for k=valid
    %         text(ZoneCenters(k, 1), ZoneCenters(k, 2), num2str(k), 'Color', 'b', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'fontsize', fontSize);
    %     end
    %
    %     out = marked(~issheltered(marked));
    %     in = marked(issheltered(marked));
    %     plot(ZoneCenters(out, 1), ZoneCenters(out, 2), 'o', 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'r', 'MarkerSize', markerSize);
    %     plot(ZoneCenters(in, 1), ZoneCenters(in, 2), 's', 'MarkerEdgeColor', 'w', 'MarkerFaceColor', 'r', 'MarkerSize', markerSize);
    %     for k=marked
    %         text(ZoneCenters(k, 1), ZoneCenters(k, 2), num2str(k), 'Color', 'k', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle', 'fontsize', fontSize);
    %     end
    %     %return;
    %     axis off;
    %     hoff;
end

if (1==1)
    %%
    p = 'Graphs/SocialInteractionMap/';
    mkdir(p);
    figure(1);
    saveFigure([p '' obj.FilePrefix '']);
end