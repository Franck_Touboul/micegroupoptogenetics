function TrackMetaSegment(nruns, id)
%%
fprintf('# segmenting movie frames:\n');
if ~license('checkout', 'image_toolbox')
    fprintf('# - waiting for image processing toolbox license\n');
    while ~license('checkout', 'image_toolbox'); pause(5); end
end
TrackDefaults;
%%
fprintf('# segmenting frames\n');
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nFrames = xyloObj.NumberOfFrames;
dt = 1/xyloObj.FrameRate;
%%
fprintf('# - loading meta data\n');
filename = [options.output_path options.test.name '.meta.mat'];
waitforfile(filename);
load(filename);

%%
if nargin == 0
    startframe = 14600;
    endframe = nFrames;
else
    step = floor(nFrames / nruns);
    curr = 0;
    for i=1:id
        prev = curr + 1;
        if i == nruns
            curr = nFrames;
        else
            curr = prev + step - 1;
        end
    end
    startframe = prev;
    endframe = curr;
end
nFrames = endframe - startframe + 1;
%%
prevProps = [];
sx = []; sy = [];

cents.x = zeros(nFrames, options.maxNumCents);
cents.y = zeros(nFrames, options.maxNumCents);
cents.label = zeros(nFrames, options.maxNumCents, 'uint8');
cents.area = zeros(nFrames, options.maxNumCents, 'uint16');
cents.logprob = zeros(nFrames, options.maxNumCents, options.nSubjects);
%%
doubleBkgFrame = im2double(meta.bkgFrame);
bkgNoise = std(doubleBkgFrame(:));
cmap = meta.subject.centerColors;
%%
nchars = RePrintf('# - frame %6d [%d-%d] (%6.2fxiRT)', startframe, startframe, endframe, 0); 
tic;
epochStart = -1;
for r=1:nFrames
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d [%d-%d] (%6.2fxiRT)', r+startframe-1, startframe, endframe, RT);
    currTime = (r+startframe-1) * dt;
    if isfield(options, 'movieStartTime') && currTime < options.movieStartTime;
        continue;
    end
    if isfield(options, 'movieEndTime') && options.movieEndTime > 0 && currTime > options.movieEndTime
        continue;
    end
    
    m = myMMReader(options.MovieFile, r+startframe-1);
    if options.output
        orig = m;
    end
    m = imsubtract(m, meta.bkgFrame);
    m = imresize(m, options.scale);
    
    if isempty(sx)
        [sx, sy, nc] = size(m);
    end
    m = im2double(m);
%    m = (m > options.noiseThresh * std(m(:))) .* m;
    m = (m > options.noiseThresh * bkgNoise) .* m;
    %%
    hsv_m = rgb2hsv(m);
    hm = hsv_m(:,:,1);
    sm = hsv_m(:,:,2);
    vm = hsv_m(:,:,3);
    %%
    bw = sum(m > 0, 3);
    bw = bwareaopen(bw, options.minNumPixels);
    labels = bwlabel(bw);
    count = max(labels(:));
    %labels = bwlabel(bw);
    %nobjects = max(labels(:));
    %%
    props = regionprops(labels, 'Centroid', 'PixelList', 'Solidity');
    for i=1:length(props)
        if props(i).Solidity < options.solidity || sum(labels(:) == i) < options.minNumPixels
            labels(labels == i) = 0;
            count = count - 1;
        end
    end
    if count < max(options.minObjects, 1)
        if epochStart > 0
            randFrameIdx = floor((r - epochStart + 1) * rand(1));
            m = myMMReader(options.MovieFile, r+startframe-1+randFrameIdx);
            unique = hash(randi(1000000), 'MD5');
            filename = [options.output_path options.test.name '.' unique '.sample.png'];
            imwrite(m, filename);
            count = 0;
            RePrintf(0, ['\n# writing frame to ' filename '\n']);
            epochStart = -1;
        end
        continue;
    end
    if epochStart < 0
        epochStart = r;
    end
    %%
end
fprintf('\n');
fprintf(['# - total time: ' sec2time(toc) '\n']);
%
cents.startframe = startframe;
cents.endframe   = endframe;
%
if nargin == 0
    filename = [options.output_path options.test.name '.segm.mat'];
else
    filename = [options.output_path options.test.name '.segm.' sprintf('%03d', id) '.mat'];
end
fprintf(['# - saving segmentation: "' filename '"']);
save(filename, 'cents');

