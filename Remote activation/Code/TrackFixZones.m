function obj = TrackFixZones(obj)
res.x = obj.x;
res.y = obj.y;
res.sheltered = obj.sheltered;
obj.valid = all(~isnan(obj.x));
res.x(:, ~obj.valid) = 1;
res.y(:, ~obj.valid) = 1;
res.sheltered(:, ~obj.valid) = false;

for s=1:obj.nSubjects
    obj.ROI.ZoneNames{1} = 'Open';
    index = 2;
    for r=1:obj.ROI.nRegions
        res.regions(s, obj.ROI.Regions{r}(sub2ind(size(obj.ROI.Regions{r}), round(res.y(s, :)), round(res.x(s, :))))) = r;
        res.regions(s, ~obj.valid) = 0;
        if s==1; obj.ROI.ZoneNames{index} = obj.ROI.RegionNames{r}; end
        res.zones(s, res.regions(s, :) == r & ~res.sheltered(s, :)) = index;
        if obj.ROI.IsSheltered(r)
            index = index + 1;
            if s==1; obj.ROI.ZoneNames{index} = ['(' obj.ROI.RegionNames{r} ')']; end
            res.zones(s, res.regions(s, :) == r & res.sheltered(s, :)) = index;
        end
        index = index + 1;
    end
end
obj.zones = res.zones;
obj.nValid = sum(obj.valid);
obj.nFrames = length(obj.valid);