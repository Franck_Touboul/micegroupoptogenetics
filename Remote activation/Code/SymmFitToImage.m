function SymmFitToImage(Depth, map, opt)

if nargin < 4
    opt.HeightThresh = 0.01;
end

%%
% depth
prop = regionprops(map, 'PixelIdxList', 'Centroid', 'MajorAxis');
cmDX = Depth.X(round(prop.Centroid(2)), round(prop.Centroid(1)));
cmDY = Depth.Y(round(prop.Centroid(2)), round(prop.Centroid(1)));

dx = Depth.X(prop.PixelIdxList) - cmDX;
dy = Depth.Y(prop.PixelIdxList) - cmDY;

Cxx = mean(dx.^2);
Cyy = mean(dy.^2);
Cxy = mean(dx.*dy);
C = [Cxx, Cxy; Cxy, Cyy];
pC = pcacov(C);

distToOrth = [dx, dy] * pC;
imx = zeros(1, 2); mx = zeros(1, 2);
imn = zeros(1, 2); mn = zeros(1, 2);
for i=1:2
    [~, imx(i)] = max(abs(distToOrth(:, i)));
    mx(i) = distToOrth(imx(i), i);
    [~, imn(i)] = max(-distToOrth(:, i) * sign(mx(i)));
    mn(i) = distToOrth(imn(i), i);
end

Res.Depth.MajorPt1 = prop.PixelIdxList(imx(1));
Res.Depth.MajorPt2 = prop.PixelIdxList(imn(1));
Res.Depth.MajorAxis = mx(1) - mn(1);
Res.Depth.Orientation = atan2(pC(2, 1), pC(1, 1));
Res.Depth.MaxHeight = max(Depth.Z(map));

Res.Depth.MinorPt1 = prop.PixelIdxList(imx(2));
Res.Depth.MinorPt2 = prop.PixelIdxList(imn(2));
Res.Depth.MinorAxis = mx(2) - mn(2);

%%
pos = RotMatRz(pi-Res.Depth.Orientation) * [Depth.X(:), Depth.Y(:), Depth.Z(:)]';
X = reshape(pos(1, :), size(Depth.X));
Y = reshape(pos(2, :), size(Depth.X));

%  model
vX = X(map);
vY = Y(map);
X1 = X(Res.Depth.MajorPt2);
Y1 = Y(Res.Depth.MajorPt2);
X2 = X(Res.Depth.MajorPt1);
Y2 = Y(Res.Depth.MajorPt1);
initVec = [X1, Y1, abs(Res.Depth.MajorAxis), [atan2(Y2-Y1, X2-X1), zeros(1, 4)]];
%SymmModel(vX, vY, initVec);
%hon
%plot(X2, Y2, 'x', 'MarkerSize', 10);
%hoff
%
SkinDistance = SymmModel(vX, vY, initVec);

func = @(v) SymmModelFit(vX, vY, v);
tic; [vec, d, eflag] = patternsearch(func, initVec, [], [], [], [], [], [], [], psoptimset('TolFun', 1e-4)); toc
d
SymmModel(vX, vY, vec);