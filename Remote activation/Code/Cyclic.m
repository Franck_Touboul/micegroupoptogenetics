function r = Cyclic(offset, count)
d = [];
if length(count) > 1
    d = count;
    count = length(d);
end
offset = round(offset);
offset = sign(offset) * mod(abs(offset), count);
    
if offset >= 0
    r = [1 + offset:count, 1:offset];
else
    r = [count + offset + 1:count, 1:count + offset];
end
if ~isempty(d)
    r = d(r);
end