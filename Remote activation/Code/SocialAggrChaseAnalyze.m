SocialExperimentData;
%%
for id=1:GroupsData.nExperiments
    %%
    obj = TrackLoad({id, 2}, {'Hierarchy'});
    if id == 1
        data = obj.Hierarchy.Group;
    else
        data(id) = obj.Hierarchy.Group;
    end
end
clf
%%
days = 1:GroupsData.nDays;
clf
subplot(3,1,1:2);
for id=1:GroupsData.nExperiments
    pC = [];
    pE = [];
    curr.nContacts = sum(cat(3, data(id).AggressiveChase.Days(days).Contacts), 3);
    curr.nCE = sum(cat(3, data(id).AggressiveChase.Days(days).ChaseEscape), 3);
    pC = sum(curr.nCE, 2) ./ sum(curr.nContacts, 2) * 100;
    pE = sum(curr.nCE, 1) ./ sum(curr.nContacts, 1) * 100;
    [status, name] = SocialRankToStatus(data(id).AggressiveChase.rank);
    for s=1:obj.nSubjects
        plot(pC(s), pE(s), 'o', 'MarkerFaceColor', GroupsData.Colors(GroupsData.group(id), :), 'MarkerEdgeColor', 'none', 'MarkerSize', 10); hon;
        text(pC(s), pE(s), name{s}, 'HorizontalAlignment', 'center', 'verticalAlignment', 'middle', 'color', 'w')
        for r=find(status == status(s) - 1)
            plot([pC(s) pC(r)], [pE(s) pE(r)], '--', 'color', GroupsData.Colors(GroupsData.group(id), :));
        end
    end
end
xlabel('% contants that ended in a chase');
ylabel('% contants that ended in an escape');
xaxis(0, 35)
hoff;