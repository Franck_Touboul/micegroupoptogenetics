function vec = SocialPottsVector(obj, me)
%me = obj.Analysis.Potts.Model{2};
res = PottsModelExpand(obj, me);
vec = [];
for s=1:obj.nSubjects
    map = res.subjectmap(s, :) & me.order == 2;
    idx = find(map);
    data = zeros(obj.ROI.nZones, obj.ROI.nZones);
    seq = 1:obj.nSubjects;
    for j=1:length(idx)
        i = idx(j);
        z = res.zones(seq ~= s, i);
        z = z(z > 0);
        data(res.zones(s, i), z) = data(res.zones(s, i), z) + me.weights(i);
    end
    vec(s, :) = data(:)';
end


return;
%% mean weights
model = obj.Analysis.Potts.Model{2};
vec = [];
for i=1:obj.nSubjects
    v = zeros(obj.ROI.nZones);
    v_ = cell(obj.ROI.nZones);
    for l=1:length(model.labels)
        idx = model.labels{l}(1, :) == i;
        if model.order(l) == 2 && any(idx)
            r = model.labels{l}(2, idx);
            c = model.labels{l}(2, ~idx);
            v(r, c) = v(r, c) + model.weights(l);
            v_{r, c} = [v_{r,c}, model.weights(l)];
        end
    end
    
%     for j=1:obj.ROI.nZones
%         for k=1:obj.ROI.nZones
%             v(j,k) = var(v_{j, k});
%         end
%     end
    vec = [vec; v(:)'];
end

return;
%% all weights
model = obj.Analysis.Potts.Model{2};
v_ = cell(obj.ROI.nZones);
vec = [];
for i=1:obj.nSubjects
    v = [];
    idx = 1;
    for l=1:length(model.labels)
        match = model.labels{l}(1, :) == i;
        if model.order(l) == 2 && any(match)
            v(idx) = model.weights(l);
            idx = idx + 1;
        end
    end
    vec = [vec; v(:)'];
end
%% self weights
model = obj.Analysis.Potts.Model{2};
vec = [];
for i=1:obj.nSubjects
    v = [];
    idx = 1;
    for l=1:length(model.labels)
        match = model.labels{l}(1, :) == i;
        if model.order(l) == 1 && any(match)
            v(idx) = model.weights(l);
            idx = idx + 1;
        end
    end
    vec = [vec; v(:)'];
end

%%
% vec = [];
% for s=1:obj.nSubjects
%     for i=1:obj.ROI.nZones
%         for j=1:obj.ROI.nZones
%         end
%     end
% end