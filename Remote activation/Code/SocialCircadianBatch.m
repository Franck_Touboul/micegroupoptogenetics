SocialExperimentData
stat = struct;
stat.Djs = [];
for id = 11
    for day=1:GroupsData.nDays
        %%
        obj = TrackLoad({id day});
        %%
        map = obj.Circadian.Djs;
        d = diag(obj.Circadian.Validations.InterlacedMin.DJS);
        map(d ~= 0) = diag(d);
        stat.Djs = cat(3, stat.Djs, map);
        %
        subplot(2,2,day);
        imagesc(map);
    end
end