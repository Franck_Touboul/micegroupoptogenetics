function zones = SimulateIndepProcess(obj, subj)
if ~exist('subj', 'var')
    subj=1:obj.nSubjects;
end
zones = obj.zones;
for s=subj
    z = obj.zones(s, :);
    %%
    h = hist(z, 1:obj.ROI.nZones);
    p = h / sum(h);
    cp = cumsum(p);
    r = rand(1, obj.nFrames);
    sim = zeros(1, obj.nFrames);
    for i=1:obj.ROI.nZones
        sim(sim == 0 & r < cp(i)) = i;
    end
    zones(s, :) = sim;
end