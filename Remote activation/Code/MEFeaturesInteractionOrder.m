function me = MEFeaturesInteractionOrder(data, nvals, n)
MEPrint('Computing features with interaction order\n');
options.NeutralValue = 1;
%%
[ndata, dim] = size(data);
me.nData = ndata;
me.Dim = dim;
me.nVals = nvals;
%%
me.SubjectPerms = nchoose(1:dim);
me.SubjectPerms = me.SubjectPerms(cellfun(@length, me.SubjectPerms) <= n);
me.InputPerms = myPerms(dim, nvals);
%%
me.nPatterns = sum(nvals .^ cellfun(@length, me.SubjectPerms));
me.Patterns = nan(me.nPatterns, dim);
offset = 0;
for s=1:length(me.SubjectPerms)
    local = myPerms(length(me.SubjectPerms{s}), nvals);
    me.Patterns(offset+1:offset+size(local, 1), me.SubjectPerms{s}) = local;
    offset = offset + size(local, 1);
end
%%
me.Features = MEFindPatterns(me.InputPerms, me.Patterns);
me.PatternHist = MEPriors(data, me.Patterns);
%%
Redundent = any(me.Patterns == options.NeutralValue, 2);
me.Patterns = me.Patterns(~Redundent, :);
me.Features = me.Features(:, ~Redundent);
me.PatternHist = me.PatternHist(~Redundent);
me.nPatterns = size(me.Patterns, 1);
