defaults.PredObjectLength_cm = 20;
defaults.PreyObjectLength_cm = 20;
defaults.ZoneOfProximity_cm = 12;
defaults.ZoneOfContact_cm = 6;

defaults.MinSubInteractionDuration = 6;
defaults.MinIdleDuration = 8;
defaults.MinContanctDuration = 6; % minimal duration of a contact
defaults.MaxShelteredContanctDuration = 25;

defaults.MinSpeed_cm2sec = 0;
defaults.SmoothSpan = 5;

defaults.MinNumOfContacts = 3;
defaults.RelativeSpeedInChase = .5;

defaults.EmissionMatch = .5;
defaults.EmissionMismatch = .2;
defaults.ContactMismatch = 0;

defaults.ChaseMinOverlap = 6;
defaults.ChaseMinOverlapPercentage = 0;

arg = struct();
f = fieldnames(defaults);
for i=1:length(f)
    arg.(f{i}) = defaults.(f{i})(randi(length(defaults.(f{i}))));
end
% arg.EmissionMismatch = defaults.EmissionMismatch(randi(sum(defaults.EmissionMismatch < arg.EmissionMatch)));

f = fieldnames(arg);
for i=1:length(f)
    fprintf('@%s=%f\n', f{i}, arg.(f{i}));
end

obj.OutputToFile = false;
obj = SocialPredPreyModel(obj, arg);
SocialOptimizeScore(obj);