function obj = SocialFilterZones(obj, MinNumOfFrames)
%%
nzones = obj.zones;
for s=1:obj.nSubjects
    for z=1:obj.ROI.nZones
        [begF, endF, len] = FindEvents(obj.zones(s, :) == z);
        for i=find(len < MinNumOfFrames)
            nzones(s, begF(i):endF(i)) = -1;
        end
    end
end
%%
for s=1:obj.nSubjects
    [begF, endF, len] = FindEvents(nzones(s, :) == -1);
    for i=1:length(begF)
        if begF(i) > 1
            e1 = nzones(s, begF(i)-1);
        else
            e1 = 1;
        end
            
        if endF(i) < obj.nFrames
            e2 = nzones(s, endF(i)+1);
        else
            e2 = 1;
        end
        nzones(s, begF(i):begF(i) + round(len/2) - 1) = e1;
        nzones(s, begF(i) + round(len/2):endF(i)) = e2;
    end
end
%%
obj.zones = nzones;