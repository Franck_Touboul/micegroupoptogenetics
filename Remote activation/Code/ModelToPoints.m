function Points = ModelToPoints(Model)
Points = struct();
P0 = [0; 0; 0];

Points.Skin.X = zeros(Model.NumVerteb, Model.Resolution);
Points.Skin.Y = zeros(Model.NumVerteb, Model.Resolution);
Points.Skin.Z = zeros(Model.NumVerteb, Model.Resolution);
Points.Skin.BoneID = zeros(Model.NumVerteb, Model.Resolution, 'uint8');
Points.Skin.SkinID = zeros(Model.NumVerteb, Model.Resolution, 'uint8');
Points.Skin.Type = zeros(Model.NumVerteb, Model.Resolution, 'uint8');

Points.Bone.X = zeros(Model.NumVerteb, 2);
Points.Bone.Y = zeros(Model.NumVerteb, 2);
Points.Bone.Z = zeros(Model.NumVerteb, 2);

Joints = cell(1, length(unique(Model.Types(:)'))+1);
for i=unique(Model.Types(:)')
    Joints{i+1} = [];
end
    
sidx = 1;
skinrot = eye(3);
MundiMat = RotMat(Model.AxisMundi.Yaw, Model.AxisMundi.Pitch, Model.AxisMundi.Roll) * Model.AxisMundi.Scale;
for i=1:Model.NumVerteb
    Bone = Model.Bones(i);
    tid = Model.Types(i);
    if isempty(Joints{tid+1})
        dep = Model.Dependence(i);
        if dep >= 0
            Joints{tid + 1} = Joints{dep + 1} * RotMat(Bone.Yaw, Bone.Pitch, Bone.Roll);
        else
            Joints{tid + 1} = RotMat(Bone.Yaw, Bone.Pitch, Bone.Roll);
        end
    else
        Joints{tid + 1} = Joints{tid + 1} * RotMat(Bone.Yaw, Bone.Pitch, Bone.Roll);
    end
    rot = Joints{tid + 1};
    if i < Model.NumVerteb
        frwdrot = rot * RotMat(Model.Bones(i+1).Yaw, Model.Bones(i+1).Pitch, Model.Bones(i+1).Roll);
    end
    if Bone.Memory
        if i==1
            skinrot = (rot + frwdrot) / 2;
        else
            skinrot = Model.SkinMemory * skinrot + (1 - Model.SkinMemory) * (rot + frwdrot) / 2;
        end
    else
        skinrot = rot;
    end
    P1 = P0 + rot * [Bone.Length; 0; 0];
    %%
    rP0 = MundiMat * P0;
    rP1 = MundiMat * P1;
    Points.Bone.X(i, 1) = rP0(1);
    Points.Bone.Y(i, 1) = rP0(2);
    Points.Bone.Z(i, 1) = rP0(3);
    Points.Bone.X(i, 2) = rP1(1);
    Points.Bone.Y(i, 2) = rP1(2);
    Points.Bone.Z(i, 2) = rP1(3);
    %%
    for j=1:length(Bone.Skin)
        Skin = Bone.Skin(j);
        S = P0 + skinrot * ([0; sin(Skin.Angle); cos(Skin.Angle) ] * Skin.Distance);
        Points.Skin.BoneID(i, j) = i;
        Points.Skin.SkinID(i, j) = j;
        Points.Skin.Type(i, j) = Model.Types(i);
        rS = MundiMat * S;
        Points.Skin.X(i, j) = rS(1);
        Points.Skin.Y(i, j) = rS(2);
        Points.Skin.Z(i, j) = rS(3);
        sidx = sidx + 1;
    end
    %%    
    P0 = P1;
end

X = Points.Skin.X;
Y = Points.Skin.Y;
e1 = convn(X(:, 1:end-1) .* Y(:, 2:end) - X(:, 2:end) .* Y(:, 1:end-1), [1;  -1], 'valid');
e2 = convn(X(1:end-1, :) .* Y(2:end, :) - X(2:end, :) .* Y(1:end-1, :), [-1,  1], 'valid');
v = e1 + e2;
v(:, end+1) = v(:, end);
v(end+1, :) = v(end, :);
Points.Skin.Area = v;

if nargout == 0
    surfl(Points.Skin.X,Points.Skin.Y,1-Points.Skin.Z); 
end

