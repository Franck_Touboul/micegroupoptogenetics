function [features, valid, segmented, begF, endF] = PPModelComputeFeatures_v2(obj, m1, m2, minspeed)
%%
obj.MaxShelteredContanctDuration = 25;
%obj.MinSpeedRatio = .005;
%%
invalid = obj.sheltered(m1, :) | obj.sheltered(m2, :);
[start, finish, len] = FindEvents(invalid);
for i=find(len <= obj.MaxShelteredContanctDuration)
    invalid(start(i):finish(i)) = 0;
end
valid = ~invalid;
%%
currAngles = obj.angle{m1, m2};
currAngles(obj.speed(m1, :) < minspeed) = nan;
currAngles(isnan(currAngles)) = rand(1, sum(isnan(currAngles))) * pi;

%currAngles = tan(obj.angle{m1, m2}) .* obj.Interactions.distance{m1, m2};


% setting random angles to nans
% nNan = sum(isnan(currAngles));
% currAngles(isnan(currAngles)) = pi * rand(1, nNan);
%
currContacts = obj.contact{min(m1, m2), max(m1, m2)};
%
currSpeed = log(obj.speed(m1, :));
finiteSpeed = currSpeed(isfinite(currSpeed));
currSpeed(~isfinite(currSpeed)) = finiteSpeed(randi(length(finiteSpeed), 1, sum(~isfinite(currSpeed))));
%
features = [...
    currAngles;
    currContacts;
    currSpeed;
    ];

%%
if nargout > 2
    [begF, endF] = FindEvents(valid);
    segmented = cell(1, length(begF));
    for i=1:length(begF)
        segmented{i} = features(:, begF(i):endF(i));
    end
end
