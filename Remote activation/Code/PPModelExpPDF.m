function p = PPModelExpPDF(x, model, state)

p = zeros(1, size(x, 2));

if nargin > 2
    curr = model.states(state);
else
    curr = model.all;
end
valid = (~curr.check | x(2, :) == curr.contact) & ~isnan(x(1, :));
input = x(1, :);

p(valid & input>0) = exppdf(input(valid & input>0), curr.pmu) * curr.weights(1);
p(valid & input<0) = exppdf(abs(input(valid & input<0)), curr.nmu) * curr.weights(2);
