function [obj, meta] = SocialTrainSimulatedRegularizedPotts(srcobj, options)
inParallel = isunix;
srcobj = TrackLoad(srcobj);
nPoints = srcobj.nFrames;
cm = cumsum(srcobj.Analysis.Potts.Model{srcobj.nSubjects}.prob);
r = rand(1, nPoints);
zones = zeros(srcobj.nSubjects, nPoints);
for i=1:nPoints
    zones(:, i) = srcobj.Analysis.Potts.Model{srcobj.nSubjects}.inputPerms(find(r(i) < cm, 1), :)';
end

%zones = srcobj.zones;

obj = srcobj;
obj = rmfield(obj, 'Analysis');
obj.zones = zones;
obj.nFrames = nPoints;
obj.valid = true(1, nPoints);
%%
local = obj;
local.OutputToFile = false;
local.OutputInOldFormat = false;
local = SocialPotts(local, struct('InParallel', true));
obj.Analysis.Potts = local.Analysis.Potts;
local = [];
%%
if ~exist('options', 'var')
    options = struct();
end

options = setDefaultParameters(options, ...
    'order', 1:obj.nSubjects,...
    'nIters', [500 1000 20000],...
    'confidence', 0.05,...
    'betas', .5 .^ (0:12));
%%
trainobj = obj;
testobj = obj;
%%
options.betas = unique(options.betas(options.betas ~= 0));
me = cell(1, obj.nSubjects);
meta = [];
for idx = 1:obj.nSubjects
    me{idx} = obj.Analysis.Potts.Model{idx};
    me{idx}.RegBeta = 0;
    meta.RegBeta(idx) = 0;
    meta.order(idx) = idx;
end

%%
data = trainobj.zones(:, obj.valid) - 1;
%%
for level=1:obj.nSubjects
    p = exp(me{level}.perms * me{level}.weights');
    perms_p = exp(me{level}.perms * me{level}.weights');
    Z = sum(perms_p);
    p = p / Z;
    me{level}.prob = p;
end
%%
idx = obj.nSubjects + 1;
if inParallel
    %%
    h = {};
    hidx = 1;
    for level=1:obj.nSubjects
        %%
        for beta = options.betas
            fprintf('#    - beta %.4f (log2 = %.2f)\n', beta, log2(beta));
            fprintf('#      . level %d\n', level);
            local = trainobj;
            local.n = level;
            local.beta = beta;
            local.output = true;
            local.count = trainobj.ROI.nZones;
            local.nIters = options.nIters(3);
            local.MinNumberOfIters = 5;
            local.initialPottsWeights = me{level}.weights;
            local.KLConvergenceRatio = 1e-4;
            local.Confidence = options.confidence;
            local.OutputToFile = false;
            local.OutputInOldFormat = false;
            %         me{idx} = trainPottsModelUsingPlummet(local, data, options.confidence);
            %         local.initialPottsWeights = me{idx}.weights;
            %me{idx} = trainPottsModelUsingSummet(local, data, options.confidence);
            h{hidx} = Job(local);
            h{hidx}.Run(@BksPottsUsingSUMMET);
            hidx = hidx + 1;
        end
    end

    idx = 1;
    for level=1:obj.nSubjects
        %%
        for beta = options.betas
            h{idx}.WaitUntilFinished;
            curr = h{idx}.Object;
            me{idx} = curr.Analysis.RPotts;

            p = exp(me{idx}.perms * me{idx}.weights');
            Z = sum(p);
            p = p / Z;
            me{idx}.prob = p;
            me{idx}.RegBeta = beta;
            
            meta.RegBeta(idx) = beta;
            meta.order(idx) = level;
            
            idx = idx + 1;

        end
    end
else
    %%
    for level=1:obj.nSubjects
        %%
        for beta = options.betas
            fprintf('#    - beta %.4f (log2 = %.2f)\n', beta, log2(beta));
            fprintf('#      . level %d\n', level);
            local = trainobj;
            local.n = level;
            local.beta = beta;
            local.output = true;
            local.count = trainobj.ROI.nZones;
            local.nIters = options.nIters(3);
            local.MinNumberOfIters = 5;
            local.initialPottsWeights = me{level}.weights;
            local.KLConvergenceRatio = 1e-4;
            %         me{idx} = trainPottsModelUsingPlummet(local, data, options.confidence);
            %         local.initialPottsWeights = me{idx}.weights;
            %me{idx} = trainPottsModelUsingSummet(local, data, options.confidence);
            me{idx} = trainPottsModelUsingSUMMET(local, data, options.confidence);
            p = exp(me{idx}.perms * me{idx}.weights');
            Z = sum(p);
            p = p / Z;
            me{idx}.prob = p;
            me{idx}.RegBeta = beta;
            
            meta.RegBeta(idx) = beta;
            meta.order(idx) = level;
            
            idx = idx + 1;
        end
    end
    
end
    
%%
meta.Model = me;
[trainobj, ~, jointProbs] = SocialComputePatternProbs(trainobj, false);
meta.trainProbs = jointProbs;
[testobj, ~, testProbs] = SocialComputePatternProbs(testobj, false);
meta.testProbs = testProbs;
meta.nTrainFrames = trainobj.nFrames;
meta.nTestFrames = testobj.nFrames;
obj.Analysis.Potts.Regularized = meta;
%%
reme = obj.Analysis.Potts.Regularized;
v = reme.order == obj.nSubjects;
idx = 1;
Djs = [];
beta = [];
for i=find(v)
    p1 = reme.Model{i}.prob;
    p2 = srcobj.Analysis.Potts.Model{obj.nSubjects}.prob;
    Djs(idx) = JensenShannonDivergence(p1(:)', p2(:)');
    beta(idx) = reme.RegBeta(i);
    idx = idx + 1;
end
PrintStrudel('Djs', Djs)
PrintStrudel('beta', beta)
code = sprintf('%06d', randi(1e6)); %hash(randi(2^12), 'MD5');
WriteStrudel(struct('Djs', Djs, 'beta', beta), ['Strudels/' obj.FilePrefix '.' code '.sml']);

% if srcobj.OutputToFile
%     if ~isfield(srcobj, 'Simulated')
%         srcobj.Simulated(1) = obj.Analysis.Potts;
%     else
%         srcobj.Simulated(end + 1) = obj.Analysis.Potts;
%     end
%     fprintf('# - saving data\n');
%     TrackSave(srcobj);
% end

