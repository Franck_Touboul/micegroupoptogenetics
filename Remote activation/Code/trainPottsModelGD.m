function me = trainPottsModelGD(options, data, confidence)

epsilon = 10;
use_sparse = true;
if nargin < 3
    confidence = 0.01;
end
me.confidence = confidence;

if isfield(options, 'output')
    output = options.output;
else
    output = true;
end
%
if output; fprintf('# Training Potts Model\n'); end
%
if output; fprintf('# - finding all permutation of max dimension %d...\n', options.n); end;
allPerms = nchoose(1:options.nSubjects);
nPerms = {};
j = 1;
for i=1:length(allPerms)
    if length(allPerms{i}) <= options.n
        nPerms{j} = allPerms{i};
        j = j + 1;
    end
end

if output; fprintf('# - computing features...\n'); end;
[me.features, me.labels] = assignFeature(data' + 1, options.count, nPerms, use_sparse);
%
me.inputPerms = myPerms(options.nSubjects, options.count);
me.perms = assignFeature(me.inputPerms, options.count, nPerms, use_sparse);
%
if output; fprintf('# - computing constraints...\n'); end
if confidence == 0
    me.constraints = full(mean(me.features));
else
    [me.constraints, pci] = binofit(full(sum(me.features)), size(me.features, 1), confidence);
end

me.nSubjects = options.nSubjects;
me.range = options.count;
% initialize the weights
me.weights = rand(1, size(me.features, 2)) / size(me.features, 2);
%
C = unique(sum(me.features, 2));
if length(C) > 1
    error 'number of features in each line most be consistent'
end

loglikelihood = 0;
for iter=1:options.nIters
    if output; fprintf('# - iter (%4d/%4d) : ', iter, options.nIters); end
    % compute the (un-normalized) pdf for each sample
    p = exp(me.features * me.weights');
    % normalize the pdf (the Z)
    perms_p = exp(me.perms * me.weights');
    Z = sum(perms_p);
    p = p / Z;
    prev_loglikelihood = loglikelihood;
    loglikelihood = mean(log(p));
    E = full(sum(me.perms .* repmat(perms_p / Z, 1, size(me.perms, 2))));
    if confidence ~= 0
        %[sum(E'  > pci(:, 1) & E' < pci(:, 2)) (1 - confidence) * length(me.weights)]
        nconverged = sum(E'  > pci(:, 1) & E' < pci(:, 2));
        if output; fprintf('mean log-likelihood = %6.4f, convergence = %3d%%\n', loglikelihood, round(nconverged / length(me.weights) * 100)); end
        if nconverged >= (1 - confidence) * length(me.weights)
            if output; 
                fprintf('# converged to confidence interval (alpha = %3.2f)\n', confidence); 
            end
            break;
        end
    else
        if output; fprintf('mean log-likelihood = %6.4f (%.3f)\n', loglikelihood, abs((loglikelihood-prev_loglikelihood)/prev_loglikelihood)); end
    end
    %
    dw = me.constraints - E;
    %dw = log(me.constraints ./ E');
    me.weights = me.weights + epsilon * dw;
end
me = rmfield(me, 'features');
