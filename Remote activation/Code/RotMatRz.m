function m = RotMatRz(theta)
ct = cos(theta);
st = sin(theta);
m = [ct -st 0; st ct 0; 0 0 1];
