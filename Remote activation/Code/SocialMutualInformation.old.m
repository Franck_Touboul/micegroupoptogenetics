function [obj, mi, entropy] = SocialMutualInformation(obj)
jpAll = computeJointProbs(obj.zones-1, obj.ROI.nZones, false);
pAll = computeJointSampleProb(obj.zones-1, obj.ROI.nZones, jpAll);


mi = zeros(obj.nSubjects);
entropy = zeros(1, obj.nSubjects);
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1 == m2
            p1 = histc(obj.zones(m1, :), unique(obj.zones(~isnan(obj.zones))));
            p1 = p1 / sum(p1);
            entropy(m1) = -p1 * flog2(p1)';

            idx = 1:obj.nSubjects;
            idx = idx(idx ~= m1);

            jpLoa = computeJointProbs(obj.zones(idx, :)-1, obj.ROI.nZones, false);
            pLoa = computeJointSampleProb(obj.zones(idx, :)-1, obj.ROI.nZones, jpLoa);
            jpOne = computeJointProbs(obj.zones(m1, :)-1, obj.ROI.nZones, false);
            pOne = computeJointSampleProb(obj.zones(m1, :)-1, obj.ROI.nZones, jpOne);
            mi(m1,m1) = mean(flog2(pAll(obj.valid)) - flog2(pLoa(obj.valid)) - flog2(pOne(obj.valid)));
            
        else
            if m1<m2
                curr = obj;
                curr.nSubjects = 2;
                curr.zones = obj.zones([m1 m2], :);
                [q, indepProbs, jointProbs] = SocialComputePatternProbs(curr, false);
                cmi = jointProbs * (flog2(jointProbs) - flog2(indepProbs))'; 
                mi(m1, m2) = cmi;
                mi(m2, m1) = cmi;
            end
        end
    end
end
