function obj = TrackAppend(obj)

%obj = TrackOptimize(obj);

filename = [obj.OutputPath obj.FilePrefix '.obj.mat'];
if exist(filename, 'file')
    fprintf(['# (>) appending tracking object to ''' filename '''\n']);
    save(filename, '-struct', 'obj', '-v7', '-append');
else
    fprintf(['# (>) saving tracking object to ''' filename '''\n']);
    save(filename, '-struct', 'obj', '-v7');
end
