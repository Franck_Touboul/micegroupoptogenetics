%function TrackSimplePath(nruns)
nruns = 120;
%%
fprintf('# finding paths\n');
options = struct();
TrackDefaults;


%%
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nframes = xyloObj.NumberOfFrames;
dt = 1/xyloObj.FrameRate;
%%
fprintf('# - loading segmentations\n');
cents = struct();
cents.x       = [];
for i=1:nruns
    fprintf('#      . segment no. %d\n', i);
    filename = [options.output_path options.test.name '.segm.' sprintf('%03d', i) '.mat'];
    waitforfile(filename);
    currSegm = load(filename);
    cents = structcat(cents, currSegm.cents, 1, true, false);
end
%%
if isfield(options, 'ROIIgnore')
    ignore = uint8(imread(options.ROIIgnore));
    fprintf('#  . removing unwanted cents...');
    for i=1:size(cents.label, 1)
        labels = cents.label(i, :);
        y = cents.y(i,:);
        x = cents.x(i,:);
        y = y(labels~=0) / options.scale;
        if isempty(y)
            continue;
        end
        x = x(labels~=0) / options.scale;
        labels(labels~=0) = (1 - ignore( sub2ind(size(ignore), round(y), round(x)) )) .* labels(labels~=0);
        cents.label(i, :) = labels;
    end
    fprintf('[done]\n');
end
%%
prev = cents.label;
[q, cents.label] = max(cents.logprob, [], 3);
cents.label(prev == 0) = 0;

%%
options.colorMatchThresh = 0.9;
options.MaxHiddenDuration = 25;
options.MinProb = .4;

thresh = options.MinProb;
ndata = size(cents.x, 1);
for i=1:options.nSubjects
    logprobs = cents.logprob(:, :, i);
    logprobs(cents.label ~= i) = -inf;
    [maxlogprobs, centids] = max(logprobs, [], 2);
    maxprobs = exp(maxlogprobs);
    res{i}.x = cents.x(sub2ind(size(cents.x), 1:ndata, centids'));
    res{i}.y = cents.y(sub2ind(size(cents.y), 1:ndata, centids'));
    res{i}.prob = maxprobs';
    res{i}.x(maxprobs < thresh) = nan;
    res{i}.y(maxprobs < thresh) = nan;
    res{i}.prob(maxprobs < thresh) = nan;
    res{i}.id = (maxprobs' > thresh) * i;
end

fprintf('# intepolating short epochs\n');
for i=1:options.nSubjects
    d = diff([0 isnan(res{i}.prob) 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    len = finish - start + 1;
    valid = len < options.MaxHiddenDuration;
    start = start(valid);
    finish = finish(valid);
    for j=1:length(start)
        r = start(j):finish(j);
        [res{i}.x(r), res{i}.y(r)] = MotionInterp(res{i}.x, res{i}.y, r, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
%             if finish(1) < nFrames
%                 res{i}.x(r) = res{i}.x(finish(j)+1);
%                 res{i}.y(r) = res{i}.y(finish(j)+1);
%             end
%        end
    end
end

for i=1:options.nSubjects
    d = diff([0 isnan(res{i}.prob) 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    for j=1:length(start)
        r = start(j):finish(j);
        if start(j) > 1
            res{i}.x(r) = res{i}.x(start(j)-1);
            res{i}.y(r) = res{i}.y(start(j)-1);
        else
%             if finish(1) < nFrames
%                 res{i}.x(r) = res{i}.x(finish(j)+1);
%                 res{i}.y(r) = res{i}.y(finish(j)+1);
%             end
        end
    end
end


filename = [options.output_path options.test.name '.simple-track.mat'];
save(filename, 'res');



% fprintf('# - interpolate position\n');
% for curr = 1:options.nSubjects;
%     hidden = res{curr}.id == 0;
%     d = diff([0 hidden 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     loc = find(start > 1 & finish < length(hidden));
%     for l=loc
%         range = start(l):finish(l);
%         %         res{curr}.x(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.x([start(l)-1, finish(l)+1]), start(l):finish(l));
%         %         res{curr}.y(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.y([start(l)-1, finish(l)+1]), start(l):finish(l));
%         [res{curr}.x(range), res{curr}.y(range)] = MotionInterp(res{curr}.x, res{curr}.y, range, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
%     end
%     if length(finish) > 0 && finish(end) == length(hidden) && start(end) > 1
%         res{curr}.x(start(end):finish(end)) = res{curr}.x(start(end)-1);
%         res{curr}.y(start(end):finish(end)) = res{curr}.y(start(end)-1);
%     end
% end

filename = [options.output_path options.test.name '.track.mat'];
save(filename, 'res');

return
%%
% remove false alarms
fprintf('# - removing false alarms:\n');
res = reassignLabelsRange(res, cents, options.minDistance);

% - big jumps
fprintf('#   a. connecting neighboring visible areas\n');
for curr = 1:options.nSubjects;
    visible = res{curr}.id > 0;
    d = diff([0 visible 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    distance = sqrt(...
        (res{curr}.x(start(2:end)) - res{curr}.x(finish(1:end-1))).^2 + ...
        (res{curr}.y(start(2:end)) - res{curr}.y(finish(1:end-1))).^2);
    gap = start(2:end) - finish(1:end-1) - 1;
    loc = gap < options.minHiddenDuration & distance < options.maxJumpPerStep * gap & distance < options.maxTotalJump;
    for l=find(loc)
        range = finish(l)+1:start(l+1)-1;
        res{curr}.id(range) = -1;
        %        res{curr}.x(range) = interp1([range(1)-1 range(end)+1], ([range(1)-1 range(end)+1]), range);
        %        res{curr}.y(range) = interp1([range(1)-1 range(end)+1], res{curr}.y([range(1)-1 range(end)+1]), range);
        [res{curr}.x(range), res{curr}.y(range)] = MotionInterp(res{curr}.x, res{curr}.y, range, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
    end
end

% - small visibility epochs
fprintf('#   b. removing small visible areas\n');
for curr = 1:options.nSubjects;
    visible = res{curr}.id ~= 0;
    d = diff([0 visible 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    loc = find(finish - start + 1 < options.minVisibleDuration);
    for l=loc
        if start(l) > 1
            res{curr}.x(start(l):finish(l)) = nan;
            res{curr}.y(start(l):finish(l)) = nan;
            res{curr}.id(start(l):finish(l)) = 0;
        end
    end
end

%
% fprintf('#   c. removing static areas\n');
% for curr = 1:options.nSubjects;
%     visible = res{curr}.id ~= 0;
%     d = diff([0 visible 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     for i=1:length(start)
%         r = start(i):finish(i);
%         distance = sqrt(diff(res{curr}.x(r)).^2 + diff(res{curr}.y(r)).^2);
%         if max(distance) < options.minJump
%             res{curr}.x(r) = nan;
%             res{curr}.y(r) = nan;
%             res{curr}.id(r) = 0;
%         end
%     end
% end

%%

fprintf('#   d. removing distant areas\n');
changed = true;
while changed
    changed = false;
    for curr = 1:options.nSubjects;
        visible = res{curr}.id ~= 0;
        d = diff([0 visible 0], 1, 2);
        start  = find(d > 0);
        finish = find(d < 0) - 1;
        distance = [];
        distance(1) = true;
        for i=2:length(start)
            a = finish(i-1);
            b = start(i);
            distance(i) = sqrt((res{curr}.x(b) - res{curr}.x(a)).^2 + (res{curr}.y(b) - res{curr}.y(a)).^2) > options.maxTotalJump;
            distance(i) = distance(i) || (b - a)>options.maxDurationBetweenJumps;
        end
        start(end+1) = length(visible);
        distance(end+1) = true;
        distance = logical(distance);
        for i=find(distance)
            if distance(i)
                len = finish(:) - start(i);
                f = find(len' < options.minDurationAfterJump & distance(2:end), 1, 'last');
                if f >= i
                    r = start(i):finish(f);
                    fprintf('#    . removing (%d,%d) for %d\n', r(1), r(end), curr);
                    res{curr}.x(r) = nan;
                    res{curr}.y(r) = nan;
                    res{curr}.id(r) = 0;
                    %distance(i:f-1) = false;
                    changed = true;
                end
            end
        end
    end
end
%%

% for curr = 1:options.nSubjects;
%     visible = res{curr}.id > 0;
%     d = diff([0 visible 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     distance = sqrt(...
%         (res{curr}.x(start(2:end)) - res{curr}.x(finish(1:end-1))).^2 + ...
%         (res{curr}.y(start(2:end)) - res{curr}.y(finish(1:end-1))).^2);
%     skipDistance = sqrt(...
%         (res{curr}.x(start(3:end)) - res{curr}.x(finish(1:end-2))).^2 + ...
%         (res{curr}.y(start(3:end)) - res{curr}.y(finish(1:end-2))).^2);
%     isBigJump  = distance > options.maxJumpPerStep * (start(2:end) - finish(1:end-1));
%     isSmallGap = skipDistance < options.maxJumpPerStep * (start(3:end) - finish(1:end-2));
%     isBigJump = isBigJump & [isSmallGap false];
%     loc = find(isBigJump);
%     for l=loc
%
%     end
% end

% remove small areas where the subject is hidden
% fprintf('# - removing misdetections\n');
% for curr = 1:options.nSubjects;
%     hidden = res{curr}.id == 0;
%     d = diff([0 hidden 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     loc = find(finish - start + 1 < options.minHiddenDuration & start > 1 & finish < length(hidden));
%     for l=loc
%         res{curr}.x(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.x([start(l)-1, finish(l)+1]), start(l):finish(l));
%         res{curr}.y(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.y([start(l)-1, finish(l)+1]), start(l):finish(l));
%     end
% end

fprintf('# - interpolate position\n');
for curr = 1:options.nSubjects;
    hidden = res{curr}.id == 0;
    d = diff([0 hidden 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    loc = find(start > 1 & finish < length(hidden));
    for l=loc
        range = start(l):finish(l);
        %         res{curr}.x(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.x([start(l)-1, finish(l)+1]), start(l):finish(l));
        %         res{curr}.y(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.y([start(l)-1, finish(l)+1]), start(l):finish(l));
        [res{curr}.x(range), res{curr}.y(range)] = MotionInterp(res{curr}.x, res{curr}.y, range, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
    end
    if length(finish) > 0 && finish(end) == length(hidden) && start(end) > 1
        res{curr}.x(start(end):finish(end)) = res{curr}.x(start(end)-1);
        res{curr}.y(start(end):finish(end)) = res{curr}.y(start(end)-1);
    end
end

filename = [options.output_path options.test.name '.track.mat'];
save(filename, 'res');

