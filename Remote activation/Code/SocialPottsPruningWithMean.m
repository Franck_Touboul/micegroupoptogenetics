function obj = SocialPottsPruningWithMean(obj)
obj = TrackLoad(obj);
methods = {'mean'};

for midx = 1:length(methods)
    method = methods{midx};
    %%
    order = 2;
    nobj = obj;
    me = nobj.Analysis.Potts.Model{order};
    Entropy = [];
    Djs = [];
    Maps = [];
    Removed = [];
    idx = 1;
    %%
    p = exp(me.perms * me.weights');
    perms_p = exp(me.perms * me.weights');
    Z = sum(perms_p);
    P = p / Z;
    %%
    while sum(me.order == order) > 1
        fprintf('#-------------------------------------------------\n');
        fprintf('# using %d(%d) features (%.1f%%)\n', length(me.labels), sum(me.order == order), sum(me.order == order)/sum(obj.Analysis.Potts.Model{order}.order == order)*100);
        map = true(1, length(me.labels));
        switch lower(method)
            case 'minabs'
                w = me.weights;
                w(me.order ~= order) = inf;
                [minval, minidx] = min(abs(w));
            case 'median'
                [val, o] = sort(me.weights);
                o = o(me.order(o) == order);
                minidx = o(floor(length(o)/2));
            case 'mean'
                w = me.weights;
                w(me.order ~= order) = inf;
                [minval, minidx] = min(abs(w - mean(me.weights(me.order  == order))));
%             case 'kl'
%                 KL = zeros(1, length(me.weights));
%                 w = me.weights;
%                 for i=1:length(me.weights)
%                     w(i) = 0;
%                     if i>1
%                         w(i-1) = me.weights(i-1);
%                     end
%                     p = exp(me.perms * me.weights');
%                     p = p / sum(p);
%                     KL(i) = KullbackLeiblerDivergence(p(:)', P(:)');
%                 end
%                 [val, minidx] = min(KL);
        end
        map(minidx) = false;
        Removed(idx) = minidx;
        if idx == 1
            Maps = map;
        else
            Maps(idx, Maps(idx-1, :)) = map;
        end
        nobj = retrainPottsModel(nobj, order, map);
        % Entropy
        p = exp(me.perms * me.weights');
        perms_p = exp(me.perms * me.weights');
        Z = sum(perms_p);
        p = p / Z;
        %
        Entropy(idx) = -p' * log2(p);
        Djs(idx) = JensenShannonDivergence(p(:)', P(:)');
        fprintf('# Djs=%.1f, Entropy=%.1f\n', Djs(idx), Entropy(idx));
        %
        me = nobj.Analysis.Potts.Model{order};
        idx = idx + 1;
    end
    %%
    obj.Analysis.Potts.Pruning.(method).Removed = uint16(Removed);
    obj.Analysis.Potts.Pruning.(method).Djs = single(Djs);
    obj.Analysis.Potts.Pruning.(method).Entropy = single(Entropy);
end
TrackSave(obj);
%%
% nParams = sum(Maps, 2);
% plot(nParams, Djs)
% xlabel('# interactions');
% ylabel('Djs');
%%
if isdesktop && obj.Output
    %%
    displayMethod = 'median';
    pme = obj.Analysis.Potts.Pruning.(displayMethod);
    %
%     subplot(3,4,4);
%     plot(nParams, pme.Djs)
%     xlabel('# interactions');
%     ylabel('Djs');
    %
    nInteraction = 100;
    me = obj.Analysis.Potts.Model{order};
    conn = pme.Removed(end-nInteraction+1:end);
    me.order = me.order(conn);
    me.weights = me.weights(conn);
    me.labels = { me.labels{conn} };
    
    r = 10;
    rr = .8;
    coord = [];
    for i=1:obj.ROI.nZones
        for s=1:obj.nSubjects
            R = r;
            coord(:, i, s) = [R * sin(i/obj.ROI.nZones * 2*pi) + rr * sin(s/obj.nSubjects* 2*pi), R * cos(i/obj.ROI.nZones * 2*pi) + rr * cos(s/obj.nSubjects* 2*pi)];
            plot(coord(1, i, s), coord(2, i, s), 'ko', 'MarkerFaceColor', 'k', 'MarkerSize', 10);
            hold on;
        end
        R = r + 3 * rr;
            text(R * sin(i/obj.ROI.nZones * 2*pi), R * cos(i/obj.ROI.nZones * 2*pi), obj.ROI.ZoneNames{i}, 'HorizontalAlignment', 'center');
%         if sin(i/obj.ROI.nZones * 2*pi) < 0
%             text(R * sin(i/obj.ROI.nZones * 2*pi), R * cos(i/obj.ROI.nZones * 2*pi), obj.ROI.ZoneNames{i}, 'HorizontalAlignment', 'center');
%         else
%             text(R * sin(i/obj.ROI.nZones * 2*pi), R * cos(i/obj.ROI.nZones * 2*pi), obj.ROI.ZoneNames{i});
%         end
    end
    
    [s, ow] = sort(me.weights);
    ow = ow(me.order(ow) == 2);
    cmap = MyDefaultColormap(length(ow));
    %
    set(gcf, 'Color', 'w');
%     cmap = repmat(rgb2hsl(cmap(1, :)), length(ow), 1);
%     cmap(:, 3) = sequence(cmap(1, 3), .9, length(ow));
%      cmap = hsl2rgb(cmap);
    linewidth = sequence(1, 6, length(ow));
    %
    idx = 1;
    for l=ow
        x = [coord(1, me.labels{l}(2,1), me.labels{l}(1,1)) coord(1, me.labels{l}(2,2), me.labels{l}(1,2))];
        y = [coord(2, me.labels{l}(2,1), me.labels{l}(1,1)) coord(2, me.labels{l}(2,2), me.labels{l}(1,2))];
        plot(x, y, 'Color', cmap(idx, :), 'LineWidth', linewidth(idx));
        idx = idx + 1;
    end
    axis off;
    hold off;
end
