TrackDefaults;

filename = [options.output_path options.test.name '.simple-track.mat'];
load(filename);

filename = [options.output_path options.test.name '.social.mat'];
load(filename);

p = [];
for i=1:social.nSubjects
    p(i) = sum(res{i}.id~=0) / length(res{i}.id);
end
barweb(p, p*0)
colormap(social.colors);
axis([0.5 1.5 0 1]);
xlabel('Subject');
ylabel('% time out');
set(gca, 'XTickLabel', []);