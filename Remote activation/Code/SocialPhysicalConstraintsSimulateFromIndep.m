%function SocialPhysicalConstraintsSimulateFromIndep
id = 11;
res = [];
res.zones = [];
res.p = [];
obj = TrackLoad({id, 2});
%%
for i=1:obj.nSubjects
    p = histc(obj.zones(i, obj.valid), 1:obj.ROI.nZones);
    res.p(i, :) = p / sum(p);
end
%%
z = obj.zones(i, obj.valid);
if i>1
    n = min(length(z), size(zones, 2));
    res.zones = [res.zones(:, 1:n); z(1:n)];
else
    n = length(z);
    res.zones = z;
end
size(res.zones);

%%
res.obj = obj;
res.obj.zones = res.zones;
res.obj.nFrames = size(res.zones, 2);
res.obj.valid = true(1, res.obj.nFrames);
res.obj.Output = true;
res.obj.OutputToFile = false;
res.obj.OutputInOldFormat = false;
%%
limits = [4, 2, 2, 2, 2, 2, 2, 2, 4, 2];
limits = [4, 4, 4, 4, 4, 4, 4, 4, 4, 4];
zones = res.zones;
for z=1:obj.ROI.nZones
    zones = zones(:, sum(zones == z) <= limits(z));
end
res.obj.zones = zones;
res.obj.nFrames = size(zones, 2);
res.obj.valid = true(1, res.obj.nFrames);
res.obj.Output = true;
res.obj.OutputToFile = false;
res.obj.OutputInOldFormat = false;
%%
res.constobj = SocialPotts(res.obj);