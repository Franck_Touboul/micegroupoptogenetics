function [me, output, ent] = TrainPottsUsingGIS(me, options)
if nargin < 2
    options = struct();
end
me.weights = randn(1, size(me.labels, 2)) / size(me.labels, 2); %randn(1, length(me.weights));

options = setDefaultParameters(options, 'nIters', 400, 'confidence', 0);
%%
allPerms = nchoose(1:me.nSubjects);
nPerms = {};
j = 1;
for i=1:length(allPerms)
    if length(allPerms{i}) <= max(me.order);
        nPerms{j} = allPerms{i};
        j = j + 1;
    end
    C = length(nPerms);
end
%%
output.x = me.weights;
p = exp(me.perms * me.weights');
p = p / sum(p);
E = full(sum(me.perms .* repmat(p, 1, size(me.perms, 2))));
output.fval = -me.constraints * log(E)';

%%
loglikelihood = 0;
output.ticID = tic;
output.times = 0;
for iter=1:options.nIters
    fprintf('# - iter (%4d/%4d) : ', iter, options.nIters); 
    % compute the (un-normalized) pdf for each sample
    p = exp(me.perms * me.weights');
    perms_p = p;
    % normalize the pdf (the Z)
    Z = sum(p);
    p = p / Z;
    prev_loglikelihood = loglikelihood;
    loglikelihood = sum(p .* log(p));
    E = full(sum(me.perms .* repmat(perms_p / Z, 1, size(me.perms, 2))));
    halfKL = -me.constraints * log(E)';
    if options.confidence ~= 0
        %[sum(E'  > pci(:, 1) & E' < pci(:, 2)) (1 - confidence) * length(me.weights)]
        nconverged = sum(E'  > pci(:, 1) & E' < pci(:, 2));
        fprintf('mean log-likelihood = %6.4f, convergence = %3d%%\n', loglikelihood, round(nconverged / length(me.weights) * 100));
        if nconverged >= (1 - options.confidence) * length(me.weights)
                fprintf('# converged to confidence interval (alpha = %3.2f)\n', options.confidence);
                break;
        end
    else
        fprintf('mean log-likelihood = %6.4f (%.3f), -(half KL) = %6.4f\n', loglikelihood, abs((loglikelihood-prev_loglikelihood)/prev_loglikelihood), halfKL);
        output.fval = [output.fval; halfKL];
    end
    %
    dw = log(me.constraints ./ E);
    %dw = log(me.constraints ./ E');
    dw(me.constraints == 0) = 0;
    me.weights = me.weights + 1/C * dw;
    output.x = [output.x; me.weights];
    output.times = [output.times; toc(output.ticID)];
end
ent = [];
%me = rmfield(me, 'features');
