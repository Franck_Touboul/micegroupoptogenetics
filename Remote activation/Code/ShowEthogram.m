function img = ShowEthogram(map, colormap)

img = ones(size(map,1),size(map,2),3);

for i=unique(map(~isnan(map)))'
    for k=1:3
        component = img(:, :, k);
        component(map == i) = colormap(i, k);
        img(:, :, k) = component;
    end
end

imagesc(img);