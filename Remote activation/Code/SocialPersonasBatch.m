SocialExperimentData;
%%
All = cell(1,  GroupsData.nGroups);
Titles = cell(1, GroupsData.nGroups);
for g=1:GroupsData.nGroups
    All{g} = [];
    Titles{g}  = [];
end
idx = 1;
for id=GroupsData.index
    %%
    group = GroupsData.group(id);
    try
        obj = TrackLoad({id}, {'ROI', 'zones', 'valid'});
        %%
        for s=1:obj.nSubjects
            zHist = histc(obj.zones(s, obj.valid), 1:obj.ROI.nZones);
            zHist = zHist / sum(zHist);
            %
            All{group} = [All{group}; zHist];
        end
        data = TrackParse(obj);
        Titles{g}{idx} = num2str(data.GroupID);
        %
        figure(group);
        PlotPersonas(All{group}, false, [], [], Titles{g})
        drawnow;
        %
        All{group} = [All{group}; inf(1, size(All{group}, 2))];
        idx = idx + 1;
    catch
    end
end

return;
%%
Z = {};
Z{1} = [];
Z{2} = [];
for id=GroupsData.index
    %%
    group = GroupsData.group(id);
    obj = TrackLoad({id, 2}, {'ROI', 'zones', 'valid'});
    Z{group} = [Z{group}, obj.zones(:)'];
end
%%

for g=1:GroupsData.nGroups
    %%
    h = histc(Z{g}, 1:obj.ROI.nZones);
    h = h / sum(h) * 100
    subplot(GroupsData.nGroups, 1, g); 
    PlotPersonas(h, true, MyZonesColormap);
end
        
