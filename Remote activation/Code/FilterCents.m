[~, labels] = max(cents.logprob, [], 3);
labels(cents.label == 0) = 0;

count = sum(labels~=0, 2);

%%
options.MaxIsolatedDistance = 12;
fprintf('# removing isolated cents\n');
d = diff([0 (count > 0)' 0], 1, 2);
start  = find(d > 0);
finish = find(d < 0) - 1;
len = finish - start + 1;
isolated = start - [1 finish(1:end-1)] > options.MaxIsolatedDistance & ...
    [start(2:end) size(labels, 1)] - finish > options.MaxIsolatedDistance;
idx = find(len == 1 & isolated);
for i=idx
    labels(start(i):finish(i), :) = 0;
end
count = sum(labels~=0, 2);
fprintf('# - found %d cents\n', length(idx));

%%

start = [];
finish = [];
for i=1:size(labels,2)
    d = diff([0 (count == i)' 0], 1, 2);
    start  = [start, find(d > 0)];
    finish = [finish, find(d < 0) - 1];
end

for i=1:length(start)
    prev = [];
    for f=start(i):finish(i)
        curr = sort(labels(f, :));
        if ~isempty(prev) && any(curr ~= prev)
            [i, f]
        end
        prev = curr;
    end
end