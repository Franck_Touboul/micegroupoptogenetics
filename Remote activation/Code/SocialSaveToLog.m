function obj = SocialSaveToLog(obj)

%%
obj = TrackLoad(obj);
%%
varname = strrep(obj.FilePrefix, '.', '_');
eval([varname ' = obj;']);

path = regexprep(obj.VideoFile, '[^\\/]*$', '');
logfile = [path 'log.mat'];
fprintf(['# - saving to log file ' logfile '\n']);
if isunix
    system(['lockfile ' logfile '.lock']);
    try
        if exist(logfile, 'file')
            save(logfile, varname, '-append');
        else
            save(logfile, varname);
        end
    catch me
    end
    system(['rm -f ' logfile '.lock']);
else
    if exist(logfile, 'file')
        if ~exist([path 'logbackup/'], 'dir')
            mkdir([path 'logbackup/']);
        end
        oldLogfile = [path 'logbackup/log.' datestr(now, 'yyyymmddTHHMMSS_FFF') '.mat'];
        fprintf(['# - old log file was backuped to: ' oldLogfile '\n']);
        copyfile(logfile, oldLogfile);
        save(logfile, varname, '-append');
    else
        save(logfile, varname);
    end
end
