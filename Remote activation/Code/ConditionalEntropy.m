function H = ConditionalEntropy(data)
[conditionalProbs, jointProbs] = ConditionalProbability(data);
H = -sum(jointProbs .* flog2(conditionalProbs));
