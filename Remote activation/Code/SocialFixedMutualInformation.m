function [obj, mi, entropy] = SocialFixedMutualInformation(obj)
%%
ShowOnGraph = true;
jpAll = computeJointProbs(obj.zones-1, obj.ROI.nZones, false);
pAll = computeJointSampleProb(obj.zones-1, obj.ROI.nZones, jpAll);

%%
nranks = 20;
niters = 128;

mi = zeros(obj.nSubjects);
entropy = zeros(1, obj.nSubjects);
index = 1;
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1 == m2
            %%
            H1 = nan(nranks, niters);
            Ho = nan(nranks, niters);
            for rank = 1:nranks;
                cniters = min(4^(rank-1), niters);
                for iter=1:cniters
                    [rank, iter]
                    ndata = size(obj.zones, 2);
                    if rank > 1
                        map = randperm(ndata); map = map(1:floor(ndata/rank));
                        zones = obj.zones(:, map);
                        valid = obj.valid(map);
                        map = map(valid);
                    else
                        map = 1:ndata;
                        zones = obj.zones;
                        valid = obj.valid;
                        map = map(valid);
                    end
                    
                    p1 = histc(zones(m1, :), unique(zones(~isnan(zones))));
                    p1 = p1 / sum(p1);
                    entropy(m1) = -p1 * flog2(p1)';
                    
                    idx = 1:obj.nSubjects;
                    idx = idx(idx ~= m1);
                    %
                    jpLoa = computeJointProbs(zones(idx, :)-1, obj.ROI.nZones, false);
                    pLoa = computeJointSampleProb(zones(idx, :)-1, obj.ROI.nZones, jpLoa);
                    %
                    jpOne = computeJointProbs(zones(m1, :)-1, obj.ROI.nZones, false);
                    pOne = computeJointSampleProb(zones(m1, :)-1, obj.ROI.nZones, jpOne);
                    %
                    jplAll = computeJointProbs(zones-1, obj.ROI.nZones, false);
                    plAll = computeJointSampleProb(zones-1, obj.ROI.nZones, jplAll);
                    %
                    Ho(rank, iter) = -mean(flog2(plAll(valid)) - flog2(pLoa(valid)))
                    H1(rank, iter) = -mean(flog2(pOne(valid)));
                end
            end
            %%
            Sop = polyfit(1:nranks, validmean(Ho, 2)', 2); So = Sop(end);
            S1p = polyfit(1:nranks, validmean(H1, 2)', 2); S1 = S1p(end);
            mi(m1,m1) = S1 - So;
            %%
            if ShowOnGraph
                cmap = MySubCategoricalColormap();
                %
                figure(1)
                subplot(2,obj.nSubjects, m1);
                plot(1:nranks,validmean(Ho, 2)', 'o--', 'Color', cmap(1, :), 'MarkerFaceColor', cmap(1, :), 'LineWidth', 2); hold on;
                title([num2str(m1)]);
                
                %errorbar(1:nranks,validmean(Ho, 2)', stderr(Ho, 2)', 'o--', 'Color', cmap(1, :), 'MarkerFaceColor', cmap(1, :), 'LineWidth', 2); hold on;
                plot(0:nranks, polyval(Sop, 0:nranks), '-', 'Color', cmap(2, :),'LineWidth', 2);
                plot(0,So, 'o--', 'Color', cmap(2, :), 'MarkerFaceColor', cmap(2, :), 'LineWidth', 2); hold on;
                %
                subplot(2,obj.nSubjects,obj.nSubjects + m1);
                plot(1:nranks,validmean(H1, 2)', 'o--', 'Color', cmap(3, :), 'MarkerFaceColor', cmap(3, :), 'LineWidth', 2); hold on;
%                errorbar(1:nranks,validmean(H1, 2)', stderr(H1, 2)', 'o--', 'Color', cmap(3, :), 'MarkerFaceColor', cmap(3, :), 'LineWidth', 2); hold on;
                plot(0:nranks, polyval(S1p, 0:nranks), '-', 'Color', cmap(4, :),'LineWidth', 2);
                plot(0,S1, 'o--', 'Color', cmap(4, :), 'MarkerFaceColor', cmap(4, :), 'LineWidth', 2); hold on;
                %
                %a = axis;
                %axis([0 nranks a(3) a(4)]);
            end
        else
            if m1<m2
                %%
                H1 = nan(nranks, niters);
                H2 = nan(nranks, niters);
                for rank = 1:nranks;
                    cniters = min(4^(rank-1), niters);
                    
                    for iter=1:cniters
                        ndata = size(obj.zones, 2);
                        if rank > 1
                            map = randperm(ndata); map = map(1:floor(ndata/rank));
                            zones = obj.zones(:, map);
                            valid = obj.valid(map);
                            map = map(valid);
                        else
                            map = 1:ndata;
                            zones = obj.zones;
                            valid = obj.valid;
                            map = map(valid);
                        end
                        curr = obj;
                        curr.nSubjects = 2;
                        curr.zones = zones([m1 m2], :);
                        curr.valid = valid;
                        [q, indepProbs, jointProbs] = SocialComputePatternProbs(curr, false);
                        %cmi = jointProbs * (flog2(jointProbs) - flog2(indepProbs))';
                        H1(rank, iter) = -jointProbs * flog2(jointProbs)';
                        H2(rank, iter) = -jointProbs * flog2(indepProbs)';
                    end
                end
                S1p = polyfit(1:nranks, validmean(H1, 2)', 2); S1 = S1p(end);
                S2p = polyfit(1:nranks, validmean(H2, 2)', 2); S2 = S2p(end);
                if ShowOnGraph
                    %%
                    cmap = MySubCategoricalColormap();
                    %
                    figure(2)
                    subplot(2,obj.nSubjects*(obj.nSubjects-1)/2,index);
                    plot(1:nranks,validmean(H1, 2)', 'o--', 'Color', cmap(5, :), 'MarkerFaceColor', cmap(5, :), 'LineWidth', 2); hold on;
                    title([num2str(m1) ',' num2str(m2)]);
                    %errorbar(1:nranks,validmean(Ho, 2)', stderr(Ho, 2)', 'o--', 'Color', cmap(1, :), 'MarkerFaceColor', cmap(1, :), 'LineWidth', 2); hold on;
                    plot(0:nranks, polyval(S1p, 0:nranks), '-', 'Color', cmap(6, :),'LineWidth', 2);
                    plot(0,S1, 'o--', 'Color', cmap(6, :), 'MarkerFaceColor', cmap(6, :), 'LineWidth', 2); hold on;
                    %
                    subplot(2,obj.nSubjects*(obj.nSubjects-1)/2,obj.nSubjects*(obj.nSubjects-1)/2 + index);
                    plot(1:nranks,validmean(H2, 2)', 'o--', 'Color', cmap(7, :), 'MarkerFaceColor', cmap(7, :), 'LineWidth', 2); hold on;
                    %                errorbar(1:nranks,validmean(H1, 2)', stderr(H1, 2)', 'o--', 'Color', cmap(3, :), 'MarkerFaceColor', cmap(3, :), 'LineWidth', 2); hold on;
                    plot(0:nranks, polyval(S2p, 0:nranks), '-', 'Color', cmap(8, :),'LineWidth', 2);
                    plot(0,S2, 'o--', 'Color', cmap(8, :), 'MarkerFaceColor', cmap(8, :), 'LineWidth', 2); hold on;
                    %
                    index = index + 1;
                    %a = axis;
                    %axis([0 nranks a(3) a(4)]);
                end
                
                mi(m1, m2) = S2 - S1;
                mi(m2, m1) = S2 - S1;

            end
        end
    end
end
