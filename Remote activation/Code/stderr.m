function e = stderr(x, dim)
if nargin==1
    if isvector(x)
        x = x(:);
    end
    dim = 1;
end

e = std(x, 0, dim) / sqrt(size(x, dim));
