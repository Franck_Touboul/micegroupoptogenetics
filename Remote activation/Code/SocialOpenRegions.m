map = obj.ROI.Regions{1}*0;
for i=1:obj.ROI.nRegions
   map(obj.ROI.Regions{i}) = i;
end
imagesc(map);

%%
[I_, J_] = meshgrid(1:size(img, 1), 1:size(img, 2));
md = [];
for r=1:obj.ROI.nRegions
    %%
    r
    img = map;
    [I, J] = find((imdilate(img == r, ones(3,3)) - (img == r)) ~= 0);
    
    d = pdist2([I_(:), J_(:)], [I, J]);
    if r == 1
        md = zeros(size(d, 1), obj.ROI.nRegions);
    end
    md(:, r) = min(d, [], 2);
end
%%
base = obj.ROI.nRegions;
baseVector = base.^(2-1:-1:0);

[~, idx] = sort(md, 2);
idx = (idx(:, 1:2) - 1) * baseVector' + obj.ROI.nRegions;
lmap = map;
lmap(sub2ind(size(lmap), I_, J_)) = idx;
lmap(map > 0) = map(map>0);
imagesc(lmap)


