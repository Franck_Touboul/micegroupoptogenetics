pre = false;
%%
obj = TrackLoad(obj);
obj = SocialFindInteractions(obj);
%%
nframes = 50;
features_ = [];
sources_ = [];
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1<m2; continue; end;
        [start, finish, len] = FindEvents(obj.contact{min(m1, m2), max(m1, m2)});
        
        features = zeros(length(start), nframes);
        sources = zeros(length(start), 4);
        index = 1;
        for i=1:length(start)
            %    features(i, :) = obj.Interactions.speed{m1, m2}(start(i)-nframes+1:start(i)) - obj.Interactions.speed{m2, m1}(start(i)-nframes+1:start(i));
            if pre
                currfeat = obj.Interactions.rSpeed{m1, m2}(start(i)-nframes+1:start(i));
            else
                currfeat = obj.Interactions.rSpeed{m1, m2}(finish(i):finish(i) + nframes - 1);
            end
            if ~any(~isfinite(currfeat))
                features(index, :) = currfeat;
                sources(index, :) = [m1 m2 start(i) finish(i)];
                index = index + 1;
            end
            %features(i, :) = obj.angle{m1, m2}(start(i)-nframes+1:start(i));
        end
        features_ = [features_; features(1:index-1, :)];
        sources_ = [sources_; sources(1:index-1, :)];
    end
end
subplot(5,1,1);plot(1:nframes, features_);
subplot(5,1,2);plot(1:nframes, validmean(features_));
title('average');

%%
[pcacoeff, pcavec] = pca(features_);
subplot(5,1,3);
plotyy(1:nframes, pcacoeff, 1:nframes, cumsum(pcacoeff / sum(pcacoeff)));
title('PCA');
legend('variance', '% explained');
legend boxoff
%%
nvec = 5;
cmap = lines(nvec);
subplot(5,1,4);
for i=1:5
    plot(1:nframes, pcavec(i, :), 'Color', cmap(i, :));
    hold on;
end
hold off;
%%
ncents = 10;
[idx, c] = kmeans(features_, ncents, 'MaxIter', 1000, 'Replicates',5);
count = histc(idx, 1:ncents);
[count, sidx] = sort(count, 1, 'descend');
temp = idx * 0;
for i=1:ncents
    temp(idx == sidx(i)) = i;
end
idx = temp;
c = c(sidx, :);

cmap = lines(ncents);
for i=1:ncents
    subplot(ncents+1,1,i);
    plot(1:nframes, c(i, :));
    title(sprintf('%d%% (%d)', round(count(i)/size(features_, 1) * 100), count(i)));
    %hold on;
end
subplot(ncents+1,1,ncents+1);
plot(cumsum(count/size(features_, 1)));
find(cumsum(count/size(features_, 1)) > .95, 1)
%hold off;
