function objs = SocialGroupLoad(objs, path)
if nargin < 2
    path = '.';
end
fprintf('# loading group...\n');
if ischar(objs)
    filename = objs;
    try 
        load(filename);
    catch
        fid = fopen(filename, 'r');
        list = {};
        index = 1;
        tline = fgetl(fid);
        while ischar(tline)
            list{index} = tline;
            tline = fgetl(fid);
            index = index + 1;
        end
        list = unique(list);
        fclose(fid);
        objs = struct();
        for i=1:length(list)
            field = regexprep(list{i}, '\.', '_');
            try
                currFilename = [path '/' list{i} '.obj.mat'];
                fprintf(['# - loading from: ' currFilename '\n']);
                currObj = load(currFilename);
                objs.(field) = currObj.obj;
            catch
                fprintf(['#    + failed!\n']);
            end
        end
    end
end