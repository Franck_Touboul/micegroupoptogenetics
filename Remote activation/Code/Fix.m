function obj = Fix(obj)
obj = TrackLoad(obj);
for i=1:length(obj.Analysis.Potts.Model)
    obj.Analysis.Potts.Model{i}.neutralZone = 1;
end
TrackSave(obj);