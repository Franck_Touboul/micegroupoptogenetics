subjects = 1;
for s=subjects
    for z=1:obj.ROI.nZones
        [b,e,l] = FindEvents(obj.zones(s, :) == z);
        
    end
end



%%
SocialExperimentData
L = [];
Z = [];
for i=GroupsData.Standard.idx
    obj = TrackLoad({i 2});
    %%
    for s=1:obj.nSubjects
        zones = obj.zones(s, :);
        mz = [];
        for z=1:obj.ROI.nZones
            [b,e,l] = FindEvents(zones == z);
            mz(z) = mean(l);
        end
        Z = [Z; mz];
    end
end

%%
for z=1:obj.ROI.nZones
    %%
    subplot(obj.ROI.nZones, 1, z);
    curr = Z(:, z)' * obj.dt;
    mZ = mean(curr);
    stderrZ = stderr(curr);
    %
    bar([1:size(Z, 1) size(Z, 1) + 4], [curr mZ]);
    hon
    errorbar(size(Z, 1) + 4, mZ, stderrZ, 'k');
    hoff
    a = axis;
    axis([0 size(Z, 1)+5 a(3) a(4)])
    set(gca, 'Xtick', [1:size(Z, 1) size(Z, 1) + 4]);
    labels = {};
    for i=1:size(Z, 1)
        labels{i} = num2str(i);
    end
    labels{end+1} = 'mean';
    set(gca, 'XtickLabel', labels);
    xlabel('subject');
    ylabel(obj.ROI.ZoneNames{z});
    box off
end
%%
MyBoxPlot(Z'* obj.dt, cmap)
set(gca, 'XTick',  1:obj.ROI.nZones);
set(gca, 'XTickLabel',  obj.ROI.ZoneNames);
xlabel('zone name');
ylabel('average continuous duration in zone [sec]');
%%
cmap = [    189 190 200;
    148 216 239;
    0 177 229;
    234 165 193;
    179 213 157;
    53 178 87;
    220 73 89;
    240 191 148;
    240 145 55;
    98 111 179;
    246 237 170;
    248 231 83;
    ]/256;

mZ = [];
stderrZ = [];
for z=1:obj.ROI.nZones
    %%
    curr = Z(:, z)' * obj.dt;
    mZ(z) = mean(curr);
    stderrZ(z) = stderr(curr);
    OneBar(z, mZ(z), cmap(z, :), stderrZ(z))
    hon;
%    plot(sequence(z-.4, z+.4, length(curr)), curr, 'o');
end
set(gca, 'XTick',  1:obj.ROI.nZones);
set(gca, 'XTickLabel',  obj.ROI.ZoneNames);
xlabel('zone name');
ylabel('average continuous duration in zone [sec]');
hoff


