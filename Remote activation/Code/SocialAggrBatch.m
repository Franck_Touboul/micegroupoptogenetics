
for g=1:2
    stat(g).nContacts = [];
    stat(g).Duration = [];
    stat(g).TimeOutside = [];
    stat(g).nAggrChases = [];
    stat(g).Day = [];
    stat(g).Id = [];
end

SocialExperimentData;
%%
for id=GroupsData.index
    g = GroupsData.group(id);
    for day=1:GroupsData.nDays
        obj = TrackLoad({id day});
        %%
        b = obj.Contacts.Behaviors.(options.Behavior);
        stat(g).Duration = [stat(g).Duration, obj.RecordingDuration * [0 0 24 1 1/60 1/3600]'];
        stat(g).nContacts = [stat(g).nContacts, length(obj.Contacts.List)];
        stat(g).nAggrChases = [stat(g).nAggrChases, sum(obj.Contacts.Behaviors.AggressiveChase.Map)];
        
        s = ~obj.sheltered(:, obj.valid);
        stat(g).TimeOutside = [stat(g).TimeOutside, sum(s, 2) / obj.FrameRate / 3600];
        
        stat(g).Day = [stat(g).Day, day];
        stat(g).Id = [stat(g).Id, id];
    end
end
%%
clf

res = [];
for g=1:length(stat)
    %%
    s = stat(g);
    pTimeOutside = s.TimeOutside ./ repmat(s.Duration, obj.nSubjects, 1) * 100;
    nContactsPerHour = s.nContacts ./ s.Duration;
    nAggrChasePerHour = s.nAggrChases ./ s.Duration;
    pAggrChase = s.nAggrChases ./ s.nContacts * 100;
    for day=1:GroupsData.nDays
        q = pTimeOutside(:, s.Day == day);
        res.pTimeOutside(day).m = mean(q(:));
        res.pTimeOutside(day).err = stderr(q(:));
        res.nContactsPerHour(day).m = mean(nContactsPerHour(s.Day == day));
        res.nContactsPerHour(day).err = stderr(nContactsPerHour(s.Day == day));
        res.nAggrChasePerHour(day).m = mean(nAggrChasePerHour(s.Day == day));
        res.nAggrChasePerHour(day).err = stderr(nAggrChasePerHour(s.Day == day));
        res.pAggrChase(day).m = mean(pAggrChase(s.Day == day));
        res.pAggrChase(day).err = stderr(pAggrChase(s.Day == day));
    end
    subplot(2,2,1);
    errorbar(1:GroupsData.nDays, [res.pTimeOutside.m], [res.pTimeOutside.err], 'color', GroupsData.Colors(g, :));
    xlabel('day');
    ylabel('% time outside of nest');
    hon;
    subplot(2,2,2);
    errorbar(1:GroupsData.nDays, [res.nContactsPerHour.m], [res.nContactsPerHour.err], 'color', GroupsData.Colors(g, :));
    xlabel('day');
    ylabel('contacts per hour');
    hon;
    subplot(2,2,3);
    errorbar(1:GroupsData.nDays, [res.nAggrChasePerHour.m], [res.nAggrChasePerHour.err], 'color', GroupsData.Colors(g, :));
    xlabel('day');
    ylabel('aggressive chases per hour');
    hon;
    subplot(2,2,4);
    errorbar(1:GroupsData.nDays, [res.pAggrChase.m], [res.pAggrChase.err], 'color', GroupsData.Colors(g, :));
    xlabel('day');
    ylabel('% aggressive chase');
    hon;
    
end
saveFigure(['Graphs/SocialAggrBatch']);
