function d = pdist2(x, y, varargin)
d = squareform(pdist([x; y], varargin{:}));

d = d(1:size(x, 1), size(x, 1)+(1:size(y, 1)));