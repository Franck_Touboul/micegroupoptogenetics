function p = VonMisesUniformHalfDistribution_v2(x, m, v, base)
if nargin < 4
    base = .5/pi;
end
k = 1 ./ v;
b = besseli(0, k);
p1 = exp(k .* cos(x - m)) ./ ( 2 * pi * b);
p2 = exp(k .* cos(-x - m)) ./ ( 2 * pi * b);
p = p1 + p2;
p = p * (1-base * pi) + base;
