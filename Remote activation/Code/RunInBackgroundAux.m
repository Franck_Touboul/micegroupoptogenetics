function RunInBackgroundAux(id, funcstr)
fprintf('# runnding in background\n');
log = ['../temp/' id '.output'];
filename = ['../temp/' id '.obj.mat'];
res = [];
res.status = 'running';
res.cmd = funcstr;
res.args = '';
res.starttime = datestr(now);
WriteStrudel(res, ['../temp/' id '.output']);
try
    temp.OutputPath = '../temp/';
    temp.FilePrefix = id;
    temp = eval([funcstr '(''' temp.OutputPath temp.FilePrefix '.obj.mat'')']);
    TrackSave(temp);
    res.status = 'done';
    res.endtime = datestr(now);
    res.message = '';
    fprintf(['# writing to log: ' log '\n']);
    WriteStrudel(res, log);
catch me
    res.status = 'failed';
    res.endtime = datestr(now);
    res.message = me.message;
    fprintf('# Failed!\n');
    fprintf('# %s', me.message);
    fprintf(['# writing to log: ' log '\n']);
    WriteStrudel(res, log);
end

