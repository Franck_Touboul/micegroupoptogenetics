function VarSize(obj)
%% Variable Sizes
root = struct('obj', obj, 'name', 'obj');
queue = root;
var.size = [];
var.type = {};
var.name = {};
idx = 1;
while ~isempty(queue)
    o = queue(1);
    queue = queue(2:end);
    f = fieldnames(o.obj);
    for i=1:length(f)
        a = o.obj.(f{i});
        if isstruct(a)
            queue(end+1) =  struct('obj', o.obj.(f{i}), 'name', [o.name, '.' f{i}]);
        end
        data = whos('a');
        var.size(idx) = data.bytes;
        var.type{idx} = class(o.obj.(f{i}));
        var.name{idx} = [o.name '.' f{i}];
%        fprintf('%-60s %-20s %20d\n', [o.name '.' f{i}], class(o.obj.(f{i})), data.bytes);
        idx = idx + 1;
    end
end

[var.size, order] = sort(var.size);
var.type = { var.type{order} };
var.name = { var.name{order} };

for i=1:length(var.name)
    fprintf('%-60s %-20s %20d\n', var.name{i}, var.type{i}, var.size(i));
end
