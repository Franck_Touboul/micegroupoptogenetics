function TrackBadFrames(nruns, id)
%%
fprintf('# segmenting movie frames:\n');
if ~license('checkout', 'image_toolbox')
    fprintf('# - waiting for image processing toolbox license\n');
    while ~license('checkout', 'image_toolbox'); pause(5); end
end
TrackDefaults;
%%
fprintf('# segmenting frames\n');
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nFrames = xyloObj.NumberOfFrames;
dt = 1/xyloObj.FrameRate;
%%
fprintf('# - loading meta data\n');
filename = [options.output_path options.test.name '.meta.mat'];
waitforfile(filename);
load(filename);

%%
if nargin == 0
    startframe = 1;
    endframe = 200;
else
    step = floor(nFrames / nruns);
    curr = 0;
    for i=1:id
        prev = curr + 1;
        if i == nruns
            curr = nFrames;
        else
            curr = prev + step - 1;
        end
    end
    startframe = prev;
    endframe = curr;
end
nFrames = endframe - startframe + 1;
%%
prevProps = [];
sx = []; sy = [];

cents.x = zeros(nFrames, options.maxNumCents);
cents.y = zeros(nFrames, options.maxNumCents);
cents.label = zeros(nFrames, options.maxNumCents, 'uint8');
cents.area = zeros(nFrames, options.maxNumCents, 'uint16');
cents.solidity = zeros(nFrames, options.maxNumCents, 'single');
cents.logprob = zeros(nFrames, options.maxNumCents, options.nSubjects);
%%
doubleBkgFrame = im2double(meta.bkgFrame);
bkgNoise = std(doubleBkgFrame(:));
cmap = meta.subject.centerColors;
%%
nchars = RePrintf('# - frame %6d [%d-%d] (%6.2fxiRT)', startframe, startframe, endframe, 0); 
tic;
for r=1:nFrames
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d [%d-%d] (%6.2fxiRT)', r+startframe-1, startframe, endframe, RT);
    currTime = (r+startframe-1) * dt;
    
    m = myMMReader(options.MovieFile, r+startframe-1, meta.bkgFrame);
    orig = m;
    m = im2double(m) - im2double(meta.bkgFrame);
    m = (m > 10 * bkgNoise);
    m = m(:,:,1) | m(:,:,2) | m(:,:,3);
    %%
    cc = bwconncomp(m);
    cents.x(r,1) = cc.NumObjects;
    subplot(2,1,1); imagesc(m);subplot(2,1,2); imagesc(orig);
    drawnow
end
fprintf('\n');
fprintf(['# - total time: ' sec2time(toc) '\n']);
%
cents.startframe = startframe;
cents.endframe   = endframe;
%
if nargin == 0
    filename = [options.output_path options.test.name '.bad.mat'];
else
    filename = [options.output_path options.test.name '.bad.' sprintf('%03d', id) '.mat'];
end
fprintf(['# - saving segmentation: "' filename '"']);
save(filename, 'cents');

