function [outliersIndex] = MyBoxPlot(data)
%%
cmap = MySubCategoricalColormap;
colors.inner = cmap(6, :);
colors.outer = cmap(2, :);
colors.median = [0 0 0];
%
cmap = MySubCategoricalColormap;
colors.inner = [0 180 225]/256;
colors.outer = [218 218 218]/256;
colors.median = [81 81 81]/256;

%
%outcmap = MyDefaultColormap(size(data, 2));
Q = quantile(data', [.25 .50 .75]);
if isvector(Q)
    Q = Q(:);
end
IQR = Q(3, :) - Q(1, :);
R = [];
R(1, :) = Q(1,:) - IQR * 1.5;
R(2, :) = Q(3,:) + IQR * 1.5;

width = 0.8;
outliersIndex = [];
for i=1:size(data,1)
    %errorbar(i, Q(2, i), Q(2, i) - R(1, i), Q(2, i) - R(2, i), 'k', 'LineWidth', 2);
    valid = data(i, :) <= R(2, i) & data(i, :) >= R(1, i);
    ivalidIdx = find(~valid);
    outliers = data(i, ~valid);
    inliers = data(i, valid);
    MyFilledBox([i-width/2 i+width/2], [min(data(i, valid)) max(data(i, valid))], colors.outer);
    hold on;
%    MyBox([i-width/2 i+width/2], [R(1, i) R(2, i)], cmap(1, :));
    MyFilledBox([i-width/2 i+width/2], [Q(1, i) Q(3, i)], colors.inner);
    plot([i-width/2 i+width/2], [Q(2, i) Q(2, i)], '-', 'LineWidth', 2, 'color', colors.median);
    %plot(inliers * 0 + i, inliers, '.', 'MarkerFaceColor', cmap(1, :), 'MarkerEdgeColor', cmap(3, :));

    
    %plot(outliers * 0 + i, outliers, 'o', 'MarkerFaceColor', 'w', 'MarkerEdgeColor', 'k');
    for j=1:length(outliers)

%        plot(i, outliers(j), 'o', 'MarkerFaceColor', outcmap(ivalidIdx(j), :), 'MarkerEdgeColor', 'none');
        plot(i, outliers(j), 'o', 'MarkerFaceColor', 'k',  'MarkerEdgeColor', 'none');
        text(i, outliers(j), [' ' num2str(ivalidIdx(j))]);
        outliersIndex = [outliersIndex; i, ivalidIdx(j), outliers(j)];
    end
end
hold off;

return
function myBoxPlot(x, labels, colors)
if nargin < 2
    labels = {};
end
if nargin < 3
    colors = colormap(lines);
end
%x = x / x(end);
width = 0.2;
for i=length(x):-1:1
    drawBox(x(i), width, colors(i, :))
    hold on;
end
hold off;

prev = 0;
for i=1:length(x)
    if i <= length(labels)
        text(width/2, prev + (x(i) - prev) / 2, labels{i}, 'Color', 'w', 'VerticalAlignment', 'Middle', 'HorizontalAlignment', 'center');
    end
    prev = x(i);
end
axis([0 width 0 x(end)]);
try
    set(gca, 'XTick', [], 'YTick', x);
end
tickLabels = {};
for i=1:length(x)
    tickLabels{i} = sprintf('%5.2f (%5.1f%%)', x(i), x(i)/x(end)*100);
end
try
set(gca, 'XTick', [], 'YTick', x, 'YTickLabel', tickLabels, 'YAxisLocation', 'right');
end
function drawBox(height, width, color)
fill([0 width width 0], [0 0 height height], color)
