function features = MEFindPatternIndices(data, patterns, guess)
MEPrint('Looking for patterns in data\n');
[ndata, dim] = size(data);
if nargin > 2
    features = zeros(2, guess);
else
    features = [];
end
map = [];
offset = 0;
%%
nvals = max(patterns(:));
map = true(ndata, dim * nvals);
tic
for d=1:dim
    for v=1:nvals
        map(:, (d - 1) * nvals + v) = data(:, d) == v;
    end
end
toc
%%
for i=1:size(patterns, 1)
    i
    %%
tic
    idx = find(~isnan(patterns(i, :)));
    smap = zeros(1, length(idx));
    k = 1;
    for j=idx
        smap(k) = (j - 1) * nvals + patterns(i, j);
        k = k + 1;
    end
    toc
    indices = find(all(map(:, smap), 2));
    features(1, offset + 1:offset + length(indices)) = indices;
    features(2, offset + 1:offset + length(indices)) = i;
    offset = offset + length(indices);
end
%features = sparse(features);