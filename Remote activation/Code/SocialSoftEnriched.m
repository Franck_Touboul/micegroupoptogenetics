clf;
%%
options = social.options;
%TrackDefaults;
% filename = [options.output_path options.test.name '.social.mat'];
% load(filename);
%%
filename = [options.output_path options.test.name '.analysis.mat'];
if exist(filename) ~= 0
    load(filename);
end
%%
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts
%options.zones = {'Open', 'Feeder1', 'Feeder2', 'Water', '(SmallNest)', 'Labyrinth', 'BigNest', '(BigNest)'};
options.zones = social.zones.labels;
%
options.nBins = 15;
options.circadianMins = 120; % [mins]
% if isunix
%     error('options.circadianMins short');
% end
% warning('options.circadianMins short');
options.circadianNormalize = true;

%%
output.nrows = 2;
output.ncols = 2;

%%
dt = social.time(2) - social.time(1);
circadianFrames = min(options.circadianMins * 60 / dt, social.nData);
nCircEpochs = max(floor(social.nData / circadianFrames), 1);

analysis.soft.circ.mins = options.circadianMins;
%%
% ocmap = [78/205/196; 199,244,100; 227,50,88;127,15,255]/255;
% cmap = rgb2Lab(reshape(ocmap, options.nSubjects, 1, 3));
% cmap = reshape(cmap, options.nSubjects, 3, 1);
% aux.colormap = [];
% %cmap = hsv(256);
% scmap = rgb2Lab(reshape(social.colors, options.nSubjects, 1, 3));
% scmap = reshape(scmap, options.nSubjects, 3, 1);
% for i=1:length(social.colors)
%     d = [];
%     for j=1:length(social.colors)
%         d(j) = deltaE2000(scmap(i, :), cmap(j, :));
%     end
%     [~, idx] = min(d);
%     aux.colormap(i, :) = ocmap(idx, :);
% end
%%
% cmap(:,:,2) = 1;
% cmap(:,:,3) = 1;
% cmap = reshape(rgb2hsv(cmap), options.nSubjects, 3, 1);
cmap = social.colors;

% cmap = [];
% index = 1;
% step = .25;
% for i=0:step:1
%     for j=0:step:1
%         for k=0:step:1
%             cmap(index, :) = [i, j, k];
%             index = index + 1;
%         end
%     end
% end
% 
% for i=1:length(social.colors)
% %     c = social.colors(i, :);
% %     c = (c - min(c)) / (max(c) - min(c));
%      [~, mindex] = min(sum((cmap - repmat(social.colors(i, :), size(cmap, 1), 1)).^2, 2));
% %     
%      aux.colormap(i, :) = cmap(mindex, :);
% end

%% locations ..............................................................
currRow = 1;
mySubplot(output.nrows, output.ncols, currRow, 1:output.ncols);
warning('ignoring hidden events');

zoneHist = zeros(length(options.zones), options.nSubjects);
for i=1:length(options.zones)
    [id, sheltered] = getZoneId(social, options.zones{i});
    for s=1:options.nSubjects
        zoneHist(i, s) = sum(social.zones.all(s, :) == id & (sheltered | ~social.zones.hidden(s, :)));
        %zoneHist(i, s) = sum(social.zones.all(s, :) == id);
    end
end
for s=1:options.nSubjects
    missHist(s) = sum(social.zones.hidden(s, :) & ~social.zones.sheltered(s, :)) / length(social.zones.hidden(s, :)); 
end


zoneHist = zoneHist ./ repmat(sum(zoneHist), length(options.zones), 1);
analysis.soft.zones.labels = options.zones;
analysis.soft.zones.hist = zoneHist;
%
colormap(aux.colormap);
bar(zoneHist);
set(gca, 'XTickLabel', options.zones);
prettyPlot('location histogram');
freezeColors

%% velocities .............................................................
return
currRow = 2;
mySubplot(output.nrows, output.ncols, currRow, 1:2);
rowTitle(output.nrows, output.ncols, currRow, 'Velocities')

social.velocity = [zeros(options.nSubjects, 1) sqrt(diff(social.x, 1, 2).^2 + diff(social.y, 1, 2).^2)];
%
allVelocities = [];
velocities = [];
for s=1:options.nSubjects
    currVelocity = social.velocity(s, :);
    valid = ~(social.zones.hidden(s, :) | [false social.zones.hidden(s, 1:end-1)]);
%    currVelocity = currVelocity(aux.farFromHidden(s, :));
    analysis.soft.stat.velocities.mean(s) = mean(currVelocity(valid));
    analysis.soft.stat.velocities.std(s) = stderr(currVelocity(valid));
%    allVelocities(s) = mean(currVelocity);
end
%%
mySubplot(output.nrows, output.ncols, currRow, 1);
barweb(analysis.soft.stat.velocities.mean, analysis.soft.stat.velocities.std, [], [], [], [], [], social.colors);
% colormap(jet);
% bar([velocities; allVelocities]','grouped');
% prettyPlot('', 'subject');
% legend('non-zero', 'all');
% legend boxoff
% freezeColors;
%%
return
circVelocities = [];
%circAllVelocities = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:options.nSubjects
        currVelocity = social.velocity(s, circEpoch);
        valid = ~(social.zones.hidden(s, circEpoch) | [false social.zones.hidden(s, circEpoch(1):circEpoch(end) - 1)]);
        %currHidden = aux.hidden(s, circEpoch);
        analysis.soft.circ.velocities(c, s) = mean(currVelocity(valid));
        %circAllVelocities(c, s) = mean(currVelocity);
    end
end

mySubplot(output.nrows, output.ncols, currRow, 2);
for s=1:options.nSubjects
    plot(.5:nCircEpochs, analysis.soft.circ.velocities(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', 'k');
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
prettyPlot('velocities', 'time [mins]');
freezeColors;

% mySubplot(output.nrows, output.ncols, currRow, 2);
% for s=1:options.nSubjects
%     plot(.5:nCircEpochs, circAllVelocities(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', aux.colormap(s, :));
%     hold on;
% end
% hold off;
% for c=0:nCircEpochs
%     vert_line(c, 'color', 'k', 'linestyle', '--');
% end
% set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
% prettyPlot('circadian (all)', 'time [mins]');
% freezeColors;

%% food\water ...........................................................
currRow = 3;
mySubplot(output.nrows, output.ncols, currRow, 1);
rowTitle(output.nrows, output.ncols, currRow, 'Zones')

circFoodWater = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:options.nSubjects
        curr = social.zones.all(s, circEpoch);
        circFoodWater(c, s) = sum(...
            curr == getZoneId(social, 'Feeder1') | ...
            curr == getZoneId(social, 'Feeder2') | ...
            curr == getZoneId(social, 'Water'));
    end
end
if options.circadianNormalize
    circFoodWater = circFoodWater ./ repmat(sum(circFoodWater, 1), nCircEpochs, 1);
end

analysis.soft.circ.foodwater = circFoodWater;

for s=1:options.nSubjects
    plot(.5:nCircEpochs, circFoodWater(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', 'k');
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
prettyPlot('food\\water (circadian)', 'time [mins]');
freezeColors;

%% hidden ...............................................................
currRow = 3;
mySubplot(output.nrows, output.ncols, currRow, 2);

circHidden = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:options.nSubjects
        curr = social.zones.all(s, circEpoch);
        circHidden(c, s) = sum(...
            curr == getZoneId(social, '(SmallNest)') | ...
            curr == getZoneId(social, '(BigNest)'));
    end
end
if options.circadianNormalize
    circHidden = circHidden ./ repmat(sum(circHidden, 1), nCircEpochs, 1);
end

analysis.soft.circ.hidden = circHidden;

for s=1:options.nSubjects
    plot(.5:nCircEpochs, circHidden(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', 'k');
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
prettyPlot('hidden place (circadian)', 'time [mins]');
freezeColors;

%% labyrinth ..............................................................
currRow = 4;
mySubplot(output.nrows, output.ncols, currRow, 1);

circHidden = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:options.nSubjects
        curr = social.zones.all(s, circEpoch);
        circHidden(c, s) = sum(curr == getZoneId(social, 'Labyrinth') | curr == getZoneId(social, 'BigNest'));
    end
end
if options.circadianNormalize
    circHidden = circHidden ./ repmat(sum(circHidden, 1), nCircEpochs, 1);
end
analysis.soft.circ.labyrinth_bignest = circHidden;

for s=1:options.nSubjects
    plot(.5:nCircEpochs, circHidden(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', 'k');
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
prettyPlot('labyrinth\on big-nest (circadian)', 'time [mins]');
freezeColors;

%% openspace ..............................................................
currRow = 4;
mySubplot(output.nrows, output.ncols, currRow, 2);

circHidden = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:options.nSubjects
        curr = social.zones.all(s, circEpoch);
        circHidden(c, s) = sum(curr == getZoneId(social, 'Open'));
    end
end
if options.circadianNormalize
    circHidden = circHidden ./ repmat(sum(circHidden, 1), nCircEpochs, 1);
end
analysis.soft.circ.open = circHidden;

for s=1:options.nSubjects
    plot(.5:nCircEpochs, circHidden(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', 'k');
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
prettyPlot('open-space (circadian)', 'time [mins]');
freezeColors;

%%
filename = [options.output_path options.test.name '.soft'];
fprintf(['# - saving figure to file: ''' filename '''\n']);
saveFigure(filename);

filename = [options.output_path options.test.name '.analysis.mat'];
fprintf(['# - saving to file: ''' filename '''\n']);
save(filename, 'analysis');