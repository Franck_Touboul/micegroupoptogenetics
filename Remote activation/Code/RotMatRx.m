function m = RotMatRx(theta)
ct = cos(theta);
st = sin(theta);
m = [1 0 0; 0 ct -st; 0 st ct];
