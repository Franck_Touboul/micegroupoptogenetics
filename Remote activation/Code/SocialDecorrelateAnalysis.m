[obj, indepProbs, jointProbs] = SocialComputePatternProbsFast(obj);
%%
%zones = SimulateMarkovProcess(obj);
%zones = SimulateIndepProcess(obj);
%zones = ZoneNaiveShuffle(obj);
zones = ZoneMarkovShuffle(obj);
[curr, indepProbs, jointProbs] = SocialComputePatternProbsFast(obj);
%%
curr = obj;
curr.zones = zones;
curr.nFrames = size(zones, 2);
curr.valid = true(1, size(zones, 2));
curr.OutputToFile = false;
curr.OutputInOldFormat = false;
curr.Analysis = [];
[curr, mIndepProbs, mJointProbs] = SocialComputePatternProbsFast(curr);
%%
curr = SocialPotts(curr);
%%
shift = obj;
shift.zones = obj.zones(:, obj.valid);
shift.nFrames = size(shift.zones, 2);
shift.valid = obj.valid(obj.valid);
shift.Outpu

tToFile = false;
shift.OutputInOldFormat = false;
for s=1:obj.nSubjects
    r = randi(shift.nFrames);
    shift.zones(s, :) = [shift.zones(s, r:end) shift.zones(s, 1:r-1)];
end
shift = SocialPotts(shift);
%%
subplot(1,2,1);
PlotProbProb(jointProbs, mJointProbs, curr.nFrames);
subplot(1,2,2);
plot(sort(jointProbs(jointProbs > 0), 'descend'), 'b.-');
hon
plot(sort(mJointProbs(mJointProbs > 0), 'descend'), 'r.-');
hoff;
set(gca, 'YScale', 'log')