function SocialSimulatePottsTrain(obj)
obj = TrackLoad(obj);
me = obj.Analysis.Potts.Model{3};

rand('twister',sum(100*clock));
randn('seed',sum(100*clock));

r= 3;
while r == 3 || r == 1
    r = randi(4)
end
switch r
    case 1
        [me, output, ent] = TrainPottsUsingGIS(me)
    case 2
        [me, output, ent] = TrainPottsUsingBFGS(me)
    case 3
        [me, output, ent] = TrainPottsUsingSteepestDescent(me)
    case 4
        [me, output, ent] = TrainPottsUsingLBFGS(me)
end

%%
fprintf('@method ');
fprintf('%d ', r);
fprintf('\n');

fprintf('@fval ');
fprintf('%.3f ', output.fval);
fprintf('\n');

fprintf('@times ');
fprintf('%.3f ', output.times);
fprintf('\n');