function [ho, xo] = histpdf(varargin)

[h,x] = hist(varargin{:});
h = h / sum(h) / (x(2) - x(1));
if nargout == 0
    plot(x, h, 'x:');
else
    ho = h;
    xo = x;
end