function [vals, Groups] = SocialBatchRun(func, fields)
percent = true;
SocialExperimentData;
if nargin < 2
    fields = {'common'};
end
%%
vals = [];
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], fields);
        catch
            vals(id, day) = nan;
            continue;
        end
        %obj = func(obj);
        vals(id, day) = func(obj);
    end
end

%%
if nargout == 0
    %%
    colors = [];
    for g=1:length(Groups)
        colors = [colors; Groups(g).color];
        m = mean(vals(Groups(g).idx, :));
        s = stderr(vals(Groups(g).idx, :));
        subplot(1,3,1:2)
        if percent
            errorbar(1:nDays, m * 100, s * 100, 'LineWidth', 2, 'Color', Groups(g).color);
        else
            errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color);
        end
        %plot(1:nDays, all)
        hold on;
        tm(g) = mean(mean(vals(Groups(g).idx, :)));
        ts(g) = stderr(mean(vals(Groups(g).idx, :)));
    end
    hold off;
    legend(titles);
    legend boxoff
    set(gca, 'XTick', [1:4]);
    xlabel('times [days]');
        ylabel('% of time in shelter');
    
    subplot(1,3,3);
    if percent
        ylabel('% of time in shelter');
        barweb(tm * 100, ts * 100, [], [], [], [], [], colors);
    else
        barweb(tm, ts, [], [], [], [], [], colors);
    end
end