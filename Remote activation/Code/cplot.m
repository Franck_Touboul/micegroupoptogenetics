function cplot(varargin)
plot(varargin{:});
set(gca, 'XTick', [0 pi/4 pi/2 pi*3/4 pi], 'XTickLabel', {'0', '1/4 Pi', '1/2 Pi', '3/4 Pi', 'Pi'});
a = axis;
axis([0 pi a(3) a(4)]);
