d = dlmread('aux/SC.exp0001.day02.cam01.socialOptimization.table');

%%
nparams = 11;
res.parameters = d(:, 1:nparams);

res.approach.correct = d(:,  nparams+1);
res.approach.fp = d(:,  nparams+2);
res.approach.fn = d(:, nparams+3);
res.approach.count = d(:, nparams+4);

res.leave.correct = d(:, nparams+5);
res.leave.fp = d(:, nparams+6);
res.leave.fn = d(:, nparams+7);
res.leave.count = d(:, nparams+8);

res.chase.correct = d(:, nparams+9);
res.chase.fp = d(:, nparams+10);
res.chase.fn = d(:, nparams+11);
res.chase.count = d(:, nparams+12);

%%
res.chase.prob.fp = res.chase.fp ./ (res.chase.fp + res.chase.correct);
res.chase.prob.fn = res.chase.fn ./ (res.chase.fn + res.chase.correct);
cmap = MyCategoricalColormap;
plot(res.chase.prob.fp*100, 100 - res.chase.prob.fn*100, 'ko', 'MarkerFaceColor', cmap(1, :), 'MarkerEdgeColor', 'none');
ylabel('detection [%]');
xlabel('false-positive [%]');

%%
parameter = 'chase';
res.(parameter).prob.fp = res.(parameter).fp ./ (res.(parameter).fp + res.(parameter).correct);
res.(parameter).prob.fn = res.(parameter).fn ./ (res.(parameter).fn + res.(parameter).correct);
cmap = MyCategoricalColormap;
pfalse = res.(parameter).prob.fp*100;
pdetect = 100 - res.(parameter).prob.fn*100;
m1 = min(pdetect - pfalse);
m2 = max(pdetect - pfalse);
nbins = 20;
[h, b] = histc(pdetect - pfalse, sequence(m1, m2, nbins));
cmap = MyDefaultColormap(nbins);
cmap = cmap(end:-1:1, :);
for currb = unique(b(:)')
    plot(pfalse(b == currb), pdetect(b == currb), 'ko', 'MarkerFaceColor', cmap(currb, :), 'MarkerEdgeColor', 'k');
    hon
end
hoff
ylabel('detection [%]');
xlabel('false-positive [%]');
%
[q, best] = max(-(res.chase.prob.fp*100) + (100 - res.chase.prob.fn*100));
fprintf('false-positive: %f, detection: %f\n', res.chase.prob.fp(best)*100, 100 - res.chase.prob.fn(best)*100);
hon
plot(pfalse(best), pdetect(best), 'ks', 'MarkerFaceColor', cmap(nbins, :), 'MarkerEdgeColor', 'k', 'MarkerSize', 10);
hoff
axis([0 100 0 100])
res.parameters(best, :)

colormap(cmap);
h = colorbar;
set(h, 'YTick', [1 nbins+1], 'YTickLabel', {num2str(m1), num2str(m2)});

%seq = sequence(m1, m2, nbins); seq = [(seq(1:end-1) + seq(2:end)) / 2 seq(end)]; 
% entries = {};
% for i=get(h, 'YTick')
%     entries{i} = sprintf('%.1f', seq(i));
% end
% set(h, 'YTickLabel', entries)

% @arg PredObjectLength=175.000000
% @arg PreyObjectLength=100.000000
% @arg MinSubInteractionDuration=5.000000
% @arg MinIdleDuration=20.000000
% @arg ZoneOfContact=110.000000
% @arg EmissionMatch=0.500000
% @arg EmissionMismatch=0.150000
% @arg RelativeSpeedInChase=0.000000
% @arg ContactMismatch=0.400000
% @arg ChaseMinOverlap=8.000000

[s, o] = sort(-(res.chase.prob.fp*100) + (100 - res.chase.prob.fn*100));
o = o(end-9:end);
res.parameters(o(end:-1:1), :)

return
%%
cmap = MySubCategoricalColormap;
for i=1:size(res.parameters, 2)
    p = res.parameters(best, :);
    index = true(size(res.parameters, 1), 1);
    for j=1:size(res.parameters, 2)
        if i==j; continue; end
        index = index & res.parameters(:, j) == p(j);
    end
    pfalse = res.(parameter).prob.fp(index)*100;
    pdetect = 100 - res.(parameter).prob.fn(index)*100;
    [q, o]  = sort(pdetect - pfalse);
    hon;
    plot(pfalse(o), pdetect(o), 'ko-', 'MarkerFaceColor', 'none', 'MarkerEdgeColor', 'k', 'Color', cmap(i, :), 'LineWidth', 2);
    plot(80, 100 - i * 5, 'ko-', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none', 'Color', cmap(i, :), 'LineWidth', 2);
    fprintf('# parameter: %d, diff: %f\n', i, min(pdetect - pfalse) - max(pdetect - pfalse));
end
hoff;

return;
%%
[q, best] = max(-(res.chase.prob.fp*100) + (100 - res.chase.prob.fn*100));
fprintf('false-positive: %f, detection: %f\n', res.chase.prob.fp(best)*100, 100 - res.chase.prob.fn(best)*100);

defaults.PredObjectLength = res.parameters(best, 1);
defaults.PreyObjectLength = res.parameters(best, 2);
defaults.MinSubInteractionDuration = res.parameters(best, 3);
defaults.MinIdleDuration = res.parameters(best, 4);
defaults.ZoneOfContact = res.parameters(best, 5);
defaults.EmissionMatch = res.parameters(best, 6);
defaults.EmissionMismatch = res.parameters(best, 7);
defaults.RelativeSpeedInChase = res.parameters(best, 8);
defaults.ContactMismatch = res.parameters(best, 9);
defaults.ChaseMinOverlap = res.parameters(best, 10);
defaults.ChaseMinOverlapPercentage = res.parameters(best, 11);

f = fieldnames(defaults);
s = 'cat SocialOptimizeInteractionsVariation.m';
for i=1:length(f)
    s = [s, ' | sed "s/defaults.' f{i} ' =.*/defaults.' f{i} ' = ' num2str(defaults.(f{i})) '/g"' ];
end
system([s ' > soitemp; mv soitemp SocialOptimizeInteractionsVariation.m'])
