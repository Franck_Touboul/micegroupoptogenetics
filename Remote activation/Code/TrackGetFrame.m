function varargout = TrackGetFrame(obj, frame)
obj = TrackLoad(obj);
img = myMMReader(obj.VideoFile, frame);
if nargout == 0
    imshow(img);
else
    varargout{1} = img;
end
