%function obj = TrackFindBackgroundDeviation(obj)
obj = TrackLoad(obj);
%%
fprintf('# finding background image\n');
cdata = zeros(obj.VideoHeight, obj.VideoWidth, 3, obj.nBkgFrames, 'single');
fprintf('# - using random frames\n');
i = 0;
bkg = im2double(obj.BkgImage);
Q = bkg * 0;
step = bkg * 0 + .2;

thresh = 0.001;
expfactor = 1.2;
shrsfactor = .8;
prev = [];
nSteps = obj.nBkgFrames^2;
n = Reprintf('# - frame %d of %d', i, nSteps);
while i < obj.nBkgFrames^2
    m1 = 1;
    m2 = obj.nFrames;
    r = floor(rand(1) * (m2 - m1) + m1);
    if r * obj.dt < obj.StartTime; continue; end
    if obj.EndTime > 0 && r * obj.dt > obj.EndTime; continue; end
    frame = im2double(myMMReader(obj.VideoFile, r));
    if ~isempty(frame) && TrackIsDark(obj, frame, .2)
        map = abs(frame - bkg) > Q;
        if isempty(prev)
            prev = map;
        end
        step(prev ~= map) = step(prev ~= map) * shrsfactor;
        step(prev == map) = step(prev == map) * expfactor;
        Q = Q + (2 * map - 1) .* step;
        prev = map;
        ratio = mean(step(:) < thresh);
        n = Reprintf(n, '# - frame %d of %d (converge: %.2f)', i, nSteps, ratio);
        i = i + 1;
        imagesc((Q-min(Q(:))) / (max(Q(:)) - min(Q(:))));
        drawnow;
        if ratio == 1
            %break;
            disp 'a'
        end
    end
end
fprintf('\n# done\n');