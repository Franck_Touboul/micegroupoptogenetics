function [obj, img] = SocialShowFrame(obj, frame, isHidden)
if ~exist('isHidden', 'var')
    isHidden = false;
end


obj = TrackLoad(obj);
%%
subplot(1,2,2)
obj.OutputToFile = false;
obj.OutputInOldFormat = false;
TrackColorSegment(obj, frame);
%%
img.orig = myMMReader(obj.VideoFile, frame);
if isHidden
    figure('visible', 'off');
end
subplot(1,2,1)
imshow(img.orig);
hold on;
radios = 5;
white = [.9 .9 .9];
for i=1:obj.nSubjects
    x = double(obj.x(i, frame));
    y = double(obj.y(i, frame));
    color = obj.Colors.Centers(i, :);
    hsv = rgb2hsv(color);
    hsv(3) = min(hsv(3) * 1.5, 1);
    color = hsv2rgb(hsv);
    plot(x, y, 'o', 'MarkerFaceColor',color, 'MarkerEdgeColor', [0 0 0], 'MarkerSize', radios * 2 + 1, 'LineWidth', 2);
    plot(x, y, 'o', 'MarkerFaceColor',color, 'MarkerEdgeColor', white, 'MarkerSize', radios * 2, 'LineWidth', 2);
    text(x+1, y-1.5*radios+1, obj.ROI.ZoneNames{obj.zones(i, frame)}, 'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Bottom', 'Color', 'k', 'FontSize', radios*3, 'FontWeight', 'bold')
    text(x, y-1.5*radios, obj.ROI.ZoneNames{obj.zones(i, frame)}, 'HorizontalAlignment', 'Center', 'VerticalAlignment', 'Bottom', 'Color', white, 'FontSize', radios*3, 'FontWeight', 'bold')
end
axis off;
hold off;
