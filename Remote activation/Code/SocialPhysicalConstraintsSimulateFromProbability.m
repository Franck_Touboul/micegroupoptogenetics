%function SocialPhysicalConstraintsSimulateFromProbability
res = [];
options.N = 1000000;
options.order = 1;
%%
local = SocialPottsSimulation(obj, options.order, options.N);
res.full = SocialPotts(local);

limits = [4, 2, 2, 2, 2, 2, 2, 2, 4, 2];
zones = local.zones;
for z=1:obj.ROI.nZones
    zones = zones(:, sum(zones == z) <= limits(z));
end
local.zones = zones;
res.restricted = SocialPotts(local);
