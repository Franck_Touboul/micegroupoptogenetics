
options.nSubjects = 4;           % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts
options.nIters = 500;
options.confidenceIters = 0;

%% Loading the Excel file for all mice (if needed)
loadData;
options.count = zones.count;
%data = zones.all(:,1:floor(end/4));
%data = zones.all(:,floor(3/4*end):end);
%

%% train potts model
data = zones.all;
%data = testPottsModel(options, size(zones.all, 2));
me = {};
for level=1:options.nSubjects
    options.n = level;
    me{level} = trainPottsModel(options, data);
end

%% compute independent probabilities
independentProbs = computeIndependentProbs(data, zones.count);

%% compute joint probabilities
[jointProbs, jointPci] = computeJointProbs(data, zones.count);

%% compute probability for each sample
fprintf('# computing joint probabilities : ');
[sampleProbIndep, sampleProbJoint] = computeSampleProb(data, zones.count, independentProbs, jointProbs);
fprintf('mean log-likelihood = %6.4f\n', mean(log(sampleProbJoint)));
%% compute MaxEnt probabilities for each sample
Entropy = [];
for level=1:options.nSubjects
    fprintf('# computing max-entropy probabilities for level %d\n', level);
    p = exp(me{level}.features * me{level}.weights');
    perms_p = exp(me{level}.perms * me{level}.weights');
    Z = sum(perms_p);
    p = p / Z;
    Entropy(level) = mean(log2(p));
    %% plot
    subplot(3,2,level);
    b1 = sampleProbJoint;
    b2 = sampleProbIndep;
    
    p1 = sampleProbJoint;
    p2 = p;
    % confidence
    up1 = [1e-6 unique(p1)];
    counts = up1 * size(data, 2);
    [phat,pci] = binofit(counts, size(data, 2));
    myCofidencePlot(up1, pci(:, 1)', pci(:, 2)');
    hold on;
    
    %
    if options.confidenceIters > 0
        x = conf.p; rx = flipdim(x, 2);
        y = conf.mean + 2 * conf.std;
        ry = flipdim(conf.mean - 2 * conf.std, 2);
        ry(ry <= 0) = realmin;
        
        fill([x, rx(ry > 0), x(1)], [y, ry(ry > 0), y(1)], [237, 116, 116]/256, 'EdgeColor', 'None');
        %    fill([x, rx(ry > 0), x(1)], [y, ry(ry > 0), y(1)], [247, 218, 124]/256, 'EdgeColor', 'None');
        hold on;
    end
    h1 = plot(b1, b2, '.', 'Color', [.6 .6 .6]);
    hold on;
    h2 = plot(p1, p2, '.');
    hold off;
    set(gca, 'YScale', 'log', 'XScale', 'log');
    
    minLog = floor(log10(min(min(p1), min(p2))));
    maxLog = ceil(log10(max(max(p1), max(p2))));
    axis([10^minLog 10^maxLog 10^minLog 10^maxLog]);
    line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);
    
    axis([...s
        10^floor(log10(min(p1))) ...
        10^ceil(log10(max(p1))) ...
        10^floor(log10(min(p2))) ...
        10^ceil(log10(max(p2))) ...
        ]);
    
    tit = ['Maximum-Entropy Model (me-prob=' num2str(mean(log(p))) ', joint-prob' num2str(mean(log(sampleProbJoint))), ')'];
    title(tit);
    xlabel('Joint');
    ylabel('ME');
    
    %prettyPlot(tit, 'Joint', 'ME');
    legend([h1, h2], 'Independent', 'ME', 'location', 'SouthEast'); legend boxoff
end

%%
In = Entropy(end) - Entropy(1);
Ik = diff(Entropy);
subplot(3,2,5);
%pie(Ik/In, {'pairs', 'triples', 'quadruples'});
myBoxPlot(cumsum(Ik), {'Pairwise (I_{(2)})', 'Triplet (I_{(3)})', 'Quadruplet (I_{(4)})'});
title('K''th-Order Correlations');