function plot_gmm(m)

if (m.nin ~= 1)
    error 'currently support only 1D mixtures'
end

r(1) = min(m.centres - 3 * sqrt(m.covars));
r(2) = max(m.centres + 3 * sqrt(m.covars));
dr = (r(2)-r(1))/1000;
R=r(1):dr:r(2);

colors = ['r'; 'g'; 'b'; 'c'; 'm'; 'y'; 'k'; 'w'];

j=cool;
s = zeros(length(R), 1);
for i=1:m.ncentres
    x = gauss(m.centres(i, :), m.covars(i, :), R');
    plot(R, x * m.priors(i), '--', 'Color', min(j(int8((i-1)/(m.ncentres-1) * (length(j)-1))+1, :) + [0.3 0.3 0.3], [1 1 1]), 'LineWidth', 1);
    s = s + x * m.priors(i);
    hold on;
end
plot(R, s, 'k', 'LineWidth', 2);
hold off;
