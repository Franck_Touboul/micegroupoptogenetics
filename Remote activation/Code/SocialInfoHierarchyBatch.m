% AllGroups = {...
%     'Enriched.exp0006.day%02d.cam01',...
%     'Enriched.exp0005.day%02d.cam04',...
%     'Enriched.exp0001.day%02d.cam04',...
%     'SC.exp0001.day%02d.cam01',...
%     'Enriched.exp0002.day%02d.cam04',...
%     'SC.exp0002.day%02d.cam01',...
%     'Enriched.exp0003.day%02d.cam01',...
%     'SC.exp0003.day%02d.cam01',...
%     'Enriched.exp0004.day%02d.cam01',...
%     'SC.exp0004.day%02d.cam04',...
%     'SC.exp0005.day%02d.cam04',...
%     'SC.exp0006.day%02d.cam04',...
%     };
SocialExperimentData
AllGroups = experiments;
%%
output = false;
a = AllGroups;
seq = 1:obj.nSubjects;
totalUp = 0;
totalDown = 0;
nDays = 4;
upmat = zeros(length(a), nDays);
downmat = zeros(length(a), nDays);

infoweights = zeros(length(a), nDays, obj.nSubjects^2);
ppweights = zeros(length(a), nDays, obj.nSubjects^2);
ppweights1 = ppweights;
ppweights2 = ppweights;
ppweights3 = ppweights;
ppweights4 = ppweights;
for i=1:length(experiments)
    all = {};
    objs = {};
    %%
    arank = [];
    for day = 1:nDays;
        %%
        ppfile = [];
        infofile = [];
        prefix = sprintf(a{i}, day);
        fprintf('# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions', 'zones'});
            if day==1
                objs{day} = obj;
            end
            %%
            cEnt = zeros(obj.nSubjects);
            Ent = zeros(1, obj.nSubjects);
            tcEnt = zeros(1, obj.nSubjects);
            for m1=1:obj.nSubjects
                for m2=1:obj.nSubjects
                    z1 = obj.zones(m1, :);
                    if m1 == m2;
                        Ent(m1) = JointEntropy(z1);
                        %tcEnt(m1) = ConditionalEntropy(obj.zones([m1 seq(seq ~= m1)], :));
                        continue;
                    end;
                    %                     z1 = obj.zones(m1, ~obj.sheltered(m1, :) & ~obj.sheltered(m2, :));
                    %                     z2 = obj.zones(m2, ~obj.sheltered(m1, :) & ~obj.sheltered(m2, :));
                    
                    z2 = obj.zones(m2, :);
                    cEnt(m1, m2) = ConditionalEntropy([z2; z1]);
                    %cEnt(m1, m2) = ConditionalEntropy(obj.zones([m2 seq(seq ~= m2)], :)) ./ ConditionalEntropy(obj.zones([m2 seq(seq ~= m1 & seq ~= m2)], :));
                end
            end
            %%
            %              mat = cEnt ./ repmat(Ent, 4, 1);
            %              mat = mat - mat';
            mat = cEnt - cEnt';
            
            currmat = mat; currmat(1:obj.nSubjects+1:end) = nan;
            infoweights(i, day, :) = currmat(:);
            
            mat = mat .* (mat > 0);
            [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
            arank = [arank; rank];
            %TrackSave(obj);
            %rank = - (tcEnt ./ Ent);
            [~, bg] = SocialGraph(obj, mat, removed, rank);
            if output
                g = biograph.bggui(bg);
                f = get(g.biograph.hgAxes, 'Parent');
                infofile = [first.OutputPath 'info4.' prefix '.SocialInfoHierarchy.png'];
                print(f, infofile, '-dpng');
                close(g.hgFigure);
            end
            %%
            obj = SocialAnalysePredPreyModel(obj);
            curr = obj.Interactions.PredPrey;
            
            matPostPred = curr.PostPred - curr.PostPred';
            matPrePred  = curr.PrePred - curr.PrePred';
            matPrePrey  = curr.PrePrey - curr.PrePrey';
            matPostPrey = curr.PostPrey - curr.PostPrey';
            
 %           mat = matPostPred - matPrePred - matPostPrey; % - matPrePred - matPostPrey - matPrePrey;
%            mat = obj.Interactions.PredPrey.PostPredPrey; % - matPrePred - matPostPrey - matPrePrey;
            mat = curr.PostPredPrey - curr.PostPredPrey';
            %mat = mat .* (mat > 0);
            
            currmat = mat; 
            %currmat(binotest(curr.PrePred, curr.PrePred + curr.PrePred')) = nan;
            currmat(binotest(curr.PostPredPrey, curr.PostPredPrey + curr.PostPredPrey')) = nan;
            currmat(1:obj.nSubjects+1:end) = nan;
            ppweights(i, day, :) = currmat(:);
            ppweights1(i, day, :) = matPostPred(:) .* (currmat(:) ./ currmat(:));
            ppweights2(i, day, :) = matPostPrey(:) .* (currmat(:) ./ currmat(:));
            ppweights3(i, day, :) = matPrePred(:) .* (currmat(:) ./ currmat(:));
            ppweights4(i, day, :) = matPrePrey(:) .* (currmat(:) ./ currmat(:));
            
            mat = mat .* (mat > 0);
            mat(binotest(curr.PostPred, curr.PostPred + curr.PostPred')) = 0;
            %             mat(binotest(curr.PostPred, curr.PostPred + curr.PostPred') & ...
            %                 binotest(curr.PostPrey, curr.PostPrey + curr.PostPrey')) = 0;
%             mat(binotest(curr.PrePred, curr.PrePred + curr.PrePred') & ...
%                 binotest(curr.PostPred, curr.PostPred + curr.PostPred') & ...
%                 binotest(curr.PostPrey, curr.PostPrey + curr.PostPrey')) = 0;
            
            %             mat = curr.PostPred - curr.PostPred';
            %             mat(binotest(curr.PostPred, curr.PostPred + curr.PostPred')) = 0;
            
            szc = sum(curr.Contacts);
            sz = sum(curr.PostPred, 2)';
            
            [~, bg] = SocialGraph(obj, mat, [], rank);
            
            if output
                g = biograph.bggui(bg);
                f = get(g.biograph.hgAxes, 'Parent');
                ppfile = [first.OutputPath 'predprey.' prefix '.SocialAnalysePredPreyModel_v2.png'];
                print(f, ppfile, '-dpng');
                close(g.hgFigure);
            end
            %%
            conn = (mat ~= 0) * 1;
            conn(mat == 0) = nan;
            up = sum(max(conn .* repmat(rank, obj.nSubjects, 1), [], 2) > rank');
            totalUp = totalUp + up;
            down = sum(max(conn .* repmat(rank, obj.nSubjects, 1), [], 2) <= rank');
            totalDown = totalDown + down;
            upmat(i, day) = up;
            downmat(i, day) = down;
            %%
            if output
                
                f1 = imread(infofile);
                f2 = imread(ppfile);
                f = [ImgAutoCrop(f1), ImgAutoCrop(f2)];
                file = [first.OutputPath 'info.' prefix '.SocialAnalysePredPreyModel_v2.png'];
                delete(infofile);
                delete(ppfile);
                imwrite(f, file);
            end
            %%
        catch me
            fprintf(['#   . FAILED! ' me.message '\n']);
            for j=1:length(me.stack)
                fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
            end
        end
    end
end

%%
v = false(size(upmat));
v(logical([1 0 1 0 1 0 1 0 0 0 0]), :) = true;
Ecorr = mean(upmat(v) ./ (downmat(v) + upmat(v)));

v = false(size(upmat));
v(~logical([1 0 1 0 1 0 1 0 0 0]), :) = true;
SCcorr = mean(upmat(v) ./ (downmat(v) + upmat(v) + ((downmat(v) + upmat(v)) == 0)));

allperms = myPerms(length(AllGroups),2) - 1;
c = nan(1, size(allperms, 1));
for i=1:size(allperms, 1)
    v = ~isnan(upmat);
    v(logical(allperms(i, :)), :, :) = false;
    try
        c(i) = mean(upmat(v) ./ (downmat(v) + upmat(v)));
    catch
    end
end
hist(c, 40)
    vert_line(Ecorr, 'Color', 'b', 'LineStyle', '--');
    vert_line(SCcorr, 'Color', 'r', 'LineStyle', '--');

    legend('all permutations', 'enriched', 'standard conditions', 'location', 'northwest')
    legend boxoff
    
    xlabel('ratio match');
    ylabel('count');

%%
titles = {'Post-Pred', 'Post-Prey', 'Pre-Pred', 'Pre-Prey'};
for q = 1:4;
    figure(q);
    currweights = eval(['ppweights' num2str(q)]);
    
    subplot(3, 4, 1);
    
    v = ~isnan(ppweights4);
    v(logical([1 0 1 0 1 0 1 0 0 0 0]), :, :) = false;
    SCcorr = corr(infoweights(v), currweights(v));
    
    plot(infoweights(v), currweights(v), 'r.');
    hold on;
    
    v = ~isnan(ppweights4);
    v(~logical([1 0 1 0 1 0 1 0 0 0]), :, :) = false;
    Ecorr = corr(infoweights(v), currweights(v));
    
    plot(infoweights(v), currweights(v), 'b.');
    hold off;
    
    allperms = myPerms(length(AllGroups),2) - 1;
    c = nan(1, size(allperms, 1));
    for i=1:size(allperms, 1)
        v = ~isnan(currweights);
        v(logical(allperms(i, :)), :, :) = false;
        try
            c(i) = corr(infoweights(v), currweights(v));
        catch
        end
    end
    subplot(3, 4, [2:4, 6:8, 10:12]);
    hist(c, 100)
    vert_line(Ecorr, 'Color', 'b', 'LineStyle', '--');
    vert_line(SCcorr, 'Color', 'r', 'LineStyle', '--');
    
    legend('all permutations', 'enriched', 'standard conditions', 'location', 'northwest')
    legend boxoff
    
    xlabel('correlation');
    ylabel('count');
    
    title(titles{q});
    saveFigure(['Res/SocialInfoHierarchyBatch.' titles{q}]);
end

%v(logical([1 0 1 0 1 0 1 0 0 0 0]), :, :) = false;
%a = [corr(infoweights(v), ppweights1(v)), ...
% corr(infoweights(v), ppweights2(v)), ...
% corr(infoweights(v), ppweights3(v)), ...
% corr(infoweights(v), ppweights4(v))]
% corr(infoweights(v), a(1) * ppweights1(v) + a(2) * ppweights2(v) + a(3) * ppweights3(v) + a(4) * ppweights4(v))

%%
v = repmat(repmat(Groups(1).map', 1, nDays), [1,1,obj.nSubjects^2]);
i = infoweights(v);
w = ppweights(v);
plot(i(abs(w)<100), w(abs(w)<100), '.')

%%
p = polyfit(infoweights(v), ppweights(v), 1);
a = axis; x = a(1):0.01:a(2);
plot(infoweights(:), ppweights(:), '.', x, polyval(p, x));

x = ppweights(v);
X = [x*0+1 x];
y = infoweights(v);
%v(abs(ppweights) > 150) = false; 
[r, ~, ~, ~, stat] = regress(y, X); 
plot(x, y, '.', x, X * r, '.');
stat