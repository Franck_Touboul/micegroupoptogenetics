function SocialVelocityPlot(obj)

%%
nBins = [20 40];
x1 = min(obj.x(:));
x2 = max(obj.x(:))+1;
y1 = min(obj.y(:));
y2 = max(obj.y(:))+1;
%%
X = sequence(x1, x2, nBins(1)+1);
Y = sequence(y1, y2, nBins(2)+1);
for s=1:obj.nSubjects
    %%
    [q, ix] = histc(obj.x(s, :), X);
    [q, iy] = histc(obj.y(s, :), Y);
    dx = [diff(obj.x(s, :)) 0] / obj.dt;
    dy = [diff(obj.y(s, :)) 0] / obj.dt;
    valid = ~(obj.sheltered(s, :) | [obj.sheltered(s, 2:end) true]);
    
%     dx = dx(valid);
%     dy = dy(valid);
%     ix = ix(valid);
%     iy = iy(valid);
    %%
    dX = zeros(nBins);
    dY = zeros(nBins);
    for i=1:nBins(1)
        for j=1:nBins(2)
            map = ix == i & iy == j;
            dX(i, j) = median(dx(map));
            dY(i, j) = median(dy(map));
        end
    end
    %%
    [x, y] = meshgrid(Y(1:end-1), X(1:end-1));
%    quiver(x(:, 2:end), y(:, 2:end), dX(:, 2:end), dY(:, 2:end));
    zdX = dX ./ sqrt(dX.^2+dY.^2);
    zdY = dY ./ sqrt(dX.^2+dY.^2);
    quiver(x, y, dX, dY);
end
