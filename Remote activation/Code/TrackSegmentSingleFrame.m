function TrackSegmentSingleFrame(options, frame)
%framenum = 358969;
%%
%options.name = 'NewArena.exp0006.day03.cam01';
options.name = 'Enriched.exp0005.day03.cam04';
range = 1064036;

fprintf('# segmenting movie frames:\n');
if ~license('checkout', 'image_toolbox')
    fprintf('# - waiting for image processing toolbox license\n');
    while ~license('checkout', 'image_toolbox'); pause(5); end
end
TrackDefaults;
options.basepath = 'Z:\tests\SocialMice\';

options.MovieFile = ['Z:\tests\SocialMice\Movies\' options.name '.avi'];
basename = regexprep(regexprep(options.MovieFile, '^.*\\', ''), '\.[^\.]*$', '');

rawtrack_filename = [options.basepath basename '\Res\Trial.raw-track.mat'];
if ~(exist('track', 'var') && isfield(track, 'file') && strcmp(track.file, rawtrack_filename)) && exist(rawtrack_filename, 'file')
    fprintf('# loading raw-track\n');
    track = load(rawtrack_filename);
    track.file = rawtrack_filename;
    res = track.res;
end

social_filename = [options.basepath basename '\Res\Trial.social.mat'];
if ~(exist('social', 'var') && isfield(social, 'file') && strcmp(social.file, social_filename)) && exist(social_filename, 'file')
    fprintf('# loading social\n');
    use_social = true;
    load(social_filename);
    social.file = social_filename;
end

fprintf('# - loading meta data\n');
meta_filename = [options.basepath basename '\Res\Trial.meta.mat'];
%waitforfile(filename);
load(meta_filename);

%filename = [options.output_path options.test.name '.track.mat'];
%load(filename);
%%
fprintf('# segmenting frames\n');
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nFrames = xyloObj.NumberOfFrames;
height = xyloObj.Height;
width = xyloObj.Width;
dt = 1/xyloObj.FrameRate;
options.output = 1;
%%
% filename = [options.output_path options.test.name '.social.mat'];
% if exist(filename, 'file')
%     load(filename);
%     use_social = true;
% else
%     use_social = false;
% end
%%

%%
    startframe = 1;
    endframe = nFrames;
nFrames = endframe - startframe + 1;
%%
prevProps = [];
sx = []; sy = [];

%%
doubleBkgFrame = im2double(meta.bkgFrame);
bkgNoise = std(doubleBkgFrame(:));
cmap = meta.subject.centerColors;
%%
nchars = RePrintf('# - frame %6d [%d-%d] (%6.2fxiRT)', startframe, startframe, endframe, 0); 
tic;
%for r=696639
bkgFrame = im2double(meta.bkgFrame);
for r=range 
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d [%d-%d] (%6.2fxiRT)', r+startframe-1, startframe, endframe, RT);
    currTime = (r+startframe-1) * dt;
    if isfield(options, 'movieStartTime') && currTime < options.movieStartTime;
        continue;
    end
    if isfield(options, 'movieEndTime') && options.movieEndTime > 0 && currTime > options.movieEndTime
        continue;
    end
    
    m = myMMReader(options.MovieFile, r+startframe-1, meta.bkgFrame);
    if options.output
        orig = m;
    end
    m = imsubtract(m, meta.bkgFrame);
    m = imresize(m, options.scale);

    if isempty(sx)
        [sx, sy, nc] = size(m);
    end

    %%
    hsv_m = rgb2hsv(m);
    hm = hsv_m(:,:,1);
    sm = hsv_m(:,:,2);
    vm = hsv_m(:,:,3);

    meanBKG(1) = mean(hm(:)); stdBKG(1) = std(hm(:));
    meanBKG(2) = mean(sm(:)); stdBKG(2) = std(sm(:));
    meanBKG(3) = mean(vm(:)); stdBKG(3) = std(vm(:));
    bw = hm > meanBKG(1) + options.noiseThresh * stdBKG(1) | ...
        sm > meanBKG(2) + options.noiseThresh * stdBKG(2) | ...
        vm > meanBKG(3) + options.noiseThresh * stdBKG(3);
    hm = bw .* hm;
    sm = bw .* sm;
    vm = bw .* vm;
    
    %%
    %bw = sum(m > 0, 3);
    %bw = logical((hm + sm + vm) > 0);

    %%
    %bw = sum(m > 0, 3);
    % remove bad frames
    cc = bwconncomp(bw);
    imagesc(bw);
    if cc.NumObjects > options.maxNumObjects
        continue;
    end    
    bw = bwareaopen(bw, options.minNumPixels);
    %labels = bwlabel(bw);
    %nobjects = max(labels(:));
    %%
    [b, idx_h] = histc(hm(bw), meta.subject.colorBins);
    [b, idx_s] = histc(sm(bw), meta.subject.colorBins);
    [b, idx_v] = histc(vm(bw), meta.subject.colorBins);
    prob_h = zeros(options.nSubjects, length(idx_h));
    prob_s = zeros(options.nSubjects, length(idx_s));
    prob_v = zeros(options.nSubjects, length(idx_v));
    for i=1:options.nSubjects
        prob_h(i, :) = meta.subject.h(i, idx_h);
        prob_s(i, :) = meta.subject.s(i, idx_s);
        prob_v(i, :) = meta.subject.v(i, idx_v);
    end
    %     prob_h = prob_h ./ repmat(sum(prob_h, 1), options.nSubjects, 1);
    %     prob_s = prob_s ./ repmat(sum(prob_s, 1), options.nSubjects, 1);
    %     prob_v = prob_v ./ repmat(sum(prob_v, 1), options.nSubjects, 1);
    %     joint_prob = prob_h .* prob_s .* prob_v;
    joint_prob = prob_h .* prob_s .* prob_v;
    joint_prob = joint_prob ./ repmat(sum(joint_prob, 1), options.nSubjects, 1);
    
    [m, idx] = max(joint_prob, [], 1);
    nlabels = zeros(sx,sy,'uint8');
    nlabels(bw) = idx;
    
    for k=1:options.nSubjects
        logprobmap{k} = zeros(sx,sy);
        logprobmap{k}(bw) = flog(joint_prob(k, :));
    end
    
    flabels = zeros(sx,sy,'uint8');
    %%
    centIndex = 1;
    for i=1:options.nSubjects
        %%
        %curr = bwareaopen(curr, options.minNumPixels);
        reg = bwconncomp(nlabels == i);
        %        reg = bwconncomp(curr);
        %conn = regionprops(reg, 'Solidity', 'MajorAxisLength', 'MinorAxisLength');
        conn = regionprops(reg, 'Solidity', 'Centroid');
        for j=1:length(reg.PixelIdxList)
            %             if length(reg.PixelIdxList{j}) < options.minNumPixels
            %                 reg.PixelIdxList{j} = [];
            %             end
            if conn(j).Solidity < options.solidity || ...
                    length(reg.PixelIdxList{j}) < options.minNumPixels
                reg.PixelIdxList{j} = [];
            else
                cents.x(1, centIndex) = conn(j).Centroid(1);
                cents.y(1, centIndex) = conn(j).Centroid(2);
                cents.label(1, centIndex) = i;
                cents.area(1, centIndex) = length(reg.PixelIdxList{j});
                for k=1:options.nSubjects
                    cents.logprob(1, centIndex, k) = sum(logprobmap{k}(reg.PixelIdxList{j}));
                end
                cents.logprob(1, centIndex, :) = cents.logprob(1, centIndex, :) - flog(sum(exp(cents.logprob(1, centIndex, :))));
                centIndex = centIndex + 1;
                if centIndex > options.maxNumCents
                    break;
                end
            end
            %             if conn(j).Solidity < .5 || ...
            %                     length(reg.PixelIdxList{j}) < options.minNumPixels || ...
            %                     length(reg.PixelIdxList{j}) / (conn(j).MajorAxisLength * conn(j).MinorAxisLength * pi/4) < 0.5
            %                 reg.PixelIdxList{j} = [];
            %             end
        end
        if centIndex > options.maxNumCents
             cents.label(1, :) = 0;
        end

        if options.output
            img = labelmatrix(reg);
            if 1 == 2
                subplot(1,2,i);
                imagesc(img > 0);
            end
            flabels(img > 0) = i;
        end
    end
    %%
    if options.output
        subplot(1,2,2);
        rgblbls = label2rgb(flabels, cmap);
        imagesc(rgblbls);
        hold on;
        for q=1:options.nSubjects
            %plot(res{q}.x(r),  double(height * options.scale - res{i}.y(r)), 'x');
%            plot(res{q}.x(r),  double(size(orig, 1)* options.scale - res{q}.y(r)), 'x');
            if use_social
                plot(res{q}.x(r),  double(res{q}.y(r)), 'x', 'color', social.colors(q, :));
            else
                plot(res{q}.x(r),  double(res{q}.y(r)), 'x');
            end
        end
        if use_social
            for q=1:options.nSubjects
                plot(social.x(q, r)*options.scale,  double(social.y(q, r))*options.scale, 's', 'MarkerFaceColor', social.colors(q, :), 'MarkerEdgeColor', social.colors(q, :));
            end
        end            
        hold off;
        subplot(1,2,1);
        imagesc(orig);
        drawnow
        
    end
    %%
    pause(.1);
end
