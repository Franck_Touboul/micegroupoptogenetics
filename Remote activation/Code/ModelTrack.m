clear obj;

obj = Kinect('d:\5cage.1months.2012-12-02 125904.1.avi');
%%
obj.Meta.NumBkgFrames = 100;

obj.Meta.HeightThresh = 0.020; % [m]
obj.Meta.MinMouseArea = 10 / 100^2; %[m^2]

obj.Meta.MaxHeightThresh = 0.10;          % [m] for display
obj.Meta.MaxNegativeHeightThresh = -0.01; % [m] for display


%% Find background & meta data
nchars = 0;
first = true;
for i=1:obj.Meta.NumBkgFrames
    nchars = Reprintf(nchars, '# - frame no. %d/%d', i, obj.Meta.NumBkgFrames);
    obj.FrameNumber = randi(obj.NumberOfFrames);
    [obj, x,y,z] = obj.GetRectifiedRealWorldCoords;
    x(z == 0) = nan;
    y(z == 0) = nan;
    z(z == 0) = nan;
    if i == 1
        Z = z;
    else
        Z = cat(3, Z, z);
    end

    if first
        obj.Meta.Plane.Depth   = nanmedian(z(:));
        onplane = z < obj.Meta.Plane.Depth   + .01 & z > obj.Meta.Plane.Depth - .01;
        onplaneIdx = find(onplane & ~isnan(x) & ~isnan(y));
        r = randi(length(onplaneIdx), [2, 5000]);
        r = onplaneIdx(r);
        pxdist = sqrt(diff(obj.X(r)).^2 + diff(obj.Y(r)).^2);
        rwdist = sqrt(diff(x(r)).^2 + diff(y(r)).^2);
        obj.Meta.Plane.Resolution = median((rwdist) ./ pxdist); % [m/pix]
        first = false;
    end
end
obj.Meta.Bkg = nanmedian(Z, 3);
surfl(x, y, obj.Meta.Bkg);
shading interp; colormap(gray);
obj.Meta.BkgDepth = nanmedian(obj.Meta.Bkg(:));
%%
bkg = obj.Meta.Bkg;
bkg(isnan(obj.Meta.Bkg)) = obj.Meta.BkgDepth;
first = true;
for i=50:obj.NumberOfFrames
    %%
    obj.FrameNumber = i;
    [obj, x,y,z] = obj.GetRectifiedRealWorldCoords;
    x(z == 0) = nan;
    y(z == 0) = nan;
    z(z == 0) = nan;
    height = bkg-z;
    map = height > obj.Meta.HeightThresh;
    map = bwareaopen(map, round(obj.Meta.MinMouseArea / obj.Meta.Plane.Resolution^2));
    if first
        lbl = bwlabel(map);
        for l=1:max(lbl(:))
            ModelVec = ModelToDepthFit([], struct('X', x, 'Y', y, 'Z', height), lbl == l, opt);
            %%
%             ModelVec(6) = -0.045;
%             ModelVec(7) = 1.55;
%             ModelVec(3) = 0.265;
%             ModelVec(4) = -0.040;
%             ModelVec(1) = 0.80;
            plot3(x(lbl==l), y(lbl==l), height(lbl==l), '.'); axis equal
            hon
            [~, pos] = ModelToDepthDistance(ModelVec); 
            plot3(pos(:, 1), pos(:, 2), pos(:, 3), 'r.'); 
            hoff
            view(0, 90)
        end
    end
    
    %%
    subplot(2,2,1);
    [v1, v2] = view;
    im = obj.Meta.BkgDepth - z;
    im(im > obj.Meta.MaxHeightThresh) = nan;
    im(im < obj.Meta.MaxNegativeHeightThresh) = nan;
    surfl(x, y, im);
    shading interp; colormap(gray);
    axis([-.4 .4 -.4 .4 -.02 .1])
    view(v1, v2);
    %%
    subplot(2,2,2);
    imagesc(im);
    %%
    subplot(2,2,3);
    imagesc(map);
    title(num2str(i));
end
