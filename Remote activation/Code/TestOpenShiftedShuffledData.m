function TestOpenShiftedShuffledData(obj)
rand('twister',sum(100*clock));
randn('seed',sum(100*clock));
%%
r = randperm(8);
r = r(1:4);
s = randi(4, [1 4]);
data = [];
data.obj = [];
data.zones = [];
data.id = r;
data.subjs = s;
for i=1:4
    obj = TrackLoad({10 + r(i) 2});
    %%
    z = obj.zones(s(i), :);
    if i == 1
        %%
        data.obj = obj;
        data.zones = z;
    else
        %%
        if obj.nFrames > size(data.zones, 2)
            data.zones(:, size(data.zones, 2)+1:obj.nFrames) = nan;
        else
            z(:, obj.nFrames + 1:size(data.zones, 2)) = nan;
        end
        data.zones = [data.zones; z];
    end    
end
obj = [];
%%
data.obj.valid = all(~isnan(data.zones));
data.obj.zones = data.zones;
data.obj.nFrames = length(data.obj.valid);
data.obj.OutputToFile = false;
data.obj.OutputInOldFormat = false;
data.obj = SocialSetTimeScale(data.obj, 6);

data.obj = SocialPotts(data.obj);

%data.obj = SocialPotts(data.obj);
%%
shift = data.obj;
shift.zones = data.obj.zones(:, data.obj.valid);
shift.nFrames = size(shift.zones, 2);
shift.valid = data.obj.valid(data.obj.valid);
shift.OutputToFile = false;
shift.OutputInOldFormat = false;
for s=1:data.obj.nSubjects
    r = randi(shift.nFrames);
    shift.zones(s, :) = [shift.zones(s, r:end) shift.zones(s, 1:r-1)];
end
shift = SocialPotts(shift);

%%
res = [];
stat = [];
stat.src= {};
stat.map= {};
idx = 1;
zones = ZoneOpenMarkovShuffle(data.obj);
%
curr = data.obj;
curr.regions = [];
curr.zones = zones(:, 1:shift.nFrames);
curr.nFrames = size(curr.zones, 2);
curr.valid = true(1, size(curr.zones, 2));
curr.OutputToFile = false;
curr.OutputInOldFormat = false;
%curr.Analysis = [];
%%
%stat.bkgobj(idx) = RunInBackground(curr, @SocialPotts);
curr = SocialPotts(curr);
%res(iter).Ik = curr.Analysis.Potts.Ik';
fprintf('@Ik=');
fprintf(' %.2f', curr.Analysis.Potts.Ik);
fprintf('\n');

fprintf('@Ikshuff=');
fprintf(' %.2f', shift.Analysis.Potts.Ik);
fprintf('\n');

fprintf('@IkSrc=');
fprintf(' %.2f', data.obj.Analysis.Potts.Ik);
fprintf('\n');

fprintf('@IDS=');
fprintf(' %d', data.id);
fprintf('\n');
fprintf('@subjs=');
fprintf(' %d', data.subjs);
fprintf('\n');

% %%
% stat.obj = {};
% %
% for idx=1:length(stat.bkgobj)
%     %%
%     stat.obj{idx} = TrackLoad(stat.bkgobj(idx));
% end
