function me = MEAnalyze(me, data)
if iscell(me)
    for i=1:length(me)
%        subplot(floor(sqrt(length(me))), ceil(sqrt(length(me))), i);
        MEAnalyze(me{i}, data)
        saveFigure(['Graphs/MeanField.' num2str(i)]);
    end
    return;
end

%%
vec = me.nVals.^(me.Dim-1:-1:0);
h = histc((data - 1) * vec', 0:(me.nVals^me.Dim-1));
me.EmpiricalProbs = h / sum(h);
%%
MEProbProb(me.EmpiricalProbs, me.Probs, me.nData)