function rank = SortHierarchy(vals)
[pv, pn] = min(vals);
vals(pn) = inf;
rank(pn) = 1;
for i=2:length(vals)
    [v, n] = min(vals);
    if binotest(v, v + pv)
        rank(n) = rank(pn);
    else
        rank(n) = rank(pn)+1;
    end
    vals(n) = inf;
    pn = n;
    pv = v;
end