function [obj, indepProbs, jointProbs, pci, patterns] = SocialComputePatternProbsFast(obj, ignore);

base = obj.ROI.nZones;
baseVector = base.^(obj.nSubjects-1:-1:0);
jointProbs = histc(baseVector * (obj.zones(:, obj.valid) - 1) + 1, 1:base ^ obj.nSubjects);
jointProbs = jointProbs / sum(jointProbs);
%%
p = myPerms(obj.nSubjects, obj.ROI.nZones);
indepProbs = ones(1, base ^ obj.nSubjects);
for i=1:obj.nSubjects
    pInd = histc(obj.zones(i, obj.valid), 1:obj.ROI.nZones);
    pInd = pInd / sum(pInd);
    indepProbs = indepProbs .* pInd(p(:, i));
end
%%
pci = [];
patterns = p;