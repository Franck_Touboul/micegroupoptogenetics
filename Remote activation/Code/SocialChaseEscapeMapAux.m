function SocialChaseEscapeMapAux(vfunc, tfunc, data, pos)
SocialExperimentData
for g=1:GroupsData.nGroups
    f = find(GroupsData.index(GroupsData.group == g));
    for i=1:length(f)
        id = f(i);
        for d=1:GroupsData.nDays
            Stat(g).val(i, d) = eval(vfunc);
        end
        Stat(g).total(i) = eval(tfunc);
    end
    subplot(4,6,13 + (pos - 1) * 3:14 + (pos - 1) * 3)
    errorbar(1:GroupsData.nDays, mean(Stat(g).val), stderr(Stat(g).val), 'LineWidth', 2, 'Color', GroupsData.Colors(g, :));
    hon;
    set(gca, 'XTick', 1:GroupsData.nDays);
    subplot(4,6,15 + (pos - 1) * 3);
    OneBar(g, mean(Stat(g).total), GroupsData.Colors(g, :), stderr(Stat(g).total)); hon;
    set(gca, 'XTick', 1:GroupsData.nGroups);
end
subplot(4,6,15 + (pos - 1) * 3);
a1 = axis;
subplot(4,6,13 + (pos - 1) * 3:14 + (pos - 1) * 3);
a2 = axis;
axis([a2(1) a2(2) a1(3) a1(4)]);

