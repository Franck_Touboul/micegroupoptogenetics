function TrackMetaData(varargin)
% rand('twister',sum(100*clock));
% randn('seed',sum(100*clock));
rand('twister',2);
randn('seed',2);

TrackDefaults;
%%
fprintf('# meta data:\n');
% options.test.name = 'trial_1';
% options.output_path = 'res/';
% %%
% options.MovieFile= 'C:\Documents and Settings\USER\Desktop\Hezi\Trial     1.mpg';
% options.nSubjects = 4;          % the number of subjects in the arena
% options.output = true;
% %
% options.nBkgFrames = 20;        % no. of frames to use when computing background image
% %
% options.colorFrames = 20;      % no. of frames to use to determine the subject colors
% options.colorBins = 20;         % no. of bins in color histogram
% options.kmeansRepeats = 20;     % no. of time to run k-means algorithm for optimal clustring of colors
% %
% options.noiseThresh = 3;        %
% options.noiseSize = 40;         %
%%
fprintf('# computing bkg\n');
xyloObj = myMMReader(options.MovieFile);
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;
nFrames = xyloObj.NumberOfFrames;
dt = 1/xyloObj.FrameRate;
cdata = uint8(zeros(vidHeight, vidWidth, 3, options.nBkgFrames));

fprintf('# - loading movie (%d frames)... ', options.nBkgFrames);
i = 1;
while i<options.nBkgFrames
    m1 = 1; %nFrames * 1/3;
    m2 = nFrames; % * 2/3;
    r = floor(rand(1) * (m2 - m1) + m1);
    if isfield(options, 'movieStartTime') && r * dt < options.movieStartTime; continue; end
    if isfield(options, 'movieEndTime'  ) && options.movieEndTime > 0 && r * dt > options.movieEndTime; continue; end
    frame = myMMReader(options.MovieFile, r);
    if ~isempty(frame)
        cdata(:,:,:,i) = frame;
        i = i +1;
    end
end
fprintf('[done]\n');

fprintf('# - finding median... ');
bkgFrame = median(cdata, 4);
fprintf('[done]\n');

if options.output
    subplot(2,3,1);
    imagesc(bkgFrame);
    title('background');
    axis off
end
%% saving
fprintf('# - saving bkg\n');
bkg_filename = [options.output_path options.test.name '.bkg.png'];
imwrite(bkgFrame, bkg_filename);
%%
fprintf('# computing colors of subjects\n');
ROIloaded = false;
roi = struct();
if isfield(options, 'loadROI') && options.loadROI
    filename = [options.output_path options.test.name '.roi.mat'];
    load(filename);
    ROIloaded = true;
elseif isfield(options, 'ROIFile') && ~isempty(options.ROIFile)
    options.loadROI = true;
    load(options.ROIFile);
    ROIloaded = true;
end
ignoreRegion = [];
if ROIloaded &&  isfield(roi, 'Ignore')
    ignoreRegion = roi.Ignore;
    fprintf('# - ignoring region (%d pixels)\n', sum(ignoreRegion(:)));
end
%%
if isfield(options, 'ColorsFile') && ~strcmp(options.ColorsFile, '')
    fprintf(['# - from file: ''' options.ColorsFile '''\n']);
    load(options.ColorsFile, 'subject');
else
    centerpixel = [];
    qh = [];
    qs = [];
    qv = [];
    qr = [];
    qg = [];
    qb = [];
    count_ = [];
    nframes = 0;
    bins = sequence(0, 1, options.colorBins);
    fprintf('# - trying to match %d valid frames\n', options.colorFrames);
    fprintf('# - processed (%3d%%)', 0);
    doubleBkgFrame = im2double(bkgFrame);
    bkgNoise = std(doubleBkgFrame(:));
    %se = strel('disk',floor(sqrt(options.minNumPixels)/2));
    while nframes < options.colorFrames
        
        m1 = 1; %nFrames * 1/3;
        m2 = nFrames; %nFrames * 2/3;
        r = floor(rand(1) * (m2 - m1) + m1);
        if isfield(options, 'movieStartTime') && r * dt < options.movieStartTime; continue; end
        if isfield(options, 'movieEndTime'  ) && options.movieEndTime > 0 && r * dt > options.movieEndTime; continue; end
        
        orig = myMMReader(options.MovieFile, r, bkgFrame);
        
        m = imsubtract(orig, bkgFrame);
        m = imresize(m, options.scale);
        %%
        hsv_m = rgb2hsv(m);
        vm = hsv_m(:,:,3);
        %%
        meanBKG = mean(vm(:));
        stdBKG = std(vm(:));
        if ~isfield(options, 'useAdaptiveThresh')
            options.useAdaptiveThresh = true;
        end
        if options.useAdaptiveThresh
            upper = options.noiseThresh;
            lower = 1;
            prev_thresh = round((upper + lower)/2);
            while true
                thresh = round((upper + lower)/2);
                bw = vm > meanBKG + thresh * stdBKG;
                bw(ignoreRegion) = false;
                cc = bwconncomp(bw);
                if cc.NumObjects < options.maxNumObjects
                    upper = thresh - 1;
                    prev_thresh = thresh;
                else
                    lower = thresh + 1;
                end
                if lower > upper
                    break
                end
            end
            if thresh ~= prev_thresh
                thresh = prev_thresh;
                bw = vm > meanBKG + thresh * stdBKG;
            end
        else
            thresh = options.noiseThresh;
            bw = vm > meanBKG + thresh * stdBKG;
            cc = bwconncomp(bw);
            if cc.NumObjects > options.maxNumObjects
                continue;
            end
        end
        bw = bwareaopen(bw, options.minNumPixels);
        
        labels = bwlabel(bw);
        count = max(labels(:));
        if options.output
            subplot(2,3,2);
            imagesc(m);
            title('colors (failed)');
            subplot(2,3,5);
            imagesc(label2rgb(labels));
            subplot(2,3,4);
            imagesc(orig);
            drawnow;
        end
        %if count == options.nSubjects
        %%
        if count < max(options.minObjects, 1)
            continue;
        end
        %%
        props = regionprops(labels, 'Centroid', 'PixelList', 'Solidity');
        for i=1:length(props)
            if props(i).Solidity < options.solidity || sum(labels(:) == i) < options.minNumPixels
                labels(labels == i) = 0;
                count = count - 1;
            end
        end
        if count < max(options.minObjects, 1)
            continue;
        end
        fprintf('\b\b\b\b\b\b(%3d%%)', round((nframes+1)/options.colorFrames * 100));
        %
        % if rand(1) > .8 || nframes == 1
        if options.output
            subplot(2,3,3);
            imagesc(m);
            title('colors');
            drawnow;
        end
        % end
        %
        mhsv = rgb2hsv(m);
        h = mhsv(:, :, 1);
        s = mhsv(:, :, 2);
        v = mhsv(:, :, 3);
        r = m(:, :, 1);
        g = m(:, :, 2);
        b = m(:, :, 3);
        
        for l=unique(labels(labels > 0))'
            %             qh = [qh; histc(h(labels == l), bins)'];
            %             qs = [qs; histc(s(labels == l), bins)'];
            %             qv = [qv; histc(v(labels == l), bins)'];
            qh_ = histc(h(labels == l), bins)' + 1; qh_=qh_/sum(qh_); qh = [qh; qh_];
            qs_ = histc(s(labels == l), bins)' + 1; qs_=qs_/sum(qs_); qs = [qs; qs_];
            qv_ = histc(v(labels == l), bins)' + 1; qv_=qv_/sum(qv_); qv = [qv; qv_];
            
            qr = [qr; histc(r(labels == l), bins)'];
            qg = [qg; histc(g(labels == l), bins)'];
            qb = [qb; histc(b(labels == l), bins)'];
            count_ = [count_, count];
        end
        %%
        for r=unique(labels(labels > 0))'
            coord = round(props(r).Centroid);
            if any(coord(1) == props(r).PixelList(:, 1) & coord(2) == props(r).PixelList(:, 2))
                centerpixel = [centerpixel; h(coord(2), coord(1)), s(coord(2), coord(1)), v(coord(2), coord(1))];
            else
                centerpixel = [centerpixel; -1, -1, -1];
            end
        end
        %%
        nframes = nframes + 1/count;
    end
    fprintf('\n');
    %%
    fprintf('# - clustering colors... ');
    minD = inf;
    minDRGB = inf;
    minIdx = [];
    for i=1:options.kmeansRepeats
        %    [idx, currColors, subjectsD] = kwmeans([qh, qs, qv], options.nSubjects, count_ / options.nSubjects);
        [idx, currColors, subjectsD, D] = kmeans([qh, qs, qv], options.nSubjects, 'emptyaction', 'drop');
        if any(isnan(currColors))
            continue;
        end
        weightedD = sum(min(D, [], 2));
        if weightedD < minD
            minD = weightedD;
            subjectColors = currColors;
            minIdx = idx;
        end
        %     [idx, currColorsRGB, subjectsD] = kmeans([qr, qg, qb], options.nSubjects, 'emptyaction', 'singleton');
        %     if sum(subjectsD) < minDRGB
        %         minDRGB = sum(subjectsD);
        %         subjectColorsRGB = currColorsRGB;
        %     end
    end
    if ~isfinite(minD)
        error 'clustering failed';
    end
    fprintf('[done]\n');
    %%
    subjectColors = subjectColors * 0;
    mcolors = [];
    subject = struct;
    for i=1:options.nSubjects
        currIdxs = find(minIdx == i);
        %
        subject.h(i, :) = mean(qh(currIdxs, :)); subject.h(i, :) = subject.h(i, :) / sum(subject.h(i, :));
        subject.s(i, :) = mean(qs(currIdxs, :)); subject.s(i, :) = subject.s(i, :) / sum(subject.s(i, :));
        subject.v(i, :) = mean(qv(currIdxs, :)); subject.v(i, :) = subject.v(i, :) / sum(subject.v(i, :));
        %
        l = 1; subjectColors(i, l:l + size(subject.h, 2) - 1) = subject.h(i, :);
        subject.meanColor(i, 1, 1) = subject.h(i, :) * bins';
        l = l + size(qh, 2); subjectColors(i, l:l + size(qs, 2) - 1) = subject.s(i, :);
        subject.meanColor(i, 1, 2) = subject.s(i, :) * bins';
        l = l + size(qs, 2); subjectColors(i, l:l + size(qv, 2) - 1) = subject.v(i, :);
        subject.meanColor(i, 1, 3) = subject.v(i, :) * bins';
    end
    mcolorsImg = hsv2rgb(subject.meanColor);
    subject.meanColor = reshape(mcolorsImg, options.nSubjects, 3);
    if options.output
        clf;
        subplot(2,3,2);
        imshow(mcolorsImg);
        title('Mean Colors');
        axis on
        set(gca, 'XTick', []);
    end
    %
    subjectCenterColors = [];
    for j=1:3
        for i=1:options.nSubjects
            currIdxs = find(minIdx == i);
            c = centerpixel(currIdxs,j); % .* count_(currIdxs)' / options.nSubjects;
            c = c(c>=0);
            subjectCenterColors(i, 1, j) = median(c);
        end
    end
    subjectCenterColorsImg = hsv2rgb(subjectCenterColors);
    subject.centerColors = reshape(subjectCenterColorsImg, options.nSubjects, 3);
    if options.output
        subplot(2,3,3);
        imshow(subjectCenterColorsImg);
        title('Center Colors');
        axis on
        set(gca, 'XTick', []);
    end
    
    % [idx, subjectColors_h, subjectsD] = kmeans(qh, options.nSubjects);
    % [idx, subjectColors_s, subjectsD] = kmeans(qs, options.nSubjects);
    % [idx, subjectColors_v, subjectsD] = kmeans(qv, options.nSubjects);
    % subjectColors_h = subjectColors_h ./ repmat(sum(subjectColors_h, 2), 1, options.colorBins);
    % subjectColors_s = subjectColors_s ./ repmat(sum(subjectColors_s, 2), 1, options.colorBins);
    % subjectColors_v = subjectColors_v ./ repmat(sum(subjectColors_v, 2), 1, options.colorBins);
    %
    % subjectColors_h = subjectColors(:, 1:options.colorBins);
    % subjectColors_s = subjectColors(:, 1*options.colorBins+1:2*options.colorBins);
    % subjectColors_v = subjectColors(:, 2*options.colorBins+1:3*options.colorBins);
    %
    % subjectColors_h = subjectColors_h ./ repmat(sum(subjectColors_h, 2), 1, options.colorBins);
    % subjectColors_s = subjectColors_s ./ repmat(sum(subjectColors_s, 2), 1, options.colorBins);
    % subjectColors_v = subjectColors_v ./ repmat(sum(subjectColors_v, 2), 1, options.colorBins);
    %
    % subjectColors_r = subjectColorsRGB(:, 1:options.colorBins);
    % subjectColors_g = subjectColorsRGB(:, 1*options.colorBins+1:2*options.colorBins);
    % subjectColors_b = subjectColorsRGB(:, 2*options.colorBins+1:3*options.colorBins);
    %
    % subjectColors_r = subjectColors_r ./ repmat(sum(subjectColors_r, 2), 1, options.colorBins);
    % subjectColors_g = subjectColors_g ./ repmat(sum(subjectColors_g, 2), 1, options.colorBins);
    % subjectColors_b = subjectColors_b ./ repmat(sum(subjectColors_b, 2), 1, options.colorBins);
    cmap = subject.meanColor;
    colormap(cmap);
    if options.output
        for i=1:options.nSubjects
            subplot(2,3,4);
            plot(bins, subject.h(i, :)', 'Color', cmap(i, :)); hold on;
            title('Hue');
            subplot(2,3,5);
            plot(bins, subject.s(i, :)', 'Color', cmap(i, :)); hold on;
            title('Saturation');
            subplot(2,3,6);
            plot(bins, subject.v(i, :)', 'Color', cmap(i, :)); hold on;
            title('Value');
        end
    end
    subject.colorBins = bins;
    subject.color = subject.centerColors;
end
%% saving
fprintf('# - saving colors\n');
colors_filename = [options.output_path options.test.name '.colors.mat'];
save(colors_filename, 'subject');

colors_img_filename = [options.output_path options.test.name '.colors.png'];
imwrite(reshape(subject.color, 1, options.nSubjects, 3), colors_img_filename);


%% saving all data
meta.subject = subject;
meta.options = options;
meta.bkgFrame = bkgFrame;
fprintf('# saving meta-data\n');
filename = [options.output_path options.test.name '.meta.mat'];
save(filename, 'meta');

%%
fprintf('# number of video frames: %d\n', nFrames);

