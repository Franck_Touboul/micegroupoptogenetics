function objs = TrackLoadAll(varargin)
if length(varargin) == 1
    days = varargin{1};
    if ~iscell(days)
        days = {days};
    end
else
    days = varargin;
end
SocialExperimentData;
for id=1:GroupsData.nExperiments
    objs{id} = TrackLoad({id days{:}});
end
