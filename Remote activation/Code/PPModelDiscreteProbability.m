function p = PPModelDiscreteProbability(x, model, state)

p = zeros(1, size(x, 2));

if nargin > 2
    valid = x(2, :) == model.states(state).contact;
    histogram = model.states(state).histogram;
else
    valid = true(1, size(x, 2));
    histogram = model.all.histogram;
end
input = x(1, :);

p(valid) = DiscreteProbability(...
    input(valid), ...
    histogram, ...
    model.Histogram.minval, ...
    model.Histogram.maxval);