function isRemoved = SocialShowTotalHierarchy(obj, offset)
%%
if ~exist('offset', 'var')
    offset = 0;
end
factor = 2.5;
options.NodeSize = 25/factor;
options.EdgeMaxWidth = 10/factor;
options.EdgeMinWidth =  .25/factor;
options.EdgeMaxValue = 300;
options.EdgeShift = .3/factor;
options.FontSize = 12/factor;
cmap = MyMouseColormap;
dx = [0 1 -1 0];
titles = {'\alpha', '\beta', '\gamma', '\delta'};
H = obj.Hierarchy.Group.AggressiveChase;
isRemoved = false;
%%
ce = H.ChaseEscape;
mat = ce - ce';
mat(binotest(ce, ce + ce')) = 0;
mat = mat .* (mat > 0);
%mat(mat <= 2) = 0;
[rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
level = max(rank) - rank + 1;
%%

X = [];
Y = [];
for l=unique(level)
    map = level==l;
    idx = 0;
    for i=find(map)
        a = 2 * mod(l, 2) - 1;
        X(i) = offset + dx(l) + a * idx;
        Y(i) = -l;
        idx = idx + 1;
    end
end
%%
for i=1:obj.nSubjects
    for j=1:obj.nSubjects
        if H.ChaseEscape(i, j) > 0
            %plot([X(i) X(j)], [Y(i) Y(j)], '-', 'Color', [.2 .2 .2], 'LineWidth', h.Contacts(i, j) / options.EdgeMaxValue * options.EdgeMaxWidth); hon
            vec = [X(i)-X(j) Y(i)-Y(j)];
            vec = vec / sqrt(vec*vec') * options.EdgeShift;
            plot([X(i) X(j)] + vec(2), [Y(i) Y(j)] - vec(1), '-', 'Color', cmap(i, :), 'LineWidth', H.ChaseEscape(i, j) / options.EdgeMaxValue * (options.EdgeMaxWidth - options.EdgeMinWidth) + options.EdgeMinWidth); hon
        end
    end
end
%%
for i=1:obj.nSubjects
    plot(X(i), Y(i), 'o', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', cmap(i, :), 'MarkerSize', options.NodeSize); hon;
    text(X(i), Y(i), titles{level(i)}, 'verticalAlignment', 'middle', 'horizontalalignment', 'center', 'FontSize', options.FontSize);
end
%axis([-1 1 -1 1]);
set(gcf, 'Color', 'w', 'Units', 'normalized');
axis off;
axis equal
hoff;


