function MEPrint(varargin)
st = dbstack;
count = length(st)-1;
switch count
    case {0,1}
        fprintf('#(ME) ');
    case 2
        fprintf('#(ME) - ');
    case 3
        fprintf('#(ME)   - ');
    otherwise
        fprintf('#(ME)     - ');
end
fprintf(varargin{:});
    