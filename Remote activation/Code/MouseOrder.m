function MouseOrder(rank)
for r = sort(unique(rank), 2, 'descend')
    ids = find(rank == r);
    if length(ids) > 1
        fprintf(' (');
    end
    for i=1:length(ids)
        fprintf(' %s', MouseName(ids(i)));
    end
    if length(ids) > 1
        fprintf(' )');
    end
end