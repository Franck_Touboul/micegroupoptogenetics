function m = validmean(x, dim)

X = x;
X(isnan(x)) = 0;
valid = ~isnan(x);
if nargin == 2
    m = sum(X, dim) ./ sum(valid, dim);
else
    m = sum(X) ./ sum(valid);
end


% if nargin == 1
%     dim = 1;
% end
% if dim == 2
%     x = x';
% end
%     m = zeros(1, size(x,2));
%     for i=1:size(x,2)
%         c = x(:, i);
%         m(i) = mean(c(~isnan(c)));
%     end
% if dim == 2
%     m = m';
% end
