% modelobj = TrackLoad('Res/SC.exp0001.day02.cam01.obj.mat');
% modelobj = SocialPredPreyModel(modelobj);
% model = SocialAggrClassifierTrain(modelobj);
%model{1} = SocialAggrClassifierTrain({'Res/SC.exp0008.day02.cam01.obj.mat'});
model = SocialAggrClassifierTrain({'Res/SC.exp0001.day02.cam01.obj.mat'});

%%
%obj = TrackLoad('Res/SC.exp0008.day02.cam01.obj.mat');
obj = TrackLoad('Res/SC.exp0004.day02.cam04.obj.mat');
%obj = TrackLoad('Res/SC.exp0001.day02.cam01.obj.mat');
obj = SocialPredPreyModel(obj);
%%
%[obj, Score] = SocialAggrClassifier(obj, model);
[obj, Score] = SocialAggrClassifier('Res/SC.exp0004.day02.cam04.obj.mat', 'Res/SC.exp0001.day02.cam01.obj.mat');
return
%%
fa = Score.Results([Score.Results.output] & ~[Score.Results.match] & ~[Score.Results.ignore]);
md = Score.Marks([Score.Marks.output] & ~[Score.Marks.match] & ~[Score.Marks.ignore]);
%%
Score = SocialBehaviorComputeScore(obj, 'chase', marks, ...
    obj.Contacts.Behaviors.ChaseEscape.Map)
fprintf('#----------------\n');
fprintf('# detection=%.1f%% (%d/%d), false-alarm=%.1f%% (%d)\n', ...
    Score.nDetection/Score.nTrue*100, Score.nDetection, Score.nTrue, ...
    Score.nFalseAlarm / Score.nEvents * 100, Score.nFalseAlarm);
md = Score.Marks([Score.Marks.output] & ~[Score.Marks.match] & ~[Score.Marks.ignore]);