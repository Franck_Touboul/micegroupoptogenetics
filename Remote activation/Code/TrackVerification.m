nMovies = 10;
nSamples = 100;
MoviesPath = '../Movies/';

%% find latest experiments
% SocialExperimentData;
% files = {};
% dates = [];
% 
% for id=1:length(experiments)
%     for day = 1
%         prefix = sprintf(experiments{id}, day);
%         %fprintf('# -> %s\n', prefix);
%         %obj = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid', 'Hierarchy', 'Analysis'});
%         file = dir([MoviesPath prefix '.avi']);
%         files{id} = prefix;
%         dates(id) = file.datenum;
%     end
% end
% [~, o] = sort(dates);
% files = { files{o(end-nMovies+1:end)} };
%%
%files = {'SC.exp0001.day01.cam01'};
files = {...
%     'SC.exp0001.day%02d.cam01', ...
%     'SC.exp0002.day%02d.cam01',...
%     'SC.exp0003.day%02d.cam01',...
%     'SC.exp0004.day%02d.cam04',...
%     'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam04',...
    'SC.exp0007.day%02d.cam04',...
    'SC.exp0008.day%02d.cam01'};

day = 1;
%
for id=1:length(files)
    %%
    fprintf('# -> %s\n', files{id});
        prefix = sprintf(files{id}, day);
    obj = TrackLoad(['Res/' prefix '.obj.mat'], {'x', 'y', 'zones', 'ROI', 'sheltered'});
    prev = 0;
    %%
    [begF, endF, len] = FindEvents(sum(obj.sheltered) == 0);
    v = len > obj.FrameRate;
    begF = begF(v);
    endF = endF(v);
    len = len(v);
    count = 1;
    i = 1;
    while count < min(length(begF), nSamples) && i <= length(begF)
        frame = begF(i) + randi(len(i)) - 1;
        i = i + 1;
        if frame < prev + obj.FrameRate * 60 * 2.5;
            continue;
        end
        prev = frame;
        try
            %%
            clf;
            %frame = 1000;
            SocialShowFrame(obj, frame, true);
            title([obj.FilePrefix '    ' num2str(frame)]);
            name = ['Verification/TrackVerification.' prefix '.' num2str(frame)];
            print(gcf, [name '.eps'], '-dpsc2');
            system(['pstopnm -stdout -dpi=120 -portrait ' name '.eps | pnmtopng > ' name '.png']);
            system(['rm ' name '.eps']);
            count = count + 1;
        catch me
            disp me.message
        end
    end
end