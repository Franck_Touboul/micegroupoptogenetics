opt = struct();
opt.Resolution = 20;
opt.NumVerteb = 4 + 7 + 13 + 6 + 4 + 30; % skull + 7 cervical + 12-14 thoracic + 5-6 lumbar + 4 sacral + 27-30 caudal
opt.Scale = 0.1;
opt.Draw3D = false;
opt.HiddenAxis = 3;

%%
Model.Bones = struct();
Model.Types = [0 * ones(1,4), 1 * ones(1,7), 2 * ones(1,13), 3 * ones(1,6), 4 * ones(1,4), 5 * ones(1,7), 6 * ones(1,13), 7 * ones(1,10)];

for i=1:opt.NumVerteb
    Bone = struct();
    Skin = struct();
    f2b = i - find(Model.Types == Model.Types(i), 1);
    b2f = find(Model.Types == Model.Types(i), 1, 'last') - i;
    
    for j=1:opt.Resolution
        switch Model.Types(i)
            case 0
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
                Skin(j).Distance = .04 + f2b * 0.1;
                if f2b == 3
                    Skin(j).Distance = .04 + (f2b - 1) * 0.1;
                end
            case 1
                a = 0;
                b = .25 + 0.015 * f2b;
                c = b;
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
                angle = Skin(j).Angle; if angle > pi; angle = 2 * pi - pi; end
                Skin(j).Distance = spline([0 pi/2 pi], [a b c], angle);
            case 2
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
                Skin(j).Distance = 0.42 - 0.005 * b2f;
                Skin(j).Distance = Skin(j).Distance * sin(2*pi-Skin(j).Angle/2)*2/1.5;
            case 3
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
                Skin(j).Distance = 0.42 + 0.015 * (f2b);
                Skin(j).Distance = Skin(j).Distance * sin(2*pi-Skin(j).Angle/2)*2/1.5;
            case 4
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
                Skin(j).Distance = .5 - 0.14 * f2b;
                Skin(j).Distance = Skin(j).Distance * sin(2*pi-Skin(j).Angle/2)*2/1.5;
            case 5
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
                Skin(j).Distance = .04-f2b*(.04 / 30);
            case 6
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
                Skin(j).Distance = .04-(f2b+7)*(.04 / 30);
            case 7
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
                Skin(j).Distance = .04-(f2b+20)*(.04 / 30);
        end
    end
    Bone.Skin = Skin;
    Bone.Roll = 0;
    Bone.Backset = 0;
    Bone.Dependence = i - 1;
    switch Model.Types(i)
        case 0 % skull
            Bone.Length = .24;
            Bone.Yaw = 0;
            if f2b == 0
                Bone.Pitch = 1.2;
 %               Bone.Pitch = .2;
            else
                Bone.Pitch = 0;
            end
        case 1
            Bone.Length = .07;
            Bone.Yaw = 0;
            if f2b == 0
                Bone.Pitch = -1.8;
%                Bone.Pitch = -.8;
                Bone.Backset = 0.15;
            else
                Bone.Pitch = 0.18;
            end
        case 2
            Bone.Length = .07;
            Bone.Yaw = 0;
            if f2b == 0
                Bone.Pitch =  .8;
            else
                Bone.Pitch = -.12;
            end
        case 3
            Bone.Length = .15;
            Bone.Yaw = 0;
            Bone.Pitch = -.04;
        case 4
            Bone.Length = .15;
            Bone.Yaw = 0;
            Bone.Pitch = .09;
        case 5
            Bone.Length = (.15-f2b*0.005) * 1.3;
            if f2b == 0
                Bone.Yaw = -1.1;
%                Bone.Pitch = 0.35;
                Bone.Pitch = 0;
            else
                Bone.Yaw = -0.3;
%                Bone.Pitch = -0.36;
                Bone.Pitch = 0;
            end
        case 6
            Bone.Length = (.15-(f2b + 7)*0.005) * 1.3;
            Bone.Yaw = 0;
            Bone.Pitch = -0.04;
        case 7
            Bone.Length = (.15-(f2b + 20)*0.005) * 1.3;
            Bone.Yaw = 0;
            Bone.Pitch = 0.1;
    end
    if i==1
        Model.Bones = Bone;
    else
        Model.Bones(i) = Bone;
    end
end

%%
Model.Points = struct();
P0 = [0; 0; 0];
Model.Points.Skeleton.Fr = zeros(opt.NumVerteb, 3);
Model.Points.Skeleton.To = zeros(opt.NumVerteb, 3);
Model.Points.Skin.Points = zeros(opt.NumVerteb * opt.Resolution, 3);
Model.Points.Skin.ID = zeros(opt.NumVerteb * opt.Resolution, 3);
Model.Points.Skin.Bone = zeros(opt.NumVerteb * opt.Resolution, 1);
rot = eye(3);
sidx = 1;
x = [];
y = [];
z = [];
for i=1:opt.NumVerteb
    Bone = Model.Bones(i);
    rot = rot * RotMat(Bone.Yaw, Bone.Pitch, Bone.Roll);
    if Bone.Backset > 0
        P0 = P0 + rot * [Bone.Backset; 0; 0];
    end
    if i < opt.NumVerteb && Model.Bones(i+1).Backset > 0
    else
        P1 = P0 + rot * [Bone.Length; 0; 0];
    end
    Model.Points.Skeleton.Fr(i, :) = P0;
    Model.Points.Skeleton.To(i, :) = P1;
    %%
    for j=1:length(Bone.Skin)
        Skin = Bone.Skin(j);
        S = P0 + rot * ([0; sin(Skin.Angle); cos(Skin.Angle) ] * Skin.Distance);
        Model.Points.Skin.Points(sidx, :) = S;
        Model.Points.Skin.Bone(sidx, :) = i;
        Model.Points.Skin.ID(sidx, :) = j;
        
        x(i, j) = S(1);
        y(i, j) = S(2);
        z(i, j) = S(3);
        sidx = sidx + 1;
    end
    %%    
    P0 = P1;
end
plot3(Model.Points.Skeleton.Fr(:, 1), Model.Points.Skeleton.Fr(:, 2), 1 - Model.Points.Skeleton.Fr(:, 3));
hon
plot3(Model.Points.Skin.Points(:, 1), Model.Points.Skin.Points(:, 2), 1 - Model.Points.Skin.Points(:, 3));
hoff
surfl(x,y,1-z); 
return
%shading interp; colormap(gray)
%% Draw 3D
dmap = MySubCategoricalColormap;
if opt.Draw3D
    P0 = [0; 0; 0];
    r = .1;
    cmap = lines(length(Model.Bones));
    rot = eye(3);
    for i=1:opt.NumVerteb
        Bone = Model.Bones(i);
        rot = rot * RotMat(Bone.Yaw, Bone.Pitch, Bone.Roll);
        P1 = P0 + rot * [Bone.Length; 0; 0];
        plot3([P0(1) P1(1)], [P0(2) P1(2)], [P0(3) P1(3)], 'color', cmap(i, :), 'LineWidth', 2);
        hon;
        plot3(P0(1), P0(2), P0(3), 'o', 'MarkerFaceColor', ones(1,3)*.5, 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
        %DepthAddTriangle([x0+r, x0-r, x1] * opt.Scale, [y0+r, y0-r, y1] * opt.Scale, [z0 z0 z1] * opt.Scale);
        %% skin
        %if 1==2
            for j=1:length(Bone.Skin)
                Skin = Bone.Skin(j);
                S = P0 + rot * ([0; sin(Skin.Angle); cos(Skin.Angle) ] * Skin.Distance);
                KLine(P0, S, ':', 'Color', ones(1,3)*.1);
                plot3(S(1), S(2), S(3), 'o', 'MarkerFaceColor', ones(1,3)*.5, 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
            end
        %end
        %%
        P0 = P1;
    end
    plot3(P1(1), P1(2), P1(3), 'o', 'MarkerFaceColor', ones(1,3)*.7, 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
    grid on;
    campos([0 90 0]); camproj('orthographic')
    %axis([-.5 .5 -.5 .5 -.5 .5] )
    hoff
    xlabel('x')
    ylabel('y')
    zlabel('z')
else
   
    if opt.HiddenAxis == 2
        s1 = 110;
        o1 = 15;
        o2 = 138;
        
        img = imread('mouse-xray.jpg');
        imshow(img);
        hon;
    else
        s1 = 470;

        o1 = 110;
        o2 = 750;

        img = imread('lxl2_xray.jpg');
        imshow(img);
        hon;
    end
    A = 1:3; A = A(A ~= opt.HiddenAxis);
    
    P0 = [0; 0; 0];
    r = .1;
    cmap = lines(length(Model.Bones));
    rot = eye(3);
    for i=1:opt.NumVerteb
        Bone = Model.Bones(i);
        rot = rot * RotMat(Bone.Yaw, Bone.Pitch, Bone.Roll);
        %         if Model.Bones(i+1).Offset > 0
        if Bone.Backset > 0
            P0 = P0 + rot * [Bone.Backset; 0; 0];
        end
        if i < opt.NumVerteb && Model.Bones(i+1).Backset > 0
        else
            P1 = P0 + rot * [Bone.Length; 0; 0];
        end
        
        plot([P0(A(1)) P1(A(1))] * s1 + o1, [P0(A(2)) P1(A(2))] * s1 + o2, 'color', ones(1, 3) *.5, 'LineWidth', 2);
        hon;
        plot(P0(A(1)) * s1 + o1, P0(A(2)) * s1 + o2, 'o', 'MarkerFaceColor', dmap(Model.Types(i)+1, :), 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
        %DepthAddTriangle([x0+r, x0-r, x1] * opt.Scale, [y0+r, y0-r, y1] * opt.Scale, [z0 z0 z1] * opt.Scale);
        %% skin
        %if i>opt.NumVerteb - 30
            for j=1:length(Bone.Skin)
                Skin = Bone.Skin(j);
                S = P0 + rot * ([0; sin(Skin.Angle); cos(Skin.Angle) ] * Skin.Distance);
%               plot(S(A(1)) * s1 + o1, S(A(2)) * s2 + o2, 'o', 'MarkerFaceColor', dmap(Model.Types(i)+1, :), 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
               plot([P0(A(1)) S(A(1))] * s1 + o1, [P0(A(2)) S(A(2))] * s1 + o2, '-o', 'MarkerFaceColor', dmap(Model.Types(i)+1, :), 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8, 'color', ones(1,3)*.7);
            end
        %else
             S = P0 + rot * ([0; sin(0); 0 ] * 1);
             %plot([P0(A(1)) S(A(1))] * s1 + o1, [P0(A(2)) S(A(2))] * s1 + o2, 'r-');
%             S = P0 + rot * ([0; sin(Skin.Angle); cos(Skin.Angle) ] * Skin.Distance);
        %end
        %%
        P0 = P1;
    end
    plot(P1(A(1)) * s1 + o1, P1(A(2)) * s1 + o2, 'o', 'MarkerFaceColor', ones(1,3)*.7, 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
    grid on;
    %campos([0 90 0]); camproj('orthographic')
    %axis([-.5 .5 -.5 .5 -.5 .5] )
    hoff
    xlabel('x')
    ylabel('y')
    zlabel('z')
    
end