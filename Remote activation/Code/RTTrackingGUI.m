function varargout = RTTrackingGUI(varargin)
% RTTRACKINGGUI MATLAB code for RTTrackingGUI.fig
%      RTTRACKINGGUI, by itself, creates a new RTTRACKINGGUI or raises the existing
%      singleton*.
%
%      H = RTTRACKINGGUI returns the handle to a new RTTRACKINGGUI or the handle to
%      the existing singleton*.
%
%      RTTRACKINGGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RTTRACKINGGUI.M with the given input arguments.
%
%      RTTRACKINGGUI('Property','Value',...) creates a new RTTRACKINGGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before RTTrackingGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to RTTrackingGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help RTTrackingGUI

% Last Modified by GUIDE v2.5 17-Apr-2013 09:51:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RTTrackingGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @RTTrackingGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

global RTTrackingData;

function AddMessage(handles, msg)
t = get(handles.MainBox, 'String');
t{end+1}=['# ' msg];
set(handles.MainBox, 'String', t)

% --- Executes just before RTTrackingGUI is made visible.
function RTTrackingGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RTTrackingGUI (see VARARGIN)

% Choose default command line output for RTTrackingGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes RTTrackingGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

if ~isfield(handles, 'Devices')
    handles.Devices = struct();
end
if ~isfield(handles, 'CurrDevice')
    handles.CurrDevice = [];
end
%handles.CleanUp = onCleanup(@() CloseDevice(handles));
guidata(hObject, handles);
axes(handles.PreviewAxes);
axis off;
axes(handles.ColorAxes);
axis off;
global RTTrackingData;
RTTrackingData = struct();
RTTrackingData.CurrentID = 0;
RTTrackingData.PrevID = 0;
RTTrackingData.IDDuration = 0;
RTTrackingData.StartTime = [];
RTTrackingData.Handles = handles;
RTTrackingData.MinDurationBetweenEvents = 1;
RTTrackingData.PrevTime = -ones(1, 4) ;

RTTrackingData.Params.Coherence = 0.3;
RTTrackingData.Params.MinNumOfPixels = 250;
RTTrackingData.Params.MinArea = 10;

RTTrackingData.OutputFile = [];
RTTrackingData.Output = [];

s = [];
AddMessage(handles, 'Starting ni device... ');
try
    
    if strcmp(computer, 'PCWIN')
        %%
        AddMessage(handles, '- using legacy interface (32bit)');
        %%
        out = daqhwinfo('nidaq');
        d = out.InstalledBoardIds;
        names = out.BoardNames;
        if isempty(d)
            AddMessage(handles, '- no devices found');
        else
            s = [];
            AddMessage(handles, '- devices:');
            dev = '';
            for i=1:length(d)
                AddMessage(handles, ['  . ' names{i} '(' d{i} ')']);
                if i==1
                    dev = d{i};
                end
            end
            AddMessage(handles, '- creating session')
            s.dio = digitalio('nidaq', dev);
            AddMessage(handles, '- adding digital channel');
            s.lines = addline(dio,0:3,'out');
            set(handles.MessagesBox, 'String', 'NI device OK')
            AddMessage(handles, '- [DONE]');
        end
    else
        AddMessage(handles, '- using session based interface (64bit)');
        d = daq.getDevices;
        if length(d) == 0
            AddMessage(handles, '- no devices found');
        else
            AddMessage(handles, '- devices:');
            dev = '';
            for i=1:length(d)
                AddMessage(handles, ['  . ' d(i).Description '(' d(i).ID ')']);
                if i==1
                    dev = d(i).ID;
                end
            end
            AddMessage(handles, '- creating session')
            s = daq.createSession('ni');
            AddMessage(handles, '- adding digital channel');
            s. addDigitalChannel(dev, 'port0/line0:3', 'OutputOnly');
            set(handles.MessagesBox, 'String', 'NI device OK')
            AddMessage(handles, '- [DONE]');
        end
    end
    
catch err
    AddMessage(handles, ['- [FAILED]: ' err.message]);
    set(handles.MessagesBox, 'String', 'no NI device')
end
RTTrackingData.DAQ = s;

set(handles.MinAreaSlider, 'Max', 20);
set(handles.MinAreaSlider, 'Value', RTTrackingData.Params.MinArea);
set(handles.MinAreaSlider, 'SliderStep', ones(1,2) * 1/20);

MinAreaSlider_Callback(handles.MinAreaSlider, eventdata, handles);

% --- Outputs from this function are returned to the command line.
function varargout = RTTrackingGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function RunPreview(obj)
vidRes = get(obj, 'VideoResolution'); 
nBands = get(obj, 'NumberOfBands'); 
hImage = image( zeros(vidRes(2), vidRes(1), nBands) ); 
preview(obj, hImage); 

function [img, id] = ProcessFrame(img, ROI, thresh, Colors)
x = round(ROI.BB(2)); wx = round(ROI.BB(4));
y = round(ROI.BB(1)); wy = round(ROI.BB(3));
patch = img(x:x+wx, y:y+wy, :);
hsv = rgb2hsv(patch);
h = hsv(:, :, 1);
s = hsv(:, :, 2);
v = hsv(:, :, 3);
valid = v >= thresh;
global RTTrackingData;
valid = imerode(valid, strel('disk',RTTrackingData.Params.MinArea));

if nargin > 3
    ih = round(h(valid) / (1 / (length(Colors.Bins)-1))) + 1;
    is = round(s(valid) / (1 / (length(Colors.Bins)-1))) + 1;
    iv = round(v(valid) / (1 / (length(Colors.Bins)-1))) + 1;
    if length(ih) < RTTrackingData.Params.MinNumOfPixels
        id = 0;
    else
        %     s = zeros(1, size(Colors.Histrogram.H, 1));
        %     for i=1:size(Colors.Histrogram.H, 1)
        %         s(i) = mean(Colors.Histrogram.H(i, ih));
        %     end
        
        nSubj = size(Colors.Histrogram.H, 1);
        s = zeros(size(Colors.Histrogram.H, 1), length(ih));
        for i=1:nSubj
            s(i, :) = Colors.Histrogram.H(i, ih) .* Colors.Histrogram.S(i, is);
        end
        [~, imxs] = max(s);
        h = histc(imxs, 1:nSubj);
        [mxh, imxh] = max(h);
        h(imxh) = 0;
        mxh2 = max(h);
        if mxh2 > mxh * RTTrackingData.Params.Coherence
            id = 0;
        else
            %        joint_prob = prob_h .* prob_s .* prob_v;
            id = imxh;
        end
        
    end
    
else
    id = 0;
end

g = img(:, :, 2);
g(x:x+wx, y:y+wy, :) = 255 * valid;
img(:, :, 2) = g;

img(x:x+wx, y, :) = 255;
img(x:x+wx, y+1, :) = 255;
img(x:x+wx, y+wy, :) = 255;
img(x:x+wx, y+wy-1, :) = 255;
img(x, y:y+wy, :) = 255;
img(x+1, y:y+wy, :) = 255;
img(x+wx, y:y+wy, :) = 255;
img(x+wx-1, y:y+wy, :) = 255;

global RTTrackingData;
% RTTrackingData.CurrentID = 0;
% RTTrackingData.PrevID = 0;
% RTTrackingData.IDDuration = 0;
% RTTrackingData.StartTime = [];

function AcquireData(hObject, e, himage, handles)
warning('off', 'imaq:peekdata:tooManyFramesRequested');
img = peekdata(hObject, 1);
warning('on', 'imaq:peekdata:tooManyFramesRequested');
if isempty(img)
    return
end
global RTTrackingData;
if isfield(handles, 'ROI')
    if isfield(handles, 'Colors')
        [img, id] = ProcessFrame(img, handles.ROI, get(handles.ThresholdSlider, 'Value'), handles.Colors);
    else
        [img, id] = ProcessFrame(img, handles.ROI, get(handles.ThresholdSlider, 'Value'));
    end
    if RTTrackingData.CurrentID == id
        RTTrackingData.IDDuration = RTTrackingData.IDDuration + 1;
    else
        RTTrackingData.IDDuration = 0;
        RTTrackingData.StartTime = clock;
        RTTrackingData.CurrentID = id;
    end       
end
if ~isempty(RTTrackingData.Output) && isvalid(RTTrackingData.Output)
    writeVideo(RTTrackingData.Output, img);
end

set(RTTrackingData.Handles.FoundMouseTB, 'String', RTTrackingData.CurrentID);

if RTTrackingData.CurrentID ~= 0 && RTTrackingData.CurrentID == str2double(get(MouseIDTextBox, 'string'))
    %outputSingleScan.outputSingleScan(1);
    set(handles.MouseIDTextBox, 'BackgroundColor', [1 0 0]);
else
    %outputSingleScan.outputSingleScan(0);
    set(handles.MouseIDTextBox, 'BackgroundColor', [1 1 1]);
end
    
set(himage, 'CData', img);


%disp 1

function AcquireVideoFrame(hObject, e, himage, fig, handles)
data = guidata(fig);
data.FrameCount = data.FrameCount + 1;
if data.FrameCount >= handles.CurrDevice.obj.NumberOfFrames
    data.FrameCount = 1;
end
%set(handles.VideoPosition, 'Value', data.FrameCount / handles.CurrDevice.obj.NumberOfFrames);
try
    img = read(handles.CurrDevice.obj, min(data.FrameCount, handles.CurrDevice.obj.NumberOfFrames));
catch e
    data.FrameCount = 1;
    img = read(handles.CurrDevice.obj, min(data.FrameCount, handles.CurrDevice.obj.NumberOfFrames));
end

global RTTrackingData;
if ~isempty(RTTrackingData.Output) && isvalid(RTTrackingData.Output)
    writeVideo(RTTrackingData.Output, img);
end


id = 0;
if isfield(data, 'ROI')
    if isfield(handles, 'Colors')
        [img, id] = ProcessFrame(img, data.ROI, get(handles.ThresholdSlider, 'Value'), handles.Colors);
    else
        [img, id] = ProcessFrame(img, data.ROI, get(handles.ThresholdSlider, 'Value'));
    end
    %data.FoundMouseTB.String = num2str(id);
    if RTTrackingData.CurrentID == id
        RTTrackingData.IDDuration = RTTrackingData.IDDuration + 1;
    else
        RTTrackingData.IDDuration = 0;
        RTTrackingData.CurrentID = id;
        RTTrackingData.StartTime = clock;
    end       
    if ~isempty(RTTrackingData.DAQ)        
        if RTTrackingData.IDDuration == 0
            z = zeros(1, 4);
            if RTTrackingData.CurrentID > 0
                if etime(clock, datevec(RTTrackingData.PrevTime(RTTrackingData.CurrentID))) > RTTrackingData.MinDurationBetweenEvents
                    z(RTTrackingData.CurrentID) = 1;
                    set(handles.MessagesBox, 'String', sprintf('Open Ch. #%d', RTTrackingData.CurrentID));
                    RTTrackingData.PrevTime(RTTrackingData.CurrentID) = now;
                end
            else
                set(handles.MessagesBox, 'String', 'Channel closed');
            end
            if strcmp(computer, 'PCWIN')
                putvalue(RTTrackingData.DAQ.dio, z);
            else
                RTTrackingData.DAQ.outputSingleScan(z);
            end
        elseif RTTrackingData.CurrentID > 0 && etime(clock, RTTrackingData.StartTime) > 2
            set(handles.MessagesBox, 'String', sprintf('Done Ch. #%d', RTTrackingData.CurrentID));
            %RTTrackingData.PrevTime(RTTrackingData.CurrentID) = now;
            if strcmp(computer, 'PCWIN')
                putvalue(RTTrackingData.DAQ.dio, zeros(1, 4));
            else
                RTTrackingData.DAQ.outputSingleScan(zeros(1, 4));
            end
        end
    end
else
    RTTrackingData.CurrentID = id;
    RTTrackingData.IDDuration = 0;
end
set(RTTrackingData.Handles.FoundMouseTB, 'String', RTTrackingData.CurrentID);
set(himage, 'CData', img);
guidata(fig, data);

% if RTTrackingData.CurrentID ~= 0 && RTTrackingData.CurrentID == str2double(get(MouseIDTextBox, 'string'))
%     RTTrackingData.DAQ.outputSingleScan(1);
%     set(handles.MouseIDTextBox, 'BackgroundColor', [1 0 0]);
% else
%     RTTrackingData.DAQ.outputSingleScan(0);
%     set(handles.MouseIDTextBox, 'BackgroundColor', [1 1 1]);
% end

function StartDataProcessing(obj, handles)
vidRes = get(obj, 'VideoResolution');
nBands = get(obj, 'NumberOfBands');
axes(handles.PreviewAxes);
hImage = image( zeros(vidRes(2), vidRes(1), nBands) );
axis off;

triggerconfig(obj, 'manual');
obj.TimerPeriod = 0.1;
obj.TimerFcn = {@AcquireData, hImage, handles};


function CloseDevice(handles)
if isfield(handles, 'CurrDevice') && isfield(handles.CurrDevice, 'obj') && isvalid(handles.CurrDevice.obj)
    delete(handles.CurrDevice.obj);
end

global RTTrackingData;
if ~isempty(RTTrackingData.Output) && isvalid(RTTrackingData.Output)
    delete(RTTrackingData.Output);
end
set(handles.SaveVideoFileButton, 'String', 'Save');
set(handles.SaveVideoFileButton, 'Enable', 'off')

function StartCapture(hObject, handles, filename)

set(handles.MessagesBox, 'String', 'Starting recording device...')
CloseDevice(handles);
set(handles.SaveVideoFileButton, 'Enable', 'on');
handles.CurrDevice = handles.Devices(get(hObject,'Value'));
if handles.CurrDevice.ID < 0
    if nargin > 2
        filename = regexprep(filename, '\\', '\/');
        obj = MyVideoReader(filename, 'Tag', filename);
    else
        [FileName, PathName] = uigetfile('*.avi');
        obj = MyVideoReader([PathName FileName]);
        handles.Filename = [PathName FileName];
    end
    handles.CurrDevice.obj = obj;
    handles.FrameCount = 0;
    axes(handles.PreviewAxes);
    hImage = image( zeros(obj.Height, obj.Width, 3) );
    handles.Timer = timer('TimerFcn', {@AcquireVideoFrame, hImage, hObject, handles},...
        'Period',1/obj.FrameRate);
    handles.Timer.ExecutionMode = 'fixedRate';
    guidata(hObject, handles);
    start(handles.Timer);
else
    obj = videoinput(handles.CurrDevice.Adaptor, handles.CurrDevice.ID);
    handles.CurrDevice.obj = obj;
    StartDataProcessing(obj, handles);
    start(obj);
    guidata(hObject, handles);
end
%%
%set(vid, 'FramesPerTrigger', 5);
%set(vid, 'TriggerRepeat', 9);
%%
%RunPreview(obj);
%set(handles.MessagesBox, 'String', 'Preview mode')


% --- Executes on selection change in DeviceListBox.
function DeviceListBox_Callback(hObject, eventdata, handles)
% hObject    handle to DeviceListBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
StartCapture(hObject, handles);
% Hints: contents = cellstr(get(hObject,'String')) returns DeviceListBox contents as cell array


% --- Executes during object creation, after setting all properties.
function DeviceListBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DeviceListBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'String', {});
DeviceNames = {};
idx = 1;
adaptors = imaqhwinfo;
for i=1:length(adaptors.InstalledAdaptors)
    devices = imaqhwinfo(adaptors.InstalledAdaptors{i});
    for j=1:length(devices.DeviceInfo)
        handles.Devices(idx).Name = devices.DeviceInfo(j).DeviceName;
        handles.Devices(idx).Adaptor = adaptors.InstalledAdaptors{i};
        handles.Devices(idx).ID = devices.DeviceInfo(j).DeviceID;
        DeviceNames{idx} = devices.DeviceInfo(j).DeviceName;
        idx = idx + 1;
    end
end
handles.Devices(idx).Name = 'From file...';
handles.Devices(idx).Adaptor = [];
handles.Devices(idx).ID = -1;
DeviceNames{idx} = handles.Devices(idx).Name;

set(hObject, 'String', DeviceNames);
guidata(hObject, handles);


% --- Executes on button press in ROIButton.
function ROIButton_Callback(hObject, eventdata, handles)
% hObject    handle to ROIButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%frame = getsnapshot(handles.CurrDevice.obj);
%stoppreview(handles.CurrDevice.obj);
if handles.CurrDevice.ID < 0
    stop(handles.Timer);
else
    stop(handles.CurrDevice.obj);
end
roi = imrect;
handles.ROI.Mask = roi.createMask;
p = regionprops(handles.ROI.Mask, 'BoundingBox');
handles.ROI.BB = p.BoundingBox;
guidata(hObject, handles);
delete(roi)
if handles.CurrDevice.ID < 0
    start(handles.Timer);
else
    StartDataProcessing(handles.CurrDevice.obj, handles);
    start(handles.CurrDevice.obj);
end

%RunPreview(handles.CurrDevice.obj);



function MessagesBox_Callback(hObject, eventdata, handles)
% hObject    handle to MessagesBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MessagesBox as text
%        str2double(get(hObject,'String')) returns contents of MessagesBox as a double


% --- Executes during object creation, after setting all properties.
function MessagesBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MessagesBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'CurrDevice') && isfield(handles.CurrDevice, 'obj');
    try
        delete(handles.CurrDevice.obj);
    catch
    end
end


% --- Executes on button press in SetColorsButton.
function SetColorsButton_Callback(hObject, eventdata, handles)
% hObject    handle to SetColorsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%s = daq.createSession('ni');
%s.addDigitalChannel('Dev1', 'port0/line0', 'OutputOnly');
%handles.OutputDevice = s;
%guidata(hObject, handles);

[FileName, PathName] = uigetfile('*.obj.mat');
f = regexprep([PathName, FileName], '\\', '\/');
obj = TrackLoad(f, {'Colors'});
handles.Colors = obj.Colors;
axes(handles.ColorAxes);
imagesc(reshape(obj.Colors.Centers, [1, size(obj.Colors.Centers, 1), 3]));
axis off;
guidata(handles.figure1, handles);
if isfield(handles, 'CurrDevice') && isfield(handles.CurrDevice, 'obj');
    StartCapture(handles.DeviceListBox, handles, handles.Filename);
else
    StartCapture(handles.DeviceListBox, handles);
end
% --- Executes on slider movement.
function ThresholdSlider_Callback(hObject, eventdata, handles)
% hObject    handle to ThresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function ThresholdSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ThresholdSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function VideoPosition_Callback(hObject, eventdata, handles)
% hObject    handle to VideoPosition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
stop(handles.Timer);
h = get(hObject,'Value');
handles.FrameCount = floor(handles.CurrDevice.obj.NumberOfFrames * h) + 1;
guidata(handles.figure1, handles);
start(handles.Timer);

% --- Executes during object creation, after setting all properties.
function VideoPosition_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VideoPosition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over DeviceListBox.
function DeviceListBox_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to DeviceListBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over VideoPosition.
function VideoPosition_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to VideoPosition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function MouseIDTextBox_Callback(hObject, eventdata, handles)
% hObject    handle to MouseIDTextBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MouseIDTextBox as text
%        str2double(get(hObject,'String')) returns contents of MouseIDTextBox as a double


% --- Executes during object creation, after setting all properties.
function MouseIDTextBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MouseIDTextBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function FoundMouseTB_Callback(hObject, eventdata, handles)
% hObject    handle to FoundMouseTB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of FoundMouseTB as text
%        str2double(get(hObject,'String')) returns contents of FoundMouseTB as a double


% --- Executes during object creation, after setting all properties.
function FoundMouseTB_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FoundMouseTB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit5_CreateFcn(hObject, eventdata, handles)
function edit3_CreateFcn(hObject, eventdata, handles)


% --- Executes on slider movement.
function MinAreaSlider_Callback(hObject, eventdata, handles)
% hObject    handle to MinAreaSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
if mod(get(hObject,'Value'), 10) ~= 0
    set(hObject,'Value',get(hObject,'Value') - mod(get(hObject,'Value'), 1));
end
set(handles.MinAreaEdit, 'String', get(hObject,'Value'));
global RTTrackingData;
RTTrackingData.Params.MinArea = get(hObject,'Value');

% --- Executes during object creation, after setting all properties.
function MinAreaSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MinAreaSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function MinAreaEdit_Callback(hObject, eventdata, handles)
% hObject    handle to MinAreaEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MinAreaEdit as text
%        str2double(get(hObject,'String')) returns contents of MinAreaEdit as a double


% --- Executes during object creation, after setting all properties.
function MinAreaEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MinAreaEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in SaveVideoFileButton.
function SaveVideoFileButton_Callback(hObject, eventdata, handles)
% hObject    handle to SaveVideoFileButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global RTTrackingData;
if strcmp(get(handles.SaveVideoFileButton, 'String'), 'Done')
    set(handles.SaveVideoFileButton, 'String', 'Save');
    close(RTTrackingData.Output);
    RTTrackingData.Output = [];
    RTTrackingData.OutputFile = [];
    return;
end
[FileName, PathName] = uiputfile('*.avi');
RTTrackingData.OutputFile = [PathName FileName];

output = VideoWriter(RTTrackingData.OutputFile);

if isfield(handles, 'CurrDevice') && isfield(handles.CurrDevice, 'obj') && isvalid(handles.CurrDevice.obj)
    obj = handles.CurrDevice.obj;
    meta = get(obj);
    if handles.CurrDevice.ID < 0
        output.FrameRate = meta.FrameRate;
    else
        output.FrameRate = 1.0/meta.TimerPeriod;
    end
end

open(output);
set(handles.SaveVideoFileButton, 'String', 'Done');
RTTrackingData.Output = output;



function MainBox_Callback(hObject, eventdata, handles)
% hObject    handle to MainBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MainBox as text
%        str2double(get(hObject,'String')) returns contents of MainBox as a double


% --- Executes during object creation, after setting all properties.
function MainBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MainBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function HandleMouseOld(id)
global RTGuiData;
%%

data = get(RTGuiData.handles.MouseTable, 'Data');
for i=1:RTGuiData.nMice
    if RTGuiData.LastEventTime{i} < 0
        data{i, 1} = NaN;
        data{i, 2} = 0;
    else
        data{i, 1} = sec2time(toc(RTGuiData.LastEventTime{i}));
        data{i, 2} = 0;
    end
end
set(RTGuiData.handles.MouseTable, 'Data', data);

%%
z = zeros(1, 4);
if isempty(RTGuiData.Event) || RTGuiData.Event.ID ~= id
%    Alert('');
    if id > 0
        RTGuiData.Event.ID = id;
        RTGuiData.Event.Start = tic;
    else
        RTGuiData.Event = [];
    end
else
    valid = true;
    if RTGuiData.LastEventTime{id} > 0
        dt = toc(RTGuiData.LastEventTime{id});
        if dt < RTGuiData.TimeBetweenActivations
            valid = false;
            Alert('Waiting #%d (%.1f/%.0f)', id, dt, RTGuiData.TimeBetweenOns);
        end
    end
    if valid
        dt = toc(RTGuiData.Event.Start);
        if dt < RTGuiData.TimeToStart
            Alert('Prepare #%d (%.1f/%.1f)', id, dt, RTGuiData.TimeToStart);
        elseif dt < RTGuiData.TimeToStart + RTGuiData.ActivationDuration
            z = zeros(1, 4);
            z(id) = 1;
            Alert('Open #%d', id);
        else
            RTGuiData.LastEventTime{id} = tic;
            Alert('Done #%d', id);
        end
    end
end
try
    if ~RTGuiData.Warnings.DAQ
        RTGuiData.DAQ.outputSingleScan(z);
    end
catch me
    if ~RTGuiData.Warnings.DAQ
        fprintf('failed setting daq output: %s\n', me.message);
        Message('failed setting daq output: %s\n', me.message);
        RTGuiData.Warnings.DAQ = true;
    end
end
