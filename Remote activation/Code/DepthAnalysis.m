function DepthAnalysis(z)
%%
while true
    subplot(4,1,1:3);
    imagesc(z);
    axis off
    h = imline();
    pos = h.getPosition;
    x = linspace(pos(1, 1), pos(2,1), 100);
    y = linspace(pos(1, 2), pos(2,2), 100);
    [X1, X2] = ndgrid(1:size(z,1), 1:size(z, 2));
    F = griddedInterpolant(X1, X2, z);
    subplot(4,3,10);
    plot([0 cumsum(sqrt(diff(y).^2 + diff(x).^2))],...
        100 * F(y(:), x(:)))
    ylabel('cm');
    h.delete();
end
%
% pos = round(h.getPosition)
%
% z(x, y) = max(z(:));
% imagesc(z);
