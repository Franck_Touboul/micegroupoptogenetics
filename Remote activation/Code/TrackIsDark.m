function b = TrackIsDark(obj, frame, thresh)

%orig = im2double(myMMReader(obj.VideoFile, frame));
orig = im2double(frame);
lum = max(orig,[],3);
lum = mean(lum(:));
b = (lum < thresh);
    
