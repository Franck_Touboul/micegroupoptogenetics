function TrackShow(obj, frame)
%img = im2double(obj.BkgImage);
img = im2double(obj.BkgImage);
cmap = lines(obj.ROI.nRegions);

for i=1:obj.ROI.nRegions
    img = TrackShowRegion(img, obj.ROI.Regions{i}, cmap(i, :));
end
imagesc(img);
hold on;

cmap = [0 0 0; lines(obj.ROI.nRegions)];
for i=1:obj.nSubjects
    plot(obj.x(i, frame), obj.y(i, frame), 'x', 'Color', obj.Colors.Centers(i, :));
    %plot(obj.x(i, frame), obj.y(i, frame), 'o', 'Color', cmap(obj.zones(i, frame) + 1, :));
end
hold off;