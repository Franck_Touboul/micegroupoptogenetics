SocialExperimentData
%%
corrupt = {};
output = false;
infoMat = [];
outMat = [];
ppMat = [];
entMat = [];
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions', 'zones', 'Hierarchy', 'sheltered'});
        catch me
            MyWarning(me)
            corrupt = {corrupt{:}, prefix};
            continue
        end
        if ~isfield(obj, 'Interactions')
            obj = TrackLoad([obj.OutputPath prefix '.obj.mat'], {'common'});
            obj = SocialPredPreyModel(obj);
            obj = SocialAnalysePredPreyModel(obj);
        end
        if ~isfield(obj.Interactions.PredPrey, 'PostPredPrey');
            obj = SocialAnalysePredPreyModel(obj);
        end
        %%
        outMat = [outMat; mean(obj.sheltered,2)'];
        %%
        cEnt = zeros(obj.nSubjects);
        Ent = zeros(1, obj.nSubjects);
        tcEnt = zeros(1, obj.nSubjects);
        for m1=1:obj.nSubjects
            for m2=1:obj.nSubjects
                z1 = obj.zones(m1, :);
                if m1 == m2;
                    Ent(m1) = JointEntropy(z1);
                    %tcEnt(m1) = ConditionalEntropy(obj.zones([m1 seq(seq ~= m1)], :));
                    continue;
                end;
                %                     z1 = obj.zones(m1, ~obj.sheltered(m1, :) & ~obj.sheltered(m2, :));
                %                     z2 = obj.zones(m2, ~obj.sheltered(m1, :) & ~obj.sheltered(m2, :));
                
                z2 = obj.zones(m2, :);
                cEnt(m1, m2) = ConditionalEntropy([z2; z1]);
                %cEnt(m1, m2) = ConditionalEntropy(obj.zones([m2 seq(seq ~= m2)], :)) ./ ConditionalEntropy(obj.zones([m2 seq(seq ~= m1 & seq ~= m2)], :));
            end
        end
        cEnt = cEnt./repmat(Ent, obj.nSubjects, 1);
        mat = cEnt - cEnt';
        infoMat = cat(3, infoMat, mat);
        entMat = [entMat; Ent];
        obj.Colors.Centers = MyMouseColormap;
        
        currmat = mat; currmat(1:obj.nSubjects+1:end) = nan;
        mat = mat .* (mat > 0);
        [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
        [~, bg] = SocialGraph(obj, mat, removed, rank);
        if output
            g = biograph.bggui(bg);
            f = get(g.biograph.hgAxes, 'Parent');
            infofile = [obj.OutputPath 'info.' prefix '.SocialInfoHierarchyBatch_v2.png'];
            print(f, infofile, '-dpng');
            close(g.hgFigure);
        end
        if ~isfield(obj, 'Hierarchy') || ~isfield(obj.Hierarchy, 'Info');
            obj = TrackLoad(['Res/' prefix '.obj.mat']);
            obj.Hierarchy.Info.mat = mat;
            TrackSave(obj);
        end
        %%
        curr = obj.Interactions.PredPrey;
        mat = curr.PostPredPrey - curr.PostPredPrey';
        mat(binotest(curr.PostPredPrey, curr.PostPredPrey + curr.PostPredPrey')) = 0;
        ppMat = cat(3, ppMat, mat);

        mat = mat .* (mat > 0);
        
        [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
        ncontacts = sum(curr.Contacts, 2);
        labels = {};
        %         for i=1:obj.nSubjects
        %             labels{i} = num2str(ncontacts(i));
        %         end
        [~, bg] = SocialGraph(obj, mat, removed, rank, [], labels);
        if output
            g = biograph.bggui(bg);
            f = get(g.biograph.hgAxes, 'Parent');
            ppfile = [obj.OutputPath 'predprey.' prefix '.SocialInfoHierarchyBatch_v2.png'];
            print(f, ppfile, '-dpng');
            close(g.hgFigure);
        else
            %%
%             h = subplot(2,2,day);
%             cla(h)
%             a1 = gca;
%             fig2 = get(g.biograph.hgAxes,'children');
%             copyobj(fig2, a1)
%             axis off;
%             set(gcf, 'Color', 'w');
%             title(['Day ' num2str(day)]);
%             close(g.hgFigure);
        end
        if output
            f1 = imread(infofile);
            f2 = imread(ppfile);
            f = [ImgAutoCrop(f1), ImgAutoCrop(f2)];
            file = ['Res/hierarchy.' prefix '.SocialInfoHierarchyBatch_v2.png'];
            %delete(infofile);
            %delete(ppfile);
            imwrite(f, file);
        end
    end
end

return
%% measure correlation between info hier and chase-escape hier
% infoMat = [];
% ppMat = [];
% for id=1:length(experiments)
%     for day = 1:nDays
%         prefix = sprintf(experiments{id}, day);
%         fprintf('#-------------------------------------\n# -> %s\n', prefix);
%         obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Hierarchy'});
%         infoMat = cat(3, infoMat, obj.Hierarchy.Info.mat);
%         ppMat = cat(3, ppMat, obj.Hierarchy.ChaseEscape.map);
%     end
% end
%%
groups = [];
for id=1:length(experiments)
    groupId = 1;
    localId = id;
    for g=1:length(Groups)
        if any(id == Groups(g).idx)
            localId = find(id == Groups(g).idx, 1);
            groupId = g;
            break;
        end
    end
    for day = 1:nDays
        groups = [groups, groupId];
    end
end

%
fakeMat = [];
for i=1:size(entMat, 1)
    m = zeros(4);
    for j=1:obj.nSubjects
        m(j, :) = 2 * (entMat(i, j) > entMat(i, :)) - 1;
    end
    m(1:5:end) = 0;
    fakeMat = cat(3, fakeMat, m);
end

cFakeMat = fakeMat;
cFakeMat(cFakeMat == 0) = nan;

cInfoMat = infoMat;
cInfoMat(cInfoMat == 0) = nan;

cOutMat = [];
for j=1:size(outMat, 1)
    m = repmat(outMat(j, :)', 1, obj.nSubjects);
    m = m - m';
    cOutMat = cat(3, cOutMat, m);
end
cOutMat(cOutMat == 0) = nan;

cPPMat = ppMat;
cPPMat(cPPMat == 0) = nan;

g=1;
nValid = ~isnan(cPPMat(:, :, groups == g)); nValid = sum(nValid(:));
match = sign(cInfoMat(:, :, groups == g)) == sign(-cPPMat(:, :, groups == g));

match = sign(cFakeMat(:, :, groups == g)) == sign(cPPMat(:, :, groups == g));

sum(match(:)) / nValid
%infoMat

nValid = ~isnan(cPPMat(:, :, groups == g)); nValid = sum(nValid(:));
match = sign(cOutMat(:, :, groups == g)) == sign(-cPPMat(:, :, groups == g));

sum(match(:)) / nValid

return;
%%
experiments = {...
    'Enriched.exp0006.day%02d.cam01',...
    'Enriched.exp0005.day%02d.cam04',...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam01',...
    'SC.exp0007.day%02d.cam04',...
    };

E.map = logical([1 1 1 0 1 0 1 0 1 0 0 0 0]);
E.idx = find(E.map);

SC.map = ~E.map;
SC.idx = find(SC.map);

nDays = 4;
%%
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        obj = TrackLoad(['Res/' prefix '.obj.mat']);
        obj = func(obj);
    end
end





return;

AllGroups = {...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam04',...
    };

EnrichedGroups = {...
    'Enriched.exp0001.day%02d.cam04',...
    'Enriched.exp0002.day%02d.cam04',...
    'Enriched.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    };

%objs = {};
%%
a = EnrichedGroups;
all = {};
parfor i=1:length(a)
    curr = {};
    for day = 1:4;
        prefix = sprintf(a{i}, day);
        fprintf('# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions', 'Common'});
            %obj = SocialPredPreyModel(obj);
            obj = SocialAnalysePredPreyModel(obj);
            curr{day} = obj.Interactions.PredPrey;
            %saveFigure(['Graphs/' prefix '.SocialAnalysePredPreyModel']);
        catch me
            fprintf(['#   . FAILED! ' me.message '\n']);
            for j=1:length(me.stack)
                fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
            end
        end
    end
    all{i} = curr;
end

%%
PostPred = [];
PostPrey = [];
PrePred = [];
PrePrey = [];
Contacts = [];
for i=1:length(a)
    for day = 1:4;
        p = all{i}{day}.PostPred;
        p(1:first.nSubjects+1:end) = nan;
        PostPred = cat(3, PostPred, p);
        
        p = all{i}{day}.PostPrey;
        p(1:first.nSubjects+1:end) = nan;
        PostPrey = cat(3, PostPrey, p);
        
        p = all{i}{day}.PrePred;
        p(1:first.nSubjects+1:end) = nan;
        PrePred = cat(3, PrePred, p);
        
        p = all{i}{day}.PrePrey;
        p(1:first.nSubjects+1:end) = nan;
        PrePrey = cat(3, PrePrey, p);
        
        p = all{i}{day}.Contacts;
        p(1:first.nSubjects+1:end) = nan;
        Contacts = cat(3, Contacts, p);
    end
end
% x = sequence(0, max([PostPred(:), PrePred(:)]), 20);
% hPostPred = histc(PostPred(:),x);
% hPrePred = histc(PrePred(:),x);
% plot(x, hPostPred, x, hPrePred)
% legend('post', 'pre');

subplot(2,1,1);
plot(PostPrey(:)./Contacts(:), PrePred(:)./Contacts(:), '.')

subplot(2,1,2);
plot(PostPred(:)./Contacts(:), PostPrey(:)./Contacts(:), '.')
%%
a = AllGroups;
for i=6 %:length(a)
    all = {};
    PostPred = zeros(4);
    PostPrey = zeros(4);
    PrePred = zeros(4);
    objs = {};
    parfor day = 1:4;
        prefix = sprintf(a{i}, day);
        fprintf('# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions', 'Common'});
            if day==1
                objs{day} = obj;
            end
            %obj = SocialPredPreyModel(obj);
            obj = SocialAnalysePredPreyModel(obj);
            all{day} = obj.Interactions.PredPrey;
            PostPred = PostPred + obj.Interactions.PredPrey.PostPred;
            PostPrey = PostPrey + obj.Interactions.PredPrey.PostPrey;
            PrePred = PrePred + obj.Interactions.PredPrey.PrePred;
            %saveFigure(['Graphs/' prefix '.SocialAnalysePredPreyModel']);
            objs{day} = obj;
        catch me
            fprintf(['#   . FAILED! ' me.message '\n']);
            for j=1:length(me.stack)
                fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
            end
        end
    end
    %%
    first = objs{1};
    %     matPred = PostPred - PostPred';
    %     matPrey = PostPrey - PostPrey';
    %     mat = matPred - matPrey;
    %     mat(binotest(PostPred, PostPred + PostPred') & binotest(PostPrey, PostPrey + PostPrey')) = 0;
    %     mat = mat .* (mat > 0);
    %     [rank, removed] = TopoFeedbackArcSetOrder(mat);
    
    nDays = 4;
    rank = zeros(nDays, first.nSubjects);
    for day = 1:nDays
        matPred = all{day}.PostPred - all{day}.PostPred';
        matPrey = (all{day}.PostPrey - all{day}.PostPrey') * 0;
        mat = matPred - matPrey;
        mat(binotest(all{day}.PostPred, all{day}.PostPred + all{day}.PostPred') & ...
            binotest(all{day}.PostPrey, all{day}.PostPrey + all{day}.PostPrey')) = 0;
        mat = mat .* (mat > 0);
        [rank(day, :), removed] = TopoFeedbackArcSetOrder(mat ./ (all{day}.Contacts + all{day}.Contacts'));
    end
    rank = sum(rank) ./ (sum(rank > 0) + 1);
    
    for day = 1:nDays
        prefix = sprintf(a{i}, day);
        
        matPred = all{day}.PostPred - all{day}.PostPred';
        matPrey = (all{day}.PostPrey - all{day}.PostPrey') * 0;
        mat = matPred - matPrey;
        mat(binotest(all{day}.PostPred, all{day}.PostPred + all{day}.PostPred') & ...
            binotest(all{day}.PostPrey, all{day}.PostPrey + all{day}.PostPrey')) = 0;
        mat = mat .* (mat > 0);
        
        szc = sum(all{day}.Contacts);
        sz = sum(all{day}.PostPred, 2)';
        
        removed = ((mat > 0) .* repmat(rank, first.nSubjects, 1)) > repmat(rank', 1, first.nSubjects);
        [~, bg] = SocialGraph(first, mat, removed, rank, sz);
        for k=1:length(bg.nodes)
            bg.nodes(k).ID = sprintf('%s(%d/%d)', bg.nodes(k).ID, sz(k), szc(k));
        end
        
        g = biograph.bggui(bg);
        f = get(g.biograph.hgAxes, 'Parent');
        print(f, [first.OutputPath 'predprey.' prefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');
        fprintf(['# - saving to ' first.OutputPath 'predprey.' prefix '.SocialAnalysePredPreyModel_v2.png\n']);
        close(g.hgFigure);
        %%
        if 1 == 2
            matPred = all{day}.PrePred - all{day}.PrePred';
            matPrey = 0;
            mat = matPred - matPrey;
            mat(binotest(all{day}.PrePred, all{day}.PrePred + all{day}.PrePred')) = 0;
            mat = mat .* (mat > 0);
            
            szc = sum(all{day}.Contacts);
            sz = sum(all{day}.PostPred, 2)';
            
            removed = ((mat > 0) .* repmat(rank, first.nSubjects, 1)) > repmat(rank', 1, first.nSubjects);
            [~, bg] = SocialGraph(first, mat, removed, rank, sz);
            for k=1:length(bg.nodes)
                bg.nodes(k).ID = sprintf('%s(%d/%d)', bg.nodes(k).ID, sz(k), szc(k));
            end
            
            g = biograph.bggui(bg);
            f = get(g.biograph.hgAxes, 'Parent');
            print(f, [first.OutputPath 'initiate.' prefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');
            close(g.hgFigure);
            
            %%
            matPred = all{day}.PostPredPrey - all{day}.PostPredPrey';
            matPrey = 0;
            mat = matPred - matPrey;
            mat(binotest(all{day}.PostPredPrey, all{day}.PostPredPrey + all{day}.PostPredPrey')) = 0;
            mat = mat .* (mat > 0);
            
            szc = sum(all{day}.Contacts);
            sz = sum(all{day}.PostPred, 2)';
            
            removed = ((mat > 0) .* repmat(rank, first.nSubjects, 1)) > repmat(rank', 1, first.nSubjects);
            [~, bg] = SocialGraph(first, mat, removed, rank, sz);
            for k=1:length(bg.nodes)
                bg.nodes(k).ID = sprintf('%s(%d/%d)', bg.nodes(k).ID, sz(k), szc(k));
            end
            
            g = biograph.bggui(bg);
            f = get(g.biograph.hgAxes, 'Parent');
            print(f, [first.OutputPath 'ppp.' prefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');
            close(g.hgFigure);
        end
    end
end
fprintf('# done\n');return;
%%
for i=1
    for day = 2;
        prefix = sprintf(a{i}, day);
        fprintf('# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions'});
            %obj = SocialPredPreyModel(obj);
            obj = SocialAnalysePredPreyModel_v2(obj);
            
            %saveFigure(['Graphs/' prefix '.SocialAnalysePredPreyModel']);
        catch me
            fprintf(['#   . FAILED! ' me.message '\n']);
            for j=1:length(me.stack)
                fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
            end
        end
    end
end
%%
a = {...
    'Enriched.exp0001.day01.cam04',...
    'Enriched.exp0002.day01.cam04',...
    'Enriched.exp0003.day01.cam01',...
    'Enriched.exp0004.day01.cam01',...
    'SC.exp0001.day01.cam01',...
    'SC.exp0002.day01.cam01',...
    'SC.exp0003.day01.cam01',...
    'SC.exp0004.day01.cam04',...
    };

%objs1 = {};
%
for i=1:length(objs1)
    fprintf('# -> %s\n', objs1{1}.FilePrefix);
    try
        obj = objs1{i};
        %curr =load(['../base/Res/' a{i} '.obj.mat']);
        %curr.obj = SocialPredPreyModel(curr.obj);
        obj = SocialAnalysePredPreyModel(obj);
        %objs1{i} = curr.obj;
        saveFigure(['Graphs/' objs1{1}.FilePrefix '.SocialAnalysePredPreyModel2']);
    catch
    end
end