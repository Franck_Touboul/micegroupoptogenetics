function p = VonMisesDistribution(x, m, v)
k = 1 / v;
b = besseli(0, k);
p = exp(k * cos(x - m)) / ( 2 * pi * b);
