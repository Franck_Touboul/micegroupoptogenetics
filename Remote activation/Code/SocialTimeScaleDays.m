fs_ = [1 2 3 6 12 25 50 125 250 750 1500 3000 7500 15000];
nIters = 1;
stat = [];
stat.src= {};
stat.map= {};
idx = 1;
fsidx = 1;
%%
for fs=fs_
    %%
    trainobj = SocialSetTimeScale(obj, fs);
    trainobj.OutputToFile = false;
    trainobj.OutputInOldFormat = false;
    
    testobj = SocialSetTimeScale(other, fs);
    testobj.OutputToFile = false;
    testobj.OutputInOldFormat = false;
    
    stat.train{idx} = trainobj;
    stat.test{idx} = testobj;
    %%
    fprintf('# - computing probabilities of the train data\n');
    [q, independentProbs, jointProbs] = SocialComputePatternProbsFast(trainobj);
    stat.jointProbs(idx, :) = jointProbs(:)';
    fprintf('# - computing probabilities of the test data\n');
    [q, independentProbs, jointProbs] = SocialComputePatternProbsFast(testobj);
    stat.testProbs(idx, :) = jointProbs(:)';
    stat.fs(idx) = fs;
    fprintf('# - estimating Potts model\n');
    stat.bkgobj(idx) = RunInBackground(SocialPackageObjForPotts(trainobj), @SocialPottsAllSubGroups);
    %    stat.bkgobj(idx) = SocialPotts(SocialPackageObjForPotts(trainobj));
    %%
    idx = idx + 1;
    fsidx = fsidx + 1;
end
%%
stat.obj = {};
%
for idx=1:length(stat.fs)
    %%
    stat.obj{idx} = TrackLoad(stat.bkgobj(idx));
end
%%
idx = 1;
Djs = [];
meDjs = [];
res.Djs.mean = [];
res.Djs.stderr = [];
res.meDjs.mean = [];
res.meDjs.stderr = [];
for idx=1:length(stat.fs)
    Djs(idx) = JensenShannonDivergence(stat.jointProbs(idx, :), stat.testProbs(idx, :));
    meDjs(idx) = JensenShannonDivergence(stat.obj{idx}.Analysis.Potts.Model{2}.prob', stat.testProbs(idx, :));
end
idx = 1;
for fs=fs_
    %%
    res.Djs.mean(idx) = mean(Djs(stat.fs == fs));
    res.Djs.stderr(idx) = stderr(Djs(stat.fs == fs));
    res.meDjs.mean(idx) = mean(meDjs(stat.fs == fs));
    res.meDjs.stderr(idx) = stderr(meDjs(stat.fs == fs));
    idx = idx + 1;
end
%
cmap = MyCategoricalColormap;
errorbar(fs_, res.Djs.mean, res.Djs.stderr, 'o-', 'color', cmap(1, :), 'LineWidth', 2, 'MarkerFaceColor', cmap(1, :)); hon;
errorbar(fs_, res.meDjs.mean, res.meDjs.stderr, 'o-', 'color', cmap(2, :), 'LineWidth', 2, 'MarkerFaceColor', cmap(2, :)); hoff;
set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');
set(gca, 'XTick', fs_, 'XTickLabel', fs_/25)
xaxis(min(fs_), max(fs_));
xlabel('location windows duration [sec]');
ylabel('Jensen-Shannon divergence [bits]');