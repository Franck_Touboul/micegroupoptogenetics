for i=1:18; 
    for d=1:4; 
        obj = TrackLoad({i d}); 
        %%
        seq = 2:10;
        seq = seq(seq ~= 9);
        seq = seq(seq ~= 6);
        for k=seq
            g = find(all(obj.zones == k));
            if length(g) > 0
                fprintf(['# in "' obj.FilePrefix '" found %-20s starting at %6d\n'], obj.ROI.ZoneNames{k}, g(1));
            end
        end
    end
end
    