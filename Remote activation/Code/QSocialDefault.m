options.test.name = 'trial_1';
options.output_path = 'res/';
%%
filename = [options.output_path options.test.name '.social.mat'];
load(filename, 'social');
%%
social.options.pixelsInCentimeter = 10; % number of pixels in cm
