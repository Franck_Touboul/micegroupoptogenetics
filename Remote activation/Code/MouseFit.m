opt.HeightThresh = 0.01;

Params = struct();

Params.AxisMundi.Yaw = 200/180*pi;
Params.AxisMundi.Pitch = 0;
Params.AxisMundi.Roll = 0/180*pi;
Params.AxisMundi.Scale = 0.02;

Model = ModelCreateF(Params);
Model.SkinMemory = .9;

Points = ModelToPointsF(Model);
ModelShow(Model, Points);

X = Points.Skin.X; Y = Points.Skin.Y; Z = Points.Skin.Z;
%X(Points.Skin.Area > 0) = NaN;
plot3(X,Y,-Z,'.');
hon

plot3(Depth.X,Depth.Y,Depth.Z,'.');
%plot3(Depth.X(Depth.Z > opt.HeightThresh),Depth.Y(Depth.Z > opt.HeightThresh), Depth.X(Depth.Z > opt.HeightThresh)*0-.02, 'r.');

[iY, iX] = meshgrid(1:size(Depth.X, 1), 1:size(Depth.X, 2));

valid = find(~isnan(Depth.X));
r = randi(length(valid), [2, 5000]);
r = valid(r);
pxdist = sqrt(diff(iX(r)).^2 + diff(iY(r)).^2);
rwdist = sqrt(diff(Depth.X(r)).^2 + diff(Depth.Y(r)).^2);
Depth.Resolution = median((rwdist) ./ (pxdist+1)); % [m/pix]


shading interp; colormap(gray);
axis square;
axis equal;
hoff;

%% detect mouse
map = Depth.Z > opt.HeightThresh;
map = bwareaopen(map, 500);
prop = regionprops(map, 'PixelIdxList', 'Centroid', 'MajorAxisLength', 'MinorAxisLength', 'Orientation');
cmDX = Depth.X(round(prop.Centroid(2)), round(prop.Centroid(1)));
cmDY = Depth.Y(round(prop.Centroid(2)), round(prop.Centroid(1)));

X = Points.Skin.X; Y = Points.Skin.Y; Z = Points.Skin.Z;

% find main axes of depth
[~, idx] = max((Depth.X(prop.PixelIdxList) - cmDX).^2 + (Depth.Y(prop.PixelIdxList) - cmDY).^2);
pt1 = prop.PixelIdxList(idx);
[~, idx] = max((Depth.X(prop.PixelIdxList) - Depth.X(pt1)).^2 + (Depth.Y(prop.PixelIdxList) - Depth.Y(pt1)).^2);
pt2 = prop.PixelIdxList(idx);



% center
cmX = Points.Bone.X(Model.CenterOfMass, 1);
cmY = Points.Bone.Y(Model.CenterOfMass, 1);
X = X - cmX;
Y = Y - cmY;

% rotate
rot = atan2(aY, aX);
R = RotMatRz(prop.Orientation / 180 * pi - rot);

% scale
aX = diff(Points.Bone.X(Model.Axis, 1));
aY = diff(Points.Bone.Y(Model.Axis, 1));
R = R * (prop.MajorAxisLength * Depth.Resolution) / sqrt(aX.^2+aY.^2);
pos = [X(:), Y(:), Z(:)] * R';

% align
pos(:, 1) = pos(:, 1) + cmDX+0.0;
pos(:, 2) = pos(:, 2) + cmDY-0.010;

plot3(pos(:, 1),pos(:, 2),-pos(:, 3),'.');
hon
plot3(Depth.X(Depth.Z > opt.HeightThresh),Depth.Y(Depth.Z > opt.HeightThresh), Depth.X(Depth.Z > opt.HeightThresh)*0+.05, 'r.');
%plot3(Points.Bone.X(Model.CenterOfMass, 1), Points.Bone.Y(Model.CenterOfMass, 1), .05, 'ro');

surfl(Depth.X,Depth.Y,Depth.Z);
shading interp; colormap(gray);
grid on
hoff
view(0, 90)


%% detect mouse (ver 2)
map = Depth.Z > opt.HeightThresh;
map = bwareaopen(map, 500);
prop = regionprops(map, 'PixelIdxList', 'Centroid');
cmDX = Depth.X(round(prop.Centroid(2)), round(prop.Centroid(1)));
cmDY = Depth.Y(round(prop.Centroid(2)), round(prop.Centroid(1)));

X = Points.Skin.X; Y = Points.Skin.Y; Z = Points.Skin.Z;

% find main axes of depth
[~, idx] = max((Depth.X(prop.PixelIdxList) - cmDX).^2 + (Depth.Y(prop.PixelIdxList) - cmDY).^2);
pt1 = prop.PixelIdxList(idx);
[~, idx] = max((Depth.X(prop.PixelIdxList) - Depth.X(pt1)).^2 + (Depth.Y(prop.PixelIdxList) - Depth.Y(pt1)).^2);
pt2 = prop.PixelIdxList(idx);
Res.Depth.MajorAxis = sqrt((Depth.X(pt1) - Depth.X(pt2)).^2 + (Depth.Y(pt1) - Depth.Y(pt2)).^2);
Res.Depth.Pt1 = pt1;
Res.Depth.Pt2 = pt2;
Res.Depth.Orientation = atan2(Depth.Y(pt1) - Depth.Y(pt2), Depth.X(pt1) - Depth.X(pt2));
Res.Depth.MaxHeight = max(Depth.Z(map));

% center model
cmX = Points.Bone.X(Model.Axis(1), 1);
cmY = Points.Bone.Y(Model.Axis(1), 1);
X = X - cmX;
Y = Y - cmY;
Res.Model.Beg = [Points.Bone.X(Model.Axis(1), 1), Points.Bone.Y(Model.Axis(1), 1)];
Res.Model.End = [Points.Bone.X(Model.Axis(2), 1), Points.Bone.Y(Model.Axis(2), 1)];

% rotate and scale
Res.Model.MajorAxis = sqrt(...
    (Points.Bone.X(Model.Axis(1), 1) - Points.Bone.X(Model.Axis(2), 1)).^2 + ...
    (Points.Bone.Y(Model.Axis(1), 1) - Points.Bone.Y(Model.Axis(2), 1)).^2);
Res.Model.Orientation = atan2(Res.Model.End(2) - Res.Model.Beg(2), Res.Model.End(1) - Res.Model.Beg(1));

R = RotMatRz(Res.Depth.Orientation - Res.Model.Orientation) * ...
    Res.Depth.MajorAxis / Res.Model.MajorAxis;
pos = [X(:), Y(:), Z(:)] * R';

% align position
pos(:, 1) = pos(:, 1) + Depth.X(pt2);
pos(:, 2) = pos(:, 2) + Depth.Y(pt2);

% fix height
Res.Model.MaxHeight = max(-pos(:, 3));
pos(:, 3) = -pos(:, 3) + Res.Depth.MaxHeight - Res.Model.MaxHeight;

% plot
plot3(pos(:, 1),pos(:, 2),pos(:, 3),'.');
hon
plot3(Depth.X(Depth.Z > opt.HeightThresh),Depth.Y(Depth.Z > opt.HeightThresh), Depth.X(Depth.Z > opt.HeightThresh)*0+.05, 'g.');

plot3(Depth.X,Depth.Y,Depth.Z,'r.');
shading interp; colormap(gray);
grid on
hoff
view(0, 90)
%%
map = Depth.Z > opt.HeightThresh;
map = bwareaopen(map, 500);

% depth
prop = regionprops(map, 'PixelIdxList', 'Centroid');
cmDX = Depth.X(round(prop.Centroid(2)), round(prop.Centroid(1)));
cmDY = Depth.Y(round(prop.Centroid(2)), round(prop.Centroid(1)));

% find main axes of depth
[~, idx] = max((Depth.X(prop.PixelIdxList) - cmDX).^2 + (Depth.Y(prop.PixelIdxList) - cmDY).^2);
pt1 = prop.PixelIdxList(idx);
[~, idx] = max((Depth.X(prop.PixelIdxList) - Depth.X(pt1)).^2 + (Depth.Y(prop.PixelIdxList) - Depth.Y(pt1)).^2);
pt2 = prop.PixelIdxList(idx);
Res.Depth.MajorAxis = sqrt((Depth.X(pt1) - Depth.X(pt2)).^2 + (Depth.Y(pt1) - Depth.Y(pt2)).^2);
Res.Depth.Pt1 = pt1;
Res.Depth.Pt2 = pt2;
Res.Depth.Orientation = atan2(Depth.Y(pt1) - Depth.Y(pt2), Depth.X(pt1) - Depth.X(pt2));
Res.Depth.MaxHeight = max(Depth.Z(map));

%% plot
InitModel = [Res.Depth.Orientation-pi, Res.Depth.MajorAxis, Depth.X(pt1), Depth.Y(pt1), Res.Depth.MaxHeight];
InitModel = [Res.Depth.Orientation, Res.Depth.MajorAxis, Depth.X(pt2), Depth.Y(pt2), Res.Depth.MaxHeight];
[z,pos] = ModelToDepthDistance(InitModel);
plot3(pos(:, 1),pos(:, 2),pos(:, 3),'.');
hon
plot3(Depth.X(Depth.Z > opt.HeightThresh),Depth.Y(Depth.Z > opt.HeightThresh), Depth.X(Depth.Z > opt.HeightThresh)*0+.05, 'g.');

plot3(Depth.X,Depth.Y,Depth.Z,'r.');
shading interp; colormap(gray);
grid on
hoff
view(0, 90)



%%
opt.FitThresh = 0.005;
opt.MaxIters = 100;
opt.IterChunkSize = 100;

Data = [Depth.X(map), Depth.Y(map), Depth.Z(map)];
d = ModelToDepthDistance(InitModel, Data);
if d > opt.FitThresh
    func = @(x) ModelToDepthDistance(x, Data);
    searchopt = optimset('Display', 'iter', 'MaxIter', opt.IterChunkSize);
    nIters = 0;
    ModelVec = InitModel;
    while nIters < opt.MaxIters
        [ModelVec, fval] = fminsearch(func, ModelVec, searchopt);
        if fval <= opt.FitThresh
            break;
        end
        nIters = nIters + opt.IterChunkSize;
    end
end

% plot
[z,pos] = ModelToDepthDistance(ModelVec);
plot3(pos(:, 1),pos(:, 2),pos(:, 3),'.');
hon
plot3(Depth.X(Depth.Z > opt.HeightThresh),Depth.Y(Depth.Z > opt.HeightThresh), Depth.X(Depth.Z > opt.HeightThresh)*0+.05, 'g.');

plot3(Depth.X,Depth.Y,Depth.Z,'r.');
shading interp; colormap(gray);
grid on
hoff
view(0, 90)
