classdef MyDAQ
    properties
        daq;
        Is32bit = false;
    end
    methods
        function obj = CreateDigital(obj)
            try
                if strcmp(computer, 'PCWIN')
                    %%
                    fprintf('- using legacy interface (32bit)\n');
                    %%
                    out = daqhwinfo('nidaq');
                    d = out.InstalledBoardIds;
                    names = out.BoardNames;
                    if isempty(d)
                        warning('no devices found');
                    else
                        %%
                        s = [];
                        fprintf('- devices:\n');
                        dev = '';
                        for i=1:length(d)
                            fprintf(['  . ' names{i} '(' d{i} ')\n']);
                            if i==1
                                dev = d{i};
                            end
                        end
                        fprintf('- creating session\n')
                        s.dio = digitalio('nidaq', dev);
                        fprintf('- adding digital channel\n');
                        s.lines = addline(s.dio,0:3,'out');
                        fprintf('- [DONE]\n');
                    end
                    obj.Is32bit = true;
                else
                    fprintf('- using session based interface (64bit)\n');
                    d = daq.getDevices;
                    if length(d) == 0
                        warning('no devices found');
                    else
                        fprintf('- devices:\n');
                        dev = '';
                        for i=1:length(d)
                            fprintf(['  . ' d(i).Description '(' d(i).ID ')\n']);
                            if i==1
                                dev = d(i).ID;
                            end
                        end
                        fprintf('- creating session\n')
                        s = daq.createSession('ni');
                        fprintf('- adding digital channel\n');
                        try
                            s. addDigitalChannel(dev, 'port0/line0:3', 'OutputOnly');
                            fprintf('- [DONE]\n');
                        catch
                            fprintf('- [FAILED]\n');
                        end
                        
                        fprintf('- adding analog channel\n');
                        try
                            s.addAnalogInputChannel(dev, 0, 'Voltage');
                            addlistener(s,'DataAvailable', @LightDetect);
                            s.IsContinuous = true;
                            fprintf('- [DONE]\n');
                            startBackground(s);
                        catch
                            fprintf('- [FAILED]\n');
                        end
                        
                    end
                    obj.Is32bit = false;
                end
                obj.daq = s;
            catch err
                fprintf(['- [FAILED]: ' err.message]);
                warning(err.message);
            end
        end
        
    end
end