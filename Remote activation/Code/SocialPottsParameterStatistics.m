function obj = SocialPottsParameterStatistics(obj, level)

nBins = 40;
weights = obj.Analysis.Potts.Model{level}.weights;

edges = sequence(min(weights), max(weights), nBins + 1);
bins = (edges(1:end-1) + edges(2:end)) / 2;

stat = {};
%%
titles = {'independent', 'pairs', 'triplets', 'quadruplets'};

clf;
for count = 1:level
    r = 3 * (count - 1) + 1:3 * count;
    subj.mean = zeros(1, obj.nSubjects);
    subj.stderr = zeros(1, obj.nSubjects);
    for i=1:obj.nSubjects
        stat{i, count} = [];
        for j=1:length(obj.Analysis.Potts.Model{level}.labels)
            currLabel = obj.Analysis.Potts.Model{level}.labels{j};
            if size(currLabel, 2) == count && any(currLabel(1, :) == i)
                stat{i, count} = [stat{i, count}, weights(j)];
            end
        end
        h = histc(stat{i, count}, edges);
        subplot(level,3,r(1):r(2));
        plot(bins, h(1:end-1), '.-', 'Color', obj.Colors.Centers(i, :)); hold on;
        subj.mean(i) = mean(stat{i, count});
        subj.stderr(i) = stderr(stat{i, count});
    end
    graphTitle = ['weights histogram: ' titles{count}];
    if count == 1
        title({obj.FilePrefix;graphTitle});
    else
        title(graphTitle);
    end        
    subplot(level,3,r(3));
    barweb(subj.mean, subj.stderr, [], [], [], [], [], obj.Colors.Centers);
    title(['average weight: ' titles{count}]);
end

