%% background
clf;
fprintf('# computing background frame\n');
bkgBuf = zeros([vid.Height, vid.Width, 3, params.nBkgFrame]);
recount = 0;
for i=1:params.nBkgFrame
    recount = reprintf(recount, '#   . background frame no. %d/%d', i, params.nBkgFrame);
    frame = randi(nFrames);
    im = read(vid, frame); 
    im = im2double(im);
    bkgBuf(:, :, :, i) = im;
    %%
    subplot(5, 5, mod(i-1, 25)+1);
    imagesc(im);
    axis off;
    title(sprintf('frame %d', frame));
    drawnow;
end
%%
bkg.median = median(bkgBuf, 4);
bkg.std = 1.4826 * mad(bkgBuf, 1, 4);
