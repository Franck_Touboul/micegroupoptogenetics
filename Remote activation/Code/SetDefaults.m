options.test.id = 1;

options.nSubjects = 4; % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts

options.save = true;

loadData_v2b
