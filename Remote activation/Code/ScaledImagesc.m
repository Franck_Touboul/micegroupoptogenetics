function ScaledImagesc(a, showbar)
nlevels = 256;
cmap = MyDefaultColormap(nlevels);

c = a(~isnan(a));
m1 = mean(c(:)) - std(c(:));
m2 = mean(c(:)) + std(c(:));
r = (a - m1) / (m2 - m1); 
idx = round(r * (nlevels - 1)) + 1;
idx(isnan(idx)) = 1;
idx(idx < 1) = 1;
idx(idx > nlevels) = nlevels;
m = ind2rgb(idx, cmap);
% m = zeros([size(idx,1), size(idx,2), 3]);
% for i=1:size(idx,1)
%     for j=1:size(idx,2)
%         m(i,j,:)=reshape(cmap(idx(i, j), :), 1, 1, 3);
%     end
% end
m(cat(3, isnan(a), cat(3, isnan(a), isnan(a)))) = 0;
image(m);
if exist('showbar', 'var') && showbar
    colormap(cmap);
    h = colorbar;
    ticks = get(h, 'YTick') / 256 * (m2 - m1) + m1;
    labels = {};
    for i=1:length(ticks)
        labels{i} = sprintf('%.2f', ticks(i));
    end
    set(h, 'YTickLabel', labels);
end
