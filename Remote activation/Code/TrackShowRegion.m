function img = TrackShowRegion(img, roi, color)
r = img(:, :, 1);
g = img(:, :, 2);
b = img(:, :, 3);

%color = color / 2 + .5;
% r(roi) = r(roi) / max(r(roi)) * color(1);
% g(roi) = g(roi) / max(g(roi)) * color(2);
% b(roi) = b(roi) / max(b(roi)) * color(3);

boundries = (imdilate(roi ~= 0, ones(5)) - (roi ~= 0)) ~= 0;
r(boundries) = color(1);
g(boundries) = color(2);
b(boundries) = color(3);

img = cat(3, r, g, b);