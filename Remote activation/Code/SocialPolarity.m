function obj = SocialPolarity(obj)

obj = TrackLoad(obj);
%% subject permuted

[obj, indepProbs, jointProbs, pci] = SocialComputePatternProbs(obj, false);
SPPerms = perms(1:obj.nSubjects);
SPPermOrder = sum(SPPerms ~= repmat(1:obj.nSubjects, size(SPPerms, 1), 1), 2);

SPAllJSD = zeros(1, size(SPPerms, 1));
nchars = 0;
for i=1:size(SPPerms, 1)
    nchars = Reprintf(nchars, '#  . perm no. %d/%d', i, size(SPPerms, 1));
    CurrSPObj = obj;
    CurrSPObj.zones = obj.zones(SPPerms(i, :), :);
    [CurrSPObj, CurrSPIndepProbs, CurrSPJointProbs] = SocialComputePatternProbs(CurrSPObj, false);
    SPAllJSD(i) = JensenShannonDivergence(jointProbs, CurrSPJointProbs);
end
fprintf('\n');
obj.Permutations.Subjects.Djs = SPAllJSD;
obj.Permutations.Subjects.order = SPPermOrder;
TrackSave(obj);