function vert_line(X, varargin)
a = axis;
if nargin >= 2
    for i=1:length(X)
        x = X(i);
        line([x, x], [a(3) a(4)], varargin{:});
    end
else
    for i=1:length(X)
        x = X(i);
        line([x, x], [a(3) a(4)]);
        
    end
end
