%function [obj, me] = SocialRegularizedPotts(obj, options)

obj = TrackLoad(obj);
trainobj = obj;
%map = rand(1, obj.nFrames) >= .5;
%map = mod(floor((1:obj.nFrames) / (obj.FrameRate * 60)), 2) == 1;
map = (1:obj.nFrames) > (obj.nFrames / 2);
trainobj.valid = obj.valid(map);
trainobj.zones = obj.zones(:, map);
trainobj.nFrames = sum(map);

testobj = obj;
testobj.valid = obj.valid(~map);
testobj.zones = obj.zones(:, ~map);
testobj.nFrames = sum(~map);

options = [];
options = setDefaultParameters(options, ...
    'order', 1:trainobj.nSubjects,...
    'nIters', [500 1000 1000],...
    'confidence', 0.05,...
    'betas', .5 .^ (-3:12));
trainobj.Analysis.Potts = options;
%%
data = zeros(trainobj.nSubjects, sum(trainobj.valid));
for s=1:trainobj.nSubjects
    currZones = trainobj.zones(s, :);
    data(s, :) = currZones(trainobj.valid == 1) - 1;
end
%%
options.betas = unique(options.betas(options.betas ~= 0));
me = {};
idx = 1;
for level=options.order
    fprintf('#    - beta %d\n', 0);
    fprintf('#      . level %d\n', level);
    local = trainobj;
    local.n = level;
    local.output = true;
    local.count = trainobj.ROI.nZones;
    local.nIters = options.nIters(1);
    local.beta = 0;
    local.MinNumberOfIters = 0;
    local.KLConvergenceRatio = 0;
    if local.nIters > 0
        me{idx} = trainPottsModelUsingGIS(local, data, options.confidence);
        local.initialPottsWeights = me{idx}.weights;
    end
    %%
    local.nIters = options.nIters(2);
    local = rmfield(local, 'KLConvergenceRatio');
%    me{idx} = trainPottsModelWithRegularization(local, data, options.confidence);
    me{idx} = trainPottsModelUsingNesterovGD(local, data, options.confidence);
    idx = idx + 1;
    
end
%%
for level=1:4
    p = exp(me{level}.perms * me{level}.weights');
    perms_p = exp(me{level}.perms * me{level}.weights');
    Z = sum(perms_p);
    p = p / Z;
    me{level}.prob = p;
end
%%
for level=[3 1 2 4]
    %%
    for beta = options.betas
        fprintf('#    - beta %d\n', beta);
        fprintf('#      . level %d\n', level);
        local = trainobj;
        local.n = level;
        local.beta = beta;
        local.output = true;
        local.count = trainobj.ROI.nZones;
        local.nIters = options.nIters(3);
        local.MinNumberOfIters = 0;
        local.initialPottsWeights = me{level}.weights;
        local.KLConvergenceRatio = 1e-3;
        me{idx} = trainPottsModelWithRegularization(local, data, options.confidence);
        
        p = exp(me{idx}.perms * me{idx}.weights');
        Z = sum(p);
        p = p / Z;
        me{idx}.prob = p;
        
        idx = idx + 1;
    end
end
%%
[trainobj, independentProbs, jointProbs] = SocialComputePatternProbs(trainobj, false);
[testobj, q, testProbs] = SocialComputePatternProbs(testobj, false);
%%
level = 3;
xyz = [];
for i=5:length(me)
    curr = me{i};
    if curr.order == level
        xyz = [xyz; [log2(curr.beta), JensenShannonDivergence(jointProbs, curr.prob'), sum(curr.weights == 0)/length(curr.weights)*100, sum(abs(curr.weights)), JensenShannonDivergence(testProbs, curr.prob')]];
    end
end
%[AX,H1,H2] = plotyy(xyz(:, 1), xyz(:, 2), xyz(:, 1), xyz(:, 3));
[AX,H1] = plotyyy(xyz(:, 1), xyz(:, 5), xyz(:, 1), xyz(:, 3), xyz(:, 1), xyz(:, 4));
hon;
%axes(AX(1));
set(get(AX(1),'Ylabel'),'String','D_{js} [bits]')
set(get(AX(2),'Ylabel'),'String','sparse features [%]')
tokens = {};
t = get(AX(1), 'XTick');
for i=1:length(t)
    tokens{i} = ['2^' num2str(t(i))];
end
for i=1:2
    
    set(AX(i), 'XTickLabel', tokens);
    H = eval(['H' num2str(i)]);
    set(H, 'LineWidth', 2);
    set(H, 'LineStyle', '-');
    set(H, 'Marker', 'o');
end
xlabel('beta');
