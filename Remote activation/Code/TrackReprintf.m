function [obj, s] = TrackReprintf(obj, varargin)

s = TrackSPrintf(obj, varargin);

if obj.Output
    obj.Printf.Length = strlen(s);
    fprintf([s '\n']);
else
    obj.Printf.Length = 0;
end