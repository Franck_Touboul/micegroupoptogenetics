function TransparentLine(x, y, c, alpha, width)
if nargin < 3 || isempty(c)
    c = 'k';
end
if nargin < 4  || isempty(alpha)
    alpha = 1;
end
if nargin < 5
    a = axis;
    width = max(a(2) - a(1), a(4) - a(3)) / 100;
end
orth = [y(2)-y(1) -(x(2)-x(1))];
orth = orth / norm(orth);
w = width / 2;
patch(...
    [x(1) - orth(1) * w x(2) - orth(1) * w x(2) + orth(1) * w x(1) + orth(1) * w], ...
    [y(1) - orth(2) * w y(2) - orth(2) * w y(2) + orth(2) * w y(1) + orth(2) * w], ...
    c, 'FaceColor', c, 'EdgeColor', 'none', 'FaceAlpha', .5);