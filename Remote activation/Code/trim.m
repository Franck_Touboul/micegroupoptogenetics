function [nimg, h1, h2, v1, v2] = trim(img)

zmap = img == 0 | isnan(img);
h = all(zmap, 1);
v = all(zmap, 2);
h1 = find(~h, 1, 'first');
h2 = find(~h, 1, 'last');
v1 = find(~v, 1, 'first');
v2 = find(~v, 1, 'last');
nimg = img(v1:v2, h1:h2);