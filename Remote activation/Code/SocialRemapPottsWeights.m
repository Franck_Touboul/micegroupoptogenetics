function newobj = SocialRemapPottsWeights(obj, map, order)
%%
newobj = obj;
vec = (obj.nSubjects+1).^(0:obj.nSubjects-1);
zvec = (obj.ROI.nZones+1).^(0:obj.nSubjects-1);
remap = {};
if ~exist('order', 'var')
    order = 1:length(obj.Analysis.Potts.Model);
end
for i=order
    %%
    cmodel = obj.Analysis.Potts.Model{i};
    labels = nan(length(cmodel.labels), i);
    zones = nan(length(cmodel.labels), i);
    for l=1:length(cmodel.labels)
        labels(l, 1:length(cmodel.labels{l}(1, :))) = cmodel.labels{l}(1, :);
        zones(l, 1:length(cmodel.labels{l}(1, :))) = cmodel.labels{l}(2, :);
    end
    %%
    newlabels = labels;
    for m=1:length(map)
        newlabels(labels == m) = map(m);
    end
    [newlabels, order] = sort(newlabels, 2);
    newzones = zones(sub2ind(size(zones), repmat((1:size(labels, 1))', 1, i), order));
    %%
    L = labels; L(isnan(L)) = 0;
    nL = newlabels; nL(isnan(nL)) = 0;
    idx = vec(1:i) * L';
    newidx = vec(1:i) * nL';
    %%
    Z = zones; Z(isnan(Z)) = 0;
    nZ = newzones; nZ(isnan(nZ)) = 0;
    zidx = zvec(1:i) * Z';
    newzidx = zvec(1:i) * nZ';
    %%
    remap{i} = zeros(1, size(labels, 1));
    for j=1:size(labels, 1)
        remap{i}(j) = find(idx(j) == newidx & zidx(j) == newzidx);
    end
    %%
    cmodel.features = cmodel.features(:, remap{i});
    cmodel.labels = { cmodel.labels{remap{i}} };
    cmodel.perms = cmodel.perms(:, remap{i});
    cmodel.constraints = cmodel.constraints(remap{i});
    cmodel.weights = cmodel.weights(remap{i});
    cmodel.order = cmodel.order(remap{i});
    newobj.Analysis.Potts.Model{i} = cmodel;
end