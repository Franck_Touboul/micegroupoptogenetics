function obj = SocialPottsRobustnessTestWithoutCorrelation(obj, TimeScale, confidence)
obj = TrackLoad(obj);
if nargin < 2
    TimeScale = 60; % [sec] 
end
%%

map = logical(mod(floor(obj.time / TimeScale), 2));
objs = {};
for i=1:2
    %%
    objs{i} = struct();
    if i==1
        currmap = map;
    else
        currmap = ~map;
    end
    objs{i}.zones = obj.zones(:, currmap);
    objs{i}.valid = obj.valid(currmap);
    objs{i}.OutputToFile = false;
    objs{i}.OutputPath = obj.OutputPath;
    objs{i}.FilePrefix = obj.FilePrefix;
    objs{i}.nSubjects = obj.nSubjects;
    objs{i}.OutputInOldFormat = false;
    objs{i}.ROI = obj.ROI;
    %objs{i} = SocialFilterTemporalCorrelations(objs{i});

end
%
indepProbs = {};
jointProbs = {};
for i=1:2
    if i == 1
        [objs{i}, indepProbs{i}, jointProbs{i}, pci, patterns] = SocialComputePatternProbs(objs{i});
    else
        [objs{i}, indepProbs{i}, jointProbs{i}] = SocialComputePatternProbs(objs{i});
    end
end
PlotProbProb(jointProbs{1}, jointProbs{2}, sum(objs{2}.valid), true);

%%
for i=1:2
    objs{i} = SocialPotts(objs{i}, [], confidence);
end
%
clf
% objs{1} = obj;
% objs{2} = obj;
for s=1:obj.nSubjects
    figure(1);
    subplot(2,2,s);
    PlotProbProb(objs{1}.Analysis.Potts.Model{s}.prob, objs{2}.Analysis.Potts.Model{s}.prob, sum(objs{1}.valid), true, objs{1}.Analysis.Potts.Model{s}.jointProb > 0 & objs{2}.Analysis.Potts.Model{s}.jointProb > 0);
    xlabel(['Even ME (order ' num2str(s) ')']);
    ylabel(['Odd ME (order ' num2str(s) ')']);
    
    figure(2);
    subplot(2,2,s);
    PlotProbProb(objs{1}.Analysis.Potts.Model{s}.jointProb', objs{2}.Analysis.Potts.Model{s}.prob, sum(objs{1}.valid), true, objs{1}.Analysis.Potts.Model{s}.jointProb > 0 & objs{2}.Analysis.Potts.Model{s}.jointProb > 0);
    xlabel(['Even ME (empirical)']);
    ylabel(['Odd ME (order ' num2str(s) ')']);
    
    figure(3);
    subplot(2,2,s);
    PlotProbProb(objs{1}.Analysis.Potts.Model{s}.jointProb', objs{1}.Analysis.Potts.Model{s}.prob, sum(objs{1}.valid), true, objs{1}.Analysis.Potts.Model{s}.jointProb > 0);
    xlabel(['Even ME (empirical)']);
    ylabel(['Even ME (order ' num2str(s) ')']);
    
end

%%
for i=1:3
    figure(i);
    saveFigure(sprintf(['Robustness/' obj.FilePrefix '.SPRT.C%3f.' num2str(TimeScale) 'sec.' num2str(i)], confidence));
end
