[obj, indepProbs, jointProbs, pci, patterns] = SocialComputePatternProbs(obj, false);
%%
trans = [];
for s=1:obj.nSubjects
    trans = cat(3, trans, hmmestimate(obj.zones(s, obj.valid), obj.zones(s, obj.valid)));
end
%%
cJointProbs = cumsum(jointProbs);
idx = sum(rand(1) > cJointProbs) + 1;
start = patterns(idx, :);
states = MChainConstrainGenerate(obj.nFrames, trans, patterns(jointProbs>0, :), start);
%%
nobj = obj;
nobj.zones = states';
[nobj, ~, nJointProbs] = SocialComputePatternProbs(nobj, false);

return;
%%
%  Social behavior code - agrresive/non-aggresive

options.nSubjects = 4; % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts

% 1) Preparing the data matrix for work
% 2) Calculating contacts
% 3) Finding "islands" of "1" or "11" (false contacts)
% 4) Unifying contacts into "engagements" by canceling small gaps
% 5) Creating a cell array of engagements and their respective raw data
% 6) Characterizing the engagements (aggresive or non-aggresive)
% 7) Plots

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1)                 Preparing the data matrix for work
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Loading the Excel file for all mice (if needed)
loadData;

%%
hmm.trans = ...
    [...
    1 1 1 1 1 1 1; ...
    1 1 0 0 0 0 0; ...
    1 0 1 0 0 0 0; ...
    1 0 0 1 0 0 0; ...
    1 0 0 0 1 0 0; ...
    1 0 0 0 0 1 1; ...
    1 0 0 0 0 1 1; ...
    ];
hmm.emis = ...
    [...
    1 0 0 0 0 0 0; ...
    0 1 0 0 0 0 0; ...
    0 0 1 0 0 0 0; ...
    0 0 0 1 0 0 0; ...
    0 0 0 0 1 0 0; ...
    0 0 0 0 0 1 0; ...
    0 0 0 0 0 0 1; ...
    ];

hmm.trans = hmm.trans ./ repmat(sum(hmm.trans, 2), 1, size(hmm.trans, 1));
hmm.labels = {[{'Open'} {'Space'}], [{'Feeder'} {'#1'}], [{'Feeder'} {'#2'}], 'Water', [{'Small'} {'Nest'}], 'Labyrinth', [{'Big'} {'Nest'}]};
for i=1:zones.count
    %    hmm.coord(i, 1) = median(orig.x(zones.all + 1 == i));
    %   hmm.coord(i, 2) = median(orig.y(zones.all + 1 == i));
    
    hmm.size(i, 1) = std(orig.x(zones.all + 1 == i));
    hmm.size(i, 2) = std(orig.y(zones.all + 1 == i));
end
%
hmm.coord = [...
    -40 40;
    60 150;
    -100 150;
    150 80;
    -160 -60;
    150 -30;
    80 -120;
    ...
    ];
%%
set(gcf, 'Color', 'w')
for mice = 1:options.nSubjects
    mouseModel{mice} = hmm;
    mouseModel{mice}.trans = mouseModel{mice}.trans * 0;
    for i=1:size(hmm.trans, 1)
        from = (zones.all(mice, :) == i - 1) * 1;
        mouseModel{mice}.priors(i) = sum(from) / length(zones.all(mice, :));
        %mouseModel{mice}.priors(i) = .99;
        for j=1:size(hmm.trans, 2)
            %if i ~= j
            to = (zones.all(mice, :) == j - 1) * 2;
            mouseModel{mice}.count(i, j) = sum(to(2:end) - from(1:end-1) == 1);
            mouseModel{mice}.trans(i, j) = sum(to(2:end) - from(1:end-1) == 1) / sum(from);
            %end
        end
    end
    mouseModel{mice}.trans = mouseModel{mice}.trans ./ repmat(sum(mouseModel{mice}.trans, 2), 1, size(hmm.trans, 1));
    %
    %    img = drawMarkovModel(mouseModel{mice}, ['res\45min-markov-subject' num2str(mice) '.ps']);
    subplot(ceil(sqrt(options.nSubjects)), ceil(sqrt(options.nSubjects)), mice);
    %     imshow(img);
    %    drawMarkovModelUnidir(mouseModel{mice});
    drawMarkovModelUnidir_v2(mouseModel{mice});
    %saveFigure(['res\45min-unimarkov-subject' num2str(mice)]);
    title(['Mouse no. ' num2str(mice)]);
end
% validateMarkovModel(zones.all(1, :) + 1, hmm)
%
% mouseModel = {};
% for i=1:1 %options.nSubjects
%     [estTrans, estEmis] = hmmtrain(zones.all(i, :) + 1, hmm.trans, hmm.emis, 'Verbose', true);
%     mouseModel{i}.trans = estTrans;
%     mouseModel{i}.emis = estEmis;
% end
% %%
% clf;
% drawMarkovModel(hmm);


