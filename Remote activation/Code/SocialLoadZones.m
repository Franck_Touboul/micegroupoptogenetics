SocialExperimentData;
Z = {};
index = 1;
for id=GroupsData.Standard.idx
    z = [];
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        obj = TrackLoad([obj.OutputPath prefix '.obj.mat'], {'zones', 'valid'});
        z = [z, obj.zones(:, obj.valid)];
    end
    Z{index} = z;
    index = index + 1;
end

%%
Len = [];
for i=1:length(Z)
    for s=1:obj.nSubjects
        [~, ~, len] = FindEvents(Z{i}(s, :) == 4);
        Len = [Len, len];
    end
end
%%