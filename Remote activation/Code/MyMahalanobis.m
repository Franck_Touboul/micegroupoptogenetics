function d = MyMahalanobis(d1, d2, w)
d = zeros(size(d1, 1), size(d2, 1));
for i=1:size(d1, 1)
    for j=1:size(d2, 1)
        df = (d1(i, :) - d2(j, :));
        d(i, j) = sqrt(sum((d1(i, :) - d2(j, :)).^2./w));
    end
end
