function SocialOptimizeBehaviorUsingGA_v2(varargin)
%%
rand('twister',sum(100*clock));
randn('seed',sum(100*clock));
%%
GA.nBest = 50;
GA.MutationProb = 0.1;

%%
defaults = struct();
defaults.PredObjectLength = 50:50:150;
defaults.PreyObjectLength = 50:50:150;
defaults.MinSubInteractionDuration = [1 5 10];
defaults.SmoothSpan = 1:2:5;
defaults.ZoneOfProximity = [50 120 250];
defaults.ZoneOfContact = 50:50:150;
defaults.MinNumOfContacts = 1:3:7;
defaults.MinContanctDuration = [1 5 10]; % minimal duration of a contact
defaults.MaxShelteredContanctDuration = [15 30 45];
defaults.ChaseMinOverlap = [0 5 10];
defaults.RelativeSpeedInChase = 0:.25:.75;
defaults.StrictStates = true;
defaults.MinPreySpeedCMperSec = 0;

%r = randi(5);
%defaults.EmissionMismatch = defaults.EmissionMatch * r / (r+1);

[dims, fields] = ArgumentsToDims(defaults);
id = str2num(getenv('SGE_TASK_ID'));
fprintf('% TASK ID: %d\n', id);
[v, n] = NumToDims(id, dims);
arg = struct();
for i=1:length(fields)
    arg.(fields{i}) = defaults.(fields{i})(v(i) + 1);
end

% arg.EmissionMismatch = defaults.EmissionMismatch(randi(sum(defaults.EmissionMismatch < arg.EmissionMatch)));

%%

%%
objs = {'SC.exp0001.day02.cam01' 'SC.exp0008.day02.cam01' 'SC.exp0004.day02.cam04'};
%objs = {'SC.exp0001.day02.cam01'};
%objs = {'SC.exp0008.day02.cam01'};
%objs = {'SC.exp0004.day02.cam04'};
for o=1:length(objs)
    objname = objs{o};
    fprintf('# %s:\n', objname);
    %%
    data = dlmread(['aux/' objname '.marks'], '\t', [1 0 500 15]);
    marks.ids = data(:, 1);
    marks.beg = data(:, 2);
    marks.end = data(:, 3);
    marks.subjects = data(:, 4:5);
    marks.approach = data(:, 6:7);
    marks.leave = data(:, 8:9);
    marks.chase = data(:, 10);
    marks.pred = data(:, 11:12);
    marks.prey = data(:, 13:14);
    marks.artifact = data(:, 15);
    marks.agresivness = data(:, 16);
%    marks.chase = marks.agresivness > 0;
%     
%     data = dlmread(['aux/' objname '.marks'], '\t', [1 0 500 14]);
%     marks.ids = data(:, 1);
%     marks.beg = data(:, 2);
%     marks.end = data(:, 3);
%     marks.subjects = data(:, 4:5);
%     marks.approach = data(:, 6:7);
%     marks.leave = data(:, 8:9);
%     marks.chase = data(:, 10);
%     marks.pred = data(:, 11:12);
%     marks.prey = data(:, 13:14);
%     marks.artifact = data(:, 15);
    %%
    obj = TrackLoad(['Res/' objname '.obj.mat']);
    obj.OutputToFile = false;
    %obj = SocialHierarchy(obj, true, arg);
    obj = SocialPredPreyModel(obj, arg);
    %%
    results = SocialBehaviorAnalysisScore(obj, marks);
    
    f = fieldnames(results);
    for i=1:length(f)
        PrintStrudel(f{i}, results.(f{i}))
    end
    %%
    fprintf('#   detection=%.1f%% (%d), false-alarm=%.1f%% (%d)\n', results.chase(1) / (results.chase(1) + results.chase(3)) * 100, results.chase(1), results.chase(2) / results.chase(5) * 100, results.chase(2));
    %%
    if o == 1
        total = results;
    else
        for i=1:length(f)
            total.(f{i}) = total.(f{i}) + results.(f{i});
        end
    end
end

f = fieldnames(results);
for i=1:length(f)
    PrintStrudel(['total' regexprep(f{i},'(\<[a-z])','${upper($1)}')], total.(f{i}))
end


f = fieldnames(arg);
for i=1:length(f)
    %fprintf('@%s=%f\n', f{i}, arg.(f{i}));
    PrintStrudel(f{i}, arg.(f{i}));
end
PrintStrudel('version', 1);

% [correct, false-positive, false-negative, count]

if 1==2
    %%
    [results, match] = SocialBehaviorAnalysisScore(curr, marks)
    
    f = fieldnames(results);
    for i=1:length(f)
        PrintStrudel(f{i}, results.(f{i}))
    end
    %%
    data = dlmread(['aux/' objname '.marks'], '\t', [1 0 500 15]);
    marks.ids = data(:, 1);
    marks.beg = data(:, 2);
    marks.end = data(:, 3);
    marks.subjects = data(:, 4:5);
    marks.approach = data(:, 6:7);
    marks.leave = data(:, 8:9);
    marks.chase = data(:, 10);
    marks.pred = data(:, 11:12);
    marks.prey = data(:, 13:14);
    marks.artifact = data(:, 15);
    marks.agresivness = data(:, 16);
    %marks.chase = marks.agresivness > 1;
    %
    [results, match] = SocialBehaviorAnalysisScore(obj, marks);
    
    fprintf('# follow: \n');
    f = fieldnames(results);
    for i=1:length(f)
        fprintf('# \t\t'); PrintStrudel(f{i}, results.(f{i}))
    end
%%
    marks.chase = marks.agresivness > 1;
    local = SocialChaseEscapeProperties(obj);
%    local.Contacts.Behaviors.ChaseEscape = any(local.Contacts.Behaviors.PostSpeed > 1) & any(local.Contacts.Behaviors.endInNest) & any(local.Contacts.Behaviors.PostSpeed > 5);
    %local.Contacts.Behaviors.ChaseEscape = mean(local.Contacts.Behaviors.PostSpeed) > 10;
    nConditions = any(local.Contacts.Behaviors.PostSpeed > arg.MinAgressiveSpeed) + any(local.Contacts.Behaviors.PostSpeed > arg.MinAgressiveDistance) + local.Contacts.Behaviors.EndInNest > arg.MinEndInNest + arg.MinAgressiveDuration;
    
    local.Contacts.Behaviors.ChaseEscape = nConditions > arg.MinAgressiveConditions;
    [results, match] = SocialBehaviorAnalysisScore(local, marks);
    
    fprintf('# agresivness: \n');
    f = fieldnames(results);
    for i=1:length(f)
        fprintf('# \t\t'); PrintStrudel(f{i}, results.(f{i}))
    end
    fprintf('# detection=%.1f%% (%d), false-alarm=%.1f%% (%d)\n', results.chase(1) / (results.chase(1) + results.chase(3)) * 100, results.chase(1), results.chase(2) / results.chase(5) * 100, results.chase(2));
end