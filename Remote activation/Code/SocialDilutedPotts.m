function obj = SocialDilutedPotts(obj)
obj = TrackLoad(obj);

MinOcc = [1 2 4 8 16 32 64];
obj.Analysis.Diluted = {};
idx = 1;
for mo = MinOcc
    me = TrainDilutedPottsModel(obj, 1:obj.nSubjects, obj.zones(:, obj.valid), mo);
    obj.Analysis.Diluted{idx}.Model = me;
    obj.Analysis.Diluted{idx}.MinOccurances = mo;
    idx = idx + 1;
end
TrackSave(obj);