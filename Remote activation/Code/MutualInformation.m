function I = MutualInformation(x, N)
[ndata, dim] = size(x);
mx = max(x);
mn = min(x);
d = mx - mn;
mx = mx + d/1000;
mn = mn - d/1000;
d = mx - mn;
k = ceil((x - repmat(mn, ndata, 1)) ./ repmat(d, ndata, 1) * N);
dimVec = ones(1, dim) * N;
jointP = zeros(dimVec);
P = zeros(N, dim);
for i=1:ndata
    for j=1:dim
        P(k(i, j), j) = P(k(i, j), j) + 1;
    end
    loc = (k(i, :) - 1) * N.^[0:(dim-1)]' + 1;
    jointP(loc) = jointP(loc) + 1;
end
P = P ./ repmat(sum(P, 1), N, 1);
jointP = jointP ./ sum(jointP(:));
I = 0;
if (dim ~= 2)
    error 'dim ~= 2'
end
for i=1:N
    for j=1:N
        I = I + jointP(i, j) * (flog2(jointP(i, j)) - flog2(P(i, 1) * P(j, 2)));
    end
end
