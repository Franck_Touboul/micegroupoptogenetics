options.beta = 2^-5;

SocialExperimentData
subjPerms = perms(1:GroupsData.nSubjects);
Vecs = {};
Orders = 3;
%Vecs2 = {};
id_ = 1:length(experiments);
id_(6) = 9;
id_(9) = 6;
stat = [];
for id=id_
    try
        %%
        obj = TrackLoad({id 2});
        %%
        fprintf('# - extracting weights... ');
        for order=Orders
            fprintf([num2str(order) '... ']);
            vecs = [];
            for i=1:size(subjPerms, 1)
                me = SocialGetRegPotts(obj, options.beta, order);
                vecs(i, :) = PottsVectorize_v2(obj, me, subjPerms(i, :));
            end
            Vecs{order}{id} = vecs;
            %            Vecs2{order}{id} = SocialPottsVector(obj);
        end
        fprintf(' done!\n');
    catch
    end
end
stat.vecs = Vecs;
%%
fprintf('# - computing probabilities for each model ');
Probs = {};
for order=Orders
    for id=1:length(Vecs{order})
        ProgressReport((order - 1)*length(Vecs{order}) + id, length(Vecs{order}) * obj.nSubjects)
        Probs{order}{id} = [];
        for i=1:size(Vecs{order}{id}, 1)
            p = exp(obj.Analysis.Potts.Model{order}.perms * Vecs{order}{id}(i, :)');
            Z = sum(p);
            p = p / Z;
            Probs{order}{id}(i, :) = p;
        end
    end
end
ProgressReport;
fprintf('\n');

%%
match = [];
d = {};
match.idx = {};
fprintf('# - computing Djs between groups ');
for order = Orders;
    probs = Probs{order};
    d{order} = zeros(length(probs));
    match(order).idx = zeros(length(probs));
    idx = 1;
    for i=1:length(probs)
        d{order}(i,i) = nan;
        for j=i+1:length(probs)
            ProgressReport((order - 1)*.5 * length(probs) *  (length(probs) + 1) + idx, .5 * length(probs) *  (length(probs) + 1) * obj.nSubjects)
            try
                D = zeros(size(probs{i}, 1), size(probs{j}, 1));
                for i1=1:size(probs{i}, 1)
                    for i2=1:size(probs{j}, 1)
                        D(i1, i2) = JensenShannonDivergence(probs{i}(i1, :), probs{j}(i2, :));
                    end
                end
                %D = corr(vecs{i}', vecs{j}');
                [d{order}(i, j), match(order).idx(i, j)] = min(D(:));
                match(order).idx(j, i) = match(order).idx(i, j);
                d{order}(j, i) = d{order}(i, j);
            catch
                match(order).idx(i, j) = nan;
                match(order).idx(j, i) = match(order).idx(i, j);
                
                d{order}(i, j) = nan;
                d{order}(j, i) = d{order}(i, j);
            end
            idx = idx + 1;
        end
    end
end
ProgressReport(1, 1);
ProgressReport;
stat.d = d;
fprintf('\n');
%%
clf
%remove = [6];
remove = [6 ];

for order = Orders;
    if length(Orders) > 1
        subplot(2,2,order);
    else
        subplot(3,1,1:2);
    end
    %imagesc(d{order})
    d = stat.d{order};
    groups = GroupsData.group;
    indices = GroupsData.index;
    seq = 1:size(d, 2);
    for i=1:length(remove)
        seq = seq(seq ~= remove(i));
    end
    d = d(seq, :);
    d = d(:, seq);
    groups = groups(seq);
    indices = indices(seq);
    didx = 1:length(seq);
    %MyImagesc(d, true, [], [.2 .4 .6 .79]);
    VecImage(d, [0 1], makeColorMap([1 1 1], ones(1, 3)*.3, [0 0 0], 256));
    %%
    concise = true(1, size(d, 1));
    stat.significant = [];
    for k=1:size(d, 1)
        %%
        i = indices(k);
        myg = groups(k);
        same = d(k, didx(groups == myg));
        same = same(~isnan(same));
        other = d(k, didx(groups ~= myg));
        other = other(~isnan(other));
        %concise(i) = quantile(d(i, GroupsData.index(GroupsData.group ~= myg)), .5) >= quantile(d(i, GroupsData.index(GroupsData.group == myg)), .5);
        [concise(i), pval(i)] = kstest2(same, other);
        %%
        curr = {};
        for g=1:GroupsData.nGroups
            curr{g} = [];
        end
        for j=1:size(d, 1)
            if ~isnan(d(k, j))
                curr{groups(j)} = [curr{groups(j)}, d(k, j)];
            end
        end
        subplot(3,1,3);
        for g=1:GroupsData.nGroups
            plot(i, curr{g}, '.', 'color', GroupsData.Colors(g, :)); hon;
            errorbar(i, mean(curr{g}), std(curr{g}), 'x', 'color', GroupsData.Colors(g, :));
        end
        
        %            stat.significant(i) = kstest2 (curr{1}, curr{2});
        stat.significant(i) = ttest2(curr{1}, curr{2});
    end
    set(gca, 'Xtick', find(stat.significant));
    subplot(3,1,3); hoff;
    subplot(3,1,1:2);
    
    %%
    prev = 0.5;
    for i=1:GroupsData.nGroups-1
        idx = find(groups(1:end-1) == i & groups(2:end) == i+1) + .5;
        vert_line(idx, 'color', 'k');
        horiz_line(idx, 'color', 'k');
        %text(length(GroupsData.Experiments)+1, prev+.1, [' ' GroupsData.Titles{i}], 'VerticalAlignment', 'top', 'color', 'k', 'fontweight', 'bold', 'HorizontalAlignment', 'left');
        prev = idx;
    end
    %text(length(GroupsData.Experiments)+1, prev+.1, [' ' GroupsData.Titles{i}], 'VerticalAlignment', 'top', 'color', 'k', 'fontweight', 'bold', 'HorizontalAlignment', 'left');
    set(gca, 'YAxisLocation', 'right', 'YTick', find(diff([0 groups])), 'YTickLabel', GroupsData.Titles)
    set(gca, 'XTick', []);
    for i=find(concise)
        text(i, size(d, 1) + 1, '*', 'HorizontalAlignment', 'center', 'FontWeight', 'bold')
    end
    for i=find(~concise)
        %text(i, size(d, 1) + 1, num2str(i), 'HorizontalAlignment', 'center')
    end
    title(['Order ' num2str(order)]);
    axis square
    
end
%saveFigure(['Graphs/SocialRegPottsDistance.beta=' num2str(options.beta)]);

return;
%%
for order = 1:4;
    subplot(2,2,order);
    vecs = Vecs{order};
    d = zeros(length(vecs));
    for i=1:length(vecs)
        %vecs{i} = vecs{i} ./ repmat(sqrt(sum(vecs{i}.^2, 2)), 1, 640);
        %vecs{i} = vecs{i} - repmat(median(vecs{i}, 2), 1, 640);
    end
    
    for i=1:length(vecs)
        d(i,i) = nan;
        for j=i+1:length(vecs)
            try
                D = mydist2(vecs{i}, vecs{j});
                %D = corr(vecs{i}', vecs{j}');
                d(i, j) = min(D(:));
                d(j, i) = d(i, j);
            catch
                d(i, j) = nan;
                d(j, i) = d(i, j);
            end
        end
    end
    %
    %d = d(1:18,1:18)
    MyImagesc(d)
    prev = 0.5;
    for i=1:GroupsData.nGroups-1
        idx = find(GroupsData.group(1:end-1) == i & GroupsData.group(2:end) == i+1) + .5;
        vert_line(idx, 'color', 'k');
        horiz_line(idx, 'color', 'k');
        %text(length(GroupsData.Experiments)+1, prev+.1, [' ' GroupsData.Titles{i}], 'VerticalAlignment', 'top', 'color', 'k', 'fontweight', 'bold', 'HorizontalAlignment', 'left');
        prev = idx;
    end
    %text(length(GroupsData.Experiments)+1, prev+.1, [' ' GroupsData.Titles{i}], 'VerticalAlignment', 'top', 'color', 'k', 'fontweight', 'bold', 'HorizontalAlignment', 'left');
    set(gca, 'YAxisLocation', 'right', 'YTick', find(diff([0 GroupsData.group])), 'YTickLabel', GroupsData.Titles)
    title(['Order ' num2str(order)]);
end
%%
% D = d;
% i=2;
% G = GroupsData.group;
% while i<size(D, 2)
%     seq = 1:size(D, 2);
%     if isnan(D(i, 1))
%         D = D(seq(seq ~= i), seq(seq ~= i));
%         G = G(seq(seq ~= i));
%     else
%         i = i + 1;
%     end
% end
% m = mdscale(D, 2);
% for g=unique(G)
%     plot(m(:, 1), m(:, 2), '.');
% end