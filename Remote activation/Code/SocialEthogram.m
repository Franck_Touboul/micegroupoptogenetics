function [obj, img] = SocialEthogram(obj)
obj = TrackLoad(obj);
obj = TrackEnsure(obj, {'Interactions'});
etho = nan(obj.nSubjects, obj.nFrames);
obj = SoftZoneCatego(obj);
%%
seq = 1:obj.ROI.nCategories;
etho(obj.category == seq(strcmp(obj.ROI.CategoryNames, 'Shelter'))) = 1;
etho(obj.category == seq(strcmp(obj.ROI.CategoryNames, 'Food'))) = 2;
etho(obj.category == seq(strcmp(obj.ROI.CategoryNames, 'Water'))) = 3;
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        %%
        previd = 1;
        events  = obj.Interactions.PredPrey.events{m1, m2};
        ievents = obj.Interactions.PredPrey.events{m2, m1};
        for id = 1:length(events);
            e = events{id};
            map = false(1, obj.nFrames); map(e.BegFrame:e.EndFrame) = true;
            imap = false(1, obj.nFrames);
            matched = false;
            for k=previd:length(ievents)
                ie = ievents{k};
                imap(ie.BegFrame:ie.EndFrame) = true;
                if any(map & imap)
                    previd = k;
                    matched = true;
                    break;
                end
            end
            if ~matched
                continue;
            end
            %%
            sf = min(e.BegFrame, ie.BegFrame);
            ef = max(e.EndFrame, ie.EndFrame);
            %%
            if (any(e.data == 5) && any(ie.data == 6)) || (any(e.data == 6) && any(ie.data == 5))
                etho(m1, sf:ef) = 5;
                etho(m2, sf:ef) = 5;
            else
                etho(m1, sf:ef) = 4;
                etho(m2, sf:ef) = 4;
            end
        end
    end 
end

%%
cmap = [0.2070    0.6953    0.3398;
    0.9688    0.9023    0.3242;
    0         0.6914    0.8945;
    0.9688    0.9023    0.3242;
    0.8594    0.2852    0.3477;
    ];

cmap = [.65 .65 .65;
    .49 .70 .10;
    0         0.3914    0.8945;
    1 0.6 0;
    1 0 0;
    ];

img = ShowEthogram(etho, cmap);
% if ~isdesktop
try
    %%
    name = ['Graphs/Ethograms/' obj.FilePrefix '.Ethogram'];
    %print(gcf, [name '.png'], '-dpng');
    f = getframe();
    imwrite(f.cdata, [name '.png'])
catch
%    name = [obj.OutputPath obj.FilePrefix '.Ethogram'];
    name = ['Graphs/Ethograms/' obj.FilePrefix '.Ethogram'];
    f = imcapture;
    imwrite(f.cdata, [name '.png'])
end
% end