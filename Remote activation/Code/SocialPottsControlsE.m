function SocialPottsControlsE(obj)
obj = TrackLoad(obj);
%%
Options.EvenOddTimescales = [1];
obj.OutputToFile = false;
obj.OutputInOldFormat = false;
obj.Analysis.Potts.nIters = [2000 20000];
obj.Analysis.Potts.MinNumberOfIters = [500 1000];
obj.Analysis.Potts.Confidence = 0.05;
%%
oidx = 1;
objs = {};
%% even-odd
try
    for ts = Options.EvenOddTimescales
        %%
        frames = ts * obj.FrameRate;
        map = rand(1, obj.nFrames) >= .5;
        rand1 = map;
        rand2 = ~rand1;
        %%
        Types = {'rand1', 'rand2'};
        for i=1:length(Types);
            map = eval(Types{i});
            curr = obj;
            curr.zones = obj.zones(:, map);
            curr.valid = obj.valid(:, map);
            curr.nFrames = sum(map);
            %curr.me = TrainPottsModel(curr, 1:obj.nSubjects);
            curr = SocialPotts(curr);
            objs{oidx}.(Types{i}) = curr;
        end
        oidx = oidx + 1;
    end
catch me
    warning(me.message);
end
%%
save('SocialPottsControlsE', 'objs', '-v7.3');
