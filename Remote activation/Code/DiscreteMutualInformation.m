function mi = DiscreteMutualInformation(obj)

mi = zeros(obj.nSubjects);
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1 == m2
            p1 = histc(obj.zones(m1, :), unique(obj.zones(~isnan(obj.zones))));
            p1 = p1 / sum(p1);
            mi(m1,m1) = -p1 * flog2(p1)';
        else
            if m1<m2
                curr = obj;
                curr.nSubjects = 2;
                curr.zones = obj.zones([m1 m2], :);
                [~, indepProbs, jointProbs] = SocialComputePatternProbs(curr, false);
                cmi = jointProbs * (flog2(jointProbs) - flog2(indepProbs))'; 
                mi(m1, m2) = cmi;
                mi(m2, m1) = cmi;
            end
        end
    end
end
