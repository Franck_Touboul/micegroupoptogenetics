function [Area, meanDist, minDist] = SocialGeometry(obj)
%%
% allOut = all(~obj.sheltered) & obj.valid;
% idx = 1;
% area = zeros(1, sum(allOut));
% for i=find(allOut)
%     area(idx) = polyarea(obj.x(:, i), obj.y(:, i));
%     idx = idx + 1;
% end
% Area = obj.valid * 0;
% Area(allOut) = area;
% plot(Area);
% return;
Area = [];
%%
seq = 1:obj.nSubjects;
meanDist = zeros(1, obj.nSubjects);
minDist = zeros(1, obj.nSubjects);
for s=1:obj.nSubjects
    d = nan(size(obj.x));
    for s2=seq(seq ~= s)
        v = ~obj.sheltered(s, :) & ~obj.sheltered(s2, :);
        d(v) = sqrt((obj.x(s, v) - obj.x(s2, v)) .^ 2 + (obj.y(s, v) - obj.y(s2, v)) .^ 2);
    end
    %d = sqrt((repmat(obj.x(s, obj.valid), obj.nSubjects-1,1) - obj.x(seq(seq ~= s), obj.valid)) .^ 2 + (repmat(obj.y(s, obj.valid), obj.nSubjects-1,1) - obj.y(seq(seq ~= s), obj.valid)) .^ 2);
    meanDist(s) = validmean(validmean(d, 1));
    minDist(s) = validmean(min(d, [], 1));
end