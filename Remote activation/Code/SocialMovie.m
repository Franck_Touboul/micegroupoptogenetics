function obj = SocialMovie(obj, m1, m2)
obj = TrackLoad(obj);
obj = SocialFindInteractions(obj);
obj.TimePadding = obj.FrameRate;
%%
dt = obj.dt;
%%
%for id = 6
try
    figure('visible', 'off');
    events  = obj.Interactions.PredPrey.events{m1, m2};
    ievents = obj.Interactions.PredPrey.events{m2, m1};
    for id = 1:length(events);
        e = events{id};
        ie = ievents{id};
        
        e.states  = unique(e.data);
        e.bounds  = [];
        for i=1:length(e.states)
            e.bounds(i) = find(e.data == e.states(i), 1);
        end
        e.bounds(end+1) = length(e.data);
        
        ie.states = unique(ie.data);
        ie.bounds = [];
        for i=1:length(ie.states)
            ie.bounds(i) = find(ie.data == ie.states(i), 1);
        end
        ie.bounds(end+1) = length(ie.data);
        
        %%
        clf;
        currPadding = obj.TimePadding;
        sf = min(events{id}.BegFrame, ievents{id}.BegFrame) - obj.TimePadding;
        ef = max(events{id}.EndFrame, ievents{id}.EndFrame) + obj.TimePadding;
        
        %%
        set(gcf, 'Color', [1 1 1] * .6)
        
        subplot(8,1,1);
        chase1 = obj.speed(m1, sf:ef);
        chase2 = obj.speed(m2, sf:ef);
        chase1(~isfinite(chase1)) = 0;
        chase2(~isfinite(chase2)) = 0;
        plot(sf:ef, (chase1), 'Color', obj.Colors.Centers(m1, :));
        hold on;
        plot(sf:ef, (chase2), ':', 'Color', obj.Colors.Centers(m2, :));
        hold off;
        graph_axis = axis;
        line([e.BegFrame, e.BegFrame], [graph_axis(3) graph_axis(4)], 'linestyle', '--', 'Color', 'k');
        line([e.EndFrame, e.EndFrame], [graph_axis(3) graph_axis(4)], 'linestyle', '--', 'Color', 'k');
        for s = unique(e.data)
            beg = e.BegFrame+find(e.data == s, 1);
            line([beg, beg], ...
                [graph_axis(3) graph_axis(4)], 'linestyle', ':', 'Color', 'k');
            text(beg, graph_axis(4), [' ' obj.Interactions.PredPrey.model.names{s}], 'HorizontalAlignment', 'Left', 'VerticalAlignment', 'Top', 'Rotation', -90)
        end
        %axis([sf, ef, graph_axis(3), graph_axis(4)]);
        axis([sf, ef, graph_axis(3), graph_axis(4)]);
        rowTitle(8,1,1, ['Mouse ' num2str(m1)], 'Color', obj.Colors.Centers(m1, :));
        set(gca, 'XTickLabel', sec2time(get(gca, 'XTick') * dt))
        lineh1 = line([sf, sf], [graph_axis(3), graph_axis(4)], 'color', 'k');
        %%
        subplot(8,1,2);
        chase1 = obj.speed(m1, sf:ef);
        chase2 = obj.speed(m2, sf:ef);
        chase1(~isfinite(chase1)) = 0;
        chase2(~isfinite(chase2)) = 0;
        plot(sf:ef, (chase1), ':', 'Color', obj.Colors.Centers(m1, :));
        hold on;
        plot(sf:ef, (chase2), 'Color', obj.Colors.Centers(m2, :));
        hold off;
        graph_axis = axis;
        line([ie.BegFrame, ie.BegFrame], [graph_axis(3) graph_axis(4)], 'linestyle', '--', 'Color', 'k');
        line([ie.EndFrame, ie.EndFrame], [graph_axis(3) graph_axis(4)], 'linestyle', '--', 'Color', 'k');
        for s = unique(ie.data)
            beg = ie.BegFrame+find(ie.data == s, 1);
            line([beg, beg], ...
                [graph_axis(3) graph_axis(4)], 'linestyle', ':', 'Color', 'k');
            text(beg, graph_axis(4), [' ' obj.Interactions.PredPrey.model.names{s}], 'HorizontalAlignment', 'Left', 'VerticalAlignment', 'Top', 'Rotation', -90)
        end
        %axis([sf, ef, graph_axis(3), graph_axis(4)]);
        axis([sf, ef, graph_axis(3), graph_axis(4)]);
        rowTitle(8,1,2, ['Mouse ' num2str(m2)], 'Color', obj.Colors.Centers(m2, :));
        set(gca, 'XTickLabel', sec2time(get(gca, 'XTick') * dt))
        lineh2 = line([sf, sf], [graph_axis(3), graph_axis(4)], 'color', 'k');
        %%
        prevStrLen = 0;
        RePrintf('# epoch no. %d/%d\n', id, length(events));
        %%
        filename = sprintf([obj.OutputPath '/%03d[mice %d,%d](%s-%s)'], id, m1, m2, sec2time(sf*dt, true), sec2time(ef*dt, true));
        fprintf('# - creating file: %s\n', filename);
        if verLessThan('matlab', '7.12')
            aviobj = avifile(filename, 'fps', obj.FrameRate);
        else
            aviobj = VideoWriter(filename);
            aviobj.FrameRate = obj.FrameRate;
            open(aviobj);
        end
            
        %aviobj.compression = 'wmv3';
        %%
        RePrintf('# - frame no. ');
        id1 = 1;
        id2 = 1;
        clear('F');
        for i=0:ef-sf
            prevStrLen = RePrintf(prevStrLen, '%3d (%3d-%3d)', sf+i, sf, ef);
            frame = myMMReader(obj.VideoFile, sf+i);
            
            %%
            subplot(8,1,3:8);
            imagesc(frame);
            a = axis;
            
            hold on;
            plot(obj.x(m1, sf:sf+i), obj.y(m1, sf:sf+i), '.-', 'Color', obj.Colors.Centers(m1, :));
            plot(obj.x(m2, sf:sf+i), obj.y(m2, sf:sf+i), '.-', 'Color', obj.Colors.Centers(m2, :));
            hold off;
            %%
            if id1 <= length(e.states) && e.bounds(id1) <= i - currPadding + 1&& e.bounds(id1 + 1) >= i - currPadding + 1
                text(a(1) + obj.VideoWidth / 14, a(3) + obj.VideoHeight / 14, obj.Interactions.PredPrey.model.names{e.states(id1)}, 'Color', obj.Colors.Centers(m1, :), 'FontSize', 20);
            else
                if id1 <= length(e.states) &&  e.bounds(id1 + 1) < i - currPadding + 1
                    id1 = id1 + 1;
                end
            end
            %%
            if id2 <= length(ie.states) && ie.bounds(id2) <= i - currPadding + 1&& ie.bounds(id2 + 1) >= i - currPadding + 1
                text(a(1) + obj.VideoWidth / 14, a(3) + 2 * obj.VideoHeight / 14, obj.Interactions.PredPrey.model.names{ie.states(id2)}, 'Color', obj.Colors.Centers(m2, :), 'FontSize', 20);
            else
                if id2 <= length(ie.states) &&  ie.bounds(id2 + 1) < i - currPadding + 1
                    id2 = id2 + 1;
                end
            end
            %%
            subplot(8,1,1);
            if exist('lineh1'); delete(lineh1); end
            lineh1 = line([sf + i, sf + i], [graph_axis(3), graph_axis(4)], 'color', 'k');
            %
            subplot(8,1,2);
            if exist('lineh2'); delete(lineh2); end
            lineh2 = line([sf + i, sf + i], [graph_axis(3), graph_axis(4)], 'color', 'k');
            
            drawnow;
            %F(i+1) = getframe(gcf);
            F = imcapture(gcf);
            if verLessThan('matlab', '7.12')
                aviobj = addframe(aviobj,F);
            else
                writeVideo(aviobj, F);
            end
            
        end
        %movie2avi(F, filename, 'compression', 'wmv3');
        prevStrLen = RePrintf(prevStrLen, '(%3d/%3d)', sf+i, sf+ef);
        fprintf('\n');
        if verLessThan('matlab', '7.12')
            aviobj = close(aviobj);
        else
            close(aviobj);
        end
        return;
    end
catch me
    try
        aviobj = close(aviobj);
    end
    rethrow(me);
end