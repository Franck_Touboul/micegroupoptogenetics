function obj = SocialCircadianPotts(obj)
obj = TrackLoad(obj);
obj.CircadianDuration = 1 * 3600; % [secs]
obj.CircadianMinimalDuration = obj.CircadianDuration * 3/4; % [secs]
%%
obj.CreationDate = datevec(GetFileCreationDate(obj.VideoFile));
creationTimeOfDaySec = obj.CreationDate(4) * 3600 + obj.CreationDate(5) * 60 + obj.CreationDate(6);
dt = 1 / obj.FrameRate;
timeOfDaySec = creationTimeOfDaySec - obj.nFrames / obj.FrameRate + dt:dt:creationTimeOfDaySec;
timeOfDaySec(~obj.valid) = inf;
%%
edges = 0:obj.CircadianDuration:24*3600;
[h, bins] = histc(timeOfDaySec, edges);
validBins_ = unique(bins(bins > 0));
validBins_ = validBins_(h(validBins_) > obj.CircadianMinimalDuration * obj.FrameRate);

obj.Analysis.Circadian.Potts.Entropy = nan(obj.nSubjects-1, length(edges)-1);
obj.Analysis.Circadian.Potts.Ik = nan(obj.nSubjects-1, length(edges)-1);
obj.Analysis.Circadian.Potts.Model = cell(1, obj.nSubjects);
for currBin = validBins_
    sub = bins == currBin;
    currData = obj.zones(:, sub);
    currObj = obj; currObj.OutputToFile = false; currObj.OutputInOldFormat = false;
    currObj = SocialPotts(currObj, currData);
    obj.Analysis.Circadian.Potts.Entropy(:, currBin) = currObj.Analysis.Potts.Entropy';
    obj.Analysis.Circadian.Potts.Ik(:, currBin) = currObj.Analysis.Potts.Ik';
    for s=1:obj.nSubjects
        if currBin == validBins_(1)
            obj.Analysis.Circadian.Potts.Model{s}.labels = currObj.Analysis.Potts.Model.labels;
            obj.Analysis.Circadian.Potts.Model{s}.weights = ...
                nan(length(currObj.Analysis.Potts.Model{s}.weights), length(edges)-1);
        end
        obj.Analysis.Circadian.Potts.Model{s}.weights(:, currBin) = currObj.Analysis.Potts.Model.weights';
    end
end
if obj.OutputToFile
    fprintf('# - saving data\n');
    TrackSave(obj);
end
