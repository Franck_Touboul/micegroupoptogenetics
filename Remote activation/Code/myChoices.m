function res = myChoices(dim, max_value)
res = [];
for i=1:max_value
    for j=1:max_value
        for k=j+1:max_value
            res = [res; i, j, k];
        end
    end
end

function res = perm(res, dim, max_value)
for i=1:max_value
    res = [res, repmat(i, size(res, 1), 1)];
end