%% PottsCompareScore
%data = ReadStrudels('log/PottsCompare.o*');

%%
clf
Types = {'GIS', 'GD', 'NGD', 'GISnNGD'};
marks = {'-', ':', '-.', '--'};
cmap = MyCategoricalColormap;
tk_ = [0.01 0.1 0.5 1]; tk_ = tk_(end:-1:1);
iters = 50;
entries = {};
index = 1;
for order = 2:4
c = 1;
    
    for tid = 1:length(Types);
        if ~any(strcmp(Types{tid}, 'GIS')) && ~any(strcmp(Types{tid}, 'GISnNGD'))
            c = 1;
            for tk=tk_
                idx = strcmp({data.type}, Types{tid}) & tk == [data.tk] & order == [data.order];
                subplot(3,2,order*2-3);
                plot(data(idx).halfKL(1:iters), [marks{tid}], 'color', cmap(c, :), 'LineWidth', 2); hon;
                plot([iters iters+10], [data(idx).halfKL(iters - 1) data(idx).halfKL(end)], ':x', 'color', cmap(c, :), 'LineWidth', 2,'HandleVisibility','off'); hon;
                subplot(3,2,order*2-2);
                plot(data(idx).convergence*100, [marks{tid}], 'color', cmap(c, :), 'LineWidth', 2); hon;
                xlabel('iteration');
                ylabel('5% convergence [%]');
                c = c + 1;
            entries{index} = [(Types{tid}) ' (t_k=' num2str(tk) ')' ];
            index = index + 1;
            end
        else
            c = 1;
            idx = strcmp({data.type}, Types{tid}) & order == [data.order];
            subplot(3,2,order*2-3);
            plot(data(idx).halfKL(1:iters), [marks{tid}], 'color', 'k', 'LineWidth', 2); hon;
                plot([iters iters+10], [data(idx).halfKL(iters - 1) data(idx).halfKL(end)], ':x', 'color', 'k', 'LineWidth', 2,'HandleVisibility','off'); hon;
            xlabel('iteration');
            ylabel('half KL');
            subplot(3,2,order*2-2);
            plot(data(idx).convergence*100, [marks{tid}], 'color', 'k', 'LineWidth', 2); hon;
            xlabel('iteration');
            ylabel('5% convergence [%]');
            c = c + 1;
            entries{index} = [(Types{tid})];
            index = index + 1;
        end
    end
    
    subplot(3,2,2 * order - 3);
    tick = get(gca, 'XTick');
    labels = {};
    for i=1:length(tick)
        labels{i} = num2str(tick(i));
    end
    labels{end} = num2str(length(data(1).halfKL));
    
    set(gca, 'XTickLabel', labels);
    %if order == 4
        legend(entries, 'location', 'eastoutside');
        legend boxoff
    %end

end
hoff


return;
%%
clf
iters = 50;
tk_ = unique([data.tk]);
tk_ = tk_(tk_ <= 1);
type_ = unique({data.type});
types = {data.type};
marks = {'o', '^', 's', 'p'};
marks = {':', '', '--', ''};
cmap = [MyCategoricalColormap; 0 0 0];
for o=2:4
    entries = {};
    idx = [data.order] == o;
    index = 1;
    for tidx = 1:length(type_)
        for tk=tk_(:)'
            i = find([data.order] == o & [data.tk] == tk & strcmp(type_{tidx}, {data.type}));
            c = find(tk == tk_);
            if isempty(i)
                subplot(3,2,2 * (o-1) - 1);
                %plot(inf, inf, 'color', cmap(c, :), 'LineWidth', 2); hon;
            else
                i = i(1);
                t = find(strcmp(data(i).type, type_));
                try
                    subplot(3,2,2 * (o-1) - 1);
                    plot(data(i).halfKL(1:iters), [marks{tidx}], 'color', cmap(c, :), 'LineWidth', 2); hon;
                    %plot(iters + 1, data(i).halfKL(end), ['-' marks{o}], 'color', cmap(c, :), 'LineWidth', 2,'HandleVisibility','off'); hon;
                    plot([iters iters + 1], [data(i).halfKL(iters) data(i).halfKL(end)], ['--' 'x'], 'color', cmap(c, :), 'LineWidth', 2,'HandleVisibility','off'); hon;
                    %                 enditer = find(abs(diff([0 data(i).halfKL])) ./ data(i).halfKL < 0.001, 1);
                    %                 a = axis();
                    %                 plot([enditer enditer], [a(3) a(4)], ['--' 'x'], 'color', cmap(c, :), 'LineWidth', 2,'HandleVisibility','off');
                    xlabel('iteration');
                    ylabel('half KL');
                    subplot(3,2,2 * (o-1));
                    plot(data(i).convergence*100, [marks{tidx}], 'color', cmap(c, :), 'LineWidth', 2); hon;
                    xlabel('iteration');
                    ylabel('5% convergence [%]');
                catch
                    plot(inf, inf, 'color', cmap(c, :), 'LineWidth', 2); hon;
                end
                entries{index} = [upper(type_{tidx}) ' (t_k=' num2str(tk) ')' ];
                index = index + 1;
            end
        end
    end
    %%
    subplot(3,2,2 * (o-1) - 1);
    xaxis(0, iters + 1);
    hoff
    tick = get(gca, 'XTick');
    labels = {};
    for i=1:length(tick)
        labels{i} = num2str(tick(i));
    end
    labels{end + 1} = num2str(length(data(1).halfKL));
    set(gca, 'XTick', [tick, iters+1]);
    set(gca, 'XTickLabel', labels);
    
%     entries = {};
%     for i=1:length(tk_)
%         if tk_(i) == 0
%             entries{i} = ['GIS'];
%         else
%             entries{i} = ['GD (t_k=' num2str(tk_(i)) ')'];
%         end
%     end
if o == 4
    legend(entries);
    legend boxoff
end
end
subplot(3,2,1); title('half D_{KL}');
subplot(3,2,2); title('Convergence to 5%');

TieAxis(3,2,1:2:6);