function nbins = ScottChoice(data)
nbins = ceil(3.5 * std(data) / length(data).^(1/3));
