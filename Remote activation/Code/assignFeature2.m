function [features, labels] = assignFeature2(data, range, nPerms, use_sparse)
if nargin < 4
    use_sparse = false;
end

dim = 0;
for i=1:length(nPerms)
    nVals = range^length(nPerms{i});
    dim = dim + nVals;
end
% if use_sparse
%     features = sparse(size(data, 1), double(dim));
% else
%     features = logical(size(data, 1), double(dim));
% end
features = false(size(data, 1), double(dim));

offset = 1;
labels = cell(1, dim);
seq = 1:size(data, 1);
for i=1:length(nPerms)
    nVals = range^length(nPerms{i});
    baseVector = range.^(length(nPerms{i})-1:-1:0);
    v = (data(:, nPerms{i}) - 1) * baseVector'; 
    features(sub2ind(size(features), seq(~isnan(v)), offset + v(~isnan(v))')) = true;
    %features(~isnan(v), offset + v(~isnan(v))) = true;
    offset = offset + nVals;
end


if use_sparse
    features = sparse(features);
end
