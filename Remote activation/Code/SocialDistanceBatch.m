SocialExperimentData
%%
index = 1;
ranks = [];
zones = [];
weights = [];
groups = [];
ids = [];
%%
for id=1:length(experiments)
    currGroup = GroupsData.group(find(GroupsData.index == id));
%     if currGroup == 1
%         continue;
%     end
    
    curr.zones = zeros(10, GroupsData.nSubjects);
    curr.PostPredPrey = zeros(GroupsData.nSubjects);
    curr.weights = zeros(GroupsData.nSubjects, 100);
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('# -> %s\n', prefix);
        obj = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid', 'Hierarchy', 'Analysis'});
        if day == 1
            curr.weights = SocialPottsVector(obj, SocialGetRegPotts(obj, 2^-7, 3));
            rank = obj.Hierarchy.Group.AggressiveChase.rank;
            rank = max(rank) - rank + 1;
           % rank(rank > 3) = 3;
        else
            curr.weights = curr.weights + SocialPottsVector(obj, SocialGetRegPotts(obj, 2^-7, 3));
        end            
        curr.zones = curr.zones + histc(obj.zones', 1:obj.ROI.nZones);
    end
    ranks = [ranks, rank];
    zones = [zones, curr.zones];
    groups = [groups, ones(1, obj.nSubjects) * GroupsData.group(find(GroupsData.index == id))];
    weights = [weights, curr.weights'];
    ids = [ids,  ones(1, obj.nSubjects) * id];
end
%%

% g = 1;
% w = weights; % ./ repmat(sqrt(sum(weights.^2)), size(weights, 1), 1);
% nw = [];
% for id=unique(ids)
%     idx = groups == g & ids == id;
%     mw = mean(mean(w(:, idx)));
%     vw = std(mean(w(:, idx)));
%     w(:, idx) = (w(:, idx) - mw) ./ vw;
%     for r=1:obj.nSubjects
%         idx = groups == g & ranks == r & ids == id;
%         if any(idx)
%             plot(id, mean(w(:, idx)), 'o', 'MarkerFaceColor', cmap(r, :), 'MarkerEdgeColor', 'none'); hon;
%         end
%     end
% end
% xaxis(0, 11);
% hoff;
%%
clf
w = weights; % ./ repmat(sqrt(sum(weights.^2)), size(weights, 1), 1);
cmap = MyCategoricalColormap;
g = 1;
for i=1:obj.nSubjects
    subplot(2, 1, 1);
    entries{i} = num2str(i);
    currw = w(:, groups == g & ranks == i);
%    plot(currw, 'color', cmap(i, :)); hon;
    plot(mean(currw,1), 'color', cmap(i, :)); hon;
end
hoff;
subplot(2, 1, 2);
entries = {};
for i=1:obj.nSubjects
    entries{i} = num2str(i);
    plot(1, 'color', cmap(i, :));
    hon;
end
legend(entries);
%TieAxis(obj.nSubjects, 1, 1:obj.nSubjects);
hoff;
%% embedded distances
for i=1:4
    figure(i);
    clf;
end
cmap = MyCategoricalColormap;
zones = zones ./ repmat(sum(zones), size(zones, 1), 1);
data = weights;
%
figure(1);
plot(data); 
ax = axis;
clf;
entries = {};
for i=1:obj.nSubjects
    for g=1:GroupsData.nGroups
        subplot(2, GroupsData.nGroups, g);
        plot(inf, inf, 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', 5);
        hon;
    end
    entries{i} = num2str(i);
end
for g=1:GroupsData.nGroups
%    SquareSubplpot(GroupsData.nGroups, g);
        subplot(2, GroupsData.nGroups, g);
    
    legend(entries);
    legend boxoff;
end
%
entries = {};
covmat = cov(data');
stat.Y1 = [];
for g=1:GroupsData.nGroups
    %%
    %d = pdist2(data(:, groups == g)', data(:, groups == g)', 'correlation');
    % d = mydist2(data(:, groups == g)', data(:, groups == g)');
    dg = data(:, groups == g)';
    dg = dg(:, ~all(~dg));
    d = squareform(pdist(dg, 'euclidean'));
    d(sub2ind(size(d), 1:size(d,1), 1:size(d,1))) = 0;
    d(isnan(d)) = validmean(d(:));
    
    %d = MyMahalanobis(data(:, groups == g)', data(:, groups == g)', var(data'));
    %imagesc(d);
    Y1 = mdscale(d,1);
    [coeff, score, latent] = princomp(dg);
    Ypca = [coeff(1, :) * dg'; coeff(2, :) * dg']';
    
    Y = mdscale(d,2);
    rank = ranks(groups == g);
    c = [];
    chosenid = ids(groups == g);
    chosenid = chosenid(1);
    for i=1:obj.nSubjects
        figure(1);
        subplot(2, GroupsData.nGroups, g);
        plot(Y(rank == i, 1), Y(rank == i, 2), 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', 10); hon;
        
        figure(5);
        subplot(2, GroupsData.nGroups, g);
        plot(Ypca(rank == i, 1), Ypca(rank == i, 2), 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', 10); hon;
%        plot3(Y(rank == i, 1), Y(rank == i, 2), Y(rank == i, 3), 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none'); hon;


    if any(rank == i)
        stat.Y1{i} = Y1(rank == i, 1)';
        %MyCandlestick();
        %plot(Y1(rank == i, 1)', 0, 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', 10); hon;
    end

    figure(2);
        %SquareSubplpot(GroupsData.nGroups, g);
        subplot(4,1,g);
%%        ldata = data(:, groups == g & ids == chosenid & ranks == i);
        ldata = data(:, groups == g & ranks == i);
        %plot(ldata, '-', 'Color', cmap(i, :)); hon;
        SocialPottsVectorPlot(obj, ldata, cmap(i, :), i); hon;
        %
        ldata = data(:, groups == g);
        ldata = ldata(:, rank == i);
        for k=1:size(ldata, 2)
            c((i-1)  * (obj.ROI.nZones + 1) + [1:obj.ROI.nZones], (k-1) * (obj.ROI.nZones + 1) + [1:obj.ROI.nZones]) = reshape(ldata(:, k), obj.ROI.nZones, obj.ROI.nZones);
        end
    end
    c(c == 0) = nan;
    figure(3);
    SquareSubplpot(GroupsData.nGroups, g);
    imagesc(c);
    figure(1);
    title(GroupsData.Titles{g});
    figure(2);
    title(GroupsData.Titles{g});
    
    figure(4);
    ldata = data(:, groups == g);
    if g == 1
        subplot(2,1,1);
        plot(inf, inf, 'Color', cmap(1,:)); hon;
        plot(inf, inf, 'Color', cmap(3,:));
    elseif g==2
        subplot(2,1,1);
        legend(GroupsData.Titles{1:2});
        legend boxoff
    elseif g==3
        subplot(2,1,2);
        plot(inf, inf, 'Color', cmap(1,:)); hon;
        plot(inf, inf, 'Color', cmap(3,:));
        
    elseif g==4
        subplot(2,1,2);
        legend(GroupsData.Titles{3:4});
        legend boxoff
    end
    axis(ax);
    SocialPottsVectorPlot(obj, (ldata), cmap(2*mod(g, 2)+1, :), mod(g, 2)+1); hon;
    figure(1);
    subplot(2, GroupsData.nGroups, GroupsData.nGroups + g);
    MyCandlestick(stat.Y1);
    
    str = '';
    
    if kstest2(stat.Y1{1}, stat.Y1{2}); str = [str, '(1,2)* ']; end
    if kstest2(stat.Y1{1}, stat.Y1{3}); str = [str, '(1,3)* ']; end
    if kstest2(stat.Y1{2}, stat.Y1{3}); str = [str, '(2,3)* ']; end
    title(str);
end
%%
if 1==2
%%
    for i=1:4
        figure(i);
        saveFigure(['Res/SocialDistanceBatch.' num2str(i)])
    end

end
%% zones distance
clf
obj
for g=1:GroupsData.nGroups
    %%
    %d = pdist2(data(:, groups == g)', data(:, groups == g)', 'correlation');
    d = pdist2(zones(:, groups == g)', zones(:, groups == g)');
    %d = MyMahalanobis(zones(:, groups == g)', zones(:, groups == g)', var(zones'));
    %imagesc(d);
    Y = cmdscale(d);
    rank = ranks(groups == g);
    c = [];
    chosenid = ids(groups == g);
    chosenid = chosenid(1);
    for i=1:obj.nSubjects
        figure(1);
        SquareSubplpot(GroupsData.nGroups, g);
        plot(Y(rank == i, 1), Y(rank == i, 2), 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none'); hon;
    end
end

%% pca 3d
clf;
for g=1:GroupsData.nGroups
    %%
    V = cov(data(:, groups == g)');
    [coeff, l] = pcacov(V);
    c = coeff(1:3, :) * data(:, groups == g);
    rank = ranks(groups == g);
    for i=1:obj.nSubjects
        figure(1);
        SquareSubplpot(GroupsData.nGroups, g);
        plot3(c(1, rank == i), c(2, rank == i), c(3, rank == i), '.', 'Color', cmap(i,:)); hon;
    end
    hoff;
end

%% pca 2d
clf;
for g=1:GroupsData.nGroups
    %%
    V = cov(data(:, groups == g)');
    [coeff, l] = pcacov(V);
    c = coeff(1:2, :) * data(:, groups == g);
    rank = ranks(groups == g);
    for i=1:obj.nSubjects
        figure(1);
        SquareSubplpot(GroupsData.nGroups, g);
        plot(c(1, rank == i), c(2, rank == i), '.', 'Color', cmap(i,:)); hon;
    end
    hoff;
end
%%
sperms = perms(1:GroupsData.nSubjects);
vecs = {};
for i=ids
    d = data(:, ids == i);
    vecs{i} = [];
    for j=1:size(sperms, 1)
        vecs{i}(j, :) =  reshape(d(:, sperms(j, :)), 1, 100*4);
    end
end
