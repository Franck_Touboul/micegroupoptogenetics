function varargout = ShowNewEngagements(varargin)
% SHOWNEWENGAGEMENTS M-file for ShowNewEngagements.fig
%      SHOWNEWENGAGEMENTS, by itself, creates a new SHOWNEWENGAGEMENTS or raises the existing
%      singleton*.
%
%      H = SHOWNEWENGAGEMENTS returns the handle to a new SHOWNEWENGAGEMENTS or the handle to
%      the existing singleton*.
%
%      SHOWNEWENGAGEMENTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOWNEWENGAGEMENTS.M with the given input arguments.
%
%      SHOWNEWENGAGEMENTS('Property','Value',...) creates a new SHOWNEWENGAGEMENTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ShowNewEngagements_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ShowNewEngagements_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ShowNewEngagements

% Last Modified by GUIDE v2.5 08-Jul-2010 17:31:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ShowNewEngagements_OpeningFcn, ...
                   'gui_OutputFcn',  @ShowNewEngagements_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

global options;
options.VLCCmd = 'C:\Program Files\VideoLAN\VLC\VLC.exe';
options.MovieFile= 'C:\Documents and Settings\USER\Desktop\Hezi\Trial     1.mpg';

options.stop = false;

options.x1 = -40;
options.x2 =  40;
options.y1 = -33;
options.y2 =  33;
options.w = options.x2 - options.x1;
options.h = options.y2 - options.y1;

% --- Executes just before ShowNewEngagements is made visible.
function ShowNewEngagements_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ShowNewEngagements (see VARARGIN)

% Choose default command line output for ShowNewEngagements
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using ShowNewEngagements.
% if strcmp(get(hObject,'Visible'),'off')
%     plot(rand(5));
% end

global currMice;
currMice = [];
pushMouse(1);
pushMouse(2);

global myHandels;
myHandels.main = findall(gcf, 'Tag', 'mainAxes');
myHandels.list = findall(gcf, 'Tag', 'EventList');
myHandels.graph = findall(gcf, 'Tag', 'graphAxes');
myHandels.graph2 = findall(gcf, 'Tag', 'graphAxes2');
myHandels.showMovie = findall(gcf, 'Tag', 'showMovie');

% UIWAIT makes ShowNewEngagements wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ShowNewEngagements_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

global myData;

Message('Finding engagements...');
myData = SocialBehaviour;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global currMice;
global myData;
global currEngagements;
global myHandels;
global currEventId;

eventNames = {};
currEngagements = myData.engagements( min(currMice(end-1), currMice(end)), max(currMice(end-1), currMice(end)));
for i=1:length(currEngagements.startFrame)
    eventNames{i} = sprintf('%-20s (%7.1f-%7.1f)', currEngagements.title{i}, currEngagements.startFrame(i) * 0.04, currEngagements.endFrame(i) * 0.04);
end
set(myHandels.list, 'String', eventNames);
currEventId = -1;

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});


% --- Executes on selection change in EventList.
function EventList_Callback(hObject, eventdata, handles)
% hObject    handle to EventList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns EventList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from EventList
global options;
global currEngagements;
global currEventId;
global currMice;
global video;
global myHandels;
global myData;

padding = 2;

options.VLCCmd = '"C:\Program Files\VideoLAN\VLC\VLC.exe"';
%options.MovieFile= '"C:\Documents and Settings\USER\Desktop\Hezi\Trial     1.mpg"';
options.MovieFile= 'C:\Documents and Settings\USER\Desktop\Hezi\Trial     1.mpg';

%system([options.VLCCmd  ' ' options.MovieFile ' --start-time ' num2str(Events{get(hObject, 'Value')}.startTime)]);

id = get(hObject, 'Value');
dt = myData.time(2) - myData.time(1);

sf = currEngagements.startFrame(id)- padding / dt;
ef = currEngagements.endFrame(id)+ padding / dt;

mice1 = min(currMice(end-1), currMice(end));
mice2 = max(currMice(end-1), currMice(end));

if id ~= currEventId
    hmsg = waitbar(0,'Please wait while loading movie...');
    if get(myHandels.showMovie, 'value') == 1
        video = mmread(options.MovieFile,[],[currEngagements.startTime(id)-padding currEngagements.endTime(id)+padding]);
    else
        video.frames = [];
%         for i=sf:ef
%             video.frames(i - sf + 1).cdata = zeros(576,704);
%         end
    end
        
    close(hmsg);
    currEventId = id;
end


axes(myHandels.graph);
plot(sf:ef, myData.velocity(mice1, sf:ef), ...
    sf:ef, myData.velocity(mice2, sf:ef));
graph_axis = axis;
axis([sf, ef, graph_axis(3), graph_axis(4)]);
lineh = line([sf, sf], [graph_axis(3), graph_axis(4)], 'color', 'k');
title('Velocity');
% legend(num2str(mice1), num2str(mice2), 'location', 'northwest');
% legend box off

axes(myHandels.graph2);
% plot(sf:ef, myData.area(mice1, sf:ef) / mean(myData.area(mice1, isfinite(myData.area(mice1, :)))), ...
%     sf:ef, myData.area(mice2, sf:ef) / mean(myData.area(mice2, isfinite(myData.area(mice2, :)))));
% plot(sf:ef, shiftdim(myData.chase(mice1, mice2, sf:ef)), ...
%     sf:ef, shiftdim(myData.chase(mice2, mice1, sf:ef)));
chase1 = shiftdim(myData.chase(mice1, mice2, sf:ef));
chase2 = shiftdim(myData.chase(mice2, mice1, sf:ef));
chase1(~isfinite(chase1)) = 0;
chase2(~isfinite(chase2)) = 0;
plot(sf:ef, (chase1), ...
    sf:ef, (chase2));
graph2_axis = axis;
line([currEngagements.startFrame(id), currEngagements.startFrame(id)], [graph2_axis(3) graph2_axis(4)], 'linestyle', ':', 'Color', 'k');
line([currEngagements.endFrame(id), currEngagements.endFrame(id)], [graph2_axis(3) graph2_axis(4)], 'linestyle', ':', 'Color', 'k');
axis([sf, ef, graph2_axis(3), graph2_axis(4)]);
title('Area');
lineh2 = line([sf, sf], [graph2_axis(3), graph2_axis(4)], 'color', 'k');


% legend(num2str(mice1), num2str(mice2), 'location', 'northwest');
% legend box off

%%
options.stop = false;
for i=1:ef-sf+1
    axes(myHandels.main)
    if length(video.frames) > 0
        imshow(video.frames(i).cdata);
    else
        if i == 1
            imshow(zeros(576,704));
        end
    end
    a = axis;
    if i == 1
        w = a(2)-a(1);
        h = a(4)-a(3);
        text(a(1) + w / 14, a(3) + h / 14, 'Before', 'Color', 'w');
    end
    if currEngagements.endFrame(id) < i + sf - 1
        text(a(1) + w / 14, a(3) + h / 14, 'After', 'Color', 'w');
    elseif currEngagements.startFrame(id) < i + sf - 1
        text(a(1) + w / 14, a(3) + h / 14, 'Engagement', 'Color', 'w');
    else
        text(a(1) + w / 14, a(3) + h / 14, 'Before', 'Color', 'w');
    end
    hold on;
    cX1 = ((myData.x(mice1, sf:sf+i) - options.x1) / options.w) * w;
    cY1 = (1 - (myData.y(mice1, sf:sf+i) - options.y1) / options.h) * h;
    plot(cX1, cY1, 'b.-');
    %arrow([cX1(end), cY1(end)], [cX1(end) + (cX1(end) - cX1(end-1)), cY1(end) + (cY1(end) - cY1(end-1))]);
    plot(((myData.x(mice2, sf:sf+i) - options.x1) / options.w) * w, ...
         (1 - (myData.y(mice2, sf:sf+i) - options.y1) / options.h) * h, 'g.-');
    hold off;
    %%
    axes(myHandels.graph);
    delete(lineh);
    lineh = line([sf + i, sf + i], [graph_axis(3), graph_axis(4)], 'color', 'k');
    %
    axes(myHandels.graph2);
    delete(lineh2);
    lineh2 = line([sf + i, sf + i], [graph2_axis(3), graph2_axis(4)], 'color', 'k');
    %%
    drawnow;
    if options.stop; break; end
    pause(0.05);
    if options.stop; break; end
end
%disp 'a';

% --- Executes during object creation, after setting all properties.
function EventList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EventList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
if get(hObject,'Value') 
    pushMouse(1);
end

% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2
if get(hObject,'Value') 
    pushMouse(2);
end

% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3
if get(hObject,'Value') 
    pushMouse(3);
end

% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4
if get(hObject,'Value') 
    pushMouse(4);
end

function pushMouse(num)
global currMice;
currMice = [currMice, num];
h = findall(gcf, 'Tag', ['checkbox' num2str(num)]);
set(h, 'Value', 1);
if length(currMice) > 2
    remove = currMice(1:end-2);
    for r=remove
        if r~=num
            h = findall(gcf, 'Tag', ['checkbox' num2str(r)]);
            set(h, 'Value', 0);
        end
    end
    currMice = currMice(end-1:end);
end



function MessageBox_Callback(hObject, eventdata, handles)
% hObject    handle to MessageBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of MessageBox as text
%        str2double(get(hObject,'String')) returns contents of MessageBox as a double


% --- Executes during object creation, after setting all properties.
function MessageBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MessageBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Message(str)
h = findall(gcf, 'Tag', 'MessageBox');
full = get(h, 'String');
set(h, 'String', str);
if ~isempty(str)
    fprintf(['# ' str '\n']);
end


% --- Executes on button press in pushbutton1.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in stopButton.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to stopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in stopButton.
function stopButton_Callback(hObject, eventdata, handles)
% hObject    handle to stopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global options;
options.stop = true;


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over stopButton.
function stopButton_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to stopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in showMovie.
function showMovie_Callback(hObject, eventdata, handles)
% hObject    handle to showMovie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showMovie
