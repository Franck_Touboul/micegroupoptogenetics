function [obj, jointProbs] = SocialComputePatternPottsProbs(obj, order)

curr = obj;
curr.Output = false;
curr.OutputToFile = false;
curr.OutputInOldFormat = false;
curr = SocialPotts(curr, struct('order', order));
jointProbs = curr.Analysis.Potts.Model{order}.prob';

