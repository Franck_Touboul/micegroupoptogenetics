function [obj, coordinates] = TrackComputeColorsGUI(obj, frames)
%%
obj = TrackLoad(obj);
%%
if ~exist('frames', 'var')
    frames = [];
end

clf;
coordinates = struct('x', [], 'y', [], 'label', [], 'frame', []);
count = zeros(1, obj.nSubjects);
index = 0;
shouldContinue = true;
while shouldContinue
    try
        index = index + 1;
        if length(frames) > 1
            if index > length(frames)
                break;
            end
            f = frames(index);
            [obj, img] = TrackSimpleSegmentFrame(obj, f);
            if isempty(img)
                continue;
            end
        else
            if index > obj.nColorFrames
                break;
            end
            img = [];
            while isempty(img)
                f = randi(obj.nFrames, 1, 1);
                [obj, img] = TrackSimpleSegmentFrame(obj, f);
                if ~any(img.segmented(:))
                    img = [];
                end
            end
        end
        %
        img.segmented = imresize(img.segmented, 1/obj.VideoScale);
        boundries = (imdilate(img.segmented, ones(3,3)) - img.segmented) ~= 0;
        for i=1:3
            slice = img.orig(:, :, i);
            slice(boundries) = 255;
            img.orig(:, :, i) = slice;
        end
        imshow(img.orig);
        %
        while true
            title(sprintf('frame %d, insert subject id...', f));
            k = 0;
            while k ~= 1
                k = waitforbuttonpress;
            end
            key = get(gcf, 'CurrentCharacter');
            if key < 48 || key > 57
                if char(key) == 'q' || (key == 0 && strcmpi(get(gcf, 'SelectionType'), 'Open'));
                    shouldContinue = false;
                    break
                end
                imshow(img.orig*0);
                title(sprintf('loading new frame...'));
                drawnow;
                break;
            end
            label = key - 48;
            title(sprintf('frame %d, marked subject (%d)...', f, label));
            [y, x] = ginput(1);
            
            coordinates.x = [coordinates.x, round(x)];
            coordinates.y = [coordinates.y, round(y)];
            coordinates.label = [coordinates.label, label];
            coordinates.frame = [coordinates.frame, f];
            fprintf('# - added subject %d in coordinate (%3d, %3d), total: (', label, round(x), round(y));
            if label > length(count)
                count(label) = 0;
            end
            count(label) = count(label) + 1;
            fprintf(' %2d', count);
            fprintf(' )\n');
        end
    catch me
        fprintf(['# warning! ' me.message '\n'])
    end
end
title(sprintf('Done!'));
obj.Colors.Coordinates = coordinates;
obj.nSubjects = length(count);