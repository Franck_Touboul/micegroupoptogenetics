data = obj.zones(:, obj.valid)';
for order = 1:obj.nSubjects
    me = MEFeaturesInteractionOrder(data, obj.ROI.nZones, order);
    me = METrain(me, data);
    obj.Potts.Model{order} = me;
end
%%
temp = SocialSetTimeScale(obj, 6);
data = temp.zones(:, temp.valid)';
for order = 1:4
    me = MEFeaturesMeanFieldInteractionOrder(data, obj.ROI.nZones, order);
    me = METrain(me, data, 'Algorithm', 'GIS', 'nIters', 5000);
    me = METrain(me, data, 'Algorithm', 'NestrovGD', 'InitWeights', me.Weights, 'nIters', 50000);
    temp.Potts.MeanField.Model{order} = me;
end
nobj = TrackGenerateObject('auxSC.exp0006.day02.cam04');
nobj.Potts = temp.Potts;
TrackSave(nobj);
%%
me1 = MEFeaturesMeanFieldInteractionOrder(data, obj.ROI.nZones, 1);
