function zones = ZoneOpenMarkovShuffle(obj)
%%
neutral = find(strcmp(obj.ROI.ZoneNames, 'Open'));
zones = obj.zones(:, obj.valid);
for s=1:obj.nSubjects
    %%
    z = zones(s, :);
    chnage = [true z(1:end-1) ~= z(2:end)];
    beg = find(chnage);
    len = [diff(beg), length(z) - beg(end)];
    id = z(beg);
    probs = EstimateMarkov(obj, id(id ~= neutral));
    probs.cond(neutral, :) = 0;
    probs.cond(:, neutral) = 0;
    seq = 1:obj.ROI.nZones;
    probs.cond(neutral, seq(seq ~= neutral)) = 1 / (obj.ROI.nZones - 1);
    %%
    data = [];
    for i=1:obj.ROI.nZones
        data(i).len = len(id == i);
        data(i).map = id == i;
        data(i).idx = find(data(i).map);
        data(i).prob = ones(1, length(data(i).idx)) / length(data(i).idx);
    end
    %%
    nz = zeros(1, length(z));
    offset = 1;
    done = false;
    while ~done
        r = hmmgenerate(2 * length(id), probs.cond, eye(obj.ROI.nZones));
        r = r(2:end);
        for i=1:length(r)
            ri = find(rand < cumsum(data(r(i)).prob), 1);
            data(r(i)).prob(ri) = min(data(r(i)).prob) / 2;
            data(r(i)).prob = data(r(i)).prob / sum(data(r(i)).prob);
            clen = data(r(i)).len(ri);
            if data(r(i)).idx(ri) > 1 && id(data(r(i)).idx(ri) - 1) == neutral
                zlen = len(data(r(i)).idx(ri) - 1);
            else
                zlen = 0;
            end
            fr = offset;
            to = min(offset+zlen-1, size(z, 2));
            nz(fr:to) = neutral;
            offset = to + 1;
            
            fr = offset;
            to = min(offset+clen-1, size(z, 2));
            nz(fr:to) = r(i);
            offset = to + 1;
            
            if to == size(z, 2)
                done = true;
                break;
            end
        end
    end
    zones(s, :) = nz;
end