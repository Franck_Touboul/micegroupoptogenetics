options.padding = 1;

options.save = false;
%%
m1 = 1;
m2 = 2;

data = QSocialBehaviour;
engagements  = QSocialFindChases(m1, m2, data);
iEngagements = QSocialFindChases(m2, m1, data);
%

% axes(myHandels.graph);
% % plot(sf:ef, myData.area(mice1, sf:ef) / mean(myData.area(mice1, isfinite(myData.area(mice1, :)))), ...
% %     sf:ef, myData.area(mice2, sf:ef) / mean(myData.area(mice2, isfinite(myData.area(mice2, :)))));
% % plot(sf:ef, shiftdim(myData.chase(mice1, mice2, sf:ef)), ...
% %     sf:ef, shiftdim(myData.chase(mice2, mice1, sf:ef)));
% chase1 = shiftdim(myData.chase(m1, m2, sf:ef));
% chase2 = shiftdim(myData.chase(m2, m1, sf:ef));
% chase1(~isfinite(chase1)) = 0;
% chase2(~isfinite(chase2)) = 0;
% plot(sf:ef, (chase1), 'Color', myData.colors{m1});
% hold on;
% plot(sf:ef, (chase2), ':', 'Color', myData.colors{m2});
% hold off;
% graph_axis = axis;
% line([currEngagements.startFrame(id), currEngagements.startFrame(id)], [graph_axis(3) graph_axis(4)], 'linestyle', ':', 'Color', 'k');
% line([currEngagements.endFrame(id), currEngagements.endFrame(id)], [graph_axis(3) graph_axis(4)], 'linestyle', ':', 'Color', 'k');
% for i=1:length(currEngagements.epoch{id}.startFrame)
%     line([currEngagements.epoch{id}.startFrame(i), currEngagements.epoch{id}.startFrame(i)], ...
%         [graph_axis(3) graph_axis(4)], 'linestyle', ':', 'Color', 'k');
%     text(currEngagements.epoch{id}.startFrame(i), graph_axis(4), [' ' currEngagements.epoch{id}.title{i}], 'HorizontalAlignment', 'Left', 'VerticalAlignment', 'Top', 'Rotation', -90)
% end
%

%%
options.MovieFile= 'C:\Documents and Settings\USER\Desktop\Hezi\Trial     1.mpg';
options.outputMovieFile = 'temp.avi';

movieHandler = mmreader(options.MovieFile);
if exist('aviobj') && strcmp(aviobj.CurrentState, 'Open')
    aviobj = close(aviobj);
end

%for i=1:movieHandler.NumberOfFrames
%%
dt = data.time(2) - data.time(1);
%%
%for id = 6
for id = 41:length(engagements.epoch);
    clf;
    
    sf = min(engagements.startFrame(id), iEngagements.startFrame(id))- options.padding / dt;
    ef = max(engagements.endFrame(id), iEngagements.endFrame(id)) + options.padding / dt;
    
    %%
    set(gcf, 'Color', [1 1 1] * .6)
    
    subplot(8,1,1);
    chase1 = shiftdim(data.angle(m1, m2, sf:ef));
    chase2 = shiftdim(data.angle(m2, m1, sf:ef));
    chase1(~isfinite(chase1)) = 0;
    chase2(~isfinite(chase2)) = 0;
    plot(sf:ef, (chase1), 'Color', data.colors(m1, :));
    hold on;
    plot(sf:ef, (chase2), ':', 'Color', data.colors(m2, :));
    hold off;
    graph_axis = axis;
    line([engagements.startFrame(id), engagements.startFrame(id)], [graph_axis(3) graph_axis(4)], 'linestyle', ':', 'Color', 'k');
    line([engagements.endFrame(id), engagements.endFrame(id)], [graph_axis(3) graph_axis(4)], 'linestyle', ':', 'Color', 'k');
    for i=1:length(engagements.epoch{id}.startFrame)
        line([engagements.epoch{id}.startFrame(i), engagements.epoch{id}.startFrame(i)], ...
            [graph_axis(3) graph_axis(4)], 'linestyle', ':', 'Color', 'k');
        text(engagements.epoch{id}.startFrame(i), graph_axis(4), [' ' engagements.epoch{id}.title{i}], 'HorizontalAlignment', 'Left', 'VerticalAlignment', 'Top', 'Rotation', -90)
    end
    %axis([sf, ef, graph_axis(3), graph_axis(4)]);
    axis([sf, ef, 0, pi]);
    rowTitle(8,1,1, ['Mouse ' num2str(m1)], 'Color', data.colors(m1, :));
    set(gca, 'XTickLabel', sec2time(get(gca, 'XTick') * dt))
    lineh1 = line([sf, sf], [graph_axis(3), graph_axis(4)], 'color', 'k');
    %%
    subplot(8,1,2);
    chase1 = shiftdim(data.chase(m1, m2, sf:ef));
    chase2 = shiftdim(data.chase(m2, m1, sf:ef));
    chase1(~isfinite(chase1)) = 0;
    chase2(~isfinite(chase2)) = 0;
    plot(sf:ef, (chase1), ':', 'Color', data.colors(m1, :));
    hold on;
    plot(sf:ef, (chase2), 'Color', data.colors(m2, :));
    hold off;
    line([iEngagements.startFrame(id), iEngagements.startFrame(id)], [graph_axis(3) graph_axis(4)], 'linestyle', ':', 'Color', 'k');
    line([iEngagements.endFrame(id), iEngagements.endFrame(id)], [graph_axis(3) graph_axis(4)], 'linestyle', ':', 'Color', 'k');
    for i=1:length(iEngagements.epoch{id}.startFrame)
        line([iEngagements.epoch{id}.startFrame(i), iEngagements.epoch{id}.startFrame(i)], ...
            [graph_axis(3) graph_axis(4)], 'linestyle', ':', 'Color', 'k');
        text(iEngagements.epoch{id}.startFrame(i), graph_axis(4), [' ' iEngagements.epoch{id}.title{i}], 'HorizontalAlignment', 'Left', 'VerticalAlignment', 'Top', 'Rotation', -90)
    end
    axis([sf, ef, graph_axis(3), graph_axis(4)]);
    rowTitle(8,1,2, ['Mouse ' num2str(m2)], 'Color', data.colors(m2, :));
    set(gca, 'XTickLabel', sec2time(get(gca, 'XTick') * dt))
    lineh2 = line([sf, sf], [graph_axis(3), graph_axis(4)], 'color', 'k');
    %%
    prevStrLen = 0;
    RePrintf('# epoch no. %d/%d\n', id, length(engagements.epoch));
    %%
    filename = sprintf('res\\%03d[mice %d,%d](%s-%s)', id, m1, m2, sec2time(sf*dt, true), sec2time(ef*dt, true));
    if options.save
        fprintf('# - creating file: %s\n', filename);
        aviobj = avifile(filename, 'fps', movieHandler.FrameRate);
        aviobj.compression = 'wmv3';
    end
    %%
    RePrintf('# - frame no. ');
    id1 = 1;
    id2 = 1;
    clear('F');
    for i=0:ef-sf
        prevStrLen = RePrintf(prevStrLen, '(%3d/%3d)', sf+i, ef);
        frame = read(movieHandler, sf+i);
        
        %%
        subplot(8,1,3:8);
        imshow(frame);
        a = axis;
        if i == 0
            w = a(2)-a(1);
            h = a(4)-a(3);
            %text(a(1) + w / 14, a(3) + h / 14, 'Before', 'Color', 'w');
        end
        
        hold on;
        plot(orig.x(m1, sf:sf+i), orig.y(m1, sf:sf+i), '.-', 'Color', data.colors(m1, :));
        plot(orig.x(m2, sf:sf+i), orig.y(m2, sf:sf+i), '.-', 'Color', data.colors(m2, :));
        hold off;
        %%
        if id1 <= length(engagements.epoch{id}.startFrame) && engagements.epoch{id}.startFrame(id1) <= sf+i && engagements.epoch{id}.endFrame(id1) >= sf+i
            text(a(1) + w / 14, a(3) + h / 14, engagements.epoch{id}.title{id1}, 'Color', data.colors(m1, :), 'FontSize', 20);
        else
            if id1 <= length(engagements.epoch{id}.startFrame) && engagements.epoch{id}.endFrame(id1) < sf+i
                id1 = id1 + 1;
            end
        end
        %%
        if id2 <= length(iEngagements.epoch{id}.startFrame) && iEngagements.epoch{id}.startFrame(id2) <= sf+i && iEngagements.epoch{id}.endFrame(id2) >= sf+i
            text(a(1) + w / 14, a(3) + h / 14 + h / 14, iEngagements.epoch{id}.title{id2}, 'Color', data.colors(m2, :), 'FontSize', 20);
        else
            if id2 <= length(iEngagements.epoch{id}.startFrame) && iEngagements.epoch{id}.endFrame(id2) < sf+i
                id2 = id2 + 1;
            end
        end
        
        %%
        subplot(8,1,1);
        if exist('lineh1'); delete(lineh1); end
        lineh1 = line([sf + i, sf + i], [graph_axis(3), graph_axis(4)], 'color', 'k');
        %
        subplot(8,1,2);
        if exist('lineh2'); delete(lineh2); end
        lineh2 = line([sf + i, sf + i], [graph_axis(3), graph_axis(4)], 'color', 'k');
        
        drawnow;
        %F(i+1) = getframe(gcf);
        if options.save
            F = getframe(gcf);
            aviobj = addframe(aviobj,F);
        end
    end
    %movie2avi(F, filename, 'compression', 'wmv3');
    prevStrLen = RePrintf(prevStrLen, '(%3d/%3d)', sf+i, sf+ef);
    fprintf('\n');
    if options.save
        aviobj = close(aviobj);
    end
end