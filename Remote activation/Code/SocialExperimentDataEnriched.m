experiments = {...
    'Enriched.exp0001.day%02d.cam04',...
    'Enriched.exp0002.day%02d.cam04',...
    'Enriched.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'Enriched.exp0005.day%02d.cam04',...
    'Enriched.exp0006.day%02d.cam01',...
    };

E.map = logical([1 1 1 1 1 1]);
E.idx = find(E.map);

SC.map = ~E.map;
SC.idx = find(SC.map);

Groups(1).map = logical([1 1 1 1 1 1]);
Groups(1).idx = find(Groups(1).map);
Groups(1).title = 'Enriched';
Groups(1).color =  [80 171 210] / 255;

Groups(2).map = ~Groups(1).map;
Groups(2).idx = find(Groups(2).map);
Groups(2).title = 'SC';
Groups(2).color =  [232 24 46] / 255;

titles = {'Enriched', 'Control'};

nDays = 4;
nSubjects = 4;