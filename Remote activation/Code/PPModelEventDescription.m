function desc = PPModelEventDescription(model, states, f0)
desc = '';
prev = -1;
for f=1:length(states)+1
    if f>length(states) || states(f) ~= prev
        if prev > 0
            desc = sprintf([desc '%-5s (%8d - %8d)\n'], model.states(prev).title , (f0-1)+pstart, (f0-1)+f-1);
        end
        if f<=length(states)
            prev = states(f);
        end
        pstart = f;
    end
end
