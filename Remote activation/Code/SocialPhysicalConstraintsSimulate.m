%function SocialPhysicalConstraintsSimulate
id = [11, 12, 13, 14];
res = [];
res.zones = [];
for i=1:length(id)
    obj = TrackLoad({id(i), 2});
    %%
    z = obj.zones(i, obj.valid);
    if i>1
        n = min(length(z), size(res.zones, 2));
        res.zones = [res.zones(:, 1:n); z(1:n)];
    else
        n = length(z);
        res.zones = z;
    end
    size(res.zones);
end
%%
res.obj = obj;
res.obj.zones = res.zones;
res.obj.nFrames = size(res.zones, 2);
res.obj.valid = true(1, res.obj.nFrames);
res.obj.Output = true;
res.obj.OutputToFile = false;
res.obj.OutputInOldFormat = false;
res.obj.FilePrefix = 'Mix.exp0001.day02.cam04';
%%
limits = [4, 2, 2, 2, 2, 2, 2, 2, 4, 2];
limits = [4, 4, 4, 4, 4, 4, 4, 4, 4, 4];
zones = res.zones;
for z=1:obj.ROI.nZones
    zones = zones(:, sum(zones == z) <= limits(z));
end
res.obj.zones = zones;
res.obj.nFrames = size(zones, 2);
res.obj.valid = true(1, res.obj.nFrames);
res.obj.Output = true;
res.obj.OutputToFile = false;
res.obj.OutputInOldFormat = false;
%%
res.constobj = SocialPotts(res.obj);