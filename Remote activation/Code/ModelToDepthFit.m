function ModelVec = ModelToDepthFit(ModelVec, Depth, map, opt)
%%
if nargin < 4
    opt.HeightThresh = 0.01;
end

%%
% depth
prop = regionprops(map, 'PixelIdxList', 'Centroid', 'MajorAxis');
cmDX = Depth.X(round(prop.Centroid(2)), round(prop.Centroid(1)));
cmDY = Depth.Y(round(prop.Centroid(2)), round(prop.Centroid(1)));

dx = Depth.X(prop.PixelIdxList) - cmDX;
dy = Depth.Y(prop.PixelIdxList) - cmDY;

Cxx = mean(dx.^2);
Cyy = mean(dy.^2);
Cxy = mean(dx.*dy);
C = [Cxx, Cxy; Cxy, Cyy];
pC = pcacov(C);

distToOrth = [dx, dy] * pC;
imx = zeros(1, 2); mx = zeros(1, 2);
imn = zeros(1, 2); mn = zeros(1, 2);
for i=1:2
    [~, imx(i)] = max(abs(distToOrth(:, i)));
    mx(i) = distToOrth(imx(i), i);
    [~, imn(i)] = max(-distToOrth(:, i) * sign(mx(i)));
    mn(i) = distToOrth(imn(i), i);
end

Res.Depth.MajorPt1 = prop.PixelIdxList(imx(1));
Res.Depth.MajorPt2 = prop.PixelIdxList(imn(1));
Res.Depth.MajorAxis = mx(1) - mn(1);
Res.Depth.Orientation = atan2(pC(2, 1), pC(1, 1));
Res.Depth.MaxHeight = max(Depth.Z(map));

Res.Depth.MinorPt1 = prop.PixelIdxList(imx(2));
Res.Depth.MinorPt2 = prop.PixelIdxList(imn(2));
Res.Depth.MinorAxis = mx(2) - mn(2);

if 1==2
    %%
    pos = RotMatRz(pi-Res.Depth.Orientation) * [Depth.X(:), Depth.Y(:), Depth.Z(:)]';
    X = reshape(pos(1, :), size(Depth.X));
    Y = reshape(pos(2, :), size(Depth.X));
    %X = Depth.X;
    %Y = Depth.Y;
    plot(X(map), Y(map), '.'); axis equal
    hon
    plot(X(round(prop.Centroid(2)), round(prop.Centroid(1))), Y(round(prop.Centroid(2)), round(prop.Centroid(1))), 'go');
    plot(X(Res.Depth.MinorPt1), Y(Res.Depth.MinorPt1), 'ro');
    plot(X(Res.Depth.MinorPt2), Y(Res.Depth.MinorPt2), 'ro');
    plot(X(Res.Depth.MajorPt1), Y(Res.Depth.MajorPt1), 'ro');
    plot(X(Res.Depth.MajorPt2), Y(Res.Depth.MajorPt2), 'co');
    p =  polyfit(X(map), Y(map), 2);
    ux = unique(X(map));
    plot(ux, polyval(p, ux));
    
    %plot(cmDX, cmDY, 'go');
%     hoff
%     vX = X(map);
%     vY = Y(map);
%     SymmModel(vX, vY, vec);
%     %%
    %
    %  model
    vX = X(map);
    vY = Y(map);
    X1 = X(Res.Depth.MajorPt2);
    Y1 = Y(Res.Depth.MajorPt2);
    X2 = X(Res.Depth.MajorPt1);
    Y2 = Y(Res.Depth.MajorPt1);
    initVec = [X1, Y1, abs(Res.Depth.MajorAxis), [atan2(Y2-Y1, X2-X1), zeros(1, 4)]];
    %SymmModel(vX, vY, initVec);
    %hon
    %plot(X2, Y2, 'x', 'MarkerSize', 10);
    %hoff
    %
    SkinDistance = SymmModel(vX, vY, initVec);
    
    func = @(v) SymmModelFit(vX, vY, v);
    tic; [vec, d, eflag] = patternsearch(func, initVec, [], [], [], [], [], [], [], psoptimset('TolFun', 1e-4)); toc
    d
    SymmModel(vX, vY, vec);
end


%%
%map = Depth.Z > opt.HeightThresh;
%map = bwareaopen(map, 500);

% % depth
% prop = regionprops(map, 'PixelIdxList', 'Centroid', 'MajorAxis');
% cmDX = Depth.X(round(prop.Centroid(2)), round(prop.Centroid(1)));
% cmDY = Depth.Y(round(prop.Centroid(2)), round(prop.Centroid(1)));
% 
% % find main axes of depth
% [~, idx] = max((Depth.X(prop.PixelIdxList) - cmDX).^2 + (Depth.Y(prop.PixelIdxList) - cmDY).^2);
% pt1 = prop.PixelIdxList(idx);
% [~, idx] = max((Depth.X(prop.PixelIdxList) - Depth.X(pt1)).^2 + (Depth.Y(prop.PixelIdxList) - Depth.Y(pt1)).^2);
% pt2 = prop.PixelIdxList(idx);
% Res.Depth.MajorAxis = sqrt((Depth.X(pt1) - Depth.X(pt2)).^2 + (Depth.Y(pt1) - Depth.Y(pt2)).^2);
% Res.Depth.Pt1 = pt1;
% Res.Depth.Pt2 = pt2;
% Res.Depth.Orientation = atan2(Depth.Y(pt1) - Depth.Y(pt2), Depth.X(pt1) - Depth.X(pt2));
% Res.Depth.MaxHeight = max(Depth.Z(map));
% 
% p = [Depth.X(pt2) - Depth.X(pt1), Depth.Y(pt1) - Depth.Y(pt2)]'; 
% distToMajorAxis = [Depth.X(map) - Depth.X(pt1), Depth.Y(map) - Depth.Y(pt1)] * p / norm(p);
% [~, i1] = min(distToMajorAxis);
% [~, i2] = max(distToMajorAxis);
% 
% Res.Depth.Qt1 = prop.PixelIdxList(i1);
% Res.Depth.Qt2 = prop.PixelIdxList(i2);
% 
% plot(Depth.X(map), Depth.Y(map), '.'); axis equal
% hon
% plot(Depth.X(Res.Depth.Qt1), Depth.Y(Res.Depth.Qt1), 'ro');
% plot(Depth.X(Res.Depth.Qt2), Depth.Y(Res.Depth.Qt2), 'ro');
% plot(Depth.X(Res.Depth.Pt1), Depth.Y(Res.Depth.Pt1), 'ro');
% plot(cmDX, cmDY, 'go');
% hoff
%% plot
%     Params.Neck.Pitch = 0.1;
%     Params.Neck.Yaw = -0.1;
%     Params.Body.Yaw = -0.03;
%     Params.Body.FatScale = 1.3;

InitModel = [Res.Depth.Orientation, Res.Depth.MajorAxis, Depth.X(Res.Depth.MajorPt2), Depth.Y(Res.Depth.MajorPt2), Res.Depth.MaxHeight, 0, 1, Res.Depth.MinorAxis / (0.4 * Res.Depth.MajorAxis)];

opt.FitThresh = 0.005;
opt.MaxIters = 100;
opt.IterChunkSize = 100;

Data = [Depth.X(map), Depth.Y(map), Depth.Z(map)];
ModelVec = InitModel;

if 1==2
distToOrth = ModelToDepthDistance(InitModel, Data);
if distToOrth > opt.FitThresh
    func = @(x) ModelToDepthDistance(x, Data);
    searchopt = optimset('Display', 'iter', 'MaxIter', opt.IterChunkSize);
    nIters = 0;
    while nIters < opt.MaxIters
        [ModelVec, fval] = fminsearch(func, ModelVec, searchopt);
        if fval <= opt.FitThresh
            break;
        end
        nIters = nIters + opt.IterChunkSize;
    end
end
end

% plot
if nargout == 0
    subplot(3,1,1:2);
    [z, pos] = ModelToDepthDistance(ModelVec);
    plot3(pos(:, 1),pos(:, 2),pos(:, 3),'.');
    hon
    plot3(Depth.X(Depth.Z > opt.HeightThresh),Depth.Y(Depth.Z > opt.HeightThresh), Depth.X(Depth.Z > opt.HeightThresh)*0+.05, 'g.');
    
    plot3(Depth.X,Depth.Y,Depth.Z,'r.');
    shading interp; colormap(gray);
    grid on
    hoff
    view(0, 90)

    
    subplot(3,2,5);
    plot(Depth.X(map), Depth.Y(map), '.'); axis equal
    hon
    plot(Depth.X(Res.Depth.MinorPt1), Depth.Y(Res.Depth.MinorPt1), 'ro');
    plot(Depth.X(Res.Depth.MinorPt2), Depth.Y(Res.Depth.MinorPt2), 'ro');
    plot(Depth.X(Res.Depth.MajorPt1), Depth.Y(Res.Depth.MajorPt1), 'ro');
    plot(Depth.X(Res.Depth.MajorPt2), Depth.Y(Res.Depth.MajorPt2), 'ro');
    plot(cmDX, cmDY, 'go');
    hoff

    subplot(3,2,6);
    plot3(Depth.X(map),Depth.Y(map), Depth.Z(map), 'g.')
    hon
    plot3(pos(:, 1),pos(:, 2),pos(:, 3),'r.');
    hoff
    axis equal
    view(0, 90)
end