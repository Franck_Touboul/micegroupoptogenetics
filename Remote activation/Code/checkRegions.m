SetDefaults
loadData_v2

s = 1;
% z = 6
% %zones.labels{z}
[x, c] =  hist3([orig.x(s, :); orig.y(s, :)]');
% subplot(2,1,1);
% hist3([orig.x(s, zones.all(s, :) == z); orig.y(s, zones.all(s, :) == z)]', c);
% %imagesc(x')


%%
%subplot(2,1,2);
z_ = 0:max(zones.all(:));
%z_ = 10;
clf
cmap = lines;
index = 1;
x1 = min(orig.x(s,:));
x2 = max(orig.x(s,:));
y1 = min(orig.y(s,:));
y2 = max(orig.y(s,:));
for z=z_
    plot(orig.x(s, zones.all(s, :) == z), orig.y(s, zones.all(s, :) == z), 'Color', cmap(index, :));
    mx(index) = mean(orig.x(s, zones.all(s, :) == z));
    my(index) = mean(orig.y(s, zones.all(s, :) == z));
    hold on;
    index = index + 1;
end
axis([x1, x2, y1, y2]);
hold off;

index = 1;
for z=z_
    h = text(mx(index), my(index), num2str(z));
    set(h, 'FontSize', 30, 'FontWeight', 'bold', 'Color', 'w');
    index = index + 1;
end
