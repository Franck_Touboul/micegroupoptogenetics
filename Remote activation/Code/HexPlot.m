function HexPlot(bins, range, varargin)
orig = bins;
bins(isnan(orig)) = 0;
cla
mx1 = range(1);
mx2 = range(2);
my1 = range(3);
my2 = range(4);
dxs = (mx2 - mx1) / (size(bins, 2) * 3 - 1);
dx = dxs * 3;
dys = (my2 - my1) / (size(bins, 1) * 2 - 1);
dy = dys * 2;
%%
% xoff = dx / 4;
% yoff = dy / 2;
i = find(strcmpi(varargin, 'colormap'));
if isempty(i)
    %cmap = MyDefaultColormap(256);
    cmap = ColormapBlueToRed(256);
else
    cmap = varargin{i+1};
end
tau = mean(bins(:));
bins(bins > tau * 3) = tau * 3;
%bins = log(bins);

% m1 = min(bins(isfinite(bins)));
% m2 = max(bins(isfinite(bins)));

m1 = 0;
m2 = ceil(max(bins(isfinite(bins))) / sum(bins(:)) * 1000) / 1000 * sum(bins(:));

i = find(strcmpi(varargin, 'showbar'));
if ~isempty(i) && varargin{i+1} 
    colormap(cmap);
    h = colorbar('EastOutside');
    set(h, 'YTick', [1 128 256])
    ticks = (get(h, 'YTick') - 1) / (size(cmap, 1) - 1) * (m2 - m1) / sum(bins(:)) + m1 / sum(bins(:));
    labels = {};
    for i=1:length(ticks)
        labels{i} = sprintf('%.3f', ticks(i));
    end
    set(h, 'YTickLabel', labels);
end

for i=1:size(bins, 2)
    for j=1:size(bins, 1)
        x = [(i - 1) * dx - dxs, (i - 1) * dx, i * dx - dxs, i * dx, i * dx - dxs, (i - 1) * dx, (i - 1) * dx - dxs];
        b = mod(i, 2) - 1;
        y = [(j - 1) * dy + dys + b * dys, (j - 1) * dy + b * dys, (j - 1) * dy + b * dys, (j - 1) * dy + dys + b * dys, (j - 1) * dy + 2 * dys + b * dys, (j - 1) * dy + 2 * dys + b * dys, (j - 1) * dy + dys + b * dys];
        if ~isfinite(orig(j, i))
            patch(x + mx1, y + my1, 'w', 'EdgeColor', 'none'); hold on;
        else
            cidx = floor((bins(j, i) - m1) / (m2 - m1) * (size(cmap, 1) - 1)) + 1;
            patch(x + mx1, y + my1, cmap(cidx, :), 'EdgeColor', 'none'); hold on;
        end
    end
end
hold off;
axis([mx1 - dxs, mx2 + dxs, my1 - dys, my2 + dys]);
axis off;