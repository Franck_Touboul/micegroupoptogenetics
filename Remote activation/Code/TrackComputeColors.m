function obj = TrackComputeColors(obj, coordinates)
if ischar(obj)
    load(obj);
end

if ~exist('coordinates', 'var')
    coordinates = obj.Colors.Coordinates;
end

colorVec = cell(1, obj.nSubjects);

colorHistVecRGB = cell(1, obj.nSubjects);
colorHistVecHSV = cell(1, obj.nSubjects);

bkgHistVecRGB = [];
bkgHistVecHSV = [];

fillerHistVecRGB = [];
fillerHistVecHSV = [];

bins = sequence(0, 1, obj.nColorBins);

frames = unique(coordinates.frame);
for f=frames(:)'
    [obj, img] = TrackSimpleSegmentFrame(obj, f);
    img.scaled = im2double(imresize(img.orig, obj.VideoScale));
    labeledOrig    = bwlabel(img.segmented);
    labeledMatched = zeros(size(img.segmented), 'uint16');
    %%
    inframe = find(coordinates.frame == f);
    x = round(coordinates.x(inframe) * obj.VideoScale);
    y = round(coordinates.y(inframe) * obj.VideoScale);
    label = coordinates.label(inframe);
    for i=1:length(x)
        if x(i) > size(labeledOrig, 1) || x(i) < 1 || y(i) > size(labeledOrig, 2) || y(i) < 1
            fprintf('#   ! pixel (%3d, %3d) is out of range\n', label(i), x(i), y(i));
            continue;
        end
        labelId = labeledOrig(x(i), y(i));
        if labelId == 0
            fprintf('#   ! no pixels found for subject %d in (%3d, %3d)\n', label(i), x(i), y(i));
        else
            currSeg = (labeledOrig == labelId);
            if label(i) > 0
                colorVec{label(i)} = [colorVec{label(i)}; reshape(img.scaled(x(i), y(i), :), 1, 3, 1)];
            end
            if sum(sum(labeledMatched(currSeg) ~= 0)) > 0
                fprintf('#   ! segment for subject %d in (%3d, %3d) matches also segment for subject %d\n', label(i), x(i), y(i), max(max(labeledMatched(currSeg))));
                labeledMatched(currSeg) = 0;
            else
                fprintf('# - added label for subject %d in (%3d, %3d)\n', label(i), x(i), y(i));
                if obj.Output
                    emph = img.scaled * 0;
                    for c=1:3
                        slice = img.scaled(:,:,c);
                        black = slice * 0;
                        black(currSeg) = slice(currSeg);
                        emph(:, :, c) = black;
                        colors(c) = slice(x(i), y(i)); %mean(slice(currSeg));
                    end
                    subplot(2,3,[1:2, 4:5]);
                    imagesc(emph);
                    subplot(2,3,6);
                    imagesc(reshape(colors, 1, 1, 3));
                    subplot(2,3,3);
                    tempColorsImage = zeros(obj.nSubjects, 3);
                    for l=1:length(colorVec)
                        if ~isempty(colorVec{l})
                            tempColorsImage(l, :) = mean(colorVec{l}, 1);
                        end
                    end
                    tempColorsImage = reshape(tempColorsImage, 1, obj.nSubjects, 3);
                    imagesc(tempColorsImage);
                    drawnow;
                end
                if label(i) ~= 0
                    labeledMatched(currSeg) = label(i);
                else
                    labeledMatched(currSeg) = -1;
                end
            end
        end
    end
    %%
    r = img.scaled(:, :, 1);
    g = img.scaled(:, :, 2);
    b = img.scaled(:, :, 3);
    %
    h = img.hsv(:, :, 1);
    s = img.hsv(:, :, 2);
    v = img.hsv(:, :, 3);
    %
    for l=unique(labeledMatched(labeledMatched ~= 0))'
        qr_ = histc(r(labeledMatched == l), bins)';
        qg_ = histc(g(labeledMatched == l), bins)';
        qb_ = histc(b(labeledMatched == l), bins)';
        %
        qh_ = histc(h(labeledMatched == l), bins)';
        qs_ = histc(s(labeledMatched == l), bins)';
        qv_ = histc(v(labeledMatched == l), bins)';
        %
        if l>0
            colorHistVecRGB{l} = [colorHistVecRGB{l}; [qr_, qg_, qb_]];
            colorHistVecHSV{l} = [colorHistVecHSV{l}; [qh_, qs_, qv_]];
        else
            bkgHistVecRGB = [bkgHistVecRGB; [qr_, qg_, qb_]];
            bkgHistVecHSV = [bkgHistVecHSV; [qh_, qs_, qv_]];
        end
    end
    qr_ = histc(r, bins)';
    qg_ = histc(g, bins)';
    qb_ = histc(b, bins)';
    %
    qh_ = histc(h, bins)';
    qs_ = histc(s, bins)';
    qv_ = histc(v, bins)';
    %
    fillerHistVecRGB = [fillerHistVecRGB; [qr_, qg_, qb_]];
    fillerHistVecHSV = [fillerHistVecHSV; [qh_, qs_, qv_]];
end

%%
obj.Colors.Histrogram.R = zeros(obj.nSubjects, obj.nColorBins);
obj.Colors.Histrogram.G = zeros(obj.nSubjects, obj.nColorBins);
obj.Colors.Histrogram.B = zeros(obj.nSubjects, obj.nColorBins);
for l=1:length(colorHistVecRGB)
    subjectHist = sum(colorHistVecRGB{l}, 1) + .1;
    r = 0;
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.Histrogram.R(l, :) = subjectHist(r) / sum(subjectHist(r));
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.Histrogram.G(l, :) = subjectHist(r) / sum(subjectHist(r));
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.Histrogram.B(l, :) = subjectHist(r) / sum(subjectHist(r));
end

obj.Colors.Histrogram.H = zeros(obj.nSubjects, obj.nColorBins);
obj.Colors.Histrogram.S = zeros(obj.nSubjects, obj.nColorBins);
obj.Colors.Histrogram.V = zeros(obj.nSubjects, obj.nColorBins);
for l=1:length(colorHistVecHSV)
    subjectHist = sum(colorHistVecHSV{l}, 1) + .1;
    r = 0;
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.Histrogram.H(l, :) = subjectHist(r) / sum(subjectHist(r));
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.Histrogram.S(l, :) = subjectHist(r) / sum(subjectHist(r));
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.Histrogram.V(l, :) = subjectHist(r) / sum(subjectHist(r));
end

%%
obj.Colors.Samples = [];
obj.Colors.Labels = [];
for l=1:length(colorHistVecRGB)
    obj.Colors.Samples = [obj.Colors.Samples; colorHistVecRGB{l}];
    obj.Colors.Labels = [obj.Colors.Labels; sum(colorHistVecRGB{l}, 2) * 0 + l];
end

%% BKG histogram
obj.Colors.BkgHistogramRGB = [];
if ~isempty(bkgHistVecRGB)
    obj.Colors.BkgHistogramRGB = sum(bkgHistVecRGB, 1) + 1;
    obj.Colors.BkgHistogramRGB = obj.Colors.BkgHistogramRGB / sum(obj.Colors.BkgHistogramRGB);
end

obj.Colors.BkgHistogramHSV = [];
if ~isempty(bkgHistVecHSV)
    obj.Colors.BkgHistogramHSV = sum(bkgHistVecHSV, 1) + 1;
    obj.Colors.BkgHistogramHSV = obj.Colors.BkgHistogramHSV / sum(obj.Colors.BkgHistogramHSV);
end

%% Filler Histogram
obj.Colors.FillerHistogram.R = zeros(1, obj.nColorBins);
obj.Colors.FillerHistogram.G = zeros(1, obj.nColorBins);
obj.Colors.FillerHistogram.B = zeros(1, obj.nColorBins);
if ~isempty(fillerHistVecRGB)
    fillerHist = sum(fillerHistVecRGB, 1) + 1;
    r = 0;
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.FillerHistogram.R = fillerHist(r) / sum(fillerHist(r));
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.FillerHistogram.G = fillerHist(r) / sum(fillerHist(r));
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.FillerHistogram.B = fillerHist(r) / sum(fillerHist(r));
end

obj.Colors.FillerHistogram.H = zeros(1, obj.nColorBins);
obj.Colors.FillerHistogram.S = zeros(1, obj.nColorBins);
obj.Colors.FillerHistogram.V = zeros(1, obj.nColorBins);
if ~isempty(fillerHistVecHSV)
    fillerHist = sum(fillerHistVecHSV, 1) + 1;
    r = 0;
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.FillerHistogram.H = fillerHist(r) / sum(fillerHist(r));
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.FillerHistogram.S = fillerHist(r) / sum(fillerHist(r));
    r=r(end)+1:r(end)+obj.nColorBins; obj.Colors.FillerHistogram.V = fillerHist(r) / sum(fillerHist(r));
end

%%
obj.Colors.Image = zeros(obj.nSubjects, 3);
for l=1:length(colorVec)
    obj.Colors.Image(l, :) = mean(colorVec{l}, 1);
end
obj.Colors.Image = reshape(obj.Colors.Image, 1, obj.nSubjects, 3);
obj.Colors.Centers = reshape(obj.Colors.Image, obj.nSubjects, 3, 1);

obj.Colors.Bins = bins;
obj.Coordinates = coordinates;

%% Output to file
if obj.OutputToFile 
    if ~obj.OutputInOldFormat
        filename = [obj.OutputPath obj.FilePrefix '.meta.mat'];
        try
            fprintf('# - saving results to %s\n', label(i), x(i), y(i));
            save(filename, 'obj');
        catch me
            fprintf('#  . save failed: %s\n', me.message);
        end
    else
        meta = struct();
        meta.bkgFrame = obj.BkgImage;
        meta.subject.centerColors = obj.Colors.Centers;
        meta.subject.colorBins = obj.Colors.Bins;
        
        meta.subject.r = obj.Colors.Histrogram.R;
        meta.subject.g = obj.Colors.Histrogram.G;
        meta.subject.b = obj.Colors.Histrogram.B;

        meta.subject.h = obj.Colors.Histrogram.H;
        meta.subject.s = obj.Colors.Histrogram.S;
        meta.subject.v = obj.Colors.Histrogram.V;
        
        meta.subject.filler.r = obj.Colors.FillerHistogram.R;
        meta.subject.filler.g = obj.Colors.FillerHistogram.G;
        meta.subject.filler.b = obj.Colors.FillerHistogram.B;

        meta.subject.filler.h = obj.Colors.FillerHistogram.H;
        meta.subject.filler.s = obj.Colors.FillerHistogram.S;
        meta.subject.filler.v = obj.Colors.FillerHistogram.V;

        meta.options = obj;
        filename = [obj.OutputPath obj.FilePrefix '.meta.mat'];
        try
            fprintf('# - saving in old meta file format to %s\n', filename);
            save(filename, 'meta');
        catch me
            fprintf('#  . save failed: %s\n', me.message);
        end
    end
end
