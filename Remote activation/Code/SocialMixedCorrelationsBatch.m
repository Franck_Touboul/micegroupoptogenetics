SocialExperimentData
%%
niters = 30;
ENTROPY = {};
MI = {};
DAY = {};
for g=1:length(Groups)
    index = 1;
    for day=1:nDays
        for iter = 1:niters
            r = randperm(length(Groups(g).idx));
            r = Groups(g).idx(r(1:obj.nSubjects));
            
            prefix = sprintf(experiments{r(1)}, day);
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid'});
            for i=2:length(r)
                prefix = sprintf(experiments{r(i)}, day);
                o = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid'});
                len = min(size(obj.zones, 2), size(o.zones, 2));
                
                obj.zones = obj.zones(:, 1:len);
                cperm = randperm(len);
                obj.zones(i, :) = o.zones(i, cperm);
                
                obj.valid = obj.valid(1:len);
                obj.valid = obj.valid & o.valid(cperm);
            end
            [Tilde, mi, entropy] = SocialMutualInformation(obj);
            
            ENTROPY{g}(:, index) = entropy(:);
            MI{g}(:, :, index) = mi;
            DAY{g}(index) = day;
            index = index + 1;
        end
    end
end
%%
g = 1;
ALLENT = repmat(reshape(ENTROPY{g}, obj.nSubjects, 1, size(ENTROPY{g}, 2)), [1, obj.nSubjects 1]);
pairwiseP = MI{g} ./ ALLENT;
pairwiseP = pairwiseP(:);

DMI = [];
for i=1:size(MI{g}, 3)
    DMI(i, :) = diag(MI{g}(:, :, i));
end
condP = DMI ./ ENTROPY{g}';
condP = condP(:);

%%

nbins = 50;
m1 = min([min(pairwiseP), min(condP)]);
m2 = max(max(pairwiseP), max(condP));
x = sequence(m1, m2, nbins);
hpw = histc(pairwiseP, x);
htpw = histc(totalPairwiseP, x);
hc = histc(condP, x);
% [hpw, xpw] = hist(pairwiseP,25);
% [hc, xc] = hist(condP,25);
cmap = MySubCategoricalColormap;
plot(x*100, hpw/sum(hpw)*100, 'Color', cmap(2, :), 'LineWidth', 2);
hold on;
plot(x*100, hc/sum(hc)*100, 'Color', cmap(4, :), 'LineWidth', 2);
hold off;
prettyPlot('', 'information percentage [%]', 'percentage that fall in bin [%]');
%legend('Pair: I(x_i; x_j)', 'Multiple: I(x_i; x_{i~=j})');
legend('single', 'multiple');
legend boxoff;

return
%%
% index = 1;
% MI = zeros(nSubjects, nSubjects, length(experiments) * nDays);
% ENTROPY = zeros(nSubjects, length(experiments) * nDays);
% ID = zeros(1, length(experiments) * nDays);
% DAY = zeros(1, length(experiments) * nDays);
% for id=1:length(experiments)
%     for day = 1:nDays
%         prefix = sprintf(experiments{id}, day);
%         fprintf('# -> %s\n', prefix);
%         obj = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid'});
%         [~, mi, entropy] = SocialMutualInformation(obj);
%         
%         ENTROPY(:, index) = entropy(:);
%         MI(:, :, index) = mi;
%         DAY(index) = day;
%         ID(index) = id;
%         index = index + 1;
%     end
% end
%%
g=1;
nbins = 25;
m1 = floor(min(ENTROPY{g}(:)));
m2 = ceil(max(ENTROPY{g}(:)));

index = 1;
cmap = [1 1 1; 0.95 0.95 0.95];
entropy = [];
subplot(3,1,1:2);
x = sequence(m1, m2, nbins);
offset = (index - 1);
fill([m1 m1 m2 m2], [0 length(group.idx) * (nDays + 1) length(group.idx) * (nDays + 1) 0] + offset, cmap(b, :), 'EdgeColor', 'none');
hold on;
group = Groups(g);
for i=1:size(ENTROPY{g}, 2)
    for day=1:nDays
        idx = ID == i & DAY == day;
        entropy = [entropy; ENTROPY{g}(:, idx)];
        [sent, sord] = sort(ENTROPY{g}(:, idx));
        plot(sent, ones(1, 4) * index, 'ko-', 'Color', colors(b, :), 'MarkerEdgeColor', 'none'); hold on;
        for s=1:obj.nSubjects
            plot(sent(s), index, 'o', 'MarkerFaceColor', obj.Colors.Centers(sord(s), :), 'MarkerEdgeColor', 'none');
        end
        index = index + 1;
    end
    line([m1 m2], [index index],'Color', 'k', 'LineStyle', ':');
    index = index + 1;
end
%line([m1 m2], [index-1 index-1],'Color', 'k', 'LineStyle', ':');
subplot(3,1,3);
plot(x, histc(entropy, x), 'Color', group.color)
hold on;
subplot(3,1,1:2);
set(gca, 'YtickLabel', [])
a = axis;
axis([a(1) a(2) 0 index-1]);
hold off;
subplot(3,1,3);

%%
pairwise = [];
pairwiseP = [];
totalPairwiseP = [];
condP = [];
for i=SC.idx
    for day=1:nDays
        mi = MI(:, :, ID == i & DAY == day);
        entropy = ENTROPY(:, ID == i & DAY == day);
        
        condP = [condP; diag(mi) ./ entropy];
        
        mi(1:obj.nSubjects+1:end) = nan;
        pairwise = [pairwise; mi(:)];
        
        miP = mi ./ repmat(entropy, 1, obj.nSubjects);
        pairwiseP = [pairwiseP; miP(:)];
        
        mi(1:obj.nSubjects+1:end) = 0;
        totalPairwiseP = [totalPairwiseP; sum(mi, 2) ./ entropy];
    end
end
%%
% index = 1;
% for id=1:length(experiments)
%     for day = 1:nDays
%         mi=MI(:, :, index);
%         mi(1:5:end)=nan;
%         %
%         subplot(length(experiments), nDays, index);
%         MyImagesc(mi)
%         index = index  +1;
%     end
% end

%%
nbins = 50;
m1 = min([min(pairwiseP), min(condP)]);
m2 = max(max(pairwiseP), max(condP));
x = sequence(m1, m2, nbins);
hpw = histc(pairwiseP, x);
htpw = histc(totalPairwiseP, x);
hc = histc(condP, x);
% [hpw, xpw] = hist(pairwiseP,25);
% [hc, xc] = hist(condP,25);
cmap = MySubCategoricalColormap;
plot(x*100, hpw/sum(hpw)*100, 'Color', cmap(2, :), 'LineWidth', 2);
hold on;
plot(x*100, hc/sum(hc)*100, 'Color', cmap(4, :), 'LineWidth', 2);
hold off;
prettyPlot('', 'information percentage [%]', 'percentage that fall in bin [%]');
%legend('Pair: I(x_i; x_j)', 'Multiple: I(x_i; x_{i~=j})');
legend('single', 'multiple');
legend boxoff;

%%
nbins = 50;
m1 = min([min(pairwiseP), min(condP) min(totalPairwiseP)]);
m2 = max([max(pairwiseP), max(condP) max(totalPairwiseP)]);
x = sequence(m1, m2, nbins);
hpw = histc(pairwiseP, x);
htpw = histc(totalPairwiseP, x);
hc = histc(condP, x);
% [hpw, xpw] = hist(pairwiseP,25);
% [hc, xc] = hist(condP,25);
cmap = MySubCategoricalColormap;
plot(x*100, hpw/sum(hpw)*100, 'Color', cmap(2, :), 'LineWidth', 2);
hold on;
plot(x*100, hc/sum(hc)*100, 'Color', cmap(4, :), 'LineWidth', 2);
plot(x*100, htpw/sum(htpw)*100, 'Color', cmap(6, :), 'LineWidth', 2);
hold off;
prettyPlot('', 'information percentage [%]', 'percentage that fall in bin [%]');
%legend('Pair: I(x_i; x_j)', 'Multiple: I(x_i; x_{i~=j})');
legend('single', 'multiple', 'additive');
legend boxoff;