%function TrackPath(nruns)
nruns = 120;
%%
fprintf('# finding paths\n');
options = struct();
TrackDefaults;
%%
options.colorMatchThresh = 0.4;
options.MeanStep = 1;

%%
if 1==2
    fprintf('# - opening movie file\n');
    xyloObj = myMMReader(options.MovieFile);
    nframes = xyloObj.NumberOfFrames;
    dt = 1/xyloObj.FrameRate;
    %%
    fprintf('# - loading segmentations\n');
    cents = struct();
    cents.x       = [];
    for i=1:nruns
        fprintf('#      . segment no. %d\n', i);
        filename = [options.output_path options.test.name '.segm.' sprintf('%03d', i) '.mat'];
        waitforfile(filename);
        currSegm = load(filename);
        cents = structcat(cents, currSegm.cents, 1, true, false);
    end
end
%%
if isfield(options, 'ROIIgnore')
    ignore = uint8(imread(options.ROIIgnore));
    fprintf('#  . removing unwanted cents...');
    for i=1:size(cents.label, 1)
        labels = cents.label(i, :);
        y = cents.y(i,:);
        x = cents.x(i,:);
        y = y(labels~=0) / options.scale;
        if isempty(y)
            continue;
        end
        x = x(labels~=0) / options.scale;
        labels(labels~=0) = (1 - ignore( sub2ind(size(ignore), round(y), round(x)) )) .* labels(labels~=0);
        cents.label(i, :) = labels;
    end
    fprintf('[done]\n');
end
%%
prev = cents.label;
[q, cents.label] = max(cents.logprob, [], 3);
cents.label(prev == 0) = 0;

thresh = options.colorMatchThresh;
ndata = size(cents.x, 1);
for i=1:options.nSubjects
    logprobs = cents.logprob(:, :, i);
    logprobs(cents.label ~= i) = -inf;
    [maxlogprobs, centids] = max(logprobs, [], 2);
    maxprobs = exp(maxlogprobs);
    res{i}.x = cents.x(sub2ind(size(cents.x), 1:ndata, centids'));
    res{i}.y = cents.y(sub2ind(size(cents.y), 1:ndata, centids'));
    res{i}.prob = maxprobs';
    res{i}.logprob = maxlogprobs';
    res{i}.centids = centids';
    
    res{i}.valid = maxprobs' >= thresh;
    res{i}.x(maxprobs < thresh) = nan;
    res{i}.y(maxprobs < thresh) = nan;
    res{i}.prob(maxprobs < thresh) = nan;
    res{i}.logprob(maxprobs < thresh) = -inf;
    res{i}.id = (maxprobs' > thresh) * i;
    res{i}.centids(maxprobs < thresh) = nan;
    %% compute statistics
    step = sqrt(diff(res{i}.x).^2+diff(res{i}.y).^2);
    res{i}.step = [0 step];
    if options.MeanStep > 0
        res{i}.meanStep = options.MeanStep;
    else
        res{i}.meanStep = mean(step(~isnan(step)));
    end
    res{i}.pareto = gpfit(step(step>0));
end
%%
for i=1:options.nSubjects
    distances = [];
    for j=1:options.nSubjects
        if j~=i
            distances = [distances, sqrt((res{i}.x - res{j}.x).^2 +(res{i}.y - res{j}.y).^2)];
        end
    end
    res{i}.allDistMean = mean(distances(~isnan(distances)));
    res{i}.allDistVar = var(distances(~isnan(distances)));
end
    
%%
for curr = 1:options.nSubjects
fprintf('# - tracking subject %d of %d\n', curr, options.nSubjects);
    currLogprobs = cents.logprob(:, :, curr);
    %% build observation probability table
    table = ones(options.nSubjects+obj.ROI.nHidden+1, length(res{curr}.id)) * -inf;
    scoord.x = zeros(options.nSubjects, length(res{curr}.id));
    scoord.y = zeros(options.nSubjects, length(res{curr}.id));
    scoord.hidingDistance = zeros(options.nSubjects, length(res{curr}.id), obj.ROI.nHidden);
    path = zeros(size(table), 'uint16');
    nchar = 0;
    fprintf('# - computing emission probabilities\n');
    for s=1:options.nSubjects
        range = 1:ndata;
        
        range = range(res{s}.valid);
        centids = res{s}.centids(res{s}.valid);
        
        table(s, range) = currLogprobs(sub2ind(size(currLogprobs), range, centids));
        scoord.x(s, range) = res{s}.x(res{s}.valid);
        scoord.y(s, range) = res{s}.y(res{s}.valid);
        for h=1:obj.ROI.nHidden
            nchar = Reprintf(nchar, '#     . processing segment %d / %d', ((s - 1) * obj.ROI.nHidden) + h, options.nSubjects*obj.ROI.nHidden);
            d = pdist2([scoord.x(s, range)', scoord.y(s, range)'], obj.ROI.HiddenBoundarySCoordinates{h});
            scoord.hidingDistance(s, range, h) = min(d, [], 2)';
        end
    end
    fprintf('\n');
    
    for s=1:obj.ROI.nHidden+1
        table(options.nSubjects+s, :) = flog(prod(1 - exp(table(1:options.nSubjects, :)), 1));
    end 
    emitlogprobs = table;
    
    valid = sum(isfinite(table(1:options.nSubjects, :)), 1) > 0;
    d = diff([0 valid 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;

    %%
    prev = 1;
    nchar = 0;
    
    scoord.hidingPos.x = ones(1, length(res{curr}.id)) * inf;
    scoord.hidingPos.y = ones(1, length(res{curr}.id)) * inf;
    
    %%zeroprob = gppdf(0, res{curr}.pareto(2), res{curr}.pareto(1));
    zeroprob = exppdf(0, res{curr}.meanDistance);
    %zeroprob = gppdf(0, res{curr}.pareto(2), res{curr}.pareto(1));
    zeroprob = flog(zeroprob ./ (gauss(res{curr}.allDistMean, res{curr}.allDistVar, 0) + zeroprob));
    zeroprobvec = [repmat(zeroprob, options.nSubjects, 1); zeros(obj.ROI.nHidden+1, 1)];
    hiddenprobvec = [repmat(zeroprob, options.nSubjects, 1); zeros(obj.ROI.nHidden, 1); zeroprob];
    
    fprintf('# - building viterbi table\n');
    for n=1:length(start)
        nchar = Reprintf(nchar, '#     . processing segment %d / %d', n, length(start));
        r = start(n):finish(n);
        for f=r
            %
            for s=1:options.nSubjects
                if ~isfinite(table(s, f))
                    continue;
                end
                x = res{s}.x(f);
                y = res{s}.y(f);
                
                distance = scoord.hidingDistance(s, f, :);
                distance = [...
                    sqrt((x - scoord.x(:, prev)).^2 + (y - scoord.y(:, prev)).^2); ...
                    distance(:);...
                    sqrt((x - scoord.hidingPos.x(prev)).^2 + (y - scoord.hidingPos.y(prev)).^2)];
                %prob = gppdf(distance, res{curr}.pareto(2), res{curr}.pareto(1));
                prob = exppdf(distance, res{curr}.meanDistance);

                trans = flog(prob ./ (gauss(res{curr}.allDistMean, res{curr}.allDistVar, distance) + prob));
                if f>prev+1
                    trans = trans + zeroprobvec * (f - prev - 1);
                end
                
                [val, from] = max(table(:, prev) + trans);
                table(s, f) = table(s, f) + val;
                path(s, f) = from;
            end
            %
            for h=1:obj.ROI.nHidden
                distance = [scoord.hidingDistance(:, prev, h); obj.ROI.HiddenSDistances(:, h); inf];
%                prob = gppdf(distance, res{curr}.pareto(2), res{curr}.pareto(1));
                prob = exppdf(distance, res{curr}.meanDistance);
                trans = flog(prob ./ (gauss(res{curr}.allDistMean, res{curr}.allDistVar, distance) + prob));

                [val, from] = max(table(:, prev) + trans);
                table(options.nSubjects + h, f) = table(options.nSubjects + h, f) + val;
                path(options.nSubjects + h, f) = from;
            end
            %
            [val, from] = max(table(:, prev) + hiddenprobvec);
            table(options.nSubjects + obj.ROI.nHidden + 1, f) = table(options.nSubjects + obj.ROI.nHidden + 1, f) + val;
            path(options.nSubjects + obj.ROI.nHidden + 1, f) = from;
            if from <= options.nSubjects
                scoord.hidingPos.x(f) = scoord.x(from, prev);
                scoord.hidingPos.y(f) = scoord.y(from, prev);
            else
                scoord.hidingPos.x(f) = scoord.hidingPos.x(prev);
                scoord.hidingPos.y(f) = scoord.hidingPos.y(prev);
            end
            %
            prev = f;
        end
    end
    fprintf('\n');
    %% backtrack
    fprintf('# - backtracking\n');
    track{curr}.src = ones(1, size(table, 2)) * (options.nSubjects + 1);
    track{curr}.logprob = zeros(1, size(table, 2));
    track{curr}.emitlogprob = zeros(1, size(table, 2));
    track{curr}.x = zeros(1, size(table, 2));
    track{curr}.y = zeros(1, size(table, 2));

    for n=length(start):-1:1
        r = start(n):finish(n);
        f=finish(n);
        if n<length(start)
            idx = path(track{curr}.src(start(n+1)), start(n+1));
            
            track{curr}.src(finish(n)) = idx;
            track{curr}.logprob(f) = table(idx, f);
            track{curr}.emitlogprob(f) = emitlogprobs(idx, f);
        else
            [track{curr}.logprob(finish(n)), track{curr}.src(finish(n))] = max(table(:, finish(n)));
            track{curr}.emitlogprob(finish(n)) = emitlogprobs(track{curr}.src(finish(n)), finish(n));
        end
        for f=finish(n)-1:-1:start(n)
            idx = path(track{curr}.src(f+1), f+1);
            
            track{curr}.src(f) = idx;
            track{curr}.logprob(f) = table(idx, f);
            track{curr}.emitlogprob(f) = emitlogprobs(idx, f);
        end
        if n>1
            track{curr}.logprob(finish(n-1)+1:start(n)-1) = track{curr}.logprob(start(n));
            track{curr}.emitlogprob(finish(n-1)+1:start(n)-1) = track{curr}.emitlogprob(start(n));
        end
    end
    
    fprintf('# - computing track properties\n');
    track{curr}.valid = track{curr}.src <= options.nSubjects;
    track{curr}.id = track{curr}.valid * curr;
    
    range = 1:length(track{curr}.valid);
    range = range(track{curr}.valid);
    
    src = track{curr}.src(track{curr}.valid);
    
    track{curr}.x = zeros(1, length(track{curr}.valid));
    track{curr}.x(track{curr}.valid) = scoord.x(sub2ind(size(scoord.x), src, range));
    track{curr}.x(~track{curr}.valid) = nan;
    
    track{curr}.y = zeros(1, length(track{curr}.valid));
    track{curr}.y(track{curr}.valid) = scoord.y(sub2ind(size(scoord.y), src, range));
    track{curr}.y(~track{curr}.valid) = nan;
end
% %%
if 1==2
    filtered = track;
    for curr=1:options.nSubjects
        start = [];
        finish = [];
        for j=1:options.nSubjects+1
            valid = track{curr}.src == j;
            d = diff([0 valid 0], 1, 2);
            start  = [start, find(d > 0)];
            finish = [finish, find(d < 0) - 1];
        end
        for n=1:length(start)
            r = start(n):finish(n);
            post = min(finish(n)+1, length(track{curr}.src));
            prob = track{curr}.logprob(start(n):post) - track{curr}.emitlogprob(post);
            pcurr = exp(prob / sum(track{curr}.valid(r)));
            pother = 0;
            for s=1:options.nSubjects
                if s~=curr
                    len = sum(track{s}.valid(r));
                    if len > 0
                        prob = track{s}.logprob(start(n):post) - track{s}.emitlogprob(post);
                        prob = prob / len;
                        pother = pother + (1 - exp(prob));
                    end
                end
            end
            r = pcurr / pother * (options.nSubjects - 1);
            if r < 1
                filtered{curr}.x(r) = nan;
                filtered{curr}.y(r) = nan;
                filtered{curr}.valid(r) = false;
                filtered{curr}.id(r) = 0;
            end
        end
    end
end
%%
%pres = res;
res = track;
%
fprintf('# intepolating short epochs\n');
for i=1:options.nSubjects
    d = diff([0 res{i}.id == 0 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    len = finish - start + 1;
    valid = len < options.MaxHiddenDuration;
    start = start(valid);
    finish = finish(valid);
    for j=1:length(start)
        if start(j) <= 1 || finish(j) > nframes
            continue;
        end
        r = start(j):finish(j);
%        [res{i}.x(r), res{i}.y(r)] = MotionInterp(res{i}.x, res{i}.y, r, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
        [res{i}.x(r), res{i}.y(r)] = MotionInterp(res{i}.x, res{i}.y, r, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
%             if finish(1) < nFrames
%                 res{i}.x(r) = res{i}.x(finish(j)+1);
%                 res{i}.y(r) = res{i}.y(finish(j)+1);
%             end
%        end
    end
end

for i=1:options.nSubjects
    d = diff([0 res{i}.id == 0 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    for j=1:length(start)
        r = start(j):finish(j);
        if start(j) > 1
            res{i}.x(r) = res{i}.x(start(j)-1);
            res{i}.y(r) = res{i}.y(start(j)-1);
        else
%             if finish(1) < nFrames
%                 res{i}.x(r) = res{i}.x(finish(j)+1);
%                 res{i}.y(r) = res{i}.y(finish(j)+1);
%             end
        end
    end
end


filename = [options.output_path options.test.name '.simple-track.mat'];
save(filename, 'res');



% fprintf('# - interpolate position\n');
% for curr = 1:options.nSubjects;
%     hidden = res{curr}.id == 0;
%     d = diff([0 hidden 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     loc = find(start > 1 & finish < length(hidden));
%     for l=loc
%         range = start(l):finish(l);
%         %         res{curr}.x(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.x([start(l)-1, finish(l)+1]), start(l):finish(l));
%         %         res{curr}.y(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.y([start(l)-1, finish(l)+1]), start(l):finish(l));
%         [res{curr}.x(range), res{curr}.y(range)] = MotionInterp(res{curr}.x, res{curr}.y, range, [xyloObj.Width * options.scale, xyloObj.Height * options.scale]);
%     end
%     if length(finish) > 0 && finish(end) == length(hidden) && start(end) > 1
%         res{curr}.x(start(end):finish(end)) = res{curr}.x(start(end)-1);
%         res{curr}.y(start(end):finish(end)) = res{curr}.y(start(end)-1);
%     end
% end

filename = [options.output_path options.test.name '.track.mat'];
save(filename, 'res');

