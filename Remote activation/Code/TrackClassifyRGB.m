%options.minRadius = 5;

options.output = true;

options.nSubjects = 4;
options.colorFrames = 100;
options.colorBins = 20;
options.searchZones = 10^2;
options.minRadius = 2;


prevProps = [];
blobs = gmm(2, options.nSubjects, 'diag');
sedisk = strel('disk',options.minRadius);


%
nFrames = xyloObj.NumberOfFrames;
nFrames = 1000;
locations.x = zeros(options.nSubjects, nFrames);
locations.y = zeros(options.nSubjects, nFrames);
hidden    = zeros(options.nSubjects, nFrames);
smallBkgFrame = imresize(bkgFrame, .25);
fprintf('# frame %6d/%6d (%6.2fxRT)', 0, nFrames, 1);
%%
tic
for r=1:nFrames
    RT = toc / r * xyloObj.FrameRate;
    fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b%6d/%6d (%6.2fxRT)', r, nFrames, 1/RT);
    img = read(xyloObj, r);
    m = imresize(img, .25);
    m = imsubtract(m, smallBkgFrame);
    dm = im2double(m);
    m = uint8(dm > options.noiseThresh * std(dm(:))) .* m;
    if r==1
        [posx, posy] = meshgrid(1:size(m,1), 1:size(m,2));
    end
    %%
    if options.output
        subplot(2,2,1);
        imagesc(m);
    end
    %%
    %hsv_m = rgb2hsv(m);
    rm = dm(:,:,1);
    gm = dm(:,:,2);
    bm = dm(:,:,3);
    %%
    bw = sum(m > 0, 3);
    bw = bwareaopen(bw, options.noiseSize);
    %bw = imerode(bw, strel('disk',5));
    labels = bwlabel(bw);
    %nobjects = max(labels(:));
    if options.output
        subplot(2,2,3);
        rgblbls = label2rgb(labels);
        imshow(rgblbls);
    end
    
    %%
    [b, idx_r] = histc(rm(bw), subjectColorBins);
    [b, idx_g] = histc(gm(bw), subjectColorBins);
    [b, idx_b] = histc(bm(bw), subjectColorBins);
    prob_r = zeros(options.nSubjects, length(idx_r));
    prob_g = zeros(options.nSubjects, length(idx_g));
    prob_b = zeros(options.nSubjects, length(idx_b));
    for i=1:options.nSubjects
        prob_r(i, :) = subjectColors_r(i, idx_r);
        prob_g(i, :) = subjectColors_g(i, idx_g);
        prob_b(i, :) = subjectColors_b(i, idx_b);
    end
    prob_r = prob_r ./ repmat(sum(prob_r, 1), options.nSubjects, 1);
    prob_g = prob_g ./ repmat(sum(prob_g, 1), options.nSubjects, 1);
    prob_b = prob_b ./ repmat(sum(prob_b, 1), options.nSubjects, 1);
    joint_prob = prob_r .* prob_g .* prob_b;
    
    data = [posx(bw), posy(bw)];
    blob_prob = gmmpost(blobs, data);
    joint_prob = joint_prob.*blob_prob';
    [m, idx] = max(joint_prob, [], 1);
    nlabels = labels;
    nlabels(bw) = idx;
    
    for i=1:options.nSubjects
        cc = bwconncomp(nlabels == i);
        if cc.NumObjects == 0
            blobs.covars(i, :) = [1 1] * options.searchZones;
            hidden(i, r) = 1;
            continue;
        end
        max_blob = 0;
        max_blob_size = 0;
        for j=1:cc.NumObjects
            if length(cc.PixelIdxList{j}) > max_blob_size
                max_blob_size = length(cc.PixelIdxList{j});
                max_blob = j;
            end
        end
        nlabels(nlabels == i) = 0;
        
        %         map = false(size(nlabels));
        %         map(cc.PixelIdxList{j}) = 1;
        %         %
        %         nlabels(map) = i;
        map = nlabels * 0;
        map(cc.PixelIdxList{max_blob}) = 1;
        map = imopen(map, sedisk);
        
        %nlabels(cc.PixelIdxList{max_blob}) = i;
        nlabels(map > 0) = i;
        currdata = [posx(map > 0), posy(map > 0)];
        
        blobs.covars(i, :) = var(currdata);
        %blobs.covars(i, :) = var(data(idx == i, :));
        if any(isnan(blobs.covars(i, :)))
            blobs.covars(i, :) = [1 1] * options.searchZones;
            hidden(i, r) = 1;
        else
            %blobs.centres(i, :) = mean(data(idx == i, :));
            blobs.centres(i, :) = mean(currdata);
        end
    end
    locations.x(:, r) = blobs.centres(:,1);
    locations.y(:, r) = blobs.centres(:,2);
    
    %idx(m < 0.1) = 0;
    %[m, idx] = max(prob_v, [], 1);
    %%
    if options.output
        subplot(2,2,4);
        rgblbls = label2rgb(nlabels);
        imshow(rgblbls);
    end
    %     %%
    %     clabels = labels;
    %     for i=1:nobjects
    %         [v, idx] = max(sum(prob(:, labels(bw) == i), 2));
    %         clabels(labels == i) = idx;
    %     end
    %     %%
    %     subplot(3,2,5);
    %     rgblbls = label2rgb(clabels);
    %     imshow(rgblbls);
    %
    % %     [xpos, ypos] = find(bw);
    % %     pos = labels * 0;
    % %     for i=1:options.nSubjects
    % %         sprob = sum(prob(i, :));
    % %         xm(i) = (prob(i, :) * xpos) ./ sprob;
    % %         ym(i) = (prob(i, :) * ypos) ./ sprob;
    % %     end
    % %     plot(ym, xm, 'x');
    % %     axis([0 size(labels, 2) 0 size(labels, 1)]);
    % %     set(gca, 'YDir', 'rev')
    %%
    drawnow
end
fprintf('\n');
toc
