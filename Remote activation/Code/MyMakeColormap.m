function cmap = MyMakeColormap(varargin)
if isscalar(varargin{end})
    nLevels = varargin{end};
    varargin = {varargin{1:end-1}};
else
    nLevels = 256;
end

offset = 0;
prev = varargin{1};
cmap = zeros(nLevels, 3);
for i=2:length(varargin)
    curr = varargin{i};
    step = round(nLevels / (length(varargin) - 1));
    if i == length(varargin)
        step = nLevels - offset;
    end
    b = -3;
    s = 1 - exp(b * sequence(0, 1, step));
%    s = sequence(0, 1, step);
    for j=1:3
        cmap(offset+1:offset+step, j) = prev(j) + s * (curr(j) - prev(j));
    end
    prev = curr;
    offset = offset + step;
end