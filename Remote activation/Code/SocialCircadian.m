function [obj, DJS, jointProbs] = SocialCircadian(obj)
obj = TrackLoad(obj);
%obj.CircadianWindow = obj.FrameRate * 60 * 60 * 2; % 1 hour
obj.CircadianWindow = [0 0 0 1 0 0];
DayStart = [10 0 0];
validate = true;
%%
obj.RecordingEnd = datevec(GetFileCreationDate(obj.VideoFile));
obj.RecordingDuration = datevec(num2str(obj.nFrames / obj.FrameRate), 'SS');
obj.RecordingDuration(1:3) = 0;
obj.RecordingStart = datevec(datenum(obj.RecordingEnd) - datenum(obj.RecordingDuration));
obj.DayStart = obj.RecordingStart;
if obj.StartTime > 10
    DayStart = datevec(addtodate(datenum(obj.RecordingStart), floor(obj.StartTime), 'second'));
    DayStart = DayStart(4:6);
end
obj.DayStart(4:6) = DayStart;

%%
IndepProbs = [];
%%
for order = [3];
    fprintf(['# Circadian statistics of order ' num2str(order) ':\n']);
    currTime = obj.DayStart;
    count = 0;
    validation = struct;
    jointProbs = {};
    indepProbs = {};
    while true
        prevTime = currTime;
        currTime = datevec(datenum(currTime) + datenum(obj.CircadianWindow));
        if datenum(currTime) > datenum(obj.RecordingEnd)
            if datenum(currTime) - datenum(obj.RecordingEnd) < datenum(obj.CircadianWindow) / 2
                currTime = obj.RecordingEnd;
                count = count + 1;
            else
                break;
            end
        else
            if datenum(prevTime) - datenum(obj.RecordingStart) < 0
                if datenum(prevTime) < datenum(obj.DayStart)
                    continue;
                else
                    count = count + 1;
                    jointProbs{count} = nan;
                    continue;
                end
            else
                count = count + 1;
            end
            %         count = count + 1;
            %         if datenum(prevTime) - datenum(obj.RecordingStart) < 0
            %             if (datenum(obj.RecordingStart) - datenum(prevTime) > datenum(obj.CircadianWindow) / 2)
            %                 jointProbs{count} = nan;
            %                 continue;
            %             else
            %                 prevTime = obj.RecordingStart;
            %             end
            %         end
        end
        startTime = datevec(datenum(prevTime) - datenum(obj.RecordingStart));
        startFrame = (startTime(4) * 3600 + startTime(5) * 60 + startTime(6)) * obj.FrameRate + 1;
        endTime = datevec(datenum(currTime) - datenum(obj.RecordingStart));
        endFrame = (endTime(4) * 3600 + endTime(5) * 60 + endTime(6)) * obj.FrameRate;
        fprintf(['# - (%2d) times: ' datestr(prevTime, 13) '-' datestr(currTime, 13) ' [%7d, %7d]\n'], count, startFrame, endFrame);
        
        r = startFrame:endFrame;
        curr = obj;
        curr.Output = false;
        curr.OutputToFile = false;
        curr.OutputInOldFormat = false;
        curr.zones = obj.zones(:, r);
        curr.valid = obj.valid(:, r);
        if order ~= 0
            curr = SocialPotts(curr, struct('order', order));
            JointProbs = curr.Analysis.Potts.Model{order}.prob;
        else
            [curr, IndepProbs, JointProbs] = SocialComputePatternProbs(curr, false);
        end
        %     if sum(curr.valid) == 0
        %         JointProbs = nan;
        %     else
        %         [curr, JointProbs] = SocialComputePatternPairwiseProbs(curr);
        %     end
        if validate
            %%
            for i=1:3
                if i==1
                    even = mod(floor([1:length(r)] * obj.dt), 2) == 0;
                elseif i==2
                    even = mod(floor([1:length(r)] * obj.dt / 60), 2) == 0;
                else
                    even = [1:length(r)] < length(r) / 2;
                end
                
                v1 = obj;
                v1.zones = obj.zones(:, r(even));
                v1.valid = obj.valid(:, r(even));
                v1.Output = false;
                v1.OutputToFile = false;
                v1.OutputInOldFormat = false;
                if order ~= 0
                    v1 = SocialPotts(v1, struct('order', order));
                    JointProbsV1 = v1.Analysis.Potts.Model{order}.prob;
                else
                    [q, IndepProbsV1, JointProbsV1] = SocialComputePatternProbs(v1, false);
                end
                %
                v2 = obj;
                v2.zones = obj.zones(:, r(~even));
                v2.valid = obj.valid(:, r(~even));
                v2.Output = false;
                v2.OutputToFile = false;
                v2.OutputInOldFormat = false;
                if order ~= 0
                    v2 = SocialPotts(v2, struct('order', order));
                    JointProbsV2 = v2.Analysis.Potts.Model{order}.prob;
                else
                    [q, IndepProbsV2, JointProbsV2] = SocialComputePatternProbs(v2, false);
                end
                if i==1
                    validation.InterlacedSec.DJS(count) = JensenShannonDivergence(JointProbsV2(:)', JointProbsV1(:)');
                elseif i==2
                    validation.InterlacedMin.DJS(count) = JensenShannonDivergence(JointProbsV2(:)', JointProbsV1(:)');
                else
                    validation.InterlacedHalves.DJS(count) = JensenShannonDivergence(JointProbsV2(:)', JointProbsV1(:)');
                end
            end
        end
        
        
        jointProbs{count} = JointProbs;
        indepProbs{count} = IndepProbs;
    end
    
    %%
    DJS = zeros(length(jointProbs));
    indepDJS = zeros(length(jointProbs));
    for i=1:length(jointProbs)
        for j=i+1:length(jointProbs)
            if all(isnan(jointProbs{i})) || all(isnan(jointProbs{j}))
                DJS(i, j) = nan;
                DJS(j, i) = nan;
                indepDJS(i, j) = nan;
                indepDJS(j, i) = nan;
            else
                DJS(i, j) = JensenShannonDivergence(jointProbs{i}(:)', jointProbs{j}(:)');
                DJS(j, i) = DJS(i, j);
                indepDJS(i, j) = JensenShannonDivergence(indepProbs{i}(:)', indepProbs{j}(:)');
                indepDJS(j, i) = indepDJS(i, j);
            end
        end
    end
    %
    % if nargout < 2
    %     imagesc(DJS);
    % end
    Circadian.Validations = validation;
    Circadian.IndependentDjs = indepDJS;
    Circadian.Djs = DJS;
    Circadian.JointProbs = jointProbs;
    Circadian.IndepProbs = indepProbs;
    if order == 0
        obj.Circadian = Circadian;
    else
        obj.Circadian.Potts{order} = Circadian;
    end
end

if obj.OutputToFile
    fprintf('# - saving data\n');
    TrackSave(obj);
end
