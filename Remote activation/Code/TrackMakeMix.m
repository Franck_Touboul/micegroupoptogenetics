SocialExperimentData;
idx = GroupsData.Standard.idx;
resrv = zeros(length(idx), GroupsData.nSubjects);
for i=1:length(idx)
    resrv(i, :) = randperm(GroupsData.nSubjects);
end
avail = true(1, length(idx));
mix.group = zeros(length(idx), GroupsData.nSubjects);
mix.subj  = zeros(length(idx), GroupsData.nSubjects);
for i=1:length(idx)
    r = randperm(length(idx));
    r = r(find(avail(r), GroupsData.nSubjects))
    mix.group(i, :) = r;
    %%
    for s=1:GroupsData.nSubjects
        j = find(resrv(r(s), :), 1);
        mix.subj(i, s) = resrv(r(s), j);
        resrv(r(s), j) = 0;
        avail(r(s)) = any(resrv(r(s), :));
    end
end
%%
for day=[1 3 4]
for i=1:length(idx)
    for s=1:GroupsData.nSubjects
        id = idx(mix.group(i, s));
        obj = TrackLoad({id day});
        %%
        if s == 1
            curr = obj;
            curr.Analysis = [];
            curr.Hierarchy = [];
            curr.Interactions = [];
            curr.Colors = [];
            curr.Contacts = [];        
            curr.Information = [];
            curr.x = [];
            curr.y = [];
            curr.sheltered = [];
            curr.Circadian = [];
            curr.BkgImage = [];
            curr.speed = [];
            curr.speed_cm2sec = [];
            curr.speedCMperSec = [];
            curr.hidden = [];
            curr.FilePrefix = sprintf('Mix.exp%04d.day%02d.cam00', i, day);
        end
        if obj.nFrames < curr.nFrames
            curr.zones = curr.zones(:, 1:obj.nFrames);
            curr.valid = curr.valid(1:obj.nFrames);
            curr.nFrames = obj.nFrames;
        end
        curr.zones(s, :) = obj.zones(mix.subj(i, s), 1:curr.nFrames);
        curr.valid = curr.valid & obj.valid(1:curr.nFrames);
    end
    TrackSave(curr);
end
end