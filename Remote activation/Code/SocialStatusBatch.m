SocialExperimentData;
data = struct('pChase', [], 'pEscape', [], 'socialStatus', [], 'group', [], 'id', [], 'nContacts', [], 'nChasese', [], 'nEscapes', []);
for id=1:GroupsData.nExperiments
    obj = TrackLoad({id 2}, {'Hierarchy'});
    %%
    H = obj.Hierarchy.Group.AggressiveChase;
    data.nContacts = [data.nContacts sum(H.Contacts, 1)];
    data.nChasese = [data.nChasese sum(H.ChaseEscape, 2)'];
    data.nEscapes = [data.nEscapes sum(H.ChaseEscape, 1)];
    data.pChase  = [data.pChase, sum(H.ChaseEscape, 2)' ./ sum(H.Contacts, 2)'];
    data.pEscape = [data.pEscape, sum(H.ChaseEscape, 1) ./ sum(H.Contacts, 1)];
    data.socialStatus = [data.socialStatus, max(H.rank) - H.rank + 1];
    data.group = [data.group, H.rank*0 + GroupsData.group(id)];
    data.id = [data.id, H.rank*0 + id];
end
%%
stat = struct('pChase', []);
for i=1:obj.nSubjects
    for g=1:GroupsData.nGroups
        map = data.socialStatus == i & data.group == g;
        stat.pChase.m(i, g) = mean(data.pChase(map) * 100);
        stat.pChase.se(i, g) = stderr(data.pChase(map) * 100);
        stat.pEscape.m(i, g) = mean(data.pEscape(map) * 100);
        stat.pEscape.se(i, g) = stderr(data.pEscape(map) * 100);
    end
end
subplot(2,1,1);
barweb(stat.pChase.m, stat.pChase.se)
set(gca,'XTickLabel', {'\alpha', '\beta', '\gamma', '\delta'});
xlabel('social rank');
ylabel('% chase');
subplot(2,1,2);
barweb(stat.pEscape.m, stat.pEscape.se);
set(gca,'XTickLabel', {'\alpha', '\beta', '\gamma', '\delta'});
xlabel('social rank');
ylabel('% escape');
