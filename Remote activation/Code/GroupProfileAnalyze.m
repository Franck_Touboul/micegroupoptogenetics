Families = {'PTSD.new', '5mice.new', 'SC.new'};
Groups = cell(1, length(Families));
for i=1:length(Families)
    curr = load(['../' Families{i} '/Group.mat']);
    Groups{i} = curr.Group;
end
%%
clf
cmap = MyCategoricalColormap;
gidx = 1;
Entries = {};
for i=1:length(Groups)
    %%
    curr = Groups{i};
    for g=unique(curr.FamilyID)
        map = curr.FamilyID == g;
        % I2/In vs. Entropy
        subplot(2,2,1);
        y = curr.Table(map, 2) ./ curr.Table(map, 1);
        x = curr.Table(map, 4);
        hon;
        plot(x, y, 'o', 'MarkerFaceColor', cmap(gidx, :), 'MarkerEdgeColor', 'none');
        xlabel('Entropy');
        ylabel('I2/In');
        % I2/In vs. In
        subplot(2,2,2);
        y = curr.Table(map, 2) ./ curr.Table(map, 1);
        x = curr.Table(map, 1);
        hon;
        plot(x, y, 'o', 'MarkerFaceColor', cmap(gidx, :), 'MarkerEdgeColor', 'none');
        xlabel('In');
        ylabel('I2/In');
        % (I2+I3)/Entropy vs. In
        subplot(2,2,3);
        y = (curr.Table(map, 2) + curr.Table(map, 3)) ./ curr.Table(map, 1);
        x = curr.Table(map, 4);
        hon;
        plot(x, y, 'o', 'MarkerFaceColor', cmap(gidx, :), 'MarkerEdgeColor', 'none');
        xlabel('Entropy');
        ylabel('(I2+I3)/In');
        % (I2+I3)/In vs. In
        subplot(2,2,4);
        y = (curr.Table(map, 2) + curr.Table(map, 3)) ./ curr.Table(map, 1);
        x = curr.Table(map, 1);
        hon;
        plot(x, y, 'o', 'MarkerFaceColor', cmap(gidx, :), 'MarkerEdgeColor', 'none');
        xlabel('In');
        ylabel('(I2+I3)/In');
   
        %
        Entries{gidx} = curr.FamilyNames{g};
        gidx = gidx + 1;
    end
end
subplot(2,2,4);
legend(Entries);
legend boxoff