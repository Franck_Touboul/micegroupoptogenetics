function res = PottsModelExpand(obj, me)
res.subjectmap = false(obj.nSubjects, length(me.weights));
res.zonemap = false(obj.nSubjects * obj.ROI.nZones, length(me.weights));
res.zones = zeros(obj.nSubjects, length(me.weights));
for i=1:length(me.weights)
    idx = 1;
    for j=me.labels{i}(1, :)
        res.subjectmap(j, i) = true;
        res.zonemap((j - 1) * obj.ROI.nZones + me.labels{i}(2, idx), i) = true;
        res.zones(j, i) = me.labels{i}(2, idx);
        idx = idx + 1;
    end
end
vec = 2.^(0:obj.nSubjects * obj.ROI.nZones-1);
res.zonecode = res.zonemap' * vec';
