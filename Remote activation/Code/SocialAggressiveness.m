function obj = SocialAggressiveness(obj)

%%


if isdesktop
    try
        data = dlmread(['aux/' obj.FilePrefix '.marks'], '\t', [1 0 500 15]);
        amarks.ids = data(:, 1);
        amarks.beg = data(:, 2);
        amarks.end = data(:, 3);
        amarks.subjects = data(:, 4:5);
        amarks.approach = data(:, 6:7);
        amarks.leave = data(:, 8:9);
        amarks.chase = data(:, 10);
        amarks.pred = data(:, 11:12);
        amarks.prey = data(:, 13:14);
        amarks.artifact = data(:, 15);
        amarks.agresivness = data(:, 16);
        amarks.chase = amarks.agresivness > 1;
        %
        [results, match] = SocialBehaviorAnalysisScore(local, amarks);
        
        fprintf('# detection=%.1f%% (%d), false-alarm=%.1f%% (%d)\n', results.chase(1) / (results.chase(1) + results.chase(3)) * 100, results.chase(1), results.chase(2) / results.chase(5) * 100, results.chase(2));
    end
end