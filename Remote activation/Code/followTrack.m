function res = followTrack(options, cents, curr)

[N, nS] = size(cents.label);
dim = sum(cents.label ~= 0, 2);

%%
options.jumpVar = eye(2) * 5^2;
options.confProb = 0.1;
options.hideProb = nan;

table = zeros(N, nS+1);
table(cents.label == curr) = 1;
table(cents.label ~= curr) = options.confProb;
table(cents.label == 0)    = nan;
table(:, nS+1) = options.hideProb;
table = flog(table');

path = zeros(nS+1, N, 'uint8');
lastPos = [inf, inf];
prevDprob = zeros(1,nS+1);
for i=2:N
    col = [table(1:nS, i-1); table(nS+1, i-1)];
    col = table(:, i-1);
    dprob = zeros(1, nS + 1);
    for j=1:nS
%        p = [gauss([cents.x(i, j), cents.y(i, j)], options.jumpVar, [cents.x(i-1, 1:nS); cents.y(i-1, 1:nS)]'); 1];
        vec = [cents.x(i-1, :) lastPos(1); cents.y(i-1, :) lastPos(2)];
        p = gauss([cents.x(i, j), cents.y(i, j)], options.jumpVar, vec');
        [val, from] = max(flog(p) + col);
        if from <= nS
            path(j, i) = from;
        else
            path(j, i) = nS + 1;
        end
        table(j ,i) = val + table(j ,i);
        dprob(j) = val - col(from);
    end
    [val, from] = max(col);
    if from <= nS
        path(nS + 1, i) = from;
        lastPos = [cents.x(i-1, from), cents.y(i-1, from)];
    else
        path(nS + 1, i) = nS + 1;
    end
    table(nS + 1, i) = val + table(nS + 1 ,i);
    dprob(end) = val - col(from);
    prevDprob = dprob;
end

% backtrack 
backtrack = zeros(1, N);
[val, start] = max(table(:, end));
backtrack(end) = start;
res.x = zeros(1, N);
res.y = zeros(1, N);

if backtrack(end) <= nS
    res.x(end) = cents.x(end, backtrack(end));
    res.y(end) = cents.y(end, backtrack(end));
else
    res.x(end) = nan;
    res.y(end) = nan;
end
for i=N-1:-1:1
    backtrack(i) = path(backtrack(i+1), i + 1);
    if backtrack(i) <= nS
        res.x(i) = cents.x(i, backtrack(i));
        res.y(i) = cents.y(i, backtrack(i));
    else
        res.x(i) = nan;
        res.y(i) = nan;
    end
end

