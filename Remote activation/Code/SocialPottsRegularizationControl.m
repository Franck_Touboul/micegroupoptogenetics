function SocialPottsRegularizationControl(obj, options)
needToTrain = false;

trainobj = obj;
testobj = obj;

if ~exist('options', 'var')
    options = struct();
end

options = setDefaultParameters(options, ...
    'nIters', [500 1000 1000], ...
    'confidence', 0.05,...
    'beta', 0.01 ...
    );

permutations = struct();
permutations.beta = .5 .^ (-3:12);

taskid = GetTaskID;
if ~isempty(taskid)
    options.beta = permutations.beta(taskid);
end

me = {};
idx = 1;

%%
data = zeros(trainobj.nSubjects, sum(trainobj.valid));
for s=1:trainobj.nSubjects
    currZones = trainobj.zones(s, :);
    data(s, :) = currZones(trainobj.valid == 1) - 1;
end
%%
if needToTrain
    for level=options.order
        fprintf('#    - beta %d\n', 0);
        fprintf('#      . level %d\n', level);
        local = trainobj;
        local.n = level;
        local.output = true;
        local.count = trainobj.ROI.nZones;
        local.nIters = options.nIters(1);
        local.beta = 0;
        local.MinNumberOfIters = 0;
        local.KLConvergenceRatio = 0;
        if local.nIters > 0
            me{idx} = trainPottsModelUsingGIS(local, data, options.confidence);
            local.initialPottsWeights = me{idx}.weights;
        end
        %%
        local.nIters = options.nIters(2);
        local = rmfield(local, 'KLConvergenceRatio');
        %    me{idx} = trainPottsModelWithRegularization(local, data, options.confidence);
        me{idx} = trainPottsModelUsingNesterovGD(local, data, options.confidence);
        idx = idx + 1;
        
    end
    %%
    for level=1:4
        p = exp(me{level}.perms * me{level}.weights');
        perms_p = exp(me{level}.perms * me{level}.weights');
        Z = sum(perms_p);
        p = p / Z;
        me{level}.prob = p;
    end
else
    me = obj.Analysis.Potts.Model;
    idx = length(me) + 1;
end
%%
for level=[3 1 2 4]
    %%
    fprintf('#    - beta %d\n', options.beta);
    fprintf('#      . level %d\n', level);
    local = trainobj;
    local.n = level;
    local.beta = options.beta;
    local.output = true;
    local.count = trainobj.ROI.nZones;
    local.nIters = options.nIters(3);
    local.MinNumberOfIters = 0;
    local.initialPottsWeights = me{level}.weights;
    local.KLConvergenceRatio = 1e-3;
    me{idx} = trainPottsModelWithRegularization(local, data, options.confidence);
    
    p = exp(me{idx}.perms * me{idx}.weights');
    Z = sum(p);
    p = p / Z;
    me{idx}.prob = p;
    
    idx = idx + 1;
end
%%
[trainobj, independentProbs, jointProbs] = SocialComputePatternProbs(trainobj, false);
[testobj, q, testProbs] = SocialComputePatternProbs(testobj, false);

model.me = me;
model.jointProbs = jointProbs;
model.testProbs = testProbs;
model.beta = options.beta;

if ~isempty(taskid)
    path = 'Potts/SocialPottsRegularizationControl/';
    mkdir(path);
    save([path obj.FilePrefix '.taskid' num2str(taskid, '%05d')]);
end