sFrame = 10000;
data = mmread('Exp1 day 3.mpg', sFrame);
nFrames = abs(data.nrFramesTotal);
tic;
index = 1;
for r=10000:20000
    curr = mmread('Exp1 day 3.mpg', (r-1)*25+1:r*25);
    RT = toc / (index * 25) * data.rate;
    fprintf('# - frame %6d/%6d (%6.2fxiRT)\n', r, nFrames, RT);
    if index == 1
        tic;
    end
    index = index + 1;
end
