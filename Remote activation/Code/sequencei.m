function s = sequencei(from, to, num)
s = round(from:(to-from)/(num-1):to);