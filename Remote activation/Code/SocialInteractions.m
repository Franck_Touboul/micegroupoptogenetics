options.nSubjects = 4; % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts
%
options.maxLag = 100;

%% Loading the Excel file for all mice (if needed)
loadData;
%%
if 1 == 2
    % show xcorr between the subjects in different zones
    m1 = 2;
    m2 = 3;
    for i=1:zones.count
        subplot(zones.count, 1, i);
        z1 = double(zones.all(m1, :) == (i-1));
        z1 = (z1 - mean(z1)) / std(z1);
        z2 = double(zones.all(m2, :) == (i-1));
        z2 = (z2 - mean(z2)) / std(z2);
        x = xcorr(z1, z2, options.maxLeg, 'unbiased');
        plot(-options.maxLeg:options.maxLag, x);
        prettyPlot(zones.labels{i}, 'Lag', 'Correlation');
        axis([-options.maxLeg, options.maxLeg, -0.1 0.1]);
    end
end
%%
x = zeros(options.nSubjects);
for m1=1:options.nSubjects
    for m2=m1+1:options.nSubjects
        for i=1:zones.count
            z1 = double(zones.all(m1, :) == (i-1));
            z2 = double(zones.all(m2, :) == (i-1));
            x(m1, m2) = x(m1, m2) + corr(z1', z2');
%            x(m1, m2) = max(abs(x(m1, m2)), abs(corr(z1', z2')));
        end
        x(m1, m2) = x(m1, m2) / zones.count;
    end
end
imagesc(x)
colorbar