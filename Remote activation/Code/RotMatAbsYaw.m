function m = RotMatAbsYaw(yaw, pitch, roll)

%m1 = RotMatRx(roll) * RotMatRy(pitch) * RotMatRz(yaw);
sy = sin(yaw);
cy = cos(yaw);
sp = sin(pitch);
cp = cos(pitch);
sr = sin(roll);
cr = cos(roll);
m = [cp*cy, -cp*sy, sp; ...
    cr*sy+cy*sp*sr, cr*cy-sp*sr*sy, -cp*sr; ...
    sr*sy-cr*cy*sp, cy*sr+cr*sp*sy, cp*cr];

%%
return;
r = sym('r');
p = sym('p');
y = sym('y');
m = RotMatRx(r) * RotMatRy(p) * RotMatRz(y)



