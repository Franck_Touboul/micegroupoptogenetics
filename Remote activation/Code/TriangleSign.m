function s = TriangleSign(px, py)
s = (px(:,1)-px(:,3)).*(py(:,2)-py(:,3)) - (px(:,2)-px(:,3)).*(py(:,1)-py(:,3));
