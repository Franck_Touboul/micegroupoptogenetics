function me = MEFeaturesMarkov(fulldata, nvals, n)
MEPrint('Computing features with markov time dependence\n');
options.NeutralValue = 1;
%%
%fulldata = [data(1:end-1, :), data(2:end, :)];
[ndata, dim] = size(fulldata);
me.nData = ndata;
me.Dim = dim;
me.nVals = nvals;
%%
me.SubjectPerms = nchoose(1:(dim/2));
me.SubjectPerms = me.SubjectPerms(cellfun(@length, me.SubjectPerms) <= n);
[me.ObservedPerms, I, J] = unique(fulldata, 'rows');
me.ObservedHist = histc(J, 1:size(me.ObservedPerms));
allPerms = myPerms(me.Dim, nvals);
me.InputPerms = me.ObservedPerms;
[d, I] = setdiff(allPerms, me.ObservedPerms, 'rows');
me.Valid = true(size(allPerms, 1), 1);
me.Valid(I) = false;
%%
me.nPatterns = sum(nvals .^ (2 * cellfun(@length, me.SubjectPerms)));
me.Patterns = nan(me.nPatterns, dim);
offset = 0;
for s=1:length(me.SubjectPerms)
    local = myPerms(2 * length(me.SubjectPerms{s}), nvals);
    sperms = [me.SubjectPerms{s}, me.SubjectPerms{s} + me.Dim/2];
    me.Patterns(offset+1:offset+size(local, 1), sperms) = local;
    offset = offset + size(local, 1);
end
%%
tic
me.Features = MEFindPatterns(me.ObservedPerms, me.Patterns);
toc
%%
me.PatternHist = MEPriors(me.ObservedPerms, me.Patterns, me.ObservedHist);
%%
Redundent = any(me.Patterns == options.NeutralValue, 2);
me.Patterns = me.Patterns(~Redundent, :);
me.Features = me.Features(:, ~Redundent);
me.PatternHist = me.PatternHist(~Redundent);
me.nPatterns = size(me.Patterns, 1);

