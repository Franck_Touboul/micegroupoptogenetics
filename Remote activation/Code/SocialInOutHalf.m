objs = cell(1, 18);
for id=1:18
    for d=1:16
        jmp = floor(curr.nFrames/4);
        if mod(d-1,4) == 0
            currobj = TrackLoad({id ceil(d/2)}, {'ROI', 'zones', 'nFrames', 'valid', 'Hierarchy'});
            curr = currobj;
            curr = SocialSetZones(curr, curr.zones(:, jmp*0+1:jmp));
        elseif mod(d-1,4) == 1
            curr = currobj;
            curr = SocialSetZones(curr, curr.zones(:, jmp*1+1:jmp*2));
        elseif mod(d-1,4) == 2
            curr = currobj;
            curr = SocialSetZones(curr, curr.zones(:, jmp*2+1:jmp*3));
        else
            curr = currobj;
            curr = SocialSetZones(curr, curr.zones(:, jmp*3+1:jmp*4));
        end
        if d == 1
            obj = currobj;
        end
        curr = SocialInOut(curr);
        obj.InOut.Days{d} = curr.InOut;
    end
    objs{id} = obj;
end
%%
res = [];
res.IOcorr = [];
res.CE = [];
res.Aggrs = [];
res.group = [];
stat = [];
for g=1:2
    stat(g).weights = [];
end
%%
for id=1:18
    %%        subplot(7,1,7);
    
    %obj = TrackLoad({id 4}); obj = SocialInOut(obj);
    obj = objs{id};
    %%
    clf;
    weights = [];
    for d=1:8
        me = obj.InOut.Days{d}.Model{3};
        data.subjs = false(obj.nSubjects, length(me.labels));
        for i=1:length(me.labels)
            data.subjs(me.labels{i}(1, :), i) = true;
        end
        data.order = sum(data.subjs);
        data.weights = zeros(obj.nSubjects);
        
        CE = obj.Hierarchy.Group.AggressiveChase.ChaseEscape;
        for s1=1:obj.nSubjects
            data.weights(s1, s1) = nan;
            CE(s1, s1) = nan;
            for s2=s1+1:obj.nSubjects
                data.weights(s1, s2) = mean(me.weights(data.subjs(s1, :) & data.subjs(s2, :) & data.order == 2));
                data.weights(s2, s1) = data.weights(s1, s2);
            end
        end
        subplot(7,8,d:8:40);
        SocialOverlayHierarchy(obj, triu(data.weights), struct('ColorEdges', false))
        subplot(7,8,40+d);
        bar(validmean(data.weights(:)), 'FaceColor', 'b');
        yaxis(-2, 2)
        xaxis(0, 2)
        weights(d) = validmean(data.weights(:));
    end
    stat(GroupsData.group(id)).weights = [stat(GroupsData.group(id)).weights; weights];
    
    %     subplot(7,4,1:4:20);
    %     SocialOverlayHierarchy(obj, obj.Hierarchy.Group.AggressiveChase.Days(2).ChaseEscape .* sign(data.weights), struct('ColorEdges', false))
    %     title('day 2');
    %     subplot(7,4,2:4:20);
    %     SocialOverlayHierarchy(obj, obj.Hierarchy.Group.AggressiveChase.Days(3).ChaseEscape .* sign(data.weights), struct('ColorEdges', false))
    %     title('day 3');
    %     subplot(7,4,3:4:20);
    %     SocialOverlayHierarchy(obj, obj.Hierarchy.Group.AggressiveChase.Days(4).ChaseEscape .* sign(data.weights), struct('ColorEdges', false))
    %     title('day 4');
    %     subplot(7,4,4:4:20);
    %     SocialOverlayHierarchy(obj, triu(data.weights), struct('ColorEdges', false))
    %%
%     for d=1:4
%         subplot(7,1,7);
%         imagesc(obj.InOut.Days{d}.zones);
%         colormap([1 1 1; .7 .7 .7]);
%         %SocialOverlayHierarchy(obj, data.weights )
%         %%
%         %saveFigure(['Graphs/InOut/' obj.FilePrefix '.' num2str(d)]);
%         axis off
%     end
    %%
    %saveFigure(['Graphs/InOut/' obj.FilePrefix]);
end


%%
clf
for id=1:18
    for d=1:4
        curr = TrackLoad({id d}, {'zones'});
        objs{id}.InOut.Days{d}.zones = curr.zones == 9;
        %
        %         %%
        %         subplot(7,1,7);
        %         imagesc(obj.zones == 9);
        %         colormap([1 1 1; .7 .7 .7]);
        %         axis off
        %         %%
        %         saveFigure(['Graphs/InOutMap/' curr.FilePrefix]);
    end
end