%function TrackGenerateMovie(obj)

VideoFile = ['../Movies/' obj.FilePrefix '.avi'];

%VideoFile = ['/Users/forkosh/Documents/Projects/Social Movies/test.mpg'];

%%

 

 

Style.Radios = 5;

Style.TrailDuration = 40;

Style.TrailDecay = 0.9;

Style.ZoneNames = {'', 'Feeder', 'Feeder', 'Water', 'Small-Nest', '', 'Labyrinth', 'Big-Nest', '', 'Block'};

Style.TextOffset = 40;

Style.FontSize = .04;

Style.ZonesStyle = {'FontWeight', 'bold', 'FontName', 'Helvetica Neue', 'FontUnits', 'normalized', 'FontSize', Style.FontSize};

Style.ShelterOffset = 10;

Style.ShelterInterpDuration = 8;

Style.BoxRadios = obj.VideoHeight*Style.FontSize/1.9 + 1;

Style.MinTimeInZone = 10;

%

X = obj.x;

Y = obj.y;

for s=1:obj.nSubjects

    for z=1:obj.ROI.nZones

        [b, e, len] = FindEvents(obj.zones(s, :) == z);

        for i=find(len < Style.MinTimeInZone)

            obj.zones(s, b(i):e(i)) = obj.zones(s, b(i)-1);

        end

    end

    [b, e] = FindEvents(obj.sheltered(s, :));

    for i=1:length(b)

        if e(i)-b(i)+1 > Style.ShelterInterpDuration * 2

            X(s, b(i)-1:b(i)+Style.ShelterInterpDuration-1) = sequence(X(s, b(i)-1), X(s, b(i)), Style.ShelterInterpDuration + 1);

            Y(s, b(i)-1:b(i)+Style.ShelterInterpDuration-1) = sequence(Y(s, b(i)-1), Y(s, b(i)), Style.ShelterInterpDuration + 1);

            

            X(s, e(i)-Style.ShelterInterpDuration+1:e(i)+1) = sequence(X(s, e(i)-Style.ShelterInterpDuration+1), X(s, e(i)+1), Style.ShelterInterpDuration + 1);

            Y(s, e(i)-Style.ShelterInterpDuration+1:e(i)+1) = sequence(Y(s, e(i)-Style.ShelterInterpDuration+1), Y(s, e(i)+1), Style.ShelterInterpDuration + 1);

        end

    end

end

%

 StartFrame = obj.StartTime * obj.FrameRate;

 EndFrame = obj.EndTime * obj.FrameRate;

%StartFrame = 53;

%EndFrame = 53;

 

cmap = MyMouseColormap;

mmap = obj.Colors.Centers;

 

start = true;

for f=StartFrame+Style.TrailDuration:EndFrame
f
    text(100, 100, num2str(f), 'Color', 'w', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center', Style.ZonesStyle{:});

    %imagesc(obj.BkgImage); hon;

    %imagesc(myMMReader(VideoFile, f)); hon;
    imagesc(myMMReader(VideoFile, f)); hon;

    BoxPatch(obj.VideoWidth - 30, 30, 'k', {}, 30)

 

    if start

        set(gca, 'Position', [0 0 1 1]);

        start = false;

    end

    %%

    for s=1:obj.nSubjects

        %plot(obj.x(s, f), obj.y(s, f), '.', 'Color', cmap(s, :));

        for pf=-Style.TrailDuration+1:0

            x = X(s, f+pf);

            y = Y(s, f+pf);

            if obj.sheltered(s, f)

                x = x - Style.ShelterOffset * (mod(s - 1, 2) - 1);

                y = y + Style.ShelterOffset * (floor(s/2) - 1);

            end

            if pf < 0 

                CirclePatch(x, y, mmap(s, :), {'EdgeColor', 'none', 'FaceAlpha', Style.TrailDecay^abs(pf)}, Style.Radios)

            else

                CirclePatch(x, y, mmap(s, :), {'EdgeColor', 'none', 'FaceAlpha', Style.TrailDecay^abs(pf)}, Style.Radios)

            end

        end

        if obj.zones(s, f) > 1

            %text(double(obj.x(s, f))+1, double(obj.y(s, f))-Style.TextOffset+1, Style.ZoneNames{obj.zones(s, f)}, 'Color', 'k', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center', Style.ZonesStyle{:});

            text(double(obj.x(s, f)), double(obj.y(s, f))-Style.TextOffset, Style.ZoneNames{obj.zones(s, f)}, 'Color', 'w', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center', Style.ZonesStyle{:});

        end

%         BoxPatch(2*Style.BoxRadios, obj.VideoHeight - 10 - s * Style.BoxRadios * 2.1, cmap(s, :), {'EdgeColor', 'none', 'FaceAlpha', Style.TrailDecay^abs(pf)}, Style.BoxRadios)

%         text(2*Style.BoxRadios+1.5, obj.VideoHeight - 11.5 - s * Style.BoxRadios * 2.1, num2str(obj.zones(s, f)), 'Color', 'w', 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center', Style.ZonesStyle{:});

        BoxPatch(2*Style.BoxRadios + s * Style.BoxRadios * 2.1, obj.VideoHeight - 10 - Style.BoxRadios * 2.1, cmap(s, :), {'EdgeColor', 'none', 'FaceAlpha', Style.TrailDecay^abs(pf)}, Style.BoxRadios)

        text(2*Style.BoxRadios+1.5 + s * Style.BoxRadios * 2.1, obj.VideoHeight - 13 - Style.BoxRadios * 2.1, num2str(obj.zones(s, f)), 'Color', 'w', 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center', Style.ZonesStyle{:});

    end

    hoff

    axis off;

    %%

    %pause(obj.dt);

    drawnow;

end