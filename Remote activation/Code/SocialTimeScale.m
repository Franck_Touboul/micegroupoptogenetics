fs_ = [1 2 3 6 12 25 50 125 250 750 1500 3000 7500 15000];
nIters = 1;
stat = [];
stat.src= {};
stat.map= {};
idx = 1;
fsidx = 1;
%%
for fs=fs_
    %%
    curr = SocialSetTimeScale(obj, fs);
    curr.OutputToFile = false;
    curr.OutputInOldFormat = false;
    stat.src{idx} = curr;
    %%
    nIters = 2 ^ (fsidx - 1);
    nIters = min(nIters, 16);
    for iter=1:nIters
        [fs, iter]
        if curr.nFrames < 400
            mapidx = randperm(curr.nFrames);
            map = false(1, curr.nFrames);
            map(mapidx(1:round(curr.nFrames/2))) = true;
        else
            map = rand(1, curr.nFrames) >= .5;
        end
        
        stat.map{idx} = map;
        trainobj = SocialApplyMap(curr, map);
        testobj = SocialApplyMap(curr, ~map);
        fprintf('# - computing probabilities of the train data\n');
        [q, independentProbs, jointProbs] = SocialComputePatternProbsFast(trainobj);
        stat.jointProbs(idx, :) = jointProbs(:)';
        fprintf('# - computing probabilities of the test data\n');
        [q, independentProbs, jointProbs] = SocialComputePatternProbsFast(testobj);
        stat.testProbs(idx, :) = jointProbs(:)';
        stat.fs(idx) = fs;
        fprintf('# - estimating Potts model\n');
        stat.bkgobj(idx) = RunInBackground(SocialPackageObjForPotts(trainobj), @SocialPotts);
        %    stat.bkgobj(idx) = SocialPotts(SocialPackageObjForPotts(trainobj));
        %%
        idx = idx + 1;
    end
    fsidx = fsidx + 1;
end
%%
stat.obj = {};
%
for idx=1:length(stat.fs)
    %%
    stat.obj{idx} = TrackLoad(stat.bkgobj(idx));
end
%%
idx = 1;
Djs = [];
meDjs = [];
res.Djs.mean = [];
res.Djs.stderr = [];
res.meDjs.mean = [];
res.meDjs.stderr = [];
for idx=1:length(stat.fs)
    Djs(idx) = JensenShannonDivergence(stat.jointProbs(idx, :), stat.testProbs(idx, :));
    for s=1:obj.nSubjects-1
        meDjs(s, idx) = JensenShannonDivergence(stat.obj{idx}.Analysis.Potts.Model{s}.prob', stat.testProbs(idx, :));
    end
end
idx = 1;
for fs=fs_
    %%
    res.Djs.mean(idx) = mean(Djs(stat.fs == fs));
    res.Djs.stderr(idx) = stderr(Djs(stat.fs == fs));
    for s=1:obj.nSubjects-1
        res.meDjs.mean(s, idx) = mean(meDjs(s, stat.fs == fs));
        res.meDjs.stderr(s, idx) = stderr(meDjs(s, stat.fs == fs));
    end
    idx = idx + 1;
end
%
cmap = MyCategoricalColormap;
for s=1:obj.nSubjects-1
    errorbar(fs_, res.meDjs.mean(s, :), res.meDjs.stderr(s, :), 'o-', 'color', cmap(s, :), 'LineWidth', 2, 'MarkerFaceColor', cmap(s, :)); hon;
end
errorbar(fs_, res.Djs.mean, res.Djs.stderr, 'o-', 'color', cmap(obj.nSubjects, :), 'LineWidth', 2, 'MarkerFaceColor', cmap(obj.nSubjects, :)); hoff;
set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');
set(gca, 'XTick', fs_, 'XTickLabel', fs_/25)
xaxis(min(fs_), max(fs_));
xlabel('location windows duration [sec]');
ylabel('Jensen-Shannon divergence [bits]');