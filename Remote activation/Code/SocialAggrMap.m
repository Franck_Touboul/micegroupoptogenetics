function SocialAggrMap(obj)
obj = TrackLoad(obj);
%%
day = 1;

Options.HorizWidth = 10;
Options.HorizDistance = 30;
Options.MarkerSize = 40;
Options.TextSize = 15;
Options.MaxEdgeWidth = 50;
Options.MaxEdgeValue = 5000;

hier = obj.Hierarchy.Group.AggressiveChase;
nodes = [];
width = 20;

index = 1;
for r=unique(hier.rank(:))'
    currnodes = find(hier.rank == r);
    s = 2 * mod(index, 2) - 1;
    offset = s * Options.HorizWidth;
    if index == 1 || index == length(unique(hier.rank(:)))
        offset = 0;
    end
    for i=currnodes
        nodes(i).Position = [offset - s * (length(currnodes) - i) * Options.HorizDistance, (index - 1) * 10 + 1]';
        nodes(i).Rank = r;
    end
    index = index + 1;
end

cmap = MyMouseColormap;
for i=1:obj.nSubjects
    from = nodes(i);
    for j=i+1:obj.nSubjects
        to = nodes(j);
        X = [from.Position(1) to.Position(1)];
        Y = [from.Position(2) to.Position(2)];
        plot(X, Y, 'k-', 'LineWidth', hier.Days(day).Contacts(i, j) / Options.MaxEdgeValue * Options.MaxEdgeWidth); hon
        textstyle = {'VerticalAlignment', 'top', 'HorizontalAlignment', 'left', 'FontSize', Options.TextSize};
        text(mean(X), mean(Y), [num2str(hier.Days(day).ChaseEscape(i, j)) '/' num2str(hier.Days(day).ChaseEscape(j, i))], 'Color', cmap(j, :), textstyle{:});
        text(mean(X), mean(Y), [num2str(hier.Days(day).ChaseEscape(i, j)) '/'], 'Color', 'k', textstyle{:});
        text(mean(X), mean(Y), [num2str(hier.Days(day).ChaseEscape(i, j))], 'Color', cmap(i, :), textstyle{:});
    end
end

for i=1:obj.nSubjects
    curr = nodes(i);
    plot(curr.Position(1), curr.Position(2), 'o', 'MarkerFaceColor', cmap(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', Options.MarkerSize); 
end
a = axis;
axis([a(1)-15 a(2)+15 a(3) a(4)])
axis off;
hoff
