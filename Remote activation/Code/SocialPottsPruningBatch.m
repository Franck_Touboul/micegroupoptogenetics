SocialExperimentData
order = 2;
img = [];
groupName = 'Standard';
DjsThresh = 0.2;
%%
group = GroupsData.(groupName);
for id=1:length(group.idx)
    %%
    %obj = TrackLoad({groupName id 2}, {'Analysis', 'ROI', 'nSubjects'});
    
    obj = TrackLoad(regexprep(['Res/' experiments{group.idx(id)} '.obj.mat'], '%02d', '01020304'), {'Analysis', 'ROI', 'nSubjects'});
    %%
    fontname = 'AvantGarde';
    set(0,'defaultAxesFontName', fontname)
    set(0,'defaultTextFontName', fontname)

    methods = fieldnames(obj.Analysis.Potts.Pruning);
    method = 'median';
    me = obj.Analysis.Potts.Model{order};
    nInteraction = find(flipdim(obj.Analysis.Potts.Pruning.(method).Djs, 2) >=DjsThresh, 1, 'last') + 1;
    
    cmap = MyCategoricalColormap;
    figure(id)
    subplot(2, 3, 3)
    for m=1:length(methods)
        nParams = length(me.weights):-1:length(me.weights) - length(obj.Analysis.Potts.Pruning.(methods{m}).Djs) + 1;
        plot(nParams, obj.Analysis.Potts.Pruning.(methods{m}).Djs, 'Color', cmap(m, :))
        hold on;
    end
    legend(methods); legend boxoff;
    box off;
    xlabel('# interactions');
    ylabel('Djs');
    vert_line(nInteraction, 'linestyle', ':', 'color', 'k');
    hold off;
    
    subplot(2, 3, [1:2, 4:5])
    conn = obj.Analysis.Potts.Pruning.(method).Removed(end-nInteraction+1:end);
    
    me.order = me.order(conn);
    me.weights = me.weights(conn);
    me.labels = { me.labels{conn} };
    
    r = 10;
    rr = .8;
    coord = [];
    for i=1:obj.ROI.nZones
        for s=1:obj.nSubjects
            R = r;
            coord(:, i, s) = [R * sin(i/obj.ROI.nZones * 2*pi) + rr * sin(s/obj.nSubjects* 2*pi), R * cos(i/obj.ROI.nZones * 2*pi) + rr * cos(s/obj.nSubjects* 2*pi)];
            plot(coord(1, i, s), coord(2, i, s), 'ko', 'MarkerFaceColor', 'k', 'MarkerSize', 5);
            hold on;
        end
        R = r + 4 * rr;
            text(R * sin(i/obj.ROI.nZones * 2*pi), R * cos(i/obj.ROI.nZones * 2*pi), obj.ROI.ZoneNames{i}, 'HorizontalAlignment', 'center');
    end
    
    [sweights, ow] = sort(abs(me.weights - median(me.weights(me.order == 2))));
    ow = ow(me.order(ow) == 2);
    sweights = sweights(me.order(ow) == 2);
    cmap = MyDefaultColormap(length(ow));
    linewidth = sequence(.5, 4, length(ow));
    
    idx = 1;
    for l=ow
        x = [coord(1, me.labels{l}(2,1), me.labels{l}(1,1)) coord(1, me.labels{l}(2,2), me.labels{l}(1,2))];
        y = [coord(2, me.labels{l}(2,1), me.labels{l}(1,1)) coord(2, me.labels{l}(2,2), me.labels{l}(1,2))];
        plot(x, y, 'Color', cmap(idx, :), 'LineWidth', linewidth(idx));
        %TransparentLine(x, y, cmap(idx, :), .8);
        idx = idx + 1;
    end
    axis off;
    hold off;
    title([obj.FilePrefix ': ' num2str(nInteraction) '(' num2str(length(ow)) ') interactions']);
    %saveFigure(['Res/SocialPottsPruning.' obj.FilePrefix '.DjsThresh' num2str(DjsThresh)])
    
    %
    subplot(2, 24, 41);
    imagesc(reshape(cmap, [size(cmap,1), 1, 3]));
    set(gca, 'XTick', []);
    labels = {};
    iseq =  sequencei(1, size(cmap,1), 5);
    for i=1:5
        labels{i} = sprintf(' %.2f', sweights(iseq(i)));
    end
    set(gca, 'YTick', iseq, 'YTickLabel', labels, 'YAxisLocation', 'right');
end