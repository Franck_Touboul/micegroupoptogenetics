function waitforfile(filename, maxsec)
if ~exist('maxsec', 'var')
    maxsec = 60;
end
t = tic;
h = exist(filename, 'file');
nchars = 0;
while ~h 
    nchars = Reprintf(nchars, ['# waiting (%d/%d sec) for: ''' filename ''''], round(toc(t)), maxsec);
    pause(1)
    h = exist(filename, 'file');
    if toc(t) > maxsec
        break;
    end
end
if ~h
    RePrintf(nchars, ['# waiting (%d/%d sec) for: "' filename '"'], maxsec, maxsec);
    fprintf('\n');
    warning(['could not find file ' filename]);
end
