function obj = SocialAnalysePredPreyModel_v2(obj)
fprintf('# analyzing Pred-Prey model\n');

obj = TrackLoad(obj);
obj.Interactions.PredPrey.MinNumberOfEvents = 2;

PrePred  = zeros(obj.nSubjects);
PrePrey  = zeros(obj.nSubjects);
PostPred = zeros(obj.nSubjects);
PostPrey = zeros(obj.nSubjects);
PredPrey = zeros(obj.nSubjects);
contacts = zeros(obj.nSubjects);
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1 == m2; continue; end;
        [~, ~, ~, events1] = FindEvents(obj.Interactions.PredPrey.states{m1, m2}, obj.Interactions.PredPrey.states{m1, m2} > 1);
        [~, ~, ~, events2] = FindEvents(obj.Interactions.PredPrey.states{m2, m1}, obj.Interactions.PredPrey.states{m1, m2} > 1);
        for i=1:length(events1)
            PredPrey(m1, m2) = PredPrey(m1, m2) + (any(events1{i}.data == 5) && any(events2{i}.data == 6));
        end
        beg = FindEvents(obj.Interactions.PredPrey.states{m1, m2} == 4);
        contacts(m1, m2) = length(beg);

        beg = FindEvents(obj.Interactions.PredPrey.states{m1, m2} == 2);
        PrePred(m1, m2) = length(beg);
        beg = FindEvents(obj.Interactions.PredPrey.states{m1, m2} == 3);
        PrePrey(m1, m2) = length(beg);
        beg = FindEvents(obj.Interactions.PredPrey.states{m1, m2} == 5);
        PostPred(m1, m2) = length(beg);
        beg = FindEvents(obj.Interactions.PredPrey.states{m1, m2} == 6);
        PostPrey(m1, m2) = length(beg);
    end
end
%%
matPred = PostPred - PostPred';
matPrey = PostPrey - PostPrey';

mat = matPred - matPrey;

mat(binotest(PostPred, PostPred + PostPred') & binotest(PostPrey, PostPrey + PostPrey')) = 0;
mat = mat .* (mat > 0);

%rank = sum(mat>0, 2) - sum(mat>0, 1)' + sum(mat>0, 2) / 10;
%rank(sum(mat>0, 2) + sum(mat>0, 1)' == 0) = -inf;
[rank, removed] = TopoFeedbackArcSetOrder(mat);

szc = sum(contacts);
sz = sum(PostPred, 2)';

[~, bg] = SocialGraph(obj, mat, removed, rank, sz);
for i=1:length(bg.nodes)
    bg.nodes(i).ID = sprintf('%s(%d/%d)', bg.nodes(i).ID, sz(i), szc(i));
end

g = biograph.bggui(bg);
% copyobj(g.biograph.hgAxes,gcf);
% close(g.hgFigure);
f = get(g.biograph.hgAxes, 'Parent');
print(f, [obj.OutputPath 'predprey.' obj.FilePrefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');
%%
mat = PostPred - PostPred';
mat(binotest(PostPred, PostPred + PostPred')) = 0;
mat = mat .* (mat > 0);

%rank = sum(mat>0, 2) - sum(mat>0, 1)' + sum(mat>0, 2) / 10;
%rank(sum(mat>0, 2) + sum(mat>0, 1)' == 0) = -inf;
[rank, removed] = TopoFeedbackArcSetOrder(mat);

szc = sum(contacts);
sz = sum(PostPred, 2)';

[~, bg] = SocialGraph(obj, mat, removed, rank, sz);
for i=1:length(bg.nodes)
    bg.nodes(i).ID = sprintf('%s(%d/%d)', bg.nodes(i).ID, sz(i), szc(i));
end

g = biograph.bggui(bg);
% copyobj(g.biograph.hgAxes,gcf);
% close(g.hgFigure);
f = get(g.biograph.hgAxes, 'Parent');
print(f, [obj.OutputPath 'aggresive.' obj.FilePrefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');

%%
prevmat = mat;

mat = PrePred - PrePred';
mat(binotest(PrePred, PrePred + PrePred')) = 0;
mat = mat .* (mat > 0);

% rank = sum(mat>0, 2) - sum(mat>0, 1)' + sum(mat>0, 2) / 10;
% rank(sum(mat>0, 2) + sum(mat>0, 1)' == 0) = -inf;
%[rank, removed] = TopoFeedbackArcSetOrder(mat);

[~, bg] = SocialGraph(obj, mat, removed, rank);
g = biograph.bggui(bg);
% copyobj(g.biograph.hgAxes,gcf);
% close(g.hgFigure);
f = get(g.biograph.hgAxes, 'Parent');
print(f, [obj.OutputPath 'initiate.' obj.FilePrefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');


% mat = PostPrey - PostPrey';
% mat = mat .* (mat > 0);
% SocialGraph(obj, mat)
%%
prevmat = mat;

mat = PostPrey - PostPrey';
mat(binotest(PostPrey, PostPrey + PostPrey')) = 0;
mat = mat .* (mat > 0);

% rank = sum(mat>0, 2) - sum(mat>0, 1)' + sum(mat>0, 2) / 10;
% rank(sum(mat>0, 2) + sum(mat>0, 1)' == 0) = -inf;
%[rank, removed] = TopoFeedbackArcSetOrder(mat);

[~, bg] = SocialGraph(obj, mat, removed, rank);
g = biograph.bggui(bg);
% copyobj(g.biograph.hgAxes,gcf);
% close(g.hgFigure);
f = get(g.biograph.hgAxes, 'Parent');
print(f, [obj.OutputPath 'coward.' obj.FilePrefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');

%%
clf
subplot(1,2,1);
bar(PrePred, 'stacked'), colormap(obj.Colors.Centers);
subplot(1,2,2);
bar(PostPred, 'stacked'), colormap(obj.Colors.Centers);
tieaxis(1,2,[1 2]);
