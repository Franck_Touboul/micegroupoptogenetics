function [v, i, j] = min2d(m)
[V, I] = min(m);
[v, j] = min(V);
i = I(j);