isNewerOnly = true;
SocialExperimentData;
index = 1;
order = 3;
p = perms(1:obj.nSubjects);
%%
for id=GroupsData.index
    %%
    group = GroupsData.group(id);
    try
        obj = TrackLoad({id});
        if ~isNewerOnly || ~exist([obj.OutputPath obj.FilePrefix 'obj.mat'], 'file')
            TrackSave(obj);
        end
        %%
        index = index + 1;
    end
end
