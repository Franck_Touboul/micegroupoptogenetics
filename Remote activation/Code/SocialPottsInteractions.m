function obj = SocialPottsInteractions(obj)

%%
l=3; 
order = cellfun(@(x) size(x,2), obj.Analysis.Potts.Model{l}.labels);

v = (order == 3); 
iperms = obj.Analysis.Potts.Model{l}.inputPerms(v, :); 
w = obj.Analysis.Potts.Model{l}.weights(v); 
[~, o] = sort(w, 2, 'descend'); 
%%
obj.ROI.RegionCenters = [];
openRegion = true(obj.VideoWidth, obj.VideoHeight)';
for r=1:obj.ROI.nRegions
   [i, j] = find(obj.ROI.Regions{r});
   obj.ROI.RegionCenters(r, :) = [mean(i), mean(j)];
   openRegion = openRegion & ~obj.ROI.Regions{r};
end

obj.ROI.ZoneCentres = [];
for i=2:obj.ROI.nZones
    obj.ROI.ZoneCentres(i, :) = obj.ROI.RegionCenters(strcmp(regexprep(obj.ROI.ZoneNames{i}, '\((.*)\)', '$1'), obj.ROI.RegionNames), :);
end
[i, j] = find(openRegion);
%obj.ROI.ZoneCentres(1, :) = [mean(i), mean(j)];
obj.ROI.ZoneCentres(1, :) = [300, 270];
%%
nEvents = 30;
imagesc(obj.BkgImage); hold on;
colors = lines(nEvents);

for i=1:nEvents
    event = iperms(o(i), :);
    frame = find(all(obj.zones == repmat(event', 1, obj.nFrames)), 1);
    if ~isempty(frame)
        obj.ROI.ZoneNames(event)
%         x = obj.ROI.ZoneCentres(event,2) + randn(1) * 20;
%         y = obj.ROI.ZoneCentres(event, 1) + randn(1) * 20;
        x = double(obj.x(:, frame));
        y = double(obj.y(:, frame));
        plot(x, y, 'o-', 'color', colors(i, :), 'LineWidth', 5, 'MarkerFaceColor', colors(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', 20);
        for s=1:obj.nSubjects
            text(x(s), y(s), num2str(s), 'HorizontalAlignment','center','VerticalAlignment','middle')
        end
%         for s=unique(event)
%             plot(x, y, 'o', 'MarkerFaceColor', colors(i, :), 'MarkerEdgeColor', 'none', 'MarkerSize', 20 * sum(event == s));
%         end
    end
end
hold off;
hold off;
