function pos = ModelInSpace(Param, IsProjected)
Param = SetDefaults(Param, ...
    'Angle', 0, ...
    'MajorAxis', 1, ...
    'DX', 0,...
    'DY', 0,...
    'Height', 0.3,...
    'BodyYaw', 0, ...
    'BodyFat', 1, ...
    'ReadFat', 1 ...
    );
if ~exist('IsProjected', 'var')
    IsProjected = true;
end
%%
ModelParam.Body.Yaw = Param.BodyYaw;
ModelParam.Body.FatScale = Param.BodyFat;
ModelParam.Body.RearScale = Param.RearFat;
Model = ModelCreateF(ModelParam);
Model.SkinMemory = .9;
Points = ModelToPointsF(Model);
%% center model
if IsProjected
    valid = false(size(Points.Skin.X));
    valid(1:34, :) = true;
    ID = repmat((1:size(Points.Skin.X, 1))', 1, size(Points.Skin.X, 2));
    X = Points.Skin.X(Points.Skin.Area <= 0 & valid); 
    Y = Points.Skin.Y(Points.Skin.Area <= 0 & valid); 
    Z = Points.Skin.Z(Points.Skin.Area <= 0 & valid);
    ID = ID(Points.Skin.Area <= 0 & valid);
else
    X = Points.Skin.X(1:34, :);
    Y = Points.Skin.Y(1:34, :);
    Z = Points.Skin.Z(1:34, :);
end
FID = repmat((1:size(Points.Skin.X, 1))', 1, size(Points.Skin.X, 2));
cmX = Points.Bone.X(Model.Axis(1), 1);
cmY = Points.Bone.Y(Model.Axis(1), 1);
X = X - cmX;
Y = Y - cmY;
Res.Model.Beg = [Points.Bone.X(Model.Axis(1), 1), Points.Bone.Y(Model.Axis(1), 1)];
Res.Model.End = [Points.Bone.X(Model.Axis(2), 1), Points.Bone.Y(Model.Axis(2), 1)];

%% rotate & scale
Res.Model.MajorAxis = sqrt(...
    (Points.Bone.X(Model.Axis(1), 1) - Points.Bone.X(Model.Axis(2), 1)).^2 + ...
    (Points.Bone.Y(Model.Axis(1), 1) - Points.Bone.Y(Model.Axis(2), 1)).^2);
Res.Model.Orientation = atan2(Res.Model.End(2) - Res.Model.Beg(2), Res.Model.End(1) - Res.Model.Beg(1));

R = RotMatRz(Param.Angle - Res.Model.Orientation) * ...
    Param.MajorAxis / Res.Model.MajorAxis;
pos = [X(:), Y(:), Z(:)] * R';

%% align position
pos(:, 1) = pos(:, 1) + Param.DX;
pos(:, 2) = pos(:, 2) + Param.DY;

%% fix height
fZ = Points.Skin.Z * Param.MajorAxis / Res.Model.MajorAxis;
minThoracic = min(-fZ(FID == 12));
minSacral = min(-fZ(ID > 30));
gamma = (ID - 12) / (34 - 12);
pos(:, 3) = -pos(:, 3) - gamma * minSacral - (1 - gamma) * minThoracic;
%pos(:, 3) = pos(:, 3) * Param.Height / max(pos(:, 3));

if nargout == 0
    plot3(pos(:, 1), pos(:, 2), pos(:, 3), '.');
    axis equal
end
