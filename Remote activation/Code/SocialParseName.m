function [obj, data, first] = SocialParseName(obj)

first = struct();
data.Conditions = {};
data.Groups = [];
data.Days = [];

if isfield(obj, 'FilePrefix') || ischar(obj)
    if isstruct(obj)
        name = obj.FilePrefix;
    else
        name = obj;
        obj = TrackGenerateObject('');
    end
    %% single object
    obj.Condition = regexprep(name, '^([^\.]*)\..*', '$1');
    obj.GroupID = str2double(regexprep(name, '.*exp([0-9]*).*', '$1'));
    obj.Day = str2double(regexprep(name, '.*day([0-9]*).*', '$1'));
    obj.CameraID = str2double(regexprep(name, '.*cam([0-9]*).*', '$1'));
    %
    data.Conditions{1} = obj.Condition;
    data.Groups = obj.GroupID;
    data.Days = obj.Day;
    data.nExps = 1;
else 
    %% multiple objects
    names = fieldnames(obj);
    for i=1:length(names)
        currObj = getfield(obj, names{i});
        if i==1
            first = currObj;
        end
        [currObj, newdata] = SocialParseName(currObj);
        obj = setfield(obj, names{i}, currObj);
        %
        data.Conditions = {data.Conditions{:} newdata.Conditions{:}};
        data.Groups = [data.Groups, newdata.Groups];
        data.Days = [data.Days newdata.Days];
    end
    data.Conditions = unique(data.Conditions);
    data.Groups = unique(data.Groups);
    data.Days = unique(data.Days);
    data.nExps = length(names);
end
data.nConditions = length(data.Conditions);
data.nDays = length(data.Days);