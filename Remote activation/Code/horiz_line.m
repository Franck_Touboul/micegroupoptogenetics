function horiz_line(y, varargin)
a = axis;
if nargin >= 2
    line([a(1), a(2)], [y y], varargin{:});
else
    line([a(1), a(2)], [y y]);
end