id_ = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
for k = 1:length(id_)
    %%
    id = id_{k};
    res = [];
    for i=1:8;
        curr = load(['SocialPottsControlsCompact' id '.' sprintf(GroupsData.Experiments{10+i}, 2)], '-mat');
        if i == 1
            res = SocialPottsControlsCompactAnalyze(curr.objs);
        else
            res(i) = SocialPottsControlsCompactAnalyze(curr.objs);
        end
    end
    eval(['res' id ' = res']);
end
%%
resF = [];
for i=1:8
    obj = TrackLoad({10+i 2});
    other = TrackLoad({10+i 3});
    for j=1:4
        resF(i).Empirical(j, 1) = JensenShannonDivergence(obj.Analysis.Potts.Model{j}.prob', other.Analysis.Potts.Model{j}.jointProb);
    end
end
%%
id_ = {'F', 'A', 'B', 'C', 'D', 'E', 'G'};
titles = {'day', '1 min', '10 min', '30 min', '1 frame', 'random', '6 hours'};

id_ = {'R', 'A', 'B', 'C', 'D', 'E', 'G', 'F'};
titles = {'random', '1 min', '10 min', '30 min', '1 frame', '6 hours', 'day'};

cmap = MyCategoricalColormap;
idx = 1;
entries = {};
for k = 1:length(id_)
    id = id_{k};
    if k == 5
        id
        continue;
    end
    eval(['res = res' id]);
    %SquareSubplpot(length(id_), k);
    %plot(1:4, [res.Empirical], 'ok')
    errorbar(1:4, mean([res.Empirical], 2), stderr([res.Empirical], 2), 'LineWidth', 2, 'Color', cmap(idx, :))
    hon;
    title(titles{k});
    entries{idx} = titles{k};
    idx = idx +1;
end
legend(entries)
hoff;
set(gca, 'XTick', 1:obj.nSubjects);
xlabel('interactions order of the model');
ylabel('Djs of predictions [bits]');
%tieaxis(2, 3, 1:6);