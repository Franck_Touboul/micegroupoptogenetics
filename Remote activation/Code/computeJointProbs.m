function [jointProbs, pci] = computeJointProbs(data, range, fixed)
% computeJointProbs(data, max_val, fixed)
%   computed the Joint probability for a set data with discrete values
%   in the range [1:max_val].
%   If fixed is set to true then assume at least one observation for
%   each event.
if ~exist('range', 'var')
    range = max(data(:)) + 1;
end
if nargin < 3
    fixed = false;
end
baseVector = range.^[0:size(data,1)-1];
count = zeros(1, range^size(data,1));
h = baseVector * data + 1;
% for i=1:range^size(data,1)
%     count(i) = sum(h == i);
% end
count = histc(h, 1:range^size(data,1));
if fixed
    count = count + 1;
end
[jointProbs, pci] = binofit(count, sum(count));
