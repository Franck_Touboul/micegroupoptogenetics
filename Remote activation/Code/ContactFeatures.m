function [obj, features] = ContactFeatures(obj)
%%
features = struct();
aux.dx = [zeros(obj.nSubjects, 1) diff(obj.x, 1, 2)];
aux.dy = [zeros(obj.nSubjects, 1) diff(obj.y, 1, 2)];

for m1=1:obj.nSubjects
    for m2=1:m1-1
        aux.CoM.x = sum(obj.x([m1, m2], :), 1)/2 ;
        aux.CoM.y = sum(obj.y([m1, m2], :), 1)/2 ;
        %
        aux.Axis.x = obj.x(m2, :) - obj.x(m1, :);
        aux.Axis.y = obj.y(m2, :) - obj.y(m1, :);
        n = sqrt(aux.Axis.x.^2 + aux.Axis.y.^2);
        aux.Axis.x = aux.Axis.x ./ n;
        aux.Axis.y = aux.Axis.y ./ n;
        
        aux.RelativeSpeed = aux.dx(m1, :) .* aux.Axis.x + aux.dy(m1, :) .* aux.Axis.y
        
        %
        features(m1,m2).CoMSpeed = [0, sqrt(diff(aux.CoM.x).^2 + diff(aux.CoM.y).^2) / obj.dt];
        features(m2,m1).CoMSpeed = features(m1,m2).CoMSpeed;
        %
        features(m1,m2).RelativeSpeed = sqrt((aux.dx(m2, :) - aux.dx(m1, :)).^2 + (aux.dy(m2, :) - aux.dy(m1, :)).^2) / obj.dt;
    end
end