SocialExperimentDataSC
day = 1;
nSubjects = 4;
%%
for i=1:nSubjects
    prefix = sprintf(experiments{i}, day);
    if i==1
        obj = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid'});
    else
        o = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid'});
        len = min(size(obj.zones, 2), size(o.zones, 2));
        
        obj.zones = obj.zones(:, 1:len);
        obj.zones(i, :) = o.zones(i, 1:len);
        
        obj.valid = obj.valid(1:len);
        obj.valid = obj.valid & o.valid(1:len);
    end
end
%%
obj.FilePrefix = 'mixed';
obj.OutputInOldFormat = false;
obj.OutputToFile = false;
obj = SocialPotts(obj);