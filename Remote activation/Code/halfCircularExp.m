function p = halfCircularExp(x, alpha)
p = alpha * exp(-alpha*x) / (1-exp(-alpha*pi));