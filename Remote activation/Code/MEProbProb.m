function MEProbProb(y1, y2, count, varargin)
options.IsEquallAxis = true;
%%
logY1 = log10(y1);
logY2 = log10(y2);
loglog(y1, y2, '.', 'Color', [.6 .6 .6]);
a = axis;
if options.IsEquallAxis
    axis([1/count 1.2*max([y1(:); y2(:)]) 1/count 1.2*max([y1(:); y2(:)])]);
    a = axis;
end
loglog([a(1) a(2)], [a(1) a(2)], 'k-', 'LineWidth', 2, 'Color', [.7 .7 .7]);
hold on;
if exist('count', 'var') && ~isempty(count) && count > 0
    %%
    l = sequence(log10(a(1)), log10(a(2)), 100);
    r = 10.^l * count;
    [www, p] = binofit(r(r <= count), count);
    u1 = log10(p(:, 1));
    u2 = log10(p(:, 2));
    u1(~isfinite(u1) | u1 < log10(a(3))-1) = log10(a(3))-1;
    fill([10.^l, flipdim(10.^l, 2)], [10.^u1', flipdim(10.^u2', 2)], [.8 .8 .8], 'LineStyle', 'none');
end
loglog(y1, y2, '.', 'Color', [188 80 80 ]/255);
if options.IsEquallAxis
    axis square
end
%%
axis(a);
hold off;

%%
%title(['D_{JS} = ' num2str(JensenShannonDivergence(y1(:)', y2(:)')) ', D_{KL} = ' num2str(KullbackLeiblerDivergence(y1(:)', y2(:)'))]);
%text(a(1), a(4), {'', [' D_{JS} = ' num2str(JensenShannonDivergence(y1(:)', y2(:)'))], [' D_{KL}' num2str(KullbackLeiblerDivergence(y1(:)', y2(:)'))]}, 'VerticalAlignment', 'top', 'HorizontalAlignment', 'left')
text(a(2), a(3), {sprintf('D_{JS} = %.3f', JensenShannonDivergence(y1(:)', y2(:)')), sprintf('D_{KL} = %.3f', KullbackLeiblerDivergence(y1(:)', y2(:)'))}, 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'right')
MEStyle();