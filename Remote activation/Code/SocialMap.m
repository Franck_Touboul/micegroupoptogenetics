function SocialMap(obj)
x = obj.x;
y = obj.y;
%%
if isfield(obj.ROI, 'Arena')
    temp = obj;
    temp.ROI = obj.ROI.Full;
    temp = TrackSetZones(temp);
    z = temp.zones;
    props = regionprops(obj.ROI.Arena, 'BoundingBox');
    bbox = props.BoundingBox;
    v = ~(z == 5 | z == 8 | z == 11 | z == 12);
    x(x < bbox(1) & v) = bbox(1);
    x(x > bbox(1) + bbox(3) & v) = bbox(1) + bbox(3);
    y(y < bbox(2) & v) = bbox(2);
    y(y > bbox(2) + bbox(4) & v) = bbox(2) + bbox(4);
end
y = obj.VideoHeight - y;
% for s=1:obj.nSubjects
%     x = round(x); x(x<1) = 1; x(x >
%     y = round(y);
%
% end

%%
nbins = 50;
range = [min(x(:)), max(x(:)), min(y(:)), max(y(:))];
range(2) = ceil(range(2)/20) * 20;
range(4) = ceil(range(4)/20) * 20;
%range = [63 712 16 580];
%range = [0, max(x(:)), 0, max(obj.VideoHeight - y(:))];
count = [nbins, (range(4) - range(3)) / ( (range(2) - range(1)) / nbins )];
[h, range] = HexHist([x(~obj.hidden), y(~obj.hidden)], 50, 'range', range);
fh = nlfilter(h, [3 3], @(x) (x(2,2)~=0 || sum(sum(x == 0)) <= 1) * x(2, 2) + 1 / (x(2,2)~=0 || sum(sum(x == 0)) <= 1)-1);
%%
%h(h == 0) = nan;
fh(~isfinite(fh)) = nan;
HexPlot(fh, range, 'showbar', true)
set(gcf, 'color', 'w');
%%
SocialSetFigureProperties(obj);
%%

% %%
% nbins = 30;
% [h, c] = hist3([obj.x(~obj.hidden), obj.VideoHeight - obj.y(~obj.hidden)], [nbins, nbins]);
% hm = [];
% img = zeros(nbins*4,nbins*4,3);
% for i=1:obj.nSubjects
%     hm = cat(3, hm, hist3([obj.x(i, ~obj.hidden(i, :))', obj.VideoHeight - obj.y(i, ~obj.hidden(i, :))'], 'edges', c));
%     img(1:2:end, 1:2:end, :) = hm(:, :, i) / max(h(:)) * repmat(reshape(obj.Colors.Centers(i, :), 1, 1, 3), [nbins, nbins, 1]);
% end
if (1==2)
    %%
    p = 'Graphs/SocialMap/';
    mkdir(p);
    figure(1);
    saveFigure([p obj.FilePrefix '.2color']);
end