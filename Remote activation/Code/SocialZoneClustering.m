% function obj = SocialZoneClustering(obj)

AllGroups = {...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam04',...
    };

%%
a = AllGroups;

nDays = 4;
groups = {'Enriched', 'SC'};

isfirst = true;
proto = obj;
zones.histogram = nan(nDays * length(a) * proto.nSubjects, proto.ROI.nZones);
zones.group = zeros(1, size(zones.histogram, 1));
zones.day = zeros(1, size(zones.histogram, 1));
zones.id = zeros(1, size(zones.histogram, 1));
for i=1:length(a)
    %%
    for day = 1:nDays;
        prefix = sprintf(a{i}, day);
        fprintf('#----------------------------------------\n# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Common'});
            groupmatch = regexpi(prefix, groups);
            group = 0;
            for g=1:length(groupmatch)
                if ~isempty(groupmatch{g})
                    group = g;
                end
            end
            for s=1:obj.nSubjects
                index = (i - 1) * nDays * proto.nSubjects + (day - 1) * proto.nSubjects + s;
                zones.histogram(index, :) = histc(obj.zones(s, :), 1:obj.ROI.nZones);
                zones.group(index) = group;
                zones.day(index) = day;
                zones.id(index) = s;
            end
        catch me
            MyWarning(me);
        end
    end
end
zones.prob = zones.histogram ./ repmat(sum(zones.histogram, 2), 1, proto.ROI.nZones);
%%
[idx, c] = kmeans(zones.prob(~isnan(sum(zones.prob, 2)), :), obj.nSubjects);

allperms = perms(1:proto.nSubjects);
nperms = size(allperms, 1);
niters = 10;
C = nancov(zones.prob);
C = C + min(abs(eig(C)));

for iter = 1:niters
    newc = c * 0;
    count = 0;
    for i=1:length(a)
        for day = 1:nDays;
            bestDist = inf;
            bestPerm = 0;
            for j=1:nperms
                index = (i - 1) * nDays * proto.nSubjects + (day - 1) * proto.nSubjects + allperms(j, :);
                dist = sum(diag(pdist2(zones.prob(index, :), c, 'mahalanobis', C)));
                if dist < bestDist
                    bestPerm = j;
                    bestDist = dist;
                end
            end
            if bestPerm > 0
                index = (i - 1) * nDays * proto.nSubjects + (day - 1) * proto.nSubjects + allperms(bestPerm, :);
                newc = newc + zones.prob(index, :);
                count = count + 1;
            end
        end
    end
    c = newc / count;
end
plot(c');