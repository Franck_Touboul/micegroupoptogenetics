function ModelShow(Model, Points)
if nargin == 1
    Points = ModelToPointsF(Model);
end
X = Points.Skin.X; Y = Points.Skin.Y; Z = Points.Skin.Z;
valid = Points.Skin.Area < 0;
surfl(X,Y,1-Z); shading interp; colormap(gray);
hon
plot3(X(valid), Y(valid), 1-Z(valid), '.', 'color', [17 113 185]/255);
plot3(X(~valid), Y(~valid), 1-Z(~valid), '.', 'color', [170 55 73]/255);
q = X * 0;
a = axis;
surf(X, Y, q + a(5));
surf(q + a(2), Y, 1-Z);
surf(X, q + a(4), 1-Z);

cmap = MySubCategoricalColormap;
idx = 1;
q = Points.Bone.X * 0;
for i=unique(Model.Types(:)')
    plot3(Points.Bone.X(Model.Types == i, :), Points.Bone.Y(Model.Types == i, :), q(Model.Types == i, :) + a(5), 'o-', 'color', cmap(idx, :), 'MarkerFaceColor', cmap(idx, :));
    plot3(q(Model.Types == i, :) + a(2), Points.Bone.Y(Model.Types == i, :), 1-Points.Bone.Z(Model.Types == i, :), 'o-', 'color', cmap(idx, :), 'MarkerFaceColor', cmap(idx, :));
    plot3(Points.Bone.X(Model.Types == i, :), q(Model.Types == i, :) + a(4), 1-Points.Bone.Z(Model.Types == i, :), 'o-', 'color', cmap(idx, :), 'MarkerFaceColor', cmap(idx, :));
    idx = idx + 1;
end

hoff
axis equal;
