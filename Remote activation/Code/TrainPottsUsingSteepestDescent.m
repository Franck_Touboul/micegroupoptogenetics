function [me, output, ent] = TrainPottsUsingSteepestDescent(me)
x = me.perms;
%w0 = me.weights;
w0 = randn(1, size(me.labels, 2)) / size(me.labels, 2); %randn(1, length(me.weights));
aux = ModelExpectationAux(x);
fun = @(w) HalfKL(w, x, me.constraints, aux);

% options = optimset('GradObj','on', 'Display', 'iter', 'HessUpdate', 'bfgs');
% fminunc(fun, w0,options);

% options = optimset('Algorithm', 'interior-point', 'Display', 'iter', 'Hessian', 'lbfgs');
% fmincon(fun, w0, [], [], [], [], [], [], [], options);

% options = struct('Display','iter',...
%     'MaxIter',1000,...
%     'TolFun',1e-6,...
%     'TolX', 1e-10,...
%     'TolGrad',1e-6,...
%     'Convex',0);
% w = lbfgs(fun, w0, options);
%options = optimset('GradObj','on', 'Display', 'iter', 'HessUpdate', 'steepdesc');

iE = Entropy(w0, x);

options = optimset('GradObj','off', 'Display', 'iter', 'OutputFcn', @MeasureProgress, 'HessUpdate', 'steepdesc');
options = optimset('GradObj','off', 'Display', 'iter', 'OutputFcn', @MeasureProgress, 'HessUpdate', 'steepdesc', 'TolFun', 0, 'TolX', 0);
[w,fval,exitflag,output] = fminunc(fun, w0,options);
me.weights = w;

fE = Entropy(w, x);
ent = fE;
fprintf('# Entropy: %f (initial %f)\n', fE, iE);
perf = MeasureProgress;
output.times = perf.times;
output.x = [w0; perf.x];
output.fval = [HalfKL(w0, x, me.constraints, aux); perf.fval];

function output = MeasureProgress(x,optimValues,state)
persistent Obj;
if nargin == 0
    output = Obj;
    Obj = struct();
    return;
end

if isempty(Obj) 
    Obj.times = [];
    Obj.x = [];
    Obj.fval = [];
end

switch state
    case 'iter'
        if optimValues.iteration == 0
            Obj.ticID = tic;
            Obj.times = 0;
        else
            Obj.times(optimValues.iteration) = toc(Obj.ticID);
        end
        Obj.x = [Obj.x; x];
        Obj.fval = [Obj.fval; optimValues.fval]; 
          % Make updates to plot or guis as needed
    case 'interrupt'
          % Probably no action here. Check conditions to see  
          % whether optimization should quit.
    case 'init'
          % Setup for plots or guis
    case 'done'
          % Cleanup of plots, guis, or final plot
otherwise
end
output = false;

function e = Entropy(w, x)
p0 = exp(x * w');
Z = sum(p0);
p = p0 / Z;
e = -p' * log2(p);

function aux = ModelExpectationAux(x)
aux.u = unique(sum(x));
idx = 1;
for u=aux.u
    aux.p(idx).iu = sum(x,1) == u;
    aux.p(idx).nu = sum(aux.p(idx).iu);
    [i,j] = find(x(:, aux.p(idx).iu));
    aux.p(idx).i = i;
    idx = idx + 1;
end

function E = ModelExpectation(w, x, constraints, aux)
%%
perms_p = exp(x * w');
Z = sum(perms_p);

r = perms_p / Z;
E = zeros(1, length(constraints));
idx = 1;
for u=aux.u
    E(aux.p(idx).iu) = sum(reshape(r(aux.p(idx).i), u, aux.p(idx).nu)', 2);
    idx = idx + 1;
end

function [KL, d] = HalfKL(w, x, constraints, aux)
perms_p = exp(x * w');
Z = sum(perms_p);
%E = full(sum(x .* repmat(perms_p / Z, 1, size(x, 2))));
E = ModelExpectation(w, x, constraints, aux);

KL = -constraints * log(E');
if nargout > 1
    d = -(constraints - E);
end
