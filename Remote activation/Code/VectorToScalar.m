function s = VectorToScalar(vec, maxval)
dim = size(vec, 2);
v = (maxval).^(0:dim-1);
s = (vec - 1) * v';