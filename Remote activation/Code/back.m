options.test.name = 'trial_1';
options.output_path = 'res/';
%%
options.MovieFile= 'C:\Documents and Settings\USER\Desktop\Hezi\Trial     1.mpg';
options.nSubjects = 4;
options.output = false;
%
options.noiseThresh = 3;
%
options.colorFrames = 100;
options.colorBins = 20;
%
options.solidity = .5;
%
options.scale = .25;
options.maxNumCents = 10;
%
options.minVisibleDuration = 25;
options.minHiddenDuration = 5;
options.maxJumpPerStep = 10;
%
options.minNumPixels = 30;
%%
fprintf('# segmenting frames\n');
fprintf('# - opening movie file\n');
xyloObj = mmreader(options.MovieFile);
nFrames = xyloObj.NumberOfFrames;
dt = 1/xyloObj.FrameRate;
%%
fprintf('# - loading meta data\n');
filename = [options.output_path options.test.name '.meta.mat'];
load(filename);

%%
prevProps = [];
sx = []; sy = [];

cents.x = zeros(nFrames, options.maxNumCents);
cents.y = zeros(nFrames, options.maxNumCents);
cents.label = zeros(nFrames, options.maxNumCents, 'uint8');
cents.area = zeros(nFrames, options.maxNumCents, 'uint16');
cents.logprob = zeros(nFrames, options.maxNumCents, options.nSubjects);

nchars = RePrintf('# - frame %6d/%6d (%6.2fxRT)', 0, nFrames, 1);
tic;
for r=1:nFrames
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d/%6d (%6.2fxiRT)', r, nFrames, RT);
    
    m = imsubtract(read(xyloObj, r), meta.bkgFrame);
    m = imresize(m, options.scale);
    
    if isempty(sx)
        [sx, sy, nc] = size(m);
    end
    m = im2double(m);
    if options.output
        orig = m;
    end
    m = (m > options.noiseThresh * std(m(:))) .* m;
    %%
    hsv_m = rgb2hsv(m);
    hm = hsv_m(:,:,1);
    sm = hsv_m(:,:,2);
    vm = hsv_m(:,:,3);
    %%
    bw = sum(m > 0, 3);
    bw = bwareaopen(bw, options.minNumPixels);
    %labels = bwlabel(bw);
    %nobjects = max(labels(:));
    %%
    [b, idx_h] = histc(hm(bw), meta.subject.colorBins);
    [b, idx_s] = histc(sm(bw), meta.subject.colorBins);
    [b, idx_v] = histc(vm(bw), meta.subject.colorBins);
    prob_h = zeros(options.nSubjects, length(idx_h));
    prob_s = zeros(options.nSubjects, length(idx_s));
    prob_v = zeros(options.nSubjects, length(idx_v));
    for i=1:options.nSubjects
        prob_h(i, :) = meta.subject.h(i, idx_h);
        prob_s(i, :) = meta.subject.s(i, idx_s);
        prob_v(i, :) = meta.subject.v(i, idx_v);
    end
    %     prob_h = prob_h ./ repmat(sum(prob_h, 1), options.nSubjects, 1);
    %     prob_s = prob_s ./ repmat(sum(prob_s, 1), options.nSubjects, 1);
    %     prob_v = prob_v ./ repmat(sum(prob_v, 1), options.nSubjects, 1);
    %     joint_prob = prob_h .* prob_s .* prob_v;
    joint_prob = prob_h .* prob_s .* prob_v;
    joint_prob = joint_prob ./ repmat(sum(joint_prob, 1), options.nSubjects, 1);
    
    [m, idx] = max(joint_prob, [], 1);
    nlabels = zeros(sx,sy,'uint8');
    nlabels(bw) = idx;
    
    for k=1:options.nSubjects
        logprobmap{k} = zeros(sx,sy);
        logprobmap{k}(bw) = flog(joint_prob(k, :));
    end
    
    flabels = zeros(sx,sy,'uint8');
    %%
    centIndex = 1;
    for i=1:options.nSubjects
        %%
        %curr = bwareaopen(curr, options.minNumPixels);
        reg = bwconncomp(nlabels == i);
        %        reg = bwconncomp(curr);
        %conn = regionprops(reg, 'Solidity', 'MajorAxisLength', 'MinorAxisLength');
        conn = regionprops(reg, 'Solidity', 'Centroid');
        for j=1:length(reg.PixelIdxList)
            %             if length(reg.PixelIdxList{j}) < options.minNumPixels
            %                 reg.PixelIdxList{j} = [];
            %             end
            if conn(j).Solidity < options.solidity || ...
                    length(reg.PixelIdxList{j}) < options.minNumPixels
                reg.PixelIdxList{j} = [];
            else
                cents.x(r, centIndex) = conn(j).Centroid(1);
                cents.y(r, centIndex) = conn(j).Centroid(2);
                cents.label(r, centIndex) = i;
                cents.area(r, centIndex) = length(reg.PixelIdxList{j});
                for k=1:options.nSubjects
                    cents.logprob(r, centIndex, k) = sum(logprobmap{k}(reg.PixelIdxList{j}));
                end
                cents.logprob(r, centIndex, :) = cents.logprob(r, centIndex, :) - flog(sum(exp(cents.logprob(r, centIndex, :))));
                centIndex = centIndex + 1;
            end
            %             if conn(j).Solidity < .5 || ...
            %                     length(reg.PixelIdxList{j}) < options.minNumPixels || ...
            %                     length(reg.PixelIdxList{j}) / (conn(j).MajorAxisLength * conn(j).MinorAxisLength * pi/4) < 0.5
            %                 reg.PixelIdxList{j} = [];
            %             end
        end
        img = labelmatrix(reg);
        if 1 == 2
            subplot(2,2,i);
            imagesc(img > 0);
        end
        flabels(img > 0) = i;
    end
    %%
    if options.output
        subplot(2,2,4);
        rgblbls = label2rgb(flabels);
        imagesc(rgblbls);
        subplot(2,2,3);
        imagesc(orig);
        drawnow
    end
    %%
end
fprintf('\n');
fprintf(['# - total time: ' sec2time(toc) '\n']);
%
fprintf('# - saving segmentation\n');
filename = [options.output_path options.test.name '.segm.mat'];
save(filename, 'cents');

%%
fprintf('# finding tracks ');
options.jumpVar = eye(2) * 5^2;
options.confProb = 0.1;
options.hideProb = 0.0;

res = {};
nchars = RePrintf('(%2d/%2d)', 0, options.nSubjects);
for curr = 1:options.nSubjects;
    nchars = RePrintf(nchars, '(%2d/%2d)', curr, options.nSubjects);
    res{curr} = followTrack_v3(options, cents, curr);
end
fprintf('\n');

fprintf('# - saving raw track\n');
filename = [options.output_path options.test.name '.raw-track.mat'];
save(filename, 'res');
rawTrack = res;

%%
filename = [options.output_path options.test.name '.raw-track.mat'];
load(filename, 'res');
filename = [options.output_path options.test.name '.meta.mat'];
load(filename);
filename = [options.output_path options.test.name '.segm.mat'];
load(filename);

% checks for double assigned segments, and chooses the best matching subject
fprintf('# - ensuring labels are unique\n');
res = reassignLabels(res);

% remove false alarms
fprintf('# - removing false alarms:\n');

% - big jumps
% fprintf('#  i.  big jumps\n');
% for curr = 1:options.nSubjects;
%     visible = res{curr}.id > 0;
%     d = diff([0 visible 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     distance = sqrt(...
%         (res{curr}.x(start(2:end)) - res{curr}.x(finish(1:end-1))).^2 + ...
%         (res{curr}.y(start(2:end)) - res{curr}.y(finish(1:end-1))).^2);
%     gap = start(2:end) - finish(1:end-1) - 1;
%     loc = gap < options.minHiddenDuration & distance < options.maxJumpPerStep * gap;
%     for l=find(loc)
%         range = finish(l)+1:start(l+1)-1;
%         res{curr}.id(range) = inf;
%         res{curr}.x(range) = interp1([range(1)-1 range(end)+1], res{curr}.x([range(1)-1 range(end)+1]), range);
%         res{curr}.y(range) = interp1([range(1)-1 range(end)+1], res{curr}.y([range(1)-1 range(end)+1]), range);
%     end    
% end

%fprintf('#  i.  big jumps\n');
fprintf('#  ii. small visibility epochs\n');
for curr = 1:options.nSubjects;
    x = res{curr}.x; x(isnan(x)) = inf;
    y = res{curr}.y; y(isnan(y)) = inf;
    fullDistance = [0 sqrt(diff(x).^2 + diff(y).^2)];
    visible = [0 abs(diff(res{curr}.id ~= 0))];
%     visible = fullDistance > options.maxJumpPerStep | visible;
    start  = [1 find(visible > 0)];
    finish = [find(visible > 0) - 1, length(visible)];
    start  = start (isfinite(x(start))  & res{curr}.id(start)  ~= 0);
    finish = finish(isfinite(x(finish)) & res{curr}.id(finish) ~= 0);

    loc = finish - start < options.minVisibleDuration;
    for l=loc
        if start(l) > 1
            res{curr}.x(start(l):finish(l)) = nan;
            res{curr}.y(start(l):finish(l)) = nan;
            res{curr}.id(start(l):finish(l)) = 0;
        end
    end
end

% % - small visibility epochs
% fprintf('#  ii. small visibility epochs\n');
% for curr = 1:options.nSubjects;
%     visible = res{curr}.id ~= 0;
%     d = diff([0 visible 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     loc = find(finish - start + 1 < options.minVisibleDuration);
%     for l=loc
%         if start(l) > 1
%             res{curr}.x(start(l):finish(l)) = nan;
%             res{curr}.y(start(l):finish(l)) = nan;
%             res{curr}.id(start(l):finish(l)) = 0;
%         end
%     end
% end

% for curr = 1:options.nSubjects;
%     visible = res{curr}.id > 0;
%     d = diff([0 visible 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     distance = sqrt(...
%         (res{curr}.x(start(2:end)) - res{curr}.x(finish(1:end-1))).^2 + ...
%         (res{curr}.y(start(2:end)) - res{curr}.y(finish(1:end-1))).^2);
%     skipDistance = sqrt(...
%         (res{curr}.x(start(3:end)) - res{curr}.x(finish(1:end-2))).^2 + ...
%         (res{curr}.y(start(3:end)) - res{curr}.y(finish(1:end-2))).^2);
%     isBigJump  = distance > options.maxJumpPerStep * (start(2:end) - finish(1:end-1));
%     isSmallGap = skipDistance < options.maxJumpPerStep * (start(3:end) - finish(1:end-2));
%     isBigJump = isBigJump & [isSmallGap false];
%     loc = find(isBigJump);
%     for l=loc
%         
%     end
% end

% remove small areas where the subject is hidden
% fprintf('# - removing misdetections\n');
% for curr = 1:options.nSubjects;
%     hidden = res{curr}.id == 0;
%     d = diff([0 hidden 0], 1, 2);
%     start  = find(d > 0);
%     finish = find(d < 0) - 1;
%     loc = find(finish - start + 1 < options.minHiddenDuration & start > 1 & finish < length(hidden));
%     for l=loc
%         res{curr}.x(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.x([start(l)-1, finish(l)+1]), start(l):finish(l));
%         res{curr}.y(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.y([start(l)-1, finish(l)+1]), start(l):finish(l));
%     end
% end

fprintf('# - interpolate position\n');
for curr = 1:options.nSubjects;
    hidden = res{curr}.id == 0;
    d = diff([0 hidden 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;
    loc = find(start > 1 & finish < length(hidden));
    for l=loc
        res{curr}.x(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.x([start(l)-1, finish(l)+1]), start(l):finish(l));
        res{curr}.y(start(l):finish(l)) = interp1([start(l)-1 finish(l)+1], res{curr}.y([start(l)-1, finish(l)+1]), start(l):finish(l));
    end
    if finish(end) == length(hidden)
        res{curr}.x(start(end):finish(end)) = res{curr}.x(start(end)-1);
        res{curr}.y(start(end):finish(end)) = res{curr}.y(start(end)-1);
    end
end

fprintf('# - saving track\n');
filename = [options.output_path options.test.name '.track.mat'];
save(filename, 'res');


%%
for i=1:length(res)
    data.x(i, :) = res{i}.x / options.scale;
    data.y(i, :) = res{i}.y / options.scale;
end
% 
fprintf('# finding hidden zones\n');
grayBkgFrame = rgb2gray(meta.bkgFrame);
grayBkgFrameEq = im2double(histeq(grayBkgFrame)) > .4;
se = strel('disk',4);
se2 = strel('disk',15);
hiddenMask = imclose(imopen(grayBkgFrameEq, se), se2);
%
fprintf('# organizing results\n');
data.nData = size(data.x, 2);
data.time = (1:data.nData) *  dt;
zones.labyrinth = false(size(data.x));
zones.smallNest = false(size(data.x));
zones.bigNest = false(size(data.x));
orig = data;
%
for i=1:options.nSubjects
    x = round(data.x(i, :)); x(x == 0) = 1;
    y = round(data.y(i, :)); y(y == 0) = 1;
    %
    startnan = find(isnan(x), 1); 
    if startnan > 1
        x(startnan:end) = x(startnan - 1);
        y(startnan:end) = y(startnan - 1);
    else
        x(startnan:end) = 1;
        y(startnan:end) = 1;
    end
    %
    ind = sub2ind(size(hiddenMask), y, x);
    zones.hidden(i, :) = hiddenMask(ind);
end
%
scaledCents = cents;
scaledCents.x = cents.x / options.scale;
scaledCents.y = cents.y / options.scale;
scaledCents.area = cents.area / options.scale^2;

data.cents = scaledCents;
data.zones = zones;
data.meta = meta;
data.colors = data.meta.subject.color;
data.meta.hiddenMask = hiddenMask;

filename = [options.output_path options.test.name '.results.mat'];
fprintf('# - saving data\n');
save(filename, 'data');

return;

%%
fprintf('# showing tracks\n');
nchars = RePrintf(nchars, '# - frame %6d/%6d', 0, nFrames);
cmap = lines;
cmap = [1 0 0; 1 0 1; 0 1 1; 1 1 1];
window = 10;
for r=1:nFrames
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d/%6d', r, nFrames);
    
%      m = imsubtract(read(xyloObj, r), bkgFrame);
%      m = imresize(m, options.scale);
%      hold off; imagesc(m); hold on;
      hold off; imagesc(m*0); hold on;
    for curr = 1:options.nSubjects;
        r1 = max(r-window, 1);
        currx = res{curr}.x(r1:r);
        curry = res{curr}.y(r1:r);
        plot(currx, curry, '.-', 'Color', cmap(curr, :));
        nanStart  = find(isnan(currx)) - 1;
        nanFinish = find(isnan(currx)) + 1;
        plot(currx(nanStart(nanStart > 0)), curry(nanStart(nanStart > 0)), 'o', 'Color', cmap(curr, :), 'MarkerSize', 10);
        plot(currx(nanFinish(nanFinish <= window)), curry(nanFinish(nanFinish <= window)), 'x', 'Color', cmap(curr, :), 'MarkerSize', 10, 'LineWidth', 4);
    end
    title(sprintf('frame %6d/%6d', r, nFrames));
    hold off;
    drawnow;
end
fprintf('\n');
return
