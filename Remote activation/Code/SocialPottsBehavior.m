stat = struct();
data = struct();
idx = 1;
for id = 11:18;
    %%
    data(id).ChaseEscape = 0;
    data(id).nContacts = 0;
    data(id).Order = 0;
    for day=2:4
        obj = TrackLoad({id, day}, {'Analysis', 'Contacts', 'Hierarchy'});
        stat(idx).ChaseEscape = obj.Contacts.Behaviors.ChaseEscape;
        stat(idx).Map = sum(obj.Hierarchy.ChaseEscape.map(:));
        stat(idx).old = sum(obj.Hierarchy.ChaseEscape.interactions.PostPredPrey(:));
        stat(idx).pCE = sum(sum(obj.Hierarchy.ChaseEscape.interactions.PostPredPrey ./ (cellfun(@length, obj.Hierarchy.ChaseEscape.interactions.allEvents) + eps)));
        stat(idx).Ik = obj.Analysis.Potts.Ik;
        data(id).ChaseEscape = data(id).ChaseEscape + sum(stat(idx).ChaseEscape);
        data(id).nContacts = data(id).nContacts + length(stat(idx).ChaseEscape);
        data(id).Order = data(id).Order + (2:obj.nSubjects) * (obj.Analysis.Potts.Ik' / sum(obj.Analysis.Potts.Ik));
    end
    idx = idx + 1;

end
%%
for i=1:length(data)
    %%
  %  Ik = stat(i).Ik;
  %  cIk = cumsum(Ik);
  %  pIk = Ik / sum(Ik);
    order = data(i).Order;
    %order = Ik(2) / sum(Ik);
    plot(order,data(i).nContacts, 'x');
    %plot(order,sum(stat(i).pCE), 'x');
    hon;
end
hoff