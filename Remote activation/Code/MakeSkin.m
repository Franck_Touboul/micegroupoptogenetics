function Skin = MakeSkin(N, a, b, c)
if nargin == 2
    uniform = true;
else
    uniform = false;
end    
if nargin == 2 && length(a) > 1
    b = a(2);
    c = a(3);
    a = a(1);
    uniform = false;
end
%angle = (-N/2:N/2) / N * 2 * pi;
angle = (0:N-1) / (N-1) * 2 * pi;
if uniform
    Skin = struct(...
        'Angle', num2cell(angle), ...
        'Distance', num2cell(ones(1, N) * a));
else
    y = b * sin(angle); x = (a+c)/2 * cos(angle)-(a - c)/2;
    angle2 = atan2(y, x);
    Skin = struct(...
        'Angle', num2cell(angle2), ...
        'Distance', num2cell(sqrt(x.^2 + y.^2)));
end

