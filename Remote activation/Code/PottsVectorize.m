function vec = PottsVectorize(obj, me, perm)
if ~exist('perm', 'var')
    perm = 1:obj.nSubjects;
end

u_ = unique(double(me.order));
sz = 0;
for u=u_(:)'
    p = getPerms(1, obj.nSubjects, u); 
    sz = sz + size(p, 1) * double(obj.ROI.nZones)^u;
end
vec = zeros(1, sz);
offset = 1;
for u=u_(:)'
    map = me.order == u;
    w = me.weights(map);
    l = {me.labels{map}};
    %
    svec = double(obj.nSubjects.^(u-1:-1:0));
    
    p = getPerms(1, obj.nSubjects, u); 
    pcoord = svec * (p - 1)' + 1;
    seq=1:length(pcoord); 
    pconv = zeros(1, max(pcoord)); 
    pconv(pcoord) = seq;
    %
    zvec = double(obj.ROI.nZones.^(u-1:-1:0));
    for i=1:length(l)
        [s, o] = sort(perm(l{i}(1, :)));
        coord = (pconv(svec * (s - 1)' + 1) - 1) * double(obj.ROI.nZones)^u + zvec * (l{i}(2, o)' - 1);
        vec(offset + coord) = w(i);
    end
    offset = offset + size(p, 1) * double(obj.ROI.nZones)^u;
end

function V = getPerms(m1, m2, dim)
if dim == 1
    V = (m1:m2)';
    return;
end
V = [];
for i=m1:m2
    v = getPerms(i+1, m2, dim-1);
    if ~isempty(v)
        V = [V; [i * ones(size(v, 1), 1), v]];
    end
end