function histogram = DiscreteProbabilityEstimate(x, nbins, minval, maxval, fixed)

step = (maxval - minval) / nbins;
idx = floor((x - minval) / step) + 1;
idx(x == maxval) = nbins;
valid = idx <= nbins & idx >= 1;
idx = idx(valid);

histogram = zeros(1, nbins);
for i=1:nbins
    histogram(i) = sum(idx == i) + fixed;
end
histogram = histogram / sum(histogram);