function [obj, network] = SocialPairwiseMap(obj, model)
%obj = TrackEnsure(obj, {'BkgImage', 'Analysis'});
%%
markerSize = 4;
%ZoneMap  = [1 7 2 3 4 8 5 10 9 6];
%ZoneMap  = [1 7 2 3 4 10 8 5 9 6];
ZoneMap  = [1 7 4 2 3 10 8 5 9 6];
img = imread('arena.png');

issheltered = false(1, obj.ROI.nZones);
for i=1:obj.ROI.nZones
    name = regexprep(obj.ROI.ZoneNames{i}, '[()]', '');
    p = strcmp(name, obj.ROI.RegionNames);
    if ~any(p)
        m = true(size(obj.ROI.Regions{1}));
        for j=1:obj.ROI.nRegions
            m = m & ~obj.ROI.Regions{j};
        end
    else
        m = obj.ROI.Regions{p};
    end
    
    [x,y] = find(m);
    obj.ROI.ZoneCenters(i, :) = [mean(y), mean(x)];
    issheltered(i) = ~strcmp(name, obj.ROI.ZoneNames{i});
end
% obj.ROI.ZoneCenters(1, :) = [300, 300];
% obj.ROI.ZoneCenters(5, :) = obj.ROI.ZoneCenters(5, :) + [40,  -50];
% obj.ROI.ZoneCenters(8, :) = obj.ROI.ZoneCenters(8, :) + [40,  50];
%%
range = 3;

b=im2double(rgb2gray(obj.BkgImage)); mn = mean(b(:)) - range * std(b(:)); mx = mean(b(:)) + range * std(b(:));
b = min(max((b-mn) / (mx - mn), 0), 1);
cmap = MyMouseColormap;
weights = model.weights(model.order == 2);

byPairs = false;
onMap = false;

%ZoneMap = [1:obj.ROI.nZones];
%%
radios = 10;
ZoneCenters = zeros(obj.nSubjects, obj.ROI.nZones, 2);
index = 0;
for i=1:obj.nSubjects
    for j=ZoneMap
        a = (index / obj.ROI.nZones / obj.nSubjects * 2 * pi);
        ZoneCenters(i, j, 1) = radios * sin(a);
        ZoneCenters(i, j, 2) = radios * cos(a);
        index = index + 1;
    end
end


%%
markerSize = 8;
fontSize = 18;

clf
if onMap
    imshow(b); hold on;
end
hold on;

percentage = .99;
weights = model.weights;
weights(model.order ~= 2) = nan;
[s, o] = sort(weights);
o = o(1:sum(~isnan(s)));
s = s(1:sum(~isnan(s)));

lthresh = s(round(length(s) * percentage));
lmax = s(1);

uthresh = s(round(length(s) * (1-percentage)));
umax = s(end);

lidx = o(s <= lthresh); lidx = lidx(end:-1:1);
uidx = o(s >= uthresh); %uidx = uidx(end:-1:1);

for i=1:length(weights)
    model.order(i) = size(model.labels{i}, 2);
end

valid = [];
marked = [];
[qqq, idx_] = sort(abs(model.weights));
idx_ = idx_(model.order(idx_) == 2);

for idx = idx_
    %%
    r = [model.labels{idx}(2, 1), model.labels{idx}(2, 2)];
    if r(1) == r(2)
        if model.weights(idx) >= uthresh
            fprintf('# pos-marked: location %d (between %d, %d)\n', r(1), model.labels{idx}(1,1), model.labels{idx}(1,2));
        else
            fprintf('# neg-marked: location %d (between %d, %d)\n', r(1), model.labels{idx}(1,1), model.labels{idx}(1,2));
        end
        marked = unique([marked, r(1)]);
    end
    valid = unique([valid, r]);
    for s=1:2
        frx = ZoneCenters(model.labels{idx}(1, s), r(s), 1);
        fry = ZoneCenters(model.labels{idx}(1, s), r(s), 2);
        tox = ZoneCenters(model.labels{idx}(1, 3-s), r(3-s), 1);
        toy = ZoneCenters(model.labels{idx}(1, 3-s), r(3-s), 2);
        if model.weights(idx) >= 0
            w = abs(model.weights(idx)) * 1;
            if w > 0
            plot([frx frx + (tox-frx)/2], [fry fry + (toy-fry)/2], '-', 'Color', cmap(model.labels{idx}(1, s), :), 'LineWidth', w);
            end
        else
            w = abs(model.weights(idx)) * 1;
            if w > 0
            plot([frx frx + (tox-frx)/2], [fry fry + (toy-fry)/2], ':', 'Color', cmap(model.labels{idx}(1, s), :), 'LineWidth', w);
            end
        end
    end
end

zmap = MyZonesColormap;
weightsIndep = model.weights(model.order == 1);
labelsIndep = model.labels(model.order == 1);
labelsIndep = [labelsIndep{:}];
for i=1:obj.nSubjects
    for j=1:obj.ROI.nZones
        if j==model.neutralZone
            continue;
        end
        w = weightsIndep(labelsIndep(1, :) == i & labelsIndep(2, :) == j);
        currMarkerSize = abs(markerSize * w);
        currMarkerSign = sign(markerSize * w);
        currMarkerSign = -1;
        currMarkerSize = 14;
        %if ~issheltered(j)
        if currMarkerSign > 0
            plot(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2), 'o', 'MarkerEdgeColor', zmap(j, :), 'MarkerFaceColor', 'w', 'MarkerSize', currMarkerSize, 'LineWidth', 2);
            if currMarkerSize < 10
                text(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2),num2str(find(ZoneMap == j)), 'color', 'w', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center')
            else
                text(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2),num2str(find(ZoneMap == j)), 'color', zmap(j, :), 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center')
            end
        else
            if currMarkerSize > 0
            plot(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2), 'o', 'MarkerEdgeColor', zmap(j, :), 'MarkerFaceColor', zmap(j, :), 'MarkerSize', currMarkerSize, 'LineWidth', 2);
            if currMarkerSize < 10
                text(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2),num2str(find(ZoneMap == j)), 'color', 'w', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center')
            else
                text(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2),num2str(find(ZoneMap == j)), 'color', 'w', 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center')
            end
            end
        end
        %else
        %    plot(ZoneCenters(i, j, 1), ZoneCenters(i, j, 2), 's', 'MarkerEdgeColor', cmap(i, :), 'MarkerFaceColor', zmap(j, :), 'MarkerSize', currMarkerSize);
        %end
    end
end
axis off;
hoff;
set(gcf, 'Color', 'w');
axis equal

%%
network.w = [];
for i=1:length(model.weights)
    if size(model.labels{i}, 2) == 2
        z1 = model.labels{i}(1, 1);
        z2 = model.labels{i}(1, 2);
        l1 = model.labels{i}(2, 1);
        l2 = model.labels{i}(2, 2);
        a = (z1 - 1) * obj.ROI.nZones + l1;
        b = (z2 - 1) * obj.ROI.nZones + l2;
        network.w(a , b) = model.weights(i);
        network.w(b , a) = model.weights(i);
    end
end
%%
if (1==2)
    %%
    p = 'Graphs/SocialInteractionMap/';
    mkdir(p);
    figure(1);
    saveFigure([p '' obj.FilePrefix '']);
end