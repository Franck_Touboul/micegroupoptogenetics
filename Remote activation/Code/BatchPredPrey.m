experiments = {...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam01',...
    };

%%
all = {};
parfor i=1:length(experiments)
    curr = {};
    for day = 1:4;
        try
        prefix = sprintf(experiments{i}, day);
        fprintf('# -> %s\n', prefix);
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions', 'Common'});
            obj = SocialAnalysePredPreyModel(obj);
            curr = obj.Interactions.PredPrey;
            
            matPred = curr.PostPred - curr.PostPred';
            mat = matPred;
%         mat(binotest(all{day}.PostPred, all{day}.PostPred + all{day}.PostPred') & ...
%             binotest(all{day}.PostPrey, all{day}.PostPrey + all{day}.PostPrey')) = 0;
        mat(binotest(curr.PostPred, curr.PostPred + curr.PostPred')) = 0;
        mat = mat .* (mat > 0);
        
        szc = sum(curr.Contacts);
        sz = sum(curr.PostPred, 2)';
        
        [rank, removed] = TopoFeedbackArcSetOrder(mat);
        [~, bg] = SocialGraph(obj, mat, removed, rank, sz);
        for k=1:length(bg.nodes)
            bg.nodes(k).ID = sprintf('%s(%d/%d)', bg.nodes(k).ID, sz(k), szc(k));
        end
        
        g = biograph.bggui(bg);
        f = get(g.biograph.hgAxes, 'Parent');
        print(f, [obj.OutputPath 'temp.' prefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');
        f = imread([obj.OutputPath 'temp.' prefix '.SocialAnalysePredPreyModel_v2.png']);
        f = ImgAutoCrop(f);
        file = [obj.OutputPath 'pred.' prefix '.SocialAnalysePredPreyModel_v2.png'];
        delete([obj.OutputPath 'temp.' prefix '.SocialAnalysePredPreyModel_v2.png']);
        imwrite(f, file);

        close(g.hgFigure);
            
        catch me
           fprintf(['#   . FAILED! ' me.message '\n']); 
           for j=1:length(me.stack)
               fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
           end
        end
    end
end