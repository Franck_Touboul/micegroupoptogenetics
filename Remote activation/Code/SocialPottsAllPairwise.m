%me = obj.Analysis.Potts.Model{3};
nZones = obj.ROI.nZones - 1;
mat = ones(obj.nSubjects * nZones) * nan;
for i=1:length(me.weights)
    if me.order(i) == 2 
        s1 = me.labels{i}(1, 1);
        l1 = me.labels{i}(2, 1);
        s2 = me.labels{i}(1, 2);
        l2 = me.labels{i}(2, 2);
        mat((s1 - 1) * nZones + l1 - 1, (s2 - 1) * nZones + l2 - 1) = me.weights(i);
        mat((s2 - 1) * nZones + l2 - 1, (s1 - 1) * nZones + l1 - 1) = me.weights(i);
    end
end
scale = [-max(abs(mat(:))) max(abs(mat(:)))];
MyImagesc(mat, true, scale, [-10 -5 0 5 10]);
axis square
labels = {};
for i=1:obj.nSubjects
    for z=1:nZones
        labels{(i-1)*nZones + z} = num2str(z+1);
    end
    vert_line(nZones * i + .5, 'color', 'k');
    horiz_line(nZones * i + .5, 'color', 'k');
end
set(gca, 'xtick', 1:obj.nSubjects*nZones, 'xticklabel', labels)
set(gca, 'ytick', 1:obj.nSubjects*nZones, 'yticklabel', labels)
