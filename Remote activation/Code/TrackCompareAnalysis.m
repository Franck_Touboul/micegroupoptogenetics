TrackDefaults
old = load('Exp4day2_old.social.mat');
new = load('Exp4day2.social.mat');
%new.social = kalmanFilterTrack(new.social, 8, 1);
%new.social = social;
%%
options.x1 = -40;
options.x2 =  40;
options.y1 = -33;
options.y2 =  33;
options.w = options.x2 - options.x1;
options.h = options.y2 - options.y1;

xyloObj = mmreader(options.MovieFile);
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;
nFrames = xyloObj.NumberOfFrames;

dt = new.social.time(2) - new.social.time(1);

%%
% c = zeros(options.nSubjects);
% l = zeros(options.nSubjects, options.nSubjects, 2);
%
% for i=1:options.nSubjects
%     for j=1:options.nSubjects
%         for k=1:2
%             if k == 1
%                 a = old.social.x(i, :);
%                 b = new.social.x(j, :);
%             else
%                 a = old.social.y(i, :);
%                 b = new.social.y(j, :);
%             end
%             N=max(length(a), length(b));
%             a(isnan(a)) = 0;
%             b(isnan(b)) = 0;
%             a_ = padarray(a, [0 N - length(a)], 'pre');
%             b_ = padarray(b, [0 N - length(b)], 'post');
%             x = xcorr(a_, b_);
%             [v,n] = max(x);
%             d = n-N;
%             if d > 0
%                 na = a_(d+1:end);
%                 nb = b_(1:N-d);
%             else
%                 nb = b_(abs(d)+1:end);
%                 na = a_(1:N-abs(d));
%             end
%             c(i, j) = c(i, j) + corr(na', nb');
%             l(i, j, k) = d;
%         end
%     end
% end
%%
map = [2, 4, 1, 3];
delay = 17653; % 11:55 mins

fold = old;
fold.social.x = [zeros(options.nSubjects, delay) old.social.x(map, :)];
fold.social.y = [zeros(options.nSubjects, delay) old.social.y(map, :)];
fold.social.zones.all = [-1 * ones(options.nSubjects, delay) old.social.zones.all(map, :)];

n = min(size(new.social.x, 2), size(fold.social.x, 2));
new.social.x = new.social.x(:, 1:n);
new.social.y = new.social.y(:, 1:n);
fold.social.x = fold.social.x(:, 1:n);
fold.social.y = fold.social.y(:, 1:n);
fold.social.zones.all = fold.social.zones.all(:, 1:n);

sx = new.social.x(1, :);
sy = new.social.y(1, :);
ex = fold.social.x(1, :);
ey = fold.social.y(1, :);

validity.social = ~(new.social.zones.hidden | new.social.zones.all == 6);
validity.etho =  ~(fold.social.zones.all == 5 | fold.social.zones.all == 4 | fold.social.zones.all == 6);
valid = validity.social & validity.etho;

px = polyfit(ex(valid(1, :)), sx(valid(1, :)), 1);
py = polyfit(ey(valid(1, :)), sy(valid(1, :)), 1);

fold.social.x = fold.social.x * px(1) + px(2);
fold.social.y = fold.social.y * py(1) + py(2);
% fold.social.x = (fold.social.x - options.x1) / options.w * xyloObj.Width;
% fold.social.y = (1 - (fold.social.y - options.y1) / options.h) * xyloObj.Height;

% fold.social.x = fold.social.x * px(1) + px(2);
% fold.social.y = fold.social.y * py(1) + py(2);


%cdist = sqrt((fold.social.x - new.social.x) .^ 2 + (fold.social.y - new.social.y) .^ 2)  / abs(px(1));
cdist = sqrt((fold.social.x(valid) - new.social.x(valid)) .^ 2 + (fold.social.y(valid) - new.social.y(valid)) .^ 2)  / abs(px(1));

%cdist2 = sqrt((fold.social.x - new.social.x) .^ 2 + (fold.social.y - new.social.y) .^ 2)  / abs(px(1));

%%
subplot(2,1,1);
[h,x] = hist(fold.social.zones.all(:), 0:max(fold.social.zones.all(:))); 
bar(x, h/sum(h))
subplot(2,1,2); 
[h,x] = hist(new.social.zones.all(:), 0:max(new.social.zones.all(:)));
bar(x, h/sum(h))
%%
subplot(2,2,1:2);
[cdist_hist, x] = hist(cdist(:) / ((abs(px(1)) + abs(py(1))) / 2) , 100);
bar(x, cdist_hist' / sum(cdist_hist), 'EdgeColor', 'none');
%, 'EdgeColor', 'none'
title('distances histogram [cm]');
prettyPlot('location mismatch histogram [cm]', 'distance[cm]');
%
% subplot(2,2,3);
% 
% bar([std(diff(fold.social.x, 1, 2), 0, 2), std(diff(new.social.x, 1, 2), 0, 2)]);
% title('x movement std');
% subplot(2,2,4);
% bar([std(diff(fold.social.y, 1, 2), 0, 2), std(diff(new.social.y, 1, 2), 0, 2)]);
% title('y movement std');
%a()
%
subplot(2,2,4);

noise.std.etho   = std(sqrt(diff(fold.social.x, 1, 2).^2 + diff(fold.social.y, 1, 2).^2), 0, 2);
%noise.std.social = std(sqrt(diff(new.social.x, 1, 2).^2 + diff(new.social.y, 1, 2).^2), 0, 2);
noise.std.social = std(new.social.speed, 0, 2);

noise.etho   = mean(sqrt(diff(fold.social.x, 1, 2).^2 + diff(fold.social.y, 1, 2).^2), 2);
%noise.social = mean(sqrt(diff(new.social.x, 1, 2).^2 + diff(new.social.y, 1, 2).^2), 2);
noise.social = mean(new.social.speed, 2);

fold.social.speed = sqrt(diff(fold.social.x, 1, 2).^2 + diff(fold.social.y, 1, 2).^2) / dt;

cvalid = ~isnan(fold.social.x);

fprintf('# computing speeds\n');
fprintf('# - social\n');
for i=1:options.nSubjects
%     curr = repmat(1:options.nSubjects', size(fold.social.x, 2), 1)' == i;
%     noise.etho(i)   = mean(sqrt(diff(fold.social.x(cvalid & curr)', 1, 2).^2 + diff(fold.social.y(cvalid & curr)', 1, 2).^2), 2);
%     noise.social(i) = mean(sqrt(diff(new.social.x(cvalid & curr)', 1, 2).^2 + diff(new.social.y(cvalid & curr)', 1, 2).^2), 2);
    c = conv([0 validity.social(i, :) 0], [1 -1]);
    start  = find(c > 0) - 1;
    finish = find(c < 0) - 2;
    len = finish - start + 1;
    curr_speed = [];
    for j=1:length(start)
        if len(j) > 1
%             curr_speed = [curr_speed, sqrt(...
%                 diff(new.social.x(i, start(j):finish(j)), 1, 2).^2 + ...
%                 diff(new.social.y(i, start(j):finish(j)), 1, 2).^2)];
            curr_speed = [curr_speed, new.social.speed(i, start(j):finish(j)) / px(1)];
        end
    end
    speed.social(i) = mean(curr_speed);
    speed.stderr.social(i) = stderr(curr_speed);
end
fprintf('# - etho\n');
for i=1:options.nSubjects
%     curr = repmat(1:options.nSubjects', size(fold.social.x, 2), 1)' == i;
%     noise.etho(i)   = mean(sqrt(diff(fold.social.x(cvalid & curr)', 1, 2).^2 + diff(fold.social.y(cvalid & curr)', 1, 2).^2), 2);
%     noise.social(i) = mean(sqrt(diff(new.social.x(cvalid & curr)', 1, 2).^2 + diff(new.social.y(cvalid & curr)', 1, 2).^2), 2);
    c = conv([0 validity.etho(i, :) 0], [1 -1]);
    start  = find(c > 0) - 1;
    finish = find(c < 0) - 2;
    len = finish - start + 1;
    curr_speed = [];
    for j=1:length(start)
        if len(j) > 1
%             curr_speed = [curr_speed, sqrt(...
%                 diff(fold.social.x(i, start(j):finish(j)), 1, 2).^2 + ...
%                 diff(fold.social.y(i, start(j):finish(j)), 1, 2).^2)];
            curr_speed = [curr_speed, fold.social.speed(i, start(j):finish(j)) / px(1)];
        end
    end
    speed.etho(i) = mean(curr_speed);
    speed.stderr.etho(i) = stderr(curr_speed);
end

%barweb([speed.etho; speed.social]', [speed.stderr.etho; speed.stderr.social]', 'hist');
bar([speed.etho; speed.social]');
colormap(new.social.meta.subject.centerColors)
prettyPlot('average speed', 'subject', 'average speed [cm/sec]');

% noise.etho.x = std(diff(fold.social.x, 1, 2), 0, 2);
% noise.etho.y = std(diff(fold.social.y, 1, 2), 0, 2);
% 
% noise.social.x = std(diff(new.social.x, 1, 2), 0, 2);
% noise.social.y = std(diff(new.social.y, 1, 2), 0, 2);
% 
% for i=1:options.nSubjects
%     ellipse(noise.etho.x(i), noise.etho.y(i), 0, 0, 0, new.social.meta.subject.centerColors(i, :))
%     hold on;
%     ellipse(noise.social.x(i), noise.social.y(i), 0, 0, 0, new.social.meta.subject.centerColors(i, :))
% end
% axis equal
% hold off;
return;
%%
i = 2;
c = 4;
currx = fold.social.x(i, :);
curry = fold.social.y(i, :);
zones = fold.social.zones.all(i, :);
plot(currx(zones == c), curry(zones == c), '.');

axis([0 vidWidth 0, vidHeight]);
%axis([-34 32 -28, 28]);


%%
fprintf('# saving trajectories for comparisson');
for curr = 1:4;
for i = 1:20;
    c = conv([0 new.social.zones.all(curr, :) == 0 0], [1 -1]);
    start  = find(c > 0) - 1;
    finish = find(c < 0) - 2;
    len = finish-start+1;
    [s, is] = sort(len, 2, 'descend');
    r = start(is(i)):finish(is(i));
    plot(fold.social.x(curr, r), fold.social.y(curr,r), '.:', new.social.x(curr,r), new.social.y(curr,r), '.:')
    xlabel('x coordinate')
    ylabel('y coordinate')
    axis([0 vidWidth 0, vidHeight]);
    %drawnow; pause(.5)
    saveFigure(sprintf('Res/TrackCompareAnalysis.Full.mouse%d.track%d', curr, i));
end
end
%%
%for i=1:options.nSubjects
i=1;   

dx = fold.social.x - new.social.x;
dx = dx(i, :);
dy = fold.social.y - new.social.y;
dy = dy(i, :);
cdist = sqrt(dx .^ 2 + dy .^ 2)  / abs(px(1));

    c = conv([0 (cdist > 10) & valid(i, :)  0], [1 -1]);
    start  = find(c > 0) - 1;
    finish = find(c < 0) - 2;
    
    
    maxdist = []
    for j=1:length(start)    
        maxdist(j) = max(cdist(start(j):finish(j)));
    end
    [s, is] = sort(maxdist, 2, 'descend');
    index = 1;
    for j=is
        fprintf('%03d. %s - %s    (%5f) \n', index, sec2time_v2(start(j) / 25), sec2time_v2(finish(j) / 25), max(cdist(start(j):finish(j))));
        index = index + 1;
        if index > 20
            break;
        end
    end
%end

%%
%for i=1:options.nSubjects
i=3;   

speed.all.etho = [0, sqrt(diff(fold.social.x(i, :)).^2 + diff(fold.social.y(i, :)).^2)];
speed.all.social = [0, sqrt(diff(new.social.x(i, :)).^2 + diff(new.social.y(i, :)).^2)];

cdist = abs(speed.all.social - speed.all.etho);

    c = conv([0 cdist > 10 & valid(i, :) & [true valid(i, 1:end-1)] 0], [1 -1]);
    start  = find(c > 0) - 1;
    finish = find(c < 0) - 2;
    
    
    maxdist = []
    for j=1:length(start)    
        maxdist(j) = max(cdist(start(j):finish(j)));
    end
    [s, is] = sort(maxdist, 2, 'descend');
    %%
    index = 1;
    for j=is
        if index < 10
            index = index + 1;
            continue;
        end
        fprintf('%03d. %s - %s    (%5f) \n', index, sec2time_v2(start(j) / 25), sec2time_v2(finish(j) / 25), max(cdist(start(j):finish(j))));

        r0 = max(start(j)-25, 1):start(j);
        r1 = finish(j):min(finish(j)+25, size(fold.social.y, 2));
        r = start(j):finish(j);
%        axis([0 vidWidth 0, vidHeight]);
        for cr = max(start(j)-25, 1):min(finish(j)+25, size(fold.social.y, 2));
            origm = myMMReader('\\eladbackup\forkosh\SocialMice\Movies\Exp4 day 2.mpg', cr);
            imagesc(origm);
            hold on;
            plot(fold.social.x(i, r0), fold.social.y(i, r0), '.', new.social.x(i, r0),  new.social.y(i, r0), '.');
            plot(fold.social.x(i, r1), fold.social.y(i, r1), '.', new.social.x(i, r1),  new.social.y(i, r1), '.');
            plot(fold.social.x(i, r), fold.social.y(i, r), 'x', new.social.x(i, r),  new.social.y(i, r), 'x', 'MarkerSize', 14);
            hold off;
            pause(0.1);
            drawnow;
        end
        fprintf('done\n');
        
        
        drawnow;
        pause;
        index = index + 1;
        if index > 20
            break;
        end
    end
%end

%%
curr = 3;
basename = ['Res/compare_' num2str(curr)];

xyloObj = myMMReader(options.MovieFile);
nframes = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;
dt = 1/xyloObj.FrameRate;


%hf = fopen(['Res/' options.test.name '.ass'], 'w');
hf = fopen([basename '.ass'], 'w');
fprintf(hf, '[Script Info]\n');
fprintf(hf, 'Title: Default Aegisub file\n');
fprintf(hf, 'ScriptType: v4.00+\n');
fprintf(hf, 'WrapStyle: 0\n');
fprintf(hf, ['PlayResX: ' num2str(vidWidth) '\n']);
fprintf(hf, ['PlayResY: ' num2str(vidHeight) '\n']);
fprintf(hf, 'ScaledBorderAndShadow: no\n');
fprintf(hf, 'Last Style Storage: Default\n');
fprintf(hf, ['Video File: ' options.MovieFile '\n']);
fprintf(hf, 'Video Aspect Ratio: 0\n');
%fprintf(hf, 'Video Zoom: 4\n');
%fprintf(hf, 'Video Position: 175\n');

fprintf(hf, '[V4+ Styles]\n');
fprintf(hf, 'Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding\n');
fprintf(hf, 'Style: Default,Arial,20,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,2,0,2,10,10,10,1\n');

fprintf(hf, '[Events]\n');
fprintf(hf, 'Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text\n');


options.sensitivity = 10;

time = cell(1, options.nSubjects);
colorhex = cell(1, options.nSubjects);

prev.x = zeros(1, options.nSubjects);
prev.y = zeros(1, options.nSubjects);

prev.x(curr) = new.social.x(curr, 1);
prev.y(curr) = new.social.y(curr, 1);
time{curr} = sec2time_v2(0);

fprev.x = zeros(1, options.nSubjects);
fprev.y = zeros(1, options.nSubjects);

fprev.x(curr) = fold.social.x(curr, 1);
fprev.y(curr) = fold.social.y(curr, 1);
ftime{curr} = sec2time_v2(0);

color = new.social.meta.subject.centerColors(curr, :);
colorhex{curr} = [dec2hex(floor(color(1) * 255)) dec2hex(floor(color(2) * 255)) dec2hex(floor(color(3) * 255))];

fprintf('# - frame ');
nchars = RePrintf('%6d/%6d', 1, size(new.social.x, 2));
for i=2:size(new.social.x, 2)
    if mod(i, 312) == 0
        nchars = RePrintf(nchars, '%6d/%6d', i, size(new.social.x, 2));
    end
    dist = sqrt((new.social.x(:, i) - prev.x') .^ 2 + (new.social.y(:, i) - prev.y') .^ 2);
    fdist = sqrt((fold.social.x(:, i) - fprev.x') .^ 2 + (fold.social.y(:, i) - fprev.y') .^ 2);
    refresh = max(dist)  > options.sensitivity;
    if dist(curr) > options.sensitivity
        start = time{curr};
        time{curr} = sec2time_v2(new.social.time(i));
        finish = time{curr};
        posx = num2str(prev.x(curr));
        posy = num2str(prev.y(curr));
        label = new.social.zones.labels{new.social.zones.all(curr, i-1) + 1};
        fprintf(hf, ...
            ['Dialogue: ' num2str(curr) ',' start ',' finish ',Default,,0000,0000,0000,,{\\pos(' posx ',' posy ')\\b1\\c&H' colorhex{curr} '&\\fs30}(' label ')\n']);
        prev.x(curr) = new.social.x(curr, i);
        prev.y(curr) = new.social.y(curr, i);
    end
    
    if fdist(curr) > options.sensitivity
        fstart = ftime{curr};
        ftime{curr} = sec2time_v2(new.social.time(i));
        ffinish = ftime{curr};
        posx = num2str(fprev.x(curr));
        posy = num2str(fprev.y(curr));
        label = fold.social.zones.labels{fold.social.zones.all(curr, i-1) + 1};
        fprintf(hf, ...
            ['Dialogue: ' num2str(curr) ',' fstart ',' ffinish ',Default,,0000,0000,0000,,{\\pos(' posx ',' posy ')\\b1\\c&H' colorhex{curr} '&\\fs30}[' label ']\n']);
        fprev.x(curr) = fold.social.x(curr, i);
        fprev.y(curr) = fold.social.y(curr, i);
    end

end
nchars = RePrintf(nchars, '%6d/%6d', i, size(new.social.x, 2));
fprintf('\n');

start = time{curr};
time{curr} = sec2time_v2(new.social.time(i));
finish = time{curr};
posx = num2str(new.social.x(curr, i));
posy = num2str(new.social.y(curr, i));
label = new.social.zones.labels{new.social.zones.all(curr, i-1) + 1};
fprintf(hf, ...
    ['Dialogue: 0,' start ',' finish ',Default,,0000,0000,0000,,{\\pos(' posx ',' posy ')\\b1\\c&' colorhex{curr} '&\\fs30}' label '\n']);

fstart = ftime{curr};
ftime{curr} = sec2time_v2(new.social.time(i));
ffinish = ftime{curr};
posx = num2str(fold.social.x(curr, i));
posy = num2str(fold.social.y(curr, i));
label = fold.social.zones.labels{fold.social.zones.all(curr, i-1) + 1};
fprintf(hf, ...
    ['Dialogue: 0,' start ',' finish ',Default,,0000,0000,0000,,{\\pos(' posx ',' posy ')\\b1\\c&' colorhex{curr} '&\\fs30}' label '\n']);

fclose(hf);

%%
%%
%for i=1:options.nSubjects
i=3;   

valid = validity.social(i, :) & validity.etho(i, :);

fx = fold.social.x(i, :); fy = fold.social.y(i, :);
nx = new.social.x(i, :);  ny = new.social.y(i, :);

speed.all.etho = [0, sqrt(diff(fx).^2 + diff(fy).^2)];
speed.all.social = [0, sqrt(diff(nx).^2 + diff(ny).^2)];

cdist = abs(speed.all.social);

    c = conv([0 cdist > 10 & valid & [true valid(1:end-1)] 0], [1 -1]);
    start  = find(c > 0) - 1;
    finish = find(c < 0) - 2;
    
    
    maxdist = [];
    maxindex = [];
    for j=1:length(start)    
        [maxdist(j), maxindex(j)] = max(cdist(start(j):finish(j)));
        maxindex(j) = maxindex(j) + start(j) - 1;
    end
    [s, is] = sort(maxdist, 2, 'descend');
    %%
    index = 1;
    for j=is
        if index < 1
            index = index + 1;
            continue;
        end
        fprintf('%03d. %s - %s    (%5f) ', index, sec2time_v2(start(j) / 25), sec2time_v2(finish(j) / 25), max(cdist(start(j):finish(j))));
        r0 = max(start(j)-25, 1):start(j);
        r1 = finish(j):min(finish(j)+25, size(fold.social.y, 2));
        r = start(j):finish(j);
%        axis([0 vidWidth 0, vidHeight]);
        [m, v] = max(cdist(start(j):finish(j)));
clf
subplot(1,2,1);
            plot(fold.social.x(i, r0), vidHeight - fold.social.y(i, r0), '.');
            hold on;
            plot(fold.social.x(i, r1), vidHeight - fold.social.y(i, r1), '.');
            plot(fold.social.x(i, r), vidHeight - fold.social.y(i, r), '.', 'MarkerSize', 14);
            plot(fold.social.x(i, start(j) + v - 2), vidHeight - fold.social.y(i, start(j) + v - 2), 'o', 'MarkerSize', 14);
            plot(fold.social.x(i, start(j) + v - 1), vidHeight - fold.social.y(i, start(j) + v - 1), 'x', 'MarkerSize', 14);
            hold off;
            axis([0 vidWidth 0, vidHeight]);

            subplot(1,2,2);
            plot(new.social.x(i, r0), vidHeight - new.social.y(i, r0), '.');
            hold on;
            plot(new.social.x(i, r1), vidHeight - new.social.y(i, r1), '.');
            plot(new.social.x(i, r), vidHeight - new.social.y(i, r), '.', 'MarkerSize', 14);
            plot(new.social.x(i, start(j) + v - 2), vidHeight - new.social.y(i, start(j) + v - 2), 'o', 'MarkerSize', 14);
            plot(new.social.x(i, start(j) + v - 1), vidHeight - new.social.y(i, start(j) + v - 1), 'x', 'MarkerSize', 14);
            hold off;
            axis([0 vidWidth 0, vidHeight]);


%         for cr = max(start(j)-25, 1):min(finish(j)+25, size(fold.social.y, 2));
%             %origm = myMMReader('\\eladbackup\forkosh\SocialMice\Movies\Exp4 day 2.mpg', cr);
%             %imagesc(origm);
%             hold on;
%             plot(fold.social.x(i, r0), fold.social.y(i, r0), '.', new.social.x(i, r0),  new.social.y(i, r0), '.');
%             plot(fold.social.x(i, r1), fold.social.y(i, r1), '.', new.social.x(i, r1),  new.social.y(i, r1), '.');
%             plot(fold.social.x(i, r), fold.social.y(i, r), 'x', new.social.x(i, r),  new.social.y(i, r), 'x', 'MarkerSize', 14);
%             hold off;
%             pause(0.1);
%             drawnow;
%         end
        fprintf('done\n');
        
        
        drawnow;
        pause;
        index = index + 1;
        if index > 50
            break;
        end
    end
%end
