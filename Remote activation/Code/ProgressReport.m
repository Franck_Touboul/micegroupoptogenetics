function ProgressReport(curr, total)
persistent count 
if nargin > 0
    percent = curr/total * 100;
    integer = floor(percent);
    frac = floor((percent - integer) * 10);
    count = Reprintf(count, '(%3d.%1d%%)', integer, frac);
else
    count = 0;
end
    
