data = obj.zones(:, obj.valid)';
data = [data(1:end-1, :), data(2:end, :)];

nvals = obj.ROI.nZones;
n = 2;
%%
me = MEFeaturesMarkov(data, nvals, n);
me = METrain(me, data);
