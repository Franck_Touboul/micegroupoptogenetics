%load('Models/cage 1 day 1 analysis (Trial     1)-Arena 1-Potts(2)');
load('Models/45min-Potts(2)');
%%
options.maxWidth = 30;

clf

attractors.w = zeros(options.nSubjects);
attractors.count = zeros(options.nSubjects);
repulsors.w = zeros(options.nSubjects);
repulsors.count = zeros(options.nSubjects);

for s1=1:options.nSubjects
    for s2=s1+1:options.nSubjects
        for i=1:length(me.labels)
            curr = me.labels{i};
            if size(curr, 2) == 2 && curr(1,1) == s1 && curr(1,2) == s2
                if curr(2,1) == curr(2,2)
                    attractors.w(s1, s2) = attractors.w(s1, s2) + abs(me.weights(i));
                    attractors.count(s1, s2) = attractors.count(s1, s2) + 1;
                else
                    repulsors.w(s1, s2) = repulsors.w(s1, s2) + abs(me.weights(i));
                    repulsors.count(s1, s2) = repulsors.count(s1, s2) + 1;
                end
            end
        end
    end
end
attractors.w = attractors.w ./ attractors.count;
repulsors.w = repulsors.w ./ repulsors.count;
weights = attractors.w - repulsors.w;

sqrNum = sqrt(options.nSubjects);
for s1=1:options.nSubjects
    from = [floor((s1 - 1) / sqrNum) + 1; mod((s1 - 1), sqrNum)+1];
    for s2=s1+1:options.nSubjects
        to = [floor((s2 - 1) / sqrNum) + 1; mod((s2 - 1), sqrNum)+1];
        x = [from(1), to(1)];
        y = [from(2), to(2)];
        if weights(s1, s2) > 0
            plot(x, y, '-', 'Color', [78 205 196]/255, 'LineWidth', abs(weights(s1, s2)) * options.maxWidth); hold on;
        else
            plot(x, y, '-', 'Color', [196 77 88]/255, 'LineWidth', abs(weights(s1, s2)) * options.maxWidth); hold on;
        end
        plot(x, y, 'o', 'MarkerFaceColor', [85 98 112]/355, 'MarkerEdgeColor', [85 98 112]/355, 'MarkerSize', 30)
        text(x(1), y(1), num2str(s1), 'Color', 'w', 'FontSize', 18, 'HorizontalAlignment', 'center');
        text(x(2), y(2), num2str(s2), 'Color', 'w', 'FontSize', 18, 'HorizontalAlignment', 'center');
    end
end

axis([0.5 sqrNum+.5, 0.5, sqrNum+.5]);