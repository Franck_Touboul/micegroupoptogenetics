function model = GeneratePerdPreyModel_double(input)
model = struct();
model.nstates = 10;

model.UseSpeedHistogram = true;


model.Histogram.minval = min(input(1,:));
model.Histogram.maxval = max(input(1,:));
model.Histogram.nBins = 20;
model.Histogram.dbin = (model.Histogram.maxval - model.Histogram.minval) / model.Histogram.nBins;

model.Speed.Histogram.minval = min(input(3,:));
model.Speed.Histogram.maxval = max(input(3,:));
model.Speed.Histogram.nBins = 100;
model.Speed.Histogram.dbin = ...
    (model.Speed.Histogram.maxval - model.Speed.Histogram.minval) / model.Speed.Histogram.nBins;

%%
model = PPModelDiscretePDFEstimation(input, model);
for s=1:model.nstates
    model.states(s).Speed.histogram = model.all.Speed.histogram;
end

%% state 1: avoidance
s = 1;
%model.states(s).histogram = ones(1, model.Histogram.nBins) / model.Histogram.nBins;
model.states(s).histogram = model.all.histogram;
model.states(s).contact = 0;

model.states(s).posterior = @PPModelDiscretePDF;
%model.states(s).estimate  = @PPModelDiscretePDFEstimation;
model.states(s).estimate  = @(x, model, state) model;

model.states(s).title = '-';
model.states(s).type = 0;

%% state 2: predetor(1)
s = 2;
model.states(s).ExpBase.alpha = 2;
model.states(s).ExpBase.theta = 0;
model.states(s).ExpBase.m = .2;

model.states(s).posterior = @PPModelExpBasePDF;
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'pred';
model.states(s).type = 1;

%% state 3: predetor(2)
s = 3;
model.states(s).ExpBase.alpha = 4;
model.states(s).ExpBase.theta = 0;
model.states(s).ExpBase.m = .4;

model.states(s).posterior = @PPModelExpBasePDF;
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'pred';
model.states(s).type = 1;

%% state 4: prey(1)
s = 4;
model.states(s).ExpBase.alpha = 2;
model.states(s).ExpBase.theta = pi;
model.states(s).ExpBase.m = .2;

model.states(s).posterior = @PPModelExpBasePDF;
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'prey';
model.states(s).type = 1;

%% state 4: prey(2)
s = 5;
model.states(s).ExpBase.alpha = 4;
model.states(s).ExpBase.theta = pi;
model.states(s).ExpBase.m = .4;

model.states(s).posterior = @PPModelExpBasePDF;
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'prey';
model.states(s).type = 1;

%% state 4: contact
s = 6;
%model.states(s).histogram = ones(1, model.Histogram.nBins) / model.Histogram.nBins;
model.states(s).histogram = model.all.histogram;
model.states(s).contact = 1;

model.states(s).posterior = @PPModelDiscretePDF;
%model.states(s).estimate  = @PPModelDiscretePDFEstimation;
model.states(s).estimate  = @(x, model, state) model;

model.states(s).title = 'cont';
model.states(s).type = 0;

%% state 7: predetor
s = 7;
model.states(s).ExpBase.alpha = 2;
model.states(s).ExpBase.theta = 0;
model.states(s).ExpBase.m = .2;

model.states(s).posterior = @PPModelExpBasePDF;
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'pred';
model.states(s).type = 1;

%% state 8: predetor
s = 8;
model.states(s).ExpBase.alpha = 4;
model.states(s).ExpBase.theta = 0;
model.states(s).ExpBase.m = .4;

model.states(s).posterior = @PPModelExpBasePDF;
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'pred';
model.states(s).type = 1;

%% state 6: prey
s = 9;
model.states(s).ExpBase.alpha = 2;
model.states(s).ExpBase.theta = pi;
model.states(s).ExpBase.m = .2;

model.states(s).posterior = @PPModelExpBasePDF;
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'prey';
model.states(s).type = 1;

%% state 6: prey
s = 10;
model.states(s).ExpBase.alpha = 4;
model.states(s).ExpBase.theta = pi;
model.states(s).ExpBase.m = .4;

model.states(s).posterior = @PPModelExpBasePDF;
model.states(s).estimate  = @PPModelExpBasePDFEstimation;
model.states(s).title = 'prey';
model.states(s).type = 1;

%%
persistence = 10;
pst = 1-1/persistence;
ptr = 1/persistence;
pst = 1;
ptr = 1;
model.trans = [...
    pst ptr ptr ptr ptr ptr 0.0 0.0 0.0 0.0; % -
    0.0 pst 0.0 0.0 0.0 ptr 0.0 0.0 0.0 0.0; % predator (1)
    0.0 0.0 pst 0.0 0.0 ptr 0.0 0.0 0.0 0.0; % predator (2)
    0.0 0.0 0.0 pst 0.0 ptr 0.0 0.0 0.0 0.0; % prey (1)
    0.0 0.0 0.0 0.0 pst ptr 0.0 0.0 0.0 0.0; % prey (2)
    ptr 0.0 0.0 0.0 0.0 pst ptr ptr ptr ptr; % contact
    ptr 0.0 0.0 0.0 0.0 0.0 pst 0.0 0.0 0.0; % predator (1)
    ptr 0.0 0.0 0.0 0.0 0.0 0.0 pst 0.0 0.0; % predator (2)
    ptr 0.0 0.0 0.0 0.0 0.0 0.0 0.0 pst 0.0; % prey (1)
    ptr 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 pst; % prey (2)
    ];
model = validateModel(model);

model.start = zeros(model.nstates, 1);
model.start(1) = 1;
model.start(2) = 1;
model.start(3) = 1;
model.start(4) = 1;
model.start(5) = 1;
model.start(6) = 1;
model.end = zeros(model.nstates, 1);
model.end(1) = 1;
model.end(6) = 1;
model.end(7) = 1;
model.end(8) = 1;
model.end(9) = 1;
model.end(10) = 1;
