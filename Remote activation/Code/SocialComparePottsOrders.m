lo = 2;
hi = 3;
pLo = PottsProb(obj.Analysis.Potts.Model{lo});
pHi = PottsProb(obj.Analysis.Potts.Model{hi});
nstates = 9;
%%
modelHi = obj.Analysis.Potts.Model{hi};
count = obj.nValid;
[p, pci] = binofit(round(pHi * count), count);
d = (pLo - p) ./ (pci(:,2) - p);
%d = -(pLo - p) ./ (p - pci(:,1));
[~, o] = sort(d, 'descend');
figure(1)
clf
PlotProbProb(pHi, pLo, obj.nValid);
hon;
plot(log10(pHi(o(1:10))), log10(pLo(o(1:10))), 'ro', 'MarkerSize', 5);
hoff;
figure(2)
for i=1:nstates
    SquareSubplpot(nstates, i);
    l = [1:4; modelHi.inputPerms(o(i), :)];
    SocialShowState(obj, l);
    title(sprintf('p_2=%g, p_3=%g', pLo(o(i)), pHi(o(i))));
end
%%
cmap = MyMouseColormap;
modelHi = obj.Analysis.Potts.Model{hi};
[~, o] = sort(abs(modelHi.weights .* (modelHi.order == hi)), 'descend');
for i=1:nstates
    SquareSubplpot(nstates, i);
    l = modelHi.labels{o(i)};
    SocialShowState(obj, l);
    title(sprintf('weight=%g', modelHi.weights(o(i))));
end
