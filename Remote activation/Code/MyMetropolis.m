function vec = MyMetropolis(start, func, N)
noise = 0.1;
x = start;
prev_func = func(x);
vec = zeros(1, N);
for i=1:N
    new = x + randn(1) * noise;
    curr_func = func(new);
    a = curr_func / prev_func;
    if a > 1
        x = new;
    else
        if rand(1) < a
            x = new;
        end
    end
    vec(i) = x;
    prev_func = curr_func;
end
