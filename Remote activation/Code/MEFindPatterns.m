function features = MEFindPatterns(data, patterns)
MEPrint('Looking for patterns in data\n');
[ndata, dim] = size(data);
features = zeros(ndata, size(patterns, 1));
map = [];
for i=1:size(patterns, 1)
    idx = find(~isnan(patterns(i, :)));
    if length(idx) ~= size(map, 2)
        map = true(ndata, length(idx));
        reset = true;
    else
        reset = false;
    end
    k = 1;
    for j=idx
        if ~reset && i>1 && patterns(i, j) == patterns(i-1, j)
        else
            map(:, k) = data(:, j) == patterns(i, j);
        end
        k = k + 1;
    end
    features(all(map, 2), i) = true;
end
%features = sparse(features);