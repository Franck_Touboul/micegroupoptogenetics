options.nSubjects = 4;           % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts

%% Loading the Excel file for all mice (if needed)
loadData;

%%
n = 8^options.nSubjects - 1;
me.perms = zeros(n, options.nSubjects);
for i=2:n
    me.perms(i, :) = decimal2octal(i, options.nSubjects);
end
% me.features = cell(1, size(me.perms, 1));
% for i=1:size(me.perms, 1)
%     me.features{i} = @(x) ~any((x - me.perms(i, :)') ~= 0);
% end

%%
fprintf('# applying features to data...\n');
fprintf('# - current: (%5d/%5d)', 1, size(zones.all, 2));

sum_vec = ones(1, options.nSubjects);
for i=1:size(zones.all, 2)
    i
    if rand(1) < 0.1
        fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b(%5d/%5d)', i, size(zones.all, 2));
    end
    for j=1:size(me.perms, 1)
        j
        h(i, j) = sum_vec * (zones.all(:, i) - me.perms(j, :)').^2;
    end
end
h = (h == 0);
fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b(%5d/%5d)\n', size(zones.all, 2), size(zones.all, 2));
%%
p = exp(h * me.weights');

%% compute features
fprintf('# computing features...\n');
base_vec = zones.count .^ [options.nSubjects-1:-1:0];
feat = base_vec * zones.all;
me.features = zeros(size(zones.all, 2), zones.count^options.nSubjects - 1);
me.features(sub2ind(size(me.features), 1:size(zones.all, 2), feat)) = 1;
%% compute constraints
me.const = mean(me.features);
%%
% initialize the model
me.weights = randn(1, size(me.features, 2));
% compute the (un-normalized) pdf for each sample
p = exp(me.features * me.weights');
% normalize the pdf (the Z)
p = p / sum(exp(me.weights));
%%
