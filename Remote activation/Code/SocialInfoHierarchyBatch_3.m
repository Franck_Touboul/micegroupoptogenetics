% SocialInfoHierarchyBatch.m
AllGroups = {...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam04',...
    };

%%
a = AllGroups;
seq = 1:obj.nSubjects;
totalUp = 0;
totalDown = 0;
output = false;
parfor i=1:length(a)
    all = {};
    objs = {};
    %%
    for day = 1:4;
        %%
        prefix = sprintf(a{i}, day);
        fprintf('# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions', 'Common'});
            if day==1
                objs{day} = obj;
            end
            %%
            cEnt = zeros(obj.nSubjects);
            Ent = zeros(1, obj.nSubjects);
            tcEnt = zeros(1, obj.nSubjects);
            for m1=1:obj.nSubjects
                for m2=1:obj.nSubjects
                    z1 = obj.zones(m1, :);
                    if m1 == m2;
                        Ent(m1) = JointEntropy(z1);
                        tcEnt(m1) = ConditionalEntropy(obj.zones([m1 seq(seq ~= m1)], :));
                        continue;
                    end;
                    %                     z1 = obj.zones(m1, ~obj.sheltered(m1, :) & ~obj.sheltered(m2, :));
                    %                     z2 = obj.zones(m2, ~obj.sheltered(m1, :) & ~obj.sheltered(m2, :));
                    
                    z2 = obj.zones(m2, :);
                    cEnt(m1, m2) = ConditionalEntropy([z2; z1]);
                    %cEnt(m1, m2) = ConditionalEntropy(obj.zones([m2 seq(seq ~= m2)], :)) ./ ConditionalEntropy(obj.zones([m2 seq(seq ~= m1 & seq ~= m2)], :));
                end
            end
            %%
            %              mat = cEnt ./ repmat(Ent, 4, 1);
            %              mat = mat - mat';
            mat = cEnt - cEnt';
            mat = mat .* (mat > 0);
            [rank, removed] = TopoFeedbackArcSetOrder(mat);
            %rank = - (tcEnt ./ Ent);
            [~, bg] = SocialGraph(obj, mat, removed, rank);
            if output
                g = biograph.bggui(bg);
                f = get(g.biograph.hgAxes, 'Parent');
                infofile = [first.OutputPath 'info4.' prefix '.SocialInfoHierarchy.png'];
                print(f, infofile, '-dpng');
                close(g.hgFigure);
            end
            %%
            obj = SocialAnalysePredPreyModel(obj);
            curr = obj.Interactions.PredPrey;
            
            matPostPred = curr.PostPred - curr.PostPred';
            matPrePred  = curr.PrePred - curr.PrePred';
            matPostPrey = curr.PostPrey - curr.PostPrey';
            
            mat = matPostPred - matPostPrey;
            mat = mat .* (mat > 0);
            %             mat(binotest(curr.PostPred, curr.PostPred + curr.PostPred') & ...
            %                 binotest(curr.PostPrey, curr.PostPrey + curr.PostPrey')) = 0;
            mat(binotest(curr.PrePred, curr.PrePred + curr.PrePred') & ...
                binotest(curr.PostPred, curr.PostPred + curr.PostPred') & ...
                binotest(curr.PostPrey, curr.PostPrey + curr.PostPrey')) = 0;
            
            %             mat = curr.PostPred - curr.PostPred';
            %             mat(binotest(curr.PostPred, curr.PostPred + curr.PostPred')) = 0;
            
            szc = sum(curr.Contacts);
            sz = sum(curr.PostPred, 2)';
            
            [~, bg] = SocialGraph(obj, mat, [], rank);
            
            if output
                g = biograph.bggui(bg);
                f = get(g.biograph.hgAxes, 'Parent');
                ppfile = [first.OutputPath 'predprey.' prefix '.SocialAnalysePredPreyModel_v2.png'];
                print(f, ppfile, '-dpng');
                close(g.hgFigure);
            end
            %%
            conn = (mat ~= 0) * 1;
            conn(mat == 0) = nan;
            up = sum(max(conn .* repmat(rank, obj.nSubjects, 1), [], 2) > rank');
            totalUp = totalUp + up;
            down = sum(max(conn .* repmat(rank, obj.nSubjects, 1), [], 2) <= rank');
            totalDown = totalDown + down;
            %%
            if output
                
                f1 = imread(infofile);
                f2 = imread(ppfile);
                f = [ImgAutoCrop(f1), ImgAutoCrop(f2)];
                file = [first.OutputPath 'info.' prefix '.SocialAnalysePredPreyModel_v2.png'];
                delete(infofile);
                delete(ppfile);
                imwrite(f, file);
            end
            %%
        catch me
            fprintf(['#   . FAILED! ' me.message '\n']);
            for j=1:length(me.stack)
                fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
            end
        end
    end
end
