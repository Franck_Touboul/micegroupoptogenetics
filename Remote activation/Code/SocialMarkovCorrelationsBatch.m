SocialExperimentData
nSubjects = 4;
%%
index = 1;
MI = zeros(nSubjects, nSubjects, length(experiments) * nDays);
ENTROPY = zeros(nSubjects, length(experiments) * nDays);
ID = zeros(1, length(experiments) * nDays);
DAY = zeros(1, length(experiments) * nDays);
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('# -> %s\n', prefix);
        obj = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid'});
        %%
        [obj, indepProbs, jointProbs, pci, patterns] = SocialComputePatternProbs(obj, false);
        %%
        vpatterns = patterns(jointProbs>0, :);
        h = zeros(obj.nSubjects, obj.ROI.nZones);
        p = ones(1,size(vpatterns, 1));
        for s=1:obj.nSubjects
            ch = histc(obj.zones(s, :), 1:obj.ROI.nZones);
            h(s, :) = ch / sum(ch);
            p = p .* h(s, vpatterns(:, s));
        end
        p = cumsum(p/sum(p));
        r = rand(1, obj.nFrames);
        idx = zeros(1, obj.nFrames);
        for i=1:obj.nFrames
            idx(i) = sum(r(i) > p) + 1;
        end
        obj.zones = patterns(idx, :)';
        %%
        
        [~, mi, entropy] = SocialMutualInformation(obj);
       
        ENTROPY(:, index) = entropy(:);
        MI(:, :, index) = mi;
        DAY(index) = day;
        ID(index) = id;
        index = index + 1;
    end
end
%%
nbins = 25;
m1 = floor(min(ENTROPY(:)));
m2 = ceil(max(ENTROPY(:)));

    index = 1;
cmap = [1 1 1; 0.95 0.95 0.95];
for b=1:2
    if b == 1
        group = E;
    else
        group = SC;
    end
    entropy = [];
    subplot(3,1,1:2);
    x = sequence(m1, m2, nbins);
    offset = (index - 1);
    fill([m1 m1 m2 m2], [0 length(group.idx) * (nDays + 1) length(group.idx) * (nDays + 1) 0] + offset, cmap(b, :), 'EdgeColor', 'none');
    hold on;
    
    for i=group.idx
        for day=1:nDays
            idx = ID == i & DAY == day;
            entropy = [entropy; ENTROPY(:, idx)];
            [sent, sord] = sort(ENTROPY(:, idx));
            plot(sent, ones(1, 4) * index, 'ko-', 'Color', colors(b, :), 'MarkerEdgeColor', 'none'); hold on;
            for s=1:obj.nSubjects
                plot(sent(s), index, 'o', 'MarkerFaceColor', obj.Colors.Centers(sord(s), :), 'MarkerEdgeColor', 'none');
            end
            index = index + 1;
        end
        line([m1 m2], [index index],'Color', 'k', 'LineStyle', ':');
        index = index + 1;
    end
    %line([m1 m2], [index-1 index-1],'Color', 'k', 'LineStyle', ':');
    subplot(3,1,3);
    plot(x, histc(entropy, x), 'Color', group.color)
    hold on;
end
subplot(3,1,1:2);
set(gca, 'YtickLabel', [])
a = axis;
axis([a(1) a(2) 0 index-1]);
hold off;
subplot(3,1,3);

%%
pairwise = [];
pairwiseP = [];
totalPairwiseP = [];
condP = [];
for i=SC.idx
    for day=1:nDays
        mi = MI(:, :, ID == i & DAY == day);
        entropy = ENTROPY(:, ID == i & DAY == day);
     
        condP = [condP; diag(mi) ./ entropy];
        
        mi(1:obj.nSubjects+1:end) = nan;
        pairwise = [pairwise; mi(:)];
        
        miP = mi ./ repmat(entropy, 1, obj.nSubjects);
        pairwiseP = [pairwiseP; miP(:)];

        mi(1:obj.nSubjects+1:end) = 0;
        totalPairwiseP = [totalPairwiseP; sum(mi, 2) ./ entropy];
    end
end
%%
% index = 1;
% for id=1:length(experiments)
%     for day = 1:nDays
%         mi=MI(:, :, index);
%         mi(1:5:end)=nan;
%         %
%         subplot(length(experiments), nDays, index);
%         MyImagesc(mi)
%         index = index  +1;
%     end
% end

%%
nbins = 50;
m1 = min([min(pairwiseP), min(condP));
m2 = max(max(pairwiseP), max(condP));
x = sequence(m1, m2, nbins);
hpw = histc(pairwiseP, x);
htpw = histc(totalPairwiseP, x);
hc = histc(condP, x);
% [hpw, xpw] = hist(pairwiseP,25);
% [hc, xc] = hist(condP,25);
cmap = MySubCategoricalColormap;
plot(x*100, hpw/sum(hpw)*100, 'Color', cmap(2, :), 'LineWidth', 2);
hold on;
plot(x*100, hc/sum(hc)*100, 'Color', cmap(4, :), 'LineWidth', 2);
hold off;
prettyPlot('', 'information percentage [%]', 'percentage that fall in bin [%]');
%legend('Pair: I(x_i; x_j)', 'Multiple: I(x_i; x_{i~=j})');
legend('single', 'multiple');
legend boxoff;

%%
nbins = 50;
m1 = min([min(pairwiseP), min(condP) min(totalPairwiseP)]);
m2 = max([max(pairwiseP), max(condP) max(totalPairwiseP)]);
x = sequence(m1, m2, nbins);
hpw = histc(pairwiseP, x);
htpw = histc(totalPairwiseP, x);
hc = histc(condP, x);
% [hpw, xpw] = hist(pairwiseP,25);
% [hc, xc] = hist(condP,25);
cmap = MySubCategoricalColormap;
plot(x*100, hpw/sum(hpw)*100, 'Color', cmap(2, :), 'LineWidth', 2);
hold on;
plot(x*100, hc/sum(hc)*100, 'Color', cmap(4, :), 'LineWidth', 2);
plot(x*100, htpw/sum(htpw)*100, 'Color', cmap(6, :), 'LineWidth', 2);
hold off;
prettyPlot('', 'information percentage [%]', 'percentage that fall in bin [%]');
%legend('Pair: I(x_i; x_j)', 'Multiple: I(x_i; x_{i~=j})');
legend('single', 'multiple', 'additive');
legend boxoff;