%%
options.MovieFile= 'C:\Documents and Settings\USER\Desktop\Hezi\Trial     1.mpg';
options.nBkgFrames = 20;

options.noiseThresh = 3;

options.nSubjects = 4;
options.colorFrames = 100;
options.colorBins = 20;

options.output = true;
options.solidity = .5;

options.scale = .25;
options.maxNumCents = 10;

%options.minNumPixels = round(150 * options.scale);
options.minNumPixels = 30;
options.noiseSize = round(40 * options.scale);


prevProps = [];
blobs = gmm(2, options.nSubjects, 'diag');
sedisk = strel('disk',5);
nchars = RePrintf('# frame %6d/%6d (%6.2fxRT)', 0, nFrames, 1);
sx = []; sy = [];

tic;
nFrames = 50;
cents.x = zeros(nFrames, options.maxNumCents);
cents.y = zeros(nFrames, options.maxNumCents);
cents.label = zeros(nFrames, options.maxNumCents, 'uint8');
for r=1:nFrames
    %for r=21
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# frame %6d/%6d (%6.2fxiRT)', r, nFrames, RT);
    
    m = imsubtract(read(xyloObj, r), bkgFrame);
    m = imresize(m, options.scale);
    
    if isempty(sx)
        [sx, sy, nc] = size(m);
    end
    m = im2double(m);
    if options.output
        orig = m;
    end
    m = (m > options.noiseThresh * std(m(:))) .* m;
    %%
    hsv_m = rgb2hsv(m);
    hm = hsv_m(:,:,1);
    sm = hsv_m(:,:,2);
    vm = hsv_m(:,:,3);
    %%
    bw = sum(m > 0, 3);
    bw = bwareaopen(bw, options.minNumPixels);
    %labels = bwlabel(bw);
    %nobjects = max(labels(:));
    %%
    [b, idx_h] = histc(hm(bw), subjectColorBins);
    [b, idx_s] = histc(sm(bw), subjectColorBins);
    [b, idx_v] = histc(vm(bw), subjectColorBins);
    prob_h = zeros(options.nSubjects, length(idx_h));
    prob_s = zeros(options.nSubjects, length(idx_s));
    prob_v = zeros(options.nSubjects, length(idx_v));
    for i=1:options.nSubjects
        prob_h(i, :) = subjectColors_h(i, idx_h);
        prob_s(i, :) = subjectColors_s(i, idx_s);
        prob_v(i, :) = subjectColors_v(i, idx_v);
    end
    %     prob_h = prob_h ./ repmat(sum(prob_h, 1), options.nSubjects, 1);
    %     prob_s = prob_s ./ repmat(sum(prob_s, 1), options.nSubjects, 1);
    %     prob_v = prob_v ./ repmat(sum(prob_v, 1), options.nSubjects, 1);
    %     joint_prob = prob_h .* prob_s .* prob_v;
    joint_prob = prob_h .* prob_s .* prob_v;
    joint_prob = joint_prob ./ repmat(sum(joint_prob, 1), options.nSubjects, 1);
    
    [m, idx] = max(joint_prob, [], 1);
    nlabels = zeros(sx,sy,'uint8');
    nlabels(bw) = idx;
    
    flabels = zeros(sx,sy,'uint8');
    %%
    centIndex = 1;
    for i=1:options.nSubjects
        %%
        %curr = bwareaopen(curr, options.minNumPixels);
        reg = bwconncomp(nlabels == i);
        %        reg = bwconncomp(curr);
        %conn = regionprops(reg, 'Solidity', 'MajorAxisLength', 'MinorAxisLength');
        conn = regionprops(reg, 'Solidity', 'Centroid');
        for j=1:length(reg.PixelIdxList)
            %             if length(reg.PixelIdxList{j}) < options.minNumPixels
            %                 reg.PixelIdxList{j} = [];
            %             end
            if conn(j).Solidity < options.solidity || ...
                    length(reg.PixelIdxList{j}) < options.minNumPixels
                reg.PixelIdxList{j} = [];
            else
                cents.x(r, centIndex) = conn(j).Centroid(1);
                cents.y(r, centIndex) = conn(j).Centroid(2);
                cents.label(r, centIndex) = i;
                centIndex = centIndex + 1;
            end
            %             if conn(j).Solidity < .5 || ...
            %                     length(reg.PixelIdxList{j}) < options.minNumPixels || ...
            %                     length(reg.PixelIdxList{j}) / (conn(j).MajorAxisLength * conn(j).MinorAxisLength * pi/4) < 0.5
            %                 reg.PixelIdxList{j} = [];
            %             end
        end
        img = labelmatrix(reg);
        if 1 == 2
            subplot(2,2,i);
            imagesc(img > 0);
        end
        flabels(img > 0) = i;
    end
    %%
    if options.output
        subplot(2,2,4);
        rgblbls = label2rgb(flabels);
        imagesc(rgblbls);
        subplot(2,2,3);
        imagesc(orig);
        drawnow
    end
    %%
end
fprintf('\n');

%%
curr = 1;
confussionProb = 0.1;
succProb = 1;
distVar = 5^2;
distVarHidden = 10^2;

probs = (cents.label == curr) * succProb + (cents.label ~= curr) * confussionProb;
table = flog(probs)';
path = zeros(size(table), 'uint8');

[ndata, dim] = size(cents.label);
ncents = sum(cents.label > 1, 2);

prev.x = cents.x(1, :);
prev.y = cents.y(1, :);
p = zeros(dim, 1);
for i=2:nFrames
    for s=1:dim
        %        dist = sqrt((cents.x(i, s) - cents.x(i-1, :)).^2 + (cents.y(i, s) - cents.y(i-1, :)).^2);
        dist = sqrt((cents.x(i, s) - prev.x).^2 + (cents.y(i, s) - prev.y).^2);
        p(1:ncents(i))     = gauss(0, distVar, dist(1:ncents(i))');
        p(ncents(i)+1:end) = gauss(0, distVarHidden, dist(ncents(i)+1:end)');
        [val, from] = max(table(:, i-1) + flog(p));
        table(s, i) = table(s, i) + val;
        path(s, i) = from;
        if cents.label(i-1, from) == 0
            loc.x(s) = prev.x(from);
            loc.y(s) = prev.y(from);
        else
            loc.x(s) = cents.x(i-1, from);
            loc.y(s) = cents.y(i-1, from);
        end
    end
    prev = loc;
end

%%
confProb = 0.1;
hiddenProb = 0.1;

N = 100;
nS = 4;
cents.x = zeros(N, nS);
cents.y = zeros(N, nS);
cents.label = zeros(N, nS);
p = zeros(N, nS);
for i=1:N
    p(i, :) = randperm(nS);
    for j=1:nS
        r = rand(1);
        if r < confProb
            temp = randperm(nS);
            c = 1;
            if temp(c) == j
                c = 2;
            end
            cents.label(i, p(i, j)) = temp(c);
        else
            cents.label(i, p(i, j)) = j;
        end
    end
end

for j=1:nS
    x = cumsum(randn(1, N));
    y = cumsum(randn(1, N));
    indx = sub2ind([N, nS], 1:N, p(:, j)');
    cents.x(indx) = x;
    cents.y(indx) = y;
    orig.x(:, j) = x;
    orig.y(:, j) = y;
end
cents.label(rand(size(cents.label)) < hiddenProb) = 0;
cents.x(cents.label == 0) = inf;
cents.y(cents.label == 0) = inf;
%%
options.jumpVar = eye(2) * 5^2;
options.confProb = 0.1;
options.hideProb = 0.01;

curr = 2;
table = zeros(N, nS+1);
table(cents.label == curr) = 1;
table(cents.label ~= curr) = options.confProb;
table(cents.label == 0)    = options.hideProb;
table = table';

path = zeros(nS, N, 'uint8');
% table(:, 1) = table(:, 1) .* model.start(:);
for i=2:N
    for j=1:nS
        p = gauss([cents.x(i, j), cents.y(i, j)], options.jumpVar, [cents.x(i-1, 1:nS); cents.y(i-1, 1:nS)]');
        [val, from] = max(flog(p) + table(1:nS, i-1));
        path(j, i) = from;
    end
    [val, from] = max(table(:, i-1));
    path(nS + 1, i) = from;
end
% backtrack
backtrack = zeros(1, N);
backtrack(end) = 4;
res.x = zeros(1, N);
res.y = zeros(1, N);

res.x(end) = cents.x(end, backtrack(end));
res.y(end) = cents.y(end, backtrack(end));
for i=N-1:-1:1
    backtrack(i) = path(backtrack(i+1), i + 1);
    res.x(i) = cents.x(i, backtrack(i));
    res.y(i) = cents.y(i, backtrack(i));
end
%
i=2; plot(orig.y(:, i), orig.x(:, i), '-', res.y, res.x, '.:')