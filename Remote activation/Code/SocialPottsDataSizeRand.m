function SocialPottsDataSizeRand(obj)
%%
obj = TrackLoad(obj);
seed = sum(100*clock);
rand('twister',seed);
randn('seed',seed);
%%
Options.ShrinkFactor = .9;
Options.nShrinks = 40;
Options.nIters = 10;
%%
obj.OutputToFile = false;
obj.OutputInOldFormat = false;
obj.Output = true;
%%
r = randperm(obj.nFrames); 
ind1 = r(1:ceil(obj.nFrames/2));
map1 = false(1, obj.nFrames);
map1(ind1) = true;
map2 = ~map1;
ind2 = find(map2);
%%
obj1 = obj;
obj1.nFrames = sum(map1);
obj1.valid = obj.valid(map1);
obj1.zones = obj.zones(:, map1);
obj1 = SocialPotts(obj1);
[obj1, probs] = SocialComputePatternJointProbs(obj1);
obj1.probs = probs;
%%
s_ = 0:Options.nShrinks;
s_ = s_(randi(length(s_)));
%%
data = [];
idx = 1;
for i=1:obj.nSubjects
    data(idx).order = i;
    data(idx).probs = obj1.Analysis.Potts.Model{i}.prob;
    data(idx).DjsByOrder = 0;
    data(idx).DjsByObserved = JensenShannonDivergence(obj1.probs, obj1.Analysis.Potts.Model{i}.prob');
    data(idx).nSamples = -1;
    data(idx).seed = seed;
    idx = idx + 1;
end
data(idx).order = 0;
data(idx).probs = obj1.probs';
data(idx).DjsByOrder = 0;
data(idx).DjsByObserved = 0;
data(idx).nSamples = -1;
data(idx).seed = seed;
idx = idx + 1;
%%
for s = s_
    niters = floor(1/(.9^s));
    fprintf('#--------------------------------------------------------\n');
    fprintf('# shrink no. %02d\n', s);
    for iter=1:niters
        %%
        try
        r = randperm(length(ind2)); 
        currind = ind2(r(1:floor(obj.nFrames/2 * Options.ShrinkFactor^s)));
        currmap = false(1, obj.nFrames);
        currmap(currind) = true;
        fprintf('#  iter no. %0d/%2d: %d/%d samples\n', iter, niters, sum(currmap), sum(map2));
        %

        obj2 = obj;
        obj2.nFrames = sum(currmap);
        obj2.valid = obj.valid(currmap);
        obj2.zones = obj.zones(:, currmap);
        obj2 = SocialPotts(obj2);
        
        %
        for i=1:obj.nSubjects
            data(idx).order = i;
            data(idx).probs = obj2.Analysis.Potts.Model{i}.prob;  
            data(idx).DjsByOrder = JensenShannonDivergence(obj1.Analysis.Potts.Model{i}.prob', obj2.Analysis.Potts.Model{i}.prob');
            data(idx).DjsByObserved = JensenShannonDivergence(obj1.probs, obj2.Analysis.Potts.Model{i}.prob');
            data(idx).nSamples = sum(currmap);  
            data(idx).seed = seed;
            idx = idx + 1;
        end
        catch me
            warning(me.message);
        end
    end
end
unique_id = sprintf('%09d', randi(999999999));
save(['mat/SocialPottsDataSize/' obj.FilePrefix '.' unique_id], 'data');

