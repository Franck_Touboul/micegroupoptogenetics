field = 'AggressiveChase';
SocialExperimentData;
clf;

colors = [];
for i=1:length(Groups)
    colors(i, :) = Groups(i).color;
end
rankmap = colormap(gray(GroupsData.nSubjects));
%%
clf
%shapes = {'o', 's', 'd', 'p', 'h', '^', 'v', '>', '<', 'o', 's', 'd', 'p', 'h', '^', 'v', '>', '<', 'o', 's', 'd', 'p', 'h', '^', 'v', '>', '<', 'o', 's', 'd', 'p', 'h', '^', 'v', '>', '<', 'o'};
shapes = {'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'};
chaseEscapeProb = {};
nLevels = {};
hStrength = {};
Contacts = {};
ChaseEscape = {};
for day = 1:nDays
    chaseEscapeProb{day} = [];
    nLevels{day} = [];
    hStrength{day} = [];
    Contacts{day} = [];
    ChaseEscape{day} = [];
    Duration{day} = [];
end
pGroupId = inf;
CumEvents = [];
for id=GroupsData.index
    groupId = GroupsData.group(id);
    localId = find(GroupsData.index(GroupsData.group == groupId) == id, 1);
    if pGroupId ~= groupId
        %CumEvents = [];
        CumEvents = [CumEvents; nan(1, size(CumEvents, 2))]
        pGroupId = groupId;
    end

    group = Groups(groupId);
    
    chaseEscape = zeros(obj.nSubjects);
    contacts = zeros(obj.nSubjects);
    rank = zeros(1, obj.nSubjects);
    
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        try
            obj = TrackLoad([obj.OutputPath prefix '.obj.mat'], {'Interactions', 'Hierarchy'});
        catch me
            MyWarning(me)
            corrupt = {corrupt{:}, prefix};
            continue;
        end
        chaseEscape = chaseEscape + obj.Hierarchy.(field).ChaseEscape;
        rank = rank + obj.Hierarchy.(field).rank;
%        chaseEscapeProb{day} = [chaseEscapeProb{day}, sum(obj.Interactions.PredPrey.PostPredPrey(:)) ./ (sum(obj.Interactions.PredPrey.Contacts(:)) / 2)];
        nLevels{day} = [nLevels{day}, obj.Hierarchy.(field).nLevels];
        hStrength{day} = [hStrength{day}, obj.Hierarchy.(field).strength];
        Duration{day} = [Duration{day}, obj.RecordingDuration * [0 0 0 1 1/60 1/3600]'];
        nContacts = 0;
        currContacts = obj.Hierarchy.Group.Days(day).Contacts;
        nContacts = sum(obj.Hierarchy.Group.Days(day).Contacts(:));
        contacts = contacts + currContacts;
        chaseEscapeProb{day} = [chaseEscapeProb{day}, mean(sum( obj.Hierarchy.(field).ChaseEscape, 2) ./ sum(currContacts, 2))];
        Contacts{day} = [Contacts{day}, nContacts / 2];
        ChaseEscape{day} = [ChaseEscape{day}, sum( obj.Hierarchy.(field).ChaseEscape(:))];
    end
    %%
    figure(1);
    subplot(2,3,5:6);
    pChase = sum(chaseEscape, 2) ./ sum(contacts, 2) * 100;
    pEscape = sum(chaseEscape, 1) ./ sum(contacts, 1) * 100;
    [q, order] = sort(pChase' / 1000 + obj.Hierarchy.Group.(field).rank);
    %[rank, removed] = TopoFeedbackArcSetHierarchicalOrder(obj.Hierarchy.Group.map);
    %order = sort(rank);
    %c = rgb2hsv(reshape(group.color, [1 1 3])); c(2) = .5 + .5 * localId / sum(group.map); c = hsv2rgb(c); c = c(:)';
    c = group.color;
    color = c;
    plot(pChase(order), pEscape(order), [shapes{localId} '-'], 'Color',  color, 'MarkerFaceColor', color, 'MarkerEdgeColor', 'none', 'MarkerSize', 1, 'LineWidth', 2);
    hon;
    for i=1:obj.nSubjects
       plot(pChase(order(i)), pEscape(order(i)), 'o:', 'MarkerFaceColor', rankmap(max(obj.Hierarchy.Group.(field).rank) + 1 - obj.Hierarchy.Group.(field).rank(order(i)), :), 'MarkerEdgeColor', color, 'MarkerSize', 8, 'LineWidth', .5)
        %text(pChase(order(i)), pEscape(order(i)), num2str(max(obj.Hierarchy.Group.rank) + 1 - obj.Hierarchy.Group.rank(order(i))), 'VerticalAlignment', 'middle' ,'HorizontalAlignment', 'center', 'Color', 'w', 'FontSize', 10, 'FontWeight', 'bold');
    end
    drawnow;
    hold on;
%     figure(2);
%     cumEvents = [sum(chaseEscape, 2) sum(chaseEscape, 1)' ];
%     cumEvents = [cumEvents sum(contacts, 2) - sum(cumEvents, 2)];
%     cumEvents = [cumEvents; inf(1,size(cumEvents, 2))];
%     CumEvents = [CumEvents; cumEvents];
%     [Tilde, cobj] = MyCategoricalColormap;
%     PlotPersonas(CumEvents, true, [cobj.Red; cobj.Blue; cobj.Green]);
%     title(GroupsData.Titles{groupId});
%     hold on;
end
hold off;
xlabel('contacts that end in chase [%]');
ylabel('contacts that end in escape [%]');

return;
%%


%% measure chase escape levels over days
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(chaseEscapeProb{day}(Groups(g).idx)*100);
        s(day) = stderr(chaseEscapeProb{day}(Groups(g).idx)*100);
        all(day, :) = chaseEscapeProb{day}(Groups(g).idx)*100;
    end
    subplot(1,3,1:2);
    errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    %plot(1:nDays, all)
    hold on;
    %plot(1:nDays, all,'Color', Groups(g).color)
    
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
legend(GroupsData.Titles);
legend boxoff
hold off
set(gca, 'XTick', [1:4]);
xlabel('times [days]');
ylabel('% contacts that end in a chase');
subplot(1,3,3); 
MyBar(1:length(Groups), tm, ts, colors);
set(gca, 'XTickLabel', GroupsData.Initials);
ylabel('% contacts that end in a chase');

%% measure chase escape numbers over days
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(chaseEscapeProb{day}(Groups(g).idx)*100);
        s(day) = stderr(chaseEscapeProb{day}(Groups(g).idx)*100);
        all(day, :) = chaseEscapeProb{day}(Groups(g).idx)*100;
    end
    subplot(1,3,1:2);
    errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    %plot(1:nDays, all)
    hold on;
    %plot(1:nDays, all,'Color', Groups(g).color)
    
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
legend(GroupsData.Titles);
hold off
set(gca, 'XTick', [1:4]);
xlabel('times [days]');
ylabel('% contacts that end in a chase');
subplot(1,3,3); 
MyBar(1:length(Groups), tm, ts, colors);
set(gca, 'XTickLabel', GroupsData.Titles);
ylabel('% contacts that end in a chase');


%% measure number of contacts over days
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(Contacts{day}(Groups(g).idx)/2 ./ Duration{day}(Groups(g).idx));
        s(day) = stderr(Contacts{day}(Groups(g).idx)/2./ Duration{day}(Groups(g).idx));
        all(day, :) = Contacts{day}(Groups(g).idx)/2 ./ Duration{day}(Groups(g).idx);
    end
subplot(1,3,1:2)    
    errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    %plot(1:nDays, all)
    hold on;
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
hold off;
legend(GroupsData.Titles);
legend boxoff
set(gca, 'XTick', [1:4]);
xlabel('times [days]');
ylabel('number of contacts per hour');
subplot(1,3,3); 
MyBar(1:length(Groups), tm, ts, colors);
set(gca, 'XTickLabel', GroupsData.Initials);
ylabel('number of contacts per hout');
%% measure number of hierarchy levels over days
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(nLevels{day}(Groups(g).idx));
        s(day) = stderr(nLevels{day}(Groups(g).idx));
        all(day, :) = nLevels{day}(Groups(g).idx);
    end
    subplot(1,3,1:2)
    errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    %plot(1:nDays, all)
    hold on;
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
hold off;
legend(GroupsData.Titles);
subplot(1,3,3); 
MyBar(1:length(Groups), tm, ts, colors);
set(gca, 'XTickLabel', GroupsData.Initials);
%% measure number of hierarchy strength over days
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(hStrength{day}(Groups(g).idx));
        s(day) = stderr(hStrength{day}(Groups(g).idx));
        all(day, :) = hStrength{day}(Groups(g).idx);
    end
    subplot(1,3,1:2)
    errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    %plot(1:nDays, all)
    hold on;
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
hold off;
legend(GroupsData.Titles);
subplot(1,3,3); 
barweb(tm, ts, [], [], [], [], [], colors);
%% measure number of chaseEscape  over days
tm = [];
ts = [];
for g=1:length(Groups)
    m = [];
    s = [];
    all = [];
    for day=1:nDays
        m(day) = mean(ChaseEscape{day}(Groups(g).idx) ./ Duration{day}(Groups(g).idx));
        s(day) = stderr(ChaseEscape{day}(Groups(g).idx) ./ Duration{day}(Groups(g).idx));
        all(day, :) = ChaseEscape{day}(Groups(g).idx) ./ Duration{day}(Groups(g).idx);
    end
    subplot(1,3,1:2); errorbar(1:nDays, m, s, 'LineWidth', 2, 'Color', Groups(g).color); 
    hold on;
    tm(g) = mean(mean(all));
    ts(g) = stderr(mean(all));
end
hold off;
legend(GroupsData.Titles);
legend boxoff;
subplot(1,3,3); 
MyBar(1:length(Groups), tm, ts, colors);
set(gca, 'XTickLabel', GroupsData.Initials);
