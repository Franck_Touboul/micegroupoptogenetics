function [obj, mi, entropy] = SocialExactMutualInformation(obj)
% Entropy and Information in Neural Spike Trains
% Strong, S. P. and Koberle, Roland  and de Ruyter van Steveninck, Rob R.
% and Bialek, William
% Phys. Rev. Lett. 80(1) 1998
% URL:	http://link.aps.org/doi/10.1103/PhysRevLett.80.197
% DOI:	10.1103/PhysRevLett.80.197
% PACS:	 87.10.+e, 05.20.-y, 89.70.+c

nranks = 20;
niters = 128;

MI = {};
ndata = size(obj.zones, 2);

for rank = 1:nranks;
    cniters = min(4^(rank-1), niters);
    for iter=1:cniters
        [rank, iter]
        if rank > 1
            map = randperm(ndata); map = map(1:floor(ndata/rank));
            zones = obj.zones(:, map) - 1;
        else
            %            map = 1:ndata;
            zones = obj.zones - 1;
        end
        pAll = ComputeJointProbsForDimension(zones, obj.ROI.nZones, 1:obj.nSubjects);
        %pAll = computeJointSampleProb(obj.zones-1, obj.ROI.nZones, jpAll);
        
        
        mi = zeros(obj.nSubjects);
        entropy = zeros(1, obj.nSubjects);
        pONE = cell(1, obj.nSubjects);
        for m1=1:obj.nSubjects
            pONE{m1} = ComputeJointProbsForDimension(zones, obj.ROI.nZones, m1);
            entropy(m1) = pAll * flog2(pONE{m1})';
        end
        
        for m1=1:obj.nSubjects
            for m2=1:obj.nSubjects
                if m1 == m2
                    idx = 1:obj.nSubjects;
                    idx = idx(idx ~= m1);
                    
                    pLOO = ComputeJointProbsForDimension(zones, obj.ROI.nZones, idx);
                    mi(m1,m1) = pAll * (flog2(pAll) - flog2(pLOO) - flog2(pONE{m1}))';
                    
                else
                    if m1<m2
                        curr = obj;
                        curr.nSubjects = 2;
                        pTWO = ComputeJointProbsForDimension(zones, obj.ROI.nZones, [m1, m2]);
                        cmi = pAll * (flog2(pTWO) - flog2(pONE{m1}) - flog2(pONE{m2}))';
                        mi(m1, m2) = cmi;
                        mi(m2, m1) = cmi;
                    end
                end
            end
        end
        MI{rank, iter} = mi;
    end % for iter
end % for rank

%%