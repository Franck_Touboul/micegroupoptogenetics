function obj = SocialAnalysePredPreyModel(obj)
fprintf('# analyzing Pred-Prey model\n');

obj = TrackLoad(obj);
obj.Interactions.PredPrey.MinNumberOfEvents = 2;

PrePred  = zeros(obj.nSubjects);
PrePrey  = zeros(obj.nSubjects);
PostPred = zeros(obj.nSubjects);
PostPrey = zeros(obj.nSubjects);
PredPrey = zeros(obj.nSubjects);

PostPredPrey = zeros(obj.nSubjects);
PPPredPrey = zeros(obj.nSubjects);

contacts = zeros(obj.nSubjects);
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1 == m2; continue; end;
        [Tilde, Tilde, Tilde, events1] = FindEvents(obj.Interactions.PredPrey.states{m1, m2}, obj.Interactions.PredPrey.states{m1, m2} > 1);
        [Tilde, Tilde, Tilde, events2] = FindEvents(obj.Interactions.PredPrey.states{m2, m1}, obj.Interactions.PredPrey.states{m1, m2} > 1);
        for i=1:length(events1)
            PredPrey(m1, m2) = PredPrey(m1, m2) + (any(events1{i}.data == 5) && any(events2{i}.data == 6));
        end
        beg = FindEvents(obj.Interactions.PredPrey.states{m1, m2} == 4);
        contacts(m1, m2) = length(beg);

        beg = FindEvents(obj.Interactions.PredPrey.states{m1, m2} == 2);
        PrePred(m1, m2) = length(beg);
        beg = FindEvents(obj.Interactions.PredPrey.states{m1, m2} == 3);
        PrePrey(m1, m2) = length(beg);
        beg = FindEvents(obj.Interactions.PredPrey.states{m1, m2} == 5);
        PostPred(m1, m2) = length(beg);
        beg = FindEvents(obj.Interactions.PredPrey.states{m1, m2} == 6);
        PostPrey(m1, m2) = length(beg);

        beg = FindEvents(obj.Interactions.PredPrey.states{m1, m2} == 5 & obj.Interactions.PredPrey.states{m2, m1} == 6);
        PostPredPrey(m1, m2) = length(beg);    
    end
end
obj.Interactions.PredPrey.PrePred  = PrePred;
obj.Interactions.PredPrey.PrePrey  = PrePrey;
obj.Interactions.PredPrey.PostPred = PostPred;
obj.Interactions.PredPrey.PostPrey = PostPrey;
obj.Interactions.PredPrey.PostPredPrey = PostPredPrey;
obj.Interactions.PredPrey.Contacts = contacts;
