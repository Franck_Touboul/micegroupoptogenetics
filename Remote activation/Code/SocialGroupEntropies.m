SocialExperimentData;
stat = [];
for i=1:GroupsData.nExperiments
    for day=1:GroupsData.nDays
        %%
        obj = TrackLoad({i day});
        [obj, independentProbs, jointProbs] = SocialComputePatternProbs(obj, false);
        stat.Entropy(i, day) = -jointProbs * log2(jointProbs + (jointProbs == 0))';
        stat.IndividualEntropy(i, day) = -independentProbs * log2(independentProbs + (independentProbs == 0))';
    end
end
%%