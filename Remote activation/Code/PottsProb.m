function p = PottsProb(model)
p = exp(model.perms * model.weights');
perms_p = exp(model.perms * model.weights');
Z = sum(perms_p);
p = p / Z;

