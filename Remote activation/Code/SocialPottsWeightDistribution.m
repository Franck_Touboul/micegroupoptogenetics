order = 3;

beta = 2^-5;

SocialExperimentData
W = {};
for o=1:order
    W{o} = [];
end
for i=GroupsData.Standard.idx
    obj = TrackLoad({i 2}, {'Analysis'});
    %me = obj.Analysis.Potts.Regularized.Model;
    me = SocialGetRegPotts(obj, beta, order);
    %me = obj.Analysis.Potts.Windowed.Model{order};
    for o=1:order
        w = me.weights(me.order == o);
        W{o} = [W{o}, w];
    end
end
%%
%clf
subplot(2,1,1);
hon
cmap = MyCategoricalColormap;
entries = {};
NBINS = [18 50 50];
for i=1:order
    w = W{i};
    w = w(w ~= 0);
    nbins = NBINS(i);
    [h, x] = hist(w, nbins);
    plot(x, h/sum(h)/(x(2)-x(1)), ':', 'color', cmap(i, :)); hon
    entries{i} = num2str(i);
end
xaxis(-8, 8);
prettyPlot
legend(entries);
hoff

%%
me2 = obj.Analysis.Potts.Model{2};
me3 = obj.Analysis.Potts.Model{3};
w2 = me2.weights(me2.order == 2);
w3 = me3.weights(me3.order == 2);
plot(w2, w3, '.')
axis equal