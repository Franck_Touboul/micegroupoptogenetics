
weightIndexFrom = [];
weightIndexTo = [];
for i=1:length(obj.Analysis.DPotts.Model.labels)
    if size(obj.Analysis.DPotts.Model.labels{i}, 2) == 2
        weightIndexFrom(obj.Analysis.DPotts.Model.labels{i}(1,1), i) = true;
        weightIndexTo(obj.Analysis.DPotts.Model.labels{i}(1,2), i) = true;
    end
end

%%
d = zeros(obj.nSubjects);
for s1=1:obj.nSubjects
    for s2=1:obj.nSubjects
        if s1 == s2
            continue;
        end
        d(s1, s2) = mean(obj.Analysis.DPotts.Model.weights(weightIndexFrom(s1, :) & weightIndexTo(s2, :)));
    end
end
d = exp(d);
%%
mat = d - d';
mat = mat .* (mat > 0);

[rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
[~, bg] = SocialGraph(obj, mat, removed, rank);
g = biograph.bggui(bg);
