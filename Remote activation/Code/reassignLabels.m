function res = reassignLabels(res, cents, minDistance)
% ensures that no blob was assigned to two different subjects, uses
% the sequence probability to determine the correct subject.

for i=1:length(res)
    for j=i+1:length(res)
        match = res{i}.id == res{j}.id & res{i}.id ~= 0;
        if nargin > 1
            idi = double(res{i}.id);
            idj = double(res{j}.id);
            valid = idi > 0 & idj > 0;
            range = find(valid);
            temp = sqrt((...
                cents.x(sub2ind(size(cents.x), range, idi(valid))) - ...
                cents.x(sub2ind(size(cents.x), range, idj(valid)))) .^ 2 + (...
                cents.y(sub2ind(size(cents.y), range, idi(valid))) - ...
                cents.y(sub2ind(size(cents.y), range, idj(valid)))) .^ 2);
            distance = inf(1, size(cents.x, 1));
            distance(valid) = temp;
            dmatch = distance < minDistance;
            
            probsi = cents.logprob(:, :, i);
            probsj = cents.logprob(:, :, j);
            indices = 1:length(dmatch);
            
            res{i}.prob(dmatch) = res{i}.prob(dmatch) + probsi(sub2ind(size(probsi), indices(dmatch), idj(dmatch)));
            res{j}.prob(dmatch) = res{j}.prob(dmatch) + probsj(sub2ind(size(probsj), indices(dmatch), idi(dmatch)));
            match = match | dmatch;
        end
        res = assignMaxProb(res, i, j, match);
    end
end

function res = assignMaxProb(res, i, j, match)
d = diff([0 match 0], 1, 2);
start  = find(d > 0);
finish = find(d < 0) - 1;
for k=1:length(start)
    p1 = res{i}.prob(finish(k));
    p2 = res{j}.prob(finish(k));
    if start(k) > 1
        p1 = p1 - res{i}.prob(start(k) - 1);
        p2 = p2 - res{j}.prob(start(k) - 1);
    end
    if p1 > p2
        res{j}.id(start(k):finish(k)) = 0;
        res{j}.x(start(k):finish(k)) = nan;
        res{j}.y(start(k):finish(k)) = nan;
    else
        res{i}.id(start(k):finish(k)) = 0;
        res{i}.x(start(k):finish(k)) = nan;
        res{i}.y(start(k):finish(k)) = nan;
    end
end
