% The function takes vector and determines whether the mouse is "immobile"

function [result]=immobileMouse(XVec,YVec);
%turning the position vector into a matrix

XMatrix=repmat(XVec,length(XVec),1);

XMatrixTrans=XMatrix';

YMatrix=repmat(YVec,length(YVec),1);

YMatrixTrans=YMatrix';

Xdif=XMatrix-XMatrixTrans;
Ydif=YMatrix-YMatrixTrans;

XYdist=sqrt(Xdif.^2 + Ydif.^2);
if find(XYdist>6)>0
    result=false;
else result=true;
end

    