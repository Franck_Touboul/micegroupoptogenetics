function SocialPottsVectorPlot(obj, vecs, color, index)
if nargin < 4
    index = 1;
end

count = 5;
height = 5;

plot(vecs, 'Color', color);
m = mean(vecs');
r1 = min(m);
r2 = max(m);

[qqq, o] = sort(m);
p = o(end-count+1:end);
n = o(1:count);

a = axis;

%% value
if 1==2
for i=p
    z1 = floor((i-1) / obj.ROI.nZones) + 1;
    z2 = mod(i-1, obj.ROI.nZones) + 1;
    text(i, a(4) - height * (index - 1), [' ' num2str(z1)  ','  num2str(z2)],'VerticalAlignment', 'top', 'Color', color);
    vert_line(i, 'LineStyle', '--', 'Color', color);
end

for i=n
    z1 = floor((i-1) / obj.ROI.nZones) + 1;
    z2 = mod(i-1, obj.ROI.nZones) + 1;
    %text(i, m(i), [obj.ROI.ZoneNames{z1}  ', '  obj.ROI.ZoneNames{z2}]);
    vert_line(i, 'LineStyle', ':', 'Color', color);
    text(i, a(3) + height * (index - 1), [' ' num2str(z1)  ','  num2str(z2)],'VerticalAlignment', 'bottom', 'Color', color);
end
end
%% arrow
for i=p
    z1 = floor((i-1) / obj.ROI.nZones) + 1;
    z2 = mod(i-1, obj.ROI.nZones) + 1;
    text(i, a(4) - height * (index - 1) - 1, '◄','VerticalAlignment', 'top', 'Color', color);
    vert_line(i, 'LineStyle', '--', 'Color', color);
end

for i=n
    z1 = floor((i-1) / obj.ROI.nZones) + 1;
    z2 = mod(i-1, obj.ROI.nZones) + 1;
    %text(i, m(i), [obj.ROI.ZoneNames{z1}  ', '  obj.ROI.ZoneNames{z2}]);
    vert_line(i, 'LineStyle', ':', 'Color', color);
    text(i, a(3) + height * (index - 1) + 1, '◄','VerticalAlignment', 'bottom', 'Color', color);
end