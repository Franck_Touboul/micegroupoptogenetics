function data = QSocialBehaviour
%  Social behavior code - agrresive/non-aggresive
fprintf('# classifying behaviour\n');

%% Loading the data
fprintf('# - loading social data\n');
QSocialDefault;

%%
options = social.options;
options.minContactDistance = 15 * options.pixelsInCentimeter; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts

options.minNanDuration = 3;
options.minHiddenDuration = 37;

options.minSpeedForAngle = 0.2; % if chases speed is less then this, angle is set to nan

% 1) Preparing the data matrix for work
% 2) Calculating contacts
% 3) Finding "islands" of "1" or "11" (false contacts)
% 4) Unifying contacts into "engagements" by canceling small gaps
% 5) Creating a cell array of engagements and their respective raw data
% 6) Characterizing the engagements (aggresive or non-aggresive)
% 7) Plots

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1)                 Preparing the data matrix for work
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
% Checking if the mouse was in the labyrinth, in the small nest or in the big
% nest and adding NaN in the corresponding coordinates. The consequence of
% adding NaNs is canceling contacts in the labyrinth, small and big nest
% so we just deal with contacts in the open space.
orig = social;
data = social;

outOfSight = ...
    social.zones.all == social.roi.fieldsIndex.Labyrinth | ...
    social.zones.all == social.roi.fieldsIndex.SmallNest | ...
    social.zones.all == social.roi.fieldsIndex.BigNest;

marker = removeGaps(outOfSight', options.minNanDuration)';

data.hidden = ...
    social.zones.all == social.roi.fieldsIndex.SmallNest | ...
    social.zones.all == social.roi.fieldsIndex.BigNest;

data.hidden = removeGaps(data.hidden', options.minHiddenDuration)';
data.x(marker) = nan;
data.y(marker) = nan;

% data.x(data.hidden) = nan;
% data.y(data.hidden) = nan;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2)                            Calculating contacts

fprintf('# - finding contacts\n');
%Pitagoras to calculate distances between 2 mice in each time bin
orig.miceDistance = ones(options.nSubjects,options.nSubjects,orig.nData) * inf;
data.miceDistance = ones(options.nSubjects,options.nSubjects,orig.nData) * inf;

for i=1:options.nSubjects-1
    for j=i+1:options.nSubjects
        orig.miceDistance(i, j, :) = sqrt((orig.x(i, :) - orig.x(j, :)).^2 + (orig.y(i, :) - orig.y(j, :)).^2);
        data.miceDistance(i, j, :) = sqrt((data.x(i, :) - data.x(j, :)).^2 + (data.y(i, :) - data.y(j, :)).^2);
        %data.miceDistance(j, i, :) = data.miceDistance(i, j, :);
    end
end

%Calculating contacts (contact is defined as distance<15 cm)
data.contacts = data.miceDistance < options.minContactDistance;
orig.contacts = orig.miceDistance < options.minContactDistance;

diff = orig.contacts - data.contacts;
hiddenContacts = data.contacts(:,:,1:end-1) + diff(:,:,2:end);
% if contact started\ended outside of the hidding places, continue the interaction
fprintf('#      . adding continuity...\n');
for i=1:options.nSubjects-1
    for j=i+1:options.nSubjects
        range = findRange(hiddenContacts(i, j, :));
        if range.start(1)==1
            continous = data.contacts(i, j, range.start(2:end)-1);
            change.start = range.start([false, continous(:)']);
            change.end = range.end([false, continous(:)']);
            for k=1:length(change.start)
                data.contacts(i,j,change.start(k):change.end(k)) = 1;
            end
        else
            continous = data.contacts(i, j, range.start-1);
            change.start = range.start(continous);
            change.end = range.end(continous);
            for k=1:length(change.start)
                data.contacts(i,j,change.start(k):change.end(k)) = 1;
            end
        end

        if range.end(end)==data.nData
            continous = data.contacts(i, j, range.end(1:end-1)+1);
            change.start = range.start([continous(:)', false]);
            change.end = range.end([continous(:)', false]);
            for k=1:length(change.start)
                data.contacts(i,j,change.start(k):change.end(k)) = 1;
            end
        else
            continous = data.contacts(i, j, range.end+1);
            change.start = range.start(continous);
            change.end = range.end(continous);
            for k=1:length(change.start)
                data.contacts(i,j,change.start(k):change.end(k)) = 1;
            end
        end
        
    end
end

%data.contacts(outOfSight) = 0;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%3)             Finding "islands" of "1" or "11" (false contacts)
fprintf('# - removing short contacts (less then %3d timebins)\n', options.minContanctDuration);
for i=1:options.nSubjects-1
    for j=i+1:options.nSubjects
        data.contacts(i,j,:) = removeGaps(shiftdim(data.contacts(i,j,:)), options.minContanctDuration);
    end
end

% inRows = reshape(data.contacts, options.nSubjects * options.nSubjects, size(data.contacts, 3));
% c = convn(double(convn(inRows, ones(1, options.minContanctDuration)) >= 3), ones(1, options.minContanctDuration));
% c = c(:, options.minContanctDuration:end-options.minContanctDuration + 1) > 0;
% data.contacts = reshape(c, options.nSubjects, options.nSubjects, size(data.contacts, 3));

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4)           Unifying contacts into "engagements" by canceling small gaps
fprintf('# - removing short gaps     (less then %3d timebins)\n', options.minGapDuration);

for i=1:options.nSubjects-1
    for j=i+1:options.nSubjects
        data.contacts(i,j,:) = 1 - removeGaps(1 - shiftdim(data.contacts(i,j,:)), options.minGapDuration);
    end
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5)         Find Chases

fprintf('# - finding chases\n');
for i=1:options.nSubjects
    for j=1:options.nSubjects
        if i == j
            continue
        end
        relative = [orig.x(j, :) - orig.x(i, :); orig.y(j, :) - orig.y(i, :)];
        relative = relative ./ repmat(sqrt(sum(relative.^2)), 2, 1);
        
        speed = [orig.x(i, 2:end) - orig.x(i, 1:end-1) 0; orig.y(i, 2:end) - orig.y(i, 1:end-1) 0];
        chase = sum(speed .* relative);
%         dim = 50;
%         chase = conv(chase, ones(1,dim) / dim);
        chase(~isfinite(chase)) = 0;
       
        direction = speed ./ repmat(sqrt(sum(speed.^2)), 2, 1);
        angle = acos(sum(direction .* relative));
        %angle(chase < 0.05) = p;
        %atan2(relative(2, :) - direction(2, :), relative(1, :) - direction(1, :));
        
%         offset = 1;
%         data.chase(i, j, :) = chase(offset:offset + size(speed, 2) - 1);
        data.chase(i, j, :) = chase;
        
        angle(abs(chase) < options.minSpeedForAngle) = pi/2;
        
        %data.projection(i, j, :) = angle .* shiftdim(orig.miceDistance(i, j, :))';
        data.angle(i, j, :) = angle;
    end
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6)         Creating a cell array of engagements and their respective raw data

fprintf('# - setting engagemetns\n');
data.engagements = [];
for i=1:options.nSubjects-1
    for j=i+1:options.nSubjects
        c = conv([0 shiftdim(data.contacts(i,j,:))' 0], [1 -1]);
        data.engagements(i, j).startFrame = find(c > 0) - 1;
        data.engagements(i, j).endFrame = find(c < 0) - 2;
        data.engagements(i, j).startTime = data.engagements(i, j).startFrame * (orig.time(2) - orig.time(1));
        data.engagements(i, j).endTime   = data.engagements(i, j).endFrame * (orig.time(2) - orig.time(1));
    end
end

%%
for i=1:options.nSubjects-1
    for j=i+1:options.nSubjects
        for k=1:length(data.engagements(i, j).startFrame)
            s = data.engagements(i, j).startFrame(k);
            e = data.engagements(i, j).endFrame(k);
            if mean(data.chase(i, j, s:e)) - mean(data.chase(j, i, s:e)) > 0
                data.engagements(i, j).title{k} = ['[ ' num2str(i) ' -> ' num2str(j) ' ]'];
            else
                data.engagements(i, j).title{k} = ['[ ' num2str(j) ' -> ' num2str(i) ' ]'];
            end
        end
    end
end
fprintf('# done\n');
