function options = ParseArguments(varargin)
if isempty(varargin) || ~isstruct(varargin{1})
    options = struct();
else
    options = varargin{1};
    varargin = { varargin{2:end} };
end
for i=1:2:length(varargin)
    options.(varargin{i}) = varargin{i+1};
end
    