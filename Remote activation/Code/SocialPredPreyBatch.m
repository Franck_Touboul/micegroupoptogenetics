%%
SocialExperimentData
experiments = {experiments{6}};

output = false;
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions'});
        catch me
            MyWarning(me)
            corrupt = {corrupt{:}, prefix};
            continue
        end
        if ~isfield(obj, 'Interactions')
            obj = TrackLoad([obj.OutputPath prefix '.obj.mat'], {'common'});
            obj = SocialPredPreyModel(obj);
            obj = SocialAnalysePredPreyModel(obj);
        end
        if ~isfield(obj.Interactions.PredPrey, 'PostPredPrey');
            obj = SocialAnalysePredPreyModel(obj);
        end
        %%
        curr = obj.Interactions.PredPrey;
        mat = curr.PostPredPrey - curr.PostPredPrey';
        mat(binotest(curr.PostPredPrey, curr.PostPredPrey + curr.PostPredPrey')) = 0;
        mat = mat .* (mat > 0);
        
        [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
        obj.Colors.Centers = MyMouseColormap;
        ncontacts = sum(curr.Contacts, 2);
        labels = {};
%         for i=1:obj.nSubjects
%             labels{i} = num2str(ncontacts(i));
%         end
        [~, bg] = SocialGraph(obj, mat, removed, rank, [], labels);
        g = biograph.bggui(bg);
        if output
            f = get(g.biograph.hgAxes, 'Parent');
            ppfile = [obj.OutputPath 'predprey2.' prefix '.SocialPredPreyBatch.eps'];
            print(f, ppfile, '-dpsc2');
            close(g.hgFigure);
        else
            %%
            h = subplot(2,2,day);
            cla(h)
            a1 = gca;
            fig2 = get(g.biograph.hgAxes,'children');
            copyobj(fig2, a1)
            axis off;
            set(gcf, 'Color', 'w');
            title(['Day ' num2str(day)]);
            close(g.hgFigure);
        end

    end
end

return
%%
experiments = {...
    'Enriched.exp0006.day%02d.cam01',...
    'Enriched.exp0005.day%02d.cam04',...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam01',...
    'SC.exp0007.day%02d.cam04',...
    };

E.map = logical([1 1 1 0 1 0 1 0 1 0 0 0 0]);
E.idx = find(E.map);

SC.map = ~E.map;
SC.idx = find(SC.map);

nDays = 4;
%%
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        obj = TrackLoad(['Res/' prefix '.obj.mat']);
        obj = func(obj);
    end
end





return;

AllGroups = {...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam04',...
    };

EnrichedGroups = {...
    'Enriched.exp0001.day%02d.cam04',...
    'Enriched.exp0002.day%02d.cam04',...
    'Enriched.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    };

%objs = {};
%%
a = EnrichedGroups;
all = {};
parfor i=1:length(a)
    curr = {};
    for day = 1:4;
        prefix = sprintf(a{i}, day);
        fprintf('# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions', 'Common'});
            %obj = SocialPredPreyModel(obj);
            obj = SocialAnalysePredPreyModel(obj);
            curr{day} = obj.Interactions.PredPrey;
            %saveFigure(['Graphs/' prefix '.SocialAnalysePredPreyModel']);
        catch me
           fprintf(['#   . FAILED! ' me.message '\n']); 
           for j=1:length(me.stack)
               fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
           end
        end
    end
    all{i} = curr;
end

%%
PostPred = [];
PostPrey = [];
PrePred = [];
PrePrey = [];
Contacts = [];
for i=1:length(a)
    for day = 1:4;
        p = all{i}{day}.PostPred;
        p(1:first.nSubjects+1:end) = nan;
        PostPred = cat(3, PostPred, p);

        p = all{i}{day}.PostPrey;
        p(1:first.nSubjects+1:end) = nan;
        PostPrey = cat(3, PostPrey, p);

        p = all{i}{day}.PrePred;
        p(1:first.nSubjects+1:end) = nan;
        PrePred = cat(3, PrePred, p);

        p = all{i}{day}.PrePrey;
        p(1:first.nSubjects+1:end) = nan;
        PrePrey = cat(3, PrePrey, p);

        p = all{i}{day}.Contacts;
        p(1:first.nSubjects+1:end) = nan;
        Contacts = cat(3, Contacts, p);
    end
end
% x = sequence(0, max([PostPred(:), PrePred(:)]), 20);
% hPostPred = histc(PostPred(:),x);
% hPrePred = histc(PrePred(:),x);
% plot(x, hPostPred, x, hPrePred)
% legend('post', 'pre');

subplot(2,1,1);
plot(PostPrey(:)./Contacts(:), PrePred(:)./Contacts(:), '.')

subplot(2,1,2);
plot(PostPred(:)./Contacts(:), PostPrey(:)./Contacts(:), '.')
%%
a = AllGroups;
for i=6 %:length(a)
    all = {};
    PostPred = zeros(4);
    PostPrey = zeros(4);
    PrePred = zeros(4);
    objs = {};
    parfor day = 1:4;
        prefix = sprintf(a{i}, day);
        fprintf('# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions', 'Common'});
            if day==1
                objs{day} = obj;
            end
            %obj = SocialPredPreyModel(obj);
            obj = SocialAnalysePredPreyModel(obj);
            all{day} = obj.Interactions.PredPrey;
            PostPred = PostPred + obj.Interactions.PredPrey.PostPred;
            PostPrey = PostPrey + obj.Interactions.PredPrey.PostPrey;
            PrePred = PrePred + obj.Interactions.PredPrey.PrePred;
            %saveFigure(['Graphs/' prefix '.SocialAnalysePredPreyModel']);
            objs{day} = obj;
        catch me
           fprintf(['#   . FAILED! ' me.message '\n']); 
           for j=1:length(me.stack)
               fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
           end
        end
    end
    %%
    first = objs{1};
%     matPred = PostPred - PostPred';
%     matPrey = PostPrey - PostPrey';
%     mat = matPred - matPrey;
%     mat(binotest(PostPred, PostPred + PostPred') & binotest(PostPrey, PostPrey + PostPrey')) = 0;
%     mat = mat .* (mat > 0);    
%     [rank, removed] = TopoFeedbackArcSetOrder(mat);
    
    nDays = 4;
    rank = zeros(nDays, first.nSubjects);
    for day = 1:nDays
        matPred = all{day}.PostPred - all{day}.PostPred';
        matPrey = (all{day}.PostPrey - all{day}.PostPrey') * 0;
        mat = matPred - matPrey;
        mat(binotest(all{day}.PostPred, all{day}.PostPred + all{day}.PostPred') & ...
            binotest(all{day}.PostPrey, all{day}.PostPrey + all{day}.PostPrey')) = 0;
        mat = mat .* (mat > 0);
        [rank(day, :), removed] = TopoFeedbackArcSetOrder(mat ./ (all{day}.Contacts + all{day}.Contacts'));
    end
    rank = sum(rank) ./ (sum(rank > 0) + 1);
    
    for day = 1:nDays
        prefix = sprintf(a{i}, day);

        matPred = all{day}.PostPred - all{day}.PostPred';
        matPrey = (all{day}.PostPrey - all{day}.PostPrey') * 0;
        mat = matPred - matPrey;
        mat(binotest(all{day}.PostPred, all{day}.PostPred + all{day}.PostPred') & ...
            binotest(all{day}.PostPrey, all{day}.PostPrey + all{day}.PostPrey')) = 0;
        mat = mat .* (mat > 0);
        
        szc = sum(all{day}.Contacts);
        sz = sum(all{day}.PostPred, 2)';
        
        removed = ((mat > 0) .* repmat(rank, first.nSubjects, 1)) > repmat(rank', 1, first.nSubjects);
        [~, bg] = SocialGraph(first, mat, removed, rank, sz);
        for k=1:length(bg.nodes)
            bg.nodes(k).ID = sprintf('%s(%d/%d)', bg.nodes(k).ID, sz(k), szc(k));
        end
        
        g = biograph.bggui(bg);
        f = get(g.biograph.hgAxes, 'Parent');
        print(f, [first.OutputPath 'predprey.' prefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');
        fprintf(['# - saving to ' first.OutputPath 'predprey.' prefix '.SocialAnalysePredPreyModel_v2.png\n']);
        close(g.hgFigure);
        %%
        if 1 == 2
        matPred = all{day}.PrePred - all{day}.PrePred';
        matPrey = 0;
        mat = matPred - matPrey;
        mat(binotest(all{day}.PrePred, all{day}.PrePred + all{day}.PrePred')) = 0;
        mat = mat .* (mat > 0);
        
        szc = sum(all{day}.Contacts);
        sz = sum(all{day}.PostPred, 2)';
        
        removed = ((mat > 0) .* repmat(rank, first.nSubjects, 1)) > repmat(rank', 1, first.nSubjects);
        [~, bg] = SocialGraph(first, mat, removed, rank, sz);
        for k=1:length(bg.nodes)
            bg.nodes(k).ID = sprintf('%s(%d/%d)', bg.nodes(k).ID, sz(k), szc(k));
        end
        
        g = biograph.bggui(bg);
        f = get(g.biograph.hgAxes, 'Parent');
        print(f, [first.OutputPath 'initiate.' prefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');
        close(g.hgFigure);
    
        %%
        matPred = all{day}.PostPredPrey - all{day}.PostPredPrey';
        matPrey = 0;
        mat = matPred - matPrey;
        mat(binotest(all{day}.PostPredPrey, all{day}.PostPredPrey + all{day}.PostPredPrey')) = 0;
        mat = mat .* (mat > 0);
        
        szc = sum(all{day}.Contacts);
        sz = sum(all{day}.PostPred, 2)';
        
        removed = ((mat > 0) .* repmat(rank, first.nSubjects, 1)) > repmat(rank', 1, first.nSubjects);
        [~, bg] = SocialGraph(first, mat, removed, rank, sz);
        for k=1:length(bg.nodes)
            bg.nodes(k).ID = sprintf('%s(%d/%d)', bg.nodes(k).ID, sz(k), szc(k));
        end
        
        g = biograph.bggui(bg);
        f = get(g.biograph.hgAxes, 'Parent');
        print(f, [first.OutputPath 'ppp.' prefix '.SocialAnalysePredPreyModel_v2.png'], '-dpng');
        close(g.hgFigure);
        end
    end
end
fprintf('# done\n');return;
%%
for i=1
    for day = 2;
        prefix = sprintf(a{i}, day);
        fprintf('# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions'});
            %obj = SocialPredPreyModel(obj);
            obj = SocialAnalysePredPreyModel_v2(obj);
            
            %saveFigure(['Graphs/' prefix '.SocialAnalysePredPreyModel']);
        catch me
           fprintf(['#   . FAILED! ' me.message '\n']); 
           for j=1:length(me.stack)
               fprintf('#      - <a href="matlab: opentoline(which(''%s''),%d)">%s at %d</a>\n', me.stack(j).file, me.stack(j).line, me.stack(j).name, me.stack(j).line);
           end
        end
    end
end
%%
a = {...
    'Enriched.exp0001.day01.cam04',...
    'Enriched.exp0002.day01.cam04',...
    'Enriched.exp0003.day01.cam01',...
    'Enriched.exp0004.day01.cam01',...
    'SC.exp0001.day01.cam01',...
    'SC.exp0002.day01.cam01',...
    'SC.exp0003.day01.cam01',...
    'SC.exp0004.day01.cam04',...
    };

%objs1 = {};
%
for i=1:length(objs1)
    fprintf('# -> %s\n', objs1{1}.FilePrefix);
    try
        obj = objs1{i};
        %curr =load(['../base/Res/' a{i} '.obj.mat']);
        %curr.obj = SocialPredPreyModel(curr.obj);
        obj = SocialAnalysePredPreyModel(obj);
        %objs1{i} = curr.obj;
        saveFigure(['Graphs/' objs1{1}.FilePrefix '.SocialAnalysePredPreyModel2']);
    catch
    end
end