% creates a 'SubStation Alpha' to show the locations of the mice
%%
TrackDefaults
filename = [options.output_path options.test.name '.social.mat'];
load(filename);
%%
xyloObj = myMMReader(options.MovieFile);
nframes = xyloObj.NumberOfFrames;
vidHeight = xyloObj.Height;
vidWidth = xyloObj.Width;
dt = 1/xyloObj.FrameRate;

%%
basename = regexprep(options.MovieFile, '\.[^\.]*$', '');

%hf = fopen(['Res/' options.test.name '.ass'], 'w');
hf = fopen([basename '.ass'], 'w');
fprintf(hf, '[Script Info]\n');
fprintf(hf, 'Title: Default Aegisub file\n');
fprintf(hf, 'ScriptType: v4.00+\n');
fprintf(hf, 'WrapStyle: 0\n');
fprintf(hf, ['PlayResX: ' num2str(vidWidth) '\n']);
fprintf(hf, ['PlayResY: ' num2str(vidHeight) '\n']);
fprintf(hf, 'ScaledBorderAndShadow: no\n');
fprintf(hf, 'Last Style Storage: Default\n');
fprintf(hf, ['Video File: ' options.MovieFile '\n']);
fprintf(hf, 'Video Aspect Ratio: 0\n');
%fprintf(hf, 'Video Zoom: 4\n');
%fprintf(hf, 'Video Position: 175\n');

fprintf(hf, '[V4+ Styles]\n');
fprintf(hf, 'Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding\n');
fprintf(hf, 'Style: Default,Arial,20,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,2,0,2,10,10,10,1\n');

fprintf(hf, '[Events]\n');
fprintf(hf, 'Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text\n');


options.sensitivity = 5;

time = cell(1, options.nSubjects);
colorhex = cell(1, options.nSubjects);

for curr = 1:options.nSubjects
    prev.x(curr) = social.x(curr, 1);
    prev.y(curr) = social.y(curr, 1);
    time{curr} = sec2time_v2(0);
    color = social.meta.subject.centerColors(curr, :);
    colorhex{curr} = [dec2hex(floor(color(1) * 255)) dec2hex(floor(color(2) * 255)) dec2hex(floor(color(3) * 255))];
end
fprintf('# - frame ');
nchars = RePrintf('%6d/%6d', 1, size(social.x, 2));
for i=2:size(social.x, 2)
    if mod(i, 312) == 0
        nchars = RePrintf(nchars, '%6d/%6d', i, size(social.x, 2));
    end
    dist = sqrt((social.x(:, i) - prev.x') .^ 2 + (social.y(:, i) - prev.y') .^ 2);
    refresh = max(dist)  > options.sensitivity;
    for curr=1:options.nSubjects
        if dist(curr) > options.sensitivity
            start = time{curr};
            time{curr} = sec2time_v2(social.time(i));
            finish = time{curr};
            posx = num2str(prev.x(curr));
            posy = num2str(prev.y(curr));
            fprintf(hf, ...
                ['Dialogue: ' num2str(curr) ',' start ',' finish ',Default,,0000,0000,0000,,{\\pos(' posx ',' posy ')\\b1\\c&H' colorhex{curr} '&\\fs30}' num2str(curr) '\n']);
            prev.x(curr) = social.x(curr, i);
            prev.y(curr) = social.y(curr, i);
        end
    end
end
nchars = RePrintf(nchars, '%6d/%6d', i, size(social.x, 2));
fprintf('\n');

for curr=1:options.nSubjects
    start = time{curr};
    time{curr} = sec2time_v2(social.time(i));
    finish = time{curr};
    posx = num2str(social.x(curr, i));
    posy = num2str(social.y(curr, i));
    fprintf(hf, ...
        ['Dialogue: 0,' start ',' finish ',Default,,0000,0000,0000,,{\\pos(' posx ',' posy ')\\b1\\c&' colorhex{curr} '&\\fs30}' num2str(curr) '\n']);
end
fclose(hf);
