function V = Majority(x)
[v, c] = count_unique(x);
[q, i] = max(c);
V = v(i);