SocialExperimentData;
experiments = {experiments{GroupsData.Standard.idx}};

%%
nbins = 50;
%range = [min(obj.x(:)), max(obj.x(:)), min(obj.VideoHeight - obj.y(:)), max(obj.VideoHeight - obj.y(:))];
%%
vals = [];
H = {zeros(nbins), zeros(nbins), zeros(nbins), zeros(nbins)};
stat = struct('papproach', {[], [], [], []}, 'pchase', {[], [], [], []}, 'chases', {[], [], [], []}, 'escapes', {[], [], [], []}, 'contacts', {[], [], [], []}, 'zones', {[], [], [], []}, 'entropy', {[], [], [], []});
common.nlevels = [];
common.strength = [];

for id=1:length(experiments)
    pp = zeros(GroupsData.nSubjects);
    approach = zeros(GroupsData.nSubjects);
    ppfull = zeros(GroupsData.nSubjects);
    ev = zeros(GroupsData.nSubjects);
    zones = zeros(GroupsData.nSubjects, 10);
    x = [];
    y = [];
    pmap = [];
    prank = [];
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'common', 'Hierarchy', 'Interactions'});
        catch
            vals(id, day) = nan;
            continue;
        end
        if ~isfield(obj, 'Interactions') || ~isfield(obj.Interactions.PredPrey, 'PostPredPrey')
            obj = TrackLoad(['../interactions/Res/' prefix '.obj.mat'], {'common', 'Hierarchy', 'Interactions'});
        end

        ppfull = ppfull + obj.Interactions.PredPrey.PostPredPrey;
        if day > 0
            pp = pp + obj.Interactions.PredPrey.PostPredPrey;
            for i=1:obj.nSubjects
                zones(i, :) = zones(i, :) + histc(obj.zones(i, :), 1:obj.ROI.nZones);
                for j=1:obj.nSubjects
                    ev(i, j) = ev(i, j) + length(obj.Interactions.PredPrey.events{i, j});
                    for e=1:length(obj.Interactions.PredPrey.events{i,j})
                        approach(i, j) = approach(i, j) + any(obj.Interactions.PredPrey.events{i,j}{e}.data == 2);
                    end
                end
            end
        end

        mat = ppfull - ppfull';
        %mat(binotest(pp, pp + pp')) = 0;
        mat(binotest(ppfull, ppfull + ppfull')) = 0;
        mat = mat .* (mat > 0);
        [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);

        common.nlevels(id, day) = length(unique(rank));
        common.strength(id, day) = sum(mat(:) > 0);
        if day>1
            common.change(id, day) = sum(sum(mat > 0 & pmap > 0));
        end
        pmap = mat;
        prank = rank;
        %x = [x, obj.x(:, ~obj.hidden)];
        %y = [y, obj.y(:, ~obj.hidden)];
    end
    mat = ppfull - ppfull';
    %mat(binotest(pp, pp + pp')) = 0;
    mat = mat .* (mat > 0);
    [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
    try
        ranks = max(rank) - rank + 1;
        ranks
        %ranks = rank;
        for i=1:obj.nSubjects
            r = ranks(i);
            stat(r).chases = [stat(r).chases, sum(pp(i, :))'];
            stat(r).papproach = [stat(r).papproach, sum(approach(i, :))'/ sum(ev(i, :))'];
            stat(r).pchase = [stat(r).pchase, sum(pp(i, :))' / sum(ev(i, :))'];
            stat(r).escapes = [stat(r).escapes, sum(pp(:, i))];
            stat(r).contacts = [stat(r).contacts, sum(ev(i, :))'];
            stat(r).zones = [stat(r).zones; zones(i, :)/sum(zones(i, :))];
            stat(r).entropy = [stat(r).entropy, JointEntropy(obj.zones(i, :))];
            %H{r} = cat(3, H{r}, HexHist([obj.x(~obj.hidden), obj.VideoHeight - obj.y(~obj.hidden)], nbins, 'range', range));
            %H{r} = H{r} + HexHist([x, obj.VideoHeight - y], nbins, 'range', range);
        end
    catch me
        warning me.message
    end

%     for day = 1:nDays
%         prefix = sprintf(experiments{id}, day);
%         try
%             obj = TrackLoad(['Res/' prefix '.obj.mat'], {'common', 'Hierarchy', 'Interactions'});
%         catch
%             vals(id, day) = nan;
%             continue;
%         end
%         try
%         ranks = max(obj.Hierarchy.ChaseEscape.rank) - obj.Hierarchy.ChaseEscape.rank + 1;
%         for i=1:obj.nSubjects
%             r = ranks(i);
%             stat(r).chases = [stat(r).chases, sum(obj.Interactions.PredPrey.PostPredPrey(i, :))'];
%             stat(r).escapes = [stat(r).escapes, sum(obj.Interactions.PredPrey.PostPredPrey(:, i))];
%             contacts = 0;
%             for j=1:obj.nSubjects
%                 contacts = contacts + length(obj.Interactions.PredPrey.events{i, j});
%             end
%             stat(r).contacts = [stat(r).contacts, contacts];
%             stat(r).zones = [stat(r).zones; histc(obj.zones(i, :), 1:obj.ROI.nZones)];
%             %H{r} = cat(3, H{r}, HexHist([obj.x(~obj.hidden), obj.VideoHeight - obj.y(~obj.hidden)], nbins, 'range', range));
%             H{r} = H{r} + HexHist([obj.x(~obj.hidden), obj.VideoHeight - obj.y(~obj.hidden)], nbins, 'range', range);
%         end
%         catch me
%             warning me.message
%         end
%     end
end

%%
%HexPlot([H{1}/length(stat(1).contacts) H{2}/length(stat(2).contacts) H{3}/length(stat(3).contacts) H{4}/length(stat(4).contacts)], range)
%%
[c, names] = SoftZoneCategorize(obj.ROI.ZoneNames);
allstat = struct('zones', [], 'stdzones', [], 'contacts', [], 'chases', [], 'stdcontacts', [], 'stdchases', [], 'entropy', [], 'stdentropy', [], 'pchase', [], 'stdpchase', [], 'papproach', [], 'stdpapproach', []);
prev = [];
for i=1:obj.nSubjects
    %mySubplot(obj.nSubjects, 5, i, 1:3);
    zones = stat(i).zones; % ./ repmat(sum(stat(i).zones,2), 1, size(stat(i).zones, 2));
    nzones = zeros(size(zones, 1), length(names));
    for j=1:length(c)
        nzones(:, c(j)) = nzones(:, c(j)) + zones(:, j);
    end
    allstat.zones(i, :) = mean(100 * nzones);
    allstat.stdzones(i, :) = stderr(100 * nzones);
    allstat.contacts(i) = mean(stat(i).contacts);
    allstat.stdcontacts(i) = stderr(stat(i).contacts);
    allstat.chases(i) = mean(stat(i).chases);
    allstat.stdchases(i) = stderr(stat(i).chases);
    allstat.entropy(i) = mean(stat(i).entropy);
    allstat.stdentropy(i) = stderr(stat(i).entropy);
    allstat.pchase(i) = mean(100 * stat(i).pchase);
    allstat.stdpchase(i) = stderr(100 * stat(i).pchase);
    allstat.papproach(i) = mean(100 * stat(i).papproach);
    allstat.stdpapproach(i) = stderr(100 * stat(i).papproach);
    if i>1
        for k=1:length(names)
            allstat.sig(k, i) = ttest2(nzones(:, k), prev(:, k));
        end
    end
    prev = nzones;
end
for i=1:size(allstat.zones, 2)
    mySubplot(3, size(allstat.zones, 2), 1, i);
    MyBar(1:obj.nSubjects, allstat.zones(:, i), allstat.stdzones(:, i), MyDefaultColormap(obj.nSubjects+1));
    if i == 1
        ylabel('precentage of time');
    end
    title(names{i});
end

% significance plot 
for i=1:size(allstat.zones, 2)
    mySubplot(3, size(allstat.zones, 2), 1, i);
    for j=1:obj.nSubjects
        if allstat.sig(i, j)
            a = axis;
            text(j-.5, a(4), '*');
        end
    end
end

mySubplot(3, size(allstat.zones, 2), 2, 1);
MyBar(1:obj.nSubjects, allstat.contacts, allstat.stdcontacts, MyDefaultColormap(obj.nSubjects+1));
title('# contacts');

mySubplot(3, size(allstat.zones, 2), 2, 2);
MyBar(1:obj.nSubjects, allstat.chases, allstat.stdchases, MyDefaultColormap(obj.nSubjects+1));
title('# chases');

mySubplot(3, size(allstat.zones, 2), 2, 3);
MyBar(1:obj.nSubjects, allstat.pchase, allstat.stdpchase, MyDefaultColormap(obj.nSubjects+1));
title('% chase');

mySubplot(3, size(allstat.zones, 2), 2, 4);
MyBar(1:obj.nSubjects, allstat.papproach, allstat.stdpapproach, MyDefaultColormap(obj.nSubjects+1));
title('% approach');

mySubplot(3, size(allstat.zones, 2), 2, 5);
MyBar(1:obj.nSubjects, allstat.entropy, allstat.stdentropy, MyDefaultColormap(obj.nSubjects+1));
title('entropy');

mySubplot(3, size(allstat.zones, 2), 3, 1);
MyBar(1:nDays, mean(common.strength), stderr(common.strength), repmat([0 0 1], nDays, 1));
title('hierarchy strength');
xlabel('Days');

mySubplot(3, size(allstat.zones, 2), 3, 2);
MyBar(1:nDays, mean(common.nlevels), stderr(common.nlevels), repmat([0 0 1], nDays, 1));
title('hierarchy levels');
xlabel('Days');

mySubplot(3, size(allstat.zones, 2), 3, 3);
MyBar(1:nDays, mean(common.change), stderr(common.change), repmat([0 0 1], nDays, 1));
title('hierarchy levels');
xlabel('Days');
%%
% cmap = MyCategoricalColormap;
% allstat = struct('zones', [], 'stdzones', []);
% for i=1:obj.nSubjects
%     zones = stat(i).zones ./ repmat(sum(stat(i).zones,2), 1, size(stat(i).zones, 2));
%     allstat.zones(i, :) = mean(zones);
%     allstat.stdzones(i, :) = stderr(zones);
% end
% for i=1:obj.ROI.nZones
%     SquareSubplpot(obj.ROI.nZones, i);
%     %barweb(allstat.zones(:, i)', allstat.stdzones(:, i)', [], [], [], [], [], MyCategoricalColormap);
%     MyBar(allstat.zones(:, i), allstat.stdzones(:, i));
%     pzones = [];
%      for j=1:obj.nSubjects
%         hon
%         zones = stat(j).zones ./ repmat(sum(stat(j).zones,2), 1, size(stat(j).zones, 2));
%         if j>1
%             a = axis;
%             if ttest2(zones(:, i), pzones(:, i))
%                 text(j-.5, a(4), '*');
%             end
%
%         end
%         pzones = zones;
%         %plot(j-.2, zones(:, i), 'k.');
%     end
%      hoff;
%     title(obj.ROI.ZoneNames{i});
% end
