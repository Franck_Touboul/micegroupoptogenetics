function [NormDSonDij, NormDSonPij] = SocialDavidScore(obj)
%%
nij = obj.Hierarchy.Group.AggressiveChase.ChaseEscape + obj.Hierarchy.Group.AggressiveChase.ChaseEscape';
Sij = obj.Hierarchy.Group.AggressiveChase.ChaseEscape;

% Sij = [0 2 1 2 1; 1 0 1 1 1; 0 0 0 1 2; 0 0 1 0 3; 0 0 0 0 0];
% nij = Sij + Sij';
% 
%  Sij = [0 0 1 2 10 63 8; 0 0 2 3 0 88 4; 0 0 0 4 65 84 3; 0 0 0 0 0 80 10; 0 0 0 0 0 4 1; 0 1 5 0 10 0 6; 0 0 0 0 0 2 0];
%  nij = Sij + Sij';

% Sij = [0 10 5 10 5; 5 0 5 5 5; 0 0 0 5 10; 0 0 5 0 15; 0 0 0 0 0];
% nij = Sij + Sij';
 
Pij = Sij./nij;
Pij(nij == 0) = 0;
[w, w2, l, l2] = SocialDavidScoreAux(Pij);
DS = w + w2 - l - l2;
NormDSonPij = (DS + size(Sij,1) * (size(Sij,1) - 1)/2) / size(Sij,1);

Dij = (Sij + .5) ./ (nij + 1);
Dij(nij == 0) = 0;
[w, w2, l, l2] = SocialDavidScoreAux(Dij);
DS = w + w2 - l - l2;
NormDSonDij = (DS + size(Sij,1) * (size(Sij,1) - 1)/2) / size(Sij,1);
%plot(sort(NormDS));
