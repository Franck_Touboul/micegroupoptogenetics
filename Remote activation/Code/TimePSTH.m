function [h, x] = TimePSTH (obj)
WindowSize = 200;
FrameSize = 10;
MinWindowSize = 50;

%%
zones = obj.zones(:, obj.valid);
[si, st] = find(diff([zeros(obj.nSubjects, 1) zones], 1, 2));
ST = cell(1, obj.nSubjects);
for s=1:obj.nSubjects
    ST{s} = st(si == s);
end
%%
h = [];
nrmlzWindowSize = ceil(WindowSize / FrameSize);
x = (-nrmlzWindowSize:nrmlzWindowSize) * FrameSize / obj.FrameRate;
for s1 = 1:obj.nSubjects;
    other = 1:obj.nSubjects; other = other(other ~= s1);
    
    change = [false(obj.nSubjects, 1) diff(zones, 1, 2) ~= 0];
    changeF = convn(change, ones(1, FrameSize));
    changeF = changeF(:, FrameSize:FrameSize:length(changeF)-mod(length(change), FrameSize)) > 0;
    
    idxF = find(changeF(s1, :));
    idxF = idxF([idxF(1) diff(idxF)] >= ceil(MinWindowSize / FrameSize));
    
    z = zeros(obj.nSubjects-1, 2*nrmlzWindowSize + 1);
    zonecount = zeros(obj.ROI.nZones, 2*nrmlzWindowSize + 1);
    for i=1:length(idxF)
        if idxF(i) <= nrmlzWindowSize || idxF(i) + nrmlzWindowSize >= size(changeF, 2)
            continue;
        end
        z = z + changeF(other, idxF(i)-nrmlzWindowSize:idxF(i)+nrmlzWindowSize);
    end
    %%
    if nargout == 0
        plot(x, sum(z)/ length(idxF))
        a = axis;
        fill([-MinWindowSize / obj.FrameRate, MinWindowSize / obj.FrameRate, MinWindowSize / obj.FrameRate, -MinWindowSize / obj.FrameRate], [a(3) a(3) a(4) a(4)], ones(1,3)*.8, 'edgecolor', 'none');
        hon;
        plot(x, sum(z) / length(idxF))
        vert_line(0, 'color', 'k');
        axis([min(x) max(x) a(3) a(4)]);
        xlabel('time lag [secs]');
        hoff;
    end
    %%
    h(s1, :) = sum(z)/ length(idxF);
end