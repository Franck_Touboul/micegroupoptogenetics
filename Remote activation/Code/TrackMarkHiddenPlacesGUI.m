function [obj, roi] = TrackMarkHiddenPlacesGUI(obj, frame)

if ~exist('frame', 'var')
    if ~isempty(obj.BkgImage)
        img = obj.BkgImage;
    else
        img = myMMReader(obj.VideoFile, randi(obj.nFrames));
    end
else
    img = frame;
end
img = im2double(img);

clf;
imshow(img);
index = 1;
roi = {};
cmap = lines(7);
while true
    title(sprintf('mark hidden region no. %d...', index));
    roi{index} = roipoly;
    img = TrackShowRegion(img, roi{index}, cmap(mod(index-1, 7)+1, :));
    imshow(img);
    title(sprintf('continue?'));
    k = 0;
    while k ~= 1
        k = waitforbuttonpress;
    end
    key = get(gcf, 'CurrentCharacter');
    if char(key) ~= 'y' && char(key) ~= 'Y'
        break;
    end
    index = index + 1;
end

title(sprintf('Done!'));

obj.ROI.Hidden = roi;
obj.ROI.HiddenImage = img;
obj.ROI.nHidden = length(obj.ROI.Hidden);

%%
obj.ROI.HiddenCenters = zeros(obj.ROI.nHidden, 2);
obj.ROI.HiddenSCenters = zeros(obj.ROI.nHidden, 2);
for i=1:obj.ROI.nHidden
    [x, y] = find(obj.ROI.Hidden{i});
    obj.ROI.HiddenCenters(i, :) = [mean(y), mean(x)];
    obj.ROI.HiddenSCenters(i, :) = obj.ROI.HiddenCenters(i, :) * obj.VideoScale;
end

%% conpute bounderies of roi's in scaled coordinates 
obj.ROI.HiddenBoundarySCoordinates = {};
for i=1:obj.ROI.nHidden
    img = imresize(obj.ROI.Hidden{i}, obj.VideoScale);
    [x, y] = find((imdilate(img ~= 0, ones(3,3)) - (img ~= 0)) ~= 0);
    obj.ROI.HiddenBoundarySCoordinates{i} = [y, x];
end

% compute min distance between roi's
obj.ROI.HiddenSDistances = zeros(obj.ROI.nHidden);
for i=1:obj.ROI.nHidden
    for j=i:obj.ROI.nHidden
        if i~=j
            p = pdist2(obj.ROI.HiddenBoundarySCoordinates{i}, obj.ROI.HiddenBoundarySCoordinates{j});
            obj.ROI.HiddenSDistances(i,j) = min(p(:));
            obj.ROI.HiddenSDistances(j,i) = min(p(:));
        end
    end
end
