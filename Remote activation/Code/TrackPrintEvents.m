%function TrackPrintEvents(obj)
%%
export.x = double(obj.x);
export.y = double(obj.y);
export.time = obj.time;
export.colors = double(obj.Colors.Centers);
export.meta.subject.centerColors = double(obj.Colors.Centers);
export.zones = double(obj.zones);
export.labels = obj.ROI.ZoneNames;
%
export.Events(1).Title = 'contacts';
idx = 1;
events.Begin = [];
events.End = [];
events.BeginFrame = [];
events.EndFrame = [];
events.Members = [];
events.Ids = [];
for s1=1:obj.nSubjects
    for s2=s1+1:obj.nSubjects
        for i=1:length(obj.Hierarchy.ChaseEscape.interactions.events{s1, s2})
            events.Begin(idx) = obj.Hierarchy.ChaseEscape.interactions.events{s1, s2}{i}.BegFrame / obj.FrameRate;
            events.End(idx) = obj.Hierarchy.ChaseEscape.interactions.events{s1, s2}{i}.EndFrame / obj.FrameRate;
            events.BeginFrame(idx) = obj.Hierarchy.ChaseEscape.interactions.events{s1, s2}{i}.BegFrame;
            events.EndFrame(idx) = obj.Hierarchy.ChaseEscape.interactions.events{s1, s2}{i}.EndFrame;
%            events.Desc{idx} = obj.Hierarchy.ChaseEscape.interactions.events{s1, s2}{i}.desc;
            %events.Desc{idx} = '';
            events.Members(idx) = s1 * 10 + s2;
            events.Ids(idx) = i;
            %fprintf('%d\n', idx);
            
            idx = idx + 1;
            
        end
    end
end
[qqq, o] = sort(events.End);
events.Begin = events.Begin(o);
events.End = events.End(o);
events.BeginFrame = events.BeginFrame(o);
events.EndFrame = events.EndFrame(o);
events.Ids = events.Ids(o);

events.Members = events.Members(o);
% match
s1 = floor(events.Members / 10);
s2 = mod(events.Members, 10);
for i=1:length(events.Begin)
    if i>500
        break;
    end
    C = obj.Hierarchy.ChaseEscape.interactions.events{s2(i), s1(i)};
    e1 = obj.Hierarchy.ChaseEscape.interactions.events{s1(i), s2(i)}{events.Ids(i)};
    if s1(i)==1; S1='P'; elseif s1(i)==2; S1='R'; elseif s1(i)==3; S1='O'; else S1='G'; end
    if s2(i)==1; S2='P'; elseif s2(i)==2; S2='R'; elseif s2(i)==3; S2='O'; else S2='G'; end
    fprintf('%03d', i-1);
    for j=1:length(C)
        if SegmentsOverlap([C{j}.BegFrame C{j}.EndFrame], [events.BeginFrame(i) events.EndFrame(i)]);
            e2 = C{j};
            if any(e1.data == 2)
                fprintf('\tApproach(%s, %d-%d)', S1, e1.BegFrame + find(e1.data == 2, 1) - 1, e1.BegFrame + find(e1.data == 2, 1, 'last') - 1);
            end
            if any(e2.data == 2)
                fprintf('\tApproach(%s, %d-%d)', S2, e2.BegFrame + find(e2.data == 2, 1) - 1, e2.BegFrame + find(e2.data == 2, 1, 'last') - 1);
            end
            if any(e1.data == 5) && any(e2.data == 6)
                fprintf('\tChase(%s->%s, %d-%d)', S1, S2, ...
                    max(e1.BegFrame + find(e1.data == 5, 1) - 1, e2.BegFrame + find(e2.data == 6, 1) - 1), ...
                    min(e1.BegFrame + find(e1.data == 5, 1, 'last') - 1, e2.BegFrame + find(e2.data == 6, 1, 'last') - 1));
            elseif any(e1.data == 6) && any(e2.data == 5)
                fprintf('\tChase(%s->%s, %d-%d)', S2, S1, ...
                    max(e1.BegFrame + find(e1.data == 6, 1) - 1, e2.BegFrame + find(e2.data == 5, 1) - 1), ...
                    min(e1.BegFrame + find(e1.data == 6, 1, 'last') - 1, e2.BegFrame + find(e2.data == 5, 1, 'last') - 1));
            else
                if any(e1.data == 6)
                    fprintf('\tLeave(%s, %d-%d)', S1, e1.BegFrame + find(e1.data == 6, 1) - 1, e1.BegFrame + find(e1.data == 6, 1, 'last') - 1);                
                end
                if any(e2.data == 6)
                    fprintf('\tLeave(%s, %d-%d)', S2, e2.BegFrame + find(e2.data == 6, 1) - 1, e2.BegFrame + find(e2.data == 6, 1, 'last') - 1);                
                end
            end
            
        end
    end
    fprintf('\n');
end
%
export.Events(1).data = [events.Begin; events.End; events.Members];

