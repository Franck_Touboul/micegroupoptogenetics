function vec = PottsVectorize_v2(obj, me, map, permuts)
%%
if ~exist('permuts', 'var')
    %%
    permuts = nchoose(1:obj.nSubjects);
    valid = false(1, length(permuts));
    for i=1:length(permuts)
        permuts{i} = sort(permuts{i});
        valid(i) = length(permuts{i}) <= max(me.order);
    end
    permuts = { permuts{valid} };
end    
%%
%aux.id = zeros(1, length(permuts));
aux.order = zeros(1, length(permuts));
aux.offsets = zeros(1, length(permuts));
aux.offsets(1) = 1;
aux.ids = struct();
for i=1:length(permuts)
    if i>1
        % aux.offsets(i) = aux.offsets(i-1) + double(obj.ROI.nZones)^length(permuts{i-1});
        j = aux.offsets(i-1);
        while j <= length(me.labels)
            if size(me.labels{j}, 2) ~=  length(permuts{i - 1}) || any(me.labels{j}(1, :) ~= permuts{i - 1})
                break;
            end
            j = j + 1;
        end
        aux.offsets(i) = j;
    end
    aux.order(i) = length(permuts{i});
    svec = double(obj.nSubjects.^(aux.order(i)-1:-1:0));
    %aux.id(i) = svec * permuts{i}';
    aux.ids(aux.order(i)).map(svec * (permuts{i}' - 1) + 1) = i;
end
%%
u_ = unique(double(me.order));
for u=u_(:)'
    aux.labels = { me.labels{me.order == u} };
    aux.weights = me.weights(me.order == u);
    svec = double(obj.nSubjects.^(u-1:-1:0));
    zvec = double(obj.ROI.nZones.^(u-1:-1:0));
    %%
    p = myPerms(u, obj.ROI.nZones);
    valid = any(p == me.neutralZone, 2);
    dst = zeros(1, size(p, 1));
    dst(~valid) = 1:sum(~valid);
    %%
    for i=1:length(aux.labels)
        [s, o] = sort(map(aux.labels{i}(1, :)));
        id = svec * (s' - 1) + 1;
        offset = aux.offsets(aux.ids(u).map(id));
        d = dst((aux.labels{i}(2, o) - 1) * zvec' + 1) - 1;
        if d >= 0
            vec(offset + d) = aux.weights(i);
        end
    end
end