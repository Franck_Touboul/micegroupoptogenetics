function backtrack = ModelViterbiSequence_v2(model, input, useTransProbs)

if nargin == 2
    useTransProbs = true;
end
useDurationProbs = true;

if iscell(input)
    ncells = length(input);
    backtrack = cell(1, ncells);
else
    ncells = 1;
    backtrack = cell(1);
end

for c=1:ncells
    if iscell(input)
        data = input{c};
    else
        data = input;
    end
    [dim, ndata] = size(data);
    table = zeros(model.nstates, ndata);
    path = table;
    % emission probabilities
    prior = model.all.posterior(data, model);
    prior(isnan(prior)) = 1/pi;
    for s=1:model.nstates
        table(s, :) = model.states(s).posterior(data, model, s) ./ prior;
        %table(s, find(max(isnan(data)))) = 0;
    end
    table(:, 1) = table(:, 1) .* model.start(:);
    
    table = flog(table);
    if useTransProbs
        logTrans = log(model.trans);
    else
        logTrans = model.trans * 0;
        logTrans(model.trans == 0) = -inf;
    end
    
    %%
%     plot(exp(table)');
%     entries = {};
%     for i=1:model.nstates
%         entries{i} = num2str(i);
%     end
%     legend(entries);
    %%
    for i=2:ndata
        for s=1:model.nstates
            [val, from] = max(table(:, i-1) + logTrans(:, s));
            table(s, i) = table(s, i) + val;
            path(s, i) = from;
        end
    end
    
    %% backtrack
    curr_backtrack = zeros(1, ndata);
    table(:, end) = table(:, end) + log(model.end(:));
    [val, best] = max(table(:, end));
    curr_backtrack(end) = best;
    for i=ndata-1:-1:1
        curr_backtrack(i) = path(curr_backtrack(i+1), i+1);
    end
    %%
    if iscell(input)
        backtrack{c} = curr_backtrack;
    else
        backtrack{1} = curr_backtrack;
    end
end
