function obj = SocialPottsWithWindow(obj, param)
obj = TrackLoad(obj);
%%
if ~exist('param', 'var')
    param = struct();
end
options = setDefaultParameters(param, 'WindowDuration', 6, 'inParallel', false);
Potts = [];
%%
temp = SocialSetTimeScale(obj, options.WindowDuration);
temp.OutputToFile = false;
temp.OutputInOldFormat = false;
%%
mapidx = randperm(temp.nFrames);
map = false(1, temp.nFrames);
map(mapidx(1:round(temp.nFrames/2))) = true;
%%
trainobj = SocialApplyMap(temp, map);
testobj = SocialApplyMap(temp, ~map);
%%
if options.inParallel 
    h = {};
    h{1} = Job(SocialPackageObjForPotts(trainobj));
    h{1}.Run(@SocialPottsFirstOrder);
    h{2} = Job(SocialPackageObjForPotts(trainobj));
    h{2}.Run(@SocialPottsSecondOrder);
    h{3} = Job(SocialPackageObjForPotts(trainobj));
    h{3}.Run(@SocialPottsThirdOrder);
    h{4} = Job(SocialPackageObjForPotts(trainobj));
    h{4}.Run(@SocialPottsFourthOrder);
    for s=1:obj.nSubjects
        h{s}.WaitUntilFinished;
        curr = h{s}.Object;
        if s==1
            trainobj.Analysis.Potts = curr.Analysis.Potts;
        end
        trainobj.Analysis.Potts.Model{s} = curr.Analysis.Potts.Model{s};
    end
else
    trainobj = SocialPotts(trainobj, options);
end
Potts = trainobj.Analysis.Potts;
%%
fprintf('# - computing probabilities of the train data\n');
[q, independentProbs, jointProbs] = SocialComputePatternProbsFast(trainobj);
Potts.trainProbs = jointProbs(:)';
fprintf('# - computing probabilities of the test data\n');
[q, independentProbs, jointProbs] = SocialComputePatternProbsFast(testobj);
Potts.testProbs = jointProbs(:)';
%%
Potts.map = map;
Potts.WindowDuration = options.WindowDuration;
obj.Analysis.Potts.Windowed = Potts;
if obj.OutputToFile
    TrackSave(obj);
end
