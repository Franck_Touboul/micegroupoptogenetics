function perms = FastPerms(dim, nvals)
%%
perms = zeros(nvals^dim, dim);
vec = ones(1, dim);
perms(1, :) = vec;
for i=2:size(perms, 1)
    vec(end) = vec(end) + 1;
    for j=dim:-1:1
        if vec(j) > nvals
            vec(j-1) = vec(j-1) + 1;
            vec(j) = 1;
        else
            break;
        end
    end
    perms(i, :) = vec;
end
