function nanimagesc(x, range, cmap)
i = round((x - range(1)) / (range(2) - range(1)) * 255);
i(i < 0) = 0;
i(i > 255) = 255;
i = i + 2;
cmap = [0, 0, 0; cmap];
i(isnan(x)) = 1;
imagesc(i);
colormap(cmap);
