nSubjects = 7;
Elo.k = 200;

%%
Elo.Scores = zeros(nSubjects, size(data, 1) + 1);
Elo.Times = zeros(1, size(data, 1) + 1);
Elo.Scores(:, 1) = ones(nSubjects, 1) * 1000;
Elo.Times(1) = 1;
Elo.Map = false(nSubjects, size(data, 1) + 1);
%%
idx = 1;
for i=1:size(data, 1)
    %   obj.Contacts.List.beg
    %    obj.Contacts.Behaviors.AggressiveChase.Chaser(i)
    Pred = data(i, 1);
    Prey = data(i, 2);
    
    d = Elo.Scores(Pred, idx) - Elo.Scores(Prey, idx);
    
    ploss = 1 ./ (1 + 10 .^ (d/400));
    Elo.Scores(Pred, idx + 1) = Elo.Scores(Pred, idx) + ploss * Elo.k;
    Elo.Scores(Prey, idx + 1) = Elo.Scores(Prey, idx) - ploss * Elo.k;

    other = 1:nSubjects;
    other = other(other ~= Pred);
    other = other(other ~= Prey);
    
    Elo.Scores(other, idx + 1) = Elo.Scores(other, idx);
    Elo.Map(Pred, idx + 1) = true;
    Elo.Map(Prey, idx + 1) = true;
    
    Elo.Times(idx + 1) = i+1;
    idx = idx + 1;
end
%%
range = 2:31;

[~, rank] = sort(Elo.Scores(:, range));
for i=1:size(rank, 2)
    rank(rank(:, i), i) = 1:nSubjects;
end

C = sum(abs(diff(rank, 1, 2)));
map = [false(nSubjects, 1) diff(rank, 1, 2) == 0];
weighted = Elo.Scores(:, range) - repmat(min(Elo.Scores(:, range)), nSubjects, 1); 
weighted = weighted ./ repmat(max(weighted), nSubjects, 1);
weighted(map) = 0;
w = max(weighted);

N = sum(~map);
Elo.StabilityIndex = C*w(2:end)' / sum(N(2:end))


%%
    %     plot(Elo.Times, Elo.Scores, 'color');
    %     axis;
    
    cmap = MySubCategoricalColormap;
    for s=1:nSubjects
        plot(Elo.Times, Elo.Scores(s, :), 'color', cmap(s, :));
        hon;
    end
    hoff;
