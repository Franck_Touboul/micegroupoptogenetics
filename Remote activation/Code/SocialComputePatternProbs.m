function [obj, indepProbs, jointProbs, pci, patterns] = SocialComputePatternProbs(obj, fixed)
if ~exist('fixed', 'var')
    fixed = false;
end
% compute independent probabilities
[obj, sampIndepProbs] = SocialComputeIndependentProbs(obj);
% compute joint probabilities
[obj, jointProbs, pci] = SocialComputePatternJointProbs(obj, fixed);

%%
nZones = length(obj.ROI.ZoneNames);
nPerms = nZones^obj.nSubjects;
patterns = zeros(nPerms, obj.nSubjects);

baseVector = nZones.^(obj.nSubjects-1:-1:0);

indepProbs = zeros(1, nPerms);
for i=1:nPerms
    patterns(i, :) = decimal2base(i-1, nZones, obj.nSubjects) + 1;
    idx = baseVector * (patterns(i, :)' - 1) + 1;
    indepProbs(idx) = prod(sampIndepProbs(sub2ind(size(sampIndepProbs), 1:obj.nSubjects, patterns(i, :))));
end
