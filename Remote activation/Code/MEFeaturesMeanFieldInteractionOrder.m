function me = MEFeaturesMeanFieldInteractionOrder(data, nvals, n)
MEPrint('Computing mean field features with interaction order\n');
orig = MEFeaturesInteractionOrder(data, nvals, n);
me.Dim = orig.Dim;
me.nVals = orig.nVals;
me.nData = orig.nData;
%%
patternsOrder = sum(~isnan(orig.Patterns), 2);
patternMap = zeros(size(orig.Patterns, 1), 1);

offset = 0;
for o=1:n
    if o==1
        %%
        vec = nvals .* (0:me.Dim-1);
        p = orig.Patterns(patternsOrder == 1, :);
        %p(isnan(p)) = 0;
        add = repmat(vec, size(p, 1), 1);
        add(isnan(p)) = 0;
        p = p + add;
        patternMap(patternsOrder == 1) = p(~isnan(p));
        offset = offset + me.Dim * nvals;
    else
        vec = nvals .^ (o-1:-1:0);
        p = orig.Patterns(patternsOrder == o, :);
        p = reshape(p(~isnan(p))', sum(patternsOrder == o), o);
        patternMap(patternsOrder == o) = (p - 1) * vec' + offset + 1;
    end
    offset = offset + nvals^o;
end
me.nPatterns =  offset;
%%
me.Features = zeros(size(orig.InputPerms, 1), me.nPatterns);
me.Patterns = nan(me.nPatterns, orig.Dim);
me.PatternHist = zeros(1, me.nPatterns);
for i=1:length(patternMap)
    me.Features(:, patternMap(i)) = me.Features(:, patternMap(i)) + orig.Features(:, i) / sum(patternMap(i) == patternMap);
    me.Patterns(patternMap(i), :) = orig.Patterns(i, :);
    me.PatternHist(patternMap(i)) = me.PatternHist(patternMap(i)) + orig.PatternHist(i) / sum(patternMap(i) == patternMap);
end
me.PatternOrder = sum(~isnan(me.Patterns), 2);
%%
Redundent = all(isnan(me.Patterns), 2);
me.Patterns = me.Patterns(~Redundent, :);
me.Features = me.Features(:, ~Redundent);
me.PatternHist = me.PatternHist(~Redundent);
me.nPatterns = size(me.Patterns, 1);
