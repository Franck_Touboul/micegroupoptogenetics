function [jointProbs, pci, values] = JointProbability(data, range, fixed)
% computeJointProbs(data, max_val, fixed)
%   computed the Joint probability for a set data with discrete values
%   in the range [1:max_val].
%   If fixed is set to true then assume at least one observation for
%   each event.
if ~exist('range', 'var')
    range = max(data(:)) + 1;
end
if nargin < 3
    fixed = false;
end
baseVector = range.^[size(data,1)-1:-1:0];
h = baseVector * data + 1;
count = histc(h, 1:range^size(data,1));
if fixed
    count = count + 1;
end

if nargout > 1
    [jointProbs, pci] = binofit(count, sum(count));
    
    if nargout > 2
        values = myPerms(size(data, 1), range) - 1;
    end
else
    jointProbs = count / sum(count);
end