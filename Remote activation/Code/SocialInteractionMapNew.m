order = 3;
nEvents = 6;
%%
zoneCents(1, :) = [375, 385];
zoneCents(2, :) = [239, 60];
zoneCents(3, :) = [60, 405];
zoneCents(4, :) = [60, 177];
zoneCents(5, :) = [710, 513];
zoneCents(6, :) = [752, 468];
zoneCents(7, :) = [516, 290];
zoneCents(8, :) = [698, 96];
zoneCents(9, :) = [753, 157];
zoneCents(10, :) = [213, 290];
img = imread('arena.png');
issheltered = false(1, obj.ROI.nZones);

for i=1:obj.ROI.nZones
    name = regexprep(obj.ROI.ZoneNames{i}, '[()]', '');
    issheltered(i) = ~strcmp(name, obj.ROI.ZoneNames{i});
end
%%
cmap = MyMouseColormap;
me = obj.Analysis.Potts.Model{order};
[weights, idx] = sort(me.weights);
idx = idx(me.order(idx) == order);
offset = 0;
for s=[-1 1]
    for i=1:nEvents
        SquareSubplpot(2*nEvents, offset + i);
        imagesc(img); hold on; axis off;
        if s==-1
            j = idx(i);
        else
            j = idx(end-i+1);
        end
        l = me.labels{j};
        me.weights(j))
        l
        for k=1:order
            x = zoneCents(l(2, k), 1);
            y = zoneCents(l(2, k), 2);
            o = l(1, k);
%             if issheltered(l(2, k))
%                 x = x + mod(o-1, 2) * 5;
%                 y = y + floor((o-1)/2) * 5;
%             end
            plot(x, y, 'o', 'MarkerFaceColor', cmap(o, :), 'MarkerEdgeColor', 'none');
        end
        title(num2str(me.weights(j)));
        hoff
    end
    offset = offset + nEvents;
end

%%
if (1==1)
    %%
    p = 'Graphs/SocialInteractionMap/';
    mkdir(p);
    figure(1);
    saveFigure([p num2ordinal(order) 'Order.' obj.FilePrefix '']);
end