function [Ik, Entropy] = SocialFastMultiInformation(obj)
obj.Analysis.Potts.nIters = [5000 50000];
obj.Analysis.Potts.MinNumberOfIters = [0 0];
obj.Analysis.Potts.Confidence = 0.05;
obj.Output = false;
me = TrainPottsModel(obj, 1:obj.nSubjects-1);
obj.OutputToFile = false;
obj.OutputInOldFormat = false;
for i=1:obj.nSubjects - 1
    p = exp(me{i}.perms * me{i}.weights');
    Z = sum(p);
    p = p / Z;
    me{i}.prob = p;
end
[obj, independentProbs, jointProbs] = SocialComputePatternProbsFast(obj);

me{obj.nSubjects}.prob = jointProbs;
%%
Entropy = zeros(1, obj.nSubjects);
for i=1:obj.nSubjects
    p = me{i}.prob(:);
    log2p = log2(p);
    log2p(p == 0) = 0;
    Entropy(i) = -p' * log2p;
end
Ik = -diff(Entropy);
