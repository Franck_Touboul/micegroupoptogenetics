SocialExperimentData;
%%
res = [];
for i=1:GroupsData.nExperiments
    obj = TrackLoad({i 1}, {'Hierarchy', 'nSubjects'});
    nDS = SocialDavidScore(obj);
    res(i).nDS = nDS;
end
%%
for i=1:GroupsData.nExperiments
    curr = res(i).nDS;
    g = GroupsData.group(i);
    plot(sort(curr), 'color', GroupsData.Colors(g, :), 'Marker', 'o', 'MarkerFaceColor', GroupsData.Colors(g, :));
    hon;
end
xaxis(.7, obj.nSubjects + .3);
set(gca, 'XTick', 1:obj.nSubjects)
xlabel('Rank');
ylabel('Normalized David''s score');
hoff
