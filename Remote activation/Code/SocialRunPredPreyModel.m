function [obj, states] = SocialRunPredPreyModel(obj, features, m1, m2)
%%
model = SocialGeneratePredPreyModel(obj);
obj.Interactions.PredPrey.model = model;

%%
states = hmmviterbi(features{m1, m2},model.trans,model.emis);
states = model.id(states);
