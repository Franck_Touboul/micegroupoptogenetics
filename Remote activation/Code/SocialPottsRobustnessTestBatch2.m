SocialExperimentData
confidence_ = 0.05;
MinNumOfFrames_ = [12 25 50];
for i = 1:length(MinNumOfFrames_);
    MinNumOfFrames = MinNumOfFrames_(i);
    for confidence = confidence_
        index = 1;
        for id=GroupsData.Standard.idx
            if index > 2
                break;
            end
            prefix = sprintf(experiments{id}, 2);
            fprintf('# -> %s\n', prefix);
            try
                SocialFilteredPottsRobustnessTest(['Res/' prefix '.obj.mat'], 60, confidence, MinNumOfFrames);
            catch me
                fprintf('%s\n', me.message);
            end
            try
                SocialFilteredPottsRobustnessTest(['Res/' prefix '.obj.mat'], 1, confidence, MinNumOfFrames);
            catch me
                fprintf('%s\n', me.message);
            end
            index = index + 1;
        end
    end
end