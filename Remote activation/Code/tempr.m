x = double(dist12);
y = double(spd1);

x = [x, x];
y = [y, -y];
%%
bilin = struct();
bilin.s = [1 5 1];
bilin.b1 = [15 0 -15];
bilin.b0 = [-50 0 50];
%%
ux = min(x):max(x);
nchars = 0;
d = [];
for iter = 1:100
    %%
    nchars = Reprintf(nchars, '# iter no. %d/100', iter);
    for i=1:3
        cx = x;
        cy = y;
        dist = (bilin.b1(i) * cx - cy + bilin.b0(i)) / sqrt(bilin.b1(i)^2 + 1);
        d(i, :) = 1/(bilin.s(i) * sqrt(2*pi)) * exp(.5 * dist .^ 2 / bilin.s(i)^2);
    end
    [~, idx] = min(d);
    map1 = idx == 1;
    map2 = idx == 2;
    map3 = idx == 3;
    
    cmap = {'r', 'g', 'b'};
    for i=1:3
        plot(x(idx == i), y(idx == i), [cmap{i} '.']);
        hon
        plot(ux, bilin.b1(i)*ux + bilin.b0(i), 'k-');
    end
    yaxis(-100, 100);
    hoff
    drawnow
    %%
    %
    b = robustfit(x(map2), y(map2));
    bilin.b0(2) = b(1);
    bilin.b1(2) = b(2);
    cx = x(map2);
    cy = y(map2);
    dist = (b(2) * cx - cy + b(1)) / sqrt(b(2)^2 + 1);
    bilin.s(2) = mad(dist) * 1/norminv(3/4);
    %
    b = robustfit([x(map1), x(map3)], [y(map1), -y(map3)]);
    bilin.b0(1) = b(1);
    bilin.b1(1) = b(2);

    cx = [x(map1)  x(map3)];
    cy = [y(map1) -y(map3)];
    dist = (b(2) * cx - cy + b(1)) / sqrt(b(2)^2 + 1);
    bilin.s(1) = mad(dist) * 1/norminv(3/4);
    bilin.b0(3) = -bilin.b0(1);
    bilin.b1(3) = -bilin.b1(1);
    bilin.s(3) = bilin.s(1);
end
fprintf('\n');

%%
win = 10;
for i=-100:100
    
end