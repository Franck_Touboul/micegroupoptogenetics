SocialExperimentData;
nBins = 20;

experiments = {...
    'SC.exp0002.day%02d.cam01',...
    };
%%
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        try
            obj = TrackLoad(['../base/Res/' prefix '.obj.mat'], {'x', 'y', 'hidden', 'ROI'});
            obj.Colors.Centers = MyMouseColormap;
        catch me
            MyWarning(me)
            corrupt = {corrupt{:}, prefix};
            continue;
        end
        [~, c] = hist3([obj.x(:), obj.y(:)]);
        
        
        range = [min(obj.x(:)), max(obj.x(:)), min(obj.VideoHeight - obj.y(:)), max(obj.VideoHeight - obj.y(:))];
        h={};
        map = zeros(nBins);
        mapi = zeros(nBins);
        for i=1:obj.nSubjects;
            [h{i}, range] = HexHist([obj.x(i, ~obj.hidden(i, :))', obj.VideoHeight - obj.y(i, ~obj.hidden(i, :))'], nBins, 'range', range);
            c = map < h{i};
            mapi(c) = i;
            map(c) = h{i}(c);
        end
        %%
        HexPlot(mapi, range, 'Colormap', [.8 .8 .8; obj.Colors.Centers])
        %HexPlot(h{1}, range)
        %
        cmap = lines(16) * 0 + .4;
        
        for i=1:obj.ROI.nRegions
            roi = obj.ROI.Regions{i};
            boundries = (imdilate(roi ~= 0, ones(3,3)) - (roi ~= 0)) ~= 0;
            [x, y] = find(boundries);
            hold on;
            plot(y, obj.VideoHeight- x, '.', 'Color', cmap(i, :));
            hold off;
        end
        ppfile = [obj.OutputPath '/' prefix '.SocialCommonMap'];
        print(gcf, [ppfile '.png'], '-dpng');
        print(gcf, [ppfile '.eps'], '-dpsc2');
    end
end