function obj = SocialSoft(obj)
% Adds the following information to the obj:
%   obj.Analysis.Soft
%       + Zones
%           + Histogram: zone histogram - fraction of time spent in each zone
%           + CircadianMins: number of minutes used for 
%       + Social
%%
fprintf('# runnning ''Soft'' analysis\n');
obj = TrackLoad(obj);
%%
if obj.Output
    clf
end
output.nrows = 2;
%% location histogram
cmap = MyMouseColormap;
obj.Analysis.Soft.Zones.Histogram = zeros(length(obj.ROI.ZoneNames), obj.nSubjects);
for i=1:length(obj.ROI.ZoneNames)
    [id, sheltered] = getZoneId(obj, obj.ROI.ZoneNames{i});
    for s=1:obj.nSubjects
        obj.Analysis.Soft.Zones.Histogram(i, s) = sum(obj.zones(s, :) == id & (sheltered | ~obj.hidden(s, :)) & obj.valid);
    end
end
obj.Analysis.Soft.Zones.Histogram = obj.Analysis.Soft.Zones.Histogram ./ ...
    repmat(sum(obj.Analysis.Soft.Zones.Histogram), length(obj.ROI.ZoneNames), 1);

% plot:
if obj.Output
currRow = 1;
mySubplot(output.nrows, 1, currRow, 1);
barweb(obj.Analysis.Soft.Zones.Histogram * 100, obj.Analysis.Soft.Zones.Histogram * 0, [], obj.ROI.ZoneNames, ['location histogram: ' obj.FilePrefix], 'location', '% time', cmap);
set(gca, 'FontSize', 8);
end
%% percent of the time spent with at least one other subject
obj.Analysis.Soft.Social.FractionOfTime = zeros(1, obj.nSubjects);
for s=1:obj.nSubjects
    match = sum(repmat(obj.zones(s, :), obj.nSubjects, 1) == obj.zones, 1) > 1;
    obj.Analysis.Soft.Social.FractionOfTime(s) = mean(match);
end
% plot:
if obj.Output
currRow = 2;
mySubplot(output.nrows, 2, currRow, 1);
barweb(obj.Analysis.Soft.Social.FractionOfTime * 100, zeros(1, obj.nSubjects), [], [], [], 'subject', '% time', obj.Colors.Centers);
title('percentage of time spent with at least one other subject');
end
%%
if obj.OutputToFile
    fprintf('# - saving data\n');
    TrackSave(obj);
end

% if obj.OutputInOldFormat
%     filename = [obj.OutputPath obj.FilePrefix '.analysis.mat'];
%     fprintf(['# - saving to file: ''' filename '''\n']);
%     save(filename, 'obj');
% end
%%
return;
%% 
zones = obj.ROI.ZoneNames;

obj.Analysis.Soft.CircadianMins = 120; % [mins]
obj.CircadianNormalize = true;
%%
output.nrows = 4;
output.ncols = 2;
output.zones.y = [0 0.4];

%%
circadianFrames = min(obj.CircadianMins * 60 / obj.dt, obj.nFrames);
nCircEpochs = max(floor(obj.nFrames / circadianFrames), 1);

analysis.soft.circ.mins = obj.CircadianMins;
%%
aux.colormap = [];
for i=1:obj.nSubjects
    aux.colormap(i, :) = obj.Colors.Centers(i, :);
end

%% locations ..............................................................
currRow = 1;
mySubplot(output.nrows, 4, currRow, 1:3);
warning('ignoring hidden events');

zoneHist = zeros(length(zones), obj.nSubjects);
for i=1:length(obj.ROI.ZoneNames)
    [id, sheltered] = getZoneId(obj, zones{i});
    for s=1:obj.nSubjects
        zoneHist(i, s) = sum(obj.zones(s, :) == id & (sheltered | ~obj.hidden(s, :)));
        %zoneHist(i, s) = sum(social.zones.all(s, :) == id);
    end
end
for s=1:obj.nSubjects
    missHist(s) = sum(obj.hidden(s, :) & ~obj.sheltered(s, :)) / length(obj.hidden(s, :)); 
end


zoneHist = zoneHist ./ repmat(sum(zoneHist), length(zones), 1);
analysis.soft.zones.labels = zones;
analysis.soft.zones.hist = zoneHist;

obj.Analysis.Soft.Zones.ZoneNames = zones;
obj.Analysis.Soft.Zones.Histogram = zoneHist;
%
colormap(aux.colormap);
bar(zoneHist);
set(gca, 'XTickLabel', zones);
prettyPlot(['location histogram: ' obj.FilePrefix]);
freezeColors

%% togetherness
currRow = 1;
mySubplot(output.nrows, 4, currRow, 4);

together = false(obj.nSubjects, obj.nFrames);
unsheltered = false(obj.nSubjects, obj.nFrames);
for i=1:obj.nSubjects
    for j=1:obj.nSubjects
        if i==j; continue; end
        together(i, :) = together(i, :) | obj.zones(i, :) == obj.zones(j, :);
        unsheltered(i, :) = unsheltered(i, :) | (obj.zones(i, :) == obj.zones(j, :) & ~obj.sheltered(i, :));
    end
end
colormap(aux.colormap);
barweb([mean(together, 2)'; mean(unsheltered, 2)'], zeros(2, obj.nSubjects), [], {'everywhere', 'unsheltered'}, [], [], [], obj.Colors.Centers);
freezeColors
prettyPlot('% of time with other', 'subject');

%% togetherness circadian
currRow = 2;
circTogether = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:obj.nSubjects
        together = false(1, length(circEpoch));
        for j=1:obj.nSubjects
            if s==j; continue; end
            together = together | obj.zones(s, circEpoch) == obj.zones(j, circEpoch);
        end
        circTogether(c, s) = mean(together);
    end
end

mySubplot(output.nrows, output.ncols, currRow, 1);
for s=1:obj.nSubjects
    plot(.5:nCircEpochs, circTogether(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', 'k');
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * obj.CircadianMins);
prettyPlot('% time together', 'time [mins]');
freezeColors;

%% velocities .............................................................
currRow = 2;
%mySubplot(output.nrows, output.ncols, currRow, 1:2);
%rowTitle(output.nrows, output.ncols, currRow, 'Velocities')

obj.velocity = [zeros(obj.nSubjects, 1) sqrt(diff(obj.x, 1, 2).^2 + diff(obj.y, 1, 2).^2)];
%
allVelocities = [];
velocities = [];
for s=1:obj.nSubjects
    currVelocity = obj.velocity(s, :);
    valid = ~(obj.hidden(s, :) | [false obj.hidden(s, 1:end-1)]);
%    currVelocity = currVelocity(aux.farFromHidden(s, :));
    analysis.soft.stat.velocities.mean(s) = mean(currVelocity(valid));
    analysis.soft.stat.velocities.std(s) = stderr(currVelocity(valid));
%    allVelocities(s) = mean(currVelocity);
end

%%
% mySubplot(output.nrows, output.ncols, currRow, 1);
% barweb(analysis.soft.stat.velocities.mean, analysis.soft.stat.velocities.std, [], [], [], [], [], obj.Colors.Centers);


% colormap(jet);
% bar([velocities; allVelocities]','grouped');
% prettyPlot('', 'subject');
% legend('non-zero', 'all');
% legend boxoff
% freezeColors;
%%
circVelocities = [];
%circAllVelocities = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:obj.nSubjects
        currVelocity = obj.velocity(s, circEpoch);
        valid = ~(obj.hidden(s, circEpoch) | [false obj.hidden(s, circEpoch(1):circEpoch(end) - 1)]);
        %currHidden = aux.hidden(s, circEpoch);
        analysis.soft.circ.velocities(c, s) = mean(currVelocity(valid));
        %circAllVelocities(c, s) = mean(currVelocity);
    end
end

mySubplot(output.nrows, output.ncols, currRow, 2);
for s=1:obj.nSubjects
    plot(.5:nCircEpochs, analysis.soft.circ.velocities(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', 'k');
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * obj.CircadianMins);
prettyPlot('velocities', 'time [mins]');
freezeColors;

% mySubplot(output.nrows, output.ncols, currRow, 2);
% for s=1:obj.nSubjects
%     plot(.5:nCircEpochs, circAllVelocities(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', aux.colormap(s, :));
%     hold on;
% end
% hold off;
% for c=0:nCircEpochs
%     vert_line(c, 'color', 'k', 'linestyle', '--');
% end
% set(gca, 'XTickLabel', [0:nCircEpochs] * options.circadianMins);
% prettyPlot('circadian (all)', 'time [mins]');
% freezeColors;

%% food\water ...........................................................
currRow = 3;
mySubplot(output.nrows, output.ncols, currRow, 1);
rowTitle(output.nrows, output.ncols, currRow, 'Zones')

circFoodWater = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:obj.nSubjects
        curr = obj.zones(s, circEpoch);
        circFoodWater(c, s) = sum(...
            curr == getZoneId(obj, 'Feeder1') | ...
            curr == getZoneId(obj, 'Feeder2') | ...
            curr == getZoneId(obj, 'Water'));
    end
end
if obj.CircadianNormalize
    circFoodWater = circFoodWater ./ repmat(sum(circFoodWater, 1), nCircEpochs, 1);
end

analysis.soft.circ.foodwater = circFoodWater;

for s=1:obj.nSubjects
    plot(.5:nCircEpochs, circFoodWater(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', 'k');
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * obj.CircadianMins);
prettyPlot('food\\water (circadian)', 'time [mins]');
axis([0 nCircEpochs output.zones.y(1) output.zones.y(2)]);
freezeColors;

%% hidden ...............................................................
currRow = 3;
mySubplot(output.nrows, output.ncols, currRow, 2);

circHidden = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:obj.nSubjects
        curr = obj.zones(s, circEpoch);
        circHidden(c, s) = sum(...
            curr == getZoneId(obj, '(SmallNest)') | ...
            curr == getZoneId(obj, '(BigNest)'));
    end
end
if obj.CircadianNormalize
    circHidden = circHidden ./ repmat(sum(circHidden, 1), nCircEpochs, 1);
end

analysis.soft.circ.hidden = circHidden;

for s=1:obj.nSubjects
    plot(.5:nCircEpochs, circHidden(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', 'k');
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * obj.CircadianMins);
prettyPlot('hidden place (circadian)', 'time [mins]');
axis([0 nCircEpochs output.zones.y(1) output.zones.y(2)]);
freezeColors;

%% labyrinth ..............................................................
currRow = 4;
mySubplot(output.nrows, output.ncols, currRow, 1);

circHidden = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:obj.nSubjects
        curr = obj.zones(s, circEpoch);
        circHidden(c, s) = sum(curr == getZoneId(obj, 'Labyrinth') | curr == getZoneId(obj, 'Block'));
    end
end
if obj.CircadianNormalize
    circHidden = circHidden ./ repmat(sum(circHidden, 1), nCircEpochs, 1);
end
analysis.soft.circ.labyrinth_bignest = circHidden;

for s=1:obj.nSubjects
    plot(.5:nCircEpochs, circHidden(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', 'k');
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * obj.CircadianMins);
prettyPlot('labyrinth\\Block (circadian)', 'time [mins]');
axis([0 nCircEpochs output.zones.y(1) output.zones.y(2)]);
freezeColors;

%% openspace ..............................................................
currRow = 4;
mySubplot(output.nrows, output.ncols, currRow, 2);

circHidden = [];
for c=1:nCircEpochs
    circEpoch = (c - 1) * circadianFrames + 1 : c * circadianFrames;
    for s=1:obj.nSubjects
        curr = obj.zones(s, circEpoch);
        circHidden(c, s) = sum(curr == getZoneId(obj, 'Open'));
    end
end
if obj.CircadianNormalize
    circHidden = circHidden ./ repmat(sum(circHidden, 1), nCircEpochs, 1);
end
analysis.soft.circ.open = circHidden;

for s=1:obj.nSubjects
    plot(.5:nCircEpochs, circHidden(:, s), 'o:', 'MarkerFaceColor', aux.colormap(s, :), 'Color', aux.colormap(s, :), 'MarkerEdgeColor', 'k');
    hold on;
end
hold off;
for c=0:nCircEpochs
    vert_line(c, 'color', 'k', 'linestyle', '--');
end
set(gca, 'XTickLabel', [0:nCircEpochs] * obj.CircadianMins);
axis([0 nCircEpochs output.zones.y(1) output.zones.y(2)]);
prettyPlot('open-space (circadian)', 'time [mins]');
freezeColors;

%%

% filename = [options.output_path options.test.name '.soft'];
% fprintf(['# - saving figure to file: ''' filename '''\n']);
% saveFigure(filename);
% 
% filename = [options.output_path options.test.name '.analysis.mat'];
% fprintf(['# - saving to file: ''' filename '''\n']);
% save(filename, 'analysis');