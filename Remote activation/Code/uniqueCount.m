function [u, count] = uniqueCount(x)
u = unique(x);
count = u * 0;
for i=1:length(u)
    count(i) = sum(x == u(i));
end