%Opt.nAngels = 10;
Opt = struct();
Opt.Resolution = 100;

%Opt.Angels = linspace(0, 2*pi, Opt.nAngels + 1); 
%Opt.Angels = Opt.Angels(1:end-1);

%%
fields = {...
    'Angle',0.0,...
    'MajorAxis',1,...
    'DX',0,...
    'DY',0,...
    'Height', 0.5,...
    'BodyYaw', 0.0 ...
    'BodyFat', 1, ...
    'RearFat', 1 ...
    };
Param = struct(fields{:});
%
clf
ModelParam.Body.Yaw = 0.03;
ModelParam.Body.FatScale = Param.BodyFat;
ModelParam.Body.RearScale = Param.RearFat;
Model = ModelCreateF(ModelParam, Opt);
Model.SkinMemory = .9;

[x,y,z] = ModelToDepth(Model, Opt.Resolution);
imagesc(z);

return
%%
dt = DelaunayTri([vX, vY], edges);
inside = inOutStatus(dt);
triplot(dt(inside, :), dt.X(:,1), dt.X(:,2))

F = TriScatteredInterp(dt(inside, :), [Z(valid); 0]);

imagesc(F(x, y));

%%
t = TriRep(vtri, [vX, vY]);
fe = edges(t)';
dt = DelaunayTri([vX, vY]);
io = dt.inOutStatus();
triplot(dt)
patch('faces',dt(io,:), 'vertices', dt.X, 'FaceColor','r');

%%
clf
subplot(3,2,1:4);
ModelShow(Model, Points)

vX = X(valid);
vY = Y(valid);
vZ = Z(valid);

mnx = min(vX(:));
mny = min(vY(:));
mxx = max(vX(:));
mxy = max(vY(:));


[x, y] = meshgrid(mnx:1/Opt.Resolution:mxx, mny:1/Opt.Resolution:mxy);
i = knnsearch([vX, vY], [x(:), y(:)], 'K', 1);
z = reshape(vZ(i), size(x));


%%
fields = {...
    'Angle',0.0,...
    'MajorAxis',1,...
    'DX',0,...
    'DY',0,...
    'Height', 0.5,...
    'BodyYaw', 0.05 ...
    'BodyFat', 1, ...
    'RearFat', 1 ...
    };
Param = struct(fields{:});
%
pos = ModelInSpace(Param);
[mnx, mxx, mx] = Range(pos(:, 1));
[mny, mxy, my] = Range(pos(:, 2));
res = max(mxx - mnx, mxy - mny) / (Opt.Resolution - 1);
[x, y] = meshgrid(...
    mx - res * Opt.Resolution / 2 : res : mx + res * Opt.Resolution / 2,...
    my - res * Opt.Resolution / 2 : res : my + res * Opt.Resolution / 2);
[a, b, c] = griddata(pos(:, 1), pos(:, 2), pos(:, 3), x, y);
c(isnan(c)) = 0;
% plot
subplot(2,2,1);
plot3(pos(:, 1), pos(:, 2), pos(:, 3), '.');
axis equal
grid on;

subplot(2,2,2);
imagesc(c);

subplot(2,2,3);
surfl(a, b, c);
shading interp; 
colormap(gray); axis equal
grid on;
