function frame = CaptureFrame()
%%
name = [hash(randi(1000000), 'MD5')];
print(gcf, [name '.eps'], '-dpsc2', '-r300'); 
system(['setenv TMPDIR "/tmp"; pstopnm -stdout -dpi=300 -portrait ' name '.eps | pnmtopng > ' name '.png']);

[cdata, colormap] = imread([name '.png']); 
frame.cdata = ind2rgb(cdata, colormap);
try
    %system(['rm ' name '.eps']);
    %system(['rm ' name '.png']);
end