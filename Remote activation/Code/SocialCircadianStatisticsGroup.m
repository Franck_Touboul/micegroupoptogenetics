% save('Res/SocialCircadianStatisticsGroup');
%%% save('TempSocialCircadianStatisticsGroup', 'DJS', 'jointProbs')

% experiments = {...
%     'Enriched.exp0006.day%02d.cam01',...
%     'Enriched.exp0005.day%02d.cam04',...
%     'Enriched.exp0001.day%02d.cam04',...
%     'SC.exp0001.day%02d.cam01',...
%     'Enriched.exp0002.day%02d.cam04',...
%     'SC.exp0002.day%02d.cam01',...
%     'Enriched.exp0003.day%02d.cam01',...
%     'SC.exp0003.day%02d.cam01',...
%     'Enriched.exp0004.day%02d.cam01',...
%     'SC.exp0004.day%02d.cam04',...
%     'SC.exp0005.day%02d.cam04',...
%     'SC.exp0006.day%02d.cam01',...
%     'SC.exp0007.day%02d.cam04',...
%     };
% 
% nDays = 4;
% 
% E.map = logical([1 1 1 0 1 0 1 0 1 0 0 0 0]);
% E.idx = find(E.map);
% 
% SC.map = ~E.map;
% SC.idx = find(SC.map);
% 
% colors = [80 171 210; 232 24 46] / 255;
% titles = {'Enriched', 'Control'};

% objs = {};
% for id=1:length(experiments)
%     for day = 1:nDays
%         prefix = sprintf(experiments{id}, day);
%         objs{id, day} = trackload([obj.OutputPath prefix '.obj.mat'], {'common'});
%     end
% end
SocialExperimentData

%%
% jointProbs = {};
% DJS = {};
% for id=1:length(experiments)
%     for day = 1:nDays
%         prefix = sprintf(experiments{id}, day);
%         fprintf('# -> %s\n', prefix);
%         obj = TrackLoad(['Res/' prefix '.obj.mat'], {'common'});
%         [obj, DJS{id, day}, jointProbs{id, day}] = SocialCircadianStatistics(obj);
%     end
% end
mn = 12;
jointProbs = {};
indepProbs = {};
DJS = {};
iDJS = {};
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('# -> %s\n', prefix);
        obj = TrackLoad(['Res/' prefix '.obj.mat'], {'common', 'Circadian'});
        DJS{id, day} = obj.Circadian.Djs;
        iDJS{id, day} = obj.Circadian.IndependentDjs;
%         if size(DJS{id, day}, 1) ~= mn
%             obj = SocialCircadianStatistics(obj);
%         end
        jointProbs{id, day} = obj.Circadian.JointProbs;
        indepProbs{id, day} = obj.Circadian.IndepProbs;
        %[obj, DJS{id, day}, jointProbs{id, day}] = SocialCircadianStatistics(obj);
    end
end
Group = Groups(2);
prev.DJS = DJS;
prev.jointProbs = jointProbs;
if 1==2;
    %%
    DJS = iDJS;
    jointProbs = indepProbs;
    %%
    jointProbs = prev.jointProbs;
    DJS = prev.DJS;
end

%% within days
sumDJS = zeros(mn);
countDJS = zeros(mn);

sumDJSbyDay = {};
countDJSbyDay = {};
for day = 1:nDays
    sumDJSbyDay{day} = zeros(mn);
    countDJSbyDay{day} = zeros(mn);
end
for id=1:length(experiments)
    if ~any(id == Group.idx)
        continue;
    end
    for day = 1:nDays
        mmn = min(size(DJS{id, day}, 1), mn);
        curr = nan(mn, mn);
        curr(1:mmn, 1:mmn) = DJS{id, day}(1:mmn, 1:mmn);
        countDJS = countDJS + ~isnan(curr);
        countDJSbyDay{day} = countDJSbyDay{day} + ~isnan(curr);
        curr(isnan(curr)) = 0;
        sumDJS = sumDJS + curr;
        sumDJSbyDay{day} = sumDJSbyDay{day} + curr;
    end
end
sumDJS(1:mn+1:end) = nan;
sumDJS = sumDJS ./ countDJS;
scale = [inf, -inf];

values = sumDJS(:);
for day = 1:nDays
    sumDJSbyDay{day}(1:mn+1:end) = nan;
    sumDJSbyDay{day} = sumDJSbyDay{day} ./ countDJSbyDay{day};
    values = [values; sumDJSbyDay{day}(:)];
end
values = values(~isnan(values));
scale(1) = mean(values) - 2 * std(values);
scale(2) = mean(values) + 2 * std(values);

subplot(4,2,1:4)
MyImagesc(sumDJS, true, scale);
title('all days');
for day = 1:nDays
    subplot(4,2,4+day);
    MyImagesc(sumDJSbyDay{day}, false, scale);    
    title(['day ' num2str(day)]);
end

%% between following days
clf
dDJS =  zeros(mn, mn);
countDJS = zeros(mn, mn);

dDJSbyDay =  {};
countDJSbyDay = {};
for day = 1:nDays-1
    dDJSbyDay{day} = zeros(mn);
    countDJSbyDay{day} = zeros(mn);
end

for id=1:length(experiments)
    if ~any(id == Group.idx)
        continue;
    end
    %fprintf('%d\n',     id);
    for day = 1:nDays-1
        for f1=1:min(mn, length(jointProbs{id, day}))
            if all(isnan(jointProbs{id, day}{f1}))
                continue;
            end
            for f2=1:min(mn, length(jointProbs{id, day+1}))
                if all(isnan(jointProbs{id, day+1}{f2}))
                    continue;
                end
                curr = JensenShannonDivergence(jointProbs{id, day}{f1}(:)', jointProbs{id, day+1}{f2}(:)');
                dDJS(f1, f2) = dDJS(f1, f2) + curr;
                countDJS(f1, f2) = countDJS(f1, f2) + 1;
                
                dDJSbyDay{day}(f1, f2) = dDJSbyDay{day}(f1, f2) + curr;
                countDJSbyDay{day}(f1, f2) = countDJSbyDay{day}(f1, f2) + 1;
            end
        end
    end
end

b = 4;
dDJS = dDJS ./ countDJS;
scale = [inf, -inf];
values = dDJS(:);
for day = 1:nDays-1
    dDJSbyDay{day} = dDJSbyDay{day} ./ countDJSbyDay{day};
    values = [values; dDJSbyDay{day}(:)];
end
o = sort(values);
o = o(~isnan(o));
scale(1) = mean(o) - 2 * std(o);
scale(2) = mean(o) + 2 * std(o);

subplot(4,2,1:4)
MyImagesc(dDJS, true, scale);
axis square

title('all days');
for day = 1:nDays-1
    subplot(4,2,4+day);
    MyImagesc(dDJSbyDay{day}, false, scale);    
axis square
    title(['day ' num2str(day) '-> day ' num2str(day+1)]);
end

%% between all days
clf
dDJS =  zeros(mn, mn);
countDJS = zeros(mn, mn);

dDJSbyDay =  {};
countDJSbyDay = {};
for day1 = 1:nDays-1
for day2 = day1+1:nDays
    dDJSbyDay{day1, day2} = zeros(mn);
    countDJSbyDay{day1, day2} = zeros(mn);
end
end

for id=1:length(experiments)
    if ~any(id == Group.idx)
        continue;
    end
    %fprintf('%d\n',     id);
    for day1 = 1:nDays-1
        for day2 = day1+1:nDays
            for f1=1:min(mn, length(jointProbs{id, day1}))
                if all(isnan(jointProbs{id, day1}{f1}))
                    continue;
                end
                for f2=1:min(mn, length(jointProbs{id, day1}))
                    if all(isnan(jointProbs{id, day2}{f2}))
                        continue;
                    end
                    curr = JensenShannonDivergence(jointProbs{id, day1}{f1}(:)', jointProbs{id, day2}{f2}(:)');
                    dDJS(f1, f2) = dDJS(f1, f2) + curr;
                    countDJS(f1, f2) = countDJS(f1, f2) + 1;
                    
                    dDJSbyDay{day1, day2}(f1, f2) = dDJSbyDay{day1, day2}(f1, f2) + curr;
                    countDJSbyDay{day1, day2}(f1, f2) = countDJSbyDay{day1, day2}(f1, f2) + 1;
                    dDJSbyDay{day2, day1} = dDJSbyDay{day1, day2};
                    countDJSbyDay{day2, day1} = countDJSbyDay{day1, day2};
                end
            end
        end
    end
end

prevdDJS = dDJS;
prevdDJSbyDay = dDJSbyDay;

%
dDJSbyDay = prevdDJSbyDay;
dDJS = prevdDJS;
smooth = 0;

dDJS = dDJS ./ countDJS;
scale = [inf, -inf];
values = [];
for day1 = 1:nDays-1
    for day2 = day1+1:nDays
        dDJSbyDay{day1, day2} = dDJSbyDay{day1, day2} ./ countDJSbyDay{day1, day2};
        dDJSbyDay{day2, day1} =  dDJSbyDay{day1, day2};
        values = [values; dDJSbyDay{day1, day2}(:)];
    end
end
o = sort(values);
o = o(~isnan(o));
scale(1) = mean(o) - 2 * std(o);
scale(2) = mean(o) + 2 * std(o);


subplot(nDays+1,nDays-1, 1:2*(nDays-1))
if smooth > 0
    c = convn(dDJS, ones(smooth), 'valid');
    MyImagesc(c, true);
axis square
else
    MyImagesc(dDJS, true, scale);
axis square
end
title('all days');
for day1 = 1:nDays-1
    for day2 = day1+1:nDays
    mySubplot(nDays+1,nDays-1, day1+2, day2-1)
if smooth > 0
    c = convn(dDJSbyDay{day1, day2}, ones(smooth), 'valid');
    MyImagesc(c, true);
axis square
else
    MyImagesc(dDJSbyDay{day1, day2}, false, scale);    
axis square
end

    title(['day ' num2str(day1) '-> day ' num2str(day2)]);
    end
end
%%
H = nan(1, 2*mn-1);
C = zeros(1, 2*mn-1);
stdH = zeros(1, size(H, 2));
meanH = zeros(1, size(H, 2));
for day1 = 1:nDays-1
    for day2 = day1+1
        h = zeros(1, 2*mn-1);
        c = zeros(1, 2*mn-1);
        index = 1;
        for f1=1:mn
            for f2=1:mn
                if ~isnan(dDJSbyDay{day1, day2}(f1, f2))
                    i = f1 - f2 + mn;
                    h(i) = h(i) + dDJSbyDay{day1, day2}(f1, f2);
                    c(i) = c(i) + 1;
                    H(index, i) = dDJSbyDay{day1, day2}(f1, f2);
                end
            end
            index = index + 1;
            H(index, :) = nan;
        end
        mySubplot(nDays+1,nDays-1, day1+2, day2-1)
        plot(-mn+1:mn-1, h./c)
    end
end
for i=1:size(H, 2)
    h = H(:, i);
    stdH(i) = stderr(h(~isnan(h)));
    meanH(i) = mean(h(~isnan(h)));
end
errorbar(-mn+1:mn-1, meanH, stdH);
return;
%% day corr
H = {};
for day1 = 1:nDays-1
    for day2 = day1+1:nDays
        H{day1, day2} = [];
    end
end

for id=1:length(experiments)
    if ~any(id == Group.idx)
        continue;
    end
    %fprintf('%d\n',     id);
    for day1 = 1:nDays-1
        for day2 = day1+1:nDays
            h = zeros(1, 2*mn-1);
            c = zeros(1, 2*mn-1);

            for f1=1:min(mn, length(jointProbs{id, day1}))
                if all(isnan(jointProbs{id, day1}{f1}))
                    continue;
                end
                for f2=1:min(mn, length(jointProbs{id, day2}))
                    if all(isnan(jointProbs{id, day2}{f2}))
                        continue;
                    end
                    curr = JensenShannonDivergence(jointProbs{id, day1}{f1}(:)', jointProbs{id, day2}{f2}(:)');
                    i = f1 - f2 + mn;
                    h(i) = h(i) + curr;
                    c(i) = c(i) + 1;
                end
            end
            H{day1, day2} = [H{day1, day2}; h./c];
        end
    end
end

%%
counth = zeros(size(H{1, 2}));
fullh = zeros(size(H{1, 2}));
% counth = [];

% fullh = [];
cmap = MyCategoricalColormap;
subplot(2,1,1); hoff;
entries = {};
    style = {'LineWidth', 2};
for day1 = 1:nDays-1
    entries{day1} = ['Day ' num2str(day1)];
    for day2 = day1+1:nDays
        if day2 == day1+1
            h = H{day1, day2};
            h(isnan(h)) = 0;
%             counth = [counth; ~isnan(H{day1, day2})];
%             fullh = [fullh; h];
            counth = counth + ~isnan(H{day1, day2});
            fullh = fullh + h;
        end
        stdH = zeros(1, size(H{day1, day2}, 2));
        meanH = zeros(1, size(H{day1, day2}, 2));
        for i=1:size(H{day1, day2}, 2)
            h = H{day1, day2}(:, i);
            stdH(i) = stderr(h(~isnan(h)));
            meanH(i) = mean(h(~isnan(h)));
        end
        %mySubplot(nDays+1,nDays-1, day1+2, day2-1)
        subplot(2,1,1)
%        errorbar(-mn+1:mn-1, meanH, stdH); hon;
%         if day2==day1+1        
%             plot(-mn+1:mn-1, meanH, style{:}, 'Color', cmap(day1, :)); hon;
%         else
%             plot(-mn+1:mn-1, meanH, style{:}, 'Color', cmap(day1, :),'HandleVisibility','off'); hon;
%             plot(-mn+1:mn-1, meanH, ':', style{:}, 'Color', cmap(day2, :),'HandleVisibility','off'); hon;
%         end
        if day2==day1+1        
            plot((day2-1) * 2 * mn + [-mn+1:mn-1], meanH, style{:}, 'Color', cmap(day1, :)); hon;
        else
            plot((day2-1) * 2 * mn + [-mn+1:mn-1], meanH, style{:}, 'Color', cmap(day1, :),'HandleVisibility','off'); hon;
            %plot(-mn+1:mn-1, meanH, style{:}, 'Color', cmap(day1, :),'HandleVisibility','off'); hon;
            %plot(-mn+1:mn-1, meanH, ':', style{:}, 'Color', cmap(day2, :),'HandleVisibility','off'); hon;
        end
        
        %axis([-mn mn .55 1]);
    end
end

fullh = fullh ./ counth;
stdFH = zeros(1, size(H{day1, day2}, 2));
meanFH = zeros(1, size(H{day1, day2}, 2));
for i=1:size(H{day1, day2}, 2)
    h = fullh(:, i);
    stdFH(i) = stderr(h(~isnan(h)));
    meanFH(i) = mean(h(~isnan(h)));
end

%subplot(nDays+1,nDays-1, 1:6)
subplot(2,1,1)
errorbar(-mn+1:mn-1, meanFH, stdFH, 'k:', style{:});
a = axis;
axis([-mn mn a(3) a(4)]);

legend({entries{:}, 'average'});
legend boxoff;

return

%%
mn = 5;
mDJS = zeros(mn, mn, nDays);
count = zeros(1, nDays);
for day = 1:nDays
    if ~any(id == SC.idx)
        continue;
    end
    for id=1:length(experiments)
        if length(DJS{id, day}) >= mn
            mDJS(:, :, day) = mDJS(:, :, day) + DJS{id, day}(1:mn, 1:mn);
            count(day) = count(day) + 1;
        end
    end
    subplot(2,2,day);
    c = mDJS(:, :, day) / count(day);
    c(1:size(c,2)+1:end) = nan;
    MyImagesc(c);
    set(gca, 'XTick', 1:mn)
    set(gca, 'YTick', 1:mn)
    title(['Day ' num2str(day)]);
end
%%
mn = 5;
dDJS =  zeros(mn, mn);
count = 0;
for id=1:length(experiments)
    if ~any(id == SC.idx)
        continue;
    end
    fprintf('%d ',     id);
    for day = 1:nDays-1
        if length(jointProbs{id, day}) >= mn && length(jointProbs{id, day+1}) >= mn 
            for f1=1:mn
                for f2=1:mn
                    djs(f1, f2) = JensenShannonDivergence(jointProbs{id, day}{f1}, jointProbs{id, day+1}{f2});
                end
            end
            djs
            dDJS = dDJS + djs;
            count = count + 1;
        end
    end
end
fprintf('\n');

c = dDJS / count;
%c(1:size(c,2)+1:end) = nan;
MyImagesc(c, true);
set(gca, 'XTick', 1:mn)
set(gca, 'YTick', 1:mn)
ylabel('next day');
xlabel('previous day');
return;
%%
mn = 5;
dDJS =  zeros(mn, mn, nDays);
count = zeros(1, nDays);
for id=1:length(experiments)
    if ~any(id == SC.idx)
        continue;
    end
    fprintf('%d ',     id);
    for day = 1:nDays-1
        if length(jointProbs{id, day}) >= mn && length(jointProbs{id, day+1}) >= mn 
            for f1=1:mn
                for f2=1:mn
                    djs(f1, f2) = JensenShannonDivergence(jointProbs{id, day}{f1}, jointProbs{id, day+1}{f2});
                end
            end
            dDJS(:, :, day) = dDJS(:, :, day) + djs;
            count(day) = count(day) + 1;
        end
    end
end

for day = 1:nDays-1
    subplot(2,2,day);
    c = dDJS(:, :, day) / count(day);
    %c(1:size(c,2)+1:end) = nan;
    MyImagesc(c);
    set(gca, 'XTick', 1:mn)
    set(gca, 'YTick', 1:mn)
end
fprintf('\n');
%%


return
%%
for day = 1:nDays
    subplot(2,2,day);
    imagesc(DJS{id, day});
end

%%
d1 = 3;
d2 = 4;
id = 1;
D = [];
for i1=1:length(jointProbs{id, d1})
    for i2=1:length(jointProbs{id, d2})
        D(i1, i2) = JensenShannonDivergence(jointProbs{id, d1}{i1}, jointProbs{id, d2}{i2});
    end
end
imagesc(D)        