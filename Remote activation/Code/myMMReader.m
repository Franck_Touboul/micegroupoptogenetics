function img = myMMReader(filename, frame, bkg)

if isstruct(filename)
    filename = filename.VideoFile;
end

persistent VideoFilename;
persistent VideoObj;

UseBkg = exist('bkg', 'var') && ~isempty(bkg);

if ~strcmp(VideoFilename, filename)
    VideoFilename = filename;
    VideoObj = VideoReader(filename);
end

if nargin > 1
    img = read(VideoObj, frame);
    if isempty(img) && UseBkg
        img = bkg;
    end
else
    img = VideoObj;
end
