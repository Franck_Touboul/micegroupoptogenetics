function MyArrow(from, to)
v = dsxy2figxy([from(:)', to(:)' - from(:)']);
x = [v(1) v(1) + v(3)];
y = [v(2) v(2) + v(4)];
y = min(y, [1 1]);
x = min(x, [1 1]);
y = max(y, [0 0]);
x = max(x, [0 0]);
annotation('arrow', x, y, 'color', 'g');