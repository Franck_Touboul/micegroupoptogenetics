function res = followTrack_v4(options, cents, curr)

[N, nS] = size(cents.label);
dim = sum(cents.label ~= 0, 2);

%%
options.jumpVar = 3^2;
options.confProb = 0.1;
options.hideProb = options.confProb;
options.hideJump = options.jumpVar;
covarMat = eye(2) * options.jumpVar;

table = zeros(N, nS+2);
table(cents.label == curr) = 1;
table(cents.label ~= curr) = options.confProb;
table(cents.label == 0)    = nan;
table(:, nS+1) = options.hideProb;
table(:, nS+2) = options.hideProb;
table = flog(table');

path = zeros(nS+2, N, 'uint8');
hiddenPos = zeros(N, 2);
hiddenPos(1, :) = [inf, inf];
pHide = gauss([0, 0], covarMat, [options.hideJump, 0]);
for i=2:N
    col = table(:, i-1);
    vec = [cents.x(i-1, :) hiddenPos(i-1, 1); cents.y(i-1, :) hiddenPos(i-1, 2)];
    for j=1:nS
        p = gauss([cents.x(i, j), cents.y(i, j)], covarMat, vec');
        p(nS + 2) = gauss([0, 0], covarMat, [options.hideJump, 0]);
        [val, from] = max(flog(p) + col);
        path(j, i) = from;
        table(j ,i) = val + table(j ,i);
    end
    % misdetected
    [val, from] = max(flog(pHide) + col);
    path(nS + 1, i) = from;
    if from <= nS
        hiddenPos(i, :) = [cents.x(i-1, from), cents.y(i-1, from)];
    else
        hiddenPos(i, :) = hiddenPos(i-1, :);
    end
    table(nS + 1, i) = val + table(nS + 1 ,i);
    % hidden
    [val, from] = max(flog(pHide) + col);
    path(nS + 2, i) = from;
    table(nS + 2, i) = val + table(nS + 2 ,i);
end

% backtrack 
backtrack = zeros(1, N);
[val, start] = max(table(:, end));
backtrack(end) = start;
res.x = zeros(1, N);
res.y = zeros(1, N);
res.prob = zeros(1, N);
res.id = zeros(1, N, 'int8');

if backtrack(end) <= nS
    res.x(end) = cents.x(end, backtrack(end));
    res.y(end) = cents.y(end, backtrack(end));
    res.id(end) = backtrack(end);
elseif backtrack(end) == nS + 1
    res.x(end) = hiddenPos(end, 1);
    res.y(end) = hiddenPos(end, 2);
    res.id(end) = -1;
else
    res.x(end) = nan;
    res.y(end) = nan;
    res.id(end) = 0;
end
res.prob(end) = table(end, backtrack(end));
for i=N-1:-1:1
    backtrack(i) = path(backtrack(i+1), i + 1);
    if backtrack(i) <= nS
        res.x(i) = cents.x(i, backtrack(i));
        res.y(i) = cents.y(i, backtrack(i));
        res.id(i) = backtrack(i);
    elseif backtrack(i) == nS + 1
        res.x(i) = hiddenPos(i, 1);
        res.y(i) = hiddenPos(i, 2);
        res.id(i) = -1;
    else
        res.x(i) = nan;
        res.y(i) = nan;
        res.id(i) = 0;
    end
    res.prob(i) = table(backtrack(i), i);
end

