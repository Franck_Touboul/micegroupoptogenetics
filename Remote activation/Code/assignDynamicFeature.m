function [features, labels] = assignDynamicFeature(data, range, nPerms, use_sparse, full)
if nargin < 4
    use_sparse = false;
end

dim = 0;
for i=1:length(nPerms)
    if length(nPerms{i}) == 1
        nVals = range^2;
    else
        nVals = range^length(nPerms{i});
    end
    dim = dim + nVals;
end

if use_sparse
    features = sparse(size(data, 1), double(dim));
else
    features = zeros(size(data, 1), double(dim));
end

index = 1;
labels = cell(1, dim);
for i=1:length(nPerms)
    if length(nPerms{i}) == 1
        nVals = range^2;
        for j=0:nVals-1
            values = decimal2base(j, range, 2) + 1;
            mapPresent = [true; data(2:end, nPerms{i}) == values(1)];
            map = mapPresent & [data(1:end-1, nPerms{i}) == values(2); false];
            if full
                features(:, index) = map;
            else
                features(:, index) = mapPresent;
            end
            
            labels{index} = [nPerms{i} nPerms{i}; values];
            index = index + 1;
        end
    else
        nVals = range^length(nPerms{i});
        for j=0:nVals-1
            values = decimal2base(j, range, length(nPerms{i})) + 1;
            map = true(size(data, 1), 1);
            for k=1:length(nPerms{i})
                map = map & data(:, nPerms{i}(k)) == values(k);
            end
            features(:, index) = map;
            
            labels{index} = [nPerms{i}; values];
            index = index + 1;
        end
    end
end


