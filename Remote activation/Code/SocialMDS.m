clf
cmap = MyCategoricalColormap;
cmap = [cmap(1,:); cmap(3,:)];
SocialExperimentData
D{1} = [];
D{2} = [];
for id=1:GroupsData.nExperiments
    %%
    obj = TrackLoad({id 1}, {'Hierarchy'});
    d = (obj.Hierarchy.Group.ChaseEscape + obj.Hierarchy.Group.ChaseEscape') ./ obj.Hierarchy.Group.Contacts;
    D{GroupsData.group(id)} = [D{GroupsData.group(id)}, d(~(eye(obj.nSubjects) == 1))];
end
hoff;