function [I, H] = ComputeExactMutualInformation(X, Y)
%%
output = false;
nfractions = 20;
maxIters = 128;
stat = struct();
idx = 1;
count = 0;
for fraction=1:nfractions
    niters = 2^(fraction - 1);
    niters = min(niters, maxIters);
    for iter=1:niters
        count = RePrintf(count, '# fraction %d/%d, iter %d/%d', fraction, nfractions, iter, niters);
        if fraction > 1
            map = randperm(size(X, 2));
            map = map(1:floor(size(X, 2)/fraction));
            x = X(:, map);
            y = Y(:, map);
        else
            x = X;
            y = Y;
        end
        [I, H] = ComputeMutualInformation(x, y);
        stat.fraction(idx) = fraction;
        stat.I(idx) = I;
        stat.H(idx, :) = H;
        idx = idx + 1;
    end
end
fprintf('\n');
%%
stat.meanH = [];
idx = 1;
color = [0.2070    0.6953    0.3398];

for fraction=unique(stat.fraction)
    map = stat.fraction == fraction;
    stat.stat.fraction(idx) = fraction;
    stat.stat.H1(idx) = mean(stat.H(map, 1));
    stat.stat.H2(idx) = mean(stat.H(map, 2));
    stat.stat.H1g2(idx) = mean(stat.H(map, 1) - stat.I(map)');
    stat.stat.H2g1(idx) = mean(stat.H(map, 2) - stat.I(map)');
    if output; 
        subplot(2,1,1); 
        plot(fraction, stat.H(map, 1) - stat.I(map)', 'k.'); hon;
        subplot(2,2,3); 
        plot(fraction, stat.H(map, 1), 'k.'); hon;
        subplot(2,2,4); 
        plot(fraction, stat.H(map, 2), 'k.'); hon;
    end
    idx = idx + 1;
end
Hx = stat.stat.H1;
Hxgy = stat.stat.H1g2;
Hy = stat.stat.H2;
if output;
    subplot(2,1,1); plot(stat.stat.fraction, Hxgy, '-', 'LineWidth', 2, 'color', color);
    title('H(x|y)');
    xlabel('fraction of data');
    ylabel('information [bits]');
    hoff;
    subplot(2,2,3); plot(stat.stat.fraction, Hx, '-', 'LineWidth', 2, 'color', color);
    title('H(x)');
    xlabel('fraction of data');
    ylabel('information [bits]');
    hoff;
    subplot(2,2,4); plot(stat.stat.fraction, Hy, '-', 'LineWidth', 2, 'color', color);
    title('H(y)');
    xlabel('fraction of data');
    ylabel('information [bits]');
    hoff;
end
%%
color = [0.8594    0.2852    0.3477];
h = Hx;
px = polyfit(stat.stat.fraction ./ size(x, 2), h, 2);
h = Hxgy;
pxgy = polyfit(stat.stat.fraction ./ size(x, 2), h, 2);
h = Hy;
py = polyfit(stat.stat.fraction ./ size(x, 2), h, 2);
if output;
    subplot(2,1,1); hon
    plot([0 stat.stat.fraction], polyval(pxgy, [0 stat.stat.fraction ./ size(x, 2)]), ':', 'LineWidth', 2, 'color', color);
    hoff
    subplot(2,2,3); hon
    plot([0 stat.stat.fraction], polyval(px, [0 stat.stat.fraction ./ size(x, 2)]), ':', 'LineWidth', 2, 'color', color);
    hoff
    subplot(2,2,4); hon
    plot([0 stat.stat.fraction], polyval(py, [0 stat.stat.fraction ./ size(x, 2)]), ':', 'LineWidth', 2, 'color', color);
    hoff
end
I = px(end) - pxgy(end);
H = [px(end) py(end)];