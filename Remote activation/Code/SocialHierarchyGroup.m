function rank = SocialHierarchyGroup(obj)
%%
SocialExperimentData;
obj = TrackLoad(obj);
objs = TrackGroupLoad(obj, 1:GroupsData.nDays, 'Res/');
%%
fields = {'ChaseEscape', 'AggressiveChase'};
for fidx=1:length(fields)
    edges = cell(1,2); edges{1} = 1:obj.nSubjects; edges{2} = edges{1};
    f = fieldnames(objs);
    ce = zeros(GroupsData.nSubjects);
    ce_ = cell(1, GroupsData.nDays);
    ncontacts = zeros(GroupsData.nSubjects);
    ncontacts_ = cell(1, GroupsData.nDays);
    ranks_ = cell(1, GroupsData.nDays);
    hours_ = zeros(1, GroupsData.nDays);
    for i=1:length(f)
        
        obj = objs.(f{i});
        ranks_{i} = obj.Hierarchy.(fields{fidx}).rank;
        ce_{i} = hist3(...
            [obj.Contacts.Behaviors.(fields{fidx}).Chaser(obj.Contacts.Behaviors.(fields{fidx}).Map)', ...
            obj.Contacts.Behaviors.(fields{fidx}).Escaper(obj.Contacts.Behaviors.(fields{fidx}).Map)'], ...
            'Edges', edges);
        ce = ce + ce_{i};
        
        ncontacts_{i} = hist3(sort([obj.Contacts.List.subjects]', 2), 'Edges', edges);
        ncontacts_{i} = ncontacts_{i} + ncontacts_{i}';
        ncontacts = ncontacts + ncontacts_{i};
        
        if obj.StartTime < 0 || obj.EndTime < 0
            obj = TrackFindDarkPeriodAndSave(obj);
        end
        hours_(i) = (obj.EndTime - obj.StartTime) / 3600;
    end
    %% newer version
    mat = ce - ce';
    fmat = mat;
    mat(binotest(ce, ce + ce')) = 0;
    fmat = fmat .* (fmat > 0);
    mat = mat .* (mat > 0);
    
    [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
    diluted = mat;
    diluted(removed ~= 0) = 0;
    
    [frank, fremoved] = TopoFeedbackArcSetHierarchicalOrder(fmat);
    fdiluted = fmat;
    fdiluted(fremoved ~= 0) = 0;
    
    
    obj.Hierarchy.Group.(fields{fidx}) = struct();
    for i=1:length(f)
        obj = objs.(f{i});
        if fidx == 1
            obj.Hierarchy.Group = struct();
        end
        
        obj.Hierarchy.Group.(fields{fidx}) = struct();
        obj.Hierarchy.Group.(fields{fidx}).rank = rank;
        obj.Hierarchy.Group.(fields{fidx}).nLevels = length(unique(rank));
        obj.Hierarchy.Group.(fields{fidx}).strength = sum(diluted(:) > 0);
        obj.Hierarchy.Group.(fields{fidx}).map = diluted;
        obj.Hierarchy.Group.(fields{fidx}).fullMap = fdiluted;
        obj.Hierarchy.Group.(fields{fidx}).ChaseEscape = ce;
        obj.Hierarchy.Group.(fields{fidx}).Contacts = ncontacts;
        obj.Hierarchy.Group.(fields{fidx}).removed = removed;
        obj.Hierarchy.Group.(fields{fidx}).RecordingDurationInHours = sum(hours_);
        obj.Hierarchy.Group.(fields{fidx}).Days = struct();
        for day=1:GroupsData.nDays
            obj.Hierarchy.Group.(fields{fidx}).Days(day).ranks = ranks_{day};
            obj.Hierarchy.Group.(fields{fidx}).Days(day).ChaseEscape = ce_{day};
            obj.Hierarchy.Group.(fields{fidx}).Days(day).Contacts = ncontacts_{day};
            obj.Hierarchy.Group.(fields{fidx}).Days(day).RecordingDurationInHours = hours_(day);
        end
        TrackSave(obj);
    end
end

%% old verions
return
SocialExperimentData;
%%
PostPredPrey = zeros(GroupsData.nSubjects);
for day=1:GroupsData.nDays
    obj = TrackLoad({id, day});
    try
        curr = obj.Interactions.PredPrey;
        PostPredPrey = PostPredPrey + curr.PostPredPrey;
    catch
        obj = SocialHierarchy(obj);
        curr = obj.Interactions.PredPrey;
        PostPredPrey = PostPredPrey + curr.PostPredPrey;
    end
end
%%
mat = PostPredPrey - PostPredPrey';
mat(binotest(PostPredPrey, PostPredPrey + PostPredPrey')) = 0;
mat = mat .* (mat > 0);

[rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
