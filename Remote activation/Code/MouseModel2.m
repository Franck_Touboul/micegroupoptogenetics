opt = struct();
opt.Resolution = 10;
opt.NumVerteb = 1 + 7 + 13 + 6 + 4 + 30; % skull + 7 cervical + 12-14 thoracic + 5-6 lumbar + 4 sacral + 27-30 caudal
opt.Scale = 0.1;
opt.Draw3D = false;

%%
Model.Bones = struct();
Model.Types = [0, 1 * ones(1,7), 2 * ones(1,13), 3 * ones(1,6), 4 * ones(1,4), 5 * ones(1,10), 6 * ones(1,10), 7 * ones(1,10)];

for i=1:opt.NumVerteb
    Bone = struct();
    Skin = struct();
    f2b = i - find(Model.Types == Model.Types(i), 1);
    b2f = find(Model.Types == Model.Types(i), 1, 'last') - i;
    for j=1:opt.Resolution
        switch Model.Types(i)
            case 0
                Skin(j).Distance = .1;
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
            case 1
                Skin(j).Distance = .1;
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
            case 2
                %Skin(j).Distance = .45 + .13*ref;
                Skin(j).Distance = .07;
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
            case 3
                Skin(j).Distance = .15;
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
            case 4
                Skin(j).Distance = .15;
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
            case 5
                Skin(j).Distance = .04-f2b*(.04 / 30);
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
            case 6
                Skin(j).Distance = .04-(f2b+10)*(.04 / 30);
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
            case 7
                Skin(j).Distance = .04-(f2b+20)*(.04 / 30);
                Skin(j).Angle = j / opt.Resolution * 2 * pi;
        end
    end
    Bone.Skin = Skin;
    switch Model.Types(i)
        case 0 % skull
            Bone.Length = .70;
            Bone.Yaw = 0;
            Bone.Pitch = 0.4;
            Bone.Roll = 0;
        case 1
            Bone.Length = .07;
            Bone.Yaw = 0;
            if f2b == 0
                Bone.Pitch = -.4;
            else
                Bone.Pitch = -0;
            end
            Bone.Roll = 0;
        case 2
            Bone.Length = .07;
            Bone.Yaw = 0;
            if f2b == 0
                Bone.Pitch =  .85;
            else
                Bone.Pitch = -.06;
            end
            Bone.Roll = 0;
        case 3
            Bone.Length = .15;
            Bone.Yaw = 0;
            Bone.Pitch = -.18;
            Bone.Roll = 0;
        case 4
            Bone.Length = .15;
            Bone.Yaw = 0;
            Bone.Pitch = 0;
            Bone.Roll = 0;
        case 5
            Bone.Length = (.15-f2b*0.005) * 1.2;
            Bone.Yaw = 0;
            if f2b == 0
                Bone.Pitch = 0.35;
            else
                Bone.Pitch = -0.36;
            end
            Bone.Roll = 0;
        case 6
            Bone.Length = (.15-(f2b + 10)*0.005) * 1.2;
            Bone.Yaw = 0;
                Bone.Pitch = 0.1;
            Bone.Roll = 0;
        case 7
            Bone.Length = (.15-(f2b + 20)*0.005) * 1.2;
            Bone.Yaw = 0;
            Bone.Pitch = 0.1;
            Bone.Roll = 0;
    end
    if i==1
        Model.Bones = Bone;
    else
        Model.Bones(i) = Bone;
    end
end

%% Draw 3D
dmap = MySubCategoricalColormap;
if opt.Draw3D
    P0 = [0; 0; 0];
    r = .1;
    cmap = lines(length(Model.Bones));
    rot = eye(3);
    for i=1:opt.NumVerteb
        Bone = Model.Bones(i);
        rot = rot * RotMat(Bone.Yaw, Bone.Pitch, Bone.Roll);
        P1 = P0 + rot * [Bone.Length; 0; 0];
        plot3([P0(1) P1(1)], [P0(2) P1(2)], [P0(3) P1(3)], 'color', cmap(i, :), 'LineWidth', 2);
        hon;
        plot3(P0(1), P0(2), P0(3), 'o', 'MarkerFaceColor', ones(1,3)*.5, 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
        %DepthAddTriangle([x0+r, x0-r, x1] * opt.Scale, [y0+r, y0-r, y1] * opt.Scale, [z0 z0 z1] * opt.Scale);
        %% skin
        if 1==2
            for j=1:length(Bone.Skin)
                Skin = Bone.Skin(j);
                S = P0 + rot * ([0; sin(Skin.Angle); cos(Skin.Angle) ] * Skin.Distance);
                KLine(P0, S, ':', 'Color', ones(1,3)*.1);
                plot3(S(1), S(2), S(3), 'o', 'MarkerFaceColor', ones(1,3)*.5, 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
            end
        end
        %%
        P0 = P1;
    end
    plot3(P1(1), P1(2), P1(3), 'o', 'MarkerFaceColor', ones(1,3)*.7, 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
    grid on;
    campos([0 90 0]); camproj('orthographic')
    %axis([-.5 .5 -.5 .5 -.5 .5] )
    hoff
    xlabel('x')
    ylabel('y')
    zlabel('z')
else
    img = imread('mouse-skeleton.Par.0001.Image.gif');
    %img = imread('mouse-xray.jpg');
    sx = 155;
    ox = 20;

    sy = 155;
    oy = 130;
    
    imshow(img);
    hon;
    P0 = [0; 0; 0];
    r = .1;
    cmap = lines(length(Model.Bones));
    rot = eye(3);
    for i=1:opt.NumVerteb
        Bone = Model.Bones(i);
        rot = rot * RotMat(Bone.Yaw, Bone.Pitch, Bone.Roll);
        P1 = P0 + rot * [Bone.Length; 0; 0];
        plot([P0(1) P1(1)] * sx + ox, [P0(3) P1(3)] * sy + oy, 'color', ones(1, 3) *.5, 'LineWidth', 2);
        hon;
        plot(P0(1) * sx + ox, P0(3) * sy + oy, 'o', 'MarkerFaceColor', dmap(Model.Types(i)+1, :), 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
        %DepthAddTriangle([x0+r, x0-r, x1] * opt.Scale, [y0+r, y0-r, y1] * opt.Scale, [z0 z0 z1] * opt.Scale);
        %% skin
        if i>opt.NumVerteb - 30
            for j=1:length(Bone.Skin)
                Skin = Bone.Skin(j);
                S = P0 + rot * ([0; sin(Skin.Angle); cos(Skin.Angle) ] * Skin.Distance);
               plot(S(1) * sx + ox, S(3) * sy + oy, 'o', 'MarkerFaceColor', dmap(Model.Types(i)+1, :), 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
            end
        end
        %%
        P0 = P1;
    end
    plot(P1(1) * sx + ox, P1(3) * sy + oy, 'o', 'MarkerFaceColor', ones(1,3)*.7, 'LineWidth', 2, 'MarkerEdgeColor', 'none', 'MarkerSize', 8);
    grid on;
    %campos([0 90 0]); camproj('orthographic')
    %axis([-.5 .5 -.5 .5 -.5 .5] )
    hoff
    xlabel('x')
    ylabel('y')
    zlabel('z')
    
end