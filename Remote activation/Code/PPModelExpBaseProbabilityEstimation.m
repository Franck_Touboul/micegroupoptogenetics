function model = PPModelExpBaseProbabilityEstimation(x, model, state)
%%
input = abs(model.states(state).ExpBase.theta - x(1, :));

p = DiscreteProbabilityEstimate(...
    input, ...
    model.Histogram.nBins, ...
    model.Histogram.minval, ...
    model.Histogram.maxval);
bins = sequence(model.Histogram.minval, model.Histogram.maxval, model.Histogram.nBins + 1);

centers = (bins(1:end-1) + bins(2:end)) / 2;

b = nlinfit(centers, p, ...
    @(b, X) halfCircularUniformExp(abs(model.states(state).ExpBase.theta - X), b(1), b(2)), ...
    [model.states(state).ExpBase.alpha, model.states(state).ExpBase.m], statset('FunValCheck', 'off'));

model.states(state).ExpBase.alpha = b(1);
model.states(state).ExpBase.m = b(2);

%%
cplot(centers, p, 'kx:', centers, model.states(state).posterior(centers, model, state))
