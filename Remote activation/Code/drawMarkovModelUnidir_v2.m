function img = drawMarkovModelUnidir_v2(hmm)
radios = 35;
hmm.count = hmm.count / max(hmm.count(:));
for i=1:size(hmm.trans, 1)
    for j=1:size(hmm.trans, 2)
        if i~=j && hmm.count(i,j) ~= 0
            x1 = hmm.coord(i, 1);
            y1 = hmm.coord(i, 2);
            x2 = hmm.coord(j, 1);
            y2 = hmm.coord(j, 2);
            line([x1 x2], [y1 y2], 'Color', [.3 .3 .3], 'LineWidth', hmm.count(i, j) * 5);
        end
    end
end
hold on;

cmap = prettyBlue;
%cmap = hot(256);
m1 = min(diag(hmm.trans));
m2 = max(diag(hmm.trans));
for i=1:size(hmm.coord, 1)
    x = hmm.coord(i, 1);
    y = hmm.coord(i, 2);
    ratio = hmm.priors(i) / sum(hmm.priors);
    weight = .5 * (hmm.trans(i, i) - m1) / (m2 - m1) + .5
    %color = floor((hmm.priors(i) / max(hmm.priors)) * (size(cmap, 1) - 1)) + 1;
    drawCircle(x, y, radios, [48, 165, 256]/256, 'lineWidth', 5, 'edgecolor', [.95 .95 .95]);
    %drawPie(x, y, radios, ratio* 2 * pi,  [48, 256, 165]/256, 'EdgeColor', 'None');
    %drawCircle(x, y, radios * weight, [48, 165, 256]/256, 'lineWidth', 5, 'edgecolor', [.95 .95 .95], 'faceColor', 'none');
    text(x, y, hmm.labels{i}, 'HorizontalAlignment','center', 'FontWeight', 'Bold', 'FontSize', 8, 'Color', 'w');
end
hold off;
axis equal;
axis off
box off

