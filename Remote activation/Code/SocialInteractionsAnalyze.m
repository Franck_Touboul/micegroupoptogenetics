function obj = SocialInteractionsAnalyze(obj)
%%
fprintf('# finding ''Chases''\n');
nIters = 10;
obj.Interactions.MinEventDuration = 8;
obj.MaxShelteredContanctDuration = 25;
%%
obj = TrackLoad(obj);
obj = SocialFindInteractions(obj);

%%
obj.Interactions.states = cell(obj.nSubjects);
obj.Interactions.Model = cell(obj.nSubjects);
for m1=1:obj.nSubjects
    for m2=1:obj.nSubjects
        if m1 == m2
            continue;
        end
        %%
        invalid = obj.sheltered(m1, :) | obj.sheltered(m2, :);
        [start, finish, len] = FindEvents(invalid);
        for i=find(len <= obj.MaxShelteredContanctDuration)
            invalid(start(i):finish(i)) = 0;
        end
        
        %%
        currAngles = obj.angle{m1, m2};
        
        % setting random angles to nans
        nNan = sum(isnan(currAngles));
        currAngles(isnan(currAngles)) = pi * rand(1, nNan);
        %
        currContacts = obj.contact{min(m1, m2), max(m1, m2)};
        input = [...
            currAngles;
            currContacts
            ];
        %valid = ~obj.hidden(m1, :) & ~obj.hidden(m2, :);
        
        [begF, endF] = FindEvents(~invalid);
        inputs = cell(1, length(begF));
        for i=1:length(begF)
            inputs{i} = [input(1, begF(i):endF(i)); input(2, begF(i):endF(i))];
        end
        
        %%
        model = GeneratePerdPreyModel(input(:, ~invalid));
        %%
        bins = sequence(model.Histogram.minval, model.Histogram.maxval, model.Histogram.nBins + 1);
        centers = (bins(1:end-1) + bins(2:end)) / 2;
        
        count = Reprintf('# - iter %d/%d', 0, nIters);
        for iter=0:nIters
            count = Reprintf(count, '# - iter %d/%d', iter, nIters);
            backtrack = ModelViterbiSequence_v2(model, inputs, false);
            
            state = ones(1, obj.nFrames);
            for i=1:length(begF)
                currBacktrack = backtrack{i};
                %currBacktrack(currBacktrack == 1 & currContacts(begF(i):endF(i)) == 1) = 4;
                state(begF(i):endF(i)) = currBacktrack;
            end
            
            %%
            if iter < nIters
                for s=1:length(model.states)
                    model = model.states(s).estimate(input(:, state == s), model, s);
                end
                drawnow;
            end
        end
        fprintf('\n');
        statesBackup = state;
        %% remove non contact events
        [start, finish] = FindEvents(state > 1);
        for s=1:length(model.states)
            if strcmp(model.states(s).title, 'cont')
                for i=1:length(start)
                    if ~any(state(start(i):finish(i)) == s)
                        state(start(i):finish(i)) = 1;
                    end
                end
            end
        end
        
        %% remove short events
        for i=2:model.nstates
            if model.states(i).type == 1
                c = conv([0 (state==i)  0], [1 -1]);
                startFrame = find(c > 0) - 1;
                endFrame   = find(c < 0) - 2;
                duration = endFrame - startFrame + 1;
                for l=find(duration < obj.Interactions.MinEventDuration)
                    state(startFrame(l):endFrame(l)) = 1;
                end
            end
        end
        obj.Interactions.states{m1, m2} = state;
        obj.Interactions.Model{m1, m2} = model;
    end
end
%%
% [start, finish, len] = FindEvents(state~=1);
% epochs = cell(1, length(start));
% for i=1:length(start)
%     prev = -1;
%     index = 1;
%     for f=start(i):finish(i)
%         if state(f) ~= prev
%             epochs{i}.title{index} = model.states(state(f)).title;
%             epochs{i}.start(index) = f;
%             if index > 1
%                 epochs{i}.finish(index-1) = f-1;
%             end
%             prev = state(f);
%             index = index + 1;
%         end
%     end
%     epochs{i}.finish(index-1) = finish(i);
% end

% %%
% clf
% for s=1:model.nstates
%     SquareSubplpot(model.nstates, s);
%     sp = double(obj.relativeSpeed{m1,m2}(state == s));
% %    sp = obj.relativeSpeed{m1,m2}(1, state == s);
%     [h, x] = hist(sp, 100);
%     %x = x / sum(x);
%     valid = h > 50;
%     h = h / sum(h) / (x(2) - x(1));
%     phat = gamfit(sp(sp > 0)) ;
%     g = gampdf(x, phat(1), phat(2));
%     plot(x(valid), h(valid), x(valid), g(valid)); hold on;
%     title(model.states(s).title);
% end
% hold off;
