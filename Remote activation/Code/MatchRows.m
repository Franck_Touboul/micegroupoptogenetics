function m = MatchRows(z)
m = zeros(size(z), class(z));
s = sortrows(z);
m(1, :) = s(1, :);
d = pdist2(s, s, 'hamming'); 
d(logical(eye(size(d, 1)))) = inf;
prev = 1;
for i=2:size(m, 1)
    [q, curr] = min(d(prev, :));
    m(i, :) = s(curr, :);
    d(:, curr) = inf;
    prev = curr;
end