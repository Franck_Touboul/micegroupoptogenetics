data = ReadStrudels('log/SocialPottsExtrapolate.o*');
%%
clf
cmap = MyCategoricalColormap;
res.ExpList = unique({data.exp});
for e=1:length(res.ExpList)
    map = strcmp({data.exp}, res.ExpList{e});
    levels = [data.level];
    nFrames = [data.nFrames];
    p = [data.Ik];
    %p = cumsum([data.Ik]);
    %p = p ./ repmat(p(end, :), size(p, 1), 1);
    %for i=1:length(level_)
        %lmap = map & levels == level_(i);
        for o=1:size(p, 1)
            subplot(length(res.ExpList), size(p, 1)+1, (e - 1)*(size(p, 1) + 1) + o);
            hon;
            plot(log2(levels(map)), p(o, map), 'ko', 'MarkerFaceColor', cmap(o, :), 'MarkerEdgeColor', 'none'); 
            hoff
        end
        subplot(length(res.ExpList), size(p, 1)+1, (e - 1)*(size(p, 1) + 1) + size(p, 1) + 1);
        csp = sum(p);
        plot(levels(map), csp(map), 'ko', 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'none'); 
        title(res.ExpList{e});
    %end
end

