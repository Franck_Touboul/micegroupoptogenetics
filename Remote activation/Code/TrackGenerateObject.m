function obj = TrackGenerateObject(videoFile)
if nargin == 0
    videoFile = '';
end
obj.VideoFile = videoFile;
obj.Name = '';
obj.nSubjects = 4;          % the number of subjects in the arena

obj.Unix.VideoPath = '../Movies/';
obj.Windows.VideoPath = 'x:\tests\SocialMice\Movies\';
%%
obj.Colors = struct();
%%
obj.StartTime = -1; % video start time [sec]
obj.EndTime = -1;   % video end time [sec]
%%
obj.OutputPath = 'Res/';
obj.Output = false;
obj.OutputToFile = true;
obj.OutputInOldFormat = true;
obj.FilePrefix = regexprep(regexprep(obj.VideoFile, '\.[^\.]*$', ''), '.*[\\/]', '');
%%
obj.ValidROI = [];
%% meta
obj.NoiseThresh = 10;        %
obj.UseAdaptiveThresh = true;        %
%   - background:
obj.nBkgFrames = 40;        % no. of frames to use when computing background image
obj.ErodeRadios = 2;
%   - colors:
obj.ColorsFile = '';
obj.MinNumOfObjects = 4;
obj.nColorFrames = inf;      % no. of frames to use to determine the subject colors
obj.nColorBins = 20;         % no. of bins in color histogram
obj.nKMeansRepeats = 20;     % no. of time to run k-means algorithm for optimal clustring of colors
%% segementation:
obj.MinNumOfPixels = 40;
obj.MinNumOfPixelsAfterErode = 10;
obj.MinSolidity = .8;          % minimal solidity of segments
%
obj.VideoScale = .25;            % working resolution relative to original movie
obj.MaxNumOfCents = 10;
obj.MaxNumOfObjects = 20;
obj.MinSegmentGap = 3;             % important if there are small hidden areas (like wallthrough walls)
%% path tracking
obj.Kalman.Q = 8;
obj.Kalman.R = 1;
%
obj.ColorMatchThresh = 0.1; % minimum certainty probability required to assign label to segment
obj.MaxHiddenDuration = 25; % [frames] maximal time the tracking can lose a subject
obj.NumOfProbBins = 20; %
obj.MinJumpProb = 0.01; %
obj.AllowHiddenTransitions = false; %
obj.MaxIsolatedDistance = 12;
%%
obj = TrackReadVideoFileProperties(obj);
if nargin > 0
    obj = TrackLoad(obj);
end
try
    obj.VideoFileSize = GetFileSize(obj.VideoFile);
catch
    obj.VideoFileSize = [];
end

