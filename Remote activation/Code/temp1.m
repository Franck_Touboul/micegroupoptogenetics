
order = 2;
nobj = obj;
me = nobj.Analysis.Potts.Model{order};
Entropy = [];
Djs = [];
Maps = [];
Removed = [];
idx = 1;
%%
p = exp(me.perms * me.weights');
perms_p = exp(me.perms * me.weights');
Z = sum(perms_p);
P = p / Z;
%%
while length(me.labels) > 2
    fprintf('#-------------------------------------------------\n');
    fprintf('# using %d features (%.1f%%)\n', length(me.labels), length(me.labels)/length(obj.Analysis.Potts.Model{order}.labels)*100);
    map = true(1, length(me.labels));
    [s, o] = sort(me.weights);
    minidx = o(floor(length(o)/2));
    map(minidx) = false;
    Removed(idx) = minidx;
    if idx == 1
        Maps = map;
    else
        Maps(idx, Maps(idx-1, :)) = map;
    end
    nobj = retrainPottsModel(nobj, order, map);
    % Entrop
    p = exp(me.perms * me.weights');
    perms_p = exp(me.perms * me.weights');
    Z = sum(perms_p);
    p = p / Z;
    %
    Entropy(idx) = -p' * log2(p);
    Djs(idx) = JensenShannonDivergence(p(:)', P(:)');
    fprintf('# Djs=%.1f, Entropy=%.1f\n', Djs(idx), Entropy(idx));
    %
    me = nobj.Analysis.Potts.Model{order};
    idx = idx + 1;
end
%%
nParams = sum(Maps, 2);
plot(nParams, Djs)
xlabel('# interactions');
ylabel('Djs');
