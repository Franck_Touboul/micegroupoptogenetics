function close_all_figures()
% close_all_figures()
%   Closes all figures.
%
% O.Fr. 15/4/2007

g = get(0,'CurrentFigure');
while (g)
    close(g);
    g = get(0,'CurrentFigure');
end
    