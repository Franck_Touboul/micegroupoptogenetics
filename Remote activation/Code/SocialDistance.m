function d = SocialDistance(obj, other, order)
%%






function ShapeWeights(obj, me)
%%
u_ = unique(me.order);
sz = 0;
for u=u_(:)'
    me.vec = [];
    sz = sz + double(obj.nSubjects^u) * double(obj.ROI.nZones^u);
end
me.vec = nan(1, sz);
offset = 1;
for u=u_(:)'
    map = me.order == u;
    w = me.weights(map);
    l = {me.labels{map}};
    svec = double(obj.nSubjects.^(u-1:-1:0));
    zvec = double(obj.ROI.nZones.^(u-1:-1:0));
    for i=1:length(l)
        coord = (svec * (l{i}(1, :)' - 1)) * double(obj.ROI.nZones^u) + zvec * (l{i}(2, :)' - 1) + 1;
        me.vec(offset + coord) = w(i);
    end
    offset = offset + double(obj.nSubjects^u) * double(obj.ROI.nZones^u);
end
