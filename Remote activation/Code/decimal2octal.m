function y = decimal2octal(x, c)

% Convert a decimanl number into a binary array
% 
% Similar to dec2bin but yields a numerical array instead of a string and is found to
% be rather faster

if nargin < 2
    c = ceil(log(x + 1)/log(8)); % Number of divisions necessary ( rounding up the log2(x) )
end

y(c) = 0; % Initialize output array
for i = 1:c
    r = floor(x / 8);
    y(c+1-i) = x - 8*r;
    x = r;
end
