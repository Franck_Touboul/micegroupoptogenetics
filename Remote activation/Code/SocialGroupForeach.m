function objs = SocialGroupForeach(objs, func, varargin)

tests = fieldnames(objs);
fprintf(['# performing ' func2str(func) ' on objects\n']);
for i=1:length(tests)
    fprintf(['# - running on ' tests{i} '\n']);
    figure(i);
    set(gcf, 'Name', tests{i});
    obj = func(objs.(tests{i}), varargin{:});
    objs.(tests{i}) = obj;
    saveFigure([ obj.OutputPath obj.FilePrefix '.' func2str(func) ]);
end
