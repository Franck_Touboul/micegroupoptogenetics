SocialExperimentData;

experiments = {...
    'SC.exp0001.day%02d.cam01',...
    };

%%
zonesHist = [];
corrupt = {};
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        fprintf('#-------------------------------------\n# -> %s\n', prefix);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'common'});
        catch me
            MyWarning(me)
            corrupt = {corrupt{:}, prefix};
            continue;
        end
        obj.Output = false;
        obj.OutputToFile = false;
        obj.OutputInOldFormat = false;
        obj = SocialSoft(obj);
        subplot(4,1,day);
        bar(obj.Analysis.Soft.Zones.Histogram);
        zonesHist = cat(3, zonesHist, obj.Analysis.Soft.Zones.Histogram);
    end
end
%%
clf
subplot(3,1,1);
z = mean(zonesHist, 3);
barweb(z * 100, z * 0, [], obj.ROI.ZoneNames, ['location histogram: ' obj.FilePrefix], 'location', '% time', obj.Colors.Centers);
a = axis;
axis([a(1) a(2) a(3) 60]);
return
%%

%%
all = [];
type = [];
days = [];
id = [];
pp.all = [];
pp.all2 = [];
pp.all3 = [];
pp.day = [];
pp.group = [];
pp.id = [];

for i=1:length(experiments) %:length(a)
    h = cell(1,4);
    for day = 1:nDays;
        prefix = sprintf(experiments{i}, day);
        fprintf('# -> %s\n', prefix);
        obj = TrackLoad(['Res/' prefix '.obj.mat'], {'Interactions', 'common'});
        obj = SocialAnalysePredPreyModel(obj);
        r = sum(obj.Interactions.PredPrey.PostPred, 1) ./ sum(obj.Interactions.PredPrey.Contacts, 1);
        r2 = sum(obj.Interactions.PredPrey.PostPrey, 1) ./ sum(obj.Interactions.PredPrey.Contacts, 1);
        r3 = sum(obj.Interactions.PredPrey.PrePred, 1) ./ sum(obj.Interactions.PredPrey.Contacts, 1);
        pp.all = [pp.all, r];
        pp.all2 = [pp.all2, r2];
        pp.all3 = [pp.all3, r3];
        pp.day = [pp.day, r * 0 + day];
        pp.group = [pp.group, r * 0 + i];
        pp.id = [pp.id, 1:4];
        obj = SoftZoneCategorize(obj);
        for s = 1:4;
            h{s}(day, :) = histc(obj.category(s, :), 1:obj.ROI.nCategories);
            h{s}(day, :) = h{s}(day, :) / sum(h{s}(day, :));
            all = [all; h{s}(day, :)];
            type = [type; i];
            days = [days; day];
            id = [id; s];
        end
    end
    for s = 1:4;
        subplot(ngroups, 4, (i - 1) * 4 + s)
        imagesc(h{s}); 
        set(gca, 'XTick', 1:obj.ROI.nCategories, 'Xticklabel', obj.ROI.CategoryNames, 'Ytick', [1:4])
        ylabel('day');
        drawnow;
    end
end
%colormap(gray)
%%
clf
r1 = 1:4*4*4;
sg = {'^', 's', 'd', 'p', 'x'};
for i=1:4
    for s=1:4
        idx = pp.group == i & pp.id == s;
%        plot3(mean(pp.all(idx)), mean(pp.all2(idx)), mean(pp.all3(idx)), ['r' sg{i}]);
        plot(mean(pp.all(idx)), mean(pp.all2(idx)), ['r' sg{i}]);
        hold on;
    end
end
hold on;
r2 = r1(end)+1:length(pp.all);
for i=5:9
    for s=1:4
        idx = pp.group == i & pp.id == s;
%        plot3(mean(pp.all(idx)), mean(pp.all2(idx)), mean(pp.all3(idx)), ['b' sg{i-4}]);
        plot(mean(pp.all(idx)), mean(pp.all2(idx)), ['b' sg{i-4}]);
        hold on;
    end
end
hold off;
xlabel('pred');
ylabel('prey');
zlabel('initiate');
%%
clf;
%cmap = [1 0 0; 0 0 1];
cmap = lines(ngroups );
for i=1:ngroups %:length(a)
    sign = {'^', 's', 'd', 'p'};
    for s = 1:4
        map = find((type == i) & (id == s));
%        plot3(all(map, 1), all(map, 2), all(map, 3), sign{day}, 'color', cmap((i < 5) + 1, :));
%        plot(all(map, 1), all(map, 2), '.', 'color', cmap((i < 5) + 1, :));
        if i<5
            plot(mean(all(map, 1)), mean(all(map, 2)), 's', 'color', cmap(i, :));
        else
            plot(mean(all(map, 1)), mean(all(map, 2)), 'o', 'color', cmap(i, :));
        end
        hold on;
    end
end

xlabel(obj.ROI.CategoryNames{1});
ylabel(obj.ROI.CategoryNames{2});
