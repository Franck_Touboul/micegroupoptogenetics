SocialExperimentData
cmap = lines(8*4) ;
clf
ACF = [];
for id=11:18
    %cmap = MyCategoricalColormap;
    obj = TrackLoad({id 2}, {'zones'});
    for i=1:GroupsData.nSubjects
        %%
        times = find(diff(obj.zones(i, :)) ~= 0);
        durations = diff(times);
        acf = autocorr(durations);
        plot(0:length(acf)-1, acf, 'color', cmap(i, :));
        ACF = [ACF; acf];
        hon;
    end
    drawnow;
end