function BoxPatch(x, y, C, style, radios)

if nargin < 5

    radios = 1;

end

r = radios;

X = [x-r x+r x+r x-r];

Y = [y-r y-r y+r y+r];

 

patch(X, Y, C, style{:});