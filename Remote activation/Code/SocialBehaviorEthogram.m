function [obj, img, eth, cmap] = SocialBehaviorEthogram(obj)
field = 'AggressiveChase';

source = MySubCategoricalColormap;
zonescmap = MyZonesColormap;
cmap(1, :) = source(4, :);
cmap(2, :) = source(2, :);
cmap(3, :) = source(6, :);

cmap(4, :) = source(1, :);
cmap(5, :) = source(8, :);

cmap(6, :) = zonescmap(9, :);
cmap(7, :) = zonescmap(6, :);
cmap(8, :) = [1 1 1];

eth = ones(obj.nSubjects, obj.nFrames) * size(cmap, 1);
eth(obj.zones == 9) =  6;
eth(obj.zones == 6) =  7;
size(eth)
for i=1:min(length(obj.Contacts.Behaviors.(field).Map), length(obj.Contacts.List))
    c = obj.Contacts.List(i);
    if obj.Contacts.Behaviors.(field).Map(i)
        for j=1:length(c.subjects);
            if obj.Contacts.Behaviors.(field).Chaser(i) == c.subjects(j)
                eth(c.subjects(j), c.beg(j):c.end(j)) = 3;
            elseif obj.Contacts.Behaviors.(field).Escaper(i) == c.subjects(j)
                eth(c.subjects(j), c.beg(j):c.end(j)) = 2;
            else                
                eth(c.subjects(j), c.beg(j):c.end(j)) = 1;
            end
        end
%     elseif obj.Contacts.Behaviors.ChaseEscape.Map(i)
%         for j=1:length(c.subjects);
%             if obj.Contacts.Behaviors.ChaseEscape.Chaser(i) == c.subjects(j)
%                 eth(c.subjects(j), c.beg(j):c.end(j)) = 5;
%             elseif obj.Contacts.Behaviors.ChaseEscape.Escaper(i) == c.subjects(j)
%                 eth(c.subjects(j), c.beg(j):c.end(j)) = 4;
%             else                
%                 eth(c.subjects(j), c.beg(j):c.end(j)) = 1;
%             end
%         end
    else
        for j=1:length(c.subjects);
            eth(c.subjects(j), c.beg(j):c.end(j)) = 1;
        end
    end
end
clf;
subplot(5,1,1);
imagesc(eth); 
size(eth)
axis off;
colormap(cmap);
%%
try
fname = 'temp_SocialBehaviorEthogram.png';
print(gcf, ['temp_SocialBehaviorEthogram.png'], '-dpng');
im = imread(fname);
sim = sum(im ~= 255, 3);
x1 = find(sum(sim), 1);
x2 = find(sum(sim), 1, 'last');
y1 = find(sum(sim, 2), 1);
y2 = find(sum(sim, 2), 1, 'last');
img = im(y1:y2, x1:x2, :);
imwrite(img, ['Graphs/Ethogram/SocialBehaviorEthogram.' obj.FilePrefix '.png']);
end
return
%%
source1 = MyCategoricalColormap;
source2 = MySubCategoricalColormap;
cmap(1, :) = [1 1 1];
cmap(2, :) = source2(7, :);
cmap(3, :) = source2(3, :);
cmap(4, :) = source2(9, :);
cmap(5, :) = source2(8, :);
cmap(6, :) = source2(4, :);
cmap(7, :) = source2(6, :);
cmap(8, :) = source2(2, :);
cmap(9, :) = [1 1 1];

eth = ones(obj.nSubjects, obj.nFrames) * size(cmap, 1);
for i=1:length(obj.Contacts.List)
    c = obj.Contacts.List(i);
    for j=1:length(c.subjects);
        eth(c.subjects(j), c.beg(j):c.end(j)) = c.data{j};
    end
    if obj.Contacts.Behaviors.ChaseEscape(i)
        for j=1:length(c.subjects);
            prev = eth(c.subjects(j), c.beg(j):c.end(j));
            if obj.Contacts.Behaviors.Chaser(i) == c.subjects(j)
                prev(c.data{j} == 5) = 7;
                eth(c.subjects(j), c.beg(j):c.end(j)) = prev;
            elseif obj.Contacts.Behaviors.Escaper(i) == c.subjects(j)
                prev(c.data{j} == 6) = 8;
                eth(c.subjects(j), c.beg(j):c.end(j)) = prev;
            end
        end
    end
end
imagesc(eth); 
colormap(cmap);