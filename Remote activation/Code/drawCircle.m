function drawCircle(x, y, radios, color, varargin)
res = 0.02;
cord = [];
center = [x, y];
for angle=0:res:2*pi
    cord = [cord; center + [radios * sin(angle), radios * cos(angle)]];
end
fill(cord(:,1), cord(:,2), color, varargin{:});
