
SocialExperimentData
subjPerms = perms(1:GroupsData.nSubjects);
Vecs = {};
Orders = 1:4;
%Vecs2 = {};
for id=1:length(experiments)
    try
        %%
        obj = TrackLoad({id 2});
        %%
        c = zeros(obj.nSubjects);
        for d=1:length(obj.Hierarchy.Group.AggressiveChase.Days)
            c = c + obj.Hierarchy.Group.AggressiveChase.Days(d).ChaseEscape;
        end
        mat = c - c';
        mat = mat .* (mat > 0);
        [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
        [or, o] = sort(rank);
        %%
        fprintf('# - extracting weights... ');
        for order=Orders
            fprintf([num2str(order) '... ']);
            vecs = [];
            vecs = PottsVectorize_v2(obj, obj.Analysis.Potts.Model{order}, o);
            Vecs{order}{id} = vecs;
            %            Vecs2{order}{id} = SocialPottsVector(obj);
        end
        fprintf(' done!\n');
    catch
    end
end
%%
fprintf('# - computing probabilities for each model ');
Probs = {};
for order=Orders
    for id=1:length(Vecs{order})
        ProgressReport((order - 1)*length(Vecs{order}) + id, length(Vecs{order}) * obj.nSubjects)
        Probs{order}{id} = [];
        for i=1:size(Vecs{order}{id}, 1)
            p = exp(obj.Analysis.Potts.Model{order}.perms * Vecs{order}{id}(i, :)');
            Z = sum(p);
            p = p / Z;
            Probs{order}{id}(i, :) = p;
        end
    end
end
ProgressReport;
fprintf('\n');

%%
d = {};
match = {};
fprintf('# - computing Djs between groups ');
for order = Orders;
    probs = Probs{order};
    d{order} = zeros(length(probs));
    match(order).i = zeros(length(probs));
    match(order).j = zeros(length(probs));
    idx = 1;
    for i=1:length(probs)
        d{order}(i,i) = nan;
        for j=i+1:length(probs)
            ProgressReport((order - 1)*.5 * length(probs) *  (length(probs) + 1) + idx, .5 * length(probs) *  (length(probs) + 1) * obj.nSubjects)
            try
                D = zeros(size(probs{i}, 1), size(probs{j}, 1));
                for i1=1:size(probs{i}, 1)
                    for i2=1:size(probs{j}, 1)
                        D(i1, i2) = JensenShannonDivergence(probs{i}(i1, :), probs{j}(i2, :));
                    end
                end
                %D = corr(vecs{i}', vecs{j}');
                [d{order}(i, j), match(order).i(i, j), match(order).j(i, j)] = min2d(D);
                match(order).idx(j, i) = match(order).idx(i, j);
                d{order}(j, i) = d{order}(i, j);
            catch
                match(order).idx(i, j) = nan;
                match(order).idx(j, i) = match(order).idx(i, j);
                
                d{order}(i, j) = nan;
                d{order}(j, i) = d{order}(i, j);
            end
            idx = idx + 1;
        end
    end
end
ProgressReport(1, 1);
ProgressReport;
fprintf('\n');

%%
clf
n = 3;
idx1 = GroupsData.index(GroupsData.group == 1);
idx2 = GroupsData.index(GroupsData.group == 2);
IDX = [];
for i=1:n
    id1 = idx1(i);
    for j=1:n
      id2 = idx2(j);
      IDX = [IDX, [id1; id2]];
    end
    for j=i:1:n
      if i==j
          continue;
      end
      id2 = idx1(j);
      IDX = [IDX, [id1; id2]];
    end
    id1 = idx2(i);
    for j=i+1:n
      id2 = idx2(j);
      IDX = [IDX, [id1; id2]];
    end
end
IDX = IDX(:, IDX(1, :) ~= IDX(2, :));
map = GroupsData.group(IDX(1, :)) == GroupsData.group(IDX(2, :));
IDX = [IDX(:, map), IDX(:, ~map)];
%
for i=1:length(IDX)
    SquareSubplpot(length(IDX), i);
    id1 = IDX(1, i);
    id2 = IDX(2, i);
    i1 = min(id1, id2);
    i2 = max(id1, id2);
    [x,y] = ind2sub([size(probs{i}, 1), size(probs{i}, 1)], match(order).idx(i1, i2));
    %%
    x= 2;
    y=2;
    cmap = MyCategoricalColormap;
    for o=order:-1:1
        map = obj.Analysis.Potts.Model{3}.order == o;
        a1 = Vecs{order}{i1}(x, map);
        a2 = Vecs{order}{i2}(y, map);
        %v = ~(a1 == 0 || a2 == 0);
        plot(a1, a2, '.', 'color', cmap(o, :)); hon
        title(sprintf('%d - %d', i1, i2));
    end
    axis equal
    a = axis;
    axis([min(a(1), a(3)) max(a(2), a(4)) min(a(1), a(3)) max(a(2), a(4))]);
    plot([min(a(1), a(3)) max(a(2), a(4))], [min(a(1), a(3)) max(a(2), a(4))], 'k:')
    hoff
end


%%
clf
for order = Orders;
    if length(Orders) > 1 
        subplot(2,2,order);
    else
        subplot(1,1,1);
    end
    %imagesc(d{order})
    MyImagesc(d{order}, true);

    %%
    concise = true(1, size(d{order}, 1));
    for i=1:size(d{order}, 1)
        %%
        myg = GroupsData.group(find(GroupsData.index == i, 1));
        same = d{order}(i, GroupsData.index(GroupsData.group == myg));
        same = same(~isnan(same));
        other = d{order}(i, GroupsData.index(GroupsData.group ~= myg));
        other = other(~isnan(other));
        %concise(i) = quantile(d{order}(i, GroupsData.index(GroupsData.group ~= myg)), .5) >= quantile(d{order}(i, GroupsData.index(GroupsData.group == myg)), .5);
        concise(i) = kstest2(same, other) && median(same) <= median(other);
    end
    
    %%
    prev = 0.5;
    for i=1:GroupsData.nGroups-1
        idx = find(GroupsData.group(1:end-1) == i & GroupsData.group(2:end) == i+1) + .5;
        vert_line(idx, 'color', 'k');
        horiz_line(idx, 'color', 'k');
        %text(length(GroupsData.Experiments)+1, prev+.1, [' ' GroupsData.Titles{i}], 'VerticalAlignment', 'top', 'color', 'k', 'fontweight', 'bold', 'HorizontalAlignment', 'left');
        prev = idx;
    end
    %text(length(GroupsData.Experiments)+1, prev+.1, [' ' GroupsData.Titles{i}], 'VerticalAlignment', 'top', 'color', 'k', 'fontweight', 'bold', 'HorizontalAlignment', 'left');
    set(gca, 'YAxisLocation', 'right', 'YTick', find(diff([0 GroupsData.group])), 'YTickLabel', GroupsData.Titles)
    set(gca, 'XTick', []);
    for i=find(concise)
        text(i, size(d{order}, 1) + 1, '*', 'HorizontalAlignment', 'center', 'FontWeight', 'bold')
    end
    for i=find(~concise)
        %text(i, size(d{order}, 1) + 1, num2str(i), 'HorizontalAlignment', 'center')
    end
    title(['Order ' num2str(order)]);
    axis square
    
end
%saveFigure('Res/SocialPottsDistanceJS');

return;
%%
for order = 1:4;
    subplot(2,2,order);
    vecs = Vecs{order};
    d = zeros(length(vecs));
    for i=1:length(vecs)
        %vecs{i} = vecs{i} ./ repmat(sqrt(sum(vecs{i}.^2, 2)), 1, 640);
        %vecs{i} = vecs{i} - repmat(median(vecs{i}, 2), 1, 640);
    end
    
    for i=1:length(vecs)
        d(i,i) = nan;
        for j=i+1:length(vecs)
            try
                D = mydist2(vecs{i}, vecs{j});
                %D = corr(vecs{i}', vecs{j}');
                d(i, j) = min(D(:));
                d(j, i) = d(i, j);
            catch
                d(i, j) = nan;
                d(j, i) = d(i, j);
            end
        end
    end
    %
    %d = d(1:18,1:18)
    MyImagesc(d)
    prev = 0.5;
    for i=1:GroupsData.nGroups-1
        idx = find(GroupsData.group(1:end-1) == i & GroupsData.group(2:end) == i+1) + .5;
        vert_line(idx, 'color', 'k');
        horiz_line(idx, 'color', 'k');
        %text(length(GroupsData.Experiments)+1, prev+.1, [' ' GroupsData.Titles{i}], 'VerticalAlignment', 'top', 'color', 'k', 'fontweight', 'bold', 'HorizontalAlignment', 'left');
        prev = idx;
    end
    %text(length(GroupsData.Experiments)+1, prev+.1, [' ' GroupsData.Titles{i}], 'VerticalAlignment', 'top', 'color', 'k', 'fontweight', 'bold', 'HorizontalAlignment', 'left');
    set(gca, 'YAxisLocation', 'right', 'YTick', find(diff([0 GroupsData.group])), 'YTickLabel', GroupsData.Titles)
    title(['Order ' num2str(order)]);
end
%%
% D = d;
% i=2;
% G = GroupsData.group;
% while i<size(D, 2)
%     seq = 1:size(D, 2);
%     if isnan(D(i, 1))
%         D = D(seq(seq ~= i), seq(seq ~= i));
%         G = G(seq(seq ~= i));
%     else
%         i = i + 1;
%     end
% end
% m = mdscale(D, 2);
% for g=unique(G)
%     plot(m(:, 1), m(:, 2), '.');
% end