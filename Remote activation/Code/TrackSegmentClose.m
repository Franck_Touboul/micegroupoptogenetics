function TrackSegment(nruns, id)
%%
fprintf('# segmenting movie frames:\n');
if ~license('checkout', 'image_toolbox')
    fprintf('# - waiting for image processing toolbox license\n');
    while ~license('checkout', 'image_toolbox'); pause(5); end
end
TrackDefaults;
%%
fprintf('# segmenting frames\n');
fprintf('# - opening movie file\n');
xyloObj = myMMReader(options.MovieFile);
nFrames = xyloObj.NumberOfFrames;
dt = 1/xyloObj.FrameRate;
%%
fprintf('# - loading meta data\n');
filename = [options.output_path options.test.name '.meta.mat'];
waitforfile(filename);
load(filename);

%%
if nargin == 0
    startframe = 14600;
    endframe = nFrames;
else
    step = floor(nFrames / nruns);
    curr = 0;
    for i=1:id
        prev = curr + 1;
        if i == nruns
            curr = nFrames;
        else
            curr = prev + step - 1;
        end
    end
    startframe = prev;
    endframe = curr;
end
nFrames = endframe - startframe + 1;
%%
prevProps = [];
sx = []; sy = [];

cents.x = zeros(nFrames, options.maxNumCents);
cents.y = zeros(nFrames, options.maxNumCents);
cents.label = zeros(nFrames, options.maxNumCents, 'uint8');
cents.area = zeros(nFrames, options.maxNumCents, 'uint16');
cents.logprob = zeros(nFrames, options.maxNumCents, options.nSubjects);
%%
doubleBkgFrame = im2double(meta.bkgFrame);
bkgNoise = std(doubleBkgFrame(:));
cmap = meta.subject.centerColors;
%%
nchars = RePrintf('# - frame %6d [%d-%d] (%6.2fxiRT)', startframe, startframe, endframe, 0); 
tic;
for r=1:nFrames
    RT = toc / r * xyloObj.FrameRate;
    nchars = RePrintf(nchars, '# - frame %6d [%d-%d] (%6.2fxiRT)', r+startframe-1, startframe, endframe, RT);
    currTime = (r+startframe-1) * dt;
    if isfield(options, 'movieStartTime') && currTime < options.movieStartTime;
        continue;
    end
    if isfield(options, 'movieEndTime') && options.movieEndTime > 0 && currTime > options.movieEndTime
        continue;
    end
    
    m = myMMReader(options.MovieFile, r+startframe-1, meta.bkgFrame);
    if options.output
        orig = m;
    end
    m = imsubtract(m, meta.bkgFrame);
    m = imresize(m, options.scale);
    
    if isempty(sx)
        [sx, sy, nc] = size(m);
    end
    m = im2double(m);
%    m = (m > options.noiseThresh * std(m(:))) .* m;
%     m = (m > options.noiseThresh * bkgNoise) .* m;
    
    %bkgNoise = std(m(:));
    m = (m > options.noiseThresh * bkgNoise) .* m;

    %%
    hsv_m = rgb2hsv(m);
    hm = hsv_m(:,:,1);
    sm = hsv_m(:,:,2);
    vm = hsv_m(:,:,3);
    %%
    bw = logical(sum(m > 0, 3));
    % remove bad frames
    cc = bwconncomp(bw);
    if cc.NumObjects > options.maxNumObjects
        continue;
    end    
    bw = bwareaopen(bw, options.minNumPixels);
    %labels = bwlabel(bw);
    %nobjects = max(labels(:));
    %%
    [b, idx_h] = histc(hm(bw), meta.subject.colorBins);
    [b, idx_s] = histc(sm(bw), meta.subject.colorBins);
    [b, idx_v] = histc(vm(bw), meta.subject.colorBins);
    prob_h = zeros(options.nSubjects, length(idx_h));
    prob_s = zeros(options.nSubjects, length(idx_s));
    prob_v = zeros(options.nSubjects, length(idx_v));
    for i=1:options.nSubjects
        prob_h(i, :) = meta.subject.h(i, idx_h);
        prob_s(i, :) = meta.subject.s(i, idx_s);
        prob_v(i, :) = meta.subject.v(i, idx_v);
    end
    %     prob_h = prob_h ./ repmat(sum(prob_h, 1), options.nSubjects, 1);
    %     prob_s = prob_s ./ repmat(sum(prob_s, 1), options.nSubjects, 1);
    %     prob_v = prob_v ./ repmat(sum(prob_v, 1), options.nSubjects, 1);
    %     joint_prob = prob_h .* prob_s .* prob_v;
    joint_prob = prob_h .* prob_s .* prob_v;
    joint_prob = joint_prob ./ repmat(sum(joint_prob, 1), options.nSubjects, 1);
    
    [m, idx] = max(joint_prob, [], 1);
    nlabels = zeros(sx,sy,'uint8');
    nlabels(bw) = idx;
    
    for k=1:options.nSubjects
        logprobmap{k} = zeros(sx,sy);
        logprobmap{k}(bw) = flog(joint_prob(k, :));
    end
    
    flabels = zeros(sx,sy,'uint8');
    %%
    centIndex = 1;
    for i=1:options.nSubjects
        %%
        %curr = bwareaopen(curr, options.minNumPixels);
        %reg = bwconncomp(imclose(nlabels == i, strel('disk', options.minSegmentGap)));
        reg = bwconncomp(nlabels == i);
        %        reg = bwconncomp(curr);
        %conn = regionprops(reg, 'Solidity', 'MajorAxisLength', 'MinorAxisLength');
        %conn = regionprops(reg, 'Solidity', 'Centroid');
        for j=1:length(reg.PixelIdxList)
            %             if length(reg.PixelIdxList{j}) < options.minNumPixels
            %                 reg.PixelIdxList{j} = [];
            %             end
            if length(reg.PixelIdxList{j}) < options.minNumPixels
                reg.PixelIdxList{j} = [];
            else
                zerobkg = false(sx,sy);
                zerobkg(reg.PixelIdxList{j}) = true;
                [zx, zy] = find(imclose(zerobkg, strel('disk', 10)));
                cents.x(r, centIndex) = mean(zx);
                cents.y(r, centIndex) = mean(zy);
                
                cents.label(r, centIndex) = i;
                cents.area(r, centIndex) = length(reg.PixelIdxList{j});
                for k=1:options.nSubjects
                    cents.logprob(r, centIndex, k) = sum(logprobmap{k}(reg.PixelIdxList{j}));
                end
                cents.logprob(r, centIndex, :) = cents.logprob(r, centIndex, :) - flog(sum(exp(cents.logprob(r, centIndex, :))));
                centIndex = centIndex + 1;
                if centIndex > options.maxNumCents
                    break;
                end
            end
            %             if conn(j).Solidity < .5 || ...
            %                     length(reg.PixelIdxList{j}) < options.minNumPixels || ...
            %                     length(reg.PixelIdxList{j}) / (conn(j).MajorAxisLength * conn(j).MinorAxisLength * pi/4) < 0.5
            %                 reg.PixelIdxList{j} = [];
            %             end
        end
        if centIndex > options.maxNumCents
            cents.label(r, :) = 0;
        end

        if options.output
            img = labelmatrix(reg);
            if 1 == 2
                subplot(1,2,i);
                imagesc(img > 0);
            end
            flabels(img > 0) = i;
        end
    end
    %%
    if options.output
        subplot(1,2,2);
        rgblbls = label2rgb(flabels, cmap);
        imagesc(rgblbls);
        subplot(1,2,1);
        imagesc(orig);
        drawnow
    end
    %%
end
fprintf('\n');
fprintf(['# - total time: ' sec2time(toc) '\n']);
%
cents.startframe = startframe;
cents.endframe   = endframe;
%
if nargin == 0
    filename = [options.output_path options.test.name '.segm.mat'];
else
    filename = [options.output_path options.test.name '.segm.' sprintf('%03d', id) '.mat'];
end
fprintf(['# - saving segmentation: "' filename '"']);
save(filename, 'cents');

