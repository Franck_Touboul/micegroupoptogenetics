function seg = SimpleSegmentFrame(obj, frame)
if isscalar(frame)
    img = obj.GetFrame(frame);
else
    img = frame;
end
%%
m = imsubtract(img, obj.BkgImage);
m = imresize(m, obj.VideoScale);
%%
hsv_m = rgb2hsv(m);
vm = hsv_m(:,:,3);
%%
meanBKG = mean(vm(:));
stdBKG = std(vm(:));
if obj.UseAdaptiveThresh
    upper = obj.NoiseThresh;
    lower = 1;
    prev_thresh = round((upper + lower)/2);
    while true
        thresh = round((upper + lower)/2);
        bw = vm > meanBKG + thresh * stdBKG;
        if ~isempty(obj.ValidROI)
            bw(~obj.ValidROI) = false;
        end
        cc = bwconncomp(bw);
        if cc.NumObjects < obj.MaxNumOfObjects
            upper = thresh - 1;
            prev_thresh = thresh;
        else
            lower = thresh + 1;
        end
        if lower > upper
            break
        end
    end
    if thresh ~= prev_thresh
        thresh = prev_thresh;
        bw = vm > meanBKG + thresh * stdBKG;
    end
else
    thresh = obj.NoiseThresh;
    bw = vm > meanBKG + thresh * stdBKG;
    cc = bwconncomp(bw);
    if cc.NumObjects > obj.MaxNumOfObjects
        return;
    end
end
seg = bwareaopen(bw, obj.MinNumOfPixels);
