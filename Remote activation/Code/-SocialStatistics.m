%  Social behavior code - agrresive/non-aggresive

options.nSubjects = 4; % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts

% 1) Preparing the data matrix for work
% 2) Calculating contacts
% 3) Finding "islands" of "1" or "11" (false contacts)
% 4) Unifying contacts into "engagements" by canceling small gaps
% 5) Creating a cell array of engagements and their respective raw data
% 6) Characterizing the engagements (aggresive or non-aggresive)
% 7) Plots

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1)                 Preparing the data matrix for work
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Loading the Excel file for all mice (if needed)
loadData;
% fprintf('# loading data: \n');
% if (~exist('data'))
%     orig.x = [];
%     orig.y = [];
%     orig.distance = [];
%     orig.velocity = [];
%     
%     zones.labyrinth = [];
%     zones.bigNest = [];
%     zones.smallNest = [];
%     zones.saftyStrip = [];
%     zones.upperSmallNest = [];
%     
%     for i=1:options.nSubjects
%         fprintf('# - subject %d/%d\n', i, options.nSubjects);
%         data = xlsread(['subject ' num2str(i)])';
%         %
%         orig.time       = data(1, :);
%         orig.x          = [orig.x; data(3, :)];
%         orig.y          = [orig.y; data(4, :)];
%         orig.distance   = [orig.distance; data(8, :)];
%         orig.velocity   = [orig.velocity; data(9, :)];
%         
%         zones.labyrinth   = [zones.labyrinth; data(13, :)];
%         zones.bigNest     = [zones.bigNest; data(14, :)];
%         zones.smallNest   = [zones.smallNest; data(16, :)];
%         zones.saftyStrip  = [zones.saftyStrip; data(21, :)];
%         zones.upperSmallNest = [zones.upperSmallNest; data(22, :)];
%     end
%     orig.nData = length(orig.time);
%     zones.all = zones.labyrinth * 0;
%     zones.all(zones.labyrinth       > 0) = 1;
%     zones.all(zones.bigNest         > 0) = 2;
%     zones.all(zones.smallNest       > 0) = 3;
%     zones.all(zones.saftyStrip      > 0) = 4;
%     zones.all(zones.upperSmallNest  > 0) = 5;
%     zones.count = 6;
%     fprintf('# - done\n');
% else
%     fprintf('# - data already loaded\n');
% end

%% compute independent probabilities
independentProbs = computeIndependentProbs(zones.all, zones.count);

%% compute joint probabilities
jointProbs = computeJointProbs(zones.all, zones.count);

%% compute probability for each sample
% independent
sampleProbIndep = ones(1, orig.nData);
for i=1:options.nSubjects
    currProbs = independentProbs(i, :);
    sampleProbIndep = sampleProbIndep .* currProbs(zones.all(i, :) + 1);
end
% joint
baseVector = zones.count.^[0:options.nSubjects-1];
h = baseVector * zones.all + 1;
sampleProbJoint = jointProbs(h);

%% 
% %plot(log(sampleProbJoint), log(sampleProbIndep), '.');
% subplot(3,2,1);
% loglog(sampleProbJoint, sampleProbIndep, '.');
% minLog = floor(log10(min(min(sampleProbJoint), min(sampleProbIndep))));
% maxLog = ceil(log10(max(max(sampleProbJoint), max(sampleProbIndep))));
% axis([10^minLog 10^maxLog 10^minLog 10^maxLog]);
% line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);
% title('Original Data');
% xlabel('Joint');
% ylabel('Independent');

%%
subplot(4,2,1);
%%
%subplot(2,1,1);
h = zeros(options.nSubjects, zones.count);
for i=1:options.nSubjects
    for j=1:zones.count
        h(i, j) = sum(zones.all(i, :) == j - 1);
    end
end
h = h / size(zones.all, 2);
barweb(h', h'*0, [], zones.labels, [], 'Zones', 'Probability');

prettyPlot('', 'Zones', 'Probability');
%%
subplot(4,2,3);
jumbledZones = zones.all * 0; 
for i=1:options.nSubjects
    jumbledZones(i, :) = zones.all(i, randperm(orig.nData));
end
[jSampleProbIndep, jSampleProbJoint] = computeSampleProb(jumbledZones, zones.count);
%%
%subplot(2,2,1);
b1 = sampleProbJoint;
b2 = sampleProbIndep;

p1 = jSampleProbJoint;
p2 = jSampleProbIndep;
loglog(b1, b2, '.', 'Color', [.6 .6 .6]);
hold on;
loglog(p1, p2, '.');
hold off;

% minLog = floor(log10(min(min(jSampleProbJoint), min(jSampleProbIndep))));
% maxLog = ceil(log10(max(max(jSampleProbJoint), max(jSampleProbIndep))));
% axis([10^minLog 10^maxLog 10^minLog 10^maxLog]);

minLog = floor(log10(min(min(jSampleProbJoint), min(jSampleProbIndep))));
maxLog = ceil(log10(max(max(jSampleProbJoint), max(jSampleProbIndep))));
axis([...s
    10^floor(log10(min(p1))) ...
    10^floor(log10(max(p1))) ...
    10^floor(log10(min(p2))) ...
    10^floor(log10(max(p2))) ...
    ]);

line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);

% title('Permuted Samples');
% xlabel('Joint');
% ylabel('Independent');
prettyPlot('', 'Joint', 'Independent');
legend('Original', 'Permuted', 'location', 'SouthEast'); legend boxoff

% sampleVariance = [];
% for u=unique(jSampleProbJoint)
%     p = (jSampleProbJoint == u);
%     sampleVariance = [sampleVariance, mean((jSampleProbIndep(p) - jSampleProbJoint(p)).^2)];
% end

%%
subplot(4,2,4);
jumbledZones = zones.all * 0; 
for i=1:options.nSubjects
    jumbledZones(i, :) = zones.all(i, randperm(orig.nData));
end

[jSampleProbIndep, jSampleProbJoint] = computeSampleProb(jumbledZones, zones.count);
[jSampleProbIndepPrev, jSampleProbJointPrev] = computeSampleProb(jumbledZones, zones.count, independentProbs, jointProbs);
%%
%subplot(2,2,1);
p1 = jSampleProbJoint;
p2 = jSampleProbJointPrev;
loglog(p1, p2, '.');
hold off;

minLog = floor(log10(min(min(jSampleProbJoint), min(jSampleProbIndep))));
maxLog = ceil(log10(max(max(jSampleProbJoint), max(jSampleProbIndep))));
axis([...s
    10^floor(log10(min(p1))) ...
    10^floor(log10(max(p1))) ...
    10^floor(log10(min(p2))) ...
    10^floor(log10(max(p2))) ...
    ]);

line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);

% title('Permuted Samples');
% xlabel('Joint (permuted)');
% ylabel('Joint (original)');
prettyPlot('', 'Joint (permuted)', 'Joint (original)');
legend('Original', 'Permuted', 'location', 'SouthEast'); legend boxoff


%%
subplot(4,2,5);
jumbledZones = zones.all * 0; 
for i=1:options.nSubjects
    jumbledZones(i, :) = Cyclic(randi(orig.nData), zones.all(i, :));
end
[jSampleProbIndep, jSampleProbJoint] = computeSampleProb(jumbledZones, zones.count);
%%
%subplot(2,2,1);
b1 = sampleProbJoint;
b2 = sampleProbIndep;

p1 = jSampleProbJoint;
p2 = jSampleProbIndep;
loglog(b1, b2, '.', 'Color', [.6 .6 .6]);
hold on;
loglog(p1, p2, '.');
hold off;

minLog = floor(log10(min(min(jSampleProbJoint), min(jSampleProbIndep))));
maxLog = ceil(log10(max(max(jSampleProbJoint), max(jSampleProbIndep))));
axis([...s
    10^floor(log10(min(p1))) ...
    10^floor(log10(max(p1))) ...
    10^floor(log10(min(p2))) ...
    10^floor(log10(max(p2))) ...
    ]);

line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);

% title('Permuted Samples');
% xlabel('Joint');
% ylabel('Independent');
prettyPlot('', 'Joint', 'Independent');
legend('Original', 'Permuted', 'location', 'SouthEast'); legend boxoff

%%
jumbledZones = zones.all * 0; 
for i=1:options.nSubjects
    jumbledZones(i, :) = Cyclic(randi(orig.nData), zones.all(i, :));
end
[jSampleProbIndep, jSampleProbJoint] = computeSampleProb(jumbledZones, zones.count);
[jSampleProbIndepPrev, jSampleProbJointPrev] = computeSampleProb(jumbledZones, zones.count, independentProbs, jointProbs);
%
subplot(4,2,6);
loglog(jSampleProbJoint, jSampleProbJointPrev, 'b.');
% 
minLog = floor(log10(min(min(jSampleProbJoint), min(jSampleProbJoint))));
maxLog = ceil(log10(max(max(jSampleProbJoint), max(jSampleProbJoint))));
axis([10^minLog 10^maxLog 10^minLog 10^maxLog]);

line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);
% 
title('Permuted Samples (kept temporal structure)');
xlabel('Joint (permuted)');
ylabel('Joint (original)');

%%
perm = randperm(options.nSubjects);
jumbledZones = zones.all * 0; 
for i=1:options.nSubjects
    jumbledZones(i, :) = zones.all(perm(i), :);
end
[jSampleProbIndep, jSampleProbJoint] = computeSampleProb(jumbledZones, zones.count);
%
subplot(4,2,7);
loglog(sampleProbJoint, sampleProbIndep, '.', 'Color', [.6 .6 .6]);
hold on;
loglog(jSampleProbJoint, jSampleProbIndep, 'b.');
hold off;

minLog = floor(log10(min(min(jSampleProbJoint), min(jSampleProbIndep))));
maxLog = ceil(log10(max(max(jSampleProbJoint), max(jSampleProbIndep))));
axis([10^minLog 10^maxLog 10^minLog 10^maxLog]);
line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);

title('Permuted Mice');
xlabel('Joint');
ylabel('Independent');
legend('Original', 'Permuted', 'location', 'NorthWest'); legend boxoff

%%
subplot(4,2,8);
perm = randperm(options.nSubjects);
jumbledZones = zones.all * 0; 
for i=1:options.nSubjects
    jumbledZones(i, :) = zones.all(perm(i), :);
end
[jSampleProbIndep, jSampleProbJoint] = computeSampleProb(jumbledZones, zones.count);
[jSampleProbIndepPrev, jSampleProbJointPrev] = computeSampleProb(jumbledZones, zones.count, independentProbs, jointProbs);
%%
%subplot(2,2,1);

p1 = jSampleProbJoint;
p2 = jSampleProbJointPrev;
loglog(p1, p2, '.');
hold off;

minLog = floor(log10(min(min(jSampleProbJoint), min(jSampleProbIndep))));
maxLog = ceil(log10(max(max(jSampleProbJoint), max(jSampleProbIndep))));
axis([...s
    10^floor(log10(min(p1))) ...
    10^floor(log10(max(p1))) ...
    10^floor(log10(min(p2))) ...
    10^floor(log10(max(p2))) ...
    ]);

line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);

% title('Permuted Samples');
% xlabel('Joint (permuted)');
% ylabel('Joint (original)');
prettyPlot('', 'Joint (permuted)', 'Joint (original)');
legend('Original', 'Permuted', 'location', 'SouthEast'); legend boxoff
