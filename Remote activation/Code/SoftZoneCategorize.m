function [obj, names] = SoftZoneCategorize(obj)
classes = {'Food', 'Water', 'Shelter', 'High', 'High', 'High'};
%classes = {'Food', 'Food', 'Shelter', '', '', ''};
match = {'Feeder', 'Water', '\(.*\)', 'SmallNest', 'BigNest', 'Block'};

% classes = {'Food', 'Food', 'Shelter', ''};
% match = {'Feeder', 'Water', '\(.*\)'};

uclass = {};
uclass{1} = classes{1};
for i=1:length(classes)
    if ~any(strcmp(classes{i}, uclass))
        uclass = [uclass, classes{i}];
    end
end
% = unique(classes);
if isstruct(obj)
    obj.ROI.CategoryNames = {'None', uclass{:}};
    obj.category = obj.zones * 0 + 1;
    obj.ROI.nCategories = length(obj.ROI.CategoryNames);
    for i=1:length(obj.ROI.ZoneNames)
        q = regexpi(obj.ROI.ZoneNames{i}, match);
        for j=1:length(q)
            if ~isempty(q{j})
                for k=1:length(obj.ROI.CategoryNames)
                    if strcmp(obj.ROI.CategoryNames{k}, classes{j});
                        obj.category(obj.zones == i) = k;
                        break;
                    end
                end
                break;
            end
        end
    end
else
    ZoneNames = obj;
    category = ones(1, length(ZoneNames));
    names = {'None', uclass{:}};
    for i=1:length(ZoneNames)
        q = regexpi(ZoneNames{i}, match);
        for j=1:length(q)
            if ~isempty(q{j})
                for k=1:length(names)
                    if strcmp(names{k}, classes{j});
                        category(i) = k;
                        break;
                    end
                end
                break;
            end
        end
    end
    obj = category;
end

