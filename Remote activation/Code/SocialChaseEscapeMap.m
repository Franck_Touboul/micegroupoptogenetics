SocialExperimentData;
%%
for id=1:GroupsData.nExperiments
    %%
    obj = TrackLoad({id, 2}, {'Hierarchy'});
    if id == 1
        data = obj.Hierarchy.Group;
    else
        data(id) = obj.Hierarchy.Group;
    end
end
clf
%%

subplot(4,2,1:4)
for g=1:GroupsData.nGroups
    color = GroupsData.Colors(g, :);
    style = {'Color',  color, 'MarkerFaceColor', color, 'MarkerEdgeColor', 'none', 'MarkerSize', 20, 'LineWidth', 2};
    plot(inf, inf, 'o', style{:});
    hon;
end
for id=1:GroupsData.nExperiments
    color = GroupsData.Colors(GroupsData.group(id), :);
    %%
    prev = [];
    pChase = sum(data(id).ChaseEscape, 2) ./ sum(data(id).Contacts, 2) * 100;
    pEscape = sum(data(id).ChaseEscape, 1) ./ sum(data(id).Contacts, 1) * 100;
    for i=GroupsData.nSubjects:-1:1
        map = max(data(id).rank) - data(id).rank + 1 == i;
        idx = find(map);
        if ~isempty(idx)
            [currPChase, order] = sort(pChase(map));
            currPEscape = pEscape(map);
            currPEscape = currPEscape(order);
            idx = idx(order);
            style = {'Color',  color, 'MarkerFaceColor', color, 'MarkerEdgeColor', 'none', 'MarkerSize', 20, 'LineWidth', 2};
            plot(currPChase, currPEscape, 'o:', style{:}); hon;
            text(currPChase, currPEscape, num2str(i), 'VerticalAlignment', 'middle' ,'HorizontalAlignment', 'center', 'Color', 'w', 'FontSize', 10, 'FontWeight', 'bold');
            if ~isempty(prev)
                plot([pChase(prev) currPChase(1)], [pEscape(prev) currPEscape(1)], 'o-', style{:}, 'MarkerFaceColor', 'none'); hon;
            end
            prev = idx(end);
        end
    end
    drawnow
end
xlabel('contact ended with a chase [%]');
ylabel('contact ended with an escape [%]');
legend(GroupsData.Titles);
legend boxoff
hoff
%% % Chase Escape
pos = 1;
Stat = struct();
for g=1:GroupsData.nGroups
    f = GroupsData.index(GroupsData.group == g);
    for i=1:length(f)
        id = f(i);
        for d=1:GroupsData.nDays
            Stat(g).val(i, d) = sum(data(id).Days(d).ChaseEscape(:)) / sum(data(id).Days(d).Contacts(:)/2) * 100;
        end
        Stat(g).total(i) = sum(data(id).ChaseEscape(:)) / sum(data(id).Contacts(:)/2) * 100;
    end
    subplot(4,6,13 + (pos - 1) * 3:14 + (pos - 1) * 3);
    errorbar(1:GroupsData.nDays, mean(Stat(g).val), stderr(Stat(g).val), 'LineWidth', 2, 'Color', GroupsData.Colors(g, :));
    hon;
    set(gca, 'XTick', 1:GroupsData.nDays);
    subplot(4,6,15 + (pos - 1) * 3);
    OneBar(g, mean(Stat(g).total), GroupsData.Colors(g, :), stderr(Stat(g).total)); hon;
    set(gca, 'XTick', 1:GroupsData.nGroups);
end
subplot(4,6,15 + (pos - 1) * 3);
a1 = axis;
hoff
subplot(4,6,13 + (pos - 1) * 3:14 + (pos - 1) * 3);
    xlabel('day');
    ylabel('probability of chase-escape [%]');
a2 = axis;
axis([a2(1) a2(2) min(a1(3), a2(3)) max(a1(4), a2(4))]);
hoff
subplot(4,6,15 + (pos - 1) * 3);
axis([a1(1) a1(2) min(a1(3), a2(3)) max(a1(4), a2(4))]);
if ttest2(Stat(1).total, Stat(2).total)
    a1 = axis;
    text(1.5, a1(3) + (a1(4) - a1(3)) * .8, '*');
end

%% no of contacts per hour
pos = 2;
Stat = struct();
for g=1:GroupsData.nGroups
    f = GroupsData.index(GroupsData.group == g);
    for i=1:length(f)
        id = f(i);
        for d=1:GroupsData.nDays
            Stat(g).val(i, d) = sum(data(id).Days(d).Contacts(:)/2) / data(id).Days(d).RecordingDurationInHours;
        end
        Stat(g).total(i) = sum(data(id).Contacts(:)/2) / data(id).RecordingDurationInHours;
    end
    subplot(4,6,13 + (pos - 1) * 3:14 + (pos - 1) * 3);
    errorbar(1:GroupsData.nDays, mean(Stat(g).val), stderr(Stat(g).val), 'LineWidth', 2, 'Color', GroupsData.Colors(g, :));
    hon;
    set(gca, 'XTick', 1:GroupsData.nDays);
    subplot(4,6,15 + (pos - 1) * 3);
    OneBar(g, mean(Stat(g).total), GroupsData.Colors(g, :), stderr(Stat(g).total)); hon;
    set(gca, 'XTick', 1:GroupsData.nGroups);
end
subplot(4,6,15 + (pos - 1) * 3);
a1 = axis;
hoff
subplot(4,6,13 + (pos - 1) * 3:14 + (pos - 1) * 3);
xlabel('day');
ylabel('number of contacts per hour');
a2 = axis;
axis([a2(1) a2(2) min(a1(3), a2(3)) max(a1(4), a2(4))]);
hoff
subplot(4,6,15 + (pos - 1) * 3);
axis([a1(1) a1(2) min(a1(3), a2(3)) max(a1(4), a2(4))]);
if ttest2(Stat(1).total, Stat(2).total)
    a1 = axis;
    text(1.5, a1(3) + (a1(4) - a1(3)) * .8, '*');
end

%% no of chase-escape
pos = 3;
Stat = struct();

for g=1:GroupsData.nGroups
    f = GroupsData.index(GroupsData.group == g);
    for i=1:length(f)
        id = f(i);
        for d=1:GroupsData.nDays
            Stat(g).val(i, d) = sum(data(id).Days(d).ChaseEscape(:)) / data(id).Days(d).RecordingDurationInHours;
        end
        Stat(g).total(i) = sum(data(id).ChaseEscape(:)) / data(id).RecordingDurationInHours;
    end
    subplot(4,6,13 + (pos - 1) * 3:14 + (pos - 1) * 3);
    errorbar(1:GroupsData.nDays, mean(Stat(g).val), stderr(Stat(g).val), 'LineWidth', 2, 'Color', GroupsData.Colors(g, :));
    hon;
    set(gca, 'XTick', 1:GroupsData.nDays);
    subplot(4,6,15 + (pos - 1) * 3);
    OneBar(g, mean(Stat(g).total), GroupsData.Colors(g, :), stderr(Stat(g).total)); hon;
    set(gca, 'XTick', 1:GroupsData.nGroups);
end
subplot(4,6,15 + (pos - 1) * 3);
a1 = axis;
hoff
subplot(4,6,13 + (pos - 1) * 3:14 + (pos - 1) * 3);
xlabel('day');
ylabel('number of chase-escape');
a2 = axis;
axis([a2(1) a2(2) min(a1(3), a2(3)) max(a1(4), a2(4))]);
hoff
subplot(4,6,15 + (pos - 1) * 3);
axis([a1(1) a1(2) min(a1(3), a2(3)) max(a1(4), a2(4))]);
if ttest2(Stat(1).total, Stat(2).total)
    a1 = axis;
    text(1.5, a1(3) + (a1(4) - a1(3)) * .8, '*');
end

