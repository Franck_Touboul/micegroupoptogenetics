SocialExperimentData
%%
Profile = struct;
Profile.GroupNames = GroupsData.Types;
%%
midx = 0;
for i=1:GroupsData.nExperiments
    fprintf('# experiment no. %d of %d\n', i, GroupsData.nExperiments);
    try
        objs = cell(1, GroupsData.nDays);
        for d=1:GroupsData.nDays
            fprintf('# - loading day %d of %d\n', d, GroupsData.nDays);
            obj = TrackLoad({i d});
            objs{d} = obj;
        end
        
        %% common
        pidx = 1;
        for s=1:obj.nSubjects
            Profile.Group(midx + s) = GroupsData.group(i);
            Profile.GroupName{midx + s} = obj.FilePrefix;
            Profile.Experiment(midx + s) = i;
            Profile.ID(midx + s) = i;
        end
        
        %% Social rank
        Profile.Individual.Fields{pidx} = 'Social-rank';
        r = max(obj.Hierarchy.Group.AggressiveChase.rank) - obj.Hierarchy.Group.AggressiveChase.rank + 1;
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = r(s);
        end
        pidx = pidx + 1;
        %% David Score
        [NormDSonDij, NormDSonPij] = SocialDavidScore(obj);
        Profile.Individual.Fields{pidx} = 'David-Score';
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = NormDSonDij(s);
        end
        pidx = pidx + 1;
        %% Total time
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('total-time{%d}', day);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = sum(objs{day}.valid) / objs{day}.FrameRate / 60/ 60;
            end
            pidx = pidx + 1;
        end
        %% Time outside
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('time-outside{%d}', day);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = sum(objs{day}.valid & ~objs{day}.sheltered(s, :)) / objs{day}.FrameRate / 60/ 60;
            end
            pidx = pidx + 1;
        end
        %% number of ranks
        ce = zeros(obj.nSubjects);
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('hier-levels{%d}', day);
            ce = ce + obj.Hierarchy.Group.AggressiveChase.Days(day).ChaseEscape;
            mat= ce - ce';
            mat(binotest(ce, ce + ce')) = 0;
            mat = mat .* (mat > 0);
            
            [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = length(unique(rank));
            end
            pidx = pidx + 1;
        end
        %% Elo Stability
        Elo = SocialEloRating(objs);
        for day=2:4
            Profile.Individual.Fields{pidx} = sprintf('elo-change{%d}', day);
            prev = find(Elo.Day == day-1, 1, 'last');
            curr = find(Elo.Day == day, 1, 'last');
            e = Elo.Scores(:, curr) - Elo.Scores(:, prev);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = e(s);
            end
            pidx = pidx + 1;
        end

      
        %% rank stability
        chgRatio = SocialHierarchyStability(objs{1});
        for day=2:4
            Profile.Individual.Fields{pidx} = sprintf('hier-change{%d}', day);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = chgRatio(day-1);
            end
            pidx = pidx + 1;
        end        
        
        %% % time outside
        totalTime = 0;
        timeOutside = zeros(1, 4);
        for day=1:4
            for s=1:obj.nSubjects
                Profile.Individual.Fields{pidx} = sprintf('%%outside{%d}', day);
                Profile.Individual.Table(midx + s, pidx) = 1 - sum(objs{day}.sheltered(s, objs{day}.valid)) / sum(objs{day}.valid);
                timeOutside(s) = timeOutside(s) + sum(~objs{day}.sheltered(s, objs{day}.valid));
            end
            totalTime = totalTime + sum(objs{day}.valid);
            pidx = pidx + 1;
        end
        Profile.Individual.Fields{pidx} = sprintf('%%outside-4days');
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = timeOutside(s) / totalTime;
        end
        pidx = pidx + 1;
        %% % contacts
        for day=1:4
          	Profile.Individual.Fields{pidx} = sprintf('#contacts{%d}', day);
            p = sum(obj.Hierarchy.Group.AggressiveChase.Days(day).Contacts, 2);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = p(s);
            end
            pidx = pidx + 1;
        end
        %% time in contact
        for day=1:4
          	Profile.Individual.Fields{pidx} = sprintf('%%in-contact{%d}', day);
            subj = [objs{day}.Contacts.List.subjects];
            begf = [objs{day}.Contacts.List.beg];
            begf = min(begf);
            endf = [objs{day}.Contacts.List.end];
            endf = max(endf);
            for s=1:obj.nSubjects
                m = any(subj == s);
                len = endf(m) - begf(m) + 1;
                Profile.Individual.Table(midx + s, pidx) = sum(len) / sum(obj.valid);
            end
            pidx = pidx + 1;
        end        

        %% median time in contact
        for day=1:4
          	Profile.Individual.Fields{pidx} = sprintf('contact-duration{%d}', day);
            subj = [objs{day}.Contacts.List.subjects];
            begf = [objs{day}.Contacts.List.beg];
            begf = min(begf);
            endf = [objs{day}.Contacts.List.end];
            endf = max(endf);
            for s=1:obj.nSubjects
                m = any(subj == s);
                len = endf(m) - begf(m) + 1;
                Profile.Individual.Table(midx + s, pidx) = median(len) / obj.FrameRate;
            end
            pidx = pidx + 1;
        end        
        
        %% % contacts end in chase
        for day=1:4
          	Profile.Individual.Fields{pidx} = sprintf('#chases{%d}', day);
            p = sum(obj.Hierarchy.Group.AggressiveChase.Days(day).ChaseEscape, 2);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = p(s);
            end
            pidx = pidx + 1;
        end
        %% % contacts end in escape
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('#escapes{%d}', day);
            p = sum(obj.Hierarchy.Group.AggressiveChase.Days(day).ChaseEscape, 1);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = p(s);
            end
            pidx = pidx + 1;
        end
        %% approach
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('#approach{%d}', day);
            c = zeros(obj.nSubjects);
            for l=1:length(objs{day}.Contacts.List)
                curr = objs{day}.Contacts.List(l);
                s1 = curr.subjects(1);
                s2 = curr.subjects(2);
                c(s1, s2) = c(s1, s2) + curr.states(1, 2);
                c(s2, s1) = c(s2, s1) + curr.states(2, 2);
            end
            p = sum(c, 2);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = p(s);
            end
            pidx = pidx + 1;
        end
        %% no. approaches to similar
        [NormDSonDij, NormDSonPij] = SocialDavidScore(obj);
        [~, rank] = sort(NormDSonDij);
        map = false(obj.nSubjects);
        for s1=1:obj.nSubjects
            for s2=1:obj.nSubjects
                map(s1, s2) = abs(rank(s1) - rank(s2)) <= 1;
            end
        end
        for day = 1:4;
            Profile.Individual.Fields{pidx} = sprintf('#approachs-similar{%d}', day);
            c = zeros(obj.nSubjects);
            for l=1:length(objs{day}.Contacts.List)
                curr = objs{day}.Contacts.List(l);
                s1 = curr.subjects(1);
                s2 = curr.subjects(2);
                c(s1, s2) = c(s1, s2) + curr.states(1, 2);
                c(s2, s1) = c(s2, s1) + curr.states(2, 2);
            end
            p = sum(c.*map, 2);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = p(s);
            end
            pidx = pidx + 1;
        end
        %% being approach
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('#being-approach{%d}', day);
            c = zeros(obj.nSubjects);
            for l=1:length(objs{day}.Contacts.List)
                curr = objs{day}.Contacts.List(l);
                s1 = curr.subjects(1);
                s2 = curr.subjects(2);
                c(s1, s2) = c(s1, s2) + curr.states(1, 2);
                c(s2, s1) = c(s2, s1) + curr.states(2, 2);
            end
            p = sum(c, 1)';
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = p(s);
            end
            pidx = pidx + 1;
        end
        %% entropy
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('entropy{%d}', day);
            p = zeros(obj.nSubjects, obj.ROI.nZones);
            for s=1:obj.nSubjects
                p(s, :) = p(s, :) + histc(objs{day}.zones(s, objs{day}.valid), 1:objs{day}.ROI.nZones);
            end
            for s=1:obj.nSubjects
                h = p(s, :) / sum(p(s, :));
                Profile.Individual.Table(midx + s, pidx) = -h * log2(h' + (h' == 0));
            end
            pidx = pidx + 1;
        end
        %% % time near food or water
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('near-food{%d}', day);
            for s=1:obj.nSubjects
                z = objs{day}.zones(s, objs{day}.valid);
                near = sum(z == 2 | z == 3 | z == 4);
                total = sum(objs{day}.valid);
                Profile.Individual.Table(midx + s, pidx) = near / total;
            end
            pidx = pidx + 1;
        end
        %% % time high
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('elevated{%d}', day);
            for s=1:obj.nSubjects
                z = objs{day}.zones(s, objs{day}.valid);
                high = sum(z == 5 | z == 8 | z == 10);
                total = sum(objs{day}.valid);
                
                Profile.Individual.Table(midx + s, pidx) = high / total;
            end
            pidx = pidx + 1;
        end
        %% % time alone outside
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('alone{%d}', day);
            
            for s=1:obj.nSubjects
                other = 1:obj.nSubjects;
                other = other(other ~= s);
                p = sum(all(objs{day}.sheltered(other, objs{day}.valid)) & ~objs{day}.sheltered(s, objs{day}.valid));
                total = sum(objs{day}.valid);
                
                Profile.Individual.Table(midx + s, pidx) = p / total;
            end
            pidx = pidx + 1;
        end

        %% median speed
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('median-speed{%d}', day);
            for s=1:obj.nSubjects
                speed = objs{day}.speedCMperSec(s, objs{day}.valid & ~objs{day}.sheltered(s, :));
                
                Profile.Individual.Table(midx + s, pidx) =  nanmedian(speed);
            end
            pidx = pidx + 1;
        end
        
        %% Distance
        jumpTime = 3; %% sec
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('distance{%d}', day);
            jumpFrames = jumpTime * objs{day}.FrameRate;
            for s=1:obj.nSubjects
                xcm = objs{day}.x(s, objs{day}.valid) / objs{day}.PixelsPerCM;
                ycm = objs{day}.y(s, objs{day}.valid) / objs{day}.PixelsPerCM;
                d = sqrt(...
                    (xcm(1:end-jumpFrames+1) - xcm(jumpFrames:end)).^2 + ...
                    (ycm(1:end-jumpFrames+1) - ycm(jumpFrames:end)).^2);
                Profile.Individual.Table(midx + s, pidx) = sum(d);
            end
            pidx = pidx + 1;
        end
        %% multi-info
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('mutli-info{%d}', day);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = objs{day}.Analysis.Potts.In;
            end
            pidx = pidx + 1;
        end

        %% explained
        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('explained-pairs{%d}', day);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = objs{day}.Analysis.Potts.Ik(1) / objs{day}.Analysis.Potts.In;
            end
            pidx = pidx + 1;
        end

        for day=1:4
            Profile.Individual.Fields{pidx} = sprintf('explained-tripltes{%d}', day);
            for s=1:obj.nSubjects
                Profile.Individual.Table(midx + s, pidx) = (objs{day}.Analysis.Potts.Ik(1) + objs{day}.Analysis.Potts.Ik(2)) / objs{day}.Analysis.Potts.In;
            end
            pidx = pidx + 1;
        end
        
    catch e
        fprintf('# !!!!!!! ERROR\n', i, GroupsData.nExperiments);
        return
        warning(e.message);
    end
    %%
    midx = midx + obj.nSubjects;
end

return

SocialExperimentData
%%
Profile = struct;
Profile.TypeNames = GroupsData.Types;
%%
midx = 0;
for i=1:GroupsData.nExperiments
    fprintf('# experiment no. %d of %d\n', i, GroupsData.nExperiments);
    
    try
        objs = cell(1, GroupsData.nDays);
        for d=1:GroupsData.nDays
            fprintf('# - loading day %d of %d\n', d, GroupsData.nDays);
            obj = objs{i, d};%TrackLoad({i d});
            objs{d} = obj;
        end
        
        %% common
        pidx = 1;
        for s=1:obj.nSubjects
            Profile.Type(midx + s) = GroupsData.group(i);
            Profile.Group(midx + s) = i;
        end
        
        %% Social rank
        Profile.Individual.Fields{pidx} = 'Social rank';
        r = max(obj.Hierarchy.Group.AggressiveChase.rank) - obj.Hierarchy.Group.AggressiveChase.rank + 1;
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = r(s);
        end
        pidx = pidx + 1;
        %% David Score
        [NormDSonDij, NormDSonPij] = SocialDavidScore(obj);
        Profile.Individual.Fields{pidx} = 'David Score';
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = NormDSonDij(s);
        end
        pidx = pidx + 1;
        %% % time in sheleter
        Profile.Individual.Fields{pidx} = '% in shelter';
        for s=1:obj.nSubjects
            total = 0;
            sheltered = 0;
            for d=1:4
                sheltered = sheltered + sum(objs{d}.sheltered(s, objs{d}.valid));
                total = total + sum(objs{d}.valid);
            end
            
            Profile.Individual.Table(midx + s, pidx) = sheltered / total;
        end
        pidx = pidx + 1;
        %% # contacts
        Profile.Individual.Fields{pidx} = 'no. contacts';
        p = sum(obj.Hierarchy.Group.AggressiveChase.Contacts, 2);
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = p(s);
        end
        pidx = pidx + 1;
        %% % contacts end in chase
        Profile.Individual.Fields{pidx} = 'no. chases';
        p = sum(obj.Hierarchy.Group.AggressiveChase.ChaseEscape, 2);
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = p(s);
        end
        pidx = pidx + 1;
        %% % contacts end in escape
        Profile.Individual.Fields{pidx} = 'no. escapes';
        p = sum(obj.Hierarchy.Group.AggressiveChase.ChaseEscape, 1);
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = p(s);
        end
        pidx = pidx + 1;
        %% % contacts start in approach
        Profile.Individual.Fields{pidx} = 'no. approachs';
        c = zeros(obj.nSubjects);
        for day = 1:4;
            for l=1:length(objs{day}.Contacts.List)
                curr = objs{day}.Contacts.List(l);
                s1 = curr.subjects(1);
                s2 = curr.subjects(2);
                c(s1, s2) = c(s1, s2) + curr.states(1, 2);
                c(s2, s1) = c(s2, s1) + curr.states(2, 2);
            end
        end
        p = sum(c, 2);
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = p(s);
        end
        pidx = pidx + 1;
        %% % contacts start in approach
        Profile.Individual.Fields{pidx} = 'no. being approached';
        c = zeros(obj.nSubjects);
        for day = 1:4;
            for l=1:length(objs{day}.Contacts.List)
                curr = objs{day}.Contacts.List(l);
                s1 = curr.subjects(1);
                s2 = curr.subjects(2);
                c(s1, s2) = c(s1, s2) + curr.states(1, 2);
                c(s2, s1) = c(s2, s1) + curr.states(2, 2);
            end
        end
        p = sum(c, 1);
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = p(s);
        end
        pidx = pidx + 1;
        %% % contacts to inferior end in chase
        rank = obj.Hierarchy.Group.AggressiveChase.rank;
        map = repmat(rank(:), 1, obj.nSubjects) > repmat(rank(:)', obj.nSubjects, 1);
        
        Profile.Individual.Fields{pidx} = 'no. inferior chases';
        p = sum(obj.Hierarchy.Group.AggressiveChase.ChaseEscape .* map, 2);
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = p(s);
        end
        pidx = pidx + 1;
        %% % contacts to superior end in chase
        rank = obj.Hierarchy.Group.AggressiveChase.rank;
        map = repmat(rank(:), 1, obj.nSubjects) < repmat(rank(:)', obj.nSubjects, 1);
        
        Profile.Individual.Fields{pidx} = 'no. superior chases';
        p = sum(obj.Hierarchy.Group.AggressiveChase.ChaseEscape .* map, 2);
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = p(s);
        end
        pidx = pidx + 1;
        %% % approaches to inferior end in chase
        rank = obj.Hierarchy.Group.AggressiveChase.rank;
        map = repmat(rank(:), 1, obj.nSubjects) > repmat(rank(:)', obj.nSubjects, 1);
        Profile.Individual.Fields{pidx} = 'no. approachs to inf';
        c = zeros(obj.nSubjects);
        for day = 1:4;
            for l=1:length(objs{day}.Contacts.List)
                curr = objs{day}.Contacts.List(l);
                s1 = curr.subjects(1);
                s2 = curr.subjects(2);
                c(s1, s2) = c(s1, s2) + curr.states(1, 2);
                c(s2, s1) = c(s2, s1) + curr.states(2, 2);
            end
        end
        p = sum(c.*map, 2);
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = p(s);
        end
        pidx = pidx + 1;
        %% % approaches to superior end in chase
        rank = obj.Hierarchy.Group.AggressiveChase.rank;
        map = repmat(rank(:), 1, obj.nSubjects) < repmat(rank(:)', obj.nSubjects, 1);
        Profile.Individual.Fields{pidx} = 'no. approachs to sup';
        c = zeros(obj.nSubjects);
        for day = 1:4;
            for l=1:length(objs{day}.Contacts.List)
                curr = objs{day}.Contacts.List(l);
                s1 = curr.subjects(1);
                s2 = curr.subjects(2);
                c(s1, s2) = c(s1, s2) + curr.states(1, 2);
                c(s2, s1) = c(s2, s1) + curr.states(2, 2);
            end
        end
        p = sum(c.*map, 2);
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = p(s);
        end
        pidx = pidx + 1;        
        %% entropy
        Profile.Individual.Fields{pidx} = 'entropy';
        p = zeros(obj.nSubjects, obj.ROI.nZones);
        for day=1:4;
            for s=1:obj.nSubjects
                p(s, :) = p(s, :) + histc(objs{day}.zones(s, objs{day}.valid), 1:objs{day}.ROI.nZones);
            end
        end
        for s=1:obj.nSubjects
            h = p(s, :) / sum(p(s, :));
            Profile.Individual.Table(midx + s, pidx) = -h * log2(h' + (h' == 0));
        end
        pidx = pidx + 1;
        %% % time near food or water
        Profile.Individual.Fields{pidx} = '% time near food or water';
        
        for s=1:obj.nSubjects
            total = 0;
            near = 0;
            for day=1:4
                z = objs{day}.zones(s, objs{day}.valid);
                near = near + sum(z == 2 | z == 3 | z == 4);
                total = total + sum(objs{day}.valid);
            end
            
            Profile.Individual.Table(midx + s, pidx) = near / total;
        end
        pidx = pidx + 1;
        
        %% % time elevated
        Profile.Individual.Fields{pidx} = '% time elevated';
        
        for s=1:obj.nSubjects
            total = 0;
            high = 0;
            for day=1:4
                z = objs{day}.zones(s, objs{day}.valid);
                high = high + sum(z == 5 | z == 8 | z == 10);
                total = total + sum(objs{day}.valid);
            end
            
            Profile.Individual.Table(midx + s, pidx) = high / total;
        end
        pidx = pidx + 1;
        
        %% % time alone outside
        Profile.Individual.Fields{pidx} = '% time alone outside';
        
        for s=1:obj.nSubjects
            other = 1:obj.nSubjects;
            other = other(other ~= s);
            total = 0;
            p = 0;
            for day=1:4
                p = p + sum(all(objs{day}.sheltered(other, objs{day}.valid) == 1) & ~objs{day}.sheltered(s, objs{day}.valid));
                total = total + sum(objs{day}.valid);
            end
            
            Profile.Individual.Table(midx + s, pidx) = p / total;
        end
        pidx = pidx + 1;
        
        %% interaction entropy
        Profile.Individual.Fields{pidx} = 'mi day 2';
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) =  sum(objs{2}.Analysis.Potts.Ik);
        end
        pidx = pidx + 1;
        
        %% interaction entropy
        Profile.Individual.Fields{pidx} = 'mi day 4';
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) =  sum(objs{4}.Analysis.Potts.Ik);
        end
        pidx = pidx + 1;
        %% median speed
        Profile.Individual.Fields{pidx} = 'median speed';
        for s=1:obj.nSubjects
            speed = [];
            for day=1:4
                speed = [speed, objs{day}.speedCMperSec(s, objs{day}.valid & ~objs{day}.sheltered(s, :))];
            end
            
            Profile.Individual.Table(midx + s, pidx) =  nanmedian(speed);
        end
        pidx = pidx + 1;
        
        %% potts 2nd order 2nd day
        Profile.Individual.Fields{pidx} = 'potts 2nd order 2nd day';
        c = cumsum(objs{2}.Analysis.Potts.Ik)/sum(objs{2}.Analysis.Potts.Ik);
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = c(1);
        end
        pidx = pidx + 1;
        
        %% potts 3rd order 2nd day
        Profile.Individual.Fields{pidx} = 'potts 3rd order 2nd day';
        c = cumsum(objs{2}.Analysis.Potts.Ik)/sum(objs{2}.Analysis.Potts.Ik);
        for s=1:obj.nSubjects
            Profile.Individual.Table(midx + s, pidx) = c(2);
        end
        pidx = pidx + 1;
    catch e
        fprintf('# !!!!!!! ERROR\n', i, GroupsData.nExperiments);
        return
        warning(e.message);
    end
    %%
    midx = midx + obj.nSubjects;
end



%%
if 1==2
    %% sort by david score
    clc
    fprintf('%-30s', 'Experiment');
    fprintf(' ID', 'Experiment');
    fprintf('%25s', Profile.Individual.Fields{:});
    fprintf('\n');
    Colors = {'P', 'R', 'O', 'G'};
    for i=unique(Profile.Experiment(:)')
        map = Profile.Experiment == i;
        [~, o] = sort(Profile.Individual.Table(map, 2), 'descend');
        t = Profile.Individual.Table(map, :);
        t = t(o, :);
        for s=1:size(t, 1)
            fprintf('%-30s', GroupsData.Experiments{i});
            fprintf('  %s', Colors{o(s)});
            fprintf('%25g', t(s, :));
            fprintf('\n');
        end
    end
end