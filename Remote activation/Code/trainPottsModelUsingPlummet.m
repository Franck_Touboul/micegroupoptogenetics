function me = trainPottsModelUsingPlummet(options, data, confidence)
% Sources: 1.   Maximum entropy density estimation with generalized regulari-
%               zation and an application to species distribution modeling, 
%               with S. J. Phillips and R. E. Schapire, Journal of Machine 
%               Learning Research 8, 2007, 1217-1260
%          2.   A maximum entropy approach to species distribution modeling, 
%               with S. J. Phillips and R. E. Schapire, Proceedings of the 
%               21st International Conference on Machine Learning, 2004, 655-662

options.param_report = false;
options = setDefaultParameters(options, ...
    'Statistics', false, ...
    'tk', 1, ...
    'beta', 0.01, ...
    'KLConvergenceRatio', 1e-4, ...
    'nIters', 20000);
options.NeutralZone = 1; % Open
options.MinLog = -50;
me.tk = options.tk;
me.LineFactor = .5;
useLineSearch = true;
me.LineSearchMaxSteps = 20;
me.KLConvergenceRatio = options.KLConvergenceRatio;
%%
use_sparse = true;
if nargin < 3
    confidence = .05;
end
me.confidence = confidence;
me.neutralZone = options.NeutralZone;

if isfield(options, 'Output')
    output = options.Output;
else
    output = true;
end
%
if output; fprintf('# Training Potts Model\n'); end
%
if output; fprintf('# - finding all permutation of max dimension %d...\n', options.n); end;
%%
allPerms = nchoose(1:options.nSubjects);
nPerms = {};
j = 1;
for i=1:length(allPerms)
    if length(allPerms{i}) <= options.n 
        nPerms{j} = allPerms{i};
        j = j + 1;
    end
end

%%
if output; fprintf('# - computing constraints...\n'); end
[me.constraints, pci] = PottsComputeConstraints(data' + 1, options.count, nPerms, abs(confidence));
C = length(nPerms);
%
me.inputPerms = myPerms(options.nSubjects, options.count);
[me.perms, me.labels] = assignFeature(me.inputPerms, options.count, nPerms, use_sparse);

valid = false(1, length(me.labels));
if options.count == 2
    for i=1:length(me.labels)
        if all(me.labels{i}(2, :) ~= 2)
            valid(i) = true;
        end
    end
    me.labels = me.labels(valid);
    me.perms = me.perms(:, valid);
    me.constraints = me.constraints(valid);
    pci = pci(valid);
end
%
me.nSubjects = options.nSubjects;
me.range = options.count;
%% initialize the weights
if isfield(options, 'initialPottsWeights') && ~isempty(options.initialPottsWeights)
    me.weights = options.initialPottsWeights;
else
    me.weights = randn(1, size(me.labels, 2)) / size(me.labels, 2);
end
orig = me;

%% filter neutral zone
neutral = false(1, length(orig.labels));
for l=1:length(me.labels)
    neutral(l) = any(orig.labels{l}(2, :) == options.NeutralZone);
end
me.constraints = orig.constraints(~neutral);
me.perms = orig.perms(:, ~neutral);
me.labels = orig.labels(~neutral);
% initialize the weights
if isfield(options, 'initialPottsWeights') && ~isempty(options.initialPottsWeights)
    me.weights = options.initialPottsWeights;
else
    me.weights = orig.weights(~neutral);
end
%
me.momentum = me.weights;
pci = pci(~neutral, :);

%% compute empirical probabilities
base = me.range;
baseVector = base.^(size(data, 1)-1:-1:0);
pEmp = histc(baseVector * data+1, 1:base ^ size(data, 1));
pEmp = pEmp / sum(pEmp);
%% initial halfKL
me.beta = options.beta;
currBeta = me.beta * sqrt(me.constraints - me.constraints.^2) / sqrt(size(data, 1));
p = exp(me.perms * me.weights');
Z = sum(p);
p = p / Z;
kl2 = -pEmp * log2(p + (pEmp' == 0)) + sum(currBeta .* abs(me.weights)) + sum(currBeta .* abs(me.weights));
kl1 = pEmp * log2(pEmp' + (pEmp' == 0));
%%
if options.Statistics; me.Statistics = struct(); end
tic;
me.converged = false;
me.KLconverged = false;
currTk = me.tk;
for iter=1:options.nIters
    if output; fprintf('# - iter (%4d/%4d) : ', iter, options.nIters); end
    % compute the (un-normalized) pdf for each sample
    p = exp(me.perms * me.weights');
    % normalize the pdf (the Z)
    p = p / sum(p);
    prev_kl = kl1 + kl2;
    kl2 = -pEmp * log2(p + (pEmp' == 0)) + sum(currBeta .* abs(me.weights));
    E = full(sum(me.perms .* repmat(p, 1, size(me.perms, 2))));
    
    maxlog = 10;
    df_p = MyTrim(log((me.constraints - currBeta)./E), [-maxlog maxlog]); m_p = (df_p + me.weights > 0) & isreal(df_p);
    df_n = MyTrim(log((me.constraints + currBeta)./E), [-maxlog maxlog]); m_n = (df_n + me.weights < 0) & isreal(df_n);
    df_0 = -me.weights;
    
    df = nan(1, length(me.weights));
    df(m_p) = df_p(m_p);
    df(isnan(df) & m_n) = df_n(isnan(df) & m_n);
    df(isnan(df)) = df_0(isnan(df));
    
    if confidence ~= 0
        %[sum(E'  > pci(:, 1) & E' < pci(:, 2)) (1 - confidence) * length(me.weights)]
        nconverged = sum(E'  > pci(:, 1) & E' < pci(:, 2));
        if output; fprintf('KL = %6.4f, convergence = %5.2f%%\n', kl1 + kl2, (nconverged / length(me.weights) * 100)); end
        me.nconverged = nconverged;
    else
        if output; fprintf('KL = %6.4f (%.3f)\n', kl1 + kl2, abs((kl1 + kl2 - prev_kl)/prev_kl)); end
    end
    
    if me.KLConvergenceRatio > 0
        if abs((kl1 + kl2) - prev_kl) / prev_kl <= me.KLConvergenceRatio && iter >= options.MinNumberOfIters 
            if me.KLconverged
                break;
            else
                me.KLconverged = true;
            end
        elseif me.KLconverged
            me.KLconverged = false;
        end
    end

    if useLineSearch
        t = min(currTk / me.LineFactor, me.tk);    
        found = false;
        nsteps = 1;
        while ~found
            pt = exp(me.perms * (me.weights + t * df)');
            pt = pt / sum(pt);
            ckl2 = -pEmp * log2(pt + (pEmp' == 0)) + sum(currBeta .* abs(me.weights + t * df));
            found = ckl2 < kl2;
            if ~found
                t = me.beta * t;
            end
            if nsteps >= me.LineSearchMaxSteps
                break;
            end
            nsteps = nsteps + 1;
        end
    else
        t = 1/C;
    end
    %
    me.weights = me.weights + t * df;
    %%
    if options.Statistics;
        me.Statistics(iter).times = toc; 
        me.Statistics(iter).KL = kl1 + kl2;
    end
end
me.nIters = iter;
me.Dkl = kl1 + kl2;
%me = rmfield(me, 'features');
