function SocialPottsControlsAnalyze(data, tit)
%%
idx = 1;
f = fieldnames(data{idx});
obj1 = data{idx}.(f{1});
obj2 = data{idx}.(f{2});
%%
figure(1)
%%
for i=1:obj1.nSubjects
    me1 = obj1.Analysis.Potts.Model{i};
    me2 = obj2.Analysis.Potts.Model{i};
    
    SquareSubplpot(obj1.nSubjects, i);
    PlotProbProb(me1.prob, me2.prob,sum(obj1.valid), true)
    xlabel([num2ordinal(i) ' order model prob.']);
    ylabel([num2ordinal(i) ' order model prob.']);
end
%%
figure(2)
%%
for i=1:obj1.nSubjects
    me1 = obj1.Analysis.Potts.Model{4};
    me2 = obj2.Analysis.Potts.Model{i};
    
    SquareSubplpot(obj1.nSubjects, i);
    PlotProbProb(me1.prob, me2.prob,sum(obj1.valid), true)
    xlabel('observed prob.');
    ylabel([num2ordinal(i) ' order model prob.']);
end


