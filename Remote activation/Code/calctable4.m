curr =2;
fprintf('# - tracking subject %d of %d\n', curr, options.nSubjects);
    currLogprobs = cents.logprob(:, :, curr);
    
    %% build observation probability table
    table = ones(options.nSubjects+obj.ROI.nHidden+1, length(res{curr}.id)) * -inf;
    scoord.x = zeros(options.nSubjects, length(res{curr}.id));
    scoord.y = zeros(options.nSubjects, length(res{curr}.id));
    scoord.hidingDistance = zeros(options.nSubjects, length(res{curr}.id), obj.ROI.nHidden);
    path = zeros(size(table), 'uint16');
    nchar = 0;
    fprintf('# - computing emission probabilities\n');
    for s=1:options.nSubjects
        range = 1:ndata;
        
        range = range(res{s}.valid);
        centids = res{s}.centids(res{s}.valid);
        
        table(s, range) = currLogprobs(sub2ind(size(currLogprobs), range, centids));
        scoord.x(s, range) = res{s}.x(res{s}.valid);
        scoord.y(s, range) = res{s}.y(res{s}.valid);
        for h=1:obj.ROI.nHidden
            nchar = Reprintf(nchar, '#     . processing segment %d / %d', ((s - 1) * obj.ROI.nHidden) + h, options.nSubjects*obj.ROI.nHidden);
            d = pdist2([scoord.x(s, range)', scoord.y(s, range)'], obj.ROI.HiddenBoundarySCoordinates{h});
            scoord.hidingDistance(s, range, h) = min(d, [], 2)';
        end
    end
    fprintf('\n');
    
    for s=1:obj.ROI.nHidden+1
        table(options.nSubjects+s, :) = flog(prod(1 - exp(table(1:options.nSubjects, :)), 1));
    end
    emitlogprobs = table;
    
    valid = sum(isfinite(table(1:options.nSubjects, :)), 1) > 0;
    d = diff([0 valid 0], 1, 2);
    start  = find(d > 0);
    finish = find(d < 0) - 1;

    %%
    prev = 1;
    nchar = 0;
    
    scoord.hidingPos.x = ones(1, length(res{curr}.id)) * inf;
    scoord.hidingPos.y = ones(1, length(res{curr}.id)) * inf;
    
    %%zeroprob = gppdf(0, res{curr}.pareto(2), res{curr}.pareto(1));
    zeroprob = exppdf(0, res{curr}.meanDistance);
    %zeroprob = gppdf(0, res{curr}.pareto(2), res{curr}.pareto(1));
    zeroprob = flog(zeroprob ./ (gauss(res{curr}.allDistMean, res{curr}.allDistVar, 0) + zeroprob));
    zeroprobvec = [repmat(zeroprob, options.nSubjects, 1); zeros(obj.ROI.nHidden+1, 1)];
    hiddenprobvec = [repmat(zeroprob, options.nSubjects, 1); zeros(obj.ROI.nHidden, 1); zeroprob];
    
    fprintf('# - building viterbi table\n');
    for n=1:length(start)
        nchar = Reprintf(nchar, '#     . processing segment %d / %d', n, length(start));
        r = start(n):finish(n);
        for f=r
            %
            for s=1:options.nSubjects
                if ~isfinite(table(s, f))
                    continue;
                end
                x = res{s}.x(f);
                y = res{s}.y(f);
                
                distance = scoord.hidingDistance(s, f, :);
                distance = [...
                    sqrt((x - scoord.x(:, prev)).^2 + (y - scoord.y(:, prev)).^2); ...
                    distance(:);...
                    sqrt((x - scoord.hidingPos.x(prev)).^2 + (y - scoord.hidingPos.y(prev)).^2)];
                %prob = gppdf(distance, res{curr}.pareto(2), res{curr}.pareto(1));
                prob = exppdf(distance, res{curr}.meanDistance);

                trans = flog(prob ./ (gauss(res{curr}.allDistMean, res{curr}.allDistVar, distance) + prob));
                if f>prev+1
                    trans = trans + zeroprobvec * (f - prev - 1);
                end
                
                [val, from] = max(table(:, prev) + trans);
                table(s, f) = table(s, f) + val;
                path(s, f) = from;
            end
            %
            for h=1:obj.ROI.nHidden
                distance = [scoord.hidingDistance(:, prev, h); obj.ROI.HiddenSDistances(:, h); inf];
%                prob = gppdf(distance, res{curr}.pareto(2), res{curr}.pareto(1));
                prob = exppdf(distance, res{curr}.meanDistance);
                trans = flog(prob ./ (gauss(res{curr}.allDistMean, res{curr}.allDistVar, distance) + prob));

                [val, from] = max(table(:, prev) + trans);
                table(options.nSubjects + h, f) = table(options.nSubjects + h, f) + val;
                path(options.nSubjects + h, f) = from;
            end
            %
            [val, from] = max(table(:, prev) + hiddenprobvec);
            table(options.nSubjects + obj.ROI.nHidden + 1, f) = table(options.nSubjects + obj.ROI.nHidden + 1, f) + val;
            path(options.nSubjects + obj.ROI.nHidden + 1, f) = from;
            if from <= options.nSubjects
                scoord.hidingPos.x(f) = scoord.x(from, prev);
                scoord.hidingPos.y(f) = scoord.y(from, prev);
            else
                scoord.hidingPos.x(f) = scoord.hidingPos.x(prev);
                scoord.hidingPos.y(f) = scoord.hidingPos.y(prev);
            end
            %
            prev = f;
        end
    end
    fprintf('\n');
    %% backtrack
    fprintf('# - backtracking\n');
    track{curr}.src = ones(1, size(table, 2)) * (options.nSubjects + 1);
    track{curr}.logprob = zeros(1, size(table, 2));
    track{curr}.emitlogprob = zeros(1, size(table, 2));
    track{curr}.x = zeros(1, size(table, 2));
    track{curr}.y = zeros(1, size(table, 2));

    for n=length(start):-1:1
        r = start(n):finish(n);
        f=finish(n);
        if n<length(start)
            idx = path(track{curr}.src(start(n+1)), start(n+1));
            
            track{curr}.src(finish(n)) = idx;
            track{curr}.logprob(f) = table(idx, f);
            track{curr}.emitlogprob(f) = emitlogprobs(idx, f);
        else
            [track{curr}.logprob(finish(n)), track{curr}.src(finish(n))] = max(table(:, finish(n)));
            track{curr}.emitlogprob(finish(n)) = emitlogprobs(track{curr}.src(finish(n)), finish(n));
        end
        for f=finish(n)-1:-1:start(n)
            idx = path(track{curr}.src(f+1), f+1);
            
            track{curr}.src(f) = idx;
            track{curr}.logprob(f) = table(idx, f);
            track{curr}.emitlogprob(f) = emitlogprobs(idx, f);
        end
        if n>1
            track{curr}.logprob(finish(n-1)+1:start(n)-1) = track{curr}.logprob(start(n));
            track{curr}.emitlogprob(finish(n-1)+1:start(n)-1) = track{curr}.emitlogprob(start(n));
        end
    end
    
    fprintf('# - computing track properties\n');
    track{curr}.valid = track{curr}.src <= options.nSubjects;
    track{curr}.id = track{curr}.valid * curr;
    
    range = 1:length(track{curr}.valid);
    range = range(track{curr}.valid);
    
    src = track{curr}.src(track{curr}.valid);
    
    track{curr}.x = zeros(1, length(track{curr}.valid));
    track{curr}.x(track{curr}.valid) = scoord.x(sub2ind(size(scoord.x), src, range));
    track{curr}.x(~track{curr}.valid) = nan;
    
    track{curr}.y = zeros(1, length(track{curr}.valid));
    track{curr}.y(track{curr}.valid) = scoord.y(sub2ind(size(scoord.y), src, range));
    track{curr}.y(~track{curr}.valid) = nan;