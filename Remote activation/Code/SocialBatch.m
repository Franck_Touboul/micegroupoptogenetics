SocialExperimentData;
%%
vals = [];
H = {};
for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        try
            obj = TrackLoad(['Res/' prefix '.obj.mat'], {'common'});
        catch
            vals(id, day) = nan;
            continue;
        end
        obj = SoftZoneCategorize(obj);
%         h = zeros(1, obj.ROI.nZones);
%         for i=1:obj.nSubjects
%             h = h + histc(obj.zones(i, :), 1:obj.ROI.nZones);
%         end
        h = zeros(1, obj.ROI.nCategories);
        for i=1:obj.nSubjects
            h = h + histc(obj.category(i, :), 1:obj.ROI.nCategories);
        end
        H{id, day} = h;
    end
end
%% plot position histograms by days
h  = zeros(nDays, obj.ROI.nZones, length(Groups));
hm = {};
for day=1:nDays
    mh = [];
    sh = [];
    hh = {};
    for g=1:length(Groups)
        h = [];
        for i=1:length(Groups(g).idx)
            h = [h; H{Groups(g).idx(i), day}/sum(H{Groups(g).idx(i), day})];
        end
        hh{g} = h;
        mh = [mh; mean(h)];
        sh = [sh; stderr(h)];
    end
    subplot(4,1,day);
    barweb(mh', sh');
    for i=1:size(h, 2)
        if ttest2(hh{1}(:, i), hh{2}(:, i))
            text(i, 0.8, '*');
        end
    end
    %break;
end
%% plot locations by location
clf
h  = zeros(nDays, obj.ROI.nZones, length(Groups));
for g=1:length(Groups)
    hm = {};
    mh = [];
    sh = [];
    hh = {};
    for day=1:nDays
        h = [];
        for i=1:length(Groups(g).idx)
            h = [h; H{Groups(g).idx(i), day}/sum(H{Groups(g).idx(i), day})];
        end
        hh{g} = h;
        mh = [mh; mean(h * 100)];
        sh = [sh; stderr(h * 100)];
    end
    for i=1:size(mh, 2)
        SquareSubplpot(size(mh, 2), i);
        errorbar(1:nDays, mh(:, i), sh(:, i), 'Color', Groups(g).color);
        if i==1
            title('Open');
        else
            title(obj.ROI.CategoryNames{i});
        end
        xlabel('days');
        ylabel('% of time in region');
        set(gca, 'Box', 'off');
        set(gca, 'XTick', 1:nDays);
        hon;
    end
end
for i=1:obj.ROI.nCategories
    subplot(2,3, i);
    a = axis;
    a(1) = 0.7; a(2) = 4.3;
    axis(a);
end
%TieAxis(2,3,1:5);