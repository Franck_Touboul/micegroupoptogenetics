%% Loading the Excel file for all mice (if needed)

%% globals
global data;
global rawData;
global zones;
global orig;

%% general parameters
options.path = 'Data/';
options.output_path = 'res/';

%% experiments database
% zones: (1) feeder1, (2) feeder2, (3) water, (4) upperSmallNest, 
% (5) smallNest, (6) entrySmallNest, (7) bigNest, (8) entryBigNest
% (9) labyrinth, (10) saftyStrip
options.test.headerLines = 27;
if isfield(options.test, 'id')
    switch options.test.id
        case {1, 1.1} % enriched day 1
            options.test.number = 1;
            options.test.trial = 1;
            options.test.type = 'EN';
            options.test.day = 1;
            options.test.zones = 1:10;
        case {2, 2.1} % not-enriched (control) day 1
            options.test.number = 4;
            options.test.trial = 2;
            options.test.type = 'SC';
            options.test.day = 1;
            options.test.zones = [0 1 2 3 9 4 7 5 6 8 0];
            options.test.headerLines = 32;
        case 2.2 % not-enriched (control) day 2
            options.test.number = 4;
            options.test.trial = 2;
            options.test.type = 'SC';
            options.test.day = 2;
            options.test.zones = [0 1 2 3 9 4 7 5 6 8 0];
    end
end

%% test if the data should be reloaded
reload = false;
if (~exist('data') || isempty(data))
    reload = true;
end
if isfield(orig, 'meta') && isfield(orig.meta, 'id') && orig.meta.id ~= options.test.id
    reload = true;
end

%% parse data filenames
options.filename = {};
for i=1:options.nSubjects
    options.filename{i} = sprintf([options.path 'Exp%d (%s) day%d (Trial     %d)-Arena 1 Subject %d(1).txt'],...
        options.test.number, options.test.type, options.test.day, options.test.trial, i);
end
options.title = sprintf('Exp%d (%s) day%d (Trial     %d)-Arena 1',...
    options.test.number, options.test.type, options.test.day, options.test.trial);
options.mat = [options.path options.title '.mat'];
fprintf(['# loading data: ''' options.title '''\n']);

%% read data (if needed)
if reload
    orig.meta = options.test;
    orig.nSubjects = options.nSubjects;
    res = 0;
    if isfield(options, 'mat') && ~isempty(options.mat)
        res = fileattrib([options.mat '.mat']);
        loadfrom = [options.mat '.mat'];
        if res == 0
            res = fileattrib([options.mat]);
            loadfrom = options.mat;
        end
    end
    if 1==2 % res ~= 0
        fprintf('# - loading from mat file... ');
        prev_options = options;
        load(loadfrom);
        options = prev_options;
        fprintf('[done]\n');
    else
        orig.x = [];
        orig.y = [];
        orig.distance = [];
        orig.velocity = [];
        orig.area = [];
        orig.elongation = [];

        zones.feeder1 = [];
        zones.feeder2 = [];
        zones.water = [];
        zones.labyrinth = [];
        zones.bigNest = [];
        zones.smallNest = [];
        zones.saftyStrip = [];
        zones.upperSmallNest = [];
        zones.entrySmallNest = [];
        zones.saftyStrip = [];
        zones.all = [];
        %% remap zones
        i=1;
        remap = [];
        for z = options.test.zones
            if z > 0
                remap(z) = i;
            end
            i = i + 1;
        end
        %%
        for i=1:options.nSubjects
            fprintf('#  . subject %d/%d\n', i, options.nSubjects);

            fid = fopen(options.filename{i});
            data = textscan(fid, '', 'Delimiter', ';', 'HeaderLines', options.test.headerLines, 'TreatAsEmpty', '"-"');
            if length(data{1}) == 0
                error(['cannot load data from ' options.filename{i}]);
            end
            fclose(fid);
            %
            if i == 1
                orig.time       = data{1}';
            end
            orig.x          = [orig.x; data{3}'];
            orig.y          = [orig.y; data{4}'];
            %orig.distance   = [orig.distance; data{8}];
            orig.velocity   = [orig.velocity; data{9}'];
            %orig.area       = [orig.area;     data{5}];
            %orig.elongation = [orig.elongation; data{7}];

            %         zones.feeder1        = [zones.feeder1; data{10}];
            %         zones.feeder2        = [zones.feeder2; data{11}];
            %         zones.water          = [zones.water; data{12}];
            %         %zones.upperSmallNest = [zones.smallNest; data{13}];
            %         zones.smallNest      = [zones.smallNest; data{14}];
            %         %zones.entrySmallNest = [zones.smallNest; data{15}];
            %         zones.bigNest        = [zones.bigNest; data{16}];
            %         %zones.entryBigNest   = [zones.smallNest; data{17}];
            %         zones.labyrinth      = [zones.labyrinth; data{18}];
            %         %zones.saftyStrip     = [zones.saftyStrip; data{19}];

            all = data{10}' * 0;
%             for k=11:length(data)
%                 if any(data{k} == 0)
%                     all(data{k}        > 0) = k-11;
%                 end
%             end
%             all(data{10}        > 0) = 1; % feeder1
%             all(data{11}        > 0) = 2; % feeder2
%             all(data{12}        > 0) = 3; % water
%             all(data{13}        > 0) = 4; % water
%             all(data{14}        > 0) = 5; % smallNest
%             all(data{15}        > 0) = 6; % water
%             all(data{16}        > 0) = 7; % bigNest
%             all(data{17}        > 0) = 8; % water
%             all(data{18}        > 0) = 9; % labyrinth
             all(data{10 + remap(1) - 1}        > 0) = 1; % feeder1
             all(data{10 + remap(2) - 1}        > 0) = 2; % feeder2
             all(data{10 + remap(3) - 1}        > 0) = 3; % water
             all(data{10 + remap(5) - 1}        > 0) = 4; % smallNest
             all(data{10 + remap(7) - 1}        > 0) = 5; % bigNest
             all(data{10 + remap(9) - 1}        > 0) = 6; % labyrinth
            zones.all = [zones.all; all];
        end
        orig.nData = length(orig.time);
        zones.labels = {'Open Space', 'Feeder #1', 'Feeder #2', 'Water', 'Small-Nest', 'Labyrinth', 'Big-Nest'};
        zones.count = 7;
        fprintf('# - done\n');

        orig.colors = {[172 190 206]/255, [208 179 189]/255, [181 73 157]/255, [144 90 209]/255};

        if isfield(options, 'mat') && ~isempty(options.mat)
            fprintf('# - saving data...\n');
            save(options.mat);
        end
    end
else
    fprintf('# - data already loaded\n');
end
rawData = data;

