function model = SocialAggrClassifierTrainTest(obj)
if iscell(obj)
    objs = obj;
    for i=1:length(objs)
        %curr = TrackLoad(objs{i});
        curr = objs{i};
        if ischar(curr)
            curr = TrackLoad(curr);
            curr = SocialPredPreyModel(curr);
        end
        if i == 1
            obj = curr;
            Classifier = curr.Contacts.Behaviors.AggressiveChase.Classifier;
            Classifier.Source = ones(1, size(Classifier.Vec, 2)) * 1;
            continue;
        end
        currClassifier = curr.Contacts.Behaviors.AggressiveChase.Classifier;
        fields = {'Vec', 'ChaseEscapeMap', 'Labels', 'MarkID'};
        Classifier.Source = [Classifier.Source, ones(1, size(currClassifier.Vec, 2)) * i];
        for f=1:length(fields);
            Classifier.(fields{f}) = [Classifier.(fields{f}), currClassifier.(fields{f})];
        end
    end
    obj.Contacts.Behaviors.AggressiveChase.Classifier = Classifier;
else
    if ischar(obj)
        obj = TrackLoad(obj);
        obj = SocialPredPreyModel(obj);
    end
    obj.Contacts.Behaviors.AggressiveChase.Classifier.Source = ones(1, size(obj.Contacts.Behaviors.AggressiveChase.Classifier.Vec, 2)) * 1;
end
%%
options.DAMethod = 'quadratic';
options.AggrThreshold = 2;
options.DANumFeatures = 10;
%%
Classifier = obj.Contacts.Behaviors.AggressiveChase.Classifier;
%%
TrainMap = obj.Contacts.Behaviors.AggressiveChase.Classifier.ChaseEscapeMap;

data = Classifier.Vec';
label = Classifier.Labels';
train = data(TrainMap, :);
label = label(TrainMap, :);
label = (label >= options.AggrThreshold) .* Classifier.Source(TrainMap)';

options.MinLeaf = sum(label) / 10;

for i=1:size(train, 2) %@@@
    train(~isfinite(train(:, i)), i) = min(isfinite(train(:, i)));
end
%% train classifiers
%--------------------------------------------------------------------------
% decision tree classifier
Classifier.Tree = classregtree(train, cellstr(num2str(label,'%d')), 'minleaf', options.MinLeaf, 'names', Classifier.LabelNames);

%featidx = [1 4 5 6 7 9 10 12 14 15 16 17  20 22 23 24];
%classes = classify(train(:, featidx), train(:, featidx), label, 'mahalanobis');
%%
% treefeats = Classifier.Tree.cutvar;
% treeprobs = Classifier.Tree.nodeprob;
% treeprobs = treeprobs(~strcmp(treefeats, ''));
% [treeprobs, o] = sort(treeprobs, 1, 'descend');
% treefeats = {treefeats{~strcmp(treefeats, '')}};
% treefeats = {treefeats{o(1:options.DANumFeatures)}};
%%
%--------------------------------------------------------------------------
% DA classifier
dataTrainG1 = train(label==0,:);
dataTrainG2 = train(label==1,:);
[h,p,ci] = ttest2(dataTrainG1,dataTrainG2,[],[],'unequal');
[p, featidx] = sort(p);
featlocal = featidx(1:options.DANumFeatures);

% featlocal = [];
% for i=1:length(treefeats)
%     featlocal(i) = find(strcmp(treefeats{i}, Classifier.LabelNames), 1);
% end
%%
Classifier.FeatureSubset = featlocal;
Classifier.DA.Method = options.DAMethod;
Classifier.Train.Data = train;
Classifier.Train.Labels = label;
Classifier.KNN.Method = 'euclidean';
Classifier.KNN.nNeighbours = 5;
Classifier.Classify = @SocialAggrClassifier;
model.Contacts.Behaviors.AggressiveChase.Classifier = Classifier;
model.Contacts.Behaviors.AggressiveChase.ChaseEscapeMap = TrainMap;

if nargout == 0
    obj.Contacts.Behaviors.AggressiveChase.Classifier = Classifier;
    obj.Contacts.Behaviors.AggressiveChase.ChaseEscapeMap = TrainMap;
    if obj.OutputToFile
        fprintf('# - saving data\n');
        TrackSave(obj);
    end
end