function [conditionalProbs, jointProbs, values] = ConditionalProbability(data, range)
if ~exist('range', 'var')
    range = max(data(:)) + 1;
end

if nargout > 2
   [jointProbs, pci, values] = JointProbability(data, range);
else
    jointProbs = JointProbability(data, range);
end
pprobs = JointProbability(data(2:end, :), range);
pprobs(pprobs == 0) = 1;
pprobs = repmat(pprobs, 1, range);
conditionalProbs = jointProbs ./ pprobs;
