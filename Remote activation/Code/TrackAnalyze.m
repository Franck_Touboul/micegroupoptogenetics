function obj = TrackAnalyze(obj, fields)
if ~iscell(fields)
    fields = {fields};
end
%missing = {};
for i=1:length(fields)
    if ~isfield(obj, fields{i})
        switch lower(fields{i})
            case {'circadian'}
                obj = SocialCircadianStatistics(obj);
            otherwise
                warning(['do not know how to generate field: ' fields{i}])
        end
    end
end
