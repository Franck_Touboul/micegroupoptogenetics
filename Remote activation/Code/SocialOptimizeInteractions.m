function SocialOptimizeInteractions(obj)
%if 1==2
    %%
    data = dlmread('aux/SC.exp0001.day02.cam01.marks', '\t', [1 0 500 14]);
    marks.ids = data(:, 1);
    marks.beg = data(:, 2);
    marks.end = data(:, 3);
    marks.subjects = data(:, 4:5);
    marks.approach = data(:, 6:7);
    marks.leave = data(:, 8:9);
    marks.chase = data(:, 10);
    marks.pred = data(:, 11:12);
    marks.prey = data(:, 13:14);
    marks.artifact = data(:, 15);
    %%
%end
%%
rand('twister',sum(100*clock));
randn('seed',sum(100*clock));
%%
defaults.PredObjectLength_cm = 4:2:12;
defaults.PreyObjectLength_cm = 4:2:12;
defaults.ZoneOfProximity_cm = 8:4:20;
defaults.ZoneOfContact_cm = 8:4:20;

defaults.MinSubInteractionDuration = 0:4:16;
defaults.MinIdleDuration = 0:4:16;
defaults.MinContanctDuration = 0:4:16;
defaults.MaxShelteredContanctDuration = 0:10:30;

defaults.MinSpeed = 0:.1:.5;
defaults.SmoothSpan = 1:5;

defaults.MinNumOfContacts = 0:3:9;
defaults.RelativeSpeedInChase = .25:.25:.75;

defaults.EmissionMatch = .5;
defaults.EmissionMismatch = .1:.1:.4;
defaults.ContactMismatch = 0:.1:.2;

defaults.ChaseMinOverlap = 4:2:10;
defaults.ChaseMinOverlapPercentage = 0:.25:.5;

%r = randi(5);
%defaults.EmissionMismatch = defaults.EmissionMatch * r / (r+1);

arg = struct();
f = fieldnames(defaults);
for i=1:length(f)
    arg.(f{i}) = defaults.(f{i})(randi(length(defaults.(f{i}))));
end
% arg.EmissionMismatch = defaults.EmissionMismatch(randi(sum(defaults.EmissionMismatch < arg.EmissionMatch)));

f = fieldnames(arg);
for i=1:length(f)
    fprintf('@arg %s=%f\n', f{i}, arg.(f{i}));
end
%%

obj = TrackLoad(obj);
obj.OutputToFile = false;
obj = SocialHierarchy(obj, true, arg);
%%
results = SocialBehaviorAnalysisScore(obj, marks)

f = fieldnames(results);
for i=1:length(f)
    fprintf('@res %s=', f{i});
	fprintf(' %f', results.(f{i}));
	fprintf('\n');
end

