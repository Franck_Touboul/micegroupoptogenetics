function [events, state] = findChases_v5(m1, m2, analyzed)
if nargin < 3
    analyzed = SocialBehaviour;
end
VonMisesRationPred = 0.05;
VonMisesRationPrey = 0.5;

VonMisesBasePrey = 0.25;
VonMisesBasePred = 0.25;

options.minEventDuration = 8; % [frames]
refDistance = 5;


% %%
% model.states(1).func = @(x) halfCircularUniform(x(1)) * (1 - x(2));
% model.states(1).title = '-';
%
% model.states(2).func = @(x) halfCircularExp(x(1), pi/2);
% model.states(2).title = 'pred';
% model.states(3).func = @(x) halfCircularExp(pi - x(1), pi/2);
% model.states(3).title = 'prey';
%
% model.states(4).func = @(x) halfCircularUniform(x(1)) * x(2);
% model.states(4).title = 'cont';
%
% model.states(5).func = @(x) halfCircularExp(x(1), pi/2);
% model.states(5).title = 'pred';
% model.states(6).func = @(x) halfCircularExp(pi - x(1), pi/2);
% model.states(6).title = 'prey';
%
% model.states(7).func = @(x) halfCircularUniform(x(1));
% model.states(7).title = 'pass';
%
% model.states(8).func = @(x) halfCircularUniform(x(1));
% model.states(8).title = 'pass';
%
%
% persistence = 10;
% pst = 1-1/persistence;
% ptr = 1/persistence;
% pst = 1;
% ptr = 1;
% model.trans = [...
%     pst ptr ptr 0.0 0.0 0.0 ptr 0.0; % -
%     0.0 pst 0.0 ptr 0.0 0.0 0.0 0.0; % predator
%     0.0 0.0 pst ptr 0.0 0.0 0.0 0.0; % prey
%     0.0 0.0 0.0 pst ptr ptr 0.0 ptr; % contact
%     ptr 0.0 0.0 0.0 pst 0.0 0.0 0.0; % predator
%     ptr 0.0 0.0 0.0 0.0 pst 0.0 0.0; % prey
%     0.0 0.0 0.0 ptr 0.0 0.0 pst 0.0; % passive (1)
%     ptr 0.0 0.0 0.0 0.0 0.0 0.0 pst; % passive (2)
%     ];
% model = validateModel(model);
% model.start = zeros(model.nstates, 1);
% model.start(1) = 1;
% model.end = zeros(model.nstates, 1);
% model.end(1) = 1;

%%

model = struct();

model.states(1).func = @(x) halfCircularUniform(x(1, :)) .* (1 - x(2, :));
model.states(1).title = '-';
model.states(1).type = 0;

%model.states(2).func = @(x) halfCircularExp(x(1, :), pi/2);
model.states(2).func = @(x) VonMisesUniformHalfDistribution(x(1, :), 0, VonMisesRationPred * refDistance ./ x(3,:), VonMisesBasePred);
model.states(2).title = 'pred';
model.states(2).type = 1;

%model.states(3).func = @(x) halfCircularExp(pi - x(1, :), pi/2);
model.states(3).func = @(x) VonMisesUniformHalfDistribution(x(1, :), pi, VonMisesRationPrey * refDistance ./ x(3,:), VonMisesBasePrey);
model.states(3).title = 'prey';
model.states(3).type = 1;

model.states(4).func = @(x) halfCircularUniform(x(1, :)) .* x(2, :);
model.states(4).title = 'cont';
model.states(4).type = 0;

%model.states(5).func = @(x) halfCircularExp(x(1, :), pi/2);
model.states(5).func = @(x) VonMisesUniformHalfDistribution(x(1, :), 0, VonMisesRationPred * refDistance ./ x(3,:), VonMisesBasePred);
model.states(5).title = 'pred';
model.states(5).type = 1;

%model.states(6).func = @(x) halfCircularExp(pi - x(1, :), pi/2);
model.states(6).func = @(x) VonMisesUniformHalfDistribution(x(1, :), pi, VonMisesRationPrey * refDistance ./ x(3,:), VonMisesBasePrey);
model.states(6).title = 'prey';
model.states(6).type = 1;

% model.states(1).func = @(x) CircularUniform(x(1, :)) .* (1 - x(2, :));
% model.states(1).title = '-';
% 
% %model.states(2).func = @(x) halfCircularExp(x(1, :), pi/2);
% model.states(2).func = @(x) VonMisesUniformDistribution(x(1, :), 0, VonMisesRation, VonMisesBase);
% model.states(2).title = 'pred';
% 
% %model.states(3).func = @(x) halfCircularExp(pi - x(1, :), pi/2);
% model.states(3).func = @(x) VonMisesUniformDistribution(x(1, :), pi, VonMisesRation, VonMisesBase);
% model.states(3).title = 'prey';
% 
% model.states(4).func = @(x) CircularUniform(x(1, :)) .* x(2, :);
% model.states(4).title = 'cont';
% 
% %model.states(5).func = @(x) halfCircularExp(x(1, :), pi/2);
% model.states(5).func = @(x) VonMisesUniformDistribution(x(1, :), 0, VonMisesRation, VonMisesBase);
% model.states(5).title = 'pred';
% 
% %model.states(6).func = @(x) halfCircularExp(pi - x(1, :), pi/2);
% model.states(6).func = @(x) VonMisesUniformDistribution(x(1, :), pi, VonMisesRation, VonMisesBase);
% model.states(6).title = 'prey';

persistence = 10;
pst = 1-1/persistence;
ptr = 1/persistence;
pst = 1;
ptr = 1;
model.trans = [...
    pst ptr ptr ptr 0.0 0.0; % -
    0.0 pst 0.0 ptr 0.0 0.0; % predator
    0.0 0.0 pst ptr 0.0 0.0; % prey
    ptr 0.0 0.0 pst ptr ptr; % contact
    ptr 0.0 0.0 0.0 pst 0.0; % predator
    ptr 0.0 0.0 0.0 0.0 pst; % prey
    ];
model = validateModel(model);

model.start = zeros(model.nstates, 1);
model.start(1) = 1;
model.start(4) = 1;
model.end = zeros(model.nstates, 1);
model.end(1) = 1;
model.end(4) = 1;

%model.end = ones(model.nstates, 1);

if 1 == 2
    dx= 0.001;
    x = 0:dx:2*pi;
%     y1 = model.states(2).func(x);
%     y2 = halfCircularUniform(x);
    
    %y1 = halfCircularExp(x, pi/3);
    y1 = CircularUniform(x);
    y2 = CircularUniform(x);
    y3 = VonMisesUniformDistribution(x, 0, .05, .1);

    %y3 = VonMisesUniformHalfDistribution(x, 0, .1);
    %
    %plot(x1 / pi * 180, n_mean / (x1(2)-x1(1)), 'r', x2 / pi * 180, n_contact/ (x2(2)-x2(1)), 'g')
    %hold on;
    plot(x / pi * 180, y1, 'r', x / pi * 180, y2, 'g', x / pi * 180, y3, 'm');
    %hold off;
end

%%
global orig;
currAngles = shiftdim(analyzed.angle(m1, m2, :))';
currContacts = shiftdim(analyzed.contacts(min(m1, m2), max(m1, m2), :))';
currDistance = shiftdim(orig.miceDistance(min(m1, m2), max(m1, m2), :))';
input = [...
    currAngles;
    currContacts;
    currDistance
    ];
%valid = ~isnan(input(1, :));
valid = ~analyzed.hidden(m1, :) & ~analyzed.hidden(m2, :);
%input(1, isnan(input(1, :))) = pi/2;

% input = [input(1, valid); input(2, valid)];

if 1 == 1
    c = conv([0 valid 0], [1 -1]);
    begF = find(c > 0) - 1;
    endF = find(c < 0) - 2;
    inputs = cell(1, length(begF));
    for i=1:length(begF)
        inputs{i} = [input(1, begF(i):endF(i)); input(2, begF(i):endF(i)); input(3, begF(i):endF(i))];
    end
    
    backtrack = ModelViterbiSequence(model, inputs, false);
    
    state = ones(1, analyzed.nData);
    for i=1:length(begF)
        currBacktrack = backtrack{i};
        currBacktrack(currBacktrack == 1 & currContacts(begF(i):endF(i)) == 1) = 4;
        state(begF(i):endF(i)) = currBacktrack;
    end
else
    backtrack = ModelViterbiSequence(model, input);
    state = backtrack{1};
end

%state(valid) = backtrack;
%% remove short events
for i=2:model.nstates 
    if model.states(i).type == 1
        c = conv([0 (state==i)  0], [1 -1]);
        startFrame = find(c > 0) - 1;
        endFrame   = find(c < 0) - 2;
        duration = endFrame - startFrame + 1;
        for l=find(duration < options.minEventDuration)
            state(startFrame(l):endFrame(l)) = 1;
        end
    end
end

%%
c = conv([0 (state~=1)  0], [1 -1]);
events.startFrame = find(c > 0) - 1;
events.endFrame   = find(c < 0) - 2;
events.startTime  = events.startFrame * (analyzed.time(2) - analyzed.time(1));
events.endTime    = events.endFrame * (analyzed.time(2) - analyzed.time(1));

for i=1:length(events.startFrame)
    prev = -1;
    events.title{i} = '';
    index = 1;
    for j=events.startFrame(i):events.endFrame(i)
        if prev ~= state(j);
            if index == 1;
                events.title{i} = model.states(state(j)).title;
                events.epoch{i}.startFrame(index) = j;
            else
                events.title{i} = [events.title{i} ' -> ' model.states(state(j)).title];
                events.epoch{i}.startFrame(index) = j;
                events.epoch{i}.endFrame(index-1) = j - 1;
            end
            events.epoch{i}.title{index} = model.states(state(j)).title;
            index = index + 1;
            prev = state(j);
        end
    end
    events.epoch{i}.endFrame(index-1) = events.endFrame(i);
end
