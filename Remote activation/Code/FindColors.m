function FindColors(obj)


centerpixel = [];
qh = [];
qs = [];
qv = [];
count_ = [];
nframes = 0;

bins = sequence(0, 1, obj.nColorBins);
obj.Printf(1, 'trying to match %d valid frames', obj.nColorFrames);
obj.Reprintf(1, 'processed (%3d%%)', 0);
while nframes < obj.nColorFrames
    
    m1 = 1; %nFrames * 1/3;
    m2 = nFrames; %nFrames * 2/3;
    r = floor(rand(1) * (m2 - m1) + m1);
    
    if obj.dt < obj.StartTime; continue; end
    if obj.EndTime > 0 && r * obj.dt > obj.EndTime; continue; end
    
    m = obj.GetFrame(r);
    bw = obj.SimpleSegmentFrame(m);
    
    labels = bwlabel(bw);
    count = max(labels(:));
    if obj.Output
        subplot(2,3,2);
        imagesc(m);
        title('colors (failed)');
        subplot(2,3,5);
        imagesc(label2rgb(labels));
        subplot(2,3,4);
        imagesc(orig);
        drawnow;
    end
    %%
    if count < max(obj.MinNumOfObjects, 1)
        continue;
    end
    %%
    props = regionprops(labels, 'Centroid', 'PixelList', 'Solidity');
    for i=1:length(props)
        if props(i).Solidity < obj.MinSolidity || sum(labels(:) == i) < obj.MinNumOfPixels
            labels(labels == i) = 0;
            count = count - 1;
        end
    end
    if count < max(obj.MinNumOfObjects, 1)
        continue;
    end
    obj.Reprintf(1, 'processed (%3d%%)', round((nframes+1)/obj.nColorFrames * 100));
    %
    if obj.Output
        subplot(2,3,3);
        imagesc(m);
        title('colors');
        drawnow;
    end
    %
    mhsv = rgb2hsv(m);
    h = mhsv(:, :, 1);
    s = mhsv(:, :, 2);
    v = mhsv(:, :, 3);
    
    for l=unique(labels(labels > 0))'
        %             qh = [qh; histc(h(labels == l), bins)'];
        %             qs = [qs; histc(s(labels == l), bins)'];
        %             qv = [qv; histc(v(labels == l), bins)'];
        qh_ = histc(h(labels == l), bins)' + 1; qh_=qh_/sum(qh_); qh = [qh; qh_];
        qs_ = histc(s(labels == l), bins)' + 1; qs_=qs_/sum(qs_); qs = [qs; qs_];
        qv_ = histc(v(labels == l), bins)' + 1; qv_=qv_/sum(qv_); qv = [qv; qv_];
        
        count_ = [count_, count];
    end
    %%
    for r=unique(labels(labels > 0))'
        coord = round(props(r).Centroid);
        if any(coord(1) == props(r).PixelList(:, 1) & coord(2) == props(r).PixelList(:, 2))
            centerpixel = [centerpixel; h(coord(2), coord(1)), s(coord(2), coord(1)), v(coord(2), coord(1))];
        else
            centerpixel = [centerpixel; -1, -1, -1];
        end
    end
    %%
    nframes = nframes + 1/count;
end

%%
obj.Printf(1, 'clustering colors');
minD = inf;
minDRGB = inf;
minIdx = [];
for i=1:obj.nKMeansRepeats
    [idx, currColors, subjectsD, D] = kmeans([qh, qs, qv], obj.nSubjects, 'emptyaction', 'drop');
    if any(isnan(currColors))
        continue;
    end
    weightedD = sum(min(D, [], 2));
    if weightedD < minD
        minD = weightedD;
        subjectColors = currColors;
        minIdx = idx;
    end
end
if ~isfinite(minD)
    error 'clustering failed';
end
%%
subjectCenterColors = [];
for j=1:3
    for i=1:obj.nSubjects
        currIdxs = find(minIdx == i);
        c = centerpixel(currIdxs,j); % .* count_(currIdxs)' / options.nSubjects;
        c = c(c>=0);
        subjectCenterColors(i, 1, j) = median(c);
    end
end
subjectCenterColorsImg = hsv2rgb(subjectCenterColors);
subject.centerColors = reshape(subjectCenterColorsImg, obj.nSubjects, 3);
if obj.Output
    subplot(2,3,3);
    imagesc(subjectCenterColorsImg);
    title('Center Colors');
    axis on
    set(gca, 'XTick', []);
end

cmap = subject.meanColor;
colormap(cmap);
if obj.Output
    for i=1:obj.nSubjects
        subplot(2,3,4);
        plot(bins, subject.h(i, :)', 'Color', cmap(i, :)); hold on;
        title('Hue');
        subplot(2,3,5);
        plot(bins, subject.s(i, :)', 'Color', cmap(i, :)); hold on;
        title('Saturation');
        subplot(2,3,6);
        plot(bins, subject.v(i, :)', 'Color', cmap(i, :)); hold on;
        title('Value');
    end
end
subject.colorBins = bins;
subject.color = subject.centerColors;

obj.Colors = subject.centerColors;
obj.ColorBins = bins;