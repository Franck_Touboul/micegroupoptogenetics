%% parameters
avgWin = 1; % average on last iterations
%generateMeshGrid = 1;

warning 'using only last error result and stdev (and not stderr)';

%%
s = {};
s = parametrize(s, p, 1);

Probs = {};
localProb       = zeros(1, length(s));
localProbStdErr = zeros(1, length(s));
localGrid = {};
LearningCurve = zeros(length(s), length(totalProb{1}{1}));
Count = zeros(length(s));
TimeScales = zeros(1, length(s));

for i=1:length(s)
    l = length(totalProb{i});
    m = zeros(1, l);
    for j=1:l
        a = totalProb{i}{j};
        if (any(a ~= 0))
            LearningCurve(i, :) = LearningCurve(i, :) + a;
            Count(i) = Count(i) + 1;
            %estimated = MyExpFit(1:length(a), a);            
            %TimeScales(i) = TimeScales(i) + 1/estimated(3);
        end
        if ~isempty(a)
            m(j) = mean(a(end-avgWin:end));
        else
            m = [0];
        end
    end
    LearningCurve(i, :) = LearningCurve(i, :) / Count(i);
    TimeScales(i) = TimeScales(i) / Count(i);
    Probs{i} = m;
    localProbStdErr(i) = std(m) / sqrt(l);
    localProb(i) = mean(m);
    localGrid{i} = '(';
    for j=1:length(s{i})
        if j > 1
            localGrid{i} = [localGrid{i} '/ '];
        end
        localGrid{i} = [localGrid{i} mat2str(s{i}{j})];
    end
    localGrid{i} = [localGrid{i}, ')'];
end
%%
if (length(p) == 1)
    %x = [1:(length(localProb) - 1), (length(localProb) + 1)];
    x = [1:length(localProb)];
    errorbar(x, localProb, localProbStdErr, 'k.');
    hold on;
    h = bar(x, localProb);
    set(gca, 'XTick', x);
    if isfield(p{1}, 'inf') && p{1}.inf
        set(gca, 'XTickLabel', {localGrid{1:end-1}, ['Inf (', num2str(localGrid{end}, '%g'), ')']});
    else
        set(gca, 'XTickLabel', localGrid);
    end
    if isfield(p{1}, 'title')
        xlabel(p{1}.title);
    end
    stylizeFigure;
    hold off;
end
%%
if (length(p) == 2)
    subplot(4,4,[1:2, 5:6]);
    x1 = [1:length(p{1}.value)];
    l1 = [];
    for i=x1
        l1 = [l1, (p{1}.value{i})];
    end
    x2 = [1:length(p{2}.value)];
    l2 = [];
    for i=x2
        l2 = [l2, (p{2}.value{i})];
    end
    imagesc(reshape(localProb, length(p{2}.value), length(p{1}.value)));
    set(gca, 'XTick', x1);
    set(gca, 'XTickLabel', l1);
    xlabel(p{1}.title);
    set(gca, 'YTick', x2);
    set(gca, 'YTickLabel', l2);
    ylabel(p{2}.title);
    colorbar;
    title('Phase Plot');
    subplot(4,4,[3:4, 7:8]);
else
    subplot(4,4,1:8);
end
param_titles = '';
for i=1:length(p)
    if i > 1
        param_titles = [param_titles, ', '];
    end
    param_titles = [param_titles, p{i}.title];
end
h = barweb(localProb', localProbStdErr', [], localGrid, []);
errorbar_tick(h.errors, length(s) * 1.5);
rotateticklabel(h.ca, 45);
hold on;
for i=1:length(s)
    plot(i, Probs{i}, 'g.', 'MarkerSize', 10);
end
hold off;
ylabel('error');
xlabel('parameters');
title(['Average Error (' param_titles ')']);
subplot(4,4,[9:10, 13:14]);
% bar3(LearningCurve); 
% set(gca, 'YTick', 1:length(s), 'YTickLabel', localGrid);
% ylabel(['(' param_titles ')']);
% xlabel('iteration');
imagesc(LearningCurve);
set(gca, 'YTick', 1:length(s), 'YTickLabel', localGrid);
ylabel(['parameters (' param_titles ')']);
xlabel('iteration');
title('Average Error per Iteration');

subplot(4,4,11:12);
times = [];
asymp = [];
for i=1:length(s)
    estimated = MyExpFit(1:length(LearningCurve(i, :)), LearningCurve(i, :));
    times = [times, 1/estimated(3)];
    asymp = [asymp, estimated(2)];
end
bar(times);
set(gca, 'XTick', 1:length(s), 'XTickLabel', localGrid);
title('Learning Time Scale');
% hold on;
% plot(1:length(s), TimeScales, 'rx');
% hold off;
subplot(4,4,15:16);
bar(asymp);
set(gca, 'XTick', 1:length(s), 'XTickLabel', localGrid);
title('Asymptotic Error');


%plot3(localGrid(:,1), localGrid(:,2), localGrid(:,3), 'rx');
%%
% if (generateMeshGrid)
%     m = {};
%     x = cell(1, length(p));
%     for i=1:length(p)
%         m{i} = [];
%         for j=1:length(p{i}.value)
%             m{i} = [m{i}, p{i}.value{j}];
%         end
%     end
%     [x{:}] = ndgrid(m{:});
% end