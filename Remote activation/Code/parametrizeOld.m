function s = parametrize(s, p, i)
if nargin == 2
    i = 1;
end
if (i == length(p))
    for j=1:length(p{i}.value)
        s{end+1} = { p{i}.value{j} };
    end
else
    for j=1:length(p{i}.value)
        mark = length(s) + 1;
        s = parametrize(s, p, i+1);
        for k=mark:length(s)
            s{k}{2:end + 1} = s{k};
            s{k}{1} = num2cell(p{i}.value{j});
        end
    end
end
