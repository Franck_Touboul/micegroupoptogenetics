function SocialOverlayHierarchy(obj, data, options)
if ~exist('options', 'var')
    options = [];
end
options = setDefaultParameters(options, 'isByDay', false, 'isNormalized', false, 'setDefaultParameters', false);
if options.isNormalized
    options = setDefaultParameters(options, ...
        'ColorEdges', true, ...
        'NodeSize', 25, ...
        'EdgeMinWidth', .25, ...
        'EdgeMaxWidth', 20, ...
        'EdgeMaxValue', 1, ...
        'EdgeShift', .075);
else
    options = setDefaultParameters(options, ...
        'ColorEdges', true, ...
        'NodeSize', 25, ...
        'EdgeMinWidth', .25, ...
        'EdgeMaxWidth', 8, ...
        'EdgeMaxValue', 4, ...
        'EdgeShift', .075);
end
%%
cmap = MyMouseColormap;
dx = [0 1 -1 0];
titles = {'\alpha', '\beta', '\gamma', '\delta'};

%%
level = max(obj.Hierarchy.Group.AggressiveChase.rank) - obj.Hierarchy.Group.AggressiveChase.rank + 1;
offset = 0;
X = [];
Y = [];
for l=unique(level)
    map = level==l;
    idx = 0;
    for i=find(map)
        a = 2 * mod(l, 2) - 1;
        X(i) = offset + dx(l) + a * idx;
        Y(i) = -l;
        idx = idx + 1;
    end
end

for i=1:obj.nSubjects
    for j=1:obj.nSubjects
        
        vec = [X(i)-X(j) Y(i)-Y(j)];
        edgeFactor = 2 / (1 + abs(data(i, j) / data(j, i)));
        if isnan(edgeFactor);
            edgeFactor = 0;
        end;
        es = options.EdgeShift * edgeFactor;
        vec = vec / sqrt(vec*vec') * es;
        %width = data(i, j) / options.EdgeMaxValue * (options.EdgeMaxWidth);
        d = abs(data(i, j));
        width = d / max(options.EdgeMaxValue) * options.EdgeMaxWidth;
        width = min(width, options.EdgeMaxWidth);
        width = max(width, options.EdgeMinWidth);
        if data(i, j) == 0
            continue;
        end
        if options.ColorEdges
            c = cmap(i, :);
        else
            c = [0 0 0];
        end
        if data(i, j) > 0
            plot([X(i) X(j)] + vec(2), [Y(i) Y(j)] - vec(1), '-', 'Color', c, 'LineWidth', width); hon
        else
            plot([X(i) X(j)] + vec(2), [Y(i) Y(j)] - vec(1), '--', 'Color', c, 'LineWidth', width); hon
        end
    end
end

for i=1:obj.nSubjects
    plot(X(i), Y(i), 'o', 'MarkerEdgeColor', 'none', 'MarkerFaceColor', cmap(i, :), 'MarkerSize', options.NodeSize); hon;
    text(X(i), Y(i), titles{level(i)}, 'verticalAlignment', 'middle', 'horizontalalignment', 'center');
end
hoff;
set(gcf, 'Color', 'w', 'Units', 'normalized');
axis off;
axis equal
