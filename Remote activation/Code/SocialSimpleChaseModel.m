local = [];
local.NumNeighbors = 10;
local.MinNumFrames = 0;
local.TypicalMouseLength = 10; %% pixels
local.MaxDistance = local.TypicalMouseLength * 2;
%%
smooth = TrackSmooth(obj, 3);
ppcmy =  diff(quantile(obj.y(obj.zones==1), [0.005 0.9995])) / 50;
ppcmx =  diff(quantile(obj.x(obj.zones==1), [0.005 0.9995])) / 70;
PixPerCM = mean([ppcmy, ppcmx]);
indices = 1:obj.nFrames;

for s1=1:obj.nSubjects
    for s2=s1+1:obj.nSubjects
        %%
        valid = ~obj.sheltered(s1, :) & ~obj.sheltered(s2, :) & obj.valid &...
            (obj.x(s1, :) ~= obj.x(s2, :) | obj.y(s1, :) ~= obj.y(s2, :));
        X = [smooth.x(s1, :); smooth.x(s2, :)];
        Y = [smooth.y(s1, :); smooth.y(s2, :)];
        x = X(:, valid);
        y = Y(:, valid);
        %
        jaxis = [diff(x); diff(y)];
        njaxis = jaxis ./ repmat(sqrt(sum(jaxis.^2)), 2, 1);
        
        %         speed1 = [[0 diff(sx(1, :))]; [0 diff(sy(1, :))]];
        %         speed2 = [[0 diff(sx(2, :))]; [0 diff(sy(2, :))]];
        [bf, ef, lf] = FindEvents(valid);
        speedvec1 = zeros(2, sum(valid));
        speedvec2 = zeros(2, sum(valid));
        offset = 1;
        for i=1:length(bf)
            speedvec1(:, offset:offset+lf(i)-1) = ...
                [gradient(X(1, bf(i):ef(i))); gradient(Y(1, bf(i):ef(i)))];
            speedvec2(:, offset:offset+lf(i)-1) = ...
                [gradient(X(2, bf(i):ef(i))); gradient(Y(2, bf(i):ef(i)))];
            offset = offset + lf(i);
        end
        
        local.speed{s1, s2} =  sum(njaxis .* speedvec1) / obj.dt / PixPerCM;
        local.speed{s2, s1} = -sum(njaxis .* speedvec2) / obj.dt / PixPerCM;
        local.distance{s1, s2} = sqrt(sum(jaxis.^2)) / PixPerCM;
        local.distance{s2, s1} = sqrt(sum(jaxis.^2)) / PixPerCM;
        local.indices{s1, s2} = indices(valid);
        local.indices{s2, s1} = indices(valid);
    end
end
%% filter frames by speed correlations
s1 = 1;
s2 = 2;
spd1 = local.speed{s1, s2};
spd2 = local.speed{s2, s1};
dist12 = local.distance{s1, s2};

chases = SocialSimpleChaseFindCorrelated(local, s1, s2);

subplot(2,2,1);
h1 = plot(spd1(~chases), spd2(~chases), '.');
customDataCursor(h1, -local.indices{s1, s2}(~chases));
hon
h2 = plot(spd1(chases), spd2(chases), 'r.');
customDataCursor(h2, local.indices{s1, s2}(chases));
hoff
xaxis(-100, 100);
yaxis(-100, 100);

%% filter events shorter the MinNumFrames
if local.MinNumFrames > 0
    continous = false(1, length(chases));
    continous(chases) = [false diff(local.indices{s1, s2}(chases)) == 1];
    
    [bf, ef, lf] = FindEvents(continous);
    for i=1:length(bf)
        chases(bf(i):ef(i)) = lf(i) + 1 >= local.MinNumFrames;
    end
end
%% remove events where the mice were far apart
%chases = chases & dist12(:) < local.MaxDistance;

%%
subplot(2,2,1);
h1 = plot(spd1(~chases), spd2(~chases), '.');
customDataCursor(h1, -local.indices{s1, s2}(~chases));
hon
h2 = plot(spd1(chases), spd2(chases), 'r.');
customDataCursor(h2, local.indices{s1, s2}(chases));
hoff
xaxis(-100, 100);
yaxis(-100, 100);

subplot(2,2,3);

h1 = plot(dist12(~chases), spd1(~chases), '.');
customDataCursor(h1, -local.indices{s1, s2}(~chases));
hon
h2 = plot(dist12(chases), spd1(chases), 'r.');
customDataCursor(h2, local.indices{s1, s2}(chases));
%
%x = dist12(chases & spd1(:) > 0)'; 
%p = polyfit(x, spd1(chases & spd1(:) > 0)', 1); 
%ux = unique(x); 
%plot(ux, polyval(p, ux),'k');
%
hoff
yaxis(-100, 100);

subplot(2,2,4);
h1 = plot(dist12(~chases), spd2(~chases), '.');
customDataCursor(h1, -local.indices{s1, s2}(~chases));
hon
h2 = plot(dist12(chases), spd2(chases), 'r.');
customDataCursor(h2, local.indices{s1, s2}(chases));
hoff
%xaxis(-100, 100);
yaxis(-100, 100);

%%
x = double(dist12);
y = double(spd1);

x = [x, x];
y = [y, -y];

bilin = struct();
bilin.s = mad(y) * 1/norminv(3/4) * [1 1 1];
bilin.b1 = [1 0 -1];
bilin.b0 = [0 0 0];

nchars = 0;
d = [];
for iter = 1:100
    nchars = Reprintf(nchars, '# iter no. %d/100', iter);
    for i=1:3
        d(i, :) = 1/(bilin.s(i) * sqrt(2*pi)) * exp(.5 * (y - (x * bilin.b1(i) + bilin.b0(i))) .^ 2 / bilin.s(i)^2);
    end
    [~, idx] = min(d);
    %
    map = idx == 2;
    b = robustfit(x(map), y(map));
    bilin.b0(2) = b(1);
    bilin.b1(2) = b(2);
    bilin.s(2) = mad(y(map) - (x(map) * b(2) + b(1))) * 1/norminv(3/4);
    %
    map1 = idx == 1;
    map3 = idx == 3;
    b = robustfit([x(map1), x(map3)], [y(map1), -y(map3)]);
    bilin.b0(1) = b(1);
    bilin.b1(1) = b(2);
    bilin.s(1) = mad([...
        +y(map1) - (x(map1) * b(2) + b(1)),...
        -y(map3) - (x(map3) * b(2) + b(1))]) * 1/norminv(3/4);
    bilin.b0(3) = -bil\in.b0(1);
    bilin.b1(3) = -bilin.b1(1);
    bilin.s(3) = bilin.s(1);
end
fprintf('\n');
%%
%%
x = double(dist12(chases));
y = double(spd1(chases));
x = [x, x];
y = [y, -y];

X = [];
Y = [];

win = 10;
jump = 1;
idx = 1;
for i=0:jump:100
    i
    map = y > i & y < i + win;
    m = median(x(map));
    Y(idx) = i+win/2;
    X(idx) = m;
    idx = idx +1;
end
plot(Y, X, 'ko');

opts = statset('nlinfit');
opts.RobustWgtFun = 'bisquare';

b = nlinfit(Y, X, @MyBilinear, [X(1) -1 X(end) +1], opts);
hon
plot(Y, MyBilinear(b, Y))
hoff
