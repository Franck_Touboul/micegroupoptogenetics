d = dlmread('aux/SocialSimulatePottsTrain.SC.exp0002.day01020304.cam01');
aux.method = d(:, 1);
aux.fval = d(:, 2:500);
aux.times = d(:, 501:end);
aux.titles = {'GIS', 'BFGS', 'GD', 'L-BFGS'};
%%
cmap = MyCategoricalColormap;
u_ = unique(aux.method);
titles = {};
for u=u_(:)'
    idx = aux.method == u;
    serr = [];
    for i=1:size(aux.fval, 2)
        f = aux.fval(idx, i);
        f = f(~isnan(f));
        serr(i) = stderr(f);
    end
    r = 2:26;
    v = validmean(aux.fval(idx, :));
    t = validmean(aux.times(idx, :));
    %myCofidencePlot(r, v(r)+serr(r), v(r)-serr(r)); hon
    
    subplot(2,1,1);
    plot(v(r), 'color', cmap(u, :), 'LineWidth', 2); hon;

    subplot(2,1,2);
    plot(t(r), v(r), 'color', cmap(u, :), 'LineWidth', 2); hon;

    titles = {titles{:}, aux.titles{u}};
end
subplot(2,1,1);
hoff
legend(titles);
legend boxoff
xlabel('iteration');
ylabel('p_{empir} log p_{model}');