function states = MChainConstrainGenerate(len, trans, validPatterns, start)
ctrans = cumsum(trans,2);
states = zeros(len, size(trans, 3));
states(1, :) = start(:)';
for i=2:len
    r = rand(1);
    s = states(i-1, :);
    p = ones(1, size(validPatterns, 1));
    for j=1:size(trans, 3)
        p = p .* trans(s(j), validPatterns(:, j), j);
    end
    c = cumsum(p) / sum(p);
    states(i, :) = validPatterns(sum(r > c) + 1, :);
end
