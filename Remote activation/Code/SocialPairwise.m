
map = nan(obj.ROI.nZones* obj.ROI.nZones, obj.nSubjects * obj.ROI.nZones);
for s=1:obj.nSubjects
    for i=1:length(obj.Analysis.Potts.Model{2}.labels)
        label = obj.Analysis.Potts.Model{2}.labels{i};
        if size(label, 2) == 2 && any(label(1, :) == s)
            i1 = find(label(1, :) == s);
            i2 = find(label(1, :) ~= s);
            r1 = (s - 1) * obj.ROI.nZones + label(2, i1);
            r2 = (label(1, i2) - 1) * obj.ROI.nZones + label(2, i2);
            map(r1, r2) = obj.Analysis.Potts.Model{2}.weights(i);
        end
    end
end

%%
nr = obj.ROI.nZones * obj.nSubjects;
small = 28;
large = 20;
maxwidth = 5;
smap = sort(map(~isnan(map)), 'descend');
thresh = smap(floor(length(smap) * .95));
cmap = [202,152,227; 196,77,88; 253,189,51; 179,224,110]/256;
for s=1:obj.nSubjects
    others = 1:obj.nSubjects;
    others = others(others ~= s);
    for i=1:obj.ROI.nZones
        r1 = (s - 1) * obj.ROI.nZones + i;
        for j=1:obj.ROI.nZones
           for s2=others
                r2 = (s2 - 1) * obj.ROI.nZones + j;
                a1 = r1 / nr * 2 * pi;
                a2 = r2 / nr * 2 * pi;
                x1 = cos(a1) * large;
                x2 = cos(a2) * large;
                y1 = sin(a1) * large;
                y2 = sin(a2) * large;
                if map(r1, r2) < thresh
                    width = max(maxwidth * (map(r1, r2) - thresh) / (smap(end) - thresh), 1);
                    plot([x1 x2], [y1 y2], '-', 'Color', cmap(s2, :), 'LineWidth', width); hold on;
                end
                plot(x1, y1, 'o', 'MarkerFaceColor', 'w', 'MarkerEdgeColor', cmap(s, :), 'MarkerSize', small);
                text(x1, y1, num2str(i), 'HorizontalAlignment', 'Center', 'VerticalAlignment', 'middle', 'Color', 'w', 'FontSize', 22, 'FontWeight', 'bold', 'FontName', 'Arial')
                hold on;
                plot(x2, y2, 'o', 'MarkerFaceColor', cmap(s2, :), 'MarkerEdgeColor', 'none', 'MarkerSize', small);
                text(x2, y2, num2str(j), 'HorizontalAlignment', 'Center', 'VerticalAlignment', 'middle', 'Color', 'w', 'FontSize', 22, 'FontWeight', 'bold', 'FontName', 'Arial')
                
           end
        end
    end
    hold off;
    axis off;
    axis equal;
    axis square;
    saveFigure(['Res/' obj.FilePrefix '.SocialPairwise.' num2str(s)]);
end
return;
%%
cmap = obj.Colors.Centers;
small = 4;
large = 20;
nr = obj.nSubjects * obj.ROI.nZones;
for s=1%:obj.nSubjects
    
    for i=1:length(obj.Analysis.Potts.Model{2}.labels)
        label = obj.Analysis.Potts.Model{2}.labels{i};
        if size(label, 2) == 2
            if any(label(1, :) == s)
                [r1 r2]
                r1 = (label(1, 1) - 1) * obj.ROI.nZones + label(2, 1);
                r2 = (label(1, 2) - 1) * obj.ROI.nZones + label(2, 2);
                a1 = r1 / nr * 2 * pi;
                a2 = r2 / nr * 2 * pi;
                x1 = cos(a1) * large;
                x2 = cos(a2) * large;
                y1 = sin(a1) * large;
                y2 = sin(a2) * large;
                plot([x1 x2], [y1 y2], 'r-'); hold on;
                plot(x1, y1, 'o', 'MarkerFaceColor', cmap(label(1, 1), :), 'MarkerEdgeColor', 'none'); 
                %plot(x2, y2, 'o', 'MarkerFaceColor', cmap(label(1, 2), :), 'MarkerEdgeColor', 'none'); 
            end
        end
    end
    obj.Analysis.Potts.Model{2}.weights
end
hold off;

return;
%%
options.n = 2;
options.nSubjects = 4;           % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts
options.nIters = 50;
options.confidenceIters = 0;

%% Loading the Excel file for all mice (if needed)
loadData;
options.count = zones.count;
%data = zones.all(:,1:floor(end/4));
%data = zones.all(:,floor(3/4*end):end);
%

%% train potts model
data = zones.all;
%data = testPottsModel(options, size(zones.all, 2));
me = trainPottsModel(options, data);

%% compute independent probabilities
independentProbs = computeIndependentProbs(data, zones.count);

%% compute joint probabilities
[jointProbs, jointPci] = computeJointProbs(data, zones.count);

%% compute probability for each sample
fprintf('# computing joint probabilities : ');
[sampleProbIndep, sampleProbJoint] = computeSampleProb(data, zones.count, independentProbs, jointProbs);
fprintf('mean log-likelihood = %6.4f\n', mean(log(sampleProbJoint)));
%% compute MaxEnt probabilities for each sample
fprintf('# computing max-entropy probabilities\n');
p = exp(me.features * me.weights');
perms_p = exp(me.perms * me.weights');
Z = sum(perms_p);
p = p / Z;
%% plot
    subplot(2,2,1);
    b1 = sampleProbJoint;
    b2 = sampleProbIndep;
    
    p1 = sampleProbJoint;
    p2 = p;
    
    %
    if options.confidenceIters > 0
        x = conf.p; rx = flipdim(x, 2);
        y = conf.mean + 2 * conf.std;
        ry = flipdim(conf.mean - 2 * conf.std, 2);
        ry(ry <= 0) = realmin;
        
        fill([x, rx(ry > 0), x(1)], [y, ry(ry > 0), y(1)], [237, 116, 116]/256, 'EdgeColor', 'None');
        %    fill([x, rx(ry > 0), x(1)], [y, ry(ry > 0), y(1)], [247, 218, 124]/256, 'EdgeColor', 'None');
        hold on;
    end
    h1 = plot(b1, b2, '.', 'Color', [.6 .6 .6]);
    hold on;
    h2 = plot(p1, p2, '.');
    hold off;
    set(gca, 'YScale', 'log', 'XScale', 'log');
    %
    
    minLog = floor(log10(min(min(p1), min(p2))));
    maxLog = ceil(log10(max(max(p1), max(p2))));
    axis([10^minLog 10^maxLog 10^minLog 10^maxLog]);
    line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);
    
    axis([...s
        10^floor(log10(min(p1))) ...
        10^ceil(log10(max(p1))) ...
        10^floor(log10(min(p2))) ...
        10^ceil(log10(max(p2))) ...
        ]);
    
    tit = ['Maximum-Entropy Model (me-prob=' num2str(mean(log(p))) ', joint-prob' num2str(mean(log(sampleProbJoint))), ')'];
    title(tit);
    xlabel('Joint');
    ylabel('ME');
    
    %prettyPlot(tit, 'Joint', 'ME');
    legend([h1, h2], 'Independent', 'ME', 'location', 'SouthEast'); legend boxoff
%%
for i=1:options.nSubjects
    clf
    plotPairwisePotts(i, orig, me);
    axis off
    saveFigure(['res\int-map-for' num2str(i) ''])
end
return

%% compute confidence
fprintf('# computing confidence:\n');
conf_options = options;
conf_options.output = false;
rand_p_ = [];
rand_sampleProbJoint_ = [];
for iter=1:options.confidenceIters
    fprintf('# - iter (%4d/%4d)\n', iter, options.confidenceIters);
    fprintf('#  o generating data\n');
    rand_data = testPottsModel(conf_options, size(zones.all, 2));
    fprintf('#  o training data\n');
    rand_me = trainPottsModel(conf_options, rand_data);
    %
    rand_p = exp(rand_me.features * rand_me.weights');
    rand_perms_p = exp(rand_me.perms * rand_me.weights');
    rand_Z = sum(rand_perms_p);
    rand_p = rand_p / rand_Z;
    rand_p_ = [rand_p_, rand_p];
    %
    fprintf('#  o computing joint prob\n');
    [rand_sampleProbIndep, rand_sampleProbJoint] = computeSampleProb(rand_data, zones.count);
    rand_sampleProbJoint_ = [rand_sampleProbJoint_, rand_sampleProbJoint];
end

conf.p = unique(rand_sampleProbJoint_);
conf.vals = {};
i = 1;
for u=conf.p
    conf.vals{i} = rand_p_(rand_sampleProbJoint_ == u)';
    i = i + 1;
end
conf.mean = [];
conf.std = [];
for i=1:length(conf.vals)
    conf.mean(i) = mean(conf.vals{i});
    conf.std(i) = std(conf.vals{i});
end
fprintf('# done\n');
%% plot
for num=1:4
    load(['res\45min-potts-n' num2str(num) ''])
    
    clf
    subplot(2,2,1);
    b1 = sampleProbJoint;
    b2 = sampleProbIndep;
    
    p1 = sampleProbJoint;
    p2 = p;
    
    %
    if options.confidenceIters > 0
        x = conf.p; rx = flipdim(x, 2);
        y = conf.mean + 2 * conf.std;
        ry = flipdim(conf.mean - 2 * conf.std, 2);
        ry(ry <= 0) = realmin;
        
        fill([x, rx(ry > 0), x(1)], [y, ry(ry > 0), y(1)], [237, 116, 116]/256, 'EdgeColor', 'None');
        %    fill([x, rx(ry > 0), x(1)], [y, ry(ry > 0), y(1)], [247, 218, 124]/256, 'EdgeColor', 'None');
        hold on;
    end
    h1 = plot(b1, b2, '.', 'Color', [.6 .6 .6]);
    hold on;
    h2 = plot(p1, p2, '.');
    hold off;
    set(gca, 'YScale', 'log', 'XScale', 'log');
    %
    
    minLog = floor(log10(min(min(p1), min(p2))));
    maxLog = ceil(log10(max(max(p1), max(p2))));
    axis([10^minLog 10^maxLog 10^minLog 10^maxLog]);
    line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);
    
    axis([...s
        10^floor(log10(min(p1))) ...
        10^ceil(log10(max(p1))) ...
        10^floor(log10(min(p2))) ...
        10^ceil(log10(max(p2))) ...
        ]);
    
    tit = ['Maximum-Entropy Model (me-prob=' num2str(mean(log(p))) ', joint-prob' num2str(mean(log(sampleProbJoint))), ')'];
    title(tit);
    xlabel('Joint');
    ylabel('ME');
    
    %prettyPlot(tit, 'Joint', 'ME');
    legend([h1, h2], 'Independent', 'ME', 'location', 'SouthEast'); legend boxoff
    
    
    %saveFigure(['res\45min-potts-n' num2str(options.n) ''])
    %save(['res\45min-potts-n' num2str(options.n) ''])
end

%%
conf_options.n = 2;
rand_data = testPottsModel(conf_options, 1000000);
rand_me = trainPottsModel(conf_options, rand_data);
%
rand_p = exp(rand_me.features * rand_me.weights');
rand_perms_p = exp(rand_me.perms * rand_me.weights');
rand_Z = sum(rand_perms_p);
rand_p = rand_p / rand_Z;

p1 = rand_p;
[rand_sampleProbIndep, rand_sampleProbJoint] = computeSampleProb(rand_data, zones.count);
p2 = rand_sampleProbJoint;

h2 = plot(p1, p2, '.');
hold off;
set(gca, 'YScale', 'log', 'XScale', 'log');

minLog = floor(log10(min(min(p1), min(p2))));
maxLog = ceil(log10(max(max(p1), max(p2))));
axis([10^minLog 10^maxLog 10^minLog 10^maxLog]);
line([10^minLog 10^maxLog], [10^minLog 10^maxLog], 'Color', 'k', 'LineWidth', 2);

axis([...s
    10^floor(log10(min(p1))) ...
    10^ceil(log10(max(p1))) ...
    10^floor(log10(min(p2))) ...
    10^ceil(log10(max(p2))) ...
    ]);

title(['Maximum-Entropy Model (me-prob=' num2str(mean(log(p))) ', joint-prob' num2str(mean(log(sampleProbJoint))), ')']);
xlabel('Joint');
ylabel('ME');
