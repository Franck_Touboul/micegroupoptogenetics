SocialExperimentData;
%%
nDays = 4;
days = 1:4;

index = 1;
MI = zeros(nSubjects, nSubjects, length(experiments) * nDays);
ENTROPY = zeros(nSubjects, length(experiments) * nDays);
ID = zeros(1, length(experiments) * nDays);
DAY = zeros(1, length(experiments) * nDays);
RANK = zeros(nSubjects, length(experiments) * nDays);
for id=1:length(experiments)
    for day = days
        prefix = sprintf(experiments{id}, day);
        fprintf('# -> %s\n', prefix);
        orig = TrackLoad(['Res/' prefix '.obj.mat'], {'zones', 'ROI', 'valid', 'Hierarchy'});
        obj = SocialSetTimeScale(orig, 6);
        fprintf('# - compute MI\n');
        [q, mi, entropy] = SocialMutualInformation(obj);
        
        ENTROPY(:, index) = entropy(:);
        MI(:, :, index) = mi;
        DAY(index) = day;
        ID(index) = id;
        RANK(:, index) = obj.Hierarchy.Group.AggressiveChase.rank;
        index = index + 1;
    end
end

%%
for g=1:GroupsData.nGroups
    pairwise = [];
    pairwiseP = [];
    totalPairwiseP = [];
    condP = [];
    for id=Groups(g).idx
        map = ID == id & DAY == day;
        mi = MI(:, :, map);
        entropy = ENTROPY(:, map);
        
        condP = [condP; diag(mi) ./ entropy];
        
        mi(1:obj.nSubjects+1:end) = nan;
        pairwise = [pairwise; mi(:)];
        
        miP = mi ./ repmat(entropy, 1, obj.nSubjects);
        pairwiseP = [pairwiseP; miP(:)];
        
        mi(1:obj.nSubjects+1:end) = 0;
        totalPairwiseP = [totalPairwiseP; sum(mi, 2) ./ entropy];
        
    end
end
plot(totalPairwiseP, condP, 'o', 'MarkerFaceColor', 'g', 'MarkerEdgeColor', 'none'); 
hon
plot([0 0.5], [0 0.5], 'k');
hoff
%%
stat = [];
for g=1:GroupsData.nGroups
    
    pairwise = [];
    pairwiseP = [];
    totalPairwiseP = [];
    condP = [];
    for i=Groups(g).idx
        for day=days(:)'
            %%
            mi = MI(:, :, ID == i & DAY == day);
            entropy = ENTROPY(:, ID == i & DAY == day);
            
            condP = [condP; diag(mi) ./ entropy];
            
            mi(1:obj.nSubjects+1:end) = nan;
            pairwise = [pairwise; mi(:)];
            
            miP = mi ./ repmat(entropy, 1, obj.nSubjects);
            pairwiseP = [pairwiseP; miP(:)];
            
            mi(1:obj.nSubjects+1:end) = 0;
            totalPairwiseP = [totalPairwiseP; sum(mi, 2) ./ entropy];
        end
    end
    
    %%
    style = {'x:'};
    subplot(GroupsData.nGroups,1,g);
    
    nbins = 30;
    m1 = min([min(pairwiseP), min(condP) min(totalPairwiseP)]);
    m2 = max([max(pairwiseP), max(condP) max(totalPairwiseP)]);
    x = sequence(m1, m2, nbins);
    hpw = histc(pairwiseP, x);
    htpw = histc(totalPairwiseP, x);
    hc = histc(condP, x);
    stat(g).pairwiseP = pairwiseP;
    stat(g).totalPairwiseP = totalPairwiseP;
    stat(g).condP = condP;
    
    % [hpw, xpw] = hist(pairwiseP,25);
    % [hc, xc] = hist(condP,25);
    cmap = MySubCategoricalColormap;
    plot(x*100, hpw/sum(hpw)*100, style{:}, 'Color', cmap(2, :), 'LineWidth', 2);
    vert_line(quantile(pairwiseP * 100, [0.25 0.5 0.75]), 'linestyle', '--', 'Color', cmap(2, :));
    hold on;
    plot(x*100, hc/sum(hc)*100, style{:}, 'Color', cmap(4, :), 'LineWidth', 2);
    vert_line(quantile(condP * 100, [0.25 0.5 0.75]), 'linestyle', '--', 'Color', cmap(4, :));
    plot(x*100, htpw/sum(htpw)*100, style{:}, 'Color', cmap(6, :), 'LineWidth', 2);
    vert_line(quantile(totalPairwiseP * 100, [0.25 0.5 0.75]), 'linestyle', '--', 'Color', cmap(6, :));
    hold off;
    prettyPlot('', 'information percentage [%]', 'percentage that fall in bin [%]');
    %legend('Pair: I(x_i; x_j)', 'Multiple: I(x_i; x_{i~=j})');
    legend('single', 'multiple', 'additive');
    legend boxoff;
    a  = axis;
    axis([0 50 a(3) a(4)])
end
if (1==2)
    %%
    p = 'Graphs/SocialCorrelationBatch/';
    mkdir(p);
    figure(1);
    saveFigure([p obj.FilePrefix '']);
end