function n = IndexOf(arr, v)

n = 0;
for i=1:length(arr)
    if ischar(v)
        if strcmp(arr{i}, v)
            n = i;
            return;
        end
    else
        if v == arr(i)
            n = i;
            return;
        end
    end
end