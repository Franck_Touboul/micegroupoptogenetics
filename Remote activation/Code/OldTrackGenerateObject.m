function obj = OldTrackGenerateObject(videoFile)
obj.VideoFile = videoFile;
obj.Name = '';
obj.FilePrefix = 'Trial';
obj.nSubjects = 4;          % the number of subjects in the arena
%%
obj.StartTime = -1; % video start time [sec]
obj.EndTime = -1;   % video end time [sec]
%%
obj.OutputPath = '';
obj.Output = false;
obj.OutputToFile = true;
obj.OutputInOldFormat = true;
%%
obj.ValidROI = [];
%% meta
obj.NoiseThresh = 10;        %
obj.UseAdaptiveThresh = true;        %
%   - background:
obj.nBkgFrames = 40;        % no. of frames to use when computing background image
obj.ErodeRadios = 2;
%   - colors:
obj.ColorsFile = '';
obj.MinNumOfObjects = 4;
obj.nColorFrames = 40;      % no. of frames to use to determine the subject colors
obj.nColorBins = 20;         % no. of bins in color histogram
obj.nKMeansRepeats = 20;     % no. of time to run k-means algorithm for optimal clustring of colors
%% segementation:
obj.MinNumOfPixels = 40;
obj.MinSolidity = .8;          % minimal solidity of segments
%
obj.VideoScale = .25;            % working resolution relative to original movie
obj.MaxNumOfCents = 5;
obj.MaxNumOfObjects = 20;
obj.MinSegmentGap = 3;             % important if there are small hidden areas (like wallthrough walls)
%% tracking
obj.KalmanQ = 8;
obj.KalmanR = 1;
%
obj.MinVisibleDuration = 4; % [frames]
obj.MaxJumpPerStep = 40;
obj.MinHiddenDuration = 4; % [frames]
obj.MaxTotalJump = 200;
obj.MinDistance = 5;
obj.ParallelTrackPadding = 600; % [sec]
%% zoneing
obj.MinHiddenZoneDuration = 100; % [frames]
obj.MinInZoneDuration = 25; % [frames]
obj.MaxHiddenOutOfShelter = 25;
%%
obj.VideoHeight = 0;
obj.VideoWidth = 0;
obj.nFrames = 0;
obj.dt = 0;
%
if ~isempty(obj.VideoFile) && exist(obj.VideoFile, 'file')
    try
        xyloObj = myMMReader(obj.VideoFile);
        obj.VideoHeight = xyloObj.Height;
        obj.VideoWidth = xyloObj.Width;
        obj.nFrames = xyloObj.NumberOfFrames;
        obj.dt = 1/xyloObj.FrameRate;
    catch me
        fprintf('# WARNING! unable to open video file "%s": %s', obj.VideoFile, me.message);
    end
end