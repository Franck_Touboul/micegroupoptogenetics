function [obj, meta] = SocialTrainRegularizedPotts(obj, options)
obj = TrackLoad(obj);
if ~isfield(obj, 'Analysis') || ~isfield(obj.Analysis, 'Potts') || ~isfield(obj.Analysis.Potts, 'Windowed')
    local = obj;
    local.OutputToFile = false;
    local = SocialPottsWithWindow(local);
    obj.Analysis.Potts.Windowed = local.Analysis.Potts.Windowed;
    local = [];
end
temp = SocialSetTimeScale(obj, obj.Analysis.Potts.Windowed.WindowDuration);
temp.OutputToFile = false;
temp.OutputInOldFormat = false;

if ~exist('options', 'var')
    options = struct();
end

options = setDefaultParameters(options, ...
    'order', 1:temp.nSubjects,...
    'nIters', [500 1000 20000],...
    'confidence', 0.05,...
    'betas', .5 .^ (0:12));
%%
trainobj = SocialApplyMap(temp, temp.Analysis.Potts.Windowed.map);
testobj = SocialApplyMap(temp, ~temp.Analysis.Potts.Windowed.map);
data = zeros(temp.nSubjects, sum(trainobj.valid));
for s=1:trainobj.nSubjects
    currZones = trainobj.zones(s, :);
    data(s, :) = currZones(trainobj.valid == 1) - 1;
end
%%
options.betas = unique(options.betas(options.betas ~= 0));
me = {};
meta = [];
for idx = 1:temp.nSubjects
    me{idx} = temp.Analysis.Potts.Windowed.Model{idx};
    me{idx}.RegBeta = 0;
    meta.RegBeta(idx) = 0;
    meta.order(idx) = idx;
end

%%
data = zeros(trainobj.nSubjects, sum(trainobj.valid));
for s=1:trainobj.nSubjects
    currZones = trainobj.zones(s, :);
    data(s, :) = currZones(trainobj.valid == 1) - 1;
end
%%
for level=1:temp.nSubjects
    p = exp(me{level}.perms * me{level}.weights');
    perms_p = exp(me{level}.perms * me{level}.weights');
    Z = sum(perms_p);
    p = p / Z;
    me{level}.prob = p;
end
%%
idx = temp.nSubjects + 1;

for level=1:temp.nSubjects
    %%
    for beta = options.betas
        fprintf('#    - beta %.4f (log2 = %.2f)\n', beta, log2(beta));
        fprintf('#      . level %d\n', level);
        local = trainobj;
        local.n = level;
        local.beta = beta;
        local.output = true;
        local.count = trainobj.ROI.nZones;
        local.nIters = options.nIters(3);
        local.MinNumberOfIters = 5;
        local.initialPottsWeights = me{level}.weights;
        local.KLConvergenceRatio = 1e-4;
%         me{idx} = trainPottsModelUsingPlummet(local, data, options.confidence);
%         local.initialPottsWeights = me{idx}.weights;
        me{idx} = trainPottsModelUsingSummet(local, data, options.confidence);
        
        p = exp(me{idx}.perms * me{idx}.weights');
        Z = sum(p);
        p = p / Z;
        me{idx}.prob = p;
        me{idx}.RegBeta = beta;
        
        meta.RegBeta(idx) = beta;
        meta.order(idx) = level;
        
        idx = idx + 1;
    end
end
%%
meta.Model = me;
[trainobj, independentProbs, jointProbs] = SocialComputePatternProbs(trainobj, false);
meta.trainProbs = jointProbs;
[testobj, q, testProbs] = SocialComputePatternProbs(testobj, false);
meta.testProbs = testProbs;
meta.WindowDuration = obj.Analysis.Potts.Windowed.WindowDuration;
meta.nTrainFrames = trainobj.nFrames;
meta.nTestFrames = testobj.nFrames;
obj.Analysis.Potts.Regularized = meta;
%%
if obj.OutputToFile
    fprintf('# - saving data\n');
    TrackSave(obj);
end

