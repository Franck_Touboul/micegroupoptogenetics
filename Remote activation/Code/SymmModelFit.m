function d = SymmModelFit(vX, vY, vec)

[SkinDistance, overflow] = SymmModel(vX, vY, vec);
d = diff(SkinDistance).^2;

d(SkinDistance(1, :) == 0 & SkinDistance(2, :) == 0) = inf;
if overflow
    d = inf;
else
%    d = sqrt(sum(d));
    d = max(d);
end
