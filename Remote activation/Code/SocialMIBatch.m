function obj = SocialMIBatch(obj)
obj = TrackLoad(obj);
[obj, mi, entropy] = SocialMutualInformation(obj);
obj.Information.MI = mi;
obj.Information.Entropy = entropy;
TrackSave(obj);
