function m = ReorderMatrix(mat, order)
%%
u_ = sort(unique(order(:)'));
i = 1;
idx = order(:)' * 0;
for u = u_
    iu = find(order(:)' == u);
    for k=iu
        idx(i) = k;
        i = i + 1;
    end
end
%%
for i=1:length(idx)
    m(i, :) = mat(idx(i), idx);
end
