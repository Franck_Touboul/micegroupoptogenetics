function [me, idx] = SocialGetRegPotts(obj, beta, order)

b = obj.Analysis.Potts.Regularized.RegBeta;
b(obj.Analysis.Potts.Regularized.order ~= order) = inf;
[qqq, idx] = min(abs(b - beta));
me = obj.Analysis.Potts.Regularized.Model{idx};
f = @(x) size(x, 2);
me.order = cellfun(f, me.labels);