%% interlaced
H1Obj = obj;

quarter = floor(obj.nFrames/4);
map1 = false(1, obj.nFrames);
map1([1:quarter, 2*quarter+1:3*quarter]) = true;
H1Obj.zones = obj.zones(:, map1);
H1Obj.valid = obj.valid(:, map1);
[H1Obj, H1PWJointProbs] = SocialComputePatternPairwiseProbs(H1Obj);
[H1Obj, H1IndepProbs, H1JointProbs, TSpci] = SocialComputePatternProbs(H1Obj, false);

H2Obj = obj;
map2 = false(1, obj.nFrames);
map2([quarter+1:2*quarter, 4*quarter+1:4*quarter]) = true;
H2Obj.zones = obj.zones(:, map2);
H2Obj.valid = obj.valid(:, map2);
[H2Obj, H2PWJointProbs] = SocialComputePatternPairwiseProbs(H2Obj);
[H2Obj, H2IndepProbs, H2JointProbs, TSpci] = SocialComputePatternProbs(H2Obj, false);

H12JSD = JensenShannonDivergence(H1JointProbs, H2JointProbs);
H12PWJSD = JensenShannonDivergence(H1PWJointProbs', H2PWJointProbs');

fH1PWJointProbs = H1JointProbs * 0; fH1PWJointProbs(H1JointProbs>0) = H1PWJointProbs(H1JointProbs>0); fH1PWJointProbs = fH1PWJointProbs / sum(fH1PWJointProbs);
fH2PWJointProbs = H2JointProbs * 0; fH2PWJointProbs(H2JointProbs>0) = H2PWJointProbs(H2JointProbs>0); fH2PWJointProbs = fH2PWJointProbs / sum(fH2PWJointProbs);
fH12PWJSD = JensenShannonDivergence(fH1PWJointProbs, fH2PWJointProbs);

%% halves
HaObj = obj;
HaObj.zones = obj.zones(:, 1:floor(end/2));
HaObj.valid = obj.valid(:, 1:floor(end/2));
[HaObj, HaPWJointProbs] = SocialComputePatternPairwiseProbs(HaObj);
[HaObj, HaIndepProbs, HaJointProbs, TSpci] = SocialComputePatternProbs(HaObj, false);

HbObj = obj;
HbObj.zones = obj.zones(:, floor(end/2)+1:end);
HbObj.valid = obj.valid(:, floor(end/2)+1:end);
[HbObj, HbPWJointProbs] = SocialComputePatternPairwiseProbs(HbObj);
[HbObj, HbIndepProbs, HbJointProbs, TSpci] = SocialComputePatternProbs(HbObj, false);

HabJSD = JensenShannonDivergence(HaJointProbs, HbJointProbs);
HabPWJSD = JensenShannonDivergence(HaPWJointProbs', HbPWJointProbs');

fHaPWJointProbs = HaJointProbs * 0; fHaPWJointProbs(HaJointProbs>0) = H1PWJointProbs(HaJointProbs>0); fHaPWJointProbs = fHaPWJointProbs / sum(fHaPWJointProbs);
fHbPWJointProbs = HbJointProbs * 0; fHbPWJointProbs(HbJointProbs>0) = H2PWJointProbs(HbJointProbs>0); fHbPWJointProbs = fHbPWJointProbs / sum(fHbPWJointProbs);
fHabPWJSD = JensenShannonDivergence(fHaPWJointProbs, fHbPWJointProbs);

%%
%subplot(2,2,1);
figure(1)
PlotProbProb(H1JointProbs, H2JointProbs, ceil(obj.nValid/2), true);
prettyPlot(sprintf('interlaced (D_{JS}=%f, Pairwise D_{JS}=%f)', H12JSD, fH12PWJSD), 'odd pattern prob [log10]', 'even pattern prob [log10]');
hold on; plot(log10(fH1PWJointProbs), log10(fH2PWJointProbs), 'r.'); hold off;
%subplot(2,2,2);
saveFigure(['Res/SocialStatisticsPairwise.1.' obj.FilePrefix]);
figure(2)
PlotProbProb(H1PWJointProbs, H2PWJointProbs, ceil(obj.nValid/2), true);
prettyPlot(sprintf('pairwise interlaced (D_{JS}=%f)', H12PWJSD), 'odd pattern prob [log10]', 'even pattern prob [log10]');
hold on; plot(log10(H1JointProbs), log10(H2JointProbs), 'r.'); hold off;
saveFigure(['Res/SocialStatisticsPairwise.2.' obj.FilePrefix]);
%%
figure(3)
%subplot(2,2,3);
PlotProbProb(HaJointProbs, HbJointProbs, ceil(obj.nValid/2), true);
prettyPlot(sprintf('halves (D_{JS}=%f, Pairwise D_{JS}=%f)', HabJSD, fHabPWJSD), '1st half pattern prob [log10]', '2nd half pattern prob [log10]');
hold on; plot(log10(fHaPWJointProbs), log10(fHbPWJointProbs), 'r.'); hold off;
saveFigure(['Res/SocialStatisticsPairwise.3.' obj.FilePrefix]);
%subplot(2,2,4);
figure(4)
PlotProbProb(HaPWJointProbs, HbPWJointProbs, ceil(obj.nValid/2), true);
prettyPlot(sprintf('pairwise halves (D_{JS}=%f)', HabPWJSD), '1st half pattern prob [log10]', '2nd half pattern prob [log10]');
hold on; plot(log10(HaJointProbs), log10(HbJointProbs), 'r.'); hold off;
saveFigure(['Res/SocialStatisticsPairwise.4.' obj.FilePrefix]);
