function myKalman(data, O, H, Q, R)
% http://www.sciencedirect.com/science?_ob=MImg&_imagekey=B6WMK-4K48NBD-1-2
% B&_cdi=6937&_user=46001&_pii=S1047320306000113&_origin=search&_coverDate=12/31/2006&_sk=999829993&view=c&wchp=dGLzVzz-zSkWb&md5=f2a0492f0ce469e6b0f614ff9f966cd1&ie=/sdarticle.pdf
%   state:          s(t) = O(t-1)s(t-1) + w(t)
%   measurement:    z(t) = H(t)s(t) + v(t)
%
% where w ~ N(0,Q) meaning w is gaussian noise with covariance Q
%       v ~ N(0,R) meaning v is gaussian noise with covariance R
%
%   O = state transition matrix.
%   P = covariance of the state vector estimate. In the input struct,
%       this is "a priori," and in the output it is "a posteriori."
%   H = observation matrix.


% E(wwT) = Q
% E(wvT) = R
% s_p(1) = H \ data(1, :)';
% P_p{1} = (H \ R)/(H');
% for t=2:size(data, 1)
%     % prediction
%     s_m(t) = O * s_p(t-1);
%     P_m(t) = O * P_p{t-1} * O' + Q;
%     % Correction
%     K = P_m(t) * H' / (H * P_m{t} * H' + R);
%     s_p(t) = s_m(t) * K * (data(t, :)' - H * s_m(t));
%     P_p{t} = (eye(size(K{t}, 1)) - K * H) * P_m(t);
% end

z = data(1, :)';
s_p = H \ z;
P_p = inv(H) * R * inv(H');
for t=2:size(data, 1)
    z = data(t, :)';
    % prediction
    s_m = O * s_p;
    P_m = O * P_p * O' + Q;
    % Correction
    K = P_m * H' / (H * P_m * H' + R);
    s_p = s_m * K * (z - H * s_m);
    P_p = (eye(size(K, 1)) - K * H) * P_m;
end