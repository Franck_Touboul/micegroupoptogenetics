function str = MouseName(ind)
switch ind
    case 1
        str = 'P';
    case 2
        str = 'R';
    case 3
        str = 'O';
    case 4
        str = 'G';
end        