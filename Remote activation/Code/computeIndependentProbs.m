function probs = computeIndependentProbs(data, range)
probs = zeros(size(data, 1), range);
for i=1:range
    probs(:, i) = sum(data == (i - 1), 2);
end
probs = probs / size(data, 2);

