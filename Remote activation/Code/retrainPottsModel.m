function obj = retrainPottsModel(obj, order, map, varargin)

use_sparse = true;
options = ParseArguments(struct('nIters', 500, 'confidence', 0.01, 'output', true, 'tolerence', 1e-6, 'ZeroMeanFactor', 0.1), varargin{:});
%
if options.output; fprintf('# Retraining Potts Model\n'); end
%

if options.output; fprintf('# - computing features...\n'); end;
me = obj.Analysis.Potts.Model{order};
if ~isfield(me, 'features')
    if options.output; fprintf('# - finding all permutation of max dimension %d...\n', order); end;
    allPerms = nchoose(1:obj.nSubjects);
    nPerms = {};
    j = 1;
    for i=1:length(allPerms)
        if length(allPerms{i}) <= order
            nPerms{j} = allPerms{i};
            j = j + 1;
        end
    end
    [me.features, me.labels] = assignFeature(obj.zones(:, obj.valid)', obj.ROI.nZones, nPerms, use_sparse);
end
me.features = me.features(:, map);
me.labels = me.labels(:, map);
me.perms = me.perms(:, map);
me.weights = me.weights(map);
me.order = me.order(map);
%
if options.output; fprintf('# - computing constraints...\n'); end
if options.confidence == 0
    me.constraints = full(mean(me.features));
else
    [me.constraints, pci] = binofit(full(sum(me.features)), size(me.features, 1), options.confidence);
end


%
C = max(sum(me.features, 2));

loglikelihood = 0;
prevLL = 0;
for iter=1:options.nIters
    if options.output; fprintf('# - iter (%4d/%4d) : ', iter, options.nIters); end
    % compute the (un-normalized) pdf for each sample
    p = exp(me.features * me.weights');
    % normalize the pdf (the Z)
    perms_p = exp(me.perms * me.weights');
    Z = sum(perms_p);
    p = p / Z;
    prev_loglikelihood = loglikelihood;
    loglikelihood = mean(log(p));
    E = full(sum(me.perms .* repmat(perms_p / Z, 1, size(me.perms, 2))));
    if options.confidence ~= 0
        %[sum(E'  > pci(:, 1) & E' < pci(:, 2)) (1 - confidence) * length(me.weights)]
        nconverged = sum(E'  > pci(:, 1) & E' < pci(:, 2));
        if options.output; fprintf('mean log-likelihood = %6.4f, convergence = %3d%%\n', loglikelihood, round(nconverged / length(me.weights) * 100)); end
        if nconverged >= (1 - options.confidence) * length(me.weights)
            if options.output;
                fprintf('# converged to confidence interval (alpha = %3.2f)\n', options.confidence);
            end
            break;
        end
    else
        if options.output; fprintf('mean log-likelihood = %6.4f (%.3f)\n', loglikelihood, abs((loglikelihood-prev_loglikelihood)/prev_loglikelihood)); end
    end
    if (abs(loglikelihood-prevLL)/(1+abs(prevLL))) < options.tolerence
        if options.output;
            fprintf('# converged to tolerance interval (alpha = %3.2f)\n', options.tolerence);
        end
        break;
    end
    prevLL = loglikelihood;
    %
    dw = log(me.constraints ./ E);
    %dw = log(me.constraints ./ E');
    dw(E == 0) = log(options.ZeroMeanFactor);
    dw(me.constraints == 0) = 0;
    me.weights = me.weights + 1/C * dw;
end
%me = rmfield(me, 'features');
obj.Analysis.Potts.Model{order} = me;
