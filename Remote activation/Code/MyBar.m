function MyBar(varargin)
if length(varargin) == 1
    y = varargin{1};
    x = 1:length(y);
    err = [];
    colors = MyCategoricalColormap;
elseif length(varargin) == 2
    y = varargin{1};
    x = 1:length(y);
    err = varargin{2};
    colors = MyCategoricalColormap;
elseif length(varargin) == 3
    x = varargin{1};
    y = varargin{2};
    err = varargin{3};
    colors = MyCategoricalColormap;
else
    x = varargin{1};
    y = varargin{2};
    err = varargin{3};
    colors = varargin{4};
end
if isvector(y)
    y = y(:);
    x = x(:);
end
for j=1:size(y, 2);
    for i=1:length(x)
        bar(x(i, :), y(i, :), 'FaceColor', colors(i, :));
        hon;
        if ~isempty(err)
            errorbar(x(i), y(i, :), err(i), 'k');
        end
    end
end
xaxis(0, length(x)+1);
set(gca, 'XTick', x);
hoff;