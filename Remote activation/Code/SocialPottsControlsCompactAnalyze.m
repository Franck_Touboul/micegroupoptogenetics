function res = SocialPottsControlsCompactAnalyze(data, tit)
%%
idx = 1;
f = fieldnames(data{idx});
obj1 = data{idx}.(f{1});
obj2 = data{idx}.(f{2});
%%
figure(1)
%%
SocialExperimentData;
for i=1:GroupsData.nSubjects
    me1 = obj1.Model{i};
    me2 = obj2.Model{i};
    
    SquareSubplpot(GroupsData.nSubjects, i);
    PlotProbProb(me1.prob, me2.prob,obj1.nSamples, true)
    res.ByOrder(i, 1) = JensenShannonDivergence(me1.prob', me2.prob');
    xlabel([num2ordinal(i) ' order model prob.']);
    ylabel([num2ordinal(i) ' order model prob.']);
end
%%
figure(2)
%%
for i=1:GroupsData.nSubjects
    me1 = obj1.Model{4};
    me2 = obj2.Model{i};
    
    SquareSubplpot(GroupsData.nSubjects, i);
    PlotProbProb(me1.jointProb', me2.prob,obj1.nSamples, true)
    res.Empirical(i, 1) = JensenShannonDivergence(me1.jointProb, me2.prob');
    xlabel('observed prob.');
    ylabel([num2ordinal(i) ' order model prob.']);
end


