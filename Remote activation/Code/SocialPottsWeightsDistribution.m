function obj = SocialPottsWeightsDistribution(obj)

%%
order = 3;
me.weights = [];
me.order = [];
for id=11:18
    obj = TrackLoad({id 2});
    me.weights = [me.weights, obj.Analysis.Potts.Model{order}.weights];
    me.order = [me.order, obj.Analysis.Potts.Model{order}.order];
end

%%
clf
cmap = MyCategoricalColormap;
entries = {};
seq = sequence(-15, 15, 60);
for o=order:-1:1
    [h, x] = hist(me.weights(me.order == o), seq);
    entries{o} = num2str(o);
    plot(x, h/sum(h)*100, 'Color', cmap(o, :)); 
    hon;
end
legend(entries);
xaxis(-10, 10);
