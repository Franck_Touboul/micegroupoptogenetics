%%function obj = SocialPottsControls(obj)
timescale = 1;
obj1 = obj;
obj2 = obj;
frames = 1:obj.nFrames;
%%
even = mod(floor((frames - 1)  / timescale), 2) == 1;
obj1.zones = obj.zones(:, even);
obj1.valid = obj.valid(:, even);
obj2.zones = obj.zones(:, ~even);
obj2.valid = obj.valid(:, ~even);
[~, indepProbs1, jointProbs1] = SocialComputePatternProbs(obj1, false);
[~, indepProbs2, jointProbs2] = SocialComputePatternProbs(obj2, false);
%%
PlotProbProb(jointProbs1, jointProbs2, floor(obj.nFrames / 2))
JS = JensenShannonDivergence(jointProbs1, jointProbs2)
%%
nIters = 250;
me = {};
for i=1:2
    if i==1
        cobj = obj1;
    else
        cobj = obj2;
    end
    data = cobj.zones(:, cobj.valid) - 1;
    count = cobj.ROI.nZones;
    local = cobj;
    local.n = 4;
    local.output = true;
    local.count = count;
    local.nIters = nIters;
    me{i} = trainPottsModel(local, data);
end
%%
jointProbs = {};
for i=1:2
p = exp(me{i}.perms * me{i}.weights');
perms_p = exp(me{i}.perms * me{i}.weights');
Z = sum(perms_p);
jointProbs{i} = p / Z;
end
KL1 = KullbackLeiblerDivergence(jointProbs{1}', jointProbs{2}');
KL2 = KullbackLeiblerDivergence(jointProbs{2}', jointProbs{1}');
title(sprintf('Resolution: %.2f sec, Djs = %.3f, (ME: Dkl(1|2) = %.3f, Dkl(2|1) = %.3f)', timescale/obj.FrameRate,  JS, KL1, KL2));
return
%%
obj1.OutputToFile = false;
obj1.OutputInOldFormat = false;
obj1 = SocialPotts(obj1);
return 
%%
obj = TrackLoad(obj);
%%
allzones = zeros(obj.nSubjects, sum(obj.valid));
for s=1:obj.nSubjects
    currZones = obj.zones(s, :);
    allzones(s, :) = currZones(obj.valid);
end
%%
srcobj = obj;
srcobj.OutputInOldFormat = false;
srcobj.OutputToFile = false;

%%
index = 1;

obj.Analysis.PottsControl.Ik = [];
obj.Analysis.PottsControl.In = [];
obj.Analysis.PottsControl.Level = [];
obj.Analysis.PottsControl.Entropy = [];
levels = 2.^(0:7);

for level = levels
    fprintf('############################################\n');
    fprintf('# current level: %d\n', level);
    nValid = size(allzones, 2);
    nValid = nValid - mod(nValid, level) - 1;
    seq = 1:level:nValid;
    for i=1:level
        currzones = allzones(:, seq + i - 1);
        curr = SocialPotts(srcobj, currzones);
        
        obj.Analysis.PottsControl.Resolution(index) = level;
        obj.Analysis.PottsControl.In(index) = curr.Analysis.Potts.In;
        obj.Analysis.PottsControl.Ik(index, :) = curr.Analysis.Potts.Ik;
        obj.Analysis.PottsControl.Entropy(index, :) = curr.Analysis.Potts.Entropy;
        
        index = index + 1;
    end
end

%%
if obj.OutputToFile
    fprintf('# - saving data\n');
    TrackSave(obj);
end
