function r = testPottsModel(options, N)
% test the potts model
use_sparse = true;
allPerms = nchoose(1:options.nSubjects);
nPerms = {};
j = 1;
for i=1:length(allPerms)
    if length(allPerms{i}) <= options.n
        nPerms{j} = allPerms{i};
        j = j + 1;
    end
end
me.inputPerms = myPerms(options.nSubjects, options.count);
me.perms = assignFeature(me.inputPerms, options.count, nPerms, use_sparse);
me.weights = rand(1, size(me.perms, 2));

perms_p = exp(me.perms * me.weights');
Z = sum(perms_p);
p = perms_p / Z;
cumprob = cumsum(p);

chunk = 1000;
i = 1;
r = zeros(N, size(me.inputPerms, 2));
while i < N
    if i + chunk > N
        chunk = N - i + 1;
    end
    indices = sum(repmat(rand(1, chunk), length(cumprob), 1) > repmat(cumprob, 1, chunk)) + 1;
    r(i:i+chunk-1, :) = me.inputPerms(indices, :);
    i = i + chunk;
end
r = r' - 1;