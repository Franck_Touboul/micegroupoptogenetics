%function SocialAggressiveStatistics
%%
nbins = 20;
cmap = lines(256);
AggCE = match.analysis(6, :) > 1;
NonAggCE = match.analysis(6, :) <= 1 & obj.Contacts.Behaviors.ChaseEscape.Map;
f = fieldnames(obj.Contacts.Properties);
count = 0;
for i=1:length(f)
    count = count + size(obj.Contacts.Properties.(f{i}), 1);
end
idx = 1;
for i=1:length(f)
    curr = obj.Contacts.Properties.(f{i});
    if all(curr(:)== 0 | curr(:) == 1)
        x = [0 1];
    else
        x = sequence(min(curr(:)), max(curr(isfinite(curr))), nbins);
    end
    for j=1:size(curr, 1)
        c = curr(j, :);
        SquareSubplpot(count+1, idx);
        h1 = histc(c(AggCE), x);
        h2 = histc(c(NonAggCE), x);
        if length(x) ~= 2
            plot(x, h1/sum(h1), 'r'); hon;
            plot(x, h2/sum(h2), 'b'); 
        else
            bar(x, [h1/sum(h1); h2/sum(h2)]);
        end
        hoff;
        if size(curr, 1) == 1
            title([num2str(idx) ' ' f{i}]);
        elseif j==1
            title([num2str(idx) ' Prey' f{i}]);
        else
            title([num2str(idx) ' Pred' f{i}]);
        end
        idx = idx + 1;
    end
end
SquareSubplpot(count+1, count+1);
plot(inf,inf, 'r', inf, inf, 'b');
legend('Aggressive', 'Non-Aggressive');
legend boxoff
axis off
