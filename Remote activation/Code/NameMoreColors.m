function [Color, Specific] = NameMoreColors(im, mask)
% Based on:
%     Example code on color naming as described in:
%     Learning Color Names for Real-World Applications
%     J. van de Weijer, C. Schmid, J. Verbeek, D. Larlus.
%     IEEE Transactions in Image Processing 2009.
%     
%     Run the example_color_naming.m demo for an exmample of color naming image pixels. The programm calls the im2c function which annotates image pixels with color names. The function takes a color image as input and returns the probability for eleven color names---black, blue, brown, gray, green, orange, pink, purple, red, white, yellow---for all pixels in the image. The mapping from RGB values to color names (w2c.mat) has been learned from Google images (2500).
%     
%     A second rougher quanitization of the sRGB space, w2c_4096.mat, has also been provided. The files are also provided in .txt format where the first three columns indicate the sRGB value and column 4-15 the probability over the color names.

%%
%persistent Colors
Colors = [];
if isempty(Colors)
    load('ColorsXKCD.mat');
end

%%
if nargin < 2
    mask = [];
end
%%
if nargout > 1
    labTransformation = makecform('srgb2lab');
    labI = applycform(im2double(im),labTransformation);
    labI = reshape(labI, size(labI, 1) * size(labI, 2), 3);
    if isempty(mask)
    elseif islogical(mask)
        labI = labI(mask(:), :);
    else
        labI = labI(mask(:) > .5, :);
    end
    
    d = zeros(size(labI, 1), size(Colors.Specific.LAB, 1));
    for i=1:size(Colors.Specific.LAB, 1)
        d(:, i) = deltaE2000(repmat(Colors.Specific.LAB(i, :), size(labI, 1), 1), labI);
    end
    [q, idx] = min(median(d));
    Specific.Common = idx;
    Specific.Names = Colors.Specific.Names;
end

hsv = rgb2hsv(im2double(im));
hsv(:, :, 2) = 1;
sim = double(im2uint8(hsv2rgb(hsv)));
idx = sim(:, :, 1) * 256^2 + sim(:, :, 2) * 256 + sim(:, :, 3) + 1;
%%

if isempty(mask)
    Color.Index = Colors.Colors(idx(:));
    Color.Hist = histc(Color.Index, 1:11);
elseif islogical(mask)
    Color.Index = Colors.Colors(idx(mask));
    Color.Hist = histc(Color.Index, 1:11);
else
    Color.Index = Colors.Colors(idx(:));
    Color.Weights = mask(:);
    for u=unique(Color.Index(:)')
        Color.Hist(u) = sum(Color.Weights(Color.Index == u));
    end
end
Color.Names = Colors.Names;
[Color.Count, Color.Order] = sort(Color.Hist, 'descend');
Color.Common = mode(double(Color.Index));
