function plotPairwisePotts(mice, data, me)
angle = 2 * pi / (me.nSubjects * me.range);
currAngle = 0;
radios = 10;
smallRadios = .7;
maxLineWidth = 4;

m1 = inf;
m2 = -inf;
for k=1:length(me.labels)
    links = me.labels{k};
    if size(links, 2) ~= 2 || isempty(find(links(1, :) == mice, 1))
        continue;
    end
    weight = me.constraints(k);
    if weight < m1 
        m1 = weight;
    end
    if weight > m2
        m2 = weight;
    end
end

for k=1:length(me.labels)
    links = me.labels{k};
    weight = (me.constraints(k) - m1) / (m2 - m1);
    if weight == 0
        continue;
    end
    if size(links, 2) == 1
        continue;
    end
    loc = find(links(1, :) == mice, 1);
    if ~isempty(loc)
        coord1 = (links(1, 1) - 1) * me.range + links(2, 1) - 1;
        coord2 = (links(1, 2) - 1) * me.range + links(2, 2) - 1;
        x1 = radios * sin(coord1 * angle);
        y1 = radios * cos(coord1 * angle);
        x2 = radios * sin(coord2 * angle);
        y2 = radios * cos(coord2 * angle);
        line([x1 x2], [y1 y2], 'LineWidth', maxLineWidth * weight, 'Color', data.colors{links(1, 3-loc)});
        hold on;
    end
end


for i=1:me.nSubjects
    for j=1:me.range
        x = radios * sin(currAngle);
        y = radios * cos(currAngle);
        drawCircle(x, y, smallRadios, data.colors{i}, 'EdgeColor', 'none');
        text(x, y, num2str(j), 'Color', 'w', 'HorizontalAlignment', 'Center')
        hold on;
        currAngle = currAngle + angle;
    end
end

hold off;
