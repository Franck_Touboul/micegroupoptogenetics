
options.nSubjects = 4; % number of mice
options.minContactDistance = 15; % minimal distance considered as contact
options.minContanctDuration = 3; % minimal duration of a contact
options.minGapDuration = 50;     % minimal time gap between contacts
%
options.nBins = 15;

%% Loading the Excel file for all mice (if needed)
loadData;
data = orig;

%%
% for i=1:options.nSubjects
%     symplot(i, options.nSubjects);
%     [n, edges] = hist2(data.x(i, :), data.y(i, :), options.nBins);
%     imagesc(edges(1, :), edges(2, :), flipdim(flipdim(n, 1), 2));
%     set(gca, 'YDir', 'normal');
%     hold on;
%     plot(data.x(i, :), data.y(i, :), 'k');
%     hold off;
%     prettyPlot(['Subject no. ' num2str(i)], 'x [cm]', 'y [cm]');
% end

%%
analyzed = SocialBehaviour;

%%
nbins = 25;
m1 = 2;
m2 = 3;
c = shiftdim(analyzed.angle(m1, m2, :));
[n,x1] = hist(c, nbins);
n_mean = n / sum(n);
%polar(x, n);
%
hold on;
c = shiftdim(analyzed.angle(m1, m2, :));
c = c(shiftdim(data.contacts(min(m1, m2), max(m1, m2), :)));
[n,x2] = hist(c, nbins);
n_contact = n / sum(n);
%polar(x, n, 'r-');
hold off;

plot(x1/2/pi*360, n_mean, x2/2/pi*360, n_contact)


%%

model.states(1).func = @(x) halfCircularUniform(x);
model.states(1).title = '-';
%model.states(2).func = @(x) VonMisesDistribution(x, 0, 10);
model.states(2).func = @(x) halfCircularExp(x, 0.3);
model.states(2).title = 'predator';
model.states(3).func = @(x) halfCircularExp(x, 0.3);
model.states(3).title = 'prey';
model.nstates = length(model.states);
model.start = ones(1, model.nstates);
persistence = 1000;
pst = 1-1/persistence;
ptr = 1/persistence;
model.trans = [...
    pst ptr 0.0; 
    ptr pst 0.0; 
    ptr 0.0 pst
    ];

dx= 0.01;
x = 0:dx:pi; 
y1 = model.states(1).func(x);
y2 = model.states(2).func(x);
y3 = model.states(3).func(x);

%
plot(x1, n_mean / (x1(2)-x1(1)), 'r', x2, n_contact/ (x2(2)-x2(1)), 'g')
hold on;
plot(x, y1, 'r', x, y2, 'g');
hold off;

%%
%[pStates, logpseq, alpha, beta, scale, table] = ModelProbs(model, shiftdim(analyzed.angle(m1, m2, :))');
input = shiftdim(analyzed.angle(m1, m2, :))';
valid = ~isnan(input);
input = input(valid);
backtrack = ModelViterbiSequence(model, input);

state = ones(1, data.nData);
state(valid) = backtrack;