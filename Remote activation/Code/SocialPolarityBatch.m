experiments = {...
    'Enriched.exp0001.day%02d.cam04',...
    'SC.exp0001.day%02d.cam01',...
    'Enriched.exp0002.day%02d.cam04',...
    'SC.exp0002.day%02d.cam01',...
    'Enriched.exp0003.day%02d.cam01',...
    'SC.exp0003.day%02d.cam01',...
    'Enriched.exp0004.day%02d.cam01',...
    'SC.exp0004.day%02d.cam04',...
    'SC.exp0005.day%02d.cam04',...
    'SC.exp0006.day%02d.cam01',...
    };
nDays = 4;

E.map = logical([1 0 1 0 1 0 1 0 0 0]);
E.idx = find(E.map);

SC.map = ~E.map;
SC.idx = find(SC.map);

%%
proto = obj;

index = 1;
SPPerms = perms(1:proto.nSubjects);
SPPermOrder = sum(SPPerms ~= repmat(1:obj.nSubjects, size(SPPerms, 1), 1), 2);
all.JSD = [];
all.order = SPPermOrder;
all.id = [];
all.day = [];

for id=1:length(experiments)
    for day = 1:nDays
        prefix = sprintf(experiments{id}, day);
        obj = trackload([obj.OutputPath prefix '.obj.mat'], {'Permutations', 'common'});
        %% subject permuted
        try
        [obj, indepProbs, jointProbs, pci, patterns] = SocialComputePatternProbs(obj, false);

        all.JSD(:, index) = obj.Permutations.Subjects.Djs;
        all.id(index) = id;
        all.day(index) = id;
        index = index + 1;
        catch
        end
    end
end


%%
clf
for g=1:2
    if g==1
        group = SC;
        color = 'r';
    else
        group = E;
        color = 'b';
    end
    for i=2:obj.nSubjects
        %%
        subplot(obj.nSubjects-1, 4, (i-2)*4+[1:2]);
        c1 = all.JSD(all.order == i, :);
        map = all.id * 0;
        for j=group.idx
            map = map | all.id == j;
        end
        c2 = c1(:, map);
        [h,x] = hist(c2(:),30);
        bar(x,h,.5, color);
        hold on;
        if i==2
            legend('control', 'enriched');
            legend boxoff
        end
        
        subplot(obj.nSubjects-1, 4, (i-2)*4+3);
        bar(g, mean(mean(c2)), color);
        hold on;
        errorbar(g, mean(mean(c2)), stderr(mean(c2))', 'k');

        subplot(obj.nSubjects-1, 4, (i-2)*4+4);
        bar(g, mean(std(c2)), color);
        hold on;
        errorbar(g, mean(std(c2)), stderr(std(c2))', 'k');
    end
end