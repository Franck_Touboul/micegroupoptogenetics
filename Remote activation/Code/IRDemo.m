obj = Kinect('W:\Movies\Demos\Test.1months.2013-02-07 122637.2.avi');
%%
params.nBkgFrame = 100;
params.MinNumPixels = 100;
params.TypicalNumOfPixels = 1000;
params.NumOfPixelsTol = .2;
%% background
clf;
fprintf('# computing background frame\n');
bkgBuf = zeros([obj.VideoHeight, obj.VideoWidth, params.nBkgFrame]);
for i=1:params.nBkgFrame
    recount = reprintf(recount, '#   . background frame no. %d/%d', i, params.nBkgFrame);
    obj.FrameNumber = randi(obj.NumberOfFrames); 
    im = obj.Depth;
    bkgBuf(:, :, i) = im;
end
fprintf('\n');
fprintf('# - computing mode of each pixel... ');
obj.Meta.Bkg = mode(bkgBuf, 3);
bkgIm = obj.Meta.Bkg;
imagesc(obj.Meta.Bkg);
fprintf('[done]\n');
%% find background plane
imagesc(bkgIm);
bkgROIObj = imrect;
bounds = round(bkgROIObj.getPosition);
bkgROI = nan(size(bkgIm));
bkgROI(bounds(2):bounds(2)+bounds(4), bounds(1):bounds(1)+bounds(3)) = bkgIm(bounds(2):bounds(2)+bounds(4), bounds(1):bounds(1)+bounds(3));
imagesc(bkgROI);

[bkgCol,bkgRow] = meshgrid(1:size(bkgIm, 2), 1:size(bkgIm, 1)); 
bkgFit = robustfit([bkgRow(:),bkgCol(:)], bkgROI(:));
bkgPlane = bkgFit(1) + bkgFit(2) * bkgRow + bkgFit(3) * bkgCol;
delete(bkgROIObj);
%% mark mouse prototype
obj.FrameNumber = 899;
im = bkgPlane - prevDepth;
valid = bkgIm - prevDepth > 0.01;
valid = bwareaopen(valid, params.MinNumPixels);
im(~valid) = nan;
imagesc(im);
h = imrect;
roi = h.getPosition; 
roi = round(roi); 
map = im(roi(2):roi(2)+roi(4), roi(1):roi(1)+roi(3));
prototype = IRRectify(map);
prototype(isnan(prototype)) = 0;
%%
i = 1;
obj.FrameNumber = 1; 
prevDepth = obj.Depth;
while i < obj.NumberOfFrames
    %%
    obj.FrameNumber = i; 
    im = bkgPlane - prevDepth;
    valid = bkgIm - prevDepth > 0.01;
    valid = bwareaopen(valid, params.MinNumPixels);
    im(~valid) = nan;
    %
    color = obj.Color;
    r = color(:, :, 1);
    g = color(:, :, 2);
    b = color(:, :, 3);
    
    %
    subplot(2,2,1);
    imagesc(im);
    %
    subplot(2,2,2);
    r(~valid) = nan;
    g(~valid) = nan;
    b(~valid) = nan;
    imagesc(cat(3, cat(3, r, g), b));
    drawnow;
    %
    subplot(2,2,3);
    a = regionprops(valid, {'PixelIdxList', 'PixelList', 'Area'});
    map = zeros(size(valid));
    for j=1:4
        map(j) = j;
    end
    for j=1:length(a)
        n = floor(a(j).Area / params.TypicalNumOfPixels + params.NumOfPixelsTol);
        map(a(j).PixelIdxList) = n;
        
        if n > 1
            localmap = nan(max(a(j).PixelList) - min(a(j).PixelList) + 1);
            xy = bsxfun(@minus, a(j).PixelList, min(a(j).PixelList) - 1);
            localmap(sub2ind(size(localmap), xy(:, 1), xy(:, 2))) = im(a(j).PixelIdxList);
        end
    end
    imagesc(map);
    %
    %[~,~, ~, prevDepth] = obj.GetRectifiedRealWorldCoords();
    prevDepth = obj.Depth;
    %pause
    i = i + 1;
end



%%
retrun
%%
vid = VideoReader('w:\Movies\PatternedMice.MP4');
nFrames = vid.NumberOfFrames;
%%
params.nBkgFrame = 100;
%% background
clf;
fprintf('# computing background frame\n');
bkgBuf = zeros([vid.Height, vid.Width, params.nBkgFrame]);
bkgFiltBuf = zeros([vid.Height, vid.Width, params.nBkgFrame]);
recount = 0;
for i=1:params.nBkgFrame
    recount = reprintf(recount, '#   . background frame no. %d/%d', i, params.nBkgFrame);
    frame = randi(nFrames);
    im = read(vid, frame); 
    im = rgb2gray(im); 
    im = im2double(im);
    bkgBuf(:, :, i) = im;
    %%
    s=strel('disk', 11); 
    bkgFiltBuf(:, :, i) = stdfilt(im, s.getnhood);
    %%
    subplot(5, 5, mod(i-1, 25)+1);
    imagesc(im);
    axis off;
    title(sprintf('frame %d', frame));
    drawnow;
end
fprintf('\n');
fprintf('# - computing mode of each pixel... ');
bkgIm = mode(bkgBuf, 3);
imagesc(bkgIm);
fprintf('[done]\n');
%%
bkg.median = median(bkgBuf, 3);
bkg.mad = mad(bkgBuf, 1, 3);
bkg.mad(bkg.mad == 0) = min(bkg.mad(bkg.mad > 0));
bkg.std = 1.4826*bkg.mad;

bkg.medianFilt = median(bkgFiltBuf, 3);
bkg.madFilt = mad(bkgFiltBuf, 1, 3);
bkg.madFilt(bkg.madFilt == 0) = min(bkg.madFilt(bkg.madFilt > 0));
bkg.stdFilt = 1.4826*bkg.madFilt;
%%
s=strel('disk', 11);
filtIm = stdfilt(im, s.getnhood);
pdf = exp(-.5 * ((im - bkg.median).^2 ./ bkg.std.^2 + (filtIm - bkg.medianFilt).^2 ./ bkg.stdFilt.^2)) ./ (2 * pi * sqrt(bkg.stdFilt.*bkg.std));

%% define region of intereset
im = read(vid, i);
im = rgb2gray(im);
im = im2double(im);
imagesc(im);
roiobj = imrect;
roi = roiobj.createMask;
%%
for i=1:nFrames; 
    %%
    im = read(vid, i); 
    rgb = im;
    im = rgb2gray(im); 
    im = im2double(im);
    orig = im;
    % remove sawdust
    bkgmap = convn(edge(im, 'sobel'), ones(13), 'same') > 0.5;
    % remove small objects
    bkgmap = ~bwareaopen(~bkgmap, 500);
    % remove background
    %bkgmap(~roi) = 1;
    im(bkgmap) = 0;
    % reove noisy edges
    im = imerode(im, strel('disk', 3));
    % plot
    subplot(1,2,1); 
    imagesc(orig); 
    %
    subplot(1,2,2); 
    imagesc(im); 
    axis off;
    drawnow; 
    
end

%%
for i=1:nFrames; 
    %%
    im = read(vid, i); 
    rgb = im;
    hsv = rgb2hsv(rgb);
    im = rgb2gray(im); 
    im = im2double(im);
    orig = im;
    map = false(size(orig));
    %%
    s1 = hsv(:, :, 1) < .09;
    s1 = bwareaopen(s1, 4500);
    rp1 = regionprops(s1, 'PixelIdxList', 'Area');
    [~, id] = max([rp1.Area]);
    map(rp1(id).PixelIdxList) = true;
    %%
    s2 = hsv(:, :, 2) < .14;
    s2 = bwareaopen(s2, 4500);
    s2(:, 1:100) = 0;
    s2(:, 1350:end) = 0;
    rp2 = regionprops(s2, 'PixelIdxList', 'Area');
    [~, id] = max([rp2.Area]);
    map(rp2(id).PixelIdxList) = true;
    
    %%
    subplot(2,2,1);
    imagesc(rgb);
    axis off;
    drawnow;
    %
    subplot(2,2,2);
    imagesc(map);
    drawnow;
end