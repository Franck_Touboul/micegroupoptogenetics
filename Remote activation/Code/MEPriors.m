function count = MEPriors(data, patterns, dataPriors)
MEPrint('Computing priors\n');
if nargin < 3
    dataPriors = [];
end
[ndata, dim] = size(data);
count = zeros(1, size(patterns, 1));
for i=1:size(patterns, 1)
    idx = find(~isnan(patterns(i, :)));
    map = true(ndata, 1);
    for j=idx
        map = map & (data(:, j) == patterns(i, j));
    end
    if ~isempty(dataPriors)
        count(i) = sum(map .* dataPriors);
    else
        count(i) = sum(map);
    end
end

