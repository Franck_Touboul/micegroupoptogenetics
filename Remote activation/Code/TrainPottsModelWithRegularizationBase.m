function me = TrainPottsModelWithRegularizationBase(obj, orders, zones)
if exist('zones', 'var')
    data = zones - 1;
else
    %%
    data = zeros(obj.nSubjects, sum(obj.valid));
    for s=1:obj.nSubjects
        currZones = obj.zones(s, :);
        data(s, :) = currZones(obj.valid == 1) - 1;
    end
end

me = {};
fprintf('# - training Potts model:\n');
for level=orders
    %%
    fprintf('#      . level %d\n', level);
    local = obj;
    local.n = level;
    local.output = true;
    local.count = obj.ROI.nZones;
    local.nIters = obj.Analysis.Potts.nIters(1);
    local.MinNumberOfIters = 0;
    if local.nIters > 0
        me{level} = trainPottsModelUsingGIS(local, data, obj.Analysis.Potts.Confidence);
        local.initialPottsWeights = me{level}.weights;
    end
    %%
    local.nIters = obj.Analysis.Potts.nIters(2);
    local.beta = obj.Analysis.Potts.beta;
    local.MinNumberOfIters = obj.Analysis.Potts.MinNumberOfIters;
    me{level} = trainPottsModelWithRegularization(local, data, obj.Analysis.Potts.Confidence);
end

