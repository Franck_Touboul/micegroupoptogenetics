[q, best] = max(-(res.chase.prob.fp*100) + (100 - res.chase.prob.fn*100));
fprintf('false-positive: %f, detection: %f\n', res.chase.prob.fp(best)*100, 100 - res.chase.prob.fn(best)*100);

defaults.PredObjectLength = res.parameters(best, 1);
defaults.PreyObjectLength = res.parameters(best, 2);
defaults.MinSubInteractionDuration = res.parameters(best, 3);
defaults.MinIdleDuration = res.parameters(best, 4);
defaults.ZoneOfContact = res.parameters(best, 5);
defaults.EmissionMatch = res.parameters(best, 6);
defaults.EmissionMismatch = res.parameters(best, 7);
defaults.RelativeSpeedInChase = res.parameters(best, 8);

defaults.ContactMismatch = res.parameters(best, 9);
defaults.ChaseMinOverlap = res.parameters(best, 10);
defaults.ChaseMinOverlapPercentage = res.parameters(best, 11);

%defaults.ZoneOfContact = 150;



%if 1==2
    %%
    data = dlmread('aux/SC.exp0001.day02.cam01.marks', '\t', [1 0 500 14]);
    marks.ids = data(:, 1);
    marks.beg = data(:, 2);
    marks.end = data(:, 3);
    marks.subjects = data(:, 4:5);
    marks.approach = data(:, 6:7);
    marks.leave = data(:, 8:9);
    marks.chase = data(:, 10);
    marks.pred = data(:, 11:12);
    marks.prey = data(:, 13:14);
    marks.artifact = data(:, 15);
    %%
%end
%%
rand('twister',sum(100*clock));
randn('seed',sum(100*clock));
%%

arg = struct();
f = fieldnames(defaults);
for i=1:length(f)
    arg.(f{i}) = defaults.(f{i})(randi(length(defaults.(f{i}))));
end
% arg.EmissionMismatch = defaults.EmissionMismatch(randi(sum(defaults.EmissionMismatch < arg.EmissionMatch)));

f = fieldnames(arg);
for i=1:length(f)
    fprintf('@arg %s=%f\n', f{i}, arg.(f{i}));
end
%%

obj = TrackLoad(obj);
obj.OutputToFile = false;
obj = SocialHierarchy(obj, true, arg);
%%
results = SocialBehaviorAnalysisScore(obj, marks)

f = fieldnames(results);
for i=1:length(f)
    fprintf('@res %s=', f{i});
	fprintf(' %f', results.(f{i}));
	fprintf('\n');
end
qres.chase.correct = results.chase(1);
qres.chase.fp = results.chase(2);
qres.chase.fn = results.chase(3);

qres.chase.prob.fp = qres.chase.fp ./ (qres.chase.fp + qres.chase.correct);
qres.chase.prob.fn = qres.chase.fn ./ (qres.chase.fn + qres.chase.correct);


fprintf('Chase - false-positive: %f, detection: %f\n', qres.chase.prob.fp*100, 100 - qres.chase.prob.fn*100);

 beg = [obj.Contacts.List.beg];
 fin = [obj.Contacts.List.end];
 
 s = [obj.Contacts.List.subjects];
 