﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Media;

namespace SocialCamera
{
    class MyAudioPlayer
    {
       // Header, Format, Data chunks
        WaveHeader header;
        WaveFormatChunk format;
        WaveDataChunk data;

        public enum WaveType 
        {
            Sine,
            Square
        };

        /// <summary>
        /// Initializes the object and generates a wave.
        /// </summary>
        /// <param name="type">The type of wave to generate</param>
        public MyAudioPlayer(WaveType typeL, double freqL, WaveType typeR, double freqR, uint Duration)
        {          
            // Init chunks
            header = new WaveHeader();
            format = new WaveFormatChunk();
            data = new WaveDataChunk();

            // Number of samples = sample rate * channels * bytes per sample
            uint numSamples = (uint)(format.dwSamplesPerSec * Duration);

            // Initialize the 16-bit array
            data.shortArray = new short[numSamples * format.wChannels];

            int amplitude = 32760;  // Max amplitude for 16-bit audio

            for (int channel = 0; channel < format.wChannels; channel++)
            {
                WaveType type = channel == 0 ? typeL : typeR;
                double freq = channel == 0 ? freqL : freqR;
                double t;
                switch (type)
                {
                    case WaveType.Sine:
                        // The "angle" used in the function, adjusted for the number of channels and sample rate.
                        // This value is like the period of the wave.
                        t = (Math.PI * 2 * freq) / (format.dwSamplesPerSec);
                        for (uint i = 0; i < numSamples - 1; ++i)
                            data.shortArray[i * format.wChannels + channel] = Convert.ToInt16(amplitude * Math.Sin(t * i));
                        break;
                    case WaveType.Square:
                        t = (Math.PI * 2 * freq) / (format.dwSamplesPerSec);
                        for (uint i = 0; i < numSamples - 1; ++i)
                            data.shortArray[i * format.wChannels + channel] = Convert.ToInt16(amplitude * (Math.Sin(t * i) >= 0 ? 1 : -1));
                        break;

                }
            }

            // Fill the data array with sample data

                    
                    

//                    double freq = 440.0f;   // Concert A: 440Hz

                    // The "angle" used in the function, adjusted for the number of channels and sample rate.
                    // This value is like the period of the wave.
            /*
                    double t = (Math.PI * 2 * freq) / (format.dwSamplesPerSec * format.wChannels);

                    for (uint i = 0; i < numSamples - 1; i++)
                    {
                        // Fill with a simple sine wave at max amplitude
                        for (int channel = 0; channel < format.wChannels; channel++)
                        {
                            data.shortArray[i + channel] = Convert.ToInt16(amplitude * Math.Sin(t * i));
                        }                        
                    }

                    // Calculate data chunk size in bytes
                    data.dwChunkSize = (uint)(data.shortArray.Length * (format.wBitsPerSample / 8));

                    break;
            }          */
        }

        public void Save(String filePath)
        {
            Stream fileStream = new FileStream(filePath, FileMode.Create);
            WriteToStream(ref fileStream);
            fileStream.Close();
        }

        public void Play()
        {
            //string filename = System.IO.Path.GetTempFileName();
            string filename = @"d:\tempaa.wav";
            Save(filename);

            SoundPlayer player = new SoundPlayer(filename);
            player.Play();
        }


        public void WriteToStream(ref Stream strm)
        {
            // Create a file (it always overwrites)
            

            // Use BinaryWriter to write the bytes to the file
            BinaryWriter writer = new BinaryWriter(strm);

            // Write the header
            writer.Write(header.sGroupID.ToCharArray());
            writer.Write(header.dwFileLength);
            writer.Write(header.sRiffType.ToCharArray());

            // Write the format chunk
            writer.Write(format.sChunkID.ToCharArray());
            writer.Write(format.dwChunkSize);
            writer.Write(format.wFormatTag);
            writer.Write(format.wChannels);
            writer.Write(format.dwSamplesPerSec);
            writer.Write(format.dwAvgBytesPerSec);
            writer.Write(format.wBlockAlign);
            writer.Write(format.wBitsPerSample);

            // Write the data chunk
            writer.Write(data.sChunkID.ToCharArray());
            writer.Write(data.dwChunkSize);
            foreach (short dataPoint in data.shortArray)
            {
                writer.Write(dataPoint);
            }

            writer.Seek(4, SeekOrigin.Begin);
            uint filesize = (uint)writer.BaseStream.Length;
            writer.Write(filesize - 8);

            writer.Seek(40, SeekOrigin.Begin);
            writer.Write(filesize - 44);

            // Clean up
            writer.Close();
            strm.Close();
        }



    }

    public class WaveHeader
    {
        public string sGroupID; // RIFF
        public uint dwFileLength; // total file length minus 8, which is taken up by RIFF
        public string sRiffType; // always WAVE

        /// <summary>
        /// Initializes a WaveHeader object with the default values.
        /// </summary>
        public WaveHeader()
        {
            dwFileLength = 0;
            sGroupID = "RIFF";
            sRiffType = "WAVE";
        }
    }

    public class WaveFormatChunk
    {
        public string sChunkID;         // Four bytes: "fmt "
        public uint dwChunkSize;        // Length of header in bytes
        public ushort wFormatTag;       // 1 (MS PCM)
        public ushort wChannels;        // Number of channels
        public uint dwSamplesPerSec;    // Frequency of the audio in Hz... 44100
        public uint dwAvgBytesPerSec;   // for estimating RAM allocation
        public ushort wBlockAlign;      // sample frame size, in bytes
        public ushort wBitsPerSample;    // bits per sample

        /// <summary>
        /// Initializes a format chunk with the following properties:
        /// Sample rate: 44100 Hz
        /// Channels: Stereo
        /// Bit depth: 16-bit
        /// </summary>
        public WaveFormatChunk()
        {
            sChunkID = "fmt ";
            dwChunkSize = 16;
            wFormatTag = 1;
            wChannels = 2;
            dwSamplesPerSec = 8000;
            wBitsPerSample = 16;
            wBlockAlign = (ushort)(wChannels * (wBitsPerSample / 8));
            dwAvgBytesPerSec = dwSamplesPerSec * wBlockAlign;
        }
    }

    public class WaveDataChunk
    {
        public string sChunkID;     // "data"
        public uint dwChunkSize;    // Length of header in bytes
        public short[] shortArray;  // 8-bit audio

        /// <summary>
        /// Initializes a new data chunk with default values.
        /// </summary>
        public WaveDataChunk()
        {
            shortArray = new short[0];
            dwChunkSize = 0;
            sChunkID = "data";
        }
    }
}
