﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using CommandLine.Utility;
using System.Net;
using System.Net.Mail;

namespace MyBugTrapper
{
    static class MyBugTrapper
    {
        private static Mutex bugTrackingMutex;

        /*private static void SendCompletedCallback(object sender,  System.ComponentModel.AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            String token = (string)e.UserState;

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message sent.");
            }
            //mailSent = true;
        }*/
        /*public bool IsProcessOpen(string name)
        {
            //here we're going to get a list of all running processes on
            //the computer
            foreach (Process clsProcess in Process.GetProcesses())
            {
                //now we're going to see if any of the running processes
                //match the currently running processes. Be sure to not
                //add the .exe to the name you provide, i.e: NOTEPAD,
                //not NOTEPAD.EXE or false is always returned even if
                //notepad is running.
                //Remember, if you have the process running more than once, 
                //say IE open 4 times the loop thr way it is now will close all 4,
                //if you want it to just close the first one it finds
                //then add a return; after the Kill
                if (clsProcess.ProcessName.Contains(name))
                {
                    //if the process is found to be running then we
                    //return a true
                    return true;
                }
            }
            //otherwise we return a false
            return false;
        }*/

        public static bool SendEmail(String smtp, String port, Boolean useSSL, String account, String password, String from, String to, String subject, String body)
        {
            try
            {
                if (smtp == null)
                    return false;
                SmtpClient client = new System.Net.Mail.SmtpClient(smtp);
                if (port != null && port.Trim() != "")
                    client.Port = int.Parse(port);
                client.EnableSsl = useSSL;

                if (account != null && account.Trim() != "")
                {
                    System.Net.NetworkCredential Credentials = new System.Net.NetworkCredential(account, password);
                    client.Credentials = Credentials;
                }
                client.Send(from, to, "Social Camera Failed", "");
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] Args)
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            //System.Threading.Thread.Sleep(1000);            
            //MessageBox.Show("hello");
            Arguments CommandLine = new Arguments(Args);
            String mutexName = CommandLine["mutex"];
            bugTrackingMutex = new Mutex(false, mutexName);
            //MessageBox.Show("# BugTrapper Started: " + mutexName);
            //if (IsProcessOpen("MyBugTrapper.exe"))
            //    return;

            try
            {
                bugTrackingMutex.WaitOne();
            }
            catch
            {
            }


            String account = CommandLine["account"];
            String password = CommandLine["password"];
            String from = CommandLine["from"];
            String to = CommandLine["to"];
            String smtp = CommandLine["smtp"];
            String port = CommandLine["port"];
            String useSSL = CommandLine["useSSL"];

            //MessageBox.Show(Args.Length.ToString());
            
            //MessageBox.Show(password);

            if (smtp != null && port != null && from != null && to != null)
            {
                //MessageBox.Show("sending");
                if (useSSL == "yes")
                    SendEmail(smtp, port, true, account, password, from, to, "SocialCamera: Fatal Error", "");
                else
                    SendEmail(smtp, port, false, account, password, from, to, "SocialCamera: Fatal Error", "");
            }
            //MessageBox.Show("done");
            //            MessageBox.Show("Error!!!!");
            //bugTrackingMutex.ReleaseMutex();
            //MessageBox.Show("# BugTrapper Ended");
        }
    }
}
