﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Social_Tracking
{
    public interface SSHFileTransferDelegate
    {
        
    }

    public partial class SSHFileTransfer : Form
    {
        System.Threading.Timer timer;
        private SSHFileTransferDelegate _delegate;

        public SSHFileTransfer(SSHFileTransferDelegate d)
        {
            InitializeComponent();
            _delegate = d;
        }

        public void TransferFile(string hostname, string username, string password, string srcfile, string destfile)
        {
            try
            {
                //if (!transferFilesCB.Checked || serverTB.Text.Trim() == "" || usernameTB.Text.Trim() == "")
                //return;
                /*SshTransferProtocolBase sshCp;

                //if (proto.Equals("scp"))
                sshCp = new Scp(hostname.Trim(), username.Trim());
                sshCp.Password = password;
                //else
                //sshCp = new Sftp(input.Host, input.User);

                sshCp.OnTransferStart += new FileTransferEvent(sshCp_OnTransferStart);
                sshCp.OnTransferProgress += new FileTransferEvent(sshCp_OnTransferProgress);
                sshCp.OnTransferEnd += new FileTransferEvent(sshCp_OnTransferEnd);

                messageTB.Text += "Connecting...      ";
                sshCp.Connect();
                messageTB.Text += "[Done]" + System.Environment.NewLine;
                messageTB.Text += "Copying files:" + System.Environment.NewLine;
                messageTB.Text += " - " + srcfile + System.Environment.NewLine;
                sshCp.Put(srcfile, destfile);
                messageTB.Text += "Disconnecting...   ";
                sshCp.Close();
                messageTB.Text += "[Done]" + System.Environment.NewLine;


                TimerCallback tcb = updateProgressBar;
                AutoResetEvent autoEvent = new AutoResetEvent(false);

                timer = new System.Threading.Timer(tcb, autoEvent, 0, Timeout.Infinite);*/
            }
            catch (Exception e)
            {
                messageTB.Text += "Error: " + e.Message + System.Environment.NewLine;
            }
        }

        static int progressBarValue = 0;

        public delegate void ProgressBarDelegate();
        private void updateProgressBarThroughDelegate()
        {
            progressBar.Value = progressBarValue;
        }

        private void updateProgressBar(object source)
        {
            try
            {
                progressBar.BeginInvoke(new ProgressBarDelegate(updateProgressBarThroughDelegate));
            }
            finally
            {
                timer.Change(1000, Timeout.Infinite);
            }
        }

        private void sshCp_OnTransferStart(string src, string dst, int transferredBytes, int totalBytes, string message)
        {
            Console.WriteLine();
            //progressBar.va
            //progressBar.Update(transferredBytes, totalBytes, message);
            progressBarValue = (int)(100.0f * (float)transferredBytes / totalBytes);
        }

        private void sshCp_OnTransferProgress(string src, string dst, int transferredBytes, int totalBytes, string message)
        {
         /*   if (progressBar != null)
            {
                progressBar.Update(transferredBytes, totalBytes, message);
            }*/
            progressBarValue = (int)(100.0f * (float)transferredBytes / totalBytes);
        }

        private void sshCp_OnTransferEnd(string src, string dst, int transferredBytes, int totalBytes, string message)
        {
            //if (progressBar != null)
            //{
              //  progressBar.Update(transferredBytes, totalBytes, message);
                //progressBar = null;
            //}
            progressBarValue = (int)(100.0f * (float)transferredBytes / totalBytes);
            //_delegate.SSHFileTransferDone();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
