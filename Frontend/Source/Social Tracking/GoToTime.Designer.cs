﻿namespace Social_Tracking
{
    partial class GoToTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.goBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.hoursBox = new System.Windows.Forms.NumericUpDown();
            this.minsBox = new System.Windows.Forms.NumericUpDown();
            this.secsBox = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.hoursBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secsBox)).BeginInit();
            this.SuspendLayout();
            // 
            // goBtn
            // 
            this.goBtn.Location = new System.Drawing.Point(205, 9);
            this.goBtn.Name = "goBtn";
            this.goBtn.Size = new System.Drawing.Size(75, 39);
            this.goBtn.TabIndex = 4;
            this.goBtn.Text = "Go";
            this.goBtn.UseVisualStyleBackColor = true;
            this.goBtn.Click += new System.EventHandler(this.goBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(286, 9);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 39);
            this.cancelBtn.TabIndex = 5;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Hours";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(79, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Minutes";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(137, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Seconds";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // hoursBox
            // 
            this.hoursBox.Location = new System.Drawing.Point(12, 23);
            this.hoursBox.Name = "hoursBox";
            this.hoursBox.Size = new System.Drawing.Size(55, 20);
            this.hoursBox.TabIndex = 9;
            this.hoursBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // minsBox
            // 
            this.minsBox.Location = new System.Drawing.Point(73, 24);
            this.minsBox.Name = "minsBox";
            this.minsBox.Size = new System.Drawing.Size(55, 20);
            this.minsBox.TabIndex = 10;
            this.minsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // secsBox
            // 
            this.secsBox.Location = new System.Drawing.Point(134, 23);
            this.secsBox.Name = "secsBox";
            this.secsBox.Size = new System.Drawing.Size(55, 20);
            this.secsBox.TabIndex = 11;
            this.secsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // GoToTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 58);
            this.Controls.Add(this.secsBox);
            this.Controls.Add(this.minsBox);
            this.Controls.Add(this.hoursBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.goBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "GoToTime";
            this.Text = "   Go To Time...";
            ((System.ComponentModel.ISupportInitialize)(this.hoursBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secsBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button goBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown hoursBox;
        private System.Windows.Forms.NumericUpDown minsBox;
        private System.Windows.Forms.NumericUpDown secsBox;
    }
}