﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Social_Tracking
{
    public partial class MyTrackBar : UserControl
    {
        public float Maximum = 1;
        public float Minimum = 0;

        private bool lockValue = false;
        private float value = 0;

        System.Windows.Forms.Timer updateTime;

        public float Value
        {
            get
            {
                return this.value;
            }
            set
            {
                if (!lockValue)
                {
                    Object old = this.value;
                    this.value = value;
                    OnValueChanged(this, new EventArgs());
                }
            }
        }  

        public MyTrackBar()
        {
            InitializeComponent();

            updateTime = new Timer();
            updateTime.Interval = 200;
            updateTime.Tick += new EventHandler(PositionMarker);
            updateTime.Start();
        }

        private void ComputeValue(int x)
        {
            try
            {
                lockValue = true;
                float pos = (float)(x - VideoStartImage.Width - VideoMarkerImage.Width / 2) / (float)(VideoEndImage.Location.X - VideoStartImage.Width - VideoMarkerImage.Width);
                pos = Math.Max(0.0f, pos);
                pos = Math.Min(1.0f, pos);
                value = pos * (Maximum - Minimum) + Minimum;
                OnValueChanged(this, new EventArgs());
                
                //System.Windows.Form.Timer
                //PositionMarker();
            }
            finally
            {
                lockValue = false;
            }

        }

        private void PositionMarker(Object myObject, EventArgs myEventArgs)
        {
            PositionMarker();
        }

        private void PositionMarker()
        {
            int x = (int)(VideoStartImage.Width + (VideoEndImage.Location.X - VideoStartImage.Width - VideoMarkerImage.Width) * (value - Minimum) / (Maximum - Minimum));
            VideoBeforeImage.Width = x - VideoStartImage.Width + 1;
            VideoAfterImage.Width = VideoEndImage.Location.X - x;
            VideoAfterImage.Location = new Point(x, 0);
            VideoMarkerImage.Location = new Point(x, VideoMarkerImage.Location.Y);
        }

        private void MyTrackBar_SizeChanged(object sender, EventArgs e)
        {
            VideoEndImage.Location = new Point(this.Width - VideoEndImage.Width, 0);
        }

        private void MyTrackBar_Paint(object sender, PaintEventArgs e)
        {
            MyTrackBar_SizeChanged(null, null);
        }

        private void VideoMarkerImage_MouseDown(object sender, MouseEventArgs e)
        {
            this.Capture = true;
            ComputeValue(e.X + VideoMarkerImage.Location.X);
        }


        private void VideoBeforeImage_MouseDown(object sender, MouseEventArgs e)
        {
            this.Capture = true;
            ComputeValue(e.X + VideoBeforeImage.Location.X);
        }

        private void VideoAfterImage_MouseDown(object sender, MouseEventArgs e)
        {
            this.Capture = true;
            ComputeValue(e.X + VideoAfterImage.Location.X);
        }

        private void MyTrackBar_MouseMove(object sender, MouseEventArgs e)
        {
            ComputeValue(e.X);
        }

        private void MyTrackBar_MouseUp(object sender, MouseEventArgs e)
        {
            this.Capture = false;
        }

        //
        public delegate void ValueChangedHandler(object sender, EventArgs data);
        // The event  
        public event ValueChangedHandler ValueChanged;
        // The method which fires the Event  
        protected void OnValueChanged(object sender, EventArgs data)
        {
            // Check if there are any Subscribers  
            if (ValueChanged != null)
            {
                // Call the Event  
                ValueChanged(this, data);
            }
        }  
    }
}
