﻿namespace Social_Tracking
{
    partial class MyTrackBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.VideoMarkerImage = new System.Windows.Forms.PictureBox();
            this.VideoAfterImage = new System.Windows.Forms.PictureBox();
            this.VideoBeforeImage = new System.Windows.Forms.PictureBox();
            this.VideoEndImage = new System.Windows.Forms.PictureBox();
            this.VideoStartImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.VideoMarkerImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoAfterImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoBeforeImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoEndImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoStartImage)).BeginInit();
            this.SuspendLayout();
            // 
            // VideoMarkerImage
            // 
            this.VideoMarkerImage.BackColor = System.Drawing.Color.Transparent;
            this.VideoMarkerImage.Image = global::Social_Tracking.Properties.Resources.VideoThingy;
            this.VideoMarkerImage.Location = new System.Drawing.Point(21, 9);
            this.VideoMarkerImage.Name = "VideoMarkerImage";
            this.VideoMarkerImage.Size = new System.Drawing.Size(22, 26);
            this.VideoMarkerImage.TabIndex = 27;
            this.VideoMarkerImage.TabStop = false;
            this.VideoMarkerImage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.VideoMarkerImage_MouseDown);
            // 
            // VideoAfterImage
            // 
            this.VideoAfterImage.BackColor = System.Drawing.Color.Transparent;
            this.VideoAfterImage.BackgroundImage = global::Social_Tracking.Properties.Resources.After;
            this.VideoAfterImage.Location = new System.Drawing.Point(178, 0);
            this.VideoAfterImage.Name = "VideoAfterImage";
            this.VideoAfterImage.Size = new System.Drawing.Size(101, 44);
            this.VideoAfterImage.TabIndex = 26;
            this.VideoAfterImage.TabStop = false;
            this.VideoAfterImage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.VideoAfterImage_MouseDown);
            // 
            // VideoBeforeImage
            // 
            this.VideoBeforeImage.BackColor = System.Drawing.Color.Transparent;
            this.VideoBeforeImage.BackgroundImage = global::Social_Tracking.Properties.Resources.Before;
            this.VideoBeforeImage.Location = new System.Drawing.Point(21, 0);
            this.VideoBeforeImage.Name = "VideoBeforeImage";
            this.VideoBeforeImage.Size = new System.Drawing.Size(95, 44);
            this.VideoBeforeImage.TabIndex = 25;
            this.VideoBeforeImage.TabStop = false;
            this.VideoBeforeImage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.VideoBeforeImage_MouseDown);
            // 
            // VideoEndImage
            // 
            this.VideoEndImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.VideoEndImage.BackColor = System.Drawing.Color.Transparent;
            this.VideoEndImage.Image = global::Social_Tracking.Properties.Resources.End;
            this.VideoEndImage.Location = new System.Drawing.Point(278, 0);
            this.VideoEndImage.Name = "VideoEndImage";
            this.VideoEndImage.Size = new System.Drawing.Size(22, 44);
            this.VideoEndImage.TabIndex = 24;
            this.VideoEndImage.TabStop = false;
            // 
            // VideoStartImage
            // 
            this.VideoStartImage.BackColor = System.Drawing.Color.Transparent;
            this.VideoStartImage.Image = global::Social_Tracking.Properties.Resources.Start;
            this.VideoStartImage.Location = new System.Drawing.Point(0, 0);
            this.VideoStartImage.Name = "VideoStartImage";
            this.VideoStartImage.Size = new System.Drawing.Size(22, 44);
            this.VideoStartImage.TabIndex = 23;
            this.VideoStartImage.TabStop = false;
            // 
            // MyTrackBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.VideoMarkerImage);
            this.Controls.Add(this.VideoAfterImage);
            this.Controls.Add(this.VideoBeforeImage);
            this.Controls.Add(this.VideoEndImage);
            this.Controls.Add(this.VideoStartImage);
            this.MaximumSize = new System.Drawing.Size(10000, 44);
            this.MinimumSize = new System.Drawing.Size(66, 44);
            this.Name = "MyTrackBar";
            this.Size = new System.Drawing.Size(300, 44);
            this.SizeChanged += new System.EventHandler(this.MyTrackBar_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MyTrackBar_Paint);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MyTrackBar_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MyTrackBar_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.VideoMarkerImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoAfterImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoBeforeImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoEndImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoStartImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox VideoMarkerImage;
        private System.Windows.Forms.PictureBox VideoAfterImage;
        private System.Windows.Forms.PictureBox VideoBeforeImage;
        private System.Windows.Forms.PictureBox VideoEndImage;
        private System.Windows.Forms.PictureBox VideoStartImage;
    }
}
