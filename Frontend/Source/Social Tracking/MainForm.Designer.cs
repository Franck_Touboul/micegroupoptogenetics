﻿namespace Social_Tracking
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.videoPanel = new System.Windows.Forms.Panel();
            this.playBtn = new System.Windows.Forms.Button();
            this.stopBtn = new System.Windows.Forms.Button();
            this.goToTime = new System.Windows.Forms.Button();
            this.totalTimeLabel = new System.Windows.Forms.Label();
            this.statusBox = new System.Windows.Forms.TextBox();
            this.videoFileTB = new System.Windows.Forms.TextBox();
            this.trackingFileTB = new System.Windows.Forms.TextBox();
            this.loadBtn = new System.Windows.Forms.Button();
            this.frameBox = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.overlayIgnore = new System.Windows.Forms.CheckBox();
            this.overlayAuto = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.trackingFileBtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.eventsPage = new System.Windows.Forms.TabPage();
            this.eventsExportBTN = new System.Windows.Forms.Button();
            this.eventTabs = new System.Windows.Forms.TabControl();
            this.SettingsTab = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.Jump5 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Jump4 = new System.Windows.Forms.TextBox();
            this.Jump3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Jump2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Jump1 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tasksBox = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.subjectsPage = new System.Windows.Forms.TabPage();
            this.saveSubjLocBtn = new System.Windows.Forms.Button();
            this.subjectTable = new System.Windows.Forms.TableLayoutPanel();
            this.addSubjLocBtn = new System.Windows.Forms.Button();
            this.subjClearBtn = new System.Windows.Forms.Button();
            this.subjRestartBtn = new System.Windows.Forms.Button();
            this.numOfSubjTB = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.subjectsLB = new System.Windows.Forms.ListBox();
            this.areasPage = new System.Windows.Forms.TabPage();
            this.removeAreaBtn = new System.Windows.Forms.Button();
            this.clearAreaBtn = new System.Windows.Forms.Button();
            this.addAreaTB = new System.Windows.Forms.TextBox();
            this.addAreaBtn = new System.Windows.Forms.Button();
            this.areasLB = new System.Windows.Forms.ListBox();
            this.currTimeTB = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.loadTrackingWorker = new System.ComponentModel.BackgroundWorker();
            this.eventsExportFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.elementHost2 = new System.Windows.Forms.Integration.ElementHost();
            this.mediaControlBox = new Social_Tracking.MediaControl();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.eventsPage.SuspendLayout();
            this.SettingsTab.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.subjectsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfSubjTB)).BeginInit();
            this.areasPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // videoPanel
            // 
            this.videoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.videoPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.videoPanel.Location = new System.Drawing.Point(-1, -1);
            this.videoPanel.Name = "videoPanel";
            this.videoPanel.Size = new System.Drawing.Size(679, 500);
            this.videoPanel.TabIndex = 0;
            this.videoPanel.Click += new System.EventHandler(this.videoPanel_Click);
            this.videoPanel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseClick);
            this.videoPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseDown);
            this.videoPanel.MouseHover += new System.EventHandler(this.videoPanel_MouseHover);
            this.videoPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseMove);
            // 
            // playBtn
            // 
            this.playBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.playBtn.Location = new System.Drawing.Point(45, 8);
            this.playBtn.Name = "playBtn";
            this.playBtn.Size = new System.Drawing.Size(75, 23);
            this.playBtn.TabIndex = 2;
            this.playBtn.Text = "Play";
            this.playBtn.UseVisualStyleBackColor = true;
            this.playBtn.Visible = false;
            this.playBtn.Click += new System.EventHandler(this.playBtn_Click);
            // 
            // stopBtn
            // 
            this.stopBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.stopBtn.Location = new System.Drawing.Point(126, 8);
            this.stopBtn.Name = "stopBtn";
            this.stopBtn.Size = new System.Drawing.Size(75, 23);
            this.stopBtn.TabIndex = 3;
            this.stopBtn.Text = "Stop";
            this.stopBtn.UseVisualStyleBackColor = true;
            this.stopBtn.Visible = false;
            this.stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
            // 
            // goToTime
            // 
            this.goToTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.goToTime.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.goToTime.Location = new System.Drawing.Point(882, 9);
            this.goToTime.Name = "goToTime";
            this.goToTime.Size = new System.Drawing.Size(23, 20);
            this.goToTime.TabIndex = 5;
            this.goToTime.Text = "...";
            this.goToTime.UseVisualStyleBackColor = true;
            this.goToTime.Click += new System.EventHandler(this.goToTime_Click);
            // 
            // totalTimeLabel
            // 
            this.totalTimeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.totalTimeLabel.AutoSize = true;
            this.totalTimeLabel.Location = new System.Drawing.Point(909, 13);
            this.totalTimeLabel.Name = "totalTimeLabel";
            this.totalTimeLabel.Size = new System.Drawing.Size(64, 13);
            this.totalTimeLabel.TabIndex = 6;
            this.totalTimeLabel.Text = "00:00:00.00";
            // 
            // statusBox
            // 
            this.statusBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusBox.Enabled = false;
            this.statusBox.Location = new System.Drawing.Point(3, 439);
            this.statusBox.Multiline = true;
            this.statusBox.Name = "statusBox";
            this.statusBox.Size = new System.Drawing.Size(306, 56);
            this.statusBox.TabIndex = 7;
            this.statusBox.TextChanged += new System.EventHandler(this.statusBox_TextChanged);
            // 
            // videoFileTB
            // 
            this.videoFileTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.videoFileTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.videoFileTB.Location = new System.Drawing.Point(21, 34);
            this.videoFileTB.Name = "videoFileTB";
            this.videoFileTB.Size = new System.Drawing.Size(232, 20);
            this.videoFileTB.TabIndex = 9;
            this.videoFileTB.TextChanged += new System.EventHandler(this.videoFileTB_TextChanged);
            this.videoFileTB.MouseCaptureChanged += new System.EventHandler(this.videoFileTB_MouseCaptureChanged);
            // 
            // trackingFileTB
            // 
            this.trackingFileTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackingFileTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.trackingFileTB.Location = new System.Drawing.Point(21, 137);
            this.trackingFileTB.Name = "trackingFileTB";
            this.trackingFileTB.Size = new System.Drawing.Size(233, 20);
            this.trackingFileTB.TabIndex = 10;
            this.trackingFileTB.Click += new System.EventHandler(this.trackingFileTB_Click);
            this.trackingFileTB.TextChanged += new System.EventHandler(this.trackingFileTB_TextChanged);
            // 
            // loadBtn
            // 
            this.loadBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.loadBtn.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.loadBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadBtn.Location = new System.Drawing.Point(216, 362);
            this.loadBtn.Name = "loadBtn";
            this.loadBtn.Size = new System.Drawing.Size(75, 27);
            this.loadBtn.TabIndex = 11;
            this.loadBtn.Text = "Load";
            this.loadBtn.UseVisualStyleBackColor = false;
            this.loadBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // frameBox
            // 
            this.frameBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.frameBox.BackColor = System.Drawing.Color.White;
            this.frameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frameBox.Location = new System.Drawing.Point(719, 9);
            this.frameBox.Name = "frameBox";
            this.frameBox.Size = new System.Drawing.Size(75, 20);
            this.frameBox.TabIndex = 12;
            this.frameBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.frameBox.TextChanged += new System.EventHandler(this.frameBox_TextChanged);
            this.frameBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frameBox_KeyDown);
            this.frameBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frameBox_KeyPress);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.eventsPage);
            this.tabControl1.Controls.Add(this.SettingsTab);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.subjectsPage);
            this.tabControl1.Controls.Add(this.areasPage);
            this.tabControl1.Location = new System.Drawing.Point(3, 11);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(306, 422);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Controls.Add(this.overlayIgnore);
            this.tabPage1.Controls.Add(this.overlayAuto);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.trackingFileBtn);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.trackingFileTB);
            this.tabPage1.Controls.Add(this.videoFileTB);
            this.tabPage1.Controls.Add(this.loadBtn);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(298, 396);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "File";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(29, 177);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 17);
            this.checkBox1.TabIndex = 22;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // overlayIgnore
            // 
            this.overlayIgnore.AutoSize = true;
            this.overlayIgnore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.overlayIgnore.Location = new System.Drawing.Point(29, 91);
            this.overlayIgnore.Name = "overlayIgnore";
            this.overlayIgnore.Size = new System.Drawing.Size(86, 17);
            this.overlayIgnore.TabIndex = 21;
            this.overlayIgnore.Text = "Do Not Open";
            this.overlayIgnore.UseVisualStyleBackColor = true;
            this.overlayIgnore.CheckedChanged += new System.EventHandler(this.overlayIgnore_CheckedChanged);
            // 
            // overlayAuto
            // 
            this.overlayAuto.AutoSize = true;
            this.overlayAuto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.overlayAuto.Location = new System.Drawing.Point(29, 114);
            this.overlayAuto.Name = "overlayAuto";
            this.overlayAuto.Size = new System.Drawing.Size(114, 17);
            this.overlayAuto.TabIndex = 20;
            this.overlayAuto.Text = "Open Automatically";
            this.overlayAuto.UseVisualStyleBackColor = true;
            this.overlayAuto.CheckedChanged += new System.EventHandler(this.overlayAuto_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Overlay File";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Video File";
            // 
            // trackingFileBtn
            // 
            this.trackingFileBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.trackingFileBtn.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.trackingFileBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.trackingFileBtn.Location = new System.Drawing.Point(260, 136);
            this.trackingFileBtn.Name = "trackingFileBtn";
            this.trackingFileBtn.Size = new System.Drawing.Size(25, 23);
            this.trackingFileBtn.TabIndex = 17;
            this.trackingFileBtn.Text = "...";
            this.trackingFileBtn.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(259, 33);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(26, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // eventsPage
            // 
            this.eventsPage.Controls.Add(this.eventsExportBTN);
            this.eventsPage.Controls.Add(this.eventTabs);
            this.eventsPage.Location = new System.Drawing.Point(4, 22);
            this.eventsPage.Name = "eventsPage";
            this.eventsPage.Padding = new System.Windows.Forms.Padding(3);
            this.eventsPage.Size = new System.Drawing.Size(298, 396);
            this.eventsPage.TabIndex = 4;
            this.eventsPage.Text = "Events";
            this.eventsPage.UseVisualStyleBackColor = true;
            // 
            // eventsExportBTN
            // 
            this.eventsExportBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.eventsExportBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eventsExportBTN.Location = new System.Drawing.Point(216, 363);
            this.eventsExportBTN.Name = "eventsExportBTN";
            this.eventsExportBTN.Size = new System.Drawing.Size(75, 27);
            this.eventsExportBTN.TabIndex = 5;
            this.eventsExportBTN.Text = "Export";
            this.eventsExportBTN.UseVisualStyleBackColor = true;
            this.eventsExportBTN.Click += new System.EventHandler(this.eventsExportBTN_Click);
            // 
            // eventTabs
            // 
            this.eventTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.eventTabs.Location = new System.Drawing.Point(7, 27);
            this.eventTabs.Name = "eventTabs";
            this.eventTabs.Padding = new System.Drawing.Point(0, 0);
            this.eventTabs.SelectedIndex = 0;
            this.eventTabs.Size = new System.Drawing.Size(288, 330);
            this.eventTabs.TabIndex = 4;
            // 
            // SettingsTab
            // 
            this.SettingsTab.Controls.Add(this.label10);
            this.SettingsTab.Controls.Add(this.Jump5);
            this.SettingsTab.Controls.Add(this.label6);
            this.SettingsTab.Controls.Add(this.label9);
            this.SettingsTab.Controls.Add(this.Jump4);
            this.SettingsTab.Controls.Add(this.Jump3);
            this.SettingsTab.Controls.Add(this.label8);
            this.SettingsTab.Controls.Add(this.label7);
            this.SettingsTab.Controls.Add(this.Jump2);
            this.SettingsTab.Controls.Add(this.label5);
            this.SettingsTab.Controls.Add(this.Jump1);
            this.SettingsTab.Location = new System.Drawing.Point(4, 22);
            this.SettingsTab.Name = "SettingsTab";
            this.SettingsTab.Padding = new System.Windows.Forms.Padding(3);
            this.SettingsTab.Size = new System.Drawing.Size(298, 396);
            this.SettingsTab.TabIndex = 5;
            this.SettingsTab.Text = "Settings";
            this.SettingsTab.UseVisualStyleBackColor = true;
            this.SettingsTab.Click += new System.EventHandler(this.SettingsTab_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(71, 140);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "ctrl + shift + ▲\\▼";
            // 
            // Jump5
            // 
            this.Jump5.Location = new System.Drawing.Point(29, 137);
            this.Jump5.Name = "Jump5";
            this.Jump5.Size = new System.Drawing.Size(36, 20);
            this.Jump5.TabIndex = 10;
            this.Jump5.Text = "300";
            this.Jump5.TextChanged += new System.EventHandler(this.Jump5_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(71, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "ctrl + shift + ▶\\◀";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(71, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "ctrl + ▲\\▼";
            // 
            // Jump4
            // 
            this.Jump4.Location = new System.Drawing.Point(29, 111);
            this.Jump4.Name = "Jump4";
            this.Jump4.Size = new System.Drawing.Size(36, 20);
            this.Jump4.TabIndex = 7;
            this.Jump4.Text = "300";
            this.Jump4.TextChanged += new System.EventHandler(this.Jump4_TextChanged);
            // 
            // Jump3
            // 
            this.Jump3.Location = new System.Drawing.Point(29, 85);
            this.Jump3.Name = "Jump3";
            this.Jump3.Size = new System.Drawing.Size(36, 20);
            this.Jump3.TabIndex = 6;
            this.Jump3.Text = "1";
            this.Jump3.TextChanged += new System.EventHandler(this.Jump3_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(71, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "▲\\▼";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(71, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "ctrl + ▶\\◀";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // Jump2
            // 
            this.Jump2.Location = new System.Drawing.Point(29, 59);
            this.Jump2.Name = "Jump2";
            this.Jump2.Size = new System.Drawing.Size(36, 20);
            this.Jump2.TabIndex = 2;
            this.Jump2.Text = "300";
            this.Jump2.TextChanged += new System.EventHandler(this.Jump2_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Jumps";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // Jump1
            // 
            this.Jump1.Location = new System.Drawing.Point(29, 33);
            this.Jump1.Name = "Jump1";
            this.Jump1.Size = new System.Drawing.Size(36, 20);
            this.Jump1.TabIndex = 0;
            this.Jump1.Text = "1";
            this.Jump1.TextChanged += new System.EventHandler(this.Jump1_TextChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.tasksBox);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(298, 396);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Advanced";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(18, 114);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(74, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "Remove";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(158, 114);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(74, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Add";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Tasks";
            // 
            // tasksBox
            // 
            this.tasksBox.FormattingEnabled = true;
            this.tasksBox.Location = new System.Drawing.Point(18, 26);
            this.tasksBox.Name = "tasksBox";
            this.tasksBox.Size = new System.Drawing.Size(214, 82);
            this.tasksBox.TabIndex = 1;
            this.tasksBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.tasksBox_DragDrop);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(18, 295);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(214, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Analyze Movies";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // subjectsPage
            // 
            this.subjectsPage.Controls.Add(this.saveSubjLocBtn);
            this.subjectsPage.Controls.Add(this.subjectTable);
            this.subjectsPage.Controls.Add(this.addSubjLocBtn);
            this.subjectsPage.Controls.Add(this.subjClearBtn);
            this.subjectsPage.Controls.Add(this.subjRestartBtn);
            this.subjectsPage.Controls.Add(this.numOfSubjTB);
            this.subjectsPage.Controls.Add(this.label1);
            this.subjectsPage.Controls.Add(this.subjectsLB);
            this.subjectsPage.Location = new System.Drawing.Point(4, 22);
            this.subjectsPage.Name = "subjectsPage";
            this.subjectsPage.Padding = new System.Windows.Forms.Padding(3);
            this.subjectsPage.Size = new System.Drawing.Size(298, 396);
            this.subjectsPage.TabIndex = 1;
            this.subjectsPage.Text = "Subjects";
            this.subjectsPage.UseVisualStyleBackColor = true;
            // 
            // saveSubjLocBtn
            // 
            this.saveSubjLocBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveSubjLocBtn.Location = new System.Drawing.Point(157, 302);
            this.saveSubjLocBtn.Name = "saveSubjLocBtn";
            this.saveSubjLocBtn.Size = new System.Drawing.Size(75, 23);
            this.saveSubjLocBtn.TabIndex = 19;
            this.saveSubjLocBtn.Text = "Save";
            this.saveSubjLocBtn.UseVisualStyleBackColor = true;
            // 
            // subjectTable
            // 
            this.subjectTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.subjectTable.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.subjectTable.BackColor = System.Drawing.Color.Transparent;
            this.subjectTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.subjectTable.ColumnCount = 2;
            this.subjectTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.subjectTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.subjectTable.Location = new System.Drawing.Point(14, 181);
            this.subjectTable.Name = "subjectTable";
            this.subjectTable.Padding = new System.Windows.Forms.Padding(4);
            this.subjectTable.RowCount = 2;
            this.subjectTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.subjectTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.subjectTable.Size = new System.Drawing.Size(222, 100);
            this.subjectTable.TabIndex = 16;
            this.subjectTable.Paint += new System.Windows.Forms.PaintEventHandler(this.subjectTable_Paint);
            // 
            // addSubjLocBtn
            // 
            this.addSubjLocBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addSubjLocBtn.Location = new System.Drawing.Point(157, 331);
            this.addSubjLocBtn.Name = "addSubjLocBtn";
            this.addSubjLocBtn.Size = new System.Drawing.Size(75, 23);
            this.addSubjLocBtn.TabIndex = 18;
            this.addSubjLocBtn.Text = "Add";
            this.addSubjLocBtn.UseVisualStyleBackColor = true;
            this.addSubjLocBtn.Click += new System.EventHandler(this.addSubjLocBtn_Click);
            // 
            // subjClearBtn
            // 
            this.subjClearBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.subjClearBtn.Location = new System.Drawing.Point(21, 331);
            this.subjClearBtn.Name = "subjClearBtn";
            this.subjClearBtn.Size = new System.Drawing.Size(75, 23);
            this.subjClearBtn.TabIndex = 15;
            this.subjClearBtn.Text = "Clear Frame";
            this.subjClearBtn.UseVisualStyleBackColor = true;
            // 
            // subjRestartBtn
            // 
            this.subjRestartBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.subjRestartBtn.Location = new System.Drawing.Point(21, 302);
            this.subjRestartBtn.Name = "subjRestartBtn";
            this.subjRestartBtn.Size = new System.Drawing.Size(75, 23);
            this.subjRestartBtn.TabIndex = 14;
            this.subjRestartBtn.Text = "Clear All";
            this.subjRestartBtn.UseVisualStyleBackColor = true;
            this.subjRestartBtn.Click += new System.EventHandler(this.subjRestartBtn_Click);
            // 
            // numOfSubjTB
            // 
            this.numOfSubjTB.Location = new System.Drawing.Point(138, 16);
            this.numOfSubjTB.Name = "numOfSubjTB";
            this.numOfSubjTB.Size = new System.Drawing.Size(94, 20);
            this.numOfSubjTB.TabIndex = 2;
            this.numOfSubjTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numOfSubjTB.ValueChanged += new System.EventHandler(this.numOfSubjTB_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Number of Subjects";
            // 
            // subjectsLB
            // 
            this.subjectsLB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.subjectsLB.FormattingEnabled = true;
            this.subjectsLB.Location = new System.Drawing.Point(18, 54);
            this.subjectsLB.Name = "subjectsLB";
            this.subjectsLB.Size = new System.Drawing.Size(214, 121);
            this.subjectsLB.TabIndex = 0;
            this.subjectsLB.SelectedIndexChanged += new System.EventHandler(this.subjectsLB_SelectedIndexChanged);
            // 
            // areasPage
            // 
            this.areasPage.Controls.Add(this.removeAreaBtn);
            this.areasPage.Controls.Add(this.clearAreaBtn);
            this.areasPage.Controls.Add(this.addAreaTB);
            this.areasPage.Controls.Add(this.addAreaBtn);
            this.areasPage.Controls.Add(this.areasLB);
            this.areasPage.Location = new System.Drawing.Point(4, 22);
            this.areasPage.Name = "areasPage";
            this.areasPage.Padding = new System.Windows.Forms.Padding(3);
            this.areasPage.Size = new System.Drawing.Size(298, 396);
            this.areasPage.TabIndex = 2;
            this.areasPage.Text = "Areas";
            this.areasPage.UseVisualStyleBackColor = true;
            // 
            // removeAreaBtn
            // 
            this.removeAreaBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.removeAreaBtn.Location = new System.Drawing.Point(157, 44);
            this.removeAreaBtn.Name = "removeAreaBtn";
            this.removeAreaBtn.Size = new System.Drawing.Size(75, 23);
            this.removeAreaBtn.TabIndex = 17;
            this.removeAreaBtn.Text = "Remove";
            this.removeAreaBtn.UseVisualStyleBackColor = true;
            this.removeAreaBtn.Click += new System.EventHandler(this.removeAreaBtn_Click);
            // 
            // clearAreaBtn
            // 
            this.clearAreaBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.clearAreaBtn.Location = new System.Drawing.Point(157, 334);
            this.clearAreaBtn.Name = "clearAreaBtn";
            this.clearAreaBtn.Size = new System.Drawing.Size(75, 23);
            this.clearAreaBtn.TabIndex = 16;
            this.clearAreaBtn.Text = "Clear";
            this.clearAreaBtn.UseVisualStyleBackColor = true;
            // 
            // addAreaTB
            // 
            this.addAreaTB.Location = new System.Drawing.Point(18, 16);
            this.addAreaTB.Name = "addAreaTB";
            this.addAreaTB.Size = new System.Drawing.Size(133, 20);
            this.addAreaTB.TabIndex = 16;
            // 
            // addAreaBtn
            // 
            this.addAreaBtn.Location = new System.Drawing.Point(157, 15);
            this.addAreaBtn.Name = "addAreaBtn";
            this.addAreaBtn.Size = new System.Drawing.Size(75, 23);
            this.addAreaBtn.TabIndex = 15;
            this.addAreaBtn.Text = "Add";
            this.addAreaBtn.UseVisualStyleBackColor = true;
            this.addAreaBtn.Click += new System.EventHandler(this.addAreaBtn_Click);
            // 
            // areasLB
            // 
            this.areasLB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.areasLB.FormattingEnabled = true;
            this.areasLB.Location = new System.Drawing.Point(18, 80);
            this.areasLB.Name = "areasLB";
            this.areasLB.Size = new System.Drawing.Size(214, 238);
            this.areasLB.TabIndex = 14;
            // 
            // currTimeTB
            // 
            this.currTimeTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.currTimeTB.BackColor = System.Drawing.Color.White;
            this.currTimeTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.currTimeTB.Location = new System.Drawing.Point(800, 9);
            this.currTimeTB.Name = "currTimeTB";
            this.currTimeTB.Size = new System.Drawing.Size(75, 20);
            this.currTimeTB.TabIndex = 14;
            this.currTimeTB.Text = "00:00:00.00";
            this.currTimeTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.splitContainer1.Panel1.Controls.Add(this.videoPanel);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Window;
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Panel2.Controls.Add(this.statusBox);
            this.splitContainer1.Size = new System.Drawing.Size(995, 500);
            this.splitContainer1.SplitterDistance = 677;
            this.splitContainer1.TabIndex = 15;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.playBtn);
            this.panel1.Controls.Add(this.totalTimeLabel);
            this.panel1.Controls.Add(this.stopBtn);
            this.panel1.Controls.Add(this.frameBox);
            this.panel1.Controls.Add(this.goToTime);
            this.panel1.Controls.Add(this.currTimeTB);
            this.panel1.Location = new System.Drawing.Point(7, 556);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(982, 37);
            this.panel1.TabIndex = 15;
            // 
            // loadTrackingWorker
            // 
            this.loadTrackingWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.loadTrackingWorker_DoWork);
            // 
            // eventsExportFileDialog
            // 
            this.eventsExportFileDialog.DefaultExt = "txt";
            this.eventsExportFileDialog.Filter = "txt files|*.txt|All files|*.*";
            this.eventsExportFileDialog.Title = "Export events to file";
            // 
            // elementHost2
            // 
            this.elementHost2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.elementHost2.Location = new System.Drawing.Point(7, 507);
            this.elementHost2.Name = "elementHost2";
            this.elementHost2.Size = new System.Drawing.Size(980, 43);
            this.elementHost2.TabIndex = 16;
            this.elementHost2.Text = "elementHost2";
            this.elementHost2.Child = this.mediaControlBox;
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(995, 599);
            this.Controls.Add(this.elementHost2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "CheesePlayer";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Click += new System.EventHandler(this.MainForm_Click);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainForm_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint_1);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseClick);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.MainForm_PreviewKeyDown);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.eventsPage.ResumeLayout(false);
            this.SettingsTab.ResumeLayout(false);
            this.SettingsTab.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.subjectsPage.ResumeLayout(false);
            this.subjectsPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOfSubjTB)).EndInit();
            this.areasPage.ResumeLayout(false);
            this.areasPage.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel videoPanel;
        private System.Windows.Forms.Button playBtn;
        private System.Windows.Forms.Button stopBtn;
        private System.Windows.Forms.Button goToTime;
        private System.Windows.Forms.Label totalTimeLabel;
        private System.Windows.Forms.TextBox statusBox;
        private System.Windows.Forms.TextBox videoFileTB;
        private System.Windows.Forms.TextBox trackingFileTB;
        private System.Windows.Forms.Button loadBtn;
        private System.Windows.Forms.TextBox frameBox;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage subjectsPage;
        private System.Windows.Forms.ListBox subjectsLB;
        private System.Windows.Forms.NumericUpDown numOfSubjTB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button subjRestartBtn;
        private System.Windows.Forms.Button subjClearBtn;
        private System.Windows.Forms.TabPage areasPage;
        private System.Windows.Forms.Button removeAreaBtn;
        private System.Windows.Forms.Button clearAreaBtn;
        private System.Windows.Forms.TextBox addAreaTB;
        private System.Windows.Forms.Button addAreaBtn;
        private System.Windows.Forms.ListBox areasLB;
        private System.Windows.Forms.TextBox currTimeTB;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button trackingFileBtn;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox overlayIgnore;
        private System.Windows.Forms.CheckBox overlayAuto;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button addSubjLocBtn;
        private System.Windows.Forms.TableLayoutPanel subjectTable;
        private System.Windows.Forms.Button saveSubjLocBtn;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox tasksBox;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage eventsPage;
        private System.ComponentModel.BackgroundWorker loadTrackingWorker;
        private System.Windows.Forms.TabControl eventTabs;
        private System.Windows.Forms.Button eventsExportBTN;
        private System.Windows.Forms.SaveFileDialog eventsExportFileDialog;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TabPage SettingsTab;
        private System.Windows.Forms.Integration.ElementHost elementHost2;
        private MediaControl mediaControlBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Jump1;
        private System.Windows.Forms.TextBox Jump2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Jump5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Jump4;
        private System.Windows.Forms.TextBox Jump3;
    }
}

