﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Social_Tracking
{
    public interface PasswordDelegate
    {
        void SetPassword(String password);
        void CanceledPassword();
    }

    public partial class EnterPassword : Form
    {
        private PasswordDelegate mydelegate;

        public EnterPassword(PasswordDelegate d)
        {
            InitializeComponent();
            mydelegate = d;
        }

        private void EnterPassword_Load(object sender, EventArgs e)
        {

        }

        private void ok_Click(object sender, EventArgs e)
        {
            mydelegate.SetPassword(password.Text);
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            mydelegate.CanceledPassword();
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Close();
        }
    }
}
