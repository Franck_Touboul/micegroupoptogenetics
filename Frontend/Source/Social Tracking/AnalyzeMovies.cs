﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;

namespace Social_Tracking
{
    interface AnalyzeMoviesDelegate
    {
//        public void AddAnalysisTask(string name);
    }

    public partial class AnalyzeMovies : Form, SSHFileTransferDelegate, PasswordDelegate
    {
        private string Password;
        //private AnalyzeMoviesDelegate mydelegate;

        public AnalyzeMovies(/*AnalyzeMoviesDelegate d*/)
        {
            InitializeComponent();
            //mydelegate = d;
        }

        public AnalyzeMovies(/*AnalyzeMoviesDelegate d, */string[] s)
        {
            InitializeComponent();
            //mydelegate = d;
            foreach (string str in s)
            {
                AddFile(str);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Text = Properties.Settings.Default.AnalysisPrototype;
            markRegions.Checked = Properties.Settings.Default.MarkRegions;
            copyFiles.Checked = Properties.Settings.Default.CopyFiles;
            nameScheme.Checked = Properties.Settings.Default.ApplyScheme;
            nameScheme_CheckedChanged(null, null);
            copyUsingFTP.Checked = Properties.Settings.Default.CopyUsingSSH;
            hostname.Text = Properties.Settings.Default.HostName;
            username.Text = Properties.Settings.Default.UserName;
            remotePath.Text = Properties.Settings.Default.RemotePath;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void AddFile(String filename)
        {
            if (filelistBox.Items.Count == 0)
            {
                //Enriched.exp0005.day01.cam04.mat
                Regex scheme = new Regex(@"(?<path>.*[\\\/])?(?<type>.*)\.exp(?<number>\d*)\.day(?<day>\d*)\.cam(?<camera>\d*).*");
                Match match = scheme.Match(filename);
                typeBox.Text = match.Groups["type"].Value;
                numberBox.Text = match.Groups["number"].Value;
                cameraBox.Text = match.Groups["camera"].Value;
            }
            filelistBox.Items.Add(filename);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog chooser = new OpenFileDialog();
            chooser.Title = "Add video files...";
            DialogResult returnVal = chooser.ShowDialog();
            if (returnVal == DialogResult.OK)
            {
                foreach (String str in chooser.FileNames)
                {
                    AddFile(str);
                }
                
            }
        }

        public void CopyFiles()
        {
            if (copyFiles.Checked)
            {
                int startTime = 0;
                System.Collections.Generic.List<int> times = new System.Collections.Generic.List<int>();
                if (nameScheme.Checked)
                {
                    foreach (String str in filelistBox.Items)
                    {
                        DateTime ctime = File.GetCreationTime(str);
                        int currTime = (int)ctime.Subtract(DateTime.MinValue).TotalDays;
                        times.Add(currTime);
                    }
                    startTime = times.Min();
                }
                int index = 0;
                foreach (String str in filelistBox.Items)
                {
                    SSHFileTransfer fileTransfer = new SSHFileTransfer(this);
                    fileTransfer.Show();
                    string targetPath = remotePath.Text;
                    targetPath.Replace('\\', '/');
                    if (targetPath.Length > 0)
                        targetPath = targetPath + "/";
                    string targetFilename = System.IO.Path.GetFileName(str);
                    if (nameScheme.Checked && typeBox.Text.Length > 0)
                    {
                        //Enriched.exp0005.day01.cam04.mat
                        targetFilename =
                            string.Format("{0}.exp{1:d4}.day{2:d2}.cam{3:d2}.", typeBox.Text, numberBox.Text, times[index] - startTime + 1, cameraBox) 
                            + System.IO.Path.GetExtension(str);
                    }
                    fileTransfer.TransferFile(hostname.Text, username.Text, Password, str, targetPath + targetFilename);
                    ++index;
                }
            }
            //TransferFile(string hostname, string username, string password, string srcfile, string destfile)

        }


        private void removeButton_Click(object sender, EventArgs e)
        {
            filelistBox.Items.Remove(filelistBox.SelectedItem);
        }

        public void SetPassword(String password)
        {
            Password = password;
        }
        
        public void CanceledPassword()
        {
        }

        private void startButton_Click(object sender, EventArgs e)
        {

            if (copyFiles.Checked)
            {
                EnterPassword enterPassword = new EnterPassword(this);
                DialogResult res = enterPassword.ShowDialog();
                if (res == DialogResult.OK)
                {
                    CopyFiles();
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.AnalysisPrototype = comboBox1.Text;
            Properties.Settings.Default.Save();
        }

        private void markRegions_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.MarkRegions = markRegions.Checked;
            Properties.Settings.Default.Save();
        }

        private void copyFiles_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.CopyFiles = copyFiles.Checked;
            Properties.Settings.Default.Save();
        }

        private void nameScheme_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.ApplyScheme = nameScheme.Checked;
            Properties.Settings.Default.Save();
            if (nameScheme.Checked)
            {
                typeBox.Enabled = true;
                numberBox.Enabled = true;
            }
            else
            {
                typeBox.Enabled = false;
                numberBox.Enabled = false;
            }
        }

        private void copyUsingFTP_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.CopyUsingSSH = copyUsingFTP.Checked;
            Properties.Settings.Default.Save();
        }

        private void hostname_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.HostName = hostname.Text;
            Properties.Settings.Default.Save();
        }

        private void username_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.UserName = username.Text;
            Properties.Settings.Default.Save();
        }

        private void remotePath_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.RemotePath = remotePath.Text;
            Properties.Settings.Default.Save();
        }

        private void numberBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
