﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Social_Tracking
{
    /// <summary>
    /// Interaction logic for MediaControl.xaml
    /// </summary>
    public partial class MediaControl : UserControl
    {
        public double Time
        {
            get
            {
                return CustomSlider.Value;
            }
            set
            {
                CustomSlider.Value = value;
            }
        }

        public double TotalDuration
        {
            get
            {
                return CustomSlider.Maximum;
            }
            set
            {
                CustomSlider.Maximum = value;
            }
        }

        private bool isPlaying = false;
        public bool IsPlaying
        {
            get
            {
                return isPlaying;
            }
            set
            {
                if (value != isPlaying)
                {
                    isPlaying = value;
                    if (isPlaying)
                    {
                        MediaControlButton.Style = (Style)(this.Resources["MediaControlPauseButtonStyle"]);
                    }
                    else
                    {
                        MediaControlButton.Style = (Style)(this.Resources["MediaControlPlayButtonStyle"]);
                    }

                }
            }
        }


        // Custom Events
        public event EventHandler TimeChanged;
        public event EventHandler MediaControlButtonPressed;
        public event System.Windows.Forms.KeyEventHandler KeyDownInControl;

        //
        public MediaControl()
        {
            InitializeComponent();
            Time = 0;
            TotalDuration = 0;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.AddHandler(MouseDownEvent, new MouseButtonEventHandler(CustomSlider_MouseDown), true);

            //Mouse.Capture(this);
        }

        private void Thumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {

        }

        private void image1_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CustomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.TimeChanged != null) this.TimeChanged(sender, e);
        }

        private void MediaControlButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.MediaControlButtonPressed != null) this.MediaControlButtonPressed(sender, e);
        }

        private void CustomSlider_MouseDown(object sender, MouseButtonEventArgs e)
        {

            if (TotalDuration > 0)
            {
                Point pos = e.GetPosition(CustomSlider);
                if (pos.X >= 0 && pos.X <= CustomSlider.ActualWidth && pos.Y >= 0 && pos.Y <= CustomSlider.Height)
                    Time = pos.X / CustomSlider.ActualWidth * TotalDuration;
            }
        }

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (this.KeyDownInControl != null)
            {
                System.Windows.Forms.KeyEventArgs ne = new System.Windows.Forms.KeyEventArgs((System.Windows.Forms.Keys)KeyInterop.VirtualKeyFromKey(e.Key));
                this.KeyDownInControl(sender, ne);
            }
        }

    }
}
