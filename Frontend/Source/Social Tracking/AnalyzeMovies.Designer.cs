﻿namespace Social_Tracking
{
    partial class AnalyzeMovies
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.filelistBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.typeBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nameScheme = new System.Windows.Forms.CheckBox();
            this.markRegions = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.copyFiles = new System.Windows.Forms.CheckBox();
            this.startButton = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.username = new System.Windows.Forms.TextBox();
            this.hostname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.copyUsingFTP = new System.Windows.Forms.CheckBox();
            this.removeButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.messages = new System.Windows.Forms.RichTextBox();
            this.remotePath = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numberBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cameraBox = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // filelistBox
            // 
            this.filelistBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.filelistBox.FormattingEnabled = true;
            this.filelistBox.Location = new System.Drawing.Point(12, 27);
            this.filelistBox.Name = "filelistBox";
            this.filelistBox.Size = new System.Drawing.Size(182, 342);
            this.filelistBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Source Files";
            // 
            // typeBox
            // 
            this.typeBox.Location = new System.Drawing.Point(240, 75);
            this.typeBox.Name = "typeBox";
            this.typeBox.Size = new System.Drawing.Size(164, 20);
            this.typeBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(161, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Group type";
            // 
            // nameScheme
            // 
            this.nameScheme.AutoSize = true;
            this.nameScheme.Location = new System.Drawing.Point(21, 77);
            this.nameScheme.Name = "nameScheme";
            this.nameScheme.Size = new System.Drawing.Size(121, 17);
            this.nameScheme.TabIndex = 4;
            this.nameScheme.Text = "Apply name scheme";
            this.nameScheme.UseVisualStyleBackColor = true;
            this.nameScheme.CheckedChanged += new System.EventHandler(this.nameScheme_CheckedChanged);
            // 
            // markRegions
            // 
            this.markRegions.AutoSize = true;
            this.markRegions.Location = new System.Drawing.Point(21, 51);
            this.markRegions.Name = "markRegions";
            this.markRegions.Size = new System.Drawing.Size(87, 17);
            this.markRegions.TabIndex = 5;
            this.markRegions.Text = "Mark regions";
            this.markRegions.UseVisualStyleBackColor = true;
            this.markRegions.CheckedChanged += new System.EventHandler(this.markRegions_CheckedChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(76, 21);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(328, 21);
            this.comboBox1.TabIndex = 6;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Prototype";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(200, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(429, 252);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cameraBox);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.numberBox);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.copyFiles);
            this.tabPage1.Controls.Add(this.nameScheme);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.typeBox);
            this.tabPage1.Controls.Add(this.comboBox1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.markRegions);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(421, 226);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Main";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // copyFiles
            // 
            this.copyFiles.AutoSize = true;
            this.copyFiles.Checked = true;
            this.copyFiles.CheckState = System.Windows.Forms.CheckState.Checked;
            this.copyFiles.Location = new System.Drawing.Point(126, 51);
            this.copyFiles.Name = "copyFiles";
            this.copyFiles.Size = new System.Drawing.Size(71, 17);
            this.copyFiles.TabIndex = 9;
            this.copyFiles.Text = "Copy files";
            this.copyFiles.UseVisualStyleBackColor = true;
            this.copyFiles.CheckedChanged += new System.EventHandler(this.copyFiles_CheckedChanged);
            // 
            // startButton
            // 
            this.startButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.startButton.Location = new System.Drawing.Point(550, 377);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 8;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.remotePath);
            this.tabPage2.Controls.Add(this.username);
            this.tabPage2.Controls.Add(this.hostname);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.copyUsingFTP);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(421, 135);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Advanced";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(96, 101);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(120, 20);
            this.username.TabIndex = 4;
            this.username.TextChanged += new System.EventHandler(this.username_TextChanged);
            // 
            // hostname
            // 
            this.hostname.Location = new System.Drawing.Point(96, 34);
            this.hostname.Name = "hostname";
            this.hostname.Size = new System.Drawing.Size(308, 20);
            this.hostname.TabIndex = 3;
            this.hostname.TextChanged += new System.EventHandler(this.hostname_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Username";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Host";
            // 
            // copyUsingFTP
            // 
            this.copyUsingFTP.AutoSize = true;
            this.copyUsingFTP.Location = new System.Drawing.Point(10, 10);
            this.copyUsingFTP.Name = "copyUsingFTP";
            this.copyUsingFTP.Size = new System.Drawing.Size(128, 17);
            this.copyUsingFTP.TabIndex = 0;
            this.copyUsingFTP.Text = "Copy using SSH/FTP";
            this.copyUsingFTP.UseVisualStyleBackColor = true;
            this.copyUsingFTP.CheckedChanged += new System.EventHandler(this.copyUsingFTP_CheckedChanged);
            // 
            // removeButton
            // 
            this.removeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.removeButton.Location = new System.Drawing.Point(12, 377);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 10;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // addButton
            // 
            this.addButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addButton.Location = new System.Drawing.Point(119, 377);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 11;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // messages
            // 
            this.messages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.messages.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.messages.Location = new System.Drawing.Point(200, 273);
            this.messages.Name = "messages";
            this.messages.Size = new System.Drawing.Size(425, 96);
            this.messages.TabIndex = 12;
            this.messages.Text = "";
            // 
            // remotePath
            // 
            this.remotePath.Location = new System.Drawing.Point(96, 67);
            this.remotePath.Name = "remotePath";
            this.remotePath.Size = new System.Drawing.Size(308, 20);
            this.remotePath.TabIndex = 5;
            this.remotePath.TextChanged += new System.EventHandler(this.remotePath_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Remote Path";
            // 
            // numberBox
            // 
            this.numberBox.Location = new System.Drawing.Point(240, 105);
            this.numberBox.Name = "numberBox";
            this.numberBox.Size = new System.Drawing.Size(163, 20);
            this.numberBox.TabIndex = 10;
            this.numberBox.TextChanged += new System.EventHandler(this.numberBox_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(160, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Group number";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(159, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Camera";
            // 
            // cameraBox
            // 
            this.cameraBox.Enabled = false;
            this.cameraBox.Location = new System.Drawing.Point(240, 137);
            this.cameraBox.Name = "cameraBox";
            this.cameraBox.Size = new System.Drawing.Size(163, 20);
            this.cameraBox.TabIndex = 13;
            // 
            // AnalyzeMovies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 411);
            this.Controls.Add(this.messages);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.filelistBox);
            this.Name = "AnalyzeMovies";
            this.Text = "Analyze Movie";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox filelistBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox typeBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox nameScheme;
        private System.Windows.Forms.CheckBox markRegions;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.CheckBox copyFiles;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.CheckBox copyUsingFTP;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.TextBox hostname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox messages;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox remotePath;
        private System.Windows.Forms.TextBox numberBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox cameraBox;
        private System.Windows.Forms.Label label8;
    }
}