﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;

namespace WaitScreen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //message = args;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string[] list = Directory.GetFiles("Images/");
            if (list.Length > 0)
            {
                Random rnd = new Random();
                int idx = rnd.Next(1, list.Length);

                var image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri(list[idx], UriKind.RelativeOrAbsolute);
                image.EndInit();
                ImageBehavior.SetAnimatedSource(WaitImage, image);
                //WaitImage.Source = image;
            }
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                MessageBox.Text = "";
                for (int i=1; i<args.Length; ++i)
                {
                    MessageBox.Text += args[i] + 
                        Environment.NewLine;
                }
            }
        }
    }
}
