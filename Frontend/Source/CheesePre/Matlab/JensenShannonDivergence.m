function d = JensenShannonDivergence(P, Q)
M = .5 * (P + Q);
d = .5 * P * (flog2(P) - flog2(M))' + .5 * Q * (flog2(Q) - flog2(M))';
