function [rank, removed, mat] = TopoFeedbackArcSetHierarchicalOrder(mat)
if graphisdag(sparse(mat))
    removed = mat * 0;
    rank = myrank(mat);
    return;
end
%%
idx = find(mat);
perm = nchoose(idx);
count = zeros(1, length(perm));
for i=1:length(perm)
    s = sum(mat(perm{i})) / sum(mat(:));
    count(i) = length(perm{i});
end
[count, order] = sort(count);
newperm = perm;
for i=1:length(perm)
    newperm{i} = perm{order(i)};
end
perm = newperm;
%%
% for i=unique(count)
%     cost = zeros(1, sum(count == i));
%     k = 1;
%     for j=find(count == i)
%         cost(k) = sum(mat(perm{j}));
%         k = k + 1;
%     end
%     [cost, order] = sort(cost(:), 1, 'ascend');
%     for j=find(count == i)
%         perm{j} = 
%     end
% end

cost = zeros(1, length(perm));
for i=1:length(perm)
    cost(i) = sum(mat(perm{i}));
end
[cost, order] = sort(cost(:), 1, 'ascend');
newperm = perm;
for i=1:length(perm)
    newperm{i} = perm{order(i)};
end
perm = newperm;
%%
for i=1:length(perm)
    c = mat;
    c(perm{i}) = 0;
    if graphisdag(sparse(c))
        mat = c;
        rank = myrank(c);
        removed = mat * 0;
        removed(perm{i}) = 1;
        break;
    end
end

function rank = myrank(mat, rank)
rank = HierarchicalTopologicalSort(mat);
% rank = graphtopoorder(sparse(mat));
% e = find(sum(mat>0, 2) + sum(mat>0, 1)' == 0);
% if ~isempty(e)
%     for i=e
%         rank = rank(rank ~= i);
%     end
%     rank = [rank, e(:)'];
% end