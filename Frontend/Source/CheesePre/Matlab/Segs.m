classdef Segs
    properties (SetAccess = private)
        Events = struct('data', [], 'beg', 0, 'end', 0);
        Length = 0;
    end
    properties (Dependent)
        Map
        Data
    end
    
    properties (GetAccess = private)
        data_ = [];
        map_ = [];
        events_ = [];
        eventsset = false;
    end
    
    methods 
        function obj = Segs(varargin)
            %obj.events_ = Segs.FindEvents(map, varargin{:});
            if nargin < 2
                if isa(varargin{1}, 'Segs')
                    obj = varargin{1};
                else
                    obj.map_ = varargin{1};
                    obj.eventsset = true;
                    obj.events_ = obj.FindEvents(obj.map_);
                end
            else
                %obj.events_ = Segs.FindEvents(varargin{1}, varargin{2});
                obj.map_ = varargin{1};
                obj.data_ = varargin{2};
                obj.eventsset = true;
                obj.events_ = obj.FindEvents(obj.map_, obj.data_);
            end
            obj.Length = length(obj.map_);
        end
        
        function obj = Foreach(obj, func)
            for i=1:length(obj.Events)
                obj.data_(obj.Events(i).beg:obj.Events(i).end) = func(obj.Events(i).beg, obj.Events(i).end, obj.Events(i).data);
            end
            obj.events_ = obj.FindEvents(obj.map_, obj.data_);
        end

        function res = Run(obj, func)
            for i=1:length(obj.Events)
                res(:, i) = func(obj.Events(i).beg, obj.Events(i).end, obj.Events(i).data);
            end
        end
        
        function value = get.Map(obj)
            if isempty(obj.map_)
                obj.map_ = false(1, obj.Length);
                for i=1:length(obj.Events);
                    e = obj.Events(i);
                    obj.map_(e.beg:e.end) = true;
                end
            end
            value = obj.map_;
        end

        function value = get.Data(obj)
            value = obj.data_;
        end
        
        
        function obj = set.Map(obj, map)
            obj.map_ = map;
            obj.Length = length(map);
            obj.eventsset = true;
            obj.events_ = obj.FindEvents(obj.map_, obj.data_);
        end
        
        function value = get.Events(obj)
            if ~obj.eventsset
                if ~isempty(obj.data_)
                    obj.events_ = obj.FindEvents(obj.map_, obj.data_);
                else
                    obj.events_ = obj.FindEvents(obj.map_);
                end
                obj.Length = length(obj.map_);
                obj.eventsset = true;
            end
            value = obj.events_;
        end
       
        
        function obj = Close(obj, maxgap)
            % remove small gaps
            obj.eventsset = false;
            b = [obj.Events.beg];
            e = [obj.Events.end];
            merge = (b(2:end) - e(1:end-1) - 1) <= maxgap;
            map = obj.map_;
            for i=find(merge)
                map(e(i)+1:b(i+1)-1) = true;
            end
            obj.Map = map;
        end
        
        function segs = Open(segs, minsize)
            % remove small segs
            obj.eventsset = false;
            b = [segs.Events.beg];
            e = [segs.Events.end];
            l = e - b + 1;
            b = b(l>minsize);
            e = e(l>minsize);
            segs.Map = Q.segstomap(segs.Length, b, e);
        end

        function segs = Pad(segs, padsize)
            padsize = abs(padsize);
            map = segs.Map;
            for i=1:length(segs.Events)
                e = segs.Events(i);
                map(max(e.beg-padsize, 1):e.beg-1) = true;
                map(e.end+1:min(e.end+padsize, segs.Length)) = true;
            end
            segs.Map = map;
        end
        
        function Patch(segs, c, factor, varargin)
            if nargin < 3
                factor = 1;
            end
            b = [segs.Events.beg];
            e = [segs.Events.end];
            ax = axis;
            y = ax(3:4);
            y(1) = y(1) + factor * (y(2)-y(1)) / 100;
            y(2) = y(2) - factor * (y(2)-y(1)) / 100;
            for i=1:length(e)
                patch([b(i) e(i) e(i) b(i)], [y(1) y(1) y(2) y(2)], 'w', 'FaceColor', c, 'EdgeColor', c, 'FaceAlpha', .2, varargin{:});
            end
        end
        
        function obj = Resize(obj, newlen, method)
            obj.eventsset = false;
            if nargin < 3
                method = 'any';
            end
            if ~isempty(obj.data_)
                obj.data_ = interp1(linspace(0, 1, length(obj.data_)), double(obj.data_), linspace(0, 1, newlen), 'linear');
            end
            if newlen > obj.Length
                map = interp1(linspace(0, 1, obj.Length), double(obj.map_), linspace(0, 1, newlen), 'nearest');
                obj.Map = map;
            else
%                 res = ceil(obj.Length / newlen);
%                 map = Q.padright(obj.Map, res * newlen - obj.Length);
%                 map = reshape(map, res, length(map) / res);
                
                switch method
                    case 'any'
                        map = interp1(linspace(0, 1, obj.Length), double(obj.map_), linspace(0, 1, newlen), 'linear') > 0;
                        %map = any(map);
                    otherwise
                        error(['unknown method ''' method '''']);
                end
                obj.Map = map;
            end
        end
    end
    
    methods (Static = true)
        
        function seg = FromBegEnd(len, begf, endf)
            map = false(1, len);
            index = zeros(1, len);
            for i=1:length(begf)
                map(begf(i):endf(i)) = true;
                index(begf(i):endf(i)) = i;
            end
            seg = Segs(map, index);
        end
        
        function events = FindEvents(map, data)
            c = diff([0 map(:)' 0]);
            begf = find(c > 0);
            endf = find(c < 0) - 1;

            events = repmat(struct('data', [], 'beg', 0, 'end', 0), length(begf), 1);
            for i=1:length(begf)
                if nargin >= 2 && ~isempty(data)
                    if isvector(data)
                        events(i).data = data(begf(i):endf(i));
                    else
                        events(i).data = data(begf(i):endf(i), :);
                    end
                else
                    events(i).data = [];
                end
                events(i).beg = begf(i);
                events(i).end = endf(i);
            end
        end
        
        function varargout = Find(map, data)
            c = diff([0 map(:)' 0]);
            begf = find(c > 0);
            endf = find(c < 0) - 1;
            
            if nargin >= 2
                events = repmat(struct('data', [], 'beg', 0, 'end', 0), length(begf), 1);
                for i=1:length(begf)
                    if isvector(data)
                        events(i).data = data(begf(i):endf(i));
                    else
                        events(i).data = data(begf(i):endf(i), :);
                    end
                    events(i).beg = begf(i);
                    events(i).end = endf(i);
                end
                varargout{1} = events;
            else
                varargout{1} = begf;
                varargout{2} = endf;
            end
        end
        
        
        function map = ToMap(len, b, e)
            map = false(1, len);
            for i=1:length(b)
                map(b(i):e(i)) = true;
            end
        end
        
        function map = CloseMap(map, maxgap)
            % remove small gaps
            obj = Segs(map);
            b = [obj.Events.beg];
            e = [obj.Events.end];
            merge = (b(2:end) - e(1:end-1) - 1) <= maxgap;
            map = obj.map_;
            for i=find(merge)
                map(e(i)+1:b(i+1)-1) = true;
            end
        end
     
        function map = OpenMap(map, minsize)
            % remove small segs
            obj = Segs(map);
            b = [obj.Events.beg];
            e = [obj.Events.end];
            l = e - b + 1;
            b = b(l>minsize);
            e = e(l>minsize);
            map = Q.segstomap(obj.Length, b, e);
        end
        
    end
end