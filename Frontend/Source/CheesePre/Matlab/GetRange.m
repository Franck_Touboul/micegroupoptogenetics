function r = GetRange(map)
r1 = find(map == 1, 1);
if isempty(r1)
    r = [];
    return;
end
r2 = find(map == 1, 1,'last');
r = [r1 r2];