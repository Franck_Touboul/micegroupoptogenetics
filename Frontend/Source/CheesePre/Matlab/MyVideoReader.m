classdef MyVideoReader < Serializable
    properties
        FrameRate = 0;
        Height = 0;
        Width = 0;
        NumberOfFrames = 0;
        RealNumberOfFrames = 0;
        Duration = 0;
        StartDateTime = [];
        Meta = struct();
    end
    
    properties (Dependent)
        FrameNumber = 0;
        Filename;
        Time = 0;
    end
    properties (Access=private)
        Time_ = 0;
        Buffer_ = [];
        BufferFrameRange_ = [];
        BufferSize_ = 5;
        Filename_ = '';
        Source_ = [];
        Current = [];
    end
    
    properties (Constant, Hidden)
        UseOld = isunix;
    end
    
    
    methods
        function obj = MyVideoReader(varargin)
            if nargin < 1
                error 'Not enough input arguments.'
            end
            switch class(varargin{1})
                case 'char'
                    obj.Filename = varargin{1};
                case 'struct'
                    obj.Filename = varargin{1}.VideoFile;
                case 'MyVideoReader'
                    obj = varargin{1};
                otherwise
                    error(['MyVideoReader cannot except input argument of type ' class(varargin{1})]);
            end
            if nargin >= 2
                obj.FrameNumber = varargin{2};
            end
        end
        
        function out = SerializeOut(obj)
            out = obj.Filename;
        end

        
        function obj = set.FrameNumber(obj, value)
            obj.Time = value / obj.FrameRate;
            obj = UpdateBuffer(obj);
        end

        function value = get.FrameNumber(obj)
            value = max(round(obj.Time_ * obj.FrameRate + 1), 1);
        end

        function obj = set.Time(obj, value)
            obj.Time_ = value;
            %obj = UpdateBuffer(obj);
            try
                obj.Source_.CurrentTime = obj.Time_;
                obj.Current = obj.Source_.readFrame;
            catch
                obj.Current = [];
            end
        end
        
        function value = get.Time(obj)
            value = obj.Time_;
        end
        
        function value = get.Filename(obj)
            value = obj.Filename_;
        end
        
        function obj = set.Filename(obj, value)
            obj.Filename_ = value;
            try
                if ~MyVideoReader.UseOld
                    meta = VideoReader(obj.Filename_);
                    obj.Height = meta.height;
                    obj.Width = meta.width;
                    obj.FrameRate = meta.FrameRate;
                    obj.Duration = meta.Duration;
                    obj.NumberOfFrames = meta.Duration * meta.FrameRate;
                    obj.RealNumberOfFrames = obj.NumberOfFrames;
                    obj.Source_ = meta;
                else
                    meta = mmread(obj.Filename_, 1);
                    obj.Height = meta(end).height;
                    obj.Width = meta(end).width;
                    obj.FrameRate = ceil(meta(end).rate);
                    obj.Duration = meta(end).totalDuration;
                    obj.NumberOfFrames = obj.FrameRate * meta(end).totalDuration;
                    obj.RealNumberOfFrames = abs(meta(end).nrFramesTotal);
                end
            catch
            end
            
            % trying to read meta-data from assorted xml file
            try
                videofile = [obj.Filename_, '.xml'];
                meta.CreationDate = addtodate(MyFilename(obj.Video.Filename).CreationDate, -round(obj.Video.NumberOfFrames / obj.Video.FrameRate*1000), 'millisecond');
                meta.Remarks = '';
                if exist(videofile.Full, 'file')
                    metaxml = xmlread(videofile.Full);
                    cdate = char(metaxml.getElementsByTagName('Start').item(0).getTextContent);
                    obj.StartDateTime = datenum(cdate, 'yyyy-mm-ddTHH:MM:SS.FFF');
                    meta.Remarks =  char(metaxml.getElementsByTagName('Remarks').item(0).getTextContent);
                end
                Meta = meta;
            catch
            end
        end

        function frame = CurrentFrame(obj)
            if ~MyVideoReader.UseOld
                frame = obj.Current;
            else
                currFrame = obj.FrameNumber;
                obj = UpdateBuffer(obj);
                num = currFrame - obj.BufferFrameRange_(1) + 1;
                if length(obj.Buffer_) >= num
                    frame = obj.Buffer_(num).cdata;
                else
                    frame = zeros(obj.Height, obj.Width, 3);
                end
            end
        end
        
        function [obj, frame] = NextFrame(obj)
            if ~MyVideoReader.UseOld
                frame = obj.Source_.readFrame;
                obj.Time_ = obj.Source_.CurrentTime;
            else
                obj.FrameNumber = obj.FrameNumber + 1;
                frame = obj.CurrentFrame;
            end
        end
        
        function Show(obj)
            imagesc(obj.CurrentFrame);
            axis off;
        end
            
        function Play(obj)
            h = tic;
            rt = 0;
            rtfactor = 0.95;
            for i=1:obj.NumberOfFrames
                obj.FrameNumber = i;
                imagesc(obj.CurrentFrame);
                time = toc(h);
                if rt == 0
                    rt = obj.Time/time;
                else
                    rt = rtfactor * rt + (1 - rtfactor) * obj.Time/time;
                end
                title(sprintf('%d (x%.1f RT)', i, rt));
                axis off;
                drawnow;
            end
        end
    end
    
    methods (Access = protected)
        function obj = UpdateBuffer(obj)
            if ~MyVideoReader.UseOld
                return;
            end
            currFrame = obj.FrameNumber;
            if ~isempty(obj.Buffer_) && ...
                    obj.BufferFrameRange_(1) <= currFrame && ...
                    obj.BufferFrameRange_(2) >= currFrame
            else
                meta = mmread(obj.Filename, [], [obj.Time_ obj.Time_+(obj.BufferSize_ + .5)/obj.FrameRate]);
                if isempty(meta(end).frames)
                    pause(.2);
                    meta = mmread(obj.Filename, [], [obj.Time_ obj.Time_+(obj.BufferSize_ + .5)/obj.FrameRate]);
                end
                if ~isempty(meta(end).frames)
                    obj.Buffer_ = meta(end).frames;
                    obj.BufferFrameRange_ = [currFrame, currFrame + length(obj.Buffer_) - 1];
                else
                    obj.Buffer_ = [];
                    obj.BufferFrameRange_ = [-1 -1];
                end
            end
        end
    end
end