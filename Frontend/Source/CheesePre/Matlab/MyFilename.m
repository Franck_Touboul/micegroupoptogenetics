classdef MyFilename
    properties
        Path = '.';
        Name = '';
        Ext = '';
    end
    
    methods
        function obj = MyFilename(fname)
            if isa(fname, 'MyFilename')
                obj = fname;
            else
                [obj.Path, obj.Name, obj.Ext] = fileparts(fname);
            end
        end
        
        function fname = Full(obj)
            if isempty(obj.Path)
                obj.Path = '.';
            end
            fname = [obj.Path filesep obj.Name obj.Ext];
        end
        
        function obj = AddPrefix(obj, prefix)
            obj.Name = [prefix, obj.Name];
        end
        
        function obj = SetPath(obj, path)
            obj.Path = path;
        end

        function obj = SetName(obj, name)
            obj.Name = name;
        end

        function obj = SetExt(obj, ext)
            obj.Ext = ext;
        end
        
        function d = CreationDate(obj)
            %%
            data = dir(obj.Full);
            d = data(1).datenum;
        end
        
    end
end