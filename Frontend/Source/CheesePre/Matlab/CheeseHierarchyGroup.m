function source = CheeseHierarchyGroup(obj)
%%
if ischar(obj.Video)
    fname = MyFilename(obj.Video);
else
    fname = MyFilename(obj.Video.Filename);
end
template = CheeseSquare.GeneratePrefix(obj.Meta.GroupType, obj.Meta.GroupId, [], obj.Meta.CameraId);
objs = {};
idx = 1;
sourceidx = 1;
for i=1:99
    currfile = fname.SetName(sprintf(template, i)).Full;
    if exist(currfile, 'file') || exist([currfile '.obj.mat'], 'file')
        Console.Message(1, 'loading ''%s''', sprintf(template, i));
        objs{idx} = CheeseSquare(currfile);
        succ = false;
        try
            if ~Q.isfield(objs{idx}, 'Hierarchy') || ~Q.isfield(objs{idx}.Hierarchy, 'ChaseEscape') || ~Q.isfield(objs{idx}.Hierarchy, 'AggressiveChase')  || ~Q.isfield(objs{idx}.Hierarchy, 'Contacts')
                track = objs{idx}.ToClassical;
                track = CheeseHierarchy(track);
                objs{idx}.Hierarchy = track.Hierarchy;
                objs{idx}.Hierarchy.Contacts = track.Contacts;
            end
            succ = true;
        catch me
            succ = false;
            Console.Warning('unable to classify hierarchy in ''%s''\n\t%s', objs{idx}.Prefix, me.message);
            objs(idx) = [];
        end
        if succ
            if strcmp(sprintf(template, i), obj.Prefix)
                sourceidx = idx;
            end
            idx = idx + 1;
        end
    end
end
%%
fields = {'ChaseEscape', 'AggressiveChase'};
for fidx=1:length(fields)
    type = fields{fidx};
    % initialize
    for i=1:length(objs)
        objs{i}.Hierarchy.Group.(type) = struct();
    end
    % assign hierarchy data from all days
    for j=1:length(objs)
        for i=1:length(objs)
            objs{j}.Hierarchy.Group.(type).ChaseEscape(:, :, i) = objs{i}.Hierarchy.(type).ChaseEscape;
            %objs{i}.Hierarchy.Group.(type).Contacts(:, :, i) = objs{i}.Hierarchy.(type).Contacts;
            objs{j}.Hierarchy.Group.(type).Duration(i, 1) = sum(objs{i}.Tracking.valid) / objs{i}.Video.FrameRate;
            objs{j}.Hierarchy.Group.(type).DayID(i, 1) = objs{i}.Meta.DayId;
        end
        objs{j}.Hierarchy.Group.(type).DavidScore = DavidScore(sum(objs{1}.Hierarchy.Group.(type).ChaseEscape, 3))';
        %%
        [~, o] = sort(objs{1}.Hierarchy.Group.(type).DavidScore);
        seq = 1:objs{1}.nSubjects; seq(o) = seq;
        objs{j}.Hierarchy.Group.(type).Order = seq(:)';
    end
end
%%
for i=1:length(objs)
    Console.Message(1, 'saving ''%s''', objs{i}.Prefix);
    objs{i}.Save;
end
%%
source = obj;
try
    source = objs{sourceidx};
catch
end

%%
return;
source = TrackLoad(source);
[objs, idx] = CheeseGroupLoad(source);
%%
nSubjects = 4;
nDays = 4;
f = fieldnames(objs);
for i=1:length(f)
    try
        nDays = max(nDays, CheeseSquare.ParseName(source.FilePrefix).DayId);
    catch
    end
    try
        nSubjects = max(objs.(f{i}).nSubjects, nSubjects);
    catch
    end
end
%%
fields = {'ChaseEscape', 'AggressiveChase'};
for fidx=1:length(fields)
    edges = cell(1,2); edges{1} = 1:nSubjects; edges{2} = edges{1};
    f = fieldnames(objs);
    ce = zeros(nSubjects);
    ce_ = cell(1, nDays);
    ncontacts = zeros(nSubjects);
    ncontacts_ = cell(1, nDays);
    ranks_ = cell(1, nDays);
    hours_ = zeros(1, nDays);
    for i=1:length(f)
        obj = objs.(f{i});
        try
            if ~isfield(obj, 'Hierarchy') || ~isfield(obj.Hierarchy, fields{fidx})
                obj = CheeseHierarchy(obj);
                if i == idx
                    source.Hierarchy = obj.Hierarchy;
                end
            end
            ranks_{i} = obj.Hierarchy.(fields{fidx}).rank;
            ce_{i} = hist3(...
                [obj.Contacts.Behaviors.(fields{fidx}).Chaser(obj.Contacts.Behaviors.(fields{fidx}).Map)', ...
                obj.Contacts.Behaviors.(fields{fidx}).Escaper(obj.Contacts.Behaviors.(fields{fidx}).Map)'], ...
                'Edges', edges);
            ce = ce + ce_{i};

            ncontacts_{i} = hist3(sort([obj.Contacts.List.subjects]', 2), 'Edges', edges);
            ncontacts_{i} = ncontacts_{i} + ncontacts_{i}';
            ncontacts = ncontacts + ncontacts_{i};
        
            if obj.StartTime < 0 || obj.EndTime < 0
                try
                    obj = TrackFindDarkPeriodAndSave(obj);
                catch
                    obj.StartTime = 0;
                    obj.EndTime = obj.nFrames / obj.FrameRate;
                end
            end
            hours_(i) = (obj.EndTime - obj.StartTime) / 3600;
        catch
        end
    end
    %% newer version
    mat = ce - ce';
    fmat = mat;
    mat(binotest(ce, ce + ce')) = 0;
    fmat = fmat .* (fmat > 0);
    mat = mat .* (mat > 0);
    
    [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
    diluted = mat;
    diluted(removed ~= 0) = 0;
    
    [frank, fremoved] = TopoFeedbackArcSetHierarchicalOrder(fmat);
    fdiluted = fmat;
    fdiluted(fremoved ~= 0) = 0;
    
    
    obj.Hierarchy.Group.(fields{fidx}) = struct();
    for i=1:length(f)
        obj = objs.(f{i});
        if fidx == 1
            obj.Hierarchy.Group = struct();
        end
        
        obj.Hierarchy.Group.(fields{fidx}) = struct();
        obj.Hierarchy.Group.(fields{fidx}).rank = rank;
        obj.Hierarchy.Group.(fields{fidx}).nLevels = length(unique(rank));
        obj.Hierarchy.Group.(fields{fidx}).strength = sum(diluted(:) > 0);
        obj.Hierarchy.Group.(fields{fidx}).map = diluted;
        obj.Hierarchy.Group.(fields{fidx}).fullMap = fdiluted;
        obj.Hierarchy.Group.(fields{fidx}).ChaseEscape = ce;
        obj.Hierarchy.Group.(fields{fidx}).Contacts = ncontacts;
        obj.Hierarchy.Group.(fields{fidx}).removed = removed;
        obj.Hierarchy.Group.(fields{fidx}).RecordingDurationInHours = sum(hours_);
        obj.Hierarchy.Group.(fields{fidx}).Days = struct();
        for day=1:nDays
            obj.Hierarchy.Group.(fields{fidx}).Days(day).ranks = ranks_{day};
            obj.Hierarchy.Group.(fields{fidx}).Days(day).ChaseEscape = ce_{day};
            obj.Hierarchy.Group.(fields{fidx}).Days(day).Contacts = ncontacts_{day};
            obj.Hierarchy.Group.(fields{fidx}).Days(day).RecordingDurationInHours = hours_(day);
        end
        
        if i == idx
            source.Hierarchy.Group = obj.Hierarchy.Group;
        end
        TrackSave(obj);
    end
end


