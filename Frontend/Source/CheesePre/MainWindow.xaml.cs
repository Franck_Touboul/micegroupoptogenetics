﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using WMPLib;
using csmatio.io;
using csmatio.types;
using System.Threading;
using System.Diagnostics;

namespace CheeseOne
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private List<Experiment> ExperimentList;
        private List<Experiment> TrackingList = new List<Experiment>();
        public static MLApp.MLApp Matlab = null;
        private Mutex PreviewMutex = new Mutex();
        private Mutex MatlabMutex = new Mutex();
        private String UniqueID = "";
        private Experiment Current = null;
        private enum ProgramStates { Idle, Tracking };
        private ProgramStates ProgramState = ProgramStates.Idle;

        //ublic static FileStream LogFile = new FileStream("./CheeseOne.log", FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
        public static StreamWriter LogFile = new StreamWriter("./CheeseOne.log", true);

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //var activationContext = Type.GetTypeFromProgID("matlab.application.single");
            //Matlab = (MLApp.MLApp)Activator.CreateInstance(activationContext);
            //Console.WriteLine(matlab.Execute("1+2"));
            //Console.ReadKey();
            //Directory.GetFiles();
            UniqueID = RandString();
            FolderTextBox.Text = Properties.Settings.Default.Folder;
            Task.Run(() => StartMatlab());
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            PopulateExperimentList();
        }

        private void StartMatlab()
        {
            MatlabMutex.WaitOne(3000);
            try
            {
                MessageTextBox.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    MessageTextBox.Text = "connecting to matlab";
                }));
                var activationContext = Type.GetTypeFromProgID("matlab.application");
                var matlab = (MLApp.MLApp)Activator.CreateInstance(activationContext);
                matlab.Execute("cd '" + Path.Combine(Directory.GetCurrentDirectory(), Properties.Settings.Default.MatlabCodePath) + "'");
                Matlab = matlab;

                MessageTextBox.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    MessageTextBox.Text = "matlab connection established";
                }));
            }
            finally
            {
                MatlabMutex.ReleaseMutex();
            }

        }

        static public void LoggedMessage(String msg)
        {
            if (LogFile != null && msg.Length > 0)
            {
                //byte[] info = new UTF8Encoding(true).GetBytes(" (" + DateTime.Now.ToString("HH:mm:ss.ff dd/MM/yyyy") + "): D " + msg + System.Environment.NewLine);
                //LogFile.Write(info, 0, info.Length);
                LogFile.WriteLine("---- " + DateTime.Now.ToString("HH:mm:ss.ff dd/MM/yyyy") + " ---------------------------------------------");
                LogFile.WriteLine(msg);
                LogFile.Flush();
            }
        }

        private void RunOnMatlab(String cmd)
        {
            MatlabMutex.WaitOne(3000);
            try
            {
                Matlab.Execute("cmd = '" + Regex.Replace(cmd, "'", "''") + "'");
                string res = Matlab.Execute(cmd);
                if (res.Length > 0)
                    LoggedMessage(cmd + System.Environment.NewLine + res);
            }
            finally
            {
                MatlabMutex.ReleaseMutex();
            }
        }

        private String RandString()
        {
            int numOfCharsNeeded = 10;
            return Guid.NewGuid().ToString("n").Substring(0, numOfCharsNeeded);
        }

        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                FolderTextBox.Text = dialog.SelectedPath;
                Properties.Settings.Default.Folder = FolderTextBox.Text;
                Properties.Settings.Default.Save();
                PopulateExperimentList();
            }
        }

        private void PopulateExperimentList()
        {
            DirectoryInfo DirInfo = new DirectoryInfo(FolderTextBox.Text);
            Regex Pattern = new Regex(@"^(\w+)\.exp(\d+).day(\d+).cam(\d+)");
            try
            {
                var filelist = from f in DirInfo.EnumerateFiles()
                               where f.Extension.Equals(".avi")
                               select f;
                ExperimentList = new List<Experiment>();
                foreach (var filename in filelist)
                {
                    string name = Path.GetFileNameWithoutExtension(filename.FullName);
                    Match match = Pattern.Match(name);

                    int result = TrackingList.FindIndex(ex => String.Compare(ex.FileName, filename.FullName, true) == 0);
                    if (match.Success && result == -1)
                        ExperimentList.Add(new Experiment()
                        {
                            Type = match.Groups[1].Value,
                            Number = int.Parse(match.Groups[2].Value),
                            Day = int.Parse(match.Groups[3].Value),
                            Camera = int.Parse(match.Groups[4].Value),
                            FileName = filename.FullName
                        });

                }
                ExperimentList.Sort();
                ExperimentsListView.ItemsSource = ExperimentList;
            }
            catch
            {

            }

            
            //String[] filelist = Directory.GetFiles(Properties.Settings.Default.Folder, "*.avi");
                //string fname = Properties.Settings.Default.Folder;
            //ExperimentList = new List<Experiment>();
            //

            /*foreach (String filename in filelist)
            {
                string name = Path.GetFileNameWithoutExtension(filename);
                Match match = Pattern.Match(name);

                int result = TrackingList.FindIndex(ex => String.Compare(ex.FileName, filename, true) == 0);
                if (match.Success && result == -1)
                    ExperimentList.Add(new Experiment()
                    { 
                        Type = match.Groups[1].Value, 
                        Number = int.Parse(match.Groups[2].Value),  
                        Day = int.Parse(match.Groups[3].Value),
                        Camera = int.Parse(match.Groups[4].Value),
                        FileName = filename
                    });

            }
            ExperimentList.Sort();
            ExperimentsListView.ItemsSource = ExperimentList;*/
        }

        private void AddToTracking(Experiment ex)
        {
            if (ProgramState == ProgramStates.Tracking)
                return;
            String matfile = ex.FileName + @".profile.mat";
            ex.State = Experiment.Status.NotReady; 
            if (File.Exists(matfile))
            {
                MatFileReader reader = new MatFileReader(matfile);

                Boolean IsReady = ((MLUInt8)reader.Content["IsReady"]).Get(0) != 0;
                Boolean IsTracked = ((MLUInt8)reader.Content["IsTracked"]).Get(0) != 0;
                if (IsTracked)
                    ex.State = Experiment.Status.Tracked;
                else if (IsReady)
                    ex.State = Experiment.Status.Ready;
            }
            TrackingList.Add(ex);
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            QueueWaitImage.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
            {
                QueueWaitImage.Visibility = System.Windows.Visibility.Visible;
            }));

            foreach (Experiment ex in ExperimentsListView.SelectedItems)
            {
                AddToTracking(ex);
                ExperimentList.Remove(ex);
            }
            TrackingFilterTextBox_TextChanged(null, null);
            ExperimentFilterTextBox_TextChanged(null, null);

            QueueWaitImage.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
            {
                QueueWaitImage.Visibility = System.Windows.Visibility.Hidden;
            }));

        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (ProgramState != ProgramStates.Idle)
                return;
            foreach (Experiment ex in TrackingListView.SelectedItems)
            {
                ExperimentList.Add(ex);
                TrackingList.Remove(ex);
            }
            ExperimentList.Sort();
            TrackingFilterTextBox_TextChanged(null, null);
            ExperimentFilterTextBox_TextChanged(null, null);
        }

        private void ExperimentFilterTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ExperimentList == null)
                return;
            if (ExperimentFilterTextBox.Text.Length == 0)
            {
                ExperimentsListView.ItemsSource = null;
                ExperimentsListView.ItemsSource = ExperimentList;
                return;
            }

            List<Experiment> temp = new List<Experiment>();
            foreach (Experiment ex in ExperimentList)
            {
                if (ex.FileName.IndexOf(ExperimentFilterTextBox.Text, StringComparison.InvariantCultureIgnoreCase) != -1)
                    temp.Add(ex);
            }
            ExperimentsListView.ItemsSource = null;
            ExperimentsListView.ItemsSource = temp;
        }

        private void ClearExperimentFilterButton_Click(object sender, RoutedEventArgs e)
        {
            ExperimentFilterTextBox.Text = "";
        }

        private void TrackingFilterTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TrackingFilterTextBox.Text.Length == 0)
            {
                TrackingListView.ItemsSource = null;
                TrackingListView.ItemsSource = TrackingList;
                return;
            }

            List<Experiment> temp = new List<Experiment>();
            foreach (Experiment ex in TrackingList)
            {
                if (ex.FileName.IndexOf(TrackingFilterTextBox.Text, StringComparison.InvariantCultureIgnoreCase) != -1)
                    temp.Add(ex);
            }
            TrackingListView.ItemsSource = null;
            TrackingListView.ItemsSource = temp;
        }

        private void ClearTrackingFilterButton_Click(object sender, RoutedEventArgs e)
        {
            TrackingFilterTextBox.Text = "";
        }


        private void ReadMetaData(Experiment curr)
        {

            PreviewPanel.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
            {
                WaitImage.Visibility = System.Windows.Visibility.Visible;
                VideoPlayerControl.Preload(curr.FileName);
            }));
            

            String matfile = curr.FileName + @".profile.mat";
            if (File.Exists(matfile))
            {
                MatFileReader reader = new MatFileReader(matfile);

                try
                {
                    MLUInt8 source = reader.Content["Background"] as MLUInt8;
                    byte[] srcbuf = source.RealByteBuffer.Array();
                    byte[] tgtbuf = new byte[srcbuf.Length];
                    int[] dim = source.Dimensions;

                    for (int ch = 0; ch < dim[2]; ++ch)
                    {
                        for (int r = 0; r < dim[0]; ++r)
                        {
                            for (int c = 0; c < dim[1]; ++c)
                                tgtbuf[r * (dim[2] * dim[1]) + c * dim[2] + ch] = srcbuf[ch * (dim[0] * dim[1]) + c * dim[0] + r];
                        }
                    }

                    BackgroundImage.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                    {
                        BackgroundImage.Source = BitmapSource.Create(dim[1], dim[0], 96, 96, PixelFormats.Rgb24, null, tgtbuf, dim[1] * dim[2]);
                    }));
                }
                catch
                {
                    }

                //
                try
                {
                    string creationDate = DateTime.Parse(((MLChar)reader.Content["CreationDate"]).GetString(0)).ToString();
                    string duration = TimeSpan.FromSeconds(((MLDouble)reader.Content["Duration"]).Get(0)).ToString(@"hh\:mm\:ss\.ff");
                    string remarks = ((MLChar)reader.Content["Remarks"]).GetString(0);
                    string nSubjects = ((MLDouble)reader.Content["nSubjects"]).Get(0).ToString();
                    Boolean IsReady = ((MLUInt8)reader.Content["IsReady"]).Get(0) != 0;
                    Boolean IsTracked = ((MLUInt8)reader.Content["IsTracked"]).Get(0) != 0;
                    PreviewMutex.WaitOne();
                    PreviewPanel.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                    {
                        PreviewPanel.Children.Clear();
                        RemarksTextBox.Text = "";
                        WaitImage.Visibility = System.Windows.Visibility.Hidden;
                        PreviewAddField("Video", curr.FileName, true);
                        PreviewAddField("Created in", creationDate, true);
                        PreviewAddField("Duration", duration, true);
                        PreviewAddField("No. subjects", nSubjects, true);
                        if (IsTracked)
                        {
                            PreviewAddField("Status", "Tracked", true);
                            curr.State = Experiment.Status.Tracked;
                        }
                        else if (IsReady)
                        {
                            PreviewAddField("Status", "Ready", true);
                            curr.State = Experiment.Status.Ready;
                        }
                        else
                        {
                            PreviewAddField("Status", "Not ready", true);
                            curr.State = Experiment.Status.NotReady;
                        }

                        RemarksTextBox.Text = remarks;
                    }));
                }
                finally
                {
                    PreviewMutex.ReleaseMutex();
                }


                /*if (MatlabMutex.WaitOne(3000))
                {
                    Matlab.Execute("CheeseOne.Preview = load('" + matfile + "')");
                }
                else
                {
                    return; 
                }
                MyImage img = MatlabGetImage("CheeseOne.Preview.Background");

                BackgroundImage.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    BackgroundImage.Source = BitmapSource.Create(img.Width, img.Height, 96, 96, PixelFormats.Rgb24, null, img.buffer, img.Width * 3);
                }));

                
                Console.WriteLine(Matlab.Execute("1+2"));*/
            }
            else
            {
                InferMetaData(curr);
            }

        }

        private void InferMetaData(Experiment curr)
        {
            String xmlfile = curr.FileName + @".xml";
            String creationDate = "unknown";
            String remarks = "";
            String duration = "unknown";

            try
            {
                if (File.Exists(xmlfile))
                {
                    XDocument xdoc = XDocument.Load(xmlfile);
                    creationDate = DateTime.Parse(xdoc.Root.Element("Start").Value).ToString();
                    remarks = xdoc.Root.Element("Remarks").Value;
                }
                else
                {
                    FileInfo info = new FileInfo(curr.FileName);
                    creationDate = info.CreationTime.ToString();
                }
            }
            catch { }

            try
            {
                var player = new WindowsMediaPlayer();
                var clip = player.newMedia(curr.FileName);
                duration = TimeSpan.FromSeconds(clip.duration).ToString();
            }
            catch { }

            try
            {
                PreviewMutex.WaitOne();
                PreviewPanel.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                {
                    PreviewPanel.Children.Clear();
                    RemarksTextBox.Text = "";
                    WaitImage.Visibility = System.Windows.Visibility.Hidden;
                    PreviewAddField("Video", curr.FileName, true);
                    PreviewAddField("Created in", creationDate, true);
                    PreviewAddField("Duration", duration, true);
                    PreviewAddField("Status", "Not ready", true);

                    RemarksTextBox.Text = remarks;
                }));
            }
            finally
            {
                PreviewMutex.ReleaseMutex();
            }
        }



        private void TrackingListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BackgroundImage.Source = null;
            PreviewPanel.Children.Clear();

            //Experiment curr = TrackingListView.SelectedItem as Experiment;
            Current = TrackingListView.SelectedItem as Experiment;
            if (Current != null)
                Task.Run(() => ReadMetaData(Current));

        }

        private void PreviewAddField(String name, String value, bool enabled)
        {
            Grid row = new Grid();
            ColumnDefinition namecol = new ColumnDefinition();
            namecol.Width = new GridLength(80);
            ColumnDefinition valuecol = new ColumnDefinition();
            valuecol.Width = new GridLength(1, GridUnitType.Star);
            row.ColumnDefinitions.Add(namecol);
            row.ColumnDefinitions.Add(valuecol);

            Label namelabel = new Label();
            namelabel.Content = name;
            Grid.SetColumn(namelabel, 0);

            TextBox valuelabel = new TextBox();
            valuelabel.Text = value;
            valuelabel.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            valuelabel.Margin = new Thickness(10, 0, 10, 0);
            valuelabel.Height = 23;
            valuelabel.TextAlignment = TextAlignment.Center;
            Grid.SetColumn(valuelabel, 1);

            row.Children.Add(namelabel);
            row.Children.Add(valuelabel);

            row.IsEnabled = enabled;

            PreviewPanel.Children.Add(row);
        }

        private void FolderTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                ExperimentFilterTextBox.Text = "";
                Properties.Settings.Default.Folder = FolderTextBox.Text;
                Properties.Settings.Default.Save();
                PopulateExperimentList();
            }
        }

        private void ExperimentsListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            AddButton_Click(null, null);
        }

        private void ExperimentsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BackgroundImage.Source = null;
            PreviewPanel.Children.Clear();

            Current = ExperimentsListView.SelectedItem as Experiment;
            if (Current != null)
                Task.Run(() => ReadMetaData(Current));

        }

        private MyImage MatlabGetImage(string variable)
        {
            /*if (MatlabMutex.WaitOne(3000))
            {
                String tempvar = "temp" + RandString();
                Matlab.Execute(tempvar + " = im2uint8(" + variable + ");");
                Matlab.Execute("if size(" + tempvar + ", 3) == 1; " + tempvar + " = repmat(" + tempvar + ", [1 1 3]);");
                byte[, ,] source = Matlab.GetVariable(tempvar, "base");
                MyImage img = new MyImage() { Height = source.GetLength(0), Width = source.GetLength(1) };
                img.buffer = new byte[img.Width * img.Height * 3];
                System.Buffer.BlockCopy(source, 0, img.buffer, 0, img.buffer.Length);
                return img;
            }*/
            return null;
        }

        private void RemarksTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (RemarksTextBox.Text == "")
            {
                RemarksTextBox.Visibility = System.Windows.Visibility.Hidden;
                RemarksLabel.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                RemarksTextBox.Visibility = System.Windows.Visibility.Visible;
                RemarksLabel.Visibility = System.Windows.Visibility.Visible;
            }
        }


        [System.Runtime.InteropServices.DllImport("USER32.DLL", CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        public static extern IntPtr FindWindow(String lpClassName, String lpWindowName);

        [System.Runtime.InteropServices.DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public static void bringToFront(string title)
        {
            // Get a handle to the Calculator application.
            IntPtr handle = FindWindow(null, title);

            // Verify that Calculator is a running process.
            if (handle == IntPtr.Zero)
            {
                return;
            }

            // Make Calculator the foreground application
            SetForegroundWindow(handle);
        }

        private void PrepareButton_Click(object sender, RoutedEventArgs e)
        {
/*            if (Current == null || Current.FileName == "")
                return;
            var Pre = new PrepareExperiment(Current.FileName);
            Pre.Show();
            return;*/
            if (Current == null || Current.FileName == "")
                return;
            //Task.Run(() => 
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
            {
                PrepareButton.Content = "Wait...";
            }));
            RunOnMatlab("CheeseStart('" + Current.FileName + "')");
            bringToFront("CheeseInit");
            //RunOnMatlab("global CheeseInitData; set(CheeseInitData.Handles.FileName, 'String', '" + Current.FileName + "'); cb = get(CheeseInitData.Handles.LoadFile, 'Callback'); cb(CheeseInitData.Handles.figure1, '')");
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
            {
                PrepareButton.Content = "Prepare";
            }));
            //);
        }

        public class EnumConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return ((Experiment.Status)value).ToString();
            }
            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }
        }

        private Experiment.Status GetExperiemntState(Experiment ex)
        {
            String matfile = ex.FileName + @".profile.mat";
            Experiment.Status state = Experiment.Status.NotReady;
            if (File.Exists(matfile))
            {
                MatFileReader reader = new MatFileReader(matfile);

                Boolean IsReady = ((MLUInt8)reader.Content["IsReady"]).Get(0) != 0;
                Boolean IsTracked = ((MLUInt8)reader.Content["IsTracked"]).Get(0) != 0;
                if (IsTracked)
                    return Experiment.Status.Tracked;
                else if (IsReady)
                    return Experiment.Status.Ready;
            }
            return state;
        }

        private void RefreshTrackingList()
        {
            QueueWaitImage.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
            {
                QueueWaitImage.Visibility = System.Windows.Visibility.Visible;
            }));

            foreach (Experiment ex in TrackingList)
                ex.State = GetExperiemntState(ex);
            TrackingListView.ItemsSource = null;
            TrackingListView.ItemsSource = TrackingList;
            QueueWaitImage.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
            {
                QueueWaitImage.Visibility = System.Windows.Visibility.Hidden;
            }));
        }

        private void RunTracking()
        {
            foreach (Experiment ex in TrackingList)
            {
                if (ProgramState == ProgramStates.Tracking)
                {
                    ex.State = Experiment.Status.Running;
                    TrackingListView.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                    {
                        TrackingListView.Items.Refresh();
                    }));

                     RunOnMatlab("CheeseTrackRun('" + ex.FileName + "')");
                    //RunOnMatlab("global CheeseInitData; set(CheeseInitData.Handles.FileName, 'String', '" + Current.FileName + "'); cb = get(CheeseInitData.Handles.LoadFile, 'Callback'); cb(CheeseInitData.Handles.figure1, '')");
                    this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                    {
                        PrepareButton.Content = "Prepare";
                    }));

                    
                    ex.State = GetExperiemntState(ex);
                }
                else
                {
                    break;
                }
            }
            MainGrid.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
            {
                TrackingListView.Items.Refresh();
                TrackButton.Background = new SolidColorBrush(Color.FromRgb(215, 227, 163));
                TrackButton.Foreground = new SolidColorBrush(Color.FromRgb(92, 106, 47));
                TrackButton.Content = "Track";
                ProgramState = ProgramStates.Idle;
                AddButton.IsEnabled = true;
                RemoveButton.IsEnabled = true;

            }));

        }

        private void TrackButton_Click(object sender, RoutedEventArgs e)
        {
            if (ProgramState == ProgramStates.Tracking)
            {
                TrackButton.Background = new SolidColorBrush(Color.FromRgb(215, 227, 163));
                TrackButton.Foreground = new SolidColorBrush(Color.FromRgb(92, 106, 47));
                TrackButton.Content = "Track";
                ProgramState = ProgramStates.Idle;
                AddButton.IsEnabled = true;
                RemoveButton.IsEnabled = true;
                return;
            }

            if (TrackingList.Count == 0)
                return;

            RefreshTrackingList();

/*            foreach (Experiment ex in TrackingList)
            {
                if (ex.State == Experiment.Status.NotReady || ex.State == Experiment.Status.Unknown)
                {
                    MessageBoxResult result = MessageBox.Show(this, "Some experiments need to be prepared before tracking.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }*/

            foreach (Experiment ex in TrackingList)
            {
                if (ex.State == Experiment.Status.Tracked)
                {
                    MessageBoxResult result = MessageBox.Show(this, "Some experiments have already been tracked. If you continue, the previous tracking information will be lost.", "Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                    if (result != MessageBoxResult.OK)
                        return;
                    break;
                }
            }

            ProgramState = ProgramStates.Tracking;

            TrackButton.Background = new SolidColorBrush(Color.FromRgb(251, 185, 169));
            TrackButton.Foreground = new SolidColorBrush(Color.FromRgb(153, 63, 37));
            TrackButton.Content = "Cancel";
            AddButton.IsEnabled = false;
            RemoveButton.IsEnabled = false;

            Task.Run(() => RunTracking());
        }

        private void TrackingListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            RemoveButton_Click(null, null);
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshTrackingList();
        }

        private void PreviewButton_Click(object sender, RoutedEventArgs e)
        {
            string currPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            // Prepare the process to run
            ProcessStartInfo start = new ProcessStartInfo();
            // Enter in the command line arguments, everything you would enter after the executable name itself
            start.Arguments = Current.FileName;
            // Enter the executable to run, including the complete path
            start.FileName = Path.Combine(currPath, @"External\Social Tracking.exe");
            //start.FileName = Regex.Replace(start.FileName, "file:\\\\", "");
            // Do you want to show a console window?
            //start.WindowStyle = ProcessWindowStyle.Hidden;
            //start.CreateNoWindow = true;

            // Run the external process & wait for it to finish
            try
            {
                int exitCode;
                using (Process proc = Process.Start(start))
                {
                    proc.WaitForExit();

                    // Retrieve the app's exit code
                    exitCode = proc.ExitCode;
                }
            }
            catch (Exception err)
            {
                System.Windows.MessageBox.Show("Unable to start preview: " + err.Message);
            }

        }

        private void LogButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start("Notepad.exe", Path.Combine(Directory.GetCurrentDirectory(), "./CheeseOne.log"));
            }
            catch (Exception err)
            {
                System.Windows.MessageBox.Show("Unable to open  log " + err.Message);
            }
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new TextDialog("Matlab path", "specify path to matlab code");
            dialog.Response = Properties.Settings.Default.MatlabCodePath;
            if (dialog.ShowDialog() == true)
            {
                Properties.Settings.Default.MatlabCodePath = dialog.Response;
                Properties.Settings.Default.Save();
                Matlab.Execute("cd " + Properties.Settings.Default.MatlabCodePath);
            }
        }



    }

    public class MyImage
    {
        public int Height;
        public int Width;
        public byte[] buffer;
    }

    public class Experiment : IComparable<Experiment>
    {
        public string Type { get; set; }
        public int Number { get; set; }
        public string FileName { get; set; }
        public int Day { get; set; }
        public int Camera { get; set; }
        public enum Status { Running, Ready, Tracked, NotReady, Unknown };
        private Status state = Status.Unknown;
        public Status State 
        { 
            get { return state; } 
            set { 
                state = value;
                StateString = state.ToString();
            } 
        }
        public String StateString { get; set; }

        public int CompareTo(Experiment other)
        {
           return String.Compare(Path.GetFileNameWithoutExtension(this.FileName), Path.GetFileNameWithoutExtension(other.FileName), true);
        }
    }


}
