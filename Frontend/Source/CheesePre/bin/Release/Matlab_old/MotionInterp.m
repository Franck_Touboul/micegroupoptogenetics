function [ox, oy] = MotionInterp(x, y, range, bounds, maxWin)

ox = interp1([range(1)-1 range(end)+1], x([range(1)-1 range(end)+1]), range);
oy = interp1([range(1)-1 range(end)+1], y([range(1)-1 range(end)+1]), range);

return;
if nargin < 5
    maxWin = 3;
end

pre = range(1)-maxWin:range(1)-1;
pre = pre(pre > 0);
pre = pre(isfinite(x(pre)) & isfinite(y(pre)));

post = range(end)+1:range(end)+maxWin;
post = post(post <= length(x));
post = post(isfinite(x(post)) & isfinite(y(post)));

ox = spline([pre post], x([pre post]), range);
oy = spline([pre post], y([pre post]), range);

if nargin >= 4
    mox1 = min(ox);
    mox2 = max(ox);
    
    moy1 = min(oy);
    moy2 = max(oy);
    
    if mox1 < 1 || mox2 > bounds(1) || moy1 < 1 || moy2 > bounds(2)
        ox = interp1([range(1)-1 range(end)+1], x([range(1)-1 range(end)+1]), range);
        oy = interp1([range(1)-1 range(end)+1], y([range(1)-1 range(end)+1]), range);
    end
end