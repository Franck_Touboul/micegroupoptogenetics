function obj = TrackOptimize(obj)
%%
try
    for i=1:length(obj.Analysis.Potts.Model)
        obj.Analysis.Potts.Model{i}.inputPerms = int8(obj.Analysis.Potts.Model{i}.inputPerms);
        obj.Analysis.Potts.Model{i}.perms = logical(obj.Analysis.Potts.Model{i}.perms);
        obj.Analysis.Potts.Model{i}.order = int8(obj.Analysis.Potts.Model{i}.order);
        %obj.Analysis.Potts.Model{i} = rmfield(obj.Analysis.Potts.Model{i}, 'features');
    end
end
%%
% try
%     obj.regions = int8(obj.regions);
%     obj.zones = int8(obj.zones);
%     obj.sheltered = logical(obj.sheltered);
%     obj.speed = single(obj.speed);
%     obj.hidden = logical(obj.hidden);
% end
%%
try
    for i=1:size(obj.Hierarchy.ChaseEscape.interactions.states, 1)
        for j=1:size(obj.Hierarchy.ChaseEscape.interactions.states, 2)
            obj.Hierarchy.ChaseEscape.interactions.states{i, j} = int8(obj.Hierarchy.ChaseEscape.interactions.states{i, j});
            obj.Interactions.PredPrey.states{i, j} = int8(obj.Interactions.PredPrey.states{i, j});
        end
    end
end
%%
try
    obj.Interactions = rmfield(obj.Interactions, {'contact', 'proximity', 'distance', 'angle'});
end

