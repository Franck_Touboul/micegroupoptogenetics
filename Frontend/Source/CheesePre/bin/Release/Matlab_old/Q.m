classdef Q
    methods (Static = true)
        function x = torow(x)
            x = x(:)';
        end

        function x = tocol(x)
            x = x(:);
        end
        
        function v = minval(t)
            % minimal possible value of a specified type
            if isinteger(t)
                v = intmin(class(t));
            elseif isfloat(t)
                v = -inf;
            elseif islogical(t)
                v = false;
            else
                error;
            end
        end
        
        function f = toframes(y, sz, overlap)
            % divide y into frames of size sz and overlap
            y = y(:);
            ncol = fix((length(y)-overlap)/(sz-overlap));
            colindex = 1 + (0:(ncol-1))*(sz-overlap);
            rowindex = (1:sz)';
            f = zeros(sz, ncol, class(y)); %#ok<*ZEROLIKE>
            f(:) = y(rowindex(:,ones(1,ncol))+colindex(ones(sz,1),:)-1);
        end
        
        function m = quickmedian(y)
            hi = max(y);
            lo = min(y);
            m = (lo + hi) / 2;
            h = length(y)/2;
            while true
                count = sum(y > m);
                if abs(count - h) < 1
                    break;
                elseif count < h
                    hi = m;
                else
                    lo = m;
                end
                m = (lo + hi) / 2;
            end
            m = y(Q.argmin(abs(y-m)));
        end
        
        function order = hiersort(D)
            if ~isvector(D)
                D = squareform(D,'tovector');
            end
            z = linkage(D);
            order = optimalleaforder(z, D);
        end
        
        function c = combinations(varargin)
            if nargin == 1 && iscell(varargin{1})
                c = Q.combinations(varargin{1}{:});
                return;
            end
            if nargin == 1
                c = varargin{1}(:);
            else
                other = Q.combinations(varargin{2:end});
                nother = size(other, 1);
                c = zeros(length(varargin{1}) * nother, size(other, 2)+1);
                for i=1:length(varargin{1})
                    c((i-1)*nother+1:i*nother, :) = [ones(nother, 1) * varargin{1}(i), other];
                end
            end
        end
        
        function out = struct2xml(param, filename)
            if ischar(param)
                name = param;
                param = evalin('caller', name);
            else
                name = 'root';
            end
            doc  = com.mathworks.xml.XMLUtils.createDocument(name);
            root = doc.getDocumentElement;
            out = xmlwrite(parse(param, doc, root));
            if nargin > 1
                dlmwrite(filename, out);
            end
            function obj = parse(param, doc, root)
                if isstruct(param)
                    fields = fieldnames(param);
                    for i  = 1 : numel(fields)
                        if isstruct(param.(fields{i}))
                            write(doc, root, fields{i}, '')
                            parse(param.(fields{i}), doc, root.getLastChild)
                        else
                            write(doc, root, fields{i}, param.(fields{i}))
                        end
                    end
                else
                    doc = 'could not convert';
                end
                obj = doc;
            end
            function write(doc, root, pname, param)
                if isempty(param) == 1
                    root.appendChild(doc.createElement(pname));
                else
                    elem = doc.createElement(pname);
                    if ismatrix(param)
                        val = Console.Format(param);
                    else
                        val = Console.Format(param);
                    end
                    elem.appendChild(doc.createTextNode(val));
                    root.appendChild(elem);
                end
            end
        end
        
        function str = capital(str)
            if isempty(str)
                return;
            end
            str = [upper(str(1)) str(2:end)];
        end
        
        function args = defaultargs(varargin)
            stack = dbstack;
            %%
            offset = 1;
            if islogical(varargin{1})
                output = varargin{1};
                offset = 2;
            else
                output = true;
            end

            %%
            args = varargin{offset};
            if isfield(args, 'verbose')
                output = args.verbose;
            end
            %%
            if length(stack) > 1 && output
                Console.Message('setting arguments for <a href="matlab: opentoline(%s, %d)">%s</a>', stack(2).file, stack(2).line, stack(2).name);
            end
            %%
            namelen = 20;
            vallen = 12;
            for i=offset+1:2:length(varargin)
                if isfield(args, varargin{i})
                    if output
                        Console.Message(1, ['%-' num2str(namelen) 's = %-' num2str(vallen) 's'], ['''' varargin{i} ''''], Console.Format(args.(varargin{i})));
                    end
                else
                    args.(varargin{i}) = varargin{i+1};
                    if output
                        name = ['''' varargin{i} ''''];
                        if length(name) > namelen
                            currvallen = vallen - (length(name) - namelen);
                        else
                            currvallen = vallen;
                        end
                        Console.Message(1, ['%-' num2str(namelen) 's = %-' num2str(currvallen) 's [default]'], name, Console.Format(varargin{i+1}));
                    end
                end
            end
            
        end

        function a = getindex(arr, varargin)
            a = arr(varargin{:});
        end
        
        function c = shift(c, k, pad)
            % c = shift(c, k, pad=0)
            %   like circshift only uses pad as fill value
            if nargin < 3
                pad = 0;
            end
            c = circshift(c, k);
            for i=1:length(k)
                if k(i) == 0
                    continue;
                end
                c = Q.pushdim(c, i);
                if k(i) > 0
                    c(1:k(i), :) = pad;
                else
                    c(end-k(i):end, :) = pad;
                end
                c = Q.popdim(c, i);
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % struct operations
        function val = getfield(var, name)
            fields = textscan(name, '%s', 'Delimiter', '.');
            val = getfield(var, fields{1}{:});
        end
        
        function var = setfield(var, name, val)
            fields = textscan(name, '%s', 'Delimiter', '.');
            var = setfield(var, fields{1}{:}, val);
        end

        function to = cpfield(from, to, name, ignore)
            if nargin == 2
                f = fieldnames(from);
                for i=1:length(f)
                    to = Q.cpfield(from, to, f{i});
                end
                return;
            end
            if nargin == 3
                ignore = false;
            end
            if Q.isfield(from, name)
                to = Q.setfield(to, name, Q.getfield(from, name));
            elseif ~ignore
                throw(MException('Q:nonExistentField', 'Reference to non-existent field ''%s''', name));
            end
        end
        
        function tf = isfield(var, name)
            %% like matlab's isfield but also approves properties and
            %% can support several levels, like: Q.isfield(s, 'a.b.c.d');
            %%
            c = strsplit(name, '.');
            curr = var;
            tf = true;
            for i=1:length(c)
                if ~isfield(curr, c{i}) && ~isprop(curr, c{i})
                    tf = false;
                    break;
                end
                curr = curr.(c{i});
            end
        end
    end
    methods (Static = true)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % mathematics
        function c = nancorr(x, y)
            if nargin == 1
                y = x;
            end
            c = zeros(size(x, 2), size(y, 2));
            for i=1:size(x, 2)
                X = x(:, i);
                for j=1:size(y, 2)
                    Y = y(:, j);
                    valid = ~isnan(X) & ~isnan(Y);
                    c(i,j) = corr(X(valid), Y(valid));
                end
            end
        end
        
        function x = pushdim(x, dim)
            % pushdim(x, dim) makes dim the first dimension
            if nargin == 1 || dim == 1
                return;
            end
            x = permute(x, [dim Q.exclude(ndims(x), dim)]);
        end

        function x = popdim(x, dim)
            % popdim(x, dim) returns first dimension to dimension dim
            if nargin == 1 || dim == 1
                return;
            end
            seq = cat(2, 2:dim, 1, dim+1:ndims(x));
            x = permute(x, seq);
        end
        
        function s = smooth(x, len, dim)
            if dim == 1
                c = cumsum(x - [zeros(winframes,size(x, 2)); x(1:end-winframes, :)]);
            elseif dim == 2
                c = cumsum(x - [zeros(winframes,size(x, 2)); x(1:end-winframes, :)]);
            else
                error
            end
                
        end
        
        function s = nansmooth(x, len)
            % smoothes s using a runing average smoothing. Ignores nans.
            x1 = x; 
            x1(isnan(x)) = 0;
            s = convn(x1(:), ones(len, 1), 'same');
            m = convn(~isnan(x(:)), ones(len, 1), 'same');
            s = reshape(s ./ m, size(x));
        end

        function s = nanblock(x, len)
            % smoothes s using a block smoothing. Ignores nans.
            n = ceil(length(x) / len);
            x(end+1:n*len) = nan;
            x = reshape(x, len, n);
            s = nanmean(x);
        end
        
        function s = discspace(from, to, count)
            % like linspace only discrete values
            s = round(linspace(from, to, count));
        end
        
        function i = index(x)
            i = 1:length(x(:));
            i = reshape(i, size(x));
        end
        
        function j = exclude(n, i)
            % exclude(n, i) returns the sequence from 1 to n excluding i,
            % i.e. 1 ,.., i-1, i+1 ,..., n
            j = 1:n;
            if nargin > 1
                j(i) = [];
            end
        end
        
        function v = inrange(x, m1, m2)
            v = x >= m1 & x <= m2;
        end

        function map = overlap(x1, x2, m1, m2)
            % overlap(x1, x2, m1, m2), returns map of segments
            %    [m1(i), m2(i)] that overlap with the segments [x1, x2]
            if length(x1) == 1
                map = (x1 >= m1 & x1 <= m2) | (x2 >= m1 & x2 <= m2) | (x1 <= m1 & x2 >= m2);
            else
                gt11 = bsxfun(@ge, x1(:), m1(:)');
                lt12 = bsxfun(@le, x1(:), m2(:)');
                gt21 = bsxfun(@ge, x2(:), m1(:)');
                lt22 = bsxfun(@le, x2(:), m2(:)');
                lt11 = bsxfun(@le, x1(:), m1(:)');
                gt22 = bsxfun(@ge, x2(:), m2(:)');
                map = (gt11 & lt12) | (gt21 & lt22) |  (lt11 & gt22);
            end
        end
        
        
        function x = padtop(x, padsize, padval)
            if nargin < 2; padsize = 1; end
            if nargin < 3; padval = 0; end
            x = padarray(x, padsize, padval, 'pre');
        end

        function x = padbottom(x, padsize, padval)
            if nargin < 2; padsize = 1; end
            if nargin < 3; padval = 0; end
            x = padarray(x, padsize, padval, 'post');
        end

        function x = padleft(x, padsize, padval)
            if nargin < 2; padsize = 1; end
            if nargin < 3; padval = 0; end
            x = padarray(x, [0 padsize], padval, 'pre');
        end

        function x = padright(x, padsize, padval)
            if nargin < 2; padsize = 1; end
            if nargin < 3; padval = 0; end
            x = padarray(x, [0 padsize], padval, 'post');
        end
        
        function idx = FindFirst(x, dim)
            if nargin < 2
                dim = 1;
            end
            c = cumsum(x~=0, dim);
            idx = min(sum(c == 0) + 1, size(x, dim));
        end

        function idx = FindLast(x, dim)
            %%
            if nargin < 2
                dim = 1;
            end
            c = cumsum(flipdim(x, dim)~=0, dim);
            idx = max(size(x, dim) - sum(c == 0), 1);
        end
        
        function y = frac(x)
            % fractional part of a number
            y = abs(x - fix(x));
        end
        
        function x = normalize(x)
            % n = Normalize(x) ensures all values of x are between 0 and 1
            m1 = min(x(:));
            m2 = max(x(:));
            x = (x - m1 - eps) / (m2 - m1);
        end
        
        function [r, N] = rank(x, dim, nranks)
            % for each sample returns it's rank
            % for example, for x = [1 4 2 9 5 6], the function will return 
            % rank(x) = [1 3 2 6 4 5]
            if nargin > 1
                x = permute(x, [dim, Q.exclude(ndims(x), dim)]);
            else
                dim = [];
                if size(x, 1) == 1
                    dim = 2;
                    x = permute(x, [dim, Q.exclude(ndims(x), dim)]);
                end
            end
            [~, o] = sort(x, 1);
            N = size(x, 1);
            %%
            seq = 1:N;
            r = zeros(size(x));
            idx = cell(1, ndims(r));
            [idx{:}] = ind2sub(size(r), 1:numel(r));
            idx{1} = o(:)';
            r(sub2ind(size(r), idx{:})) = repmat(seq, 1, numel(o)/N);
            %%
            if ~isempty(dim)
                b = 1:dim-1;
                a = dim+1:ndims(x);
                r = permute(r, [b+1 1 a]);
            end
            %%
            if nargin >= 3
                r = floor((r - 1) / N * nranks) + 1;
            end
        end
        
        function y = nwarp(x, varargin)
            % warp samples to match a normal distribution
            [rank, N] = Q.rank(x, varargin{:});
            vals = norminv(linspace(1/N, 1-1/N, N));
            y = vals(rank);
        end

        function y = nwarpR(x, varargin)
            % warp samples to match a normal distribution
            [rank, N] = Q.rank(x, varargin{:});
            vals = norminv(linspace(1/N, 1-1/N, N));
            y = vals(rank);
        end
        
        function y = znorm(x, varargin)
            y = bsxfun(@rdivide, bsxfun(@minus, x, mean(x, varargin{:})), std(x, 0, varargin{:}));
        end
        
        function y = znormR(x, varargin)
%             %%
%             if nargin < 2
%                 dim = 1;
%             else
%                 x = Q.pushdim(x, dim);
%             end
%             
%             cutoff = 1;
%             p = normcdf(cutoff * [-1 1]);
%             q = quantile(x, p);
%             y = bsxfun(@rdivide, bsxfun(@minus, x, (q(1, :) + q(2, :))/2), (q(2, :)-q(1, :))/2);
%             if dim ~= 1
%                 y = Q.popdim(y, dim);
%             end
            %%
            y = bsxfun(@rdivide, bsxfun(@minus, x, Q.meanR(x, varargin{:})), Q.stdR(x, varargin{:}));
        end
        
        function m = meanR(x, varargin)
            m = median(x, varargin{:});
        end
        
        function m = stdR(x, varargin)
            % computes the std using mad (median absolute deviation), and scales accordingly
            % so that it equals the std for normaly distributed data
            m = mad(x, 1, varargin{:}) / norminv(3/4);
        end

        function m = fitR(x, dim, nstd)
            thresh = normcdf(0, nstd);
            q = quantile(x, [thresh, 1-thresh], dim);
            m = 2 * bsxfun(@rdivide, bsxfun(@minus, x, Q.meanR(x, dim)), diff(q, 1, dim)) * nstd;
        end
        
        function [bins, idx, count] = binify(data, nbins)
            s = sort(data);
            %%
            nvals = nbins - 2;
            bins = [s(1) zeros(1, nvals) inf];
            offset = 1;
            succ = true;
            for i=0:nvals
                idx = offset;
                bins(i+1) = s(idx);
                offset = round(offset + (length(s) - offset + 1) / (nvals - i + 1));
                offset = max(offset, find(s > bins(i+1), 1));
                if isempty(offset)
                    succ = false;
                    break;
                end
            end
            if ~succ
                us = unique(s);
                if length(us) >= nvals
                    bins(2:1+nvals) = us(round(linspace(1, length(us), nvals)));
                else
                    bins(2:1+nvals) = linspace(0, 2 * max(us), nvals);
                end
            end
            %%
            if nargout > 1
                [count, idx] = histc(data, bins);
            end
        end
        
        
        function m = stderr(x, dim)
            if nargin == 1
                dim = 1;
            end
            m = std(x, 0, dim) / sqrt(size(x, dim));
        end

        function m = nanstderr(x, dim)
            if nargin == 1
                dim = 1;
            end
            m = nanstd(x, 0, dim) ./ sqrt(sum(~isnan(x), dim));
        end
        
        function p = peaks(mat, dim, minpeak)
            if nargin < 3
                minpeak = -inf;
            end
            if nargin < 2
                dim = 1;
            end
            if dim ~= 1
                seq = 1:ndims(mat);
                seq(dim) = [];
                mat = permute(mat, [dim seq]);
            end
            d = diff(mat, 1);
            p = [false(1, size(mat, 2)); d(1:end-1, :) > 0 & d(2:end, :) < 0; false(1, size(mat, 2))];
            if minpeak ~= -inf
                p(mat < minpeak) = false;
            end
            if dim ~= 1
                p = permute(p, [2:dim 1 dim+1:ndims(mat)]);
            end
        end
        
        function m = softabs(x, alpha)
            m = sqrt(x.^2 + alpha^2);
        end
        
        function m = softmax(x, k, varargin)
            % m = Softmax(x, k, dim) smooth and differentiable approximation of maximum
            %    k is the scale
            if nargin < 2
                k = 1;
            end
            m = log(sum(exp(k * x), varargin{:}))/k;
            %m = sum(x .* expx, dim) ./ sum(expx, dim);
        end

        function mm = minmax(x, varargin)
            mm = [min(x, [], varargin{:}), max(x, [], varargin{:})];
        end
        
        function idx = argmax(x, dim)
            if nargin <= 1
                [~, idx] = max(x);
            else
                [~, idx] = max(x, [], dim);
            end
        end

        function idx = argmaxi(x, dim)
            %% like argmax, but returns the linear index of the cell in the matrix 
            if nargin <= 1
                [~, idx] = max(x);
            else
                [~, idx] = max(x, [], dim);
            end
            
            if nargin <= 1 || dim == 1
                idx = sub2ind(size(x), idx, 1:length(idx));
            else
                idx = sub2ind(size(x), (1:length(idx))', idx);
            end
        end
        
        function idx = argmin(x, varargin)
            if nargin <= 1
                [~, idx] = min(x);
            else
                [~, idx] = min(x, [], dim);
            end
        end
        
        function [idx, coord] = argmaxnd(x)
            [~, idx] = max(x(:));
            a = cell(1, ndims(x));
            [a{1:ndims(x)}] = ind2sub(size(x), idx);
            coord = cell2mat(a);
        end
        
        function [idx, coord] = argminnd(x)
            [~, idx] = min(x(:));
            a = cell(1, ndims(x));
            [a{1:ndims(x)}] = ind2sub(size(x), idx);
            coord = cell2mat(a);
        end
        
        function s = sumnd(x)
            s = sum(x(:));
        end
        
        function [val, coord] = maxnd(x)
            % [val, coord] = MaxIJ(x) returns the maximal value (val) in x and it's coordinated (coord)
            % unlike the built-in max x oppearates on all dimensions
            %%
            [val, i] = max(x(:));
            a = cell(1, ndims(x));
            [a{1:ndims(x)}] = ind2sub(size(x), i);
            coord = cell2mat(a);
        end
        
        function [val, coord] = minnd(x)
            % [val, coord] = MinIJ(x) returns the minimal value (val) in x and it's coordinated (coord)
            % unlike the built-in max x oppearates on all dimensions
            %%
            [val, i] = min(x(:));
            a = cell(1, ndims(x));
            [a{1:ndims(x)}] = ind2sub(size(x), i);
            coord = cell2mat(a);
        end
        
        function str = randstr(length)
            % str = RandStr(length) generate a random string of specified length
            alphabet = 'abcdefghijklmnopqrstuvwxyz0123456789';
            str = alphabet(randi(size(alphabet, 2), 1, length));
        end
        
        function varargout = findsegs(map, data)
            c = diff([0 map(:)' 0]);
            begf = find(c > 0);
            endf = find(c < 0) - 1;
            
            if nargin >= 2
                events = repmat(struct('data', [], 'beg', 0, 'end', 0), length(begf), 1);
                for i=1:length(begf)
                    if isvector(data)
                        events(i).data = data(begf(i):endf(i));
                    else
                        events(i).data = data(begf(i):endf(i), :);
                    end
                    events(i).beg = begf(i);
                    events(i).end = endf(i);
                end
                varargout{1} = events;
            else
                varargout{1} = begf;
                varargout{2} = endf;
            end
        end
        
        function plotsegs2(map, y, varargin)
            [b,e] = Q.findsegs(map);
            for i=1:length(e)
                if nargin > 2
                    plot([b(i) e(i)], [y y], varargin{:});
                else
                    plot([b(i) e(i)], [y y], 'r-', 'LineWidth', 4);
                end
                Fig.Hon;
            end
            Fig.Hoff;
        end
        
        function plotsegs(map, c, factor)
            if nargin < 3
                factor = 1;
            end
            [b, e] = Q.findsegs(map);
            ax = axis;
            y = ax(3:4);
            y(1) = y(1) + factor * (y(2)-y(1)) / 100;
            y(2) = y(2) - factor * (y(2)-y(1)) / 100;
            for i=1:length(e)
                patch([b(i) e(i) e(i) b(i)], [y(1) y(1) y(2) y(2)], 'w', 'FaceColor', c, 'EdgeColor', c, 'FaceAlpha', .2);
            end
        end
        
        function map = removegaps(map, maxgap)
            [b, e] = Q.findsegs(map);
            merge = (b(2:end) - e(1:end-1) - 1) <= maxgap;
            for i=find(merge)
                map(e(i)+1:b(i+1)-1) = true;
            end
        end
        
        function map = filtergaps(map, minsize)
            [b, e] = Q.findsegs(map);
            l = e - b - 1;
            b = b(l>minsize);
            e = e(l>minsize);
            map = Q.segstomap(length(map), b, e);
        end
        
        function [b,e] = mergesegs(map, merge)
            %%
            [b,e] = Q.findsegs(map);
            idx = 1;
            valid = false(size(b));
            for i=1:length(merge)
                if idx == i
                    valid(idx) = true;
                end
                if merge(i)
                    e(idx) = e(i+1);
                else
                    idx = i+1;
                end
            end
            b = b(valid);
            e = e(valid);
        end
        
        function map = segstomap(len, b, e)
            map = false(1, len);
            for i=1:length(b)
                map(b(i):e(i)) = true;
            end
        end
    end
end
