function str = ParseError(me)
%%
if ~isempty(me.stack)
    s = me.stack(end);
    str = sprintf('<a href="matlab: opentoline(%s,%d)">%s(%d): %s</a>', s.file, s.line, s.name, s.line, me.message);
else
    str = me.message;
end