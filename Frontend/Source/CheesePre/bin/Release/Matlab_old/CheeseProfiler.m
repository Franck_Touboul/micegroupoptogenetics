function obj = CheeseProfiler(obj)
%%
obj = CheeseSquare(obj);
%obj = TrackLoad(obj);

Profile = struct;
Profile.Individual = struct;
Profile.Group = struct;
Profile.Daily = struct;
Profile.Pairwise = struct;
Profile.Events = struct;

%[obj, meta] = SocialParseName(obj);

%% Social rank
Profile.Individual(1).Name = 'Social rank (1 day) [1 dominant - 4 submissive]';
try
    r = max(obj.Hierarchy.AggressiveChase.rank) - obj.Hierarchy.AggressiveChase.rank + 1;
    for s=1:obj.nSubjects
        Profile.Individual(end).Val(s, 1) = r(s);
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% David Score
Profile.Individual(end+1).Name = 'David score (all days) [higher = more dominant]';
try
    NormDSonDij = Hierarchy.DavidScore(sum(obj.Hierarchy.Group.AggressiveChase.ChaseEscape, 3));
    for s=1:obj.nSubjects
        Profile.Individual(end).Val(s, 1) = NormDSonDij(s);
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end


%% Time outside
Profile.Individual(end+1).Name = 'Time outside of nests [hours]';
try
    for s=1:obj.nSubjects
        Profile.Individual(end).Val(s, 1) = sum(obj.Tracking.valid & ~obj.Tracking.sheltered(s, :)) / obj.Video.FrameRate / 60/ 60;
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% Elo rating
Profile.Individual(end+1).Name = 'Elo rating (end of day)';
try
    Elo = Hierarchy.EloRating(obj);
    Profile.Individual(end).Val = Elo.Scores(:, end);
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% No. of contacts
Profile.Individual(end+1).Name = 'Number of contacts';
try
    nc = hist3(sort([obj.Hierarchy.Contacts.List.subjects], 1)', 'Edges', {1:obj.nSubjects, 1:obj.nSubjects}); 
    nc = nc + nc';

    Profile.Individual(end).Val = sum(nc, 2);
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% % time in contact
Profile.Individual(end+1).Name = '% of time in contact';
try
    subj = [obj.Hierarchy.Contacts.List.subjects];
    begf = [obj.Hierarchy.Contacts.List.beg];
    begf = min(begf);
    endf = [obj.Hierarchy.Contacts.List.end];
    endf = max(endf);
    for s=1:obj.nSubjects
        m = any(subj == s);
        len = endf(m) - begf(m) + 1;
        Profile.Individual(end).Val(s, 1) = sum(len) / sum(obj.Tracking.valid);
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end


%% median duration of contact
Profile.Individual(end+1).Name = 'Median duration of contact [secs]';
try
    subj = [obj.Hierarchy.Contacts.List.subjects];
    begf = [obj.Hierarchy.Contacts.List.beg];
    begf = min(begf);
    endf = [obj.Hierarchy.Contacts.List.end];
    endf = max(endf);
    for s=1:obj.nSubjects
        m = any(subj == s);
        len = endf(m) - begf(m) + 1;
        Profile.Individual(end).Val(s, 1) = median(len) / obj.Video.FrameRate;
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end


%% Number of chases
Profile.Individual(end+1).Name = 'Number of chases';
try
    p = sum(obj.Hierarchy.AggressiveChase.ChaseEscape, 2);
    for s=1:obj.nSubjects
        Profile.Individual(end).Val(s, 1) = p(s);
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% Number of escapes
Profile.Individual(end+1).Name = 'Number of escapes';
try
    p = sum(obj.Hierarchy.AggressiveChase.ChaseEscape, 1);
    for s=1:obj.nSubjects
        Profile.Individual(end).Val(s, 1) = p(s);
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% approach
Profile.Individual(end+1).Name = 'Number of approaches';
try
    c = zeros(obj.nSubjects);
    for l=1:length(obj.Hierarchy.Contacts.List)
        curr = obj.Hierarchy.Contacts.List(l);
        s1 = curr.subjects(1);
        s2 = curr.subjects(2);
        c(s1, s2) = c(s1, s2) + curr.states(1, 2);
        c(s2, s1) = c(s2, s1) + curr.states(2, 2);
    end
    p = sum(c, 2);
    for s=1:obj.nSubjects
        Profile.Individual(end).Val(s, 1) = p(s);
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% being approach
Profile.Individual(end+1).Name = 'Number of being approached';
try
    c = zeros(obj.nSubjects);
    for l=1:length(obj.Hierarchy.Contacts.List)
        curr = obj.Hierarchy.Contacts.List(l);
        s1 = curr.subjects(1);
        s2 = curr.subjects(2);
        c(s1, s2) = c(s1, s2) + curr.states(1, 2);
        c(s2, s1) = c(s2, s1) + curr.states(2, 2);
    end
    p = sum(c, 1)';
    for s=1:obj.nSubjects
        Profile.Individual(end).Val(s, 1) = p(s);
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% entropy
Profile.Individual(end+1).Name = 'Entropy';
try
    p = zeros(obj.nSubjects, obj.ROI.nZones);
    for s=1:obj.nSubjects
        p(s, :) = p(s, :) + histc(obj.Tracking.zones(s, obj.Tracking.valid), 1:obj.ROI.nZones);
    end
    for s=1:obj.nSubjects
        h = p(s, :) / sum(p(s, :));
        Profile.Individual(end).Val(s, 1) = -h * log2(h' + (h' == 0));
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% % time near food or water
Profile.Individual(end+1).Name = '% of time near food or water';
try
    for s=1:obj.nSubjects
        z = obj.Tracking.zones(s, obj.Tracking.valid);
        near = sum(z == 2 | z == 3 | z == 4);
        total = sum(obj.Tracking.valid);
        Profile.Individual(end).Val(s, 1) = near / total;
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% % time high
Profile.Individual(end+1).Name = '% of time at high place';
try
    for s=1:obj.nSubjects
        z = obj.Tracking.zones(s, obj.Tracking.valid);
        high = sum(z == 5 | z == 8 | z == 10);
        total = sum(obj.Tracking.valid);
        
        Profile.Individual(end).Val(s, 1) = high / total;
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% % time alone outside
Profile.Individual(end+1).Name = '% time alone outside';

try
    for s=1:obj.nSubjects
        other = 1:obj.nSubjects;
        other = other(other ~= s);
        p = sum(all(obj.Tracking.sheltered(other, obj.Tracking.valid)) & ~obj.Tracking.sheltered(s, obj.Tracking.valid));
        total = sum(obj.Tracking.valid);
        
        Profile.Individual(end).Val(s, 1) = p / total;
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% median speed
Profile.Individual(end+1).Name = 'median speed (rough) [m/sec]';
try
    PixelsPerCM = mean([obj.Meta.Scale.ArenaCoord(3) / obj.Meta.Scale.ArenaWidth, obj.Meta.Scale.ArenaCoord(4) / obj.Meta.Scale.ArenaHeight]);
    jumpTime = 1; %% sec
    for s=1:obj.nSubjects
        x = Segs(~obj.Tracking.sheltered(s, :) & obj.Tracking.valid, obj.Tracking.x(s, :));
        y = Segs(~obj.Tracking.sheltered(s, :) & obj.Tracking.valid, obj.Tracking.y(s, :));
        dt = obj.Video.FrameRate * jumpTime;
        speed = nan(1, x.Length);
        for i=1:length(x.Events)
            cx = x.Events(i).data;
            cx = cx(1:dt:end);
            cy = y.Events(i).data;
            cy = cy(1:dt:end);
            if length(cx) > 2
                d = sqrt((cx(2:end)-cx(1:end-1)).^2 + (cy(2:end)-cy(1:end-1)).^2) / (PixelsPerCM * 100) / jumpTime;
                speed(x.Events(i).beg:x.Events(i).beg+length(d)-1) = d;
            end
        end
        Profile.Individual(end).Val(s, 1) =  nanmedian(speed);
    end    
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% Distance
Profile.Individual(end+1).Name = 'distance (rough) [m]';
try
    PixelsPerCM = mean([obj.Meta.Scale.ArenaCoord(3) / obj.Meta.Scale.ArenaWidth, obj.Meta.Scale.ArenaCoord(4) / obj.Meta.Scale.ArenaHeight]);
    jumpTime = 3; %% sec
    
    jumpFrames = jumpTime * obj.Video.FrameRate;
    for s=1:obj.nSubjects
        xcm = obj.Tracking.x(s, obj.Tracking.valid) / PixelsPerCM;
        ycm = obj.Tracking.y(s, obj.Tracking.valid) / PixelsPerCM;
        d = sqrt(...
            (xcm(1:end-jumpFrames+1) - xcm(jumpFrames:end)).^2 + ...
            (ycm(1:end-jumpFrames+1) - ycm(jumpFrames:end)).^2);
        Profile.Individual(end).Val(s, 1) = sum(d) / 100;
    end
catch me
    cprintf('Errors', ['# failed for ''' Profile.Individual(end).Name ''' - %s\n'], ParseError(me));
    Profile.Individual(end).Val = nan(obj.nSubjects, 1);
end

%% Video duration
Profile.Group(1).Name = sprintf('Video duration [hours]');
try
    Profile.Group(end).Val = sum(obj.Tracking.valid) / obj.Video.FrameRate / 60/ 60;
catch me
    cprintf('Errors', ['# failed for ''' Profile.Group(end).Name ''' - %s\n'], ParseError(me));
    Profile.Group(end).Val = nan;
end

%% Number of hierarchy levels
Profile.Group(end+1).Name = sprintf('Number of hierarchy levels (1 day)');
try
    Profile.Group(end).Val = length(unique(obj.Hierarchy.AggressiveChase.rank));
catch me
    cprintf('Errors', ['# failed for ''' Profile.Group(end).Name ''' - %s\n'], ParseError(me));
    Profile.Group(end).Val = nan;
end

%% multi-info
Profile.Group(end+1).Name = 'Multi-information';
try
    Profile.Group(end).Val = obj.Analysis.Potts.In;
catch me
    cprintf('Errors', ['# failed for ''' Profile.Group(end).Name ''' - %s\n'], ParseError(me));
    Profile.Group(end).Val = nan;
end

%% explained information
Profile.Group(end+1).Name = '% Explained pairwise';
try
    Profile.Group(end).Val = obj.Analysis.Potts.Ik(1) / obj.Analysis.Potts.In;
catch me
    cprintf('Errors', ['# failed for ''' Profile.Group(end).Name ''' - %s\n'], ParseError(me));
    Profile.Group(end).Val = nan;
end

Profile.Group(end+1).Name = '% Explained triplets';
try
    Profile.Group(end).Val = obj.Analysis.Potts.Ik(2) / obj.Analysis.Potts.In;
catch me
    cprintf('Errors', ['# failed for ''' Profile.Group(end).Name ''' - %s\n'], ParseError(me));
    Profile.Group(end).Val = nan;
end

%% Aggressive chase-escape
Profile.Pairwise(1).Name = 'Aggressive chase-escape';
try
    Profile.Pairwise(end).Val = obj.Hierarchy.AggressiveChase.ChaseEscape;
catch me
    cprintf('Errors', ['# failed for ''' Profile.Pairwise(end).Name ''' - %s\n'], ParseError(me));
    Profile.Pairwise(end).Val = nan(obj.nSubjects, obj.nSubjects);
end

%% Chase-escape
Profile.Pairwise(1).Name = 'Chase-escape';
try
    Profile.Pairwise(end).Val = obj.Hierarchy.AggressiveChase.ChaseEscape;
catch me
    cprintf('Errors', ['# failed for ''' Profile.Pairwise(end).Name ''' - %s\n'], ParseError(me));
    Profile.Pairwise(end).Val = nan(obj.nSubjects, obj.nSubjects);
end

%% Contacts
Profile.Pairwise(end + 1).Name = 'Contacts';
try
    c = zeros(obj.nSubjects);
    for l=1:length(obj.Hierarchy.Contacts.List)
        curr = obj.Hierarchy.Contacts.List(l);
        s1 = curr.subjects(1);
        s2 = curr.subjects(2);
        c(s1, s2) = c(s1, s2) + curr.states(1, 2);
        c(s2, s1) = c(s2, s1) + curr.states(2, 2);
    end
    Profile.Pairwise(end).Val = c;
catch me
    cprintf('Errors', ['# failed for ''' Profile.Pairwise(end).Name ''' - %s\n'], ParseError(me));
    Profile.Pairwise(end).Val = nan(obj.nSubjects, obj.nSubjects);
end

%% Approaches
Profile.Pairwise(end + 1).Name = 'Approaches';
try
    c = zeros(obj.nSubjects);
    for l=1:length(obj.Hierarchy.Contacts.List)
        curr = obj.Hierarchy.Contacts.List(l);
        s1 = curr.subjects(1);
        s2 = curr.subjects(2);
        c(s1, s2) = c(s1, s2) + curr.states(1, 2);
        c(s2, s1) = c(s2, s1) + curr.states(2, 2);
    end
    Profile.Pairwise(end).Val = c;
catch me
    cprintf('Errors', ['# failed for ''' Profile.Pairwise(end).Name ''' - %s\n'], ParseError(me));
    Profile.Pairwise(end).Val = nan(obj.nSubjects, obj.nSubjects);
end

%% Events: Chase-escape 
Profile.Events(1).Name = 'Chase-escape [chaser escaper start_frame end_frame]';
try
    %%
    map = obj.Hierarchy.Contacts.Behaviors.AggressiveChase.Map;
    b = min([obj.Hierarchy.Contacts.List(map).beg]);
    e = max([obj.Hierarchy.Contacts.List(map).end]);
    s = [obj.Hierarchy.Contacts.List(map).subjects];
    aggr = cat(3, obj.Hierarchy.Contacts.List(map).states);
    
    id = squeeze([aggr(:, 5, :)]);
    [~, ids] = max(id);
    chaser = s(sub2ind(size(s), ids, 1:size(s, 2)));
    escaper = s(sub2ind(size(s), 3-ids, 1:size(s, 2)));
    
    Profile.Events(end).Val = [chaser(:) escaper(:) b(:) e(:)];
catch me
    cprintf('Errors', ['# failed for ''' Profile.Pairwise(end).Name ''' - %s\n'], ParseError(me));
    Profile.Events(end).Val = nan;
end

%%
obj.Profile = Profile;
