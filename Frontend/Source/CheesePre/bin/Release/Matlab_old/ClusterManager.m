classdef ClusterManager < handle
    properties
        User = 'forkosh';
        Queue = 'schneidmann';
    end
    properties (Constant = true)
        MaxNumOfProcesses = 100;
    end
    
    properties (Dependent = true, SetAccess = private)
        nRunning
    end
    methods
        function obj = ClusterManager
            
        end
        
        function value = get.nRunning(obj)
            [stat, output] = system(['bjobs -q ' obj.Queue ' -u ' obj.User ' | grep ' obj.Queue ' | grep ' obj.User ' | wc -l']);
            %output = '0';
            value = str2double(output);
        end
        
        function obj = WaitForVacancy(obj)
            count = 1;
            b = '\b\b\b\b\b';
            while obj.nRunning >= obj.MaxNumOfProcesses
                if count == 1
                    fprintf('# waiting for previous processes to finish      ');
                end
                switch mod(count-1, 4)
                    case 0
                        fprintf([b '[   ]'])
                    case 1
                        fprintf([b '[#  ]'])
                    case 2
                        fprintf([b '[## ]'])
                    case 3
                        fprintf([b '[###]'])
                end
                count = count + 1;
                pause(2);
            end
            if count > 1
                fprintf([b '[done]\n']);
            end
        end
        
        function value = GetJobStatus(obj, jobid)
            [stat, output] = system(['bjobs ' num2str(jobid) ' | grep ' num2str(jobid) ' | gawk ''$0=$3'' ']);
            switch output
                case 'RUN'
                    value = 'running';
                case 'PEND' 
                    value = 'waiting';
                case 'DONE' 
                    value = 'done';
                otherwise
                    value = 'unavail';
            end
        end
    end % methods
end