classdef Colors
    methods (Static)
        function c = Lighten(c, beta)
            hsv = rgb2hsv(reshape(c, [1 1 3]));
            hsv(2) = hsv(2) * beta;
            c = reshape(hsv2rgb(hsv), [3 1 1]);
        end
        
        function c = Brighten(c, beta)
            if nargin < 2
                beta = .5; 
            end
            c = brighten(c, beta); 
        end
        
        function c = Black;       c = Colors.ParseHex('000000'); end
        function c = White;       c = Colors.ParseHex('FFFFFF'); end
        function c = Red;         c = Colors.ParseHex('FF0000'); end
        function c = Yellow;      c = Colors.ParseHex('FFFF00'); end
        function c = Blue;        c = Colors.ParseHex('0000FF'); end
        function c = PrettyRed;   c = [220 073 089] / 255; end
        function c = PrettyGreen; c = [053 178 087] / 255; end
        function c = PrettyBlue;  c = [000 177 229] / 255; end
        function c = DarkGray;    c = [076 076 076] / 255; end
        function c = LightGray;   c = [178 178 178] / 255; end
        
        function c = DutchTeal;   c = Colors.ParseHex('1693A5'); end
        function c = HotPink;     c = Colors.ParseHex('FF0066'); end
        function c = Serenity;    c = Colors.ParseHex('ADD8C7'); end
        function c = Gold;        c = Colors.ParseHex('FBB829'); end
        function c = HauntedMilk; c = Colors.ParseHex('CDD7B6'); end
        function c = Slate;       c = Colors.ParseHex('556270'); end
        function c = Frogs;       c = Colors.ParseHex('C3FF68'); end
        function c = Vanilla;     c = Colors.ParseHex('FCFBE3'); end
        function c = Bloons;      c = Colors.ParseHex('D31996'); end
        function c = VitaminC;    c = Colors.ParseHex('FF9900'); end

        function Show(color)
            Colormaps.Show(color);
        end
        
        function rgb = TSL2RGB(tsl)
            % TSL = tint,saturation,level
            x = -cot(2*pi*tsl(:, 1));
            g = -sqrt(5./(9*(x.^2+1))).*tsl(:, 2).*(tsl(:, 1) > .5) + ...
                sqrt(5./(9*(x.^2+1))).*tsl(:, 2).*(tsl(:, 1) < .5);
            r = sqrt(5)/3*tsl(:,2).*(tsl(:, 1) == 0) + (x .* g + 1/3).*(tsl(:, 1) ~= 0);
            k = tsl(:, 3) ./ (.185*r + .473*g + .114);
            rgb(:, 1) = k.*r;
            rgb(:, 2) = k.*g;
            rgb(:, 3) = k.*(1 - r - g);
        end
        
        function tsl = RGB2TSL(c)
            % TSL = tint,saturation,level
            r = bsxfun(@rdivide, c(:, 1), sum(c, 2)) - 1/3;
            g = bsxfun(@rdivide, c(:, 2), sum(c, 2)) - 1/3;
            t = 1/(2*pi)*atan(r./g + .25) .* (g > 0) + ...
                1/(2*pi)*atan(r./g + .75) .* (g < 0);
            s = sqrt(9/5*(r.^2+g.^2));
            l = .299*c(:, 1)+.587*c(:, 2)+.114*c(:, 3);
            tsl = [t,s,l];
        end
        
        function t = GetTint(c)
            r = bsxfun(@rdivide, c(:, 1), sum(c, 2)) - 1/3;
            g = bsxfun(@rdivide, c(:, 2), sum(c, 2)) - 1/3;
            t = 1/(2*pi)*atan(r./g + (g > 0).*.25 + (g<1).*.75);
        end

        function t = SetTint(c, t)
        end
        
        function rgb = RGB2HSV(c)
            if size(c, 3) == 3
                rgb = rgb2hsv(c);
            else
                rgb = reshape(rgb2hsv(reshape(c, [size(c,1) 1 3])), [size(c,1) 3]);
            end
        end

        function hsv = HSV2RGB(c)
            if size(c, 3) == 3
                hsv = hsv2rgb(c);
            else
                hsv = reshape(hsv2rgb(reshape(c, [size(c,1) 1 3])), [size(c,1) 3]);
            end
        end
        
        function color = ParseHex(h)
            color = [hex2dec(h(1:2)) hex2dec(h(3:4)) hex2dec(h(5:6))] / 255;
        end
    end
end