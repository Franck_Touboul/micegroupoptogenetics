function res = myPerms(dim, max_value)
n = max_value ^ dim;
res = zeros(n, dim);
for i=1:n
    res(i, :) = decimal2base(i-1, max_value, dim);
end
res = res + 1;
