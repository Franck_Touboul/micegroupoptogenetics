function obj = CheeseTrackRun(obj)

uname = Reporter.GetUname();
opt.recover = false;
%%
addpath('../Basics');

%%
Reporter.Progress(uname, 0, 8);
obj = CheeseSquare(obj);

%%
opt.nSegs = 100;
Reporter.Progress(uname, 1, 8);

track = CheeseObject.Create(obj.Video.Filename);
track.nSubjects = obj.nSubjects;
track.Colors = obj.Colors;
track = Q.cpfield(obj.Meta, track, 'Scale');

%%
if isfield(track, 'Scale') && ~isempty(track.Scale)
    wscale = (track.Scale.ArenaCoord(3) / track.Scale.ArenaWidth) / 9;
    hscale = (track.Scale.ArenaCoord(4) / track.Scale.ArenaHeight) / 9;
else
    wscale = 1;
    hscale = 1;
end
track.VideoScale = track.VideoScale / min(wscale, hscale);

%%

track.Colors.Centers = zeros(track.nSubjects, 3);
for i=1:track.nSubjects
    track.Colors.Centers(i, :) = min(mean(obj.Colors.Marks.color(obj.Colors.Marks.id==i, :)/255), 1);
end

%%
track.BkgImage = obj.Background.im;
track.ROI = obj.ROI;
track.time = 1/obj.Video.FrameRate * (1:obj.Video.NumberOfFrames);
try
    if ~isempty(obj.Tracking)
        f = fields(obj.Tracking);
        for i=1:length(f)
            track.(f{i}) = obj.Tracking.(f{i});
        end
        track.time = 1/obj.Video.FrameRate * (1:length(track.x));
    end
catch err
    fprintf('%s', Console.StrudelError(err));
end

%%
Reporter.Progress(uname, 2, 8);
ns = opt.nSegs;
Console.Message('segmenting video frames (using color information)');
Console.Reprintf(0, '');
h = tic;
segmfile = [track.OutputPath track.FilePrefix '.segm.%03d.mat'];
Console.Timer(tic);
Console.Counter(nan);
parfor i=1:ns
    if opt.recover && exist(sprintf(segmfile, i), 'file') %#ok<PFBNS>
        Console.Message(1, 'skipping segment no. %d', i);
    else
        CheeseColorSegment(track, ns, i);
    end
end
Console.Message(1, 'segmentation took %.1f seconds', toc(h));
toc(h)

%%
Reporter.Progress(uname, 3, 8);
try
    track = CheeseTrackPath(track, opt.nSegs);
    
    trackfields = {'x',      'y',      'zones',  'hidden',  'sheltered', 'valid'};
    trackclass  = {'single', 'single', 'uint16', 'logical', 'logical',   'logical'};
    for i=1:length(trackfields)
        if isfield(track, trackfields{i})
            obj.Tracking.(trackfields{i}) = cast(track.(trackfields{i}), trackclass{i});
        end
    end
    obj.Save;
catch err
    fprintf('\n%s', Console.StrudelError(err));
end

try
    TrackExport(track);
catch err
    fprintf('%s', Console.StrudelError(err));
end

% %%
% Reporter.Progress(uname, 4, 8);
% try
%     track = CheeseHierarchy(track);
%     obj.Hierarchy = track.Hierarchy;
%     obj.Hierarchy.Contacts = track.Contacts;
%     obj.Save;
% catch err
%     fprintf('%s', Console.StrudelError(err));
% end

%%
Reporter.Progress(uname, 5, 8);
try
    obj = CheeseHierarchyGroup(obj);
catch err
    fprintf('%s', Console.StrudelError(err));
end

%%
Reporter.Progress(uname, 6, 8);
try
    obj = CheesePotts(obj, min(obj.Tracking.zones, 10));
    obj.Save;
catch err
    fprintf('%s', Console.StrudelError(err));
end

%%
Reporter.Progress(uname, 7, 8);
try
    obj = CheeseProfiler(obj);
    obj.Save;
catch err
    fprintf('%s', Console.StrudelError(err));
end
%%
Reporter.Progress(uname, 8, 8);

%%
% profile = track.Profile;
% excelfile = [obj.Video.Filename, '.xlsx']
%
% var = cell(1, length(curr));
% varname = cell(1, length(curr));
% for i=1:length(curr)
%     var{i} = curr(i).Val;
%     varname{i} = name;
% end
% xlswrite(excelfile, varname);


