function name = TrackName(obj, data)

days = sprintf('%02d', data.Day);
name = sprintf('%s.exp%04d.day%s.cam%02d', data.Type, data.GroupID, days, data.CameraID);
