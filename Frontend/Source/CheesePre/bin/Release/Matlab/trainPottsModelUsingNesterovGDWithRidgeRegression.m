function me = trainPottsModelUsingNesterovGDWithRidgeRegression(options, data, confidence)
options.param_report = false;
options = setDefaultParameters(options, ...
    'Statistics', false, ...
    'KLConvergenceRatio', 1e-4, ...
    'tk', 1, 'nIters', 20000, 'MinOccurance', 0);

me.tk = options.tk;
me.alpha = .5;
me.beta = .5;
me.RidgeBeta = 1;
useLineSearch = true;
me.LineSearchMaxSteps = 300;
me.KLConvergenceRatio = options.KLConvergenceRatio;

options.NeutralZone = 1; % Open
%%
use_sparse = true;
if nargin < 3
    confidence = -.05;
end
me.confidence = confidence;
me.neutralZone = options.NeutralZone;

if isfield(options, 'Output')
    output = options.Output;
else
    options.Output = true;
    output = true;
end
%
if output; fprintf('# Training Potts Model\n'); end
%
if output; fprintf('# - finding all permutation of max dimension %d...\n', options.n); end;
%%
allPerms = nchoose(1:options.nSubjects);
nPerms = {};
j = 1;
for i=1:length(allPerms)
    if length(allPerms{i}) <= options.n 
        nPerms{j} = allPerms{i};
        j = j + 1;
    end
end

%%
if output; fprintf('# - computing constraints...\n'); end
[me.constraints, pci] = PottsComputeConstraints(data' + 1, options.count, nPerms, abs(confidence));
C = length(nPerms);
%
me.inputPerms = myPerms(options.nSubjects, options.count);
[me.perms, me.labels] = assignFeature(me.inputPerms, options.count, nPerms, use_sparse);

%% remove small occurences
if options.MinOccurance > 0 && options.MinOccurance < inf
    occ = me.constraints * size(data, 2);
    valid = ~(occ > 0 & occ < options.MinOccurance);
    me.labels = me.labels(valid);
    me.perms = me.perms(:, valid);
    me.constraints = me.constraints(valid);
    pci = pci(valid, :);
end

%%
% if options.count == 2
%     for i=1:length(me.labels)
%         if all(me.labels{i}(2, :) ~= 2)
%             valid(i) = true;
%         end
%     end
%     me.labels = me.labels(valid);
%     me.perms = me.perms(:, valid);
%     me.constraints = me.constraints(valid);
%     pci = pci(valid);
% end
%
me.nSubjects = options.nSubjects;
me.range = options.count;
me.momentum = randn(1, size(me.labels, 2)) / size(me.labels, 2);
orig = me;
%% filter neutral zone
neutral = false(1, length(orig.labels));
for l=1:length(me.labels)
    neutral(l) = any(orig.labels{l}(2, :) == options.NeutralZone);
end
me.constraints = orig.constraints(~neutral);
me.perms = orig.perms(:, ~neutral);
me.labels = orig.labels(~neutral);
% initialize the weights
if isfield(options, 'initialPottsWeights') && ~isempty(options.initialPottsWeights)
    me.momentum = options.initialPottsWeights;
else
    me.momentum = orig.momentum(~neutral);
end
%
me.weights = me.momentum;
pci = pci(~neutral, :);

%% compute empirical probabilities
base = me.range;
baseVector = base.^(size(data, 1)-1:-1:0);
pEmp = histc(baseVector * data+1, 1:base ^ size(data, 1));
pEmp = pEmp / sum(pEmp);
%% initial halfKL
p = exp(me.perms * me.weights');
Z = sum(p);
p = p / Z;
betaFactor = sqrt(me.constraints - me.constraints.^2) / sqrt(size(data, 1));
kl2 = -pEmp * log2(p) + me.RidgeBeta * sum(betaFactor .* me.momentum.^2);
kl1 = pEmp * log2(pEmp' + (pEmp' == 0));
%%
if options.Statistics; me.Statistics = struct(); end
tic;
me.converged = false;
me.nconverged = inf;
me.KLconverged = false;
best = me;
bestKL = kl1 + kl2;
ticTime = 0;
prevt = me.tk;

p = [];
for iter=1:options.nIters
    currTime = toc;
    if output
        fprintf('# - Ridge-Nestrov GD, order %d, iter (%4d/%4d) [x%d ps] : ', options.n, iter, options.nIters, round(1 / (currTime - ticTime)));
    end
    ticTime = currTime;
    % compute the (un-normalized) pdf for each sample
    if isempty(p)
        p = exp(me.perms * me.weights');
        % normalize the pdf (the Z)
        Z = sum(p);
        p = p / Z;
    end
    prev_kl = kl1 + kl2;
    kl2 = -pEmp * log2(p + (pEmp' == 0)) + me.RidgeBeta * sum(betaFactor .* me.momentum.^2);
    if output; fprintf('KL = %6.4f [TF: %6.4f] (%.3f)', kl1 -pEmp * log2(p + (pEmp' == 0)), kl1 + kl2, abs((kl1 + kl2 - prev_kl)/prev_kl)); end
    me.Dkl = kl1 + kl2;
    E = p' * me.perms;
    p = [];
    %%
    if me.Dkl < bestKL
        bestKL = me.Dkl;
        best = me;
    end
    %%
    if me.KLConvergenceRatio > 0
        if abs((kl1 + kl2) - prev_kl) <= me.KLConvergenceRatio
            if me.KLconverged
                if output;
                    fprintf('\n# converged to KL convergence ratio (ratio = %3.2f)\n', me.KLConvergenceRatio);
                end
                break;
            else
                me.KLconverged = true;
            end
        elseif me.KLconverged
            me.KLconverged = false;
        end
    end

    %% # - Ridge-Nestrov GD, order 1, iter (4672/50000) [x33 ps] : KL = 1.3335 [TF: 141.3502] (0.000), alpha = 1.00

    t = min(prevt / me.beta, me.tk);
    pWeights = me.momentum;
    if useLineSearch
        df = (me.constraints - E) - 2 * me.RidgeBeta * (betaFactor .* me.momentum);
        found = false;
        nsteps = 1;
        while ~found
            lsMomentum = me.weights + t * df;
            %lsWeights = lsMomentum + (iter - 1)/(iter+2) * (lsMomentum - pWeights);
            p = exp(me.perms * lsMomentum');
            p = p / sum(p);
            found = -pEmp * log2(p + (pEmp' == 0))  + me.RidgeBeta * sum(betaFactor .* lsMomentum.^2) < kl2; % - me.alpha * t * sum(df.^2);
            if ~found
                t = me.beta * t;
            end
            if nsteps >= me.LineSearchMaxSteps
                break;
            end
            nsteps = nsteps + 1;
        end
    end
    if nsteps >= me.LineSearchMaxSteps
        break;
    end
    prevt = t;
    
    %fprintf('# line-search factor = 2^(%g)\n', log2(t));
    if output; fprintf(', alpha = %.2f', t); end;
    
    me.momentum = me.weights + t * df;
    me.weights = me.momentum + (iter - 1)/(iter+2) * (me.momentum - pWeights);

    
    %%
    if options.Statistics;
        me.Statistics(iter).times = toc; 
        me.Statistics(iter).KL = kl1 + kl2;
    end
    %%
    if output; fprintf('\n'); end
end
me = best;
me.nIters = iter;
me.Dkl = kl1 + kl2;
% if me.nconverged < best.nconverged || (me.nconverged == best.nconverged && me.Dkl > best.Dkl)
%     me = best;
% end

%me = rmfield(me, 'features');
