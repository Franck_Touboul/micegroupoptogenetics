function data = MyTrim(data, range)
data(data < range(1)) = range(1);
data(data > range(2)) = range(2);