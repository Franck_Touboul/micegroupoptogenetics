function obj=TrackFindDarkPeriod(obj, thresh)
fprintf('# finding dark period\n');
obj.StartTime = BinarySearch(obj, thresh, 1) * obj.dt;
obj.EndTime = BinarySearch(obj, thresh, -1) * obj.dt;
fprintf(['# - from ' sec2time_v2(obj.StartTime) ' to ' sec2time_v2(obj.EndTime) '\n']);

function frame = BinarySearch(obj, thresh, dir)
m1 = 1;
m2 = obj.nFrames;
minute = 60 / obj.dt;

if dir > 0
    valid = m2;
else
    valid = m1;
end
while (m2 >= m1)
    f = round(m1 + (m2 - m1) / 2);
    orig = im2double(myMMReader(obj.VideoFile, f));
    lum = max(orig,[],3);
    lum = mean(lum(:));
    if dir * (lum - thresh) > 0
        m1 = f + 1;
    else
        m2 = f - 1;
    end
    if lum <= thresh
        valid = f;
    end
    if obj.Output
        clf;
        imagesc(orig);
        if lum <= thresh
            title(['frame ' num2str(f) ' - Dark']);
        else
            title(['frame ' num2str(f) ' - Light']);
        end
        drawnow;
    end
end
if lum <= thresh
    frame = f;
else
    frame = valid;
end
