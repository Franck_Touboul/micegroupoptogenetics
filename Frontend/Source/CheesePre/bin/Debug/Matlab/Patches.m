classdef Patches
    methods (Static)
        function ds = DefaultStyle
            ds = {'EdgeColor', 'none'};
        end
        
        function Polygon(x,y,c,varargin)
            if ~ishold
                cla reset
            end
            ds = Patches.DefaultStyle;
            patch(x,y,c,ds{:}, varargin{:});
        end
        
        function Circle(x,y,r,c,varargin)
            if ~ishold
                cla reset
            end
            N = 100;
            ds = Patches.DefaultStyle;
            d = linspace(0, 2*pi, N);
            X = x + r * cos(d);
            Y = y + r * sin(d);
            patch(X, Y, c, ds{:}, varargin{:});
        end
        
        function Rect(x,y,w,h,c,varargin)
            if ~ishold
                cla reset
            end
            ds = Patches.DefaultStyle;
            patch([x x+w x+w x], [y y y+h y+h], c, ds{:}, varargin{:});
        end
        
        function Line(x,y,width,c,varargin)
            if ~ishold
                cla reset
            end
            %%
            a = atan2(y(2)-y(1), x(2)-x(1));
            if isnumeric(varargin{1})
                shift = varargin{1};
                varargin = varargin(2:end);
                x = x + shift * sin(a);
                y = y - shift * cos(a);
            end
            %%
            dx = width * sin(a);
            dy = width * cos(a);
            %ds = Patches.DefaultStyle;
            ds = {'EdgeColor', c};
            patch([x(1)+dx  x(2)+dx x(2)-dx x(1)-dx], [y(1)-dy, y(2)-dy, y(2)+dy, y(1)+dy], c, ds{:}, varargin{:});
        end
        
    end
end