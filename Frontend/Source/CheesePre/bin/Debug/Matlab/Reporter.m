classdef Reporter
    methods (Static = true)
        function uname = GetUname()
            s = dbstack;
            if length(s)>1
                uname = [s(2).name '.'];
            else
                uname = '';
            end
            uname = [uname, Q.randstr(30)];
        end
        
        function status = Progress(uname, n, total)
            persistent sources;
            if nargin == 1 && isempty(uname)
                sources = struct('uname', {}, 'curr', {}, 'total', {},  'depth', {});
                return;
            end
            
            if isempty(sources)
                sources = struct('uname', {}, 'curr', {}, 'total', {},  'depth', {});
            end
            
            idx = strcmp({sources.uname}, uname);
            if any(idx)
                if n == total
                    sources(idx) = [];
                else
                    sources(idx).curr = n;
                    sources(idx).total = total;
                end
            else
                if n == total
                else
                    rank = length(dbstack) - 1;
                    if isempty(rank)
                        rank = 0;
                    end
                    sources(end+1) = struct('uname', uname, 'curr', n, 'total', total, 'depth', rank);
                end
            end
            %%
            status.nRunning = length(sources);
            [u, ~, rank] = unique([sources.depth]);
            curr = [sources.curr];
            total = [sources.total];
            offset = sum(curr(rank == 1)) / sum(total(rank == 1));
            for i=2:length(u)
                offset = offset + sum(curr(rank == 1)) / sum(total(rank == 1)) / 10^(i-1);
            end
            status.TotalProgress = offset;
            Q.struct2xml('status', 'status.log');
        end
    end
end
