function data = ReadStrudels(fname)
%%
%fname = 'log/SocialPredictionApprox.o1195403';
try
if regexp(fname, '[\*\?]')
    list = dir(fname);
    path = regexprep(fname, '[^\/]*$', '');
    data = [];
    prev = 0;
    for i=1:length(list)
        prev = RePrintf(prev, '# reading file %d/%d', i, length(list));
        curr = ReadStrudels([path list(i).name]);
        if ~isempty(fieldnames(curr))
            if isempty(data)
                data = curr;
            else
                try
                    data(end+1) = curr;
                catch
                end
            end
        end
    end
    fprintf('\n');
    return;
end
fast = true;
if fast
    hname = [fname '.' hash(randi(1000000), 'MD5') '.temp'];
end
catch
    disp 'error'
end
try
    data = struct();
    if fast
        system(['tail -100 ./' fname ' | grep @ > ' hname]);
        fname = hname;
    end
    fid = fopen(fname);
    tline = fgetl(fid);
    while ischar(tline)
        if strfind(tline, '@')
            tline = regexprep(tline, '%.*', '');
            n = regexp(tline, '@(?<var>[\w]+)(?<type>\[.*\])?[ ]*=[ ]*(?<val>.*)', 'names');
            if ~isempty(n)
                if isempty(n.type) % for back compitability
                    if regexp(n.val, '^[ \.\deE\+\-]*$')
                        num = str2double(n.val);
                        if isnan(num)
                            matstr = regexp(n.val, '([\.\deE\+\-]*)', 'match');
                            num = [];
                            for i=1:length(matstr)
                                num(i) = str2double(matstr{i});
                            end
                        end
                        data.(n.var) = num(:);
                    else
                        data.(n.var) = n.val;
                    end
                else
                    meta = regexp(n.type, '\[(?<class>\w+)\((?<dim>.*)\)\]', 'names');
                    switch meta.class
                        case {'double'}
                            matstr = regexp(n.val, '([\.\deE\+\-]*)', 'match');
                            num = [];
                            for i=1:length(matstr)
                                num(i) = str2double(matstr{i});
                            end
                            data.(n.var) = num(:);
                        case {'char'}
                            data.(n.var) = n.val;
                    end
                end
            end
        end
        tline = fgetl(fid);
    end
    fclose(fid);
catch me
    warning(me.getReport);
end
try
    if fast
        system(['rm ' hname]);
    end
end
%%
% if regexp(fname, '[\*\?]')
%     list = dir(fname);
%     path = regexprep(fname, '\/[^\/]*$', '\/');
% else
%     list.name = fname;
%     path = '';
% end
% idx = 1;
% data = struct();
% for i=1:length(list)
%     cmd = ['gawk -f ReadStrudels.awk -vdata="D"'];
%     try
%         [status, res] = system([cmd ' ' path list(i).name]);
%         if ~isempty(res)
%             eval(res);
%             if idx == 1
%                 data = D;
%             else
%                 data(idx) = D;
%             end
%             idx = idx + 1;
%         end
%     end
% end