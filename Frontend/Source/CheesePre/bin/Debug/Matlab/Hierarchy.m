classdef Hierarchy
    methods (Static = true)
        function [NormDSonDij, NormDSonPij] = DavidScore(varargin)
            if ismatrix(varargin{1})
                Sij = varargin{1};
                nij = Sij + Sij';
            else
                nij = obj.Hierarchy.AggressiveChase.ChaseEscape + obj.Hierarchy.AggressiveChase.ChaseEscape';
                Sij = obj.Hierarchy.AggressiveChase.ChaseEscape;
            end
            
            aux = @(Pij) {sum(Pij, 2), Pij * sum(Pij, 2), sum(Pij, 1)', Pij' * sum(Pij, 1)'};
            
            Pij = Sij./nij;
            Pij(nij == 0) = 0;
            WL = aux(Pij);
            [w, w2, l, l2] = WL{:};
            DS = w + w2 - l - l2;
            NormDSonPij = (DS + size(Sij,1) * (size(Sij,1) - 1)/2) / size(Sij,1);
            
            Dij = (Sij + .5) ./ (nij + 1);
            Dij(nij == 0) = 0;
            WL = aux(Dij);
            [w, w2, l, l2] = WL{:};
            DS = w + w2 - l - l2;
            NormDSonDij = (DS + size(Sij,1) * (size(Sij,1) - 1)/2) / size(Sij,1);
        end

        function [rank, removed]  = Rank(obj, ce)
            if nargin < 2
                ce = obj.Hierarchy.AggressiveChase.ChaseEscape;
            end
            mat = ce - ce';
            mat(binotest(ce, ce + ce')) = 0;
            mat = mat .* (mat > 0);
            
            [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
        end
        
        function c = Approaches(obj)
            c = zeros(obj.nSubjects);
            for l=1:length(obj.Hierarchy.Contacts.List)
                curr = obj.Hierarchy.Contacts.List(l);
                s1 = curr.subjects(1);
                s2 = curr.subjects(2);
                c(s1, s2) = c(s1, s2) + curr.states(1, 2);
                c(s2, s1) = c(s2, s1) + curr.states(2, 2);
            end
        end
        
        function Elo = EloRating(objs)
            % Based upon :
            %    Christof Neumann, Julie Duboscq, Constance Dubuc, Andri Ginting,
            %    Ade Maulana Irwan, Muhammad Agil, Anja Widdig, Antje Engelhardt,
            %    Assessing dominance hierarchies: validation and advantages of
            %    progressive evaluation with Elo-rating, Animal Behaviour,
            %    Volume 82, Issue 4, October 2011, Pages 911-921, ISSN 0003-3472,
            %    10.1016/j.anbehav.2011.07.016.
            
            if ~isa(objs, 'cell')
                temp = objs;
                objs = {};
                objs{1} = temp;
            end
            obj = objs{1};
            
            Elo.WindowSize = 4; % hours
            Elo.k = 100;
            Elo.Scores = ones(obj.nSubjects, 1) * 1000;
            Elo.Times = find(obj.Tracking.valid, 1);
            Elo.Day = 1;
            Elo.Map = false(obj.nSubjects, 1);
            starttime = 0;
            
            %%
            idx = 1;
            for oid=1:length(objs)
                obj = objs{oid};
                map = obj.Hierarchy.Contacts.Behaviors.AggressiveChase.Map;
                Elo.Scores = [Elo.Scores, zeros(obj.nSubjects, sum(map))];
                Elo.Times = [Elo.Times, zeros(1, sum(map))];
                Elo.Day = [Elo.Day, zeros(1, sum(map))];
                Elo.Map = [Elo.Map, false(obj.nSubjects, sum(map))];
                for i=find(map)
                    %   obj.Contacts.List.beg
                    %    obj.Contacts.Behaviors.AggressiveChase.Chaser(i)
                    Pred = obj.Hierarchy.Contacts.Behaviors.AggressiveChase.Chaser(i);
                    Prey = obj.Hierarchy.Contacts.Behaviors.AggressiveChase.Escaper(i);
                    
                    d = Elo.Scores(Pred, idx) - Elo.Scores(Prey, idx);
                    ploss = 1 ./ (1 + 10 .^ (d/400));
                    Elo.Scores(Pred, idx + 1) = Elo.Scores(Pred, idx) + ploss * Elo.k;
                    Elo.Scores(Prey, idx + 1) = Elo.Scores(Prey, idx) - ploss * Elo.k;
                    other = 1:obj.nSubjects;
                    other = other(other ~= Pred);
                    other = other(other ~= Prey);
                    Elo.Scores(other, idx + 1) = Elo.Scores(other, idx);
                    Elo.Times(idx + 1) = min(obj.Hierarchy.Contacts.List(i).beg) + starttime;
                    Elo.Day(idx + 1) = oid;
                    Elo.Map(Pred, idx + 1) = true;
                    Elo.Map(Prey, idx + 1) = true;
                    idx = idx + 1;
                end
                %%
                % for s=obj.nSubjects
                %     plot(Elo.Times, Elo.Scores', '.-')
                % end
                starttime = starttime + obj.Video.NumberOfFrames;
            end
            %%
            % C = sum(abs(diff(Elo.Scores, 1, 2)));
            % weighted = Elo.Scores - repmat(min(Elo.Scores), obj.nSubjects, 1);
            % weighted = weighted ./ repmat(max(weighted), obj.nSubjects, 1);
            % weighted(Elo.Map) = 0;
            % w = max(weighted);
            %
            % N = ones(1, size(C, 2)) * 2;
            % Elo.StabilityIndex = C*w(2:end)' / sum(N);
            
            % w =
            Hours = Elo.Times/obj.Video.FrameRate/60/60;
            idx = 1;
            Elo.Median.Scores = zeros(obj.nSubjects, round(max(Hours) / Elo.WindowSize));
            for i=0:Elo.WindowSize:max(Hours)
                if max(Hours) - i < Elo.WindowSize / 2
                    break
                end
                Elo.Median.Scores(:, idx) = median(Elo.Scores(:, Hours >= i & Hours < i + Elo.WindowSize), 2);
                Elo.Median.Times(idx) = (i + Elo.WindowSize / 2) * obj.Video.FrameRate * 60 * 60;
                idx = idx + 1;
            end
            %%
            Elo.Rank = zeros(size(Elo.Scores));
            seq = 1:obj.nSubjects;
            r = zeros(1, obj.nSubjects);
            for i=1:size(Elo.Scores, 2)
                [~, o] = sort(Elo.Scores(:, i), 'descend');
                r(o) = seq;
                Elo.Rank(:, i) = r;
            end
            %%
            if nargout == 0
                %%
                %     plot(Elo.Times, Elo.Scores, 'color');
                %     axis;
                cmap = CheeseSquare.MiceColors('PRBYW');
                for s=1:obj.nSubjects
                    subplot(2,1,1);
                    plot(Elo.Times/obj.Video.FrameRate/60/60, Elo.Scores(s, :), 'color', cmap(s, :));
                    hon;
                    subplot(2,1,2);
                    plot(Elo.Median.Times/obj.Video.FrameRate/60/60, Elo.Median.Scores(s, :), 'o-', 'MarkerFaceColor', cmap(s, :), 'MarkerEdgeColor', 'k', 'color', 'k');
                    hon
                end
                for i=[true find(diff(Elo.Day) ~= 0)]
                    for s=1:2
                        subplot(2,1,s);
                        t = Elo.Times(i)/obj.Video.FrameRate/60/60;
                        a = axis;
                        Fig.VLine(t, 'color', Colors.LightGray);
                        text(t, a(4), sprintf(' day %d', Elo.Day(i+1)), 'verticalalignment', 'top', 'horizontalalignment', 'left', 'color', Colors.DarkGray);
                    end
                end
                
                subplot(2,1,1);
                Fig.Labels('time [hours]', 'score')
                Fig.Fix
                hoff;
                subplot(2,1,2);
                Fig.Labels('time [hours]', 'score')
                Fig.Fix
                hoff
            end
        end
        
        function Show(obj)
            for type=1:2
                dayid = obj.Hierarchy.Group.AggressiveChase.DayID;
                cds = zeros(obj.nSubjects, length(dayid)); % cummulative David-score
                dds = zeros(obj.nSubjects, length(dayid)); % daily David-score
                if type == 1
                    CE = obj.Hierarchy.Group.AggressiveChase.ChaseEscape;
                else
                    CE = obj.Hierarchy.Group.ChaseEscape.ChaseEscape;
                end
                
                for i=1:length(dayid)
                    ce = sum(CE(:, :, 1:i), 3);
                    cds(:, i) = Hierarchy.DavidScore(ce);
                    dds(:, i) = Hierarchy.DavidScore(CE(:, :, i));
                    %%
                    %                 subplot(2,2,3);
                    %                 Fig.Square(length(dayid), i);
                    %                 Plot.ImagePatch(CE(:, :, i), Colormaps.BlueRed, [0, 20]);
                    %                 title(sprintf('day %d', dayid(i)));
                end
                %%
                subplot(2,2,((type-1)*2) + 1);
                cmap = CheeseSquare.MiceColors('PRBYW');
                for i=1:obj.nSubjects
                    plot(dayid, cds(i, :), 'o-', 'markeredgecolor', 'none', 'markerfacecolor', cmap(i, :), 'color', cmap(i, :));
                    Fig.Hon
                end
                Fig.Hoff
                Fig.Fix;
                Fig.Labels('day', 'cummulative David-score');
                if type == 1
                    Fig.Title('Aggressive');
                else
                    Fig.Title('Aggressive & non-aggressive');
                end

                %%
                subplot(2,2,((type-1)*2) + 2);
                cmap = CheeseSquare.MiceColors('PRBYW');
                for i=1:obj.nSubjects
                    plot(dayid, dds(i, :), 'o-', 'markeredgecolor', 'none', 'markerfacecolor', cmap(i, :), 'color', cmap(i, :));
                    Fig.Hon
                end
                Fig.Hoff
                Fig.Fix;
                Fig.Labels('day', 'daily David-score');
            end
        end
        
        
        function Graph(obj, ce, interactions, scale)
            %%
            radius = .1;
            dw = .5;
            if nargin < 2 || isempty(ce)
                ce = obj.Hierarchy.AggressiveChase.ChaseEscape;
            end
            if ~exist('interactions', 'var') || isempty(interactions)
                interactions = ce;
            end
            if nargin < 4 
                scale = 500;
            end
            %%
            DS = Hierarchy.DavidScore(ce);
            [ds, o] = sort(DS);
            middle = (max(ds) + min(ds)) / 2;
            r = (max(ds) - min(ds)) / 2;
            for i=1:length(ds)
                h = middle - ds(i);
                a = acos(h/r);
                x = r * cos(a);
                y = r * sin(a);
                plot(x,y, 'o')
                pause
                Fig.Hon
            end
            Fig.Hoff
            %%
            %rank = Hierarchy.Rank(obj, ce);
            
            rank = Q.rank(DS);
            SO = max(rank) - rank;
            cmap = [CheeseSquare.MiceColors('PRBY'); lines];
            loc = zeros(length(DS), 2);
            for i=1:length(DS)
                so = SO(i);
                count = sum(SO == so);
                offset = -(2*mod(so,2) - 1) * (count - 1 + (so > 0 & so < max(SO))) / 2 + sum(SO(1:i-1) == so);
                loc(i, :) = [offset * dw, DS(i)];
                %loc(i, :) = [offset * dw, mean(DS(SO == os))];
            end
            for i=1:size(loc, 1)
                for j=i+1:size(loc, 1)
                    if interactions(i, j) > interactions(j, i)
                        Patches.Line(loc([i,j], 1), loc([i,j], 2), interactions(i, j)/scale, cmap(i, :), radius/3);
                        Patches.Line(loc([j,i], 1), loc([j,i], 2), interactions(j, i)/scale, cmap(j, :), radius/3);
                    else
                        Patches.Line(loc([j,i], 1), loc([j,i], 2), interactions(j, i)/scale, cmap(j, :), radius/3);
                        Patches.Line(loc([i,j], 1), loc([i,j], 2), interactions(i, j)/scale, cmap(i, :), radius/3);
                    end
                    Fig.Hon
                end
            end
            for i=1:size(loc, 1)
               Patches.Circle(loc(i, 1), loc(i, 2), radius, cmap(i, :));
            end
            Fig.Hoff
            axis equal;
            axis off;
        end
        
        
    end
    
end