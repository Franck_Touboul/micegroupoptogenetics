function [obj, jointProbs, pci] = SocialComputePatternJointProbs(obj, fixed)
% SocialComputeJointProbs(obj, fixed)
%   computed the Joint probability for a set data with discrete values
%   If fixed is set to true then assume at least one observation for
%   each event.
if ~exist('fixed', 'var')
    fixed = false;
end
nZones = length(obj.ROI.ZoneNames);
baseVector = nZones.^(obj.nSubjects-1:-1:0);
count = zeros(1, nZones^obj.nSubjects);
h = baseVector * (obj.zones(:, obj.valid) - 1) + 1;
for i=1:nZones^obj.nSubjects
    count(i) = sum(h == i);
end
if fixed
    count = count + 1;
end
[jointProbs, pci] = binofit(count, sum(count));
