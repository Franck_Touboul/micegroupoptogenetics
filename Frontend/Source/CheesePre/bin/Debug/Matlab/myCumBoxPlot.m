function myBoxPlot(x, labels, colors)
if nargin < 2
    labels = {};
end
if nargin < 3
    colors = colormap(lines);
end
%x = x / x(end);
width = 0.2;
for i=length(x):-1:1
    drawBox(x(i), width, colors(i, :))
    hold on;
end
hold off;

prev = 0;
for i=1:length(x)
    if i <= length(labels)
        text(width/2, prev + (x(i) - prev) / 2, labels{i}, 'Color', 'w', 'VerticalAlignment', 'Middle', 'HorizontalAlignment', 'center');
    end
    prev = x(i);
end
axis([0 width 0 x(end)]);
try
    set(gca, 'XTick', [], 'YTick', x);
end
tickLabels = {};
for i=1:length(x)
    tickLabels{i} = sprintf('%5.2f (%5.1f%%)', x(i), x(i)/x(end)*100);
end
try
set(gca, 'XTick', [], 'YTick', x, 'YTickLabel', tickLabels, 'YAxisLocation', 'right');
end
function drawBox(height, width, color)
fill([0 width width 0], [0 0 height height], color)
