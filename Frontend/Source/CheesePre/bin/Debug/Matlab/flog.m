function y = flog(x)
y = log(x);
y(x == 0) = -realmax;