function s = sec2time(sec, safe)
if nargin == 1 
    safe = false;
end
if safe 
    sep = '''';
else
    sep = ':';
end

if isscalar(sec)
    s = sprintf('%02d%s%02d%s%02d', floor(sec / 3600), sep, mod(floor(sec / 60), 60), sep, mod(floor(sec), 60));
    return;
end
s = cell(size(sec));
for i=1:size(sec,1)
    for j=1:size(sec,2)
        s{i,j} = sprintf('%02d%s%02d%s%02d', floor(sec(i, j) / 3600), sep, mod(floor(sec(i, j) / 60), 60), sep, mod(floor(sec(i, j)), 60));
    end
end
