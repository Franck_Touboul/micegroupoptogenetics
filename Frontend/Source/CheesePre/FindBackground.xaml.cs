﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;

namespace CheeseOne
{
    /// <summary>
    /// Interaction logic for FindBackground.xaml
    /// </summary>
    public partial class FindBackground : Page
    {
        String Filename;

        public FindBackground(String filename)
        {
            InitializeComponent();
            Filename = filename;
            CheeseOne.MainWindow.Matlab.Execute("CheeseBackgroundData.Buffer = {};");
            Video.Load(filename);
        }


        private void TrainButton_Click(object sender, RoutedEventArgs e)
        {
            int nFrames = int.Parse(nFrameTextBox.Text);
            Random rand = new Random();
            //Video.Visibility = System.Windows.Visibility.Hidden;
            //VideoFrameImage.Visibility = System.Windows.Visibility.Visible;
            //for (int i = 0; i < nFrames; ++i)
            //{
            Video.GoToTime(Video.Duration * rand.NextDouble());
            AddFrame();
        }

        private void AddFrame()
        {
            BitmapSource bitmapSource = Video.GrabFrameToMatlab(CheeseOne.MainWindow.Matlab, "image");
            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
            {
                Video.SetBackgroundImage(bitmapSource);
            }));
            CheeseOne.MainWindow.Matlab.Execute("CheeseBackgroundData.Buffer{end+1} = image;");
        }

        private void ManualAddButton_Click(object sender, RoutedEventArgs e)
        {
            nManualFrames.Text = (int.Parse(nManualFrames.Text) + 1).ToString();
            nFrameTextBox.Text = "0";
            AddFrame();
        }

        private void ManualClearButton_Click(object sender, RoutedEventArgs e)
        {
            nFrameTextBox.Text = "40";
            nManualFrames.Text = "0";
            CheeseOne.MainWindow.Matlab.Execute("CheeseBackgroundData.Buffer = {};");
        }
    }
}
