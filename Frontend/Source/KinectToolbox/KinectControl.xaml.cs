﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Globalization;
using Microsoft.Kinect;

namespace KinectToolbox
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class KinectControl : UserControl
    {
        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor Sensor;

        /// <summary>
        /// Bitmap that will hold color information
        /// </summary>
        private WriteableBitmap ColorBitmap;

        /// <summary>
        /// Intermediate storage for the depth data received from the camera
        /// </summary>
        private DepthImagePixel[] DepthImage;

        /// <summary>
        /// Intermediate storage for the depth data received from the camera
        /// </summary>
        private short[] DepthPixels;


        /// <summary>
        /// Intermediate storage for the depth data converted to color
        /// </summary>
        private byte[] ColorPixels;


        public KinectControl()
        {
            InitializeComponent();
        }

        private void KinectControl_Loaded(object sender, RoutedEventArgs e)
        {
            // Look through all sensors and start the first connected one.
            // This requires that a Kinect is connected at the time of app startup.
            // To make your app robust against plug/unplug, 
            // it is recommended to use KinectSensorChooser provided in Microsoft.Kinect.Toolkit
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    Sensor = potentialSensor;
                    break;
                }
            }

            if (null != Sensor)
            {
                // Turn on the depth stream to receive depth frames
                Sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);

                // Allocate space to put the depth pixels we'll receive
                DepthImage = new DepthImagePixel[Sensor.DepthStream.FramePixelDataLength];

                // Allocate space to put the color pixels we'll create
                ColorPixels = new byte[Sensor.DepthStream.FramePixelDataLength * sizeof(int)];

                // Allocate space to put the depth pixels we'll create
                DepthPixels = new short[Sensor.DepthStream.FramePixelDataLength];

                // This is the bitmap we'll display on-screen
                //ColorBitmap = new WriteableBitmap(Sensor.DepthStream.FrameWidth, Sensor.DepthStream.FrameHeight, 96.0, 96.0, PixelFormats.Gray16, null);
                ColorBitmap = new WriteableBitmap(Sensor.DepthStream.FrameWidth, Sensor.DepthStream.FrameHeight, 96.0, 96.0, PixelFormats.Bgr32, null);

                // Set the image we display to point to the bitmap where we'll put the image data
                MainImage.Source = ColorBitmap;

                // Add an event handler to be called whenever there is new depth frame data
                Sensor.DepthFrameReady += SensorDepthFrameReady;

                // Start the sensor!
                try
                {
                    Sensor.Start();
                }
                catch (IOException)
                {
                    Sensor = null;
                }
            }

            if (null == Sensor)
            {
                // this.statusBarText.Text = Properties.Resources.NoKinectReady;
            }
        }

        /// <summary>
        /// Event handler for Kinect sensor's DepthFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SensorDepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
        {
            using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
            {
                if (depthFrame != null)
                {
                    // Copy the pixel data from the image to a temporary array
                    depthFrame.CopyDepthImagePixelDataTo(DepthImage);

                    // Get the min and max reliable depth for the current frame
                    int minDepth = depthFrame.MinDepth;
                    int maxDepth = depthFrame.MaxDepth;

                    // Convert the depth to RGB
                    int colorPixelIndex = 0;
                    for (int i = 0; i < DepthImage.Length; ++i)
                    {
                        // Get the depth for this pixel
                        short depth = DepthImage[i].Depth;

                        // To convert to a byte, we're discarding the most-significant
                        // rather than least-significant bits.
                        // We're preserving detail, although the intensity will "wrap."
                        // Values outside the reliable depth range are mapped to 0 (black).

                        // Note: Using conditionals in this loop could degrade performance.
                        // Consider using a lookup table instead when writing production code.
                        // See the KinectDepthViewer class used by the KinectExplorer sample
                        // for a lookup table example.
                        //byte intensity = (byte)(depth >= minDepth && depth <= maxDepth ? depth : 0);
                        float value = (float)(depth - minDepth) / ((float)(maxDepth - minDepth));
                        value = value < 0 ? 0 : value;
                        value = value > 1 ? 1 : value;
                        byte intensity = (byte)(value * 63.0f);

                        // Write out blue byte
                        ColorPixels[colorPixelIndex++] = Colormap.map[intensity, 0];

                        // Write out green byte
                        ColorPixels[colorPixelIndex++] = Colormap.map[intensity, 1];

                        // Write out red byte                        
                        ColorPixels[colorPixelIndex++] = Colormap.map[intensity, 2];

                        // We're outputting BGR, the last byte in the 32 bits is unused so skip it
                        // If we were outputting BGRA, we would write alpha here.
                        ++colorPixelIndex;
                    }

                    // Write the pixel data into our bitmap
                    ColorBitmap.WritePixels(
                        new Int32Rect(0, 0, ColorBitmap.PixelWidth, ColorBitmap.PixelHeight),
                        ColorPixels,
                        ColorBitmap.PixelWidth * sizeof(int),
                        0);
                }
            }
        }

        private void KinectControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (null != Sensor)
            {
                Sensor.Stop();
            }
        }

        private void NearModeButton_Click(object sender, RoutedEventArgs e)
        {
            if (Sensor != null)
            {
                // will not function on non-Kinect for Windows devices
                try
                {
                    if (NearModeButton.IsChecked.GetValueOrDefault())
                    {
                        Sensor.DepthStream.Range = DepthRange.Near;
                    }
                    else
                    {
                        Sensor.DepthStream.Range = DepthRange.Default;
                    }
                }
                catch (InvalidOperationException)
                {
                }
            }
        }

        private void ButtonScreenshot_Click(object sender, RoutedEventArgs e)
        {
            if (null == Sensor)
            {
                //this.statusBarText.Text = Properties.Resources.ConnectDeviceFirst;
                return;
            }

            // Get the depth for this pixel
            for (int i = 0; i < DepthImage.Length; ++i)
                DepthPixels[i] = DepthImage[i].Depth;

            // create a png bitmap encoder which knows how to save a .png file
            BitmapEncoder encoder = new PngBitmapEncoder();

            WriteableBitmap bitmap = new WriteableBitmap(Sensor.DepthStream.FrameWidth, Sensor.DepthStream.FrameHeight, 96.0, 96.0, PixelFormats.Gray16, null);
            bitmap.WritePixels(
                new Int32Rect(0, 0, ColorBitmap.PixelWidth, ColorBitmap.PixelHeight),
                DepthPixels,
                bitmap.PixelWidth * sizeof(short),
                0);


            // create frame from the writable bitmap and add to encoder
            encoder.Frames.Add(BitmapFrame.Create(bitmap));

            string time = System.DateTime.Now.ToString("hh'-'mm'-'ss", CultureInfo.CurrentUICulture.DateTimeFormat);

            string myPhotos = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

            string path = System.IO.Path.Combine(myPhotos, "KinectSnapshot-" + time + ".png");

            Microsoft.Win32.SaveFileDialog saveFileDialog1 = new Microsoft.Win32.SaveFileDialog();
            saveFileDialog1.Filter = "PNG Image|*.png";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.FileName = path;
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                // write the new file to disk
                try
                {


                    using (FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create))
                    {
                        encoder.Save(fs);
                    }

                    //this.statusBarText.Text = string.Format("{0} {1}", Properties.Resources.ScreenshotWriteSuccess, path);
                }
                catch (IOException)
                {
                    //this.statusBarText.Text = string.Format("{0} {1}", Properties.Resources.ScreenshotWriteFailed, path);
                }
            }

        }
    }
}
