﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using DirectShowLib;

namespace CheeseSquare
{

    class DirectShowTools
    {
        [DllImport("oleaut32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
        public static extern int OleCreatePropertyFrame(
            IntPtr hwndOwner,
            int x,
            int y,
            [MarshalAs(UnmanagedType.LPWStr)] string lpszCaption,
            int cObjects,
            [MarshalAs(UnmanagedType.Interface, ArraySubType = UnmanagedType.IUnknown)] 
			ref object ppUnk,
            int cPages,
            IntPtr lpPageClsID,
            int lcid,
            int dwReserved,
            IntPtr lpvReserved);

        // remove all filters that 
        public static void RemoveDownstream(ref IGraphBuilder Graph, IBaseFilter filter)
        {
            if (filter == null)
                return;
            IEnumPins enumPins;
            IPin[] pins = new IPin[1];
            IntPtr fetched = IntPtr.Zero;

            int hr = filter.EnumPins(out enumPins);
            if (hr == 0)
            {
                while (enumPins.Next(pins.Length, pins, fetched) == 0)
                {
                    IPin ppPin;
                    pins[0].ConnectedTo(out ppPin);
                    if (ppPin != null)
                    {
                        PinInfo pInfo;
                        hr = ppPin.QueryPinInfo(out pInfo);
                        if (hr == 0)
                        {
                            if (pInfo.dir == PinDirection.Input)
                            {
                                RemoveDownstream(ref Graph, pInfo.filter);
                                Graph.Disconnect(ppPin);
                                Graph.Disconnect(pins[0]);
                                Graph.RemoveFilter(pInfo.filter);
                            }
                            pInfo.filter = null;
                        }
                        ppPin = null;
                    }
                    pins[0] = null;
                }
            }

        }
    }
}
