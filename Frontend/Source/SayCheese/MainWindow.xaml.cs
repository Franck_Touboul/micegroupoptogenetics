﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Timers;
using System.Diagnostics;

namespace SayCheese
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<EventTimer> Events = new List<EventTimer>();
        private DateTime StartDate = DateTime.Now;
        private Timer TimeUpdate = null;
        public MainWindow()
        {
            InitializeComponent();
            StartTimeDTP.Value = DateTime.Parse("10:00");
            EndTimeDTP.Value = DateTime.Parse("22:00");
        }

        private Boolean IsRunning()
        {
            return !(((String)StartBTN.Content).Equals("Start"));
        }

        private void StartTimeDTP_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

        }

        private void EndTimeDTP_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

        }

        private void Message(String msg)
        {
            MessageTextBox.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                MessageTextBox.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss    ") + msg + System.Environment.NewLine;
            }));
        }

        private void DayUpdate(Object source, ElapsedEventArgs e)
        {
            CurrentDayUD.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    CurrentDayUD.Value = CurrentDayUD.Value.Value + 1;
                }));

            TimeUpdate.Interval = TimeSpan.FromDays(1).TotalMilliseconds;
            TimeUpdate.AutoReset = false;
            TimeUpdate.Elapsed += DayUpdate;
            TimeUpdate.Enabled = true;

        }

        private void StartRecording(Object source, ElapsedEventArgs e)
        {
            Message(@"starting recording");
            IsRecordingRect.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                IsRecordingRect.Visibility = System.Windows.Visibility.Visible;
            }));
            string cmd = "";
            StartCommandTextBox.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                cmd = StartCommandTextBox.Text;
            }));
            
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.CreateNoWindow = false;
                startInfo.UseShellExecute = false;
                startInfo.FileName = cmd;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                //startInfo.Arguments = "-f j -o \"" + ex1 + "\" -z 1.0 -s y " + ex2;
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                }
            }
            catch (Exception me)
            {
                Message(@"- failed to run command: " + me.ToString());
                IsRecordingRect.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    IsRecordingRect.Visibility = System.Windows.Visibility.Hidden;
                }));
            }
        }

        private void EndRecording(Object source, ElapsedEventArgs e)
        {
            Message(@"ending recording");
            EventTimer nextEvent = new EventTimer();
            nextEvent.executed = true;
            nextEvent.dueTime = DateTime.MaxValue;
            foreach (EventTimer timer in Events)
            {
                double diff = timer.dueTime.Subtract(DateTime.Now).TotalMilliseconds;
                if (!timer.executed && diff > 0 && nextEvent.dueTime > timer.dueTime)
                    nextEvent = timer;
            }
            if (!nextEvent.executed)
            {
                Message(@"next events is scheduled to " + nextEvent.dueTime.ToString("dd/MM/yyyy HH:mm:ss"));
            }
            else
            { 
                IsRecordingRect.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    IsRecordingRect.Visibility = System.Windows.Visibility.Hidden;
                }));
            }

            string cmd = "";
            EndCommandTextBox.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                cmd = EndCommandTextBox.Text;
            }));

            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.CreateNoWindow = false;
                startInfo.UseShellExecute = false;
                startInfo.FileName = cmd;
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                //startInfo.Arguments = "-f j -o \"" + ex1 + "\" -z 1.0 -s y " + ex2;
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                }
            }
            catch (Exception me)
            {
                Message(@"- failed to run command: " + me.ToString());
            }


            CurrentDayUD.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    if (CurrentDayUD.Value.Value == NumberOfDaysUD.Value.Value)
                        Message(@"Done!");
                    StartBTN_Click(null, null);
                }));

        }

        private void StartBTN_Click(object sender, RoutedEventArgs e)
        {
            IsRecordingRect.Visibility = System.Windows.Visibility.Hidden;
            if (TimeUpdate != null)
            {
                TimeUpdate.Enabled = false;
                TimeUpdate = null;
            }
            if (IsRunning())
            { // Stop recording
                StartBTN.Content = "Start";
                StartBTN.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                StartBTN.Background = new SolidColorBrush(Color.FromRgb(215, 227, 163));
                StartBTN.Foreground = new SolidColorBrush(Color.FromRgb(92, 106, 47));
                }));

            }
            else
            { // Start recording
                StartBTN.Content = "Stop";
                StartBTN.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    StartBTN.Background = new SolidColorBrush(Color.FromRgb(251, 185, 169));
                    StartBTN.Foreground = new SolidColorBrush(Color.FromRgb(153, 63, 37));
                }));


                StartDate = DateTime.Now;
                DateTime Tomorrow = StartDate.AddDays(1);
                Tomorrow = new DateTime(Tomorrow.Year, Tomorrow.Month, Tomorrow.Day);
                TimeUpdate = new Timer();
                TimeUpdate.Interval = TimeSpan.FromTicks(Tomorrow.Ticks - StartDate.Ticks).TotalMilliseconds;
                TimeUpdate.AutoReset = false;
                TimeUpdate.Elapsed += DayUpdate;
                TimeUpdate.Enabled = true;

                int currDay = CurrentDayUD.Value.Value;
                int nDays = NumberOfDaysUD.Value.Value;
                TimeSpan starttime = (StartTimeDTP.Value.Value).TimeOfDay;
                TimeSpan endtime = (EndTimeDTP.Value.Value).TimeOfDay;
                TimeSpan currtime = (DateTime.Now).TimeOfDay;
                int dayidx = 0;
                for (int i = currDay; i <= nDays; ++i)
                {
                    Timer startTimer = new Timer();
                    Timer endTimer = new Timer();
                    TimeSpan startTS = TimeSpan.FromTicks(starttime.Ticks - currtime.Ticks).Add(TimeSpan.FromDays(dayidx));
                    TimeSpan endTS = TimeSpan.FromTicks(endtime.Ticks - currtime.Ticks).Add(TimeSpan.FromDays(dayidx));
                    
                    if (startTS.TotalMilliseconds > 0 || endTS.TotalMilliseconds > 0)
                    {
                        startTimer.Interval = startTS.TotalMilliseconds > 0 ? startTS.TotalMilliseconds : 1;
                        startTimer.Elapsed += StartRecording;
                        startTimer.AutoReset = false;
                        startTimer.Enabled = true;
                        Events.Add(startTimer);
                    }

                    if (endTS.TotalMilliseconds > 0)
                    {
                        endTimer.Interval = endTS.TotalMilliseconds;
                        endTimer.Elapsed += EndRecording;
                        endTimer.AutoReset = false;
                        endTimer.Enabled = true;
                        Events.Add(endTimer);
                    }

                    /*newTimer = new Timer();
                    newTimer.Interval = (endtime.Milliseconds - currtime.Milliseconds) + (double)(dayidx * 24 * 60 * 60 * 1000);
                    newTimer.Interval = newTimer.Interval > 0 ? newTimer.Interval : 0;
                    newTimer.Elapsed += StartRecording;
                    newTimer.AutoReset = false;
                    newTimer.Enabled = true;
                    Events.Add(newTimer);*/

                    dayidx = dayidx + 1;
                }

            }
        }
    }

    class EventTimer
    {
        public Timer timer;
        public DateTime dueTime;
        public String type;
        public Boolean executed = false;
    }
}
