function pos = mask2poly(mask, n)
B = bwboundaries(mask, 8, 'noholes');
[x, y] = minboundrect(B{1}(:, 1), B{1}(:, 2));
pos = [y(:, 1), x(:, 1)];
return;

%%
%# get boundary
B = bwboundaries(mask, 8, 'noholes');
B = B{1};

%# convert boundary from cartesian to ploar coordinates
objB = bsxfun(@minus, B, mean(B));
[theta, rho] = cart2pol(objB(:,2), objB(:,1));

%# find corners
[val, locs] = findpeaks(rho);
[~, order] = sort(val, 'descend');
locs = locs(order(1:min(n, length(locs))));

pos = [B(locs, 2), B(locs, 1)];

if nargout == 0
    %%
    subplot(2,1,1);
    plot(theta, rho, '.');
    hold on
    plot(theta(locs), rho(locs), 'go');
    hold off
    %%
    imagesc(mask);
    hold on;
    plot(pos(:, 1), pos(:, 2), 'go');
    hold off;
end
