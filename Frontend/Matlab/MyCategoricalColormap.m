function [cmap, cobj] = MyCategoricalColormap(levels)
if ~exist('levels', 'var')
    levels = 256;
end
cmap = MySubCategoricalColormap(levels * 2);
cmap = cmap(2:2:end, :);
cobj.Red = cmap(3, :);
cobj.Green = cmap(2, :);
cobj.Blue = cmap(1, :);
%cmap = [148 216 239; 0 177 229; 179 213 157; 53 178 87; 234 165 193; 220 73 89; 240 191 148; 240 145 55; 189 190 200; 98 111 179; 246 237 170; 248 231 83]/256;