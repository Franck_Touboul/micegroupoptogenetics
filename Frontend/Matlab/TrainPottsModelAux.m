function local = TrainPottsModelAux(local)
if ischar(local)
    local = TrackLoad(local);
end
Options = local.Options;
for iter = 1:Options.nIters
    local.nIters = Options.nAlgIterms(1);
    if local.nIters > 0
        local.MinNumberOfIters = Options.MinNumberOfIters(1);
        me = trainPottsModelUsingGIS(local, local.data, Options.Confidence);
        local.initialPottsWeights = me.weights;
    end
    orig = me;
    %    if local.nIters == 0 || ~me{level}.converged || obj.Analysis.Potts.MinNumberOfIters(2) > 0
    local.nIters = Options.nAlgIterms(2);
    local.MinNumberOfIters = Options.MinNumberOfIters(2);
    if Options.Regularize
        me = trainPottsModelUsingNesterovGDWithRidgeRegression(local, local.data, Options.Confidence);
    else
        me = trainPottsModelUsingNesterovGD(local, local.data, Options.Confidence);
    end
    if any(isnan(me.weights))
        me = orig;
    end
    if me.converged
        break;
    else
        local.initialPottsWeights = me.weights;
    end
end
local.Model = me;
%TrackSave(local);
