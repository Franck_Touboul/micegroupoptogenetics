function TieAxis(m, n, ids, dim)
if nargin < 4
    dim = [1 2];
end

a = [inf -inf inf -inf];
for i=ids
    subplot(m, n, i);
    curra = axis;
    %a = curra;
    if any(dim == 1)
        a([1,3]) = min([curra([1,3]); a([1,3])]);
    end
    if any(dim == 2)
        a([2,4]) = max([curra([2,4]); a([2,4])]);
    end
end

for i=ids
    subplot(m, n, i);
    axis(a);
end