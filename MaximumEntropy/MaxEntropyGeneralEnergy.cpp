#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "mex.h"
#include <algorithm>
/*
 * syntax: MaxEntropyGeneralEnergy(nvals, patterns, weights, checksort)
 */

double* energy;

uint64_t powi(uint64_t v, int c)
{
    uint64_t res = 1;
    for (int i=0; i<c; ++i)
        res *= v;
    return res;
}
 
void process(int* curr, int dim,  int nvals, double weight, uint64_t offset)
{
    //printf("%d, %d, %d\n", *curr, dim, offset);
    if (*curr == 0)
    {
        if (dim > 1)
        {
            uint64_t delta = powi(nvals, dim-1);
            for (int i=0; i<nvals; ++i)
            {
                process(curr+1, dim-1, nvals, weight, offset);
                offset += delta;
            }
        } 
        else
        {
            for (int i=0; i<nvals; ++i)
                energy[offset + i] += weight;
        }
    }
    else
    {
        if (dim > 1)
            process(curr+1, dim-1, nvals, weight, offset + (*curr) * powi(nvals, dim-1));
        else
            energy[offset + (*curr)] += weight;
    }
}


/*void process3(int* curr, int dim,  int nvals, double weight)
{
    int idx = 0;
 
    uint64_t offset = 0;
    bool first = true;
    size_t sz = nvals* dim;
    //printf("curr(%d): %d, %d, %d, %d, %d\n", idx, *curr, dim, offset, nvals, sz);
    if (qCurr == NULL)
    {
        qCurr = (int**)malloc(sz * sizeof(int*));
        qDim = (int*)malloc(sz * sizeof(int));
        qOffset = (uint64_t*)malloc(sz * sizeof(uint64_t));
    }
    
    qCurr[idx] = curr;
    qDim[idx] = dim;
    qOffset[idx] = offset;
    do
    {
        curr = qCurr[idx];
        dim = qDim[idx];
        offset = qOffset[idx];
        //printf("curr(%d): %d, %d, %d\n", idx, *curr, dim, offset);
        --idx;
        if (*curr == 0)
        {
            if (dim > 1)
            {
                uint64_t delta = powi(nvals, dim-1);
                for (int i=0; i<nvals; ++i)
                {
                    ++idx;
                    //printf("%d => %d, %d, %d\n", idx, *(curr + 1), dim-1, offset);
                    qCurr[idx] = curr + 1;
                    qDim[idx] = dim - 1;
                    qOffset[idx] = offset;
                    offset += delta;
                }
            }
            else
            {
                for (int i=0; i<nvals; ++i)
                    energy[offset + i] += weight;
            }
        }
        else
        {
            if (dim > 1)
            {
                //process(curr+1, dim-1, nvals, weight, offset + (*curr) * powi(nvals, dim-1));
                ++idx;
                    //printf("%d => %d, %d, %d\n", idx, *(curr + 1), dim-1, offset + (*curr) * powi(nvals, dim-1));
                qCurr[idx] = curr + 1;
                qDim[idx] = dim - 1;
                qOffset[idx] = offset + (*curr) * powi(nvals, dim-1);
            }
            else
                energy[offset + (*curr)] += weight;
        }
    } while (idx >= 0);
}


void process2(int* curr, int dim,  int nvals, double weight, double *energy, uint64_t* offset, int* values)
{
    int i = 0;
    //memset(values, 0, dim * sizeof(int));
    
    bool done = false;
    bool back = false;
    while (!done)
    {
        if (curr[i] == 0)
        {
            if (!back) {
                values[i] = 0;
                offset[i] = (i > 0 ? offset[i - 1] : 0);
            }
            else
            {
                values[i]++;
                if (values[i] == nvals)
                {
                }
                else
                {
                    //memset(values+i+1, 0, (dim-i-1) * sizeof(int));
                    back = false;
                    offset[i] = (i > 0 ? offset[i - 1] : 0) + values[i] * (uint64_t)powi(nvals, dim-i-1);
                }
            }
        }
        else
        {
            offset[i] = (i > 0 ? offset[i - 1] : 0) + curr[i] * (uint64_t)powi(nvals, dim-i-1);
        }
        if (back)
        {
            if (i > 0)
                i--;
            else
                break;
        }
        else
        {
            if (i==dim-1)
            {
                energy[offset[dim-1]] += weight;
                back = true;
                i--;
            }
            else
                i++;
        }
            
    }
}
*/
template<typename T> void compute(mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
/*    if (!mxIsDouble(prhs[1]))
    {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:wrongtype",
                "patterns matrix should be of class double");
    }*/
    bool checkpatterns = true;
    if (nrhs >= 4)
        checkpatterns = mxGetScalar(prhs[3]) != 0;
    
    if (!mxIsDouble(prhs[2]))
    {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:wrongtype",
                "weights vector should be of class double");
    }
    int nvals = (int)mxGetScalar(prhs[0]);
    int dim = (int)mxGetN(prhs[1]);
    size_t nPatterns = mxGetM(prhs[1]);
    uint64_t nSamples = powi(nvals, dim);
    T p;
    if (mxGetNumberOfElements(prhs[2]) != nPatterns)
    {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:dimincon",
                "dimensions of weights should equal number of samples");
    }

    /* Create a local array and load data */
    T *patterns = (T*)mxGetData(prhs[1]);
    double *weights = mxGetPr(prhs[2]);
    int *currsamp = (int*)malloc(dim * sizeof(int));
    memset(currsamp, 0, dim * sizeof(int));

    plhs[0] = mxCreateNumericMatrix((mwSize)nSamples, 1, mxDOUBLE_CLASS, mxREAL);
    energy = mxGetPr(plhs[0]);

    uint64_t *offsets = (uint64_t*)malloc(dim * sizeof(uint64_t));
    int *values = (int*)malloc(dim * sizeof(int));
    
    for (size_t c = 0; c < nPatterns; c++)
    {   
        //printf("pattern: ");
        for (int d = 0; d < dim; d++) {
            currsamp[d] = (int)patterns[c + d * nPatterns];
            //printf("%d  ", currsamp[d]);
        }
        //printf("   %f\n", weights[c]);
        process(currsamp, dim, nvals, weights[c], 0);
        //process3(currsamp, dim, nvals, weights[c]);
        //process2(currsamp, dim, nvals, weights[c], energy, offsets, values);
    }
    free(currsamp);
    free(offsets);
    free(values);

}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    if (nrhs < 3) {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:minrhs",
                "syntax: MaxEntropyGeneralEnergy(nvals, patterns, weights, checksort)");
    }
    
    if(nlhs > 1){
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:maxlhs",
                "Too many output arguments.");
    }
  
    switch (mxGetClassID(prhs[1]))  {
        case mxINT8_CLASS:   compute<int8_t>(plhs, nrhs, prhs);   break;
        case mxUINT8_CLASS:  compute<uint8_t>(plhs, nrhs, prhs); break;
        case mxINT16_CLASS:  compute<int16_t>(plhs, nrhs, prhs); break;
        case mxUINT16_CLASS: compute<uint16_t>(plhs, nrhs, prhs); break;
        case mxINT32_CLASS:  compute<int32_t>(plhs, nrhs, prhs); break;
        case mxUINT32_CLASS: compute<uint32_t>(plhs, nrhs, prhs); break;
        case mxINT64_CLASS:  compute<int64_t>(plhs, nrhs, prhs); break;
        case mxUINT64_CLASS: compute<uint64_t>(plhs, nrhs, prhs); break;
        case mxSINGLE_CLASS: compute<float>(plhs, nrhs, prhs); break;
        case mxDOUBLE_CLASS: compute<double>(plhs, nrhs, prhs); break;
      default: break;    
    }
    
}
