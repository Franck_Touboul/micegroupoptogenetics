#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "mex.h"
#include <algorithm>
/*
 * syntax: MaxEntropyGeneralExpectation(data, patterns)
 */

double gammln(double xx) // Returns the value ln[?(xx)] for xx > 0.
{
    // Internal arithmetic will be done in double precision, a nicety that you can omit if five-figure
    // accuracy is good enough.
    double x,y,tmp,ser;
    static double cof[6]={76.18009172947146,-86.50532032941677, 24.01409824083091,-1.231739572450155,0.1208650973866179e-2,-0.5395239384953e-5};
    int j;
    y=x=xx;
    tmp=x+5.5;
    tmp -= (x+0.5)*log(tmp);
    ser=1.000000000190015;
    for (j=0;j<=5;j++) ser += cof[j]/++y;
    return -tmp+log(2.5066282746310005*ser/x);
}

double factln(size_t n) //Returns ln(n!).
{
    static double a[101]; // A static array is automatically initialized to zero.
    if (n < 0)
        return NAN;
    if (n <= 1)
        return 0.0;
    if (n <= 100)
        return a[n] ? a[n] : (a[n]=gammln(n+1.0)); //In range of table.
    else
        return gammln(n+1.0); // Out of range of table.
}


double bico(size_t n, size_t k) // Returns the binomial coefficient (n over k) as a floating-point number
{
    return floor(0.5+exp(factln(n)-factln(k)-factln(n-k)));
// The floor function cleans up roundoff error for smaller values of n and k.
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    if (nrhs < 3) {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:minrhs",
                "syntax: GeneratePatterns(dim, order, nvals)");
    }
    if(nlhs > 1){
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:maxlhs",
                "Too many output arguments.");
    }
    size_t dim = (size_t)mxGetScalar(prhs[0]);
    size_t order = (size_t)mxGetScalar(prhs[1]);
    size_t nvals = (size_t)mxGetScalar(prhs[2]);
    
    size_t outputsize = 0;
    for (int o=1; o<=order; ++o)
        outputsize += (size_t)((size_t)bico(dim, o) * pow(nvals-1, o));
    mexPrintf("outsize = %d\n", outputsize);
    
    plhs[0] = mxCreateNumericMatrix(outputsize, dim, mxDOUBLE_CLASS, mxREAL);
    double* expectation = mxGetPr(plhs[0]);

    for (int index = 0; index < nPatterns; index++ )
      expectation[index] = ((double)count[index]) / nSamples;
}
