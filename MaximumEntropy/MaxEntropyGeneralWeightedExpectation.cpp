#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "mex.h"
#include <algorithm>
/*5
 * syntax: MaxEntropyGeneralWeightedExpectation(nvals, patterns, probs)
 */

uint64_t powi(uint64_t v, int c)
{
    uint64_t res = 1;
    for (int i=0; i<c; ++i)
        res *= v;
    return res;
}

void process(size_t id, int* curr, int dim,  int nvals, double* probs, double *expectation, uint64_t offset)
{
    if (*curr == 0)
    {
        if (dim > 1)
        {
            uint64_t delta = powi(nvals, dim-1);
            for (int i=0; i<nvals; ++i)
            {
                process(id, curr+1, dim-1, nvals, probs, expectation, offset);
                offset += delta;
            }
        } 
        else
        {
            for (int i=0; i<nvals; ++i)
                expectation[id] += probs[offset + i];
        }
    }
    else
    {
        if (dim > 1)
            process(id, curr+1, dim-1, nvals, probs, expectation, offset + (*curr) * powi(nvals, dim-1));
        else
            expectation[id] += probs[offset + (*curr)];
    }
}

void processold(size_t id, int* curr, int dim,  int nvals, double* probs, double *expectation, uint64_t offset)
{
    if (dim == 0)
    {
        expectation[id] += probs[offset];
    }
    else
    {
        if (*curr == 0)
        {
            for (int i=0; i<nvals; ++i)
                process(id, curr+1, dim-1, nvals, probs, expectation, offset + i * (uint64_t)powi(nvals, dim-1));
        }
        else
            process(id, curr+1, dim-1, nvals, probs, expectation, offset + (*curr) * (uint64_t)powi(nvals, dim-1));
    }
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    if (nrhs < 3) {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:minrhs",
                "syntax: MaxEntropyGeneralWeightedExpectation(nvals, patterns, probs)");
    }
    if(nlhs > 1){
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:maxlhs",
                "Too many output arguments.");
    }
    if (!mxIsDouble(prhs[1]))
    {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:wrongtype",
                "patterns matrix should be of class double");
    }
    if (!mxIsDouble(prhs[2]))
    {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:wrongtype",
                "weights vector should be of class double");
    }
    uint64_t nvals = (uint64_t)mxGetScalar(prhs[0]);
    size_t dim = mxGetN(prhs[1]);
    size_t nPatterns = mxGetM(prhs[1]);
    uint64_t nSamples = powi(nvals, dim);
    if (mxGetNumberOfElements(prhs[2]) != nSamples)
    {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:dimincon",
                "dimensions of weights should equal number of samples");
    }

    /* Create a local array and load data */
    double *patterns = mxGetPr(prhs[1]);
    double *probs = mxGetPr(prhs[2]);
    int *currsamp = (int*)malloc(dim * sizeof(int));
    memset(currsamp, 0, dim * sizeof(int));

    plhs[0] = mxCreateNumericMatrix(1, (mwSize)nPatterns, mxDOUBLE_CLASS, mxREAL);
    double* expectation = mxGetPr(plhs[0]);
    
    for (size_t c = 0; c < nPatterns; c++)
    {    
        for (int d = 0; d < dim; d++)
            currsamp[d] = (int)patterns[c + d * nPatterns];
        process(c, currsamp, dim, nvals, probs, expectation, 0);
    }
    free(currsamp);
/*
    bool succ;
    for (int r = 0; r < nSamples; r++)
    {
        for (int c = 0; c < nPatterns; c++)
        {
            succ = true;
            for (int d = 0; d < dim; d++)
            {
                if (patterns[c + d * nPatterns] != 0 && patterns[c + d * nPatterns] != currsamp[d])
                {
                    succ = false;
                    break;
                }
            }
            if (succ)
                expectation[c] += weights[r];
        }
        currsamp[dim-1]++;
        for (int d = dim-1; d >0; d--)
        {
            if (currsamp[d] >= nvals)
            {
                currsamp[d] = 0;
                if (d > 0)
                    currsamp[d-1]++;
            }
            else
                break;
        }
    }*/
}
