#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "mex.h"
#include <algorithm>
/*
 * syntax: MaxEntropyGeneralExpectation(data, patterns)
 */


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    if (nrhs < 2) {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:minrhs",
                "syntax: MaxEntropyGeneralExpectation(data, patterns)");
    }
    if(nlhs > 1){
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:maxlhs",
                "Too many output arguments.");
    }
    if (mxGetClassID(prhs[0]) != mxDOUBLE_CLASS)
    {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:wrongtype",
                "data matrix should be of class double");
    }
    if (mxGetClassID(prhs[1]) != mxDOUBLE_CLASS)
    {
        mexErrMsgIdAndTxt( "MATLAB:MaxEntropyGeneral:wrongtype",
                "patterns matrix should be of class double");
    }
    size_t nSamples = mxGetM(prhs[0]);
    size_t dim = mxGetN(prhs[0]);
    size_t nPatterns = mxGetM(prhs[1]);

    /* Create a local array and load data */
    double *data = mxGetPr(prhs[0]);
    double *patterns = mxGetPr(prhs[1]);
    double *currpat = (double*)malloc(dim * sizeof(double));
    uint64_t *count = (uint64_t*)malloc(nPatterns * sizeof(uint64_t));
    std::fill_n(count, nPatterns, 0);
    bool succ;
    for (int c = 0; c < nPatterns; c++)
    {
        for (int d = 0; d < dim; d++)
            currpat[d] = patterns[c + d * nPatterns];
        for (int r = 0; r < nSamples; r++)
        {
            succ = true;
            for (int d = 0; d < dim; d++)
            {
                if (currpat[d] != 0 && data[r + d * nSamples] != currpat[d])
                {
                    succ = false;
                    break;
                }
            }
            if (succ)
                count[c]++;
        }
    }

    plhs[0] = mxCreateNumericMatrix(1, nPatterns, mxDOUBLE_CLASS, mxREAL);
    double* expectation = mxGetPr(plhs[0]);

    for (int index = 0; index < nPatterns; index++ )
        expectation[index] = ((double)count[index]) / nSamples;
    free(currpat);
    free(count);
}
